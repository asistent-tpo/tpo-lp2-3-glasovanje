# Načrt sistema

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Andrej Hafner, Anže Mur, Gašper Hüll, Luka Galjot |
| **Kraj in datum** | Večna pot, 22. 4. 2019 |



## Povzetek

V dokumentu je opisan načrt sistema aplikacije StraightAs. Z UML diagrami je definiran načrt arhitekture, znotraj katerega sta definirana logični pogled in razvojna pogleda za spletno aplikacijo in zaledni sistem. Namenjen je razvijalcem vodjem projekta, omogoča višji pogled nad aplikacijo. Struktura je določena z razrednim diagramom, ki povzema vse ključne razrede v aplikaciji, kaj omogočajo in interakcije med njimi. Omogoča načrtovanje podatkovnega modela in razvijalcem omogoča pregled nad lastnostmi in funkcijami razredov. Primeri uporabe aplikacije so razširjeni z diagrami zaporedja v načrtu obnašanja. Slednji določajo potek določenega primera uporabe in definirajo alternativne možnosti.

## 1. Načrt arhitekture

**Logični pogled**  
![Alt text](../img/logical_view_svg.svg)  

**Razvojni pogled spletne aplikacije**  
![Alt text](../img/frontend_dev_svg.svg)

**Razvojni pogled zalednega sistema**  
![Alt text](../img/backend_dev_svg.svg)  


## 2. Načrt strukture

### 2.1 Razredni diagram

![Alt text](../img/classesDi.svg)

### 2.2 Opis razredov

##### Legenda razredov:

    * Entitetni razred - MODRA
    * Kontrolni razred - RDEČA
    * Mejni razred - ZELENA

#### Prikazi predmete in podrobnosti predmeta (Mejni razred)

* prikaziPredmete(): void
* dodajPredmet(predmet; Predmet): void
* urediPredmet(predmet: Predmet, predmetID: Integer): void
* izbrisiPredmet(predmetID: Integer): void
* prikaziStatusUspesno(): void
* prikaziStatusNeuspesno(): void

#### Pregled in upravljanje koledarja (Mejni razred)

* prikaziKoledar(): void
* prikaziSamoDoloceneTipeDogodkov(tip: Integer): void
* preklopMedTedenskimInMesecnimPrikazom(): void
* dodajDogodekNaKoledar(dogodek: Dogodek): Integer
* urediObstojeciDogodek(dogodek: Dogodek): Integer
* prikaziStatusUspesno(): void
* prikaziStatusNeuspesno(): void

#### Prikazi ocene (Mejni razred)

* prikaziOcene(studentID: Integer): void
* prikaziStatusUspesno(): void
* prikaziStatusNeuspesno(): void

#### Prikazi urnik (Mejni razred)

* prikaziUrnik(studentID: Integer): void
* prikaziStatusUspesno(): void
* prikaziStatusNeuspesno(): void

#### Urnik (Mejni razred) - Zunanji sistem

* urnik(studentID: Integer): Dogodek []

#### Študentski Informacijski Sistem (Mejni razred) - Zunanji sistem

* izpitniRoki(predmetID: Integer) : IzpitniRok []
* ocene(studentID: Integer) : Ocena []

#### Facebook Events (Mejni razred) - Zunanji sistem

* pridobiDogodke(): Dogodki []

#### Prikazi študente in upravljanje s študenti (Mejni razred)

* prikaziStudente(): void
* potrdiRegistracijoStudenta(): void
* urediPodatkeStudenta(student: Student, studentID: Integer): void
* izbrisiStudenta(studentID: Integer): void
* prikaziStatusUspesno(): void
* prikaziStatusNeuspesno(): void

#### Prikazi studentske dogodke (Mejni razred)

* prikaziDogodke(): void
* dodajDogodekNaKoledar(dogodekID: Integer): void
* prikaziStatusUspesno(): void
* prikaziStatusNeuspesno(): void


#### Prijava in registracija (Mejni razred)

* prikaziStatusUspesno(): void
* prikaziStatusNeuspesno(): void
* izvediPrijavo(uporabniskoIme: String, geslo: String): void
* izvediRegistracijo(ime: String,  priimek: String, uporabniksoIme: String, vpisnaStevilka: Integer, fakulteta: String): void

#### Prikazi predmete in podrobnosti predmeta (Kontrolni razred)
* vrniPredmete(fakulteta: String, studijskaSmer: String): Predmeti[]
* dodajPredmet(predmet: Predmet): Predmet
* urediPredmet(predmet: Predmett, predmetID: Integer): Predmet
* izrbisiPredmet(predmetID: Integer): Predmet
* dodajIzpitniRokNaKoledar(izpitniRokID: Integer)
* pridobiIzpitneRokeZaPredmet(predmetID: Integer)

#### Prikazi ocene (Kontrolni razred)

* vrniOcene(studentID): Ocene []
* pridobiOcene(studentID): Ocene[]

#### Prikazi urnik (Kontrolni razred)

* vrniUrnik(studentID): Dogodek[]
*  pridobiDogodkeUrnika(studentID: Intiger) : Dogodek[]
 
#### Pregled in upravljanje koledarja (Kontrolni razred)

* vrniDogodke(tip: Integer): Dogodek []
* dodajDogodek(dogodek: Dogodek): Dogodek
* urediDogodek(dogodek: Dogodek): Dogodek

#### Prikazi študente in upravljanje s študenti (Kontrolni razred)

* prikaziStudente(): Studenti[]
* potrdiRegistaracijo(studentID): Integer
* urediStudenta(student: Student, studentID: Integer): Student
* izrbisiStudenta(studenttID: Integer): Student

#### Prikazi studentske dogodke (Kontrolni razred)

* vrniDogodke(): Dogodek[]
* dodajDogodekNaKoledar(dogodekID: Integer): Dogodek
* pridobiStudentskeDogodke()

#### Prijava in registracija (Kontrolni razred)

* izvediPrijavo(uporabniskoIme: String, geslo: String): Boolean
* izvediRegistracijo(ime: String,  priimek: String, uporabniksoIme: String, vpisnaStevilka: Integer, fakulteta: String): Boolean

#### Oseba (Entitetni razred)

##### Atributi:

* ime :  String
* priimek : String
* uporabniskoIme : String
* geslo : String
* uporabniskaVloga : Integer 
* ID : Integer

##### Metode:

* avtentikacijaOsebe(uporabniskoIme: String, geslo: String) : Boolean
* avtorizacijaOsebe(): Boolean

#### Skrbnik (Entitetni razred)

##### Atributi:

* IDstevilka : Integer -> ID, ki zagotovi, da je uporabnik Skrbnik

#### Referent (Entitetni razred)

##### Atributi:

* IDstevilka : Integer -> ID, ki zagotovi, da je uporabnik Referent

#### Študent (Entitetni razred)

##### Atributi:

* vpisnaStevilka : Integer
* fakulteta : String

##### Metode:

* registracijaStudenta(ime: String,  priimek: String, uporabniksoIme: String, vpisnaStevilka: Integer, fakulteta: String): Študent
* IzbirisiStudenta(ID: Integer): Student
* urediStudenta(student: Student, ID: Integer): Student

#### Koledar (Entitetni razred)

##### Atributi:

* ID: Integer

##### Metode:

* filtrirajDogodke(tip: Integer): Dogodek[]

#### Dogodek (Entitetni razred)

##### Atributi:

* ime:  String
* opis: String
* lokacija: String
* casZacetka: Date
* casKonca: Date
* ID: Integer
* tip: Integer

##### Metode:

* dodajDogodek(dogodek: Dogodek): Dogodek
* izbrišiDogodek(dogodekID: Integer): Dogodek
* urediDogodek(dogodekID: Integer)

#### Ocena (Entitetni razred)

##### Atributi:

* ID : Integer
* ocena: Integer
* predmetID: Integer
* datumPrejetja: Date
* predmet: Predmet

##### Metode:

* dodajDogodek(dogodek: Dogodek): Dogodek
* izbrišiDogodek(dogodekID: Integer): Dogodek
* urediDogodek(dogodekID: Integer)

#### Predmet (Entitetni razred)

##### Atributi:

* ID : Integer
* ime :  String
* nosilec: String
* opis: String
* letnik: Integer
* studijskaSmer: String
* fakulteta: String

#### IzpitniRok (Entitetni razred)

##### Atributi:

* ID : Integer
* rok:  Date

#### Nesamoumevne metode

## 3. Načrt obnašanja

**Pregled Predmetov**  
![Alt text](../img/pregled_predmetov.svg)  

**Pregled izpitnih rokov in izvoz izpitnih rokov**  
![Alt text](../img/pregled_izpitnih_rokov.svg)  

**Pregled svojih izpitnih rokov**  
![Alt text](../img/pregled_svojih_izpitnih_rokov.svg)  

**Pregled ocen**  
![Alt text](../img/pregled_ocen.svg)  

**Ogled urnika**  
![Alt text](../img/ogled_urnika.svg)  

**Preveri kreditne točke po letnikih**  
![Alt text](../img/preveri_kreditne_tocke_po_letnikih.svg) 

**Pregled koledarja**  
![Alt text](../img/pregled_koledarja.svg)  

**Dodajanje opomnika**  
![Alt text](../img/dodajanje_opomnika.svg)  

**Pregled in urejanje opomnika**  
![Alt text](../img/pregled_in_urejanje_opomnika.svg)  

**Pregled študentskih dogodkov**  
![Alt text](../img/pregled_studentskih_dogodkov.svg)  

**Izvoz dogodkov v koledar**  
![Alt text](../img/izvoz_dogodkov_v_koledar.svg)  

**Potrdi registracijo študenta**  
![Alt text](../img/potrdi_registracijo_studenta.svg)  

**Vzdrževanje podatkov študenta**  
![Alt text](../img/vzdrzevanje_podatkov_studenta.svg)  

**Vzdrževanja podatkov predmeta**  
![Alt text](../img/vzdrzevanje_podatkov_predmeta.svg)  


