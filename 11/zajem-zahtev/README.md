# Dokument zahtev

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Andrej Hafner, Anže Mur, Gašper Hüll, Luka Galjot |
| **Kraj in datum** | Večna pot, 2. 4. 2019 |

## Povzetek projekta

StraightAs je aplikacija za organizacijo in upravljanje z sprotnimi obveznostmi, hkrati pa omogoča pregled nad izpitnimi roki, ocenami študenta in uvoz dogodkov, ki bi bili lahko študentom zanimivi. Povezana je s študijskim sistemom ŠIS in platformo Facebook, kar omogoča pregled nad vsemi aktivnostmi na enem mestu. Uporabljali jo bodo lahko registrirani in neregistrirani študentje, za preverjanje študentov bo skrbel referent, skrbnik pa bo upravljal s podatki o predmetu. Aplikacija bo morala biti zaradi pomembne vsebine dostopna večino časa, hkrati pa odzivna in študentu prijazna. Vsebovala bo osebne podatke, zaradi česar ustreza vsem zadnjim standardom o varovanju osebnih podatkov. Uporabniški vmesnik bo privlačen in preprost za uporabo, glavni del bo sestavljen iz koledarja, iz katerega bo študent lahko prehajal na vse ostale funkcionalnosti.  

## 1. Uvod

Način opravljanja študija se je v zadnjih časih močno spremenil. Včasih so se študentje učili samo za kolokvije in izpite, ter delali seminarske naloge. Z začetkom izvajanja bolonjskega sistema študija se je to močno spremenilo. Velik poudarek se je začelo dajati na sprotno delo. Študentje niso več samo močno obremenjeni v času izpitnih obdobij, ampak skozi celotno študijsko leto. Sprotno delo se izvaja v obliki kvizov, projektov, domačih nalog in izzivov. Cilj tega je, da bi se študent že sproti naučil snov določenega predmeta in bi se tako lahko izognil neučinkovitem kampanjskem učenju.

Z uvajanjem vedno več sprotnega dela, se je povečala tudi kognitivna obremenjenost študentov. Študentje morajo slediti velikem številu rokov za dokončanje obveznosti, v povprečju ima študent v enem semestru vsaj 5 predmetov, vsak pa ima lahko več sprotnih obveznosti. Poleg tega mora upoštevati tudi razna preverjanja znanja. Sledenje vsem rokom je lahko naporno, poleg tega pa se je potrebno organizirati kdaj bo kaj narejeno. 

Aplikacija StraightAs je namenjena študentom, ki se težko organizirajo in pogosto pozabijo na obveznosti, hkrati pa tudi vsem, ki bi radi imeli vse podatke povezane z študijem in študentskim življenjem zbrane na enem mestu. Omogočala bo upravljanje z obveznostmi, študent si bo lahko izdelal opomnike in imel vsak dan pregled nad tem kaj mora opraviti. Slednje bo mogoče izvoziti tudi v koledar, za boljšo preglednost. Aplikacija bo povezana bo s študijskim informacijskim sistemom, kar bo omogočalo pregled nad izpitnimi roki in ocenami študenta. V aplikaciji bo tudi urnik, ki bo povezan z urnikom fakultete študenta. Študent bo imel tudi pregled nad predmeti. Uporabljali jo bodo lahko registrirani in neregistrirani študenti, slednji pa bodo imeli omejen nabor funkcionalnosti. Za preverjanje avtentičnosti študenta in vzdrževanje podatkov bodo skrbeli referenti, podatke predmetov pa bo posodabljal skrbnik sistema. Dodajanje dogodkov v koledar bo možno tudi iz platforme Facebook, saj si marsikateri študent po napornem tednu želi sprostitve.

V Sloveniji je leta 2017 študiralo več kot 38.000 študentov, vsi pa so potencialni študenti aplikacije. Potrebno je zagotoviti, da je aplikacija vedno na voljo in omogoča nemoteno uporabo tudi v izpitnih obdobjih, ko bo uporaba največja. Potrebno je zagotoviti tudi varnost in integriteto podatkov v aplikaciji. Študenti ne smejo dostopati do osebnih podatkov ostalih študentov, podatki o predmetih pa morajo biti točni, da ne bi prišlo do nesporazumov. Vsi študenti morajo biti pravi študentje, zato bo njihova avtentičnost preverjena. Aplikacija bo upoštevala uredbo o varstvu osebnih podatkov in ostale zakone o varstvu podatkov.

## 2. Uporabniške vloge

**Neregistriran študent**
Neregistriran študent lahko na aplikaciji pregleduje splošne podatke, ki niso vezani na osebo ali vpisno številko.

**Registriran študent**
Registriran študent ima dostop do podatkov, ki so povezani z njegovim študijem in lahko spreminja vsebino na svojem profilu.

**Referent**
Skrbi za pristnost študentskih podatkov in rešuje probleme z registracijami in urejanjem profilov.

**Skrbnik**
Skrbi za aktualnost podatkov o predmetih in njihovih nosilcih. 


## 3. Slovar pojmov

**Opis predmeta**
vsebuje potek predmeta, katera snov bo obdelana na predavanjih in kako bodo potekale vaje. Poleg tega so v njem definirani tudi vsi pogoji za opravljanje predmeta in seznam literature, ki je relevantna za predmet. 

**Pogoji za opravljanje predmeta**
je seznam obveznosti, ki jih mora študent opraviti, da lahko pristopi k izpitu.

**Nosilec predmeta**
je zaposleni na fakulteti, ki ima habilitacijo ali učiteljski naziv.

**Kreditne točke**
so merilo časovne obremenjenosti povprečnega študenta, zagotavljajo pa tudi, da študentje med študijem niso preveč ali premalo obremenjeni. Ure študija se odražajo s kreditnimi točkami, vsaka nosi 25-30 ur študija.

**Izpitni rok**
je vnaprej določen datum, ko se opravlja izpit.

**Izpit**
je končni preiskus znanja, ki ga izvede nosilec predmeta, udeležijo pa se ga študentje, ki tako prejmejo oceno.

**Predmet**
je učno področje, ki je predpisano in vsebinsko določeno s predmetnikom in učnim načrtom.

**ŠIS**
je študentski informacijski sistem, ki omogoča prijavo na izpitne roke, pregled nad ocenami in ostale formalne aktivnosti povezane s študijem. 

**Letnik**
je stopnja oziroma organizacijska enota študija.    

**Baza podatkov**
je zbirka podatkov, ki so medsebojno povezani. Podatke lahko dodajamo, urejamo, posodabljamo in brišemo.

**Strežnik**
je računalnik, ki uporabnikom omogoča različne infomacijske storitve in izvršuje njihove zahteve.

**TODO**
je naloga ali aktivnost, ki jo mora študent opraviti.

## 4. Diagram primerov uporabe

![UML diagram uporabe](../img/UML_diagram.png)


## 5. Funkcionalne zahteve

V tem razdelku podrobno opišite posamezne funkcionalnosti, ki jih vaša aplikacija ponuja svojim študentom. Za vsako funkcionalnost navedite naslednje podatke:

### Pregled predmetov
**Povzetek funkcionalnosti**  
Študent (registriran, kot tudi neregistriran) lahko pregleda predmete, ki jih fakulteta ponuja. Vidi lahko opis, nosilca predmeta, kreditne točke in pogoje za opravljanje predmeta.

#### Osnovni tok
1. Študent na osnovni strani izbere funkcionalnost *Pregled predmetov*.
2. Sistem prikaže seznam predmetov razdeljene po letnikih.
3. Študent izbere enega od predmetov.
4. Odpre se nova stran s podatki o predmetu.

#### Alternativni tok
1. Študent se nahaja na urniku
2. Ob kliku na predmet na urniku se mu odpre opis in podatki predmeta

#### Pogoji
Za pregled predmetov ni potrebnih nobenih predpogojev.

#### Posledice
Rezultat osnovnega toka je predana informacija iz sistema študentu. Drugih posledic ni.

#### Posebnosti
Funkcionalnost nima posebnosti.

#### Prioritete identificiranih funkcionalnosti
Must have

#### Sprejemni testi
| Primer uporabe | Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
|:---:|:---:|:---:|:---:|:---:|
| Pregled predmetov | Izpis seznama vseh predmetov | Seznam je prazen | Izbira strani za prikaz predmetov | Prikaz vseh predmetov |
| Ogled podatkov o predmeta | Prikaz opisa | Aplikacija ne vsebuje vsebine | Klik na gumb za zahtevo prikaza podatkov o predmetu | Podatki o predmetu pravilno prikazani na strani |


### Pregled izpitnih rokov
**Povzetek funkcionalnosti**
Študent (registriran kot tudi neregistriran) lahko preveri prihajajoče izpitne roke določenega predmeta.

#### Osnovni tok
1. Študent na osnovni strani izbere funkcionalnost *Pregled predmetov*.
2. Sistem prikaže seznam predmetov razdeljene po letnikih.
3. Študent izbere enega od predmetov.
4. Odpre se nova stran s podatki o predmetu.
5. Študent na pregledu predmeta izbere funkcionalnost *Pregled izpitnih rokov*
6. Prikaže se mu seznam izpitnih rokov.

#### Izjemni tok
**Izjemni tok 1**
1. Predmet nima razpisanih rokov, zato seznama rokov ni mogoče odpreti
2. Ob izbiri se študentu pokaže opozorilo.

**Izjemni tok 2**
1. Sistem ŠIS je neodziven.
2. Študent na pregledu predmeta izbere pregled izpitnih rokov.
3. Prikaže se opozorilo, da prikaz izpitnih rokov trenutno ni mogoč.

#### Pogoji
1. Predmet mora imeti razpisane izpitne roke.
2. Zunanji sistem ŠIS mora delovati normalno.

#### Posledice
Rezultat osnovnega toka je predana informacija iz sistema k študentu. Drugih posledic ni.

#### Posebnosti
Funkcionalnost nima posebnosti. 

#### Prioritete identificiranih funkcionalnosti 
Should have

#### Sprejemni testi
| Primer uporabe | Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
|:---:|:---:|:---:|:---:|:---:|
| Ogled podatkov o predmetu | Preusmerjanje na stran z izpitnimi roki | Na aplikaciji so prikazani podatki o predmetu | Klik na gumb za prikaz izpitnih rokov | Prikaz seznama izpitnih rokov |
| Prikaz izpitnih rokov | Pridobivanje seznama izpitnih rokov določenega predmeta | Stran je prazna | Zahteva za prikaz izpitnih rokov | Seznam izpitnih rokov, enega določenega predmeta |
| Prikaz izpitnih rokov | Prikaz izpitnih rokov na strani | Stran je prazna | Zahteva za prikaz izpitnih rokov | Pravilen prikaz izpitnih rokov na strani |
| Študent zahteva prikaz izpitnih rokov za predmet, ki nima izpitnih rokov, se mu prikaže opozorilo, da predmet nima razpisanih rokov | Prikaz opozorila za predmet brez izpitnih rokov | Stran za prikaz podatkov o predmetu | Zahteva za prikaz izpitnih rokov | Prikaz opozorila, da predmet nima razpisanih rokov |
| Študent zahteva prikaz izpitnih rokov medtem, ko se ŠIS ne odziva | Prikaz opozorila za napako na strežniku | Stran za prikaz podatkov o predmetu | Zahteva za prikaz izpitnih rokov | Prikaz opozorila, da se je zgodila napaka aplikaciji |


### Izvoz izpitnih rokov
**Povzetek funkcionalnosti**
Registrirani študent lahko v svoj koledar izvozi izpitne roke določenega predmeta.

#### Osnovni tok
1. Študent na pregledu predmeta izbere pregled izpitnih rokov.
2. Prikaže se mu seznam izpitnih rokov ob vsakem pa možnost izvoza.
3. Ob kliku na izvoz se izpitni rok izvozi v študentov izpitni koledar na osnovni strani in skupni koledar.
4. Sistem obvesti študenta o dodanih rokih.

#### Alternativni tok
1. Študent izbere funkcionalnost pregled urnika.
2. Na urniku izbere predmet za katerega bi izvozil izpitne roke.
3. Odpre se mu okno s podatki o predmetu in izpitnimi roki za predmet ter izbiro izvozi roke.
4. Ob kliku na izvoz se roki izvozijo v koledar.
5. Sistem obvesti študenta o dodanih rokih.

#### Izjemni tok 
1. Zaradi napake v ŠISu so prikazani že pretekli izpitni roki.
2. Študent na pregledu predmeta izbere pregled izpitnih rokov.
3. Prikaže se mu seznam izpitnih rokov ob vsakem pa možnost izvoza.
4. Ob kliku na izvoz se izpitni rok izvozi v študentov izpitni koledar, vendar pride do napake, saj je datum izvoza kasnejši kot datum izpita.
5. Sistem javi napako v obliki sporočila študentu in izpitnega roka ne izvozi.

#### Pogoji
Za izvoz ne potrebuje študent nobenih dodatnih pogojev.

#### Posledice
Rezultat osnovnega toka je prikaz izpitnih rokov predmeta in shranjen dogodek (izpit) v študentov koledar.

#### Posebnosti
Funkcionalnost nima posebnosti. 

#### Prioriteta funkcionalnosti
Could have.

#### Sprejemni testi
| Primer uporabe | Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
|:---:|:---:|:---:|:---:|:---:|
| Izvoz izpitnega roka v koledar | Dodajanje dogodka v koledar | Dogodek v koledarju ne obstaja | Zahteva za dodajanje dogodka | Dogodek se ustvari v koledarju |
| Izvoz izpitnega roka v koledar | Prikaz opozorila, ko želiš izvoziti potekli izpitni rok | Dogodek ne obstaja v koledarju | Zahteva za dodajanje dogodka | Dogodek se ne izvozi in prikaže se opozorilo |


### Pregled svojih izpitnih rokov
**Povzetek funkcionalnosti**
Registriran študent lahko preveri svoje izvožene izpitne roke.

#### Osnovni tok
1. Na osnovni strani študent izbere pregled vseh izpitnih rokov.
2. Prikaže se mu seznam izvoženih izpitnih rokov, kjer lahko rok tudi odstrani.

#### Alternativni tok
1. Študent izbere možnost prikaza koledarja
2. Na koledarju ima po dnevih zabeležene vse dogodke, tudi izpitne roke.

#### Izjemni tok 
1. Študent nima izvoženih nobenih rokov.
2. Ob pregledu izvoženih rokov se mu izpiše opozorilo, da izvoženih rokov ni.

#### Pogoji
Pogoj, da se študentu pokažejo izvoženi roki je, da je pred tem izvozil kakšen rok.

#### Posledice
Rezultat osnovnega toka je prikaz informacije študentu.

#### Posebnosti
Funkcionalnost nima posebnosti.

#### Prioriteta funkcionalnosti
Could have

#### Sprejemni testi
| Primer uporabe | Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
|:---:|:---:|:---:|:---:|:---:|
| Pregled izpitnih rokov | Prikaz izpitnih rokov | Prazna stran brez seznama izpitnih rokov | Klik na gumb na osnovni strani za prikaz strani | Stran napolnjena s seznamom izpitnih rokov |
| Pregled izpitnih rokov na koledarju | Prikaz izpitnih rokov v koledarju dogodkov | Stran s koledarjem | Zahteva za prikaz koledarja | Med dogodki v koledarju so tudi izpitni roki |
| Pregled izpitnih rokov | Prikaz opozorila, da študent nima izvoženih izpitnih rokov | Prazna stran | Zahteva za prikaz seznama izpitnih rokov | Študentu se izpiše opozorilo, da še ni izvozil izpitnih rokov |


### Pregled ocen
**Povzetek funkcionalnosti**
Registriran študent lahko preveri svoje ocene po letnikih.

#### Osnovni tok
1. Prijavljen študent na osnovni strani izbere funkcionalnost *Pregled ocen*.
2. Prikaže se mu stran s tabelo ocen po predmetih, pri čemer ima študent možnost izbrati letnik.

#### Izjemni tokovi
**Izjemni tok 1**
1. Študent ni vpisan v letnik, vendar vseeno opravlja nekatere predmete.
2. Ob kliku na ogled ocen se mu pri izbiri letnika pokažejo le letniki v katere je trenutno ali je bil v preteklosti vpisan.

**Izjemni tok 2**
1. Sistem ŠIS je neodziven.
2. Študenta ob kliku na ogled ocen preusmeri na z izpisanim opozorilom, da ocen trenutno ni mogoče prikazati.

#### Pogoji
1. Študent mora biti redno vpisan v letnik za katerega želi preveriti ocene.
2. Ocene morajo biti vpisane v zunanjem sistemu ŠIS
3. Zunanji sistem ŠIS mora delovati normalno.

#### Posledica
Rezultat osnovnega toka je predana informacija iz sistema k študentu. Drugih posledic ni.

#### Posebnosti
Funkcionalnost nima posebnosti.

#### Prioriteta identificirane funkcionalnosti
Could have

#### Sprejemni testi
| Primer uporabe | Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
|:---:|:---:|:---:|:---:|:---:|
| Prikaz ocen v aplikaciji | Pridobivanje ocen | Prazen seznam ocen | Zahteva za prikaz ocen | Seznam z vsemi ocenami prijavljenega študenta, tudi za tiste predmete, ki jih v trenutno vpisanem letniku študent nima |
| Prikaz ocen v aplikaciji | Prikaz seznama ocen razdeljenega po letnikih | Prazen seznam | Zahteva za prikaz ocen | Seznam ocen je razdeljen po letnikih. Predmeti, ki se opravljajo vnaprej, so prikazani v letniku, v katerem se ta predmet izvaja |
| Prikaz napake, če je ŠIS neodziven | Zaznavanje neodvisnosti ŠISa | Prazen seznam ocen | Zahteva za prikaz ocen | Prikaz strani s sporočilom o napaki na strežniku |

### Ogled urnika
**Povzetek funkcionalnosti**
Registriran študent si lahko ogleda svoj urnik.

#### Osnovni tok
1. Prijavljen študent na osnovni strani izbere pregled urnika.
2. Prikaže se mu stran z njegovim urnikom, ki je vezan na njegovo vpisno številko in trenutni semester.

#### Izjemni tokovi
**Izjemni tok 1**
1. Zunanji sistem urnik ne deluje.
2. Ob kliku na pregled urnika se študentu prikaže opozorilo, da urnika trenutno ni mogoče prikazati.

**izjemni tok 2**
1. Študent ni vpisan v letnik (pavzira).
2. Ob kliku na pregled urnika se študentu prikaže prazen urnik.

#### Pogoji
1. Študent mora biti redno vpisan v letnik.
2. Zunanji sistem urnik mora delovati normalno.
3. V zunanjem sistemu mora biti urnik za trenutni semester na voljo.

#### Posledice
Rezultat osnovnega toka je prikaz informacije iz sistema študentu. Drugih posledic ni.

#### Posebnosti
Funkcionalnost nima posebnosti.

#### Prioriteta funkcionalnosti
Must have.

#### Sprejemni testi
| Primer uporabe | Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
|:---:|:---:|:---:|:---:|:---:|
| Prikaz urnika prijavljenemu študentu | Prikaz urnika na strani | Prazna stran | Zahteva za prikaz urnika | Prikaz urnika za prijavljenega študenta |
| Prikaz urnika prijavljenemu študentu | Zaznavanje nedelovanja zunanjega sistema | Prazna stran | Zahteva za prikaz urnika | Prikaz strani s sporočilom o napaki na strežniku |
| Prikaz urnika prijavljenemu študentu | Prepoznavanje pavziranja | Prazna stran | Zahteva za prikaz urnika | Prikaže se prazen urnik |


### Preveri kreditne točke po letnikih 
**Povzetek funkcionalnosti**
Registrirani študent lahko ob pregledu ocen preveri tudi trenutne kreditne točke, ki jih je zbral po letnikih.

#### Osnovni tok
1. Študent na osnovni strani izbere pregled ocen.
2. Prikaže se mu stran s tabelo ocen po predmetih.
3. Pod ocenami lahko študent izbere pregled kreditnih točk, nakar se mu izpiše trenutno število pridobljenih kreditnih točk, ki jih je zbral v trenutno izbranem letniku.

#### Izjemni tok 
1. Študent še ni opravljal nobenih izpitov v izbranem letniku.
2. Študentu se ob izbiri pregled kreditnih točk izpiše opozorilo, da kreditnih točk za trenutni letnik še ni pridobil.

#### Pogoji
Pogoj za prikaz kreditnih točk je opravljen vsaj en izpit v izbranem letniku.

#### Posledice
Rezultat osnovnega toka je prikaz informacije o številu pridobljenih kreditnih točk v trenutnem letniku študentu.

#### Posebnosti
Funkcionalnost nima posebnosti.

#### Prioriteta funkcionalnosti
Could have.

#### Sprejemni testi
| Primer uporabe | Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
|:---:|:---:|:---:|:---:|:---:|
| Prikaz kreditnih točk | Prikaz kreditnih točk | Prikazana stran s seznamom ocen | Klik na gumb za prikaz kreditnih točk | Prikaže se seznam kreditnih točk |
| Prikaz kreditnih točk | Prikaz obvestila, če študent še ni pridobil ocene v tistem letniku | Prikazana stran s seznamom ocen | Klik na gumb za prikaz kreditnih točk | Namesto seznama kreditnih točk se izpiše obvestilo, da študent še ni pridobil ocen v tem letniku |


### Pregled koledarja
**Povzetek funkcionalnosti**
Registriran študent lahko pregleda svoj študentski koledar.

#### Osnovni tok
1. Na osnovni strani študent izbere koledar.
2. Odpre se mu stran s koledarjem, kjer ima zabeležene vse izpitne roke in opomnike.

#### Pogoji
Funkcionalnost nima posebnih pogojev.

#### Posledice
Rezultat osnovnega toka je prikaz informacije (koledarja) študentu.

#### Posebnosti
Funkcionalnost nima posebnosti.

#### Prioriteta funkcionalnosti
Could have.

#### Sprejemni testi
| Primer uporabe | Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
|:---:|:---:|:---:|:---:|:---:|
| Prikaz koledarja registriranega študenta | Prikaz koledarja | Prazna stran | Zahteva za prikaz koledarja | Pravilno prikazana stran s koledarjam na katerem so dogodki registriranega študenta |


### Dodajanje opomnikov
**Povzetek funkcionalnosti**
Registriran študent si lahko v koledar dodaja opomnike.

#### Osnovni tok
1. Na osnovni strani študent izbere koledar.
2. Odpre se mu stran s koledarjem, kjer ima zabeležene vse izpitne roke in opomnike.
3. Izbere možnost dodaj opomnik.
4. Odpre se mu okno z možnostjo vpisovanja podatkov o opomniku, nakar le-tega študent lahko tudi shrani.
4. Sistem študentu javi uspešno dodajanje opomnika.

#### Alternativni tok
1. Na osnovni strani študent izbere koledar.
2. Odpre se mu stran s koledarjem, kjer ima zabeležene vse izpitne roke in opomnike.
3. Ob kliku na dan za katerega še nima zabeleženega opomnika se mu odpre okno za dodajanje opomnika za celoten dan.
4. Sistem študentu javi uspešno dodajanje opomnika.

#### Izjemni tok 
1. Na osnovni strani študent izbere koledar.
2. Odpre se mu stran s koledarjem, kjer ima zabeležene vse izpitne roke in opomnike.
3. Izbere možnost dodaj opomnik.
4. Študent ne izpolni vseh zahtevanih mest, zato se mu ob potrditvi dodajanja opomnika prikaže opozorilo, da mora izpolniti zahtevana polja.

#### Pogoji
Funkcionalnost nima posebnih pogojev.

#### Posledice
Rezultat osnovnega toka je dodan opomnik v koledarju.

#### Posebnosti
Funkcionalnost nima posebnosti.

#### Prioriteta funkcionalnosti
Should have.

#### Sprejemni testi
| Primer uporabe | Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
|:---:|:---:|:---:|:---:|:---:|
| Dodajanje opomnika | Dodajanje opomnika v koledar | Opomnik ne obstaja | Oddana zahteva za dodajanje opomnika | Opomnik se shrani v koledar |
| Dodajanje opomnika | Preprečevanje dodajanja nepopolnih opomnikov | Opomnik ne obstaja | Oddana zahteva za dodajanje opomnika z manjkajočimi polji | Opomnik se ne shrani v koledar in študentu se izpiše opozorilo ob nepopolnih poljih |


### Pregled in urejanje opomnika
**Povzetek funkcionalnosti**
Registriran študent lahko pregleda opomnik.

#### Osnovni tok
1. Na osnovni strani študent izbere koledar.
2. Odpre se mu stran s koledarjem, kjer ima zabeležene vse izpitne roke in opomnike.
3. Ob kliku na opomnik v koledarju se študentu odpre okno s podatki o opomniku, ki jih lahko študent tudi ureja.
4. Ob kliku na gumb shrani se nato opomnik posodobi in se izpiše obvestilo o uspešnosti.

#### Pogoji
Opomnik mora biti dodan in shranjen v koledarju.

#### Posledice
Rezultat osnovnega toka je prikaz informacij opomnika študentu.

#### Posebnosti
Funkcionalnost nima nobenih posebnosti.

#### Prioriteta funkcionalnosti
Could have.

#### Sprejemni testi
| Primer uporabe | Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
|:---:|:---:|:---:|:---:|:---:|
| Urejanje opomnikov | Pridobivanje in prikaz opomnika | Prazna stran | Zahteva za pridobivanje podatkov opomnika | Strežnik vrne podatke o opomniku in napolni vnosna polja v aplikaciji |
| Urejanje opomnikov | Posodabljanje opomnika | Pravilno izpolnjena stran | Klik na gumb shrani | Opomnik se posodobi in prikaže se obvestilo, da je bil opomnik uspešno posodobljen |


### Pregled študentskih dogodkov
**Povzetek funkcionalnosti**
Registrirani študent lahko preveri kateri študentski dogodki se v trenutnem mesecu odvijajo v bližini.

#### Osnovni tok
1. Študent na osnovni strani izbere možnost *študentski dogodki*.
2. Študentu se odpre stran s seznamom in podatki dogodkov.

#### Izjemni tok 
1. Zunanji sistem Facebook Events se ne odziva.
2. Študent na osnovni strani izbere možnost študentski dogodki.
3. Na zaslonu se mu prikaže opozorilo, da storitev trenutno ni na voljo. 

#### Pogoji
Funkcionalnost nima posebnih pogojev.

#### Posledice
Rezultat osnovnega toka je prikaz informacij študentu.

#### Posebnosti
Funkcionalnost je vezana na zunanji sistem Facebook Events.

#### Prioriteta funkcionalnosti
Would have.

#### Sprejemni testi
| Primer uporabe | Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
|:---:|:---:|:---:|:---:|:---:|
| Prikaz seznama dogodkov | Povezovanje s Facebookom | Povezava ni vzpostavljena | Zahteva za povezavo | Povezava uspešno vzpostavljena |
| Prikaz seznama dogodkov | Pridobivanje in prikaz dogodkov | Prazna stran | Zahteva za prikaz strani | Na strani se prikaže seznam dogodkov s Facebooka |
| Prikaz seznama dogodkov | Preverjanje povezave s Facebookom | Prikazana prva stran medtem, ko se zunanja storitev Facebook ne odziva | Zahteva za prikaz strani dogodkov | Na strani se prikaže obvestilo, da storitev trenutno ni na voljo |


### Izvoz dogodkov v koledar
**Povzetek funkcionalnosti**
Registriran študent lahko izvozi dogodek v svoj koledar.

#### Osnovni tok
1. Študent na osnovni strani izbere možnost *študentski dogodki*.
2. Študentu se odpre stran s seznamom in podatki dogodkov.
3. Ob dogodku je izbira shrani dogodek v koledar.
4. Študent ob izbiri shrani dogodek izvozi izbrani dogodek v svoj koledar kot opomnik.
5. Sistem študentu javi uspešno dodajanje dogodka v koledar.

#### Izjemni tok 
1. Zunanji sistem Facebook Events se ne odziva.
2. Študent na osnovni strani izbere možnost študentski dogodki.
3. Na zaslonu se mu prikaže opozorilo, da storitev trenutno ni na voljo. 

#### Pogoji
Funkcionalnost nima posebnih pogojev.

#### Posledice
Rezultat osnovnega toka je nov opomnik v študentovem koledarju.

#### Posebnosti
Funkcionalnost je vezana na funkcionalnost Pregled študentskih dogodkov.

#### Prioriteta funkcionalnosti
Would have.

#### Sprejemni testi
| Primer uporabe | Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
|:---:|:---:|:---:|:---:|:---:|
| Izvoz dogodkov v koledar | Dodajanje dogodkov iz Facebook Events | Dogodek ne obstaja v koledarju | Zahteva za dodajanje dogodkov | Dogodek se uspešno uvozi v koledar |
| Izvoz dogodkov v koledar | Preverjanje povezave s Facebookom | Prikazana prva stran medtem, ko se zunanja storitev Facebook ne odziva | Zahteva za prikaz strani dogodkov | Na strani se prikaže obvestilo, da storitev trenutno ni na voljo |


### Potrdi registracijo študenta
**Povzetek funkcionalnosti**
Referent lahko potrdi registracijski zahtevek študenta.

#### Osnovni tok
1. Referentu se na osnovni strani prikaže seznam vseh registriranih študentov in študentov v čakanju na potrditev ob vsakem pa tudi izbira uredi podatke.
2. Ob izbiri uredi podatke ob študentu, ki čaka na potrditev se referentu odpre stran s podatki o študentu, ki jih je študent oddal ob registraciji ter izbira potrdi registracijo.
3. Ob izbiri potrdi registracijo se referentu pokaže opozorilo ali želi potrditi svojo akcijo.
4. Ob ponovni potrditvi je potrditev registracije končana, sistem javi uspešnost.

#### Alternativni tok
1. Referentu se na osnovni strani prikaže seznam vseh registriranih študentov in študentov v čakanju na potrditev ob vsakem pa tudi izbira uredi podatke.
2. Referent ima možnost potrditve vseh študentov na čakanju hkrati.
3. Ob izbiri potrdi registracijo za vse se referentu pokaže opozorilo ali želi potrditi svojo akcijo.
4. Ob ponovni potrditvi je potrditev registracije končana, sistem javi uspešnost.

#### Pogoji
Uporaba funkcionalnosti je možna le, če obstaja zahteva za registracijo.

#### Posledice
Rezultat funkcionalnosti je dokončana registracija študenta.

#### Posebnosti
Funkcionalnosti nima posebnosti.

#### Prioriteta funkcionalnosti
Could have.

#### Sprejemni testi
| Primer uporabe | Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
|:---:|:---:|:---:|:---:|:---:|
| Potrditev registracije | Prikaz seznama študentov | Prazna stran | Zahteva za prikaz seznama | Prikaže se seznam študentov |
| Potrditev registracije | Prikaz podatkov izbranega študenta | Stran s seznamom študentov | Zahteva za prikaz podatkov določenega študenta | Prikaže se stran s podatki o izbranem študentu |
| Potrditev registracije | Potrditev študenta | Stran s podatki o študentu | Zahteva za potrditev podatkov študenta | Študent je uspešno registriran na platformo |
| Potrditev registracije | Potrditev vseh študentov naenkrat | Stran s seznamom študentov | Referent izbere vse študente in zahteva njihovo potrditev | Izbrani študenti so uspešno registrirani |


### Vzdrževanje podatkov študenta
**Povzetek funkcionalnosti**
Referent lahko ročno ureja in spreminja podatke študentov.

#### Osnovni tok
1. Referentu se na osnovni strani prikaže seznam vseh registriranih študentov in študentov v čakanju na potrditev ob vsakem pa tudi izbira uredi podatke.
2. Ob izbiri uredi podatke se referentu odpre stran s podatki o študentu.
3. Podatke lahko spreminja in shrani.
4. Sistem ob javi uspešnost pri shranjevanju podatkov.

#### Izjemni tok 
1. Referentu se na osnovni strani prikaže seznam vseh registriranih študentov in študentov v čakanju na potrditev ob vsakem pa tudi izbira uredi podatke.
2. Ob izbiri uredi podatke se referentu odpre stran s podatki o študentu.
3. Referent izbriše podatek in ga ne nadomesti z novim.
4. Prikaže se mu opozorilo, da podatek manjka in ga more vpisati preden spremembe shrani.

#### Pogoji
Pogoj za prikaz študentov je, da je vsaj en študent oddal prošnjo za registracijo.

#### Posledice
Rezultat osnovnega toka je prikaz informacije referentu in sprememba podatkov študenta.

#### Posebnosti
Funkcionalnost nima posebnosti.

#### Prioriteta funkcionalnosti
Should have.

#### Sprejemni testi
| Primer uporabe | Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
|:---:|:---:|:---:|:---:|:---:|
| Urejanje podatkov študentov | Prikaz seznama študentov | Prazna stran | Zahteva za prikaz študentov | Na strani se prikaže seznam študentov |
| Urejanje podatkov študentov | Prikaz podatkov o študentu | Stran s seznamom študentov | Klik na gumb za prikaz podatkov | Prikaže se stran s podatki o študentu |
| Urejanje podatkov študentov | Posodobitev podatkov študenta | Stran z vnosnimi polji s podatki študenta | Klik na gumb za shranjevanje podatkov | Podatki se posodobijo in prikaže se potrdilo o uspešnosti |


### Vzdrževanja podatkov predmeta
**Povzetek funkcionalnosti**
Skrbnik sistema lahko ureja in spreminja podatke predmeta.

#### Osnovni tok
1. Skrbniku sistema se ob prijavi pokaže seznam vseh predmetov z njihovimi šifranti in izbiro uredi predmet.
2. Ob izbiri uredi predmet se skrbniku odpre stran z vsemi podatki o predmetu.
3. Podatke lahko spremeni in shrani.
4. Sistem javi uspešnost akcije.

#### Izjemni tok 
1. Skrbniku sistema se ob prijavi pokaže seznam vseh predmetov z njihovimi šifranti in izbiro uredi predmet.
2. Ob izbiri uredi predmet se skrbniku odpre stran z vsemi podatki o predmetu.
3. Ob spremembi podatkov enega izmed podatkov skrbnik ne zapiše.
4. Spremembo želi shraniti, vendar mu sistem prikaže opozorilo, da manjkajo podatki.

#### Pogoji
Pogoj za urejanje šifrantov in podatkov predmeta je, da je predmet pravilno shranjen v bazi podatkov.

#### Posledice
Rezultat osnovnega toka je prikaz informacije skrbniku ter sprememba podatkov o predmetu.

#### Posebnosti
Funkcionalnost nima posebnosti.

#### Prioriteta funkcionalnosti
Must have.

#### Sprejemni testi
| Primer uporabe | Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
|:---:|:---:|:---:|:---:|:---:|
| Urejanje podatkov o predmetu | Prikaz seznama predmetov | Prazna stran | Zahteva za prikaz seznama predmetov | Prikaže se seznam predmetov |
| Urejanje podatkov o predmetu | Prikaz podatkov o predmetu | Stran s seznamom predmetov | Klik na gumb za prikaz podatkov | Prikaže se stran s podatki o predmetu |
| Urejanje podatkov o predmetu | Posodobitev podatkov predmeta | Stran z vnosnimi polji s podatki predmeta | Klik na gumb za shranjevanje podatkov | Podatki se posodobijo in prikaže se potrdilo o uspešnosti |



## 6. Nefunkcionalne zahteve

### Zahteve izdelka

#### Zahteve uporabnosti
Aplikacija mora na zahtevo odgovoriti v največ 150ms, ko jo uporablja manj kot 200 uporabnikov.  
Omogočati mora nemoteno souporabo najmanj 1200 uporabnikom z odzivnim časom nič večjim od 2000ms.

#### Varnostne zahteve
Samo skrbnik lahko spreminja podatke o predmetih.  
Samo referent lahko dodaja in spreminja podatke o študentih.  
Registriran študent lahko spreminja le svoje osebne podatke.  
Neregistriran študent ne sme videti osebnih podatkov ostalih študentov.  

#### Zahteve zanesljivosti
Aplikacija mora zagotavljati 99.5% dostopnost.

### Organizacijske zahteve

#### Zahteve okolja
Aplikacija naj bo gostovan na operacijskem sistemu Red Hat Enterprise Linux 7.6 in gostovan na spletnem strežniku NodeJS 10.15 LTS.

#### Operativne zahteve
Veljavnost podatkov registriranih študentov mora preveriti referent.

### Zunanje zahteve

#### Zahteve predpisov
Aplikacija mora upoštevati [Splošno uredbo o varstvu osebnih podatkov](https://eur-lex.europa.eu/legal-content/SL/TXT/?uri=uriserv:OJ.L_.2016.119.01.0001.01.SLV&toc=OJ:L:2016:119:FULL).

#### Zakonodajne zahteve
Aplikacija mora skladati z zakonom o varstvu osebnih podatkov. Opis zakona se nahaja na [ZVOP-1A](https://www.ip-rs.si/zakonodaja/zakon-o-varstvu-osebnih-podatkov/).  
Aplikacija mora upoštevati zakonodajo glede piškotkov. Specifično zakon [ZEKom-1](http://www.pisrs.si/Pis.web/pregledPredpisa?id=ZAKO6405#)


## 7. Prototipi vmesnikov

### Vmesnik za povezavo z zunanjim sistemom urnika
Zunanji sistem urnik uporabljamo v funkcionalnsoti *Ogled urnika*, zato bi lahko, denimo, uporabili funkcijo **urnik(studentID)**, ki sprejme vpisno številko študenta in vrne datoteko [ICAL](https://tools.ietf.org/html/rfc5545), ki vsebuje urnik za študenta določenega z vpisno številko.

### Vmesnik za povezavo z zunanjim sistemom ŠIS
Zunanji sistem ŠIS uporabljamo v dveh funkcionalnostih, *Pregled izpitnih rokov* in *Pregled ocen*, zato bi imel lahko ŠIS, denimo, ponujal funkciji:
**izpitniRoki(predmetID)**, ki bi sprejela identifikacijsko številko predmeta oz. šifrant in vrnila datoteko (denimo JSON) z roki za predmet določen s predmetID, ter **ocene(studentID)**, ki sprejme identifikacijsko številko študenta in vrne datoteko z njegovimi ocenami po letnikih in predmeti (denimo JSON).

### Vmesnik za povezavo z zunanjim sistemom Facebook Events
Zunanji sistem Facebook Events uporabljamo v funkcionalnosti *Pregled študentskih dogodkov*, zato bi lahko uporabili kar njihov [Graph API](https://developers.facebook.com/docs/graph-api/reference/event/) in tako dobili dogodke. Za uporabo APIja bi morali imeti Facebook račun, ki bi vse dogodke redno spreljal in se na njih prijavljal.


## 8. Zaslonske maske

#### Prijava uporabnika:
![Log In uporabnika](../img/logIn.png)

#### Glavna stran in pregled koledarja (registriran študent):
Nanaša se na naslednje funkcionalnosti:  

* Pregled svojih izpitnih rokov  
* Pregled koledarja  
* Dodajanje opomnikov  
* Pregled opomnika  
* Pregled študentskih dogodkov  

![Glavna stran](../img/index.png)

#### Dodajanje TODO-ja:
Nanaša se na naslednje funkcionalnosti:  

* Dodajanje opomnikov  

![Dodajanje TODO-ja](../img/addTodo.png)

#### Urejanje TODO-ja:
Nanaša se na naslednje funkcionalnosti:  

* Pregled opomnika  

![Urejanje TODO-ja](../img/editTodo.png)

#### Pregled predmetov:
Nanaša se na naslednje funkcionalnosti:  

* Pregled predmetov  

![Pregled predmetov](../img/classes.png)

#### Pregled podrobnosti predmeta in izvoz in pregled izpitnih rokov (registriran študent):
Nanaša se na naslednje funkcionalnosti:  

* Pregled predmetov  
* Pregled izpitnih rokov  
* Izvoz izpitnih rokov  

![Pregled podrobnosti predmeta](../img/classInfo.png)

#### Pregled ocen registriranega študenta:
Nanaša se na naslednje funkcionalnosti:  

* Pregled ocen  
* Preveri kreditne točke po letnikih  

![Pregled ocen predmeta](../img/grades.png)

#### Pregled urnika registriranega študenta:
Nanaša se na naslednje funkcionalnosti:  

* Ogled urnika  

![Pregled urnika](../img/timetable.png)

#### Pregled študentskih dogodkov (registriran študent):
Nanaša se na naslednje funkcionalnosti:  

* Pregled študentskih dogodkov  
* Izvoz dogodkov v koledar  

![Pregled študentskih dogodkov](../img/events.png)

#### Pregled novih in že obstoječih študentov v spletni aplikaciji (referent):
Nanaša se na naslednje funkcionalnosti:  

* Potrdi registracijo študenta  
* Vzdrževanje podatkov študenta  

![Pregled novih in že obstoječih študentov](../img/students.png)

#### Urejanje že obstoječega študenta v spletni aplikaciji (referent):
Nanaša se na naslednje funkcionalnosti:  

* Vzdrževanje podatkov študenta  

![Urejanje že obstoječega študenta v spletni aplikaciji](../img/editStudent.png)

#### Pregled vseh predmetov v spletni aplikaciji (skrbnik):
Nanaša se na naslednje funkcionalnosti:  

* Vzdrževanja podatkov predmeta  

![Pregled urnika](../img/classesAdmin.png)

#### Vzdrževanja podatkov predmeta v spletni aplikaciji (skrbnik):
Nanaša se na naslednje funkcionalnosti:  

* Vzdrževanja podatkov predmeta  

![Pregled urnika](../img/adminClassesEdit.png)