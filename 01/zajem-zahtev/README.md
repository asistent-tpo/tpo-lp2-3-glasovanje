# Dokument zahtev

| | |
|:----|:----|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Jakob Gaberc Artenjak, Jože Jerše, Januš Likozar, Luka Guštin |
| **Kraj in datum** | Ljubljana, april 2018 |



## Povzetek projekta


StraightAs je aplikacija, ki pomaga študentom pri organizaciji svojih študijskih obveznosti. Znotraj aplikacije lahko uporabniki ustvarijo skupine, vezane na določeno temo ali predmet, kateri se nato dodajo časovni roki oziroma obveznosti pri tej temi ali predmetu. Te obveznosti se nato izrišejo uporabniku na urnik, ki je znotraj aplikacije. Na zahtevo uporabnika mu aplikacija lahko izpiše tudi t.i. "TO-DO list" za določen časovni okvir (dnevni ali tedenski), kjer dobi uporabnik dnevne oziroma tedenske naloge, kar mu pomaga pri produktivnosti. 



## 1. Uvod

Sistem je primarno namenjen uporabnikom, ki obiskujejo izobraževalne institucije, saj so tekom izobraževanja podvrženi velikemu številu različnih časovnih obveznostmi,
kot so naprimer obvezne vaje, preverjanje znanja, roki oddaje domačih nalog in podobno. To težavo našim uporabnikom olajša naša spletna aplikacija, s pomočjo
katere uporabniku ponudi lep in organiziran pregled nad njegovimi obveznostmi, na ta način ima uporabnik zbrane vse časovne informacije svojih obveznosti na enem mestu,
kar je še posebaj privlačno za študente, ki se izobražujejo na večih študijskih programih hkrati.

Aplikacija ponuja možnost ustvarjanja t.i. skupin, kateri lahko njegovi administratorji, dodajajo obveznosti, ki tej skupini pripadajo. Skupine so v osnovi
namenjene organizaciji uporabnikov, ki pripadajo eni enoti znotraj izobraževalne ustanove, ponavadi so to na nivoju predmeta. Ko se uporabnik pridruži skupini lahko 
pridobi obveznosti, ki pripadajo tej skupine.

Aplikacija obravnava 4 različne tipe uporabnikov, najpogostejši uporabnik bo t.i. potrjeni uporabnik, ki ima znotraj našega sistema tudi največ možnosti. Poleg ustvarjanja
skupin in dodajanja dogodkov v skupine, omogoča aplikacija med drugim tudi iskanje novih skupin, kamor se lahko uporabnik dodatno pridruži, odjavo iz skupine, s pomočjo
zunanje storitve si lahko uporabnik tudi prikaže lokacijo obveznosti na zemljevidu,  spreminja in odstranjuje dogodke in še več.
Uporabniku se po pridružitvi v skupino prikažejo časovne obveznosti na njegovem urniku, s pomočjo katerega so pomaga pri organizaciji študijskih obveznosti. Urnik je pregleden,
intuitiven in najpomembneje, preprost. Da dodatno pomagamo uporabniku pri produktivnosti, mu omogočimo tudi kreacijo t.i. “TO-DO list”-ov, kjer mu sistem vrne, na podlagi časovnega 
okvirja (dan ali teden), seznam obveznosti, katere ima v tem času za narest.

Aplikacija je primarno namenjena za uporabo v državi Republika Slovenija, zato jo uporabniški vmesnik v celoti na voljo v slovenskem in angleškem jeziku.
Aplikacija bo na voljo kot spletna stran, do katere bo možen dostov tako z namiznim računalnikom, kot tudi z mobilno napravo.
Zagotovljeno bo delovanje aplikacije vsaj 23 ur na dan, vsak dan med urami od 4:00 do 3:00.

Vsi osebni podatki uporabnikov, katere bomo hranili na naši strani bojo šifrirani, saj bomo realizirali našo aplikacijo v skladu z zakonom o varstvu osebnih podatkov RS (ZVOP-1).
Prav tako bo naša aplikacija skladna z Evropsko Direktivo o avtorskih pravicah na digitalnem enotnem trgu.

Zelo pomembna nam je uporabniška izkušnja, kar je tudi en glavnih vodnikov razvoja naše aplikacije, zato bomo razvili prijazen in lep uporabniški vmesnik.
Del uporabniške izkušnje je tudi odzivnost, zato se bo sistem vedno odzval uporabniku v maksimalno 10 sekundah potem, ki uporabnik izvede kakšno akcijo znotraj sistema.
Odzivnost zagotovimo z načrtovanjem aplikacije tako, da bo bila skalabilna in se zato prilagodila večjemu številu uporabnikov.



## 2. Uporabniške vloge

### Aplikacija bo imela štiri uporabniške vloge:
* Ne registriran uporabnik - uporabnik aplikacije, ki ni registriran
    * Možna mu je le registracija v sistem.
* Registriran uporabnik - uporabnik, ki ima v aplikaciji ustvarjen uporabniški račun:
    * Vidni so mu javni dogodki ter opisi skupin.
    * Skupinam se lahko pridruži, vendar so mu nekatere akcije onemogočene. Onemogočeno ima naprimer akcijo ustvarjanja nove skupine.
* Potrjen uporabnik - registriran uporabnik, ki je na nek način zadovoljivo dokazal, da je oseba, za katero se izdaja.
    * Ima vse lastnosti kot registriran uporabnik, ter možnost dodatnih akcij.
* Skupinski administrator - registriran ali potrjen uporabnik, kateri lahko spreminja določene skupine
    * Lahko ureja dogodke v skupini kakor njegov ustvarjalec.
    * Nima dostopov do nastavitev skupine.
* Google maps - Zunanji sistem, ki pripomore k lažjemu iskanju dogodkov.





## 3. Slovar pojmov

| Beseda                    | Pomen                                                                                                                 |
| :------------------------ | :-------------------------------------------------------------------------------------------------------------------- |
| Skupina                   | Ne predstavlja skupino ljudi, vendar skupino dogodkov katerim se lahko uporabniki pridružijo.                    	    |
| Dogodek                   | Lahko predstavlja predmet v šoli, izreden dogodek v šoli (ekskurzije, druženja, domača naloga itd.) ali zabavo.       |
| Skalabilnost				| Lastnost programske opreme. Označuje možnost dodajanja dodatnih računskih ali prostorskih sposobnosti aplikaciji.	    |
| SCRUM						| SCRUM je eden od agilnih načinov razvoja programske opreme.														    |
| Odzivni čas akcije		| Čas, v katerem dobi uporabnik nek odziv od sistema, na podlagi akcije, izvedene z strani uporabnika.				    |



## 4. Diagram primerov uporabe

![Diagram primera uporabe](../img/Diagram-primera-uporabe.png)


## 5. Funkcionalne zahteve


### Registracija

#### Povzetek funkcionalnosti

Ne registriran uporabnik ustvari račun, da lahko uporablja aplikacijo kot registriran uporabnik.
Podatke, ki jih mora uporabnit podai pri registraciji so:

* Uporabniško ime*
* Geslo
* Spletna pošta*

Pri čemer morajo biti podatki označeni z * unikatni za vse uporabnike.
Uporabniško ime je lahko sestavljeno iz črk, števil in '_', geslo pa mora biti daljše od 8 znakov.

#### Osnovni tok

1. Uporabnik odpre spletno stran aplikacije.
2. Uporabnik izbere "Registracija".
3. Uporabiku se prikažejo vnosna polja za registracijo, kamor vnese podatke.
4. Spletna aplikacija preveri, ali so podatki veljavni, na uporabnikov e-mail naslov pošlje potrditveni link.
5. Uporabnik klikne na podani link.
6. Aplikacija uporabnika doda v podatkovno bazo
7. Uporabniku se prikaže njegova domača stran.



#### Izjemni tok 

* Že registrirani uporabnik se želi registrirati, aplikacija uporabniku sporoči, da je že registriran.
* Uporabnik vnese uporabniško ime, ki vsebuje prepovedane znake, aplikacija ne dovoli registracije in uporabniku sporoči razlog.
* Uporabnik vnese geslo, krajše od 8 znakov, aplikacija ne dovoli registracije in uporabniku sporoči razlog.

#### Pogoji

Uporabnik ni registriran in ima veljaven e-mail naslov.

#### Posledice

Uporabnik je zapisan v podatkovni bazi in lahko uporablja spletno aplikacijo kot registriran uporabnik.

#### Prioritete identificiranih funkcionalnosti

* Must have

##### Sprejemni testi

Uporabnik na strani izbere aplikacija, vnese svoje podatke, navigira na svoj email in klikne na podani link.

------------------------------------------------------------------------


### Prijava

#### Povzetek funkcionalnosti

Registriran uporabnik se prijavi v aplikacijo z njegovim uporabniškim imenom in geslom.

#### Osnovni tok

1. Uporabnik odpre spletno stran aplikacije.
3. Uporabiku se prikažejo vnosna polja za prijavo, kamor vnese podatke.
4. Spletna aplikacija preveri, ali so podatki veljavni.
5. Spletna aplikacija po uspešnem preverjanju prikaže uporabnikovo domačo stran.

#### Izjemni tok 

* Prijava zaradi izpada sistema ni mogoča, uporabniku se prikaže ustrezno sporočilo.

#### Pogoji

Uporabnik je registriran na spletni strani, ima veljavno uporabniško ime in geslo.

#### Posledice

Uporabnik je vpisan v svoj profil, lahko dostopa do vseh svojih funkcionalnosti

#### Prioritete identificiranih funkcionalnosti

* Must have

##### Sprejemni testi

Uporabnik na strani izbere prijava, vnese svoje podatke in se prijavi.


------------------------------------------------------------------------

### Potrditev uporabnika

#### Povzetek funkcionalnosti

Registriran uporabnik se v sistemu potrdi in postane potrjen uporabnik tako, da vnese svojo telefonsko številko in nato vnese šifro poslano na telefonsko številko v spletno aplikacijo.

#### Osnovni tok

1. Uporabnik odpre spletno stran aplikacije.
2. Uporabnik izbere "Potrdi se".
3. Uporabiku se prikažejo vnosno polje za telefonsko številko, kamor jo vnese.
5. Spletna aplikacija pošlje telefonski številki šifro in prikaže uporabniku vnosno polje zanjo.
6. Uporabnik pridobi šifro preko SMS in jo vnese v spletno aplikacijo.
7. Aplikacija preveri, ali je šifra pravilna in po uspešnem vnosu prikaže uporabniku njegovo domačo stran.

#### Izjemni tok 

* Uporabnik vnese napačno šifro, aplikacija uporabniku sporoči, da šifra ni pravilna in prosi za ponoven vnos.

#### Pogoji

Uporabnik je prijavljen v spletno aplikacijo. Uporabnik ni potrjen. Uporabnik nima shranjene telefonske številke.

#### Posledice

Uporabnik je potrjen in njegova telefonska številka je shranjena v bazi.

#### Prioritete identificiranih funkcionalnosti

* Must have

##### Sprejemni testi

Uporabnik na strani izbere potrdi se, vnese svojo telefonsko številko in pridobljeno šifro.


------------------------------------------------------------------------

### Pregled urnika

#### Povzetek funkcionalnosti

Registriran ali potrjen uporabnik pregleda svoj urnik, ki se prikaže v obliki tabele vseh dogodkov za tekoči teden, lahko ga filtrira po zvrsteh in/ali skupinah. Ima tudi možnost pregleda urnika za katerikoli drugi teden.

#### Osnovni tok

1. Uporabnik izbere "Moj urnik".
2. Uporabniku se prikaže njegov urnik.

#### Alternativni tok 1

1. Uporabnik izbere "Moj urnik".
2. Uporabniku se prikaže njegov urnik.
3. Uporabnik pritisne puščico na desno.
4. Uporabniku se prikaže urnik za naslednji teden.

#### Izjemni tok 

* Pregled urnika zaradi izpada sistema ni mogoč, prikaže se ustrezno sporočilo.

#### Pogoji

Uporabnik je prijavljen v spletno aplikacijo. 

#### Posledice

Uporabniku je prikazan urnik tekočega tedna.

#### Prioritete identificiranih funkcionalnosti

* Must have

##### Sprejemni testi

Uporabnik na strani klikne moj urnik.

------------------------------------------------------------------------

### Pregled skupin

#### Povzetek funkcionalnosti

Registriran ali potrjen uporabnik gre skozi seznam vseh skupin, pri čemer lahko seznam tudi filtrira po imenu, ali je uporabnik pridružen tej skupini ali ne, zvrsti in če je zvrst šola, po kraticah fakultet.
Vsaka skupina je prikazana z zvrstjo, imenom, opisom in po možnosti kratice fakultet.

#### Osnovni tok

1. Uporabnik izbere "Poišči skupino".
2. Uporabniku se prikaže seznam skupin.

#### Alternativni tok 1

1. Uporabnik izbere "Poišči skupino".
2. Uporabniku se prikaže seznam skupin.
3. Uporabnik v iskalno polje napiše 'FRI'
4. Uporabniku se prikažejo skupine, ki pripadajo FRI-ju

#### Izjemni tok 

* Uporabnik filtrira skupine po skupinah katerim je pridružen, ko še ni v nobeni skupini, uporabniku se prikaže obvestilo, kjer se mu sporoči, da skupin ni mogoče prikazati, ker ni v nobeni.
* Uporabnik filtrira skupine z ključem, ki ne najde nobene skupine, prikaže se ustrezno obvestilo.

#### Pogoji

Uporabnik je prijavljen v spletno aplikacijo.

#### Posledice

Uporabniku je prikazan seznam vseh skupin.

#### Prioritete identificiranih funkcionalnosti

* Must have

##### Sprejemni testi

Uporabnik na svoji strani izbere poišči skupino.

------------------------------------------------------------------------


### Pridružitev skupini

#### Povzetek funkcionalnosti

Registriran ali potrjen uporabnik se pridruži skupini.

#### Osnovni tok

1. Uporabnik izbere skupino.
2. Uporabnik izbere "Pridruži se tej skupini".

#### Alternativni tok

1. Uporabnik izbere skupino z geslom.
2. Uporabnik izbere "Pridruži se tej skupini".
3. Spletna aplikacija ga vpraša za geslo, ki ga uporabnik vnese.
4. Spletna aplikacija potrdi geslo in doda uporabnika v skupino.

#### Izjemni tok 

* Uporabnik vnese napačno geslo za pridružitev skupini z geslom, prikaže se ustrezno sporočilo in prošnja za ponovni vnos.
* Zaradi izpada sistema pridružitev ni mogoča. prikaže se ustrezno sporočilo.

#### Pogoji

Uporabnik je prijavljen v spletno aplikacijo in je preko pregleda skupin našel željeno skupino.

#### Posledice

Uporabnik je pridružen skupini in ima na svojem urniku njene dogodke.

#### Prioritete identificiranih funkcionalnosti

* Must have

##### Sprejemni testi

Uporabnik na strani skupine izbere možnost pridruži se skupini

------------------------------------------------------------------------

### Odjava iz skupine

#### Povzetek funkcionalnosti

Registriran ali potrjen uporabnik se iz skupine odjavi.

#### Osnovni tok

1. Uporabnik izbere skupino v katero je trenutno prijavlen.
2. Uporabnik izbere "Odjavi se iz te skupine".

#### Izjemni tok 

* Zaradi izpada sistema odjava ni mogoča, uporabniku se prikaže ustrezno sporočilo.

#### Pogoji

Uporabnik je prijavljen v spletno aplikacijo in je preko pregleda skupin najšel željeno skupino.

#### Posledice

Uporabnik je odstranjen iz skupine, v njegovem urniku ni več prisotnih dogodkov zapuščene skupine.

#### Prioritete identificiranih funkcionalnosti

* Must have

##### Sprejemni testi

Uporabnik na strani skupine izbere odjavi se iz te skupine

------------------------------------------------------------------------

### Prikaz dogodkov blizu uporabnika

#### Povzetek funkcionalnosti

Registriranemu ali potrjenemu uporabniku se prikažejo dogodki blizu njega.

#### Osnovni tok

1. Uporabnik izbere "Dogodki blizu mene"
2. Spletna aplikacija mu prikaže seznam dogodkov, ki se dogajajo blizu lokacije uporabnika.

#### Izjemni tok 

* Sistem ni uspel pridobiti lokacije uporabnika, prikaže se ustretno sporočilo.

#### Pogoji

Uporabnik je prijavljen v spletno aplikacijo.

#### Posledice

Uporabniku se prikaže seznam dogodkov v njegovi bljižini.

#### Posebnosti

Uporabnikova naprava mora biti sposobna pridobiti njegovo lokacijo.

#### Prioritete identificiranih funkcionalnosti

* Could have

##### Sprejemni testi

Uporabnik na svoji strani izbere dogodki blizu mene

------------------------------------------------------------------------

### Pregled dogodkov

#### Povzetek funkcionalnosti

Registriran ali potrjen uporabnik pregleda svoj urnik v obliki seznama dogodkov, ki ga lahko filtrira po zvrsteh in/ali skupinah.

#### Osnovni tok

1. Uporabnik izbere "Moj urnik".
2. Uporabniku se prikaže njegov urnik.
3. Uporabnik izbere "Prikaži v obliki seznama".
4. Uporabniku se prikaže seznam vseh dogodkov.

#### Izjemni tok 

* Zaradi izpada sistema ta ne more pridobiti uporabnikovega urnika, prikaže se ustrezno sporočilo.

#### Pogoji

Uporabnik je prijavljen v spletno aplikacijo.

#### Posledice

Uporabniku se prikaže seznam vseh dogodkov

#### Prioritete identificiranih funkcionalnosti

* Must have

##### Sprejemni testi

Uporabnik izbere moj urnik in si izbere prikaz v obliki seznama.

------------------------------------------------------------------------

### Vnos dogodka

#### Povzetek funkcionalnosti

Registriran ali potrjen uporabnik v svoj urnik doda dogodek, ki ga vidi le on sam.
Dogodek ima zvrst, ki je lahko predavanje, vaje ali izjemni dogodek (npr. zabava, piknik, ekskurzija).
Ob vnosu dogodka mora dodati:

* Za predavanja ali vaje - dan v tednu, čas začetka, čas trajanja, začetni datum, končni datum, ime in po možnosti tudi lokacijo in/ali opis.
* Za izjemne dogodke - datum, čas začetka, čas trajanja, ime in po možnosti tudi lokacijo in/ali opis ter skupino.

#### Osnovni tok

1. Uporabnik izbere "Moj urnik".
2. Uporabnik izbere "Dodaj nov dogodek".
3. Spletna aplikacija mu prikaže polja za vnos dogodka, kamor vnese podatke.
4. Spletna aplikacija shrani podatke v podatkovno bazo aplikacije.
5. Spletna aplikacije se ob uspešnem vnosu podatkov v podatkovno vrne na stran urnika.

#### Izjemni tok 

* Vnešeni podatki so napačni, uporabnika aplikacija prosi, da napačne podatke popravi.
* Nujna polja so prazna, uporabnika aplikacija prosi, naj jih izpolni.
* Zaradi izpada sistema dogodka ni mogoče vnesti, prikaže se ustrezno sporočilo.

#### Pogoji

Uporabnik je prijavljen v spletno aplikacijo.

#### Posledice

Uporabnik ima v njegovem urniku dodan dogodek.

#### Prioritete identificiranih funkcionalnosti

* Must have

##### Sprejemni testi

Uporabnik na svojem urniku izbere dodaj dogodek, vanj vpiše potrebne informacije.

------------------------------------------------------------------------

### Sprememba dogodkov

#### Povzetek funkcionalnosti

Registriran ali potrjen uporabnik spremini svoj dogodek.

#### Osnovni tok

1. Uporabnik izbere dogodek.
2. Uporabnik izbere "Spremeni dogodek".
3. Če je dogodek iz skupine spletna aplikacija opozori uporabnika, da bo sprememba prekinila povezavo dogodka in skupine.
4. Spletna aplikacija prikaže nastavitvne dogodka.
5. Ob potrjenih spremembah sistem odstrani dogodek pridobljen od skupine in doda nov dogodek z podanimi podatki v podatkovno bazo.
6. Spletna aplikacija se ob uspešnem vnosu podatkov v podatkovno bazo na stran urnika.

#### Izjemni tok 

* Nastavitve dogodka so ne skladne ali prazne, aplikacija uporabnika prosi, naj podatke vnese ali popravi.
* Zaradi izpada sistema spremembe ni mogoče shraniti, prikaže se ustrezno sporočilo.

#### Pogoji

Uporabnik je prijavljen v spletno aplikacijo in ima na njej vsaj en dogodek. 

#### Posledice

Na uporabnikovem urniku je prikazan spremenjen dogodek.

#### Prioritete identificiranih funkcionalnosti

* Must have

##### Sprejemni testi

Uporabnik na svojem urniku izbere dogodek, vanj vpiše spremenjene informacije informacije.


------------------------------------------------------------------------

### Odstranjevanje dogodka

#### Povzetek funkcionalnosti

Registriran ali potrjen uporabnik odstrani dogodek iz njegovega urnika.

#### Osnovni tok

1. Uporabnik izbere dogodek.
2. Uporabnik izbere "Odstrani dogodek".
3. Aplikacija vpraša uporabnika za potrditev.
4. Uporabnik potrdi odstranitev dogodka.
5. Aplikacija odstrani dogodek iz podatkovne baze, če je dogodek iz skupine le izbriše povezavo med uporabnikom in tem dogodkom.
6. Spletna aplikacija se vrne na stran urnika.

#### Izjemni tok 

* Zaradi izpada sistema dogodka ni mogoče odstraniti, aplikacija prikaže ustrezno sporočilo.

#### Pogoji

Uporabnik je prijavljen v spletno aplikacijo in ima na njej vsaj en dogodek. 

#### Posledice

Uporabnikov urnik ne vsebuje odstranjenega dogodka.

#### Prioritete identificiranih funkcionalnosti

* Could have

##### Sprejemni testi

Uporabnik na svojem urniku izbere dogodek, na strani dogodka izbere odstrani dogodek.


------------------------------------------------------------------------


### Ustvarjanje skupine

#### Povzetek funkcionalnosti


Potrjen uporabnik vnese novo skupino v sistem, pri temu mora izbrat kakšne zvrsti je (šola ali zabava), ime skupine, opcijsko pa lahko doda dodaten opis in/ali geslo.
Če je skupina zvrsti šola lahko doda kratice fakultete, za katere bi bila tale skupina zanimiva.

#### Osnovni tok

1. Uporabnik izbere opcijo "Dodaj novo skupino".
2. Spletna aplikacija mu prikaže polja za vnos nove skupine.
2. Uporabnik vnese podatke o skupini in jih potrdi.
3. Aplikacija shrani podatke o skupini v podatkovno bazo.
4. Spletna aplikacija potrdi uporabnik pravilen vnos skupine v podatkovno bazo in se vrne na prvotno stran aplikacije za potrjene uporabnike

#### Izjemni tok 

* Polja za vnos skupine so prazna ob potrditvi, aplikacija uporabnika prosi, naj jih izpolni.
* Zaradi izpada aplikacije ustvarjanje skupine ni mogoče, prikaže se ustrezno sporočilo.

#### Pogoji

Potrjen uporabnik je prijavljen na spletni strani aplikacije.

#### Posledice

Registrirani uporabniki in potrjeni uporabniki lahko najdejo vnešeno skupino in se ji pridružijo.
Uporabnik je avtomatsko dodan kot član skupine.
Uporabnik je avtomatsko dodan kot administrator skupine.
Skupina je shranjena v podatkovni bazi.

#### Prioritete identificiranih funkcionalnosti

* Must have

##### Sprejemni testi

Uporabnik na strani izbere dodaj novo skupino, izpolni svoje informacije in potrdi.


------------------------------------------------------------------------

### Sprememba skupine

#### Povzetek funkcionalnosti

Potrjen uporabnik spremeni določene nastavitve skupine, ime, opis po možnosti spreminja kratice fakultet za katere bi naj bil dogodek zanimiv in sprememba gesla.

#### Osnovni tok

1. Uporabnik v svoji skupini izbere "Nastavitve".
2. Spletna aplikacija prikaže uporabniku vse nastavitve skupine.
3. Uporabnik spremeni nastavitve in potrdi njegove spremembe.
4. Spletna aplikacija shrani spremembe v podatkovno bazo.
5. Spletna aplikacija se vrne na stran skupine.


#### Alternativni tok 1

1. Uporabnik v svoji skupini izbere "Nastavitve".
2. Spletna aplikacija prikaže uporabniku vse nastavitve skupine.
3. Uporabnik skupini nastavi geslo.
4. Spletna aplikacija shrani spremembe v podatkovno bazo.
5. Spletna aplikacija iz skupine odstrani vse dosedanje člane.
6. Spletna aplikacija se vrne na stran skupine.

#### Izjemni tok 

* Zaradi izpada aplikacije sprememba ni mogoča, prikaže se ustrezno sporočilo.
* Vnešene kratice fakultete ne obstajajo, uporabnika aplikacija prosi, naj jih popravi.

#### Pogoji

Uporabnik je prijavlen v spletno aplikacijo in ima skupino ter je na njeni strani.
Uporabnik je potrjeni uporabnik.
Uporabnik je administrator skupine.

#### Posledice

Nove nastavitve skupine se shranijo v podatkovno bazo. 

#### Prioritete identificiranih funkcionalnosti

* Must have

##### Sprejemni testi

Uporabnik na svoji skupini izbere nastavitve, popravi željene informacije in potrdi.


------------------------------------------------------------------------

### Odstranjevanje skupine

#### Povzetek funkcionalnosti

Potrjen uporabnik odstrani skupino. 

#### Osnovni tok

1. Uporabnik v svoji skupini izbere "Nastavitve".
2. Spletna aplikacija prikaže uporabniku vse nastavitve skupine.
3. Uporabnik izbere "Odstrani skupino".
4. Spletna aplikacija vpraša za geslo uporabnika, da potrdi odstranjevanje skupine.
5. Uporabnik vnese svoje geslo in potrdi odstranjevanje skupine.
6. Spletna aplikacija odstrani vse dogodke povezane z skupino, vsi člani izgubijo dogodke iz skupine, skupina se odstrani in vsi podatki se tudi odstranijo iz podatkovne baze.
7. Spletna aplikacija se vrne na prvotno stran.

#### Izjemni tok 

* Vnešeno geslo uporabnika je napačno, aplikacija prosi za ponoven vnos.
* Zaradi izpada aplikacije odstranjevanje ni mogoče, prikaže se ustrezno sporočilo.

#### Pogoji

Uporabnik je prijavlen v spletno aplikacijo in ima skupino ter je na njeni strani.
Uporabnik je potrjeni uporabnik.
Uporabnik je administrator skupine.

#### Posledice

Vsi uporabnik v skupini zgubijo dogodke narejene v skupini.
Dogodki vezani na skupino in skupina sama se izbriše iz podatkovne baze. 

#### Prioritete identificiranih funkcionalnosti

* Must have

##### Sprejemni testi

Uporabnik na svoji skupini izbere odstrani skupino, vpiše svoje geslo in potrdi.

------------------------------------------------------------------------

### Pregled dogodkov v skupini

#### Povzetek funkcionalnosti

Registriran in potrjen uporabnik, ki je del skupine pregleda vse dogodke ustvarjene v skupini.

#### Osnovni tok

1. Uporabik navigira na stran skupine.
2. Uporabnik izbere 'seznam dogodkov skupine'.

#### Izjemni tok 

* Dogodkov ni mogoče prikazati, ker skupina nima dogodkov, prikaže se ustrezno sporočilo.
* Zaradi izpada aplikacije pregled ni mogoč, prikaže se ustrezno sporočilo.

#### Pogoji

Uporabnik je prijavlen v spletno aplikacijo in del skupine.

#### Posledice

uporabniku je prikazan seznam dogodkov skupine

#### Prioritete identificiranih funkcionalnosti

* Must have

##### Sprejemni testi

Uporabnik na skupini izbere seznam dogodkov skupine.


-------------------------------------------------------------------------



### Vnos dogodka v skupino

#### Povzetek funkcionalnosti

Potrjen uporabnik doda njegovi skupini dogodek.
Dogodek ima zvrst, katera je lahko predavanje, vaje ali izjemni dogodek (npr. zabava, piknik, ekskurzija).
Ob vnosu dogodka mora dodati:

* Za predavanja ali vaje - frekvenca (predstavlja na koliko tednov se naj tale zadeva pojavi), dan v tednu, čas začetka, čas trajanja, začetni datum, končni datum, ime in po možnosti tudi lokacijo in/ali opis.
* Za izjemne dogodke - datum, čas začetka, čas trajanja, ime in po možnosti tudi lokacijo in/ali opis.

#### Osnovni tok

1. Uporabnik je na strani njegove skupine.
2. Uporabnik izbere "Dodaj nov dogodek".
3. Uporabniku se prikažejo vnosna polja, kamor vpiše podatke o dogodku.
4. Uporabnik potrdi vnos in spletna aplikacija shrani podatke v podatkovno bazo.
5. Spletna aplikacija potrdi vnos v podatkovno bazo in vrne uporabnika na stran skupine, kateri je ravno kar dodal nov dogodek.

#### Izjemni tok 

* Uporabnik pusti nujna polja prazna, aplikacija ga prosi, naj jih vnese.
* Uporabnik vnese napačne podatke, aplikacija ga prosi naj jih popravi.
* Zaradi izpada aplikacije vnos ni mogoč, prikaže se ustrezno sporočilo.

#### Pogoji

Potrjen uporabnik je prijavlen na spletni strani aplikacije in ima njegovo skupino ter je na njeni strani.

#### Posledice

Drugi uporabniki v skupini imajo v njihovih urnikih nov dogodek.
Podatki o dogodku so shranjeni v podatkovni bazi.

#### Prioritete identificiranih funkcionalnosti

* Must have

##### Sprejemni testi

Uporabnik na svoji skupini izbere dodajanj nov dogodek, vpiše potrebne informacije in potrdi.


------------------------------------------------------------------------

### Sprememba dogodka v skupini

#### Povzetek funkcionalnosti

Potrjen uporabnik spremeni nastavitve dogodka v njegovi skupini.

#### Osnovni tok

1. Upotabnik izbere dogodek na seznamu.
2. Spletna aplikacija prikaže vse podatke o dogodku.
3. Uporabnik izbere "Spremeni".
4. Spletna aplikacija prikaže vse podatke, a jih sedaj lahko tudi spreminja.
5. Uporabnik potrdi njegove spremembe.
6. Aplikacija posodobi dogodek v podatkovni bazi.
7. Spletna aplikacija se vrne na stran skupine.

#### Izjemni tok 

* Nujni podatki manjkajo ali so napačni, aplikacija uporabnika prosi naj jih vnese ali popravi.
* Zaradi izpada aplikacije sprememba ni mogoča, prikaže se ustrezno sporočilo.

#### Pogoji

Potrjen uporabnik je prijavlen na spletni strani aplikacije in ima njegovo skupino ter je na njeni strani.
Skupina ima vsaj en dogodek in uporabnik je na seznamu dogodkov

#### Posledice

Drugi uporabniki, ki vidijo izbrani dogodek, vidijo posodobljen dogodek v njihovem urniku.
Dogodek je posodobljen v podatkovni bazi.

#### Prioritete identificiranih funkcionalnosti

* Must have

##### Sprejemni testi

Uporabnik v svoji skupini izbere dogodek, na njem izbere spremeni in spremeni željena polja.


------------------------------------------------------------------------

### Odstranjevanje dogodka iz skupine

#### Povzetek funkcionalnosti

Potrjen uporabnik odstrani dogodek iz njegove skupine.

#### Osnovni tok

1. Uporabnik izbere dogodek iz skupine.
2. Spletna aplikacija prikaže vse podatke o dogodku.
3. Uporabnik izbere "Odstrani".
4. Uporabniku se prikaže potrditveno okno.
5. Uporabnik potrdi izbris.
6. Aplikacija odstrani dogodek iz podatkovni baze in skupine.
7. Spletna aplikacija se vrne na stran skupine.


#### Izjemni tok 

* Zaradi izpada aplikacije odstranitev ni mogoča, prikaže se ustrezno sporočilo.

#### Pogoji

Potrjen uporabnik je prijavljen na spletni strani aplikacije in ima njegovo skupino z dogodki ter je na njeni strani.
Skupina ima vsaj en dogodek in uporabnik je na seznamu dogodkov.

#### Posledice

Drugi uporabniki, ki so imeli dogodek ga več nimajo.
Dogodek je odstranjen iz podatkovne baze.

#### Posebnosti

Seznam prikazan uporabniku je razvrščen po datumu, kateri tudi vključuje trenutni dan in po možnosti tudi pretekli, če je ura med 00:00 ~ 04:00.
Seznam ne prikaže dogodke, kateri so v skupinah z geslom.
Uporabnik mora tudi imeti napravo, katera lahko pridobi njegovo lokacijo.

#### Prioritete identificiranih funkcionalnosti

* Should have

##### Sprejemni testi

Uporabnik na dogodku svoje skupine izbere odstrani, potrdi.


------------------------------------------------------------------------

### Dodajanje administratorja skupine


#### Povzetek funkcionalnosti

Obostoječi administrator skupine (potrjeni uporabnik, ki je skupino ustvaril ali že dodani administrator) uporabniku v skupini doda administratorske pravice.
V nadaljevanju je administrator poimenovan uporabnik, kandidat za administratorja pa drugi uporabnik.

#### Osnovni tok

1. Uporabnik izbere profil drugega uporabnika, na seznamu, ki ga želi povišati.
2. Uporabnik izbere, dodaj administratorske pravice

#### Izjemni tok

* Zaradi izpada sistema dodajanje ni mogoče, prikaže se ustrezno sporočilo.

#### Pogoji

Uporabnik je administrator skupine. Drugi uporabnik je član skupine.
Uporabnik je na strani skupine in je na seznamu članov skupine.

#### Posledice

Drugi uporabnik postane administrator skupine. Spremembe se shranijo v podatkovno bazo.


#### Prioritete identificiranih funkcionalnosti

* Would have

##### Sprejemni testi

Uporabnik na seznamu uporabnikov skupine izbere drugega uporabnika in izbere dodaj administratorske pravice.


------------------------------------------------------------------------





## 6. Nefunkcionalne zahteve

* Zahteve izdelka
    * Sistem naj bo na voljo kot spletna stran tako za namizne zaslone, kot tudi za mobilne naprave.
    * Sistem naj deluje vsak dan med urami 4:00~3:00, med aktivnim časom naj zagotavlja 99% časovno dostopnost.
    * Sistem naj ima prijazen in intuitiven uporabniški vmesnik.
    * Shranjeni osebni podatki uporabnikov naj bojo šifrirani.
    * Odzivni čas akcije uporabnika ne sme prekoračiti 10s
	* Sistem mora biti skalabilen v primeru večjega navala uporabnikov.

* Organizaciske zahteve 
    * V razvojnem procesu naj se uporablja SCRUM.
    * Potrjeni uporabniki se verificirajo na podlagi svoje mobilne številke.
    * Uporablja naj se MEAN stack tehnologij.
    * 
* Zunanje zahteve 
    * Sistem mora biti skladen z Zakonom o varstvu osebnih podatkov Republike Slovenije (ZVOP-1).
	* Sistem mora biti skladen z Evropsko Direktivo o avtorskih pravicah na digitalnem enotnem trgu.
    * Sistem mora podpirati uporabniški vmesnik v slovenskem in angleškem jeziku.


## 7. Prototipi vmesnikov

* Registracija

![Registracija](http://shrani.si/f/3N/Nv/33QzG3Ka/lp2-registracija.png)

* Prijava

![Prijava](http://shrani.si/f/2U/lQ/1slcExNt/lp2-prijava.png)

* Potrditev uporabnika

![Potrditev](http://shrani.si/f/T/Gq/fSMpHGL/lp2-potrditev.png)

* Pregled skupin

![Moje Skupine](http://shrani.si/f/1J/G6/2MicFn5g/lp2-mojeskupine.png)


* Iskanje skupin

![Iskanje skupin](http://shrani.si/f/45/ZV/3L8OqoUe/lp2-iskanjeskupin.png)

* Dogodki

![Dogodki](http://shrani.si/f/2m/kN/1nvqOZhV/lp2-dogodki.png)

* Pregled urnika kot tabela

![Urnik Tabela](http://shrani.si/f/2e/JL/2v2laCh0/lp2-urniktabela.png)

* Pregled urnika kot seznam

![Urnik seznam](http://shrani.si/f/2u/hN/1hyjW2k0/lp2-urnikseznam.png)

* Spreminjanje skupine

![Spremeni skupino](http://shrani.si/f/5/Tv/28r8WVfu/lp2-spremeniskupino.png)

* Dodajanje nove skupine

![Dodajanje skupine](http://shrani.si/f/1S/um/4yQmNfux/lp2-dodajskupino.png)

* Ustvarjanje novega dogodka

![Nov Dogodek](http://shrani.si/f/3P/wj/33LUp8wM/lp2-novdogodek.png)

* Spreminjanje dogodka

![Spremeni dogodek](http://shrani.si/f/J/yf/gLYCP0Y/lp2-spremenidogodek.png)

* Pregled skupine

![Skupina](https://i.imgur.com/hWxkkun.png)


## 8. Vmesniki do zunanjih sistemov


Nas sistem komunicira z zunanjim sistemom Google maps z naslednjimi akcijami:

* 1 Prikaz dogodkov blizu uporabnika
    * Funkcija kot vhod prejme geografsko lokacijo uporabnika, vrne pa json objekt zemljevida njegove okolice.
    * Na pridobljenem zemljevidu nato označimo uporabniku, kje so lokacije bližnjih dogodkov, ki so njemu vidni. Dogodki, ki se izrišejo se lahko trenutno že izvajajo ali pa se bojo v prihodnosti.

* 2 Vnos dogodka
    * Funkcija kot vhod prejme geografsko lokacijo uporabnika, vrne pa json objekt zemljevida njegove okolice, kateri se uporabniku tudi izriše.
    * Na prejetemu zemljevidu lahko uporabnik nato izbere lokacije, kjer bo potekal dogodek katerega trenutno ustvarja. Na podlagi zemljevida in uporabnikovega vnosa nam zunanji sistem vrne geolokacijo, katero vnesemo v našo podatkovno bazo poleg ostalih podatkov o dogodku.

* 3 Vnos Dogodka v skupino
    * Funkcija kot vhod prejme geografsko lokacijo uporabnika, vrne pa json objekt zemljevida njegove okolice, kateri se uporabniku tudi izriše.
    * Na prejetemu zemljevidu lahko uporabnik nato izbere lokacije, kjer bo potekal dogodek katerega trenutno ustvarja. Na podlagi zemljevida in uporabnikovega vnosa nam zunanji sistem vrne geolokacijo, katero vnesemo v našo podatkovno bazo poleg ostalih podatkov o dogodku.