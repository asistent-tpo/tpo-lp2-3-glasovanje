# Načrt sistema

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Jakob Gaberc Artenjak, Jože Jerše, Januš Likozar, Luka Guštin |
| **Kraj in datum** | Ljubljana, April 2019 |



## Povzetek


Narčt sistema vsebuje načrt arhitekture sistema, ki smo si izbrali za prikaz logičnega in razvojnega. Za razvojni vpogled smo uporabili 
večplastno ahritekturo, za logčini vpogled pa uporabili ahitekturo model-pogled-krmilnik.  Naslednji del dokumenta vsebuje opis identificiranih 
razredov, skupno 11, pri čemer so trije entitetni, pet kontrolnih in preostali trije vmesniki. Vsak razred smo ustrezno napolnili z atributi ter 
metodami, katere želimo uporabljati pri konkretni implementaciji. Temu sledi načrt obnašanja, kjer smo vsakemu primeru uporabe dodali sekvenčni 
diagram, kateri prikazuje vrstni red klicanja funkcij in podajanja parametrov med razredi in akterji. Sekvenčni diagram je narejen za vsak podan 
osnovni tok, alternativni tok ter izjemni tok.


## 1. Načrt arhitekture


Pri zgradbi aplikacije bomo uporabili način večplastne arhitekture:

![Načrt](../img/razvojni.png)

Logično pa bo arhitektura po vzorcu model-pogled-krmilnik:

![NačrtLogični](../img/logicni.png)

## 2. Načrt strukture

### 2.1 Razredni diagram

![RazredniDiagram](../img/RazredniDiagram.png)

### 2.2 Opis razredov

#### 2.2.1 Uporabnik

Razred predstavlja uporabnika v naši aplikaciji.

#### Atributi

* Ime: 
    * ime: uporabniskoIme
    * tip: string
    * omejitve: uporabniško ime je lahko sestavljeno iz črk, števil in '_'.
* Geslo
    * ime: geslo
    * tip: string
    * omejitve: daljše od 8 znakov.
* Email
    * ime: eMailNaslov
    * tip: string
* Tip uporabnika
    * ime: tipUporabnika
    * tip: string
    * omejitve: omejen na 'potrjeni' in 'registriran'
* Skupine
    * ime: skupine
    * tip: int[]
    * pomen: idji skupin, katerim uporabnik pripada
* Dogodki
    * ime: dogodki
    * tip: int[]
    * pomen: predstavlja idje dogodke tega uporabnika
* Telefonska številka
    * ime: telefonskaStevilka
    * tip: string
* Id
    * ime: idUporabnika
    * tip: int
    


#### Nesamoumevne metode

* Pridobitev podatkov o uporabniku
    * ime: PridobiPodatkeOUporabniku
    * imena in tipa parametrov: int IdUporabnik
    * tip rezultata: Uporabnik

* Vrni skupine
    * ime: vrniSkupine
    * imena in tipa parametrov: void
    * tip rezultata: Skupina[]

#### 2.2.2 Skupina

Razred predstavlja skupino znotraj naše aplikacije, skupini pripadajo uporabniki.
Skupina ima administratorje, ki so tudi uporabniki.

#### Atributi

* Člani
    * ime: claniSkupine
    * tip: int[]
    * pomen: predstavlja idUporabnika članov skupine, ki niso njeni administratorji 
* Administratorji
    * ime: administratorjiSkupine
    * tip: int[]
    * pomen: predstavlja idUporabnika članov skupine, ki so tudi njeni administratorji
* Dogodki
    * ime: dogodkiSkupine
    * tip: int[]
    * pomen: predstavlja idje dogodkov znotraj skupine
* Ime skupine
    * ime: imeSkupine
    * tip: string
* zvrst
    * ime: zvrst
    * tip: string 
    * omejitve: omejen na 'sola' in 'zabava'
* opis
    * ime: opis
    * tip: string
* fakulteta
    * ime: fakulteta
    * tip: string
* geslo
    * ime: geslo
    * tip: string
    * omejitve: daljše od 8 znakov
* ID
    * ime: idSkupine
    * tip: int
    

#### Nesamoumevne metode

* prikaz strani skupine
    * ime: prikazStraniSkupine
    * imena in tipa parametrov: int idSkupine
    * tip rezultata: void
    
* prikaz seznama skupin
    * ime: prikazSeznamaSkupin
    * imena in tipa parametrov: int idSkupine
    * tip rezultata: void

* vrni geslo skupine
    * ime: vrniGesloSkupine
    * imena in tipa parametrov: int idSkupine
    * tip rezultata: string
    
* pridruzitev skupini
    * ime: pridruziSkupini
    * imena in tipa parametrov: int idSkupine
    * tip rezultata: void
    * pomen: uporabniku doda skupino v array skupin

* posodobi skupino
    * ime: PosodobiSkupino
    * imena in tipa parametrov: Skupina skupina1, Skupina skupina2
    * tip rezultata: int
    * pomen: Posodobi skupino 1 z podatki shranjeni v skupini 2. Vrne status vnosa.
    
* dodaj administratorja
    * ime: DodajAdiministratorjaSkupini
    * imena in tipa parametrov: Skupina skupina1, Skupina skupina2
    * tip rezultata: int
    * pomen: Doda novega administratorja k skupini, vrne status vnosa.
    
* vnos dogodka
    * ime: VnosDogodkaVSkupino
    * imena in tipa parametrov: int IdDogodka, int IdSkupina
    * tip rezultata: int
    * pomen: Shrani, da je prej dodan dogodek navezan na skupino. Vrne status vnosa

* odstranitev dogodka
    * ime: odstraniDogodek
    * imena in tipa parametrov: int IdDogodka, int IdSkupina
    * tip rezultata : Dogodek[]
    * pomen: Zbriše izbrani dogodek iz podatkovne baze dogodkov ter pobriše reference na ta dogodek v skupini, ter pri uporabnikih, včlanjenih v skupino. Vrne posodobljen seznam dogodkov po izbrisu.

#### 2.2.3 Dogodek

Razred predstavlja dogodek znotraj aplikacije.
Dogodki pripadajo tako uporabnikom kot skupinam.

#### Atributi

* Ime
    * ime: ime
    * tip: string
* Pripadnost
    * ime: pripadnostDogodka
    * tip: string
    * omejitve: 'skupina' ali 'uporabnik'
    * pomen: Označuje ali je dogodek del skupine ali je dogodek uporabnikov
* Tip
    * ime: tipDogodka
    * tip: string
    * omejitve: omejen na 'predavanja', 'vaje' in 'izjemni dogodek'
    * Označuje kakšen tip dogodka je
* Id
    * ime: idDogodka
    * tip: int
    
* Atributi, ki morajo biti izpolnjeni, če je Tip = 'izjemni dogodek'
    * Datum začetka
        * ime: datumZacetka
        * tip: date
    * Čas Začetka
        * ime: casZacetka
        * tip: string
    * Trajanje
        * ime: trajanje
        * tip: string
    * Lokacija
        * ime: lokacija
        * tip: string
        * pomen: string vsebuje koordinate lokacije
        * omejitve: lahko je ni
    * Opis
        * ime: opis
        * tip: string
        * omejtive: lahko je prazen
    * Če je pripadnost = 'skupina'
        * skupina
            * ime: skupina
            * tip: int
            * pomen: id skupine, kateri dogodek pripada
    * Če je pripadnost = 'uprabnik'
        * uporabnik
            * ime: uporabnik
            * tip: int
            * pomen: id uporabnika, kateremu dogodek pripada
        
* Atributi, ki morajo biti izpolnjeni, če je Tip = 'predavanja' ali Tip = 'vaje'
    * Frekvenca
        * ime: frekvenca
        * tip: int
        * pomen: na koliko tednov naj se stvar pojavi
    * Dan v tednu
        * ime: danVTednu
        * tip: int
        * pomen: na kateri dan v tednu poteka dogodek
    * Datum začetka
        * ime: datumZacetka
        * tip: date
    * Datum zaključka
        * ime: datumZaključka
        * tip: date 
    * Čas Začetka
        * ime: casZacetka
        * tip: string
    * Trajanje
        * ime: trajanje
        * tip: string
    * Lokacija
        * ime: lokacija
        * tip: string
        * pomen: string vsebuje koordinate lokacije
        * omejitve: lahko je ni
    * opis
        * ime: opis
        * tip: string
        * omejtive: lahko je prazen
    * Če je pripadnost = 'skupina'
        * skupina
            * ime: skupina
            * tip: Skupina
            * pomen: Skupina, kateri dogodek pripada
    * Če je pripadnost = 'uporabnik'
        * uporabnik
            * ime: uporabnik
            * tip: Uporabnik
            * pomen: Uporabnik, kateremu dogodek pripada      


#### Nesamoumevne metode

* Vnos dogodka
    * ime: VnosDogodka
    * imena in tipa parametrov: Dogodek dogodek
    * tip rezultata: int
    * pomen:  Vnese dogodek v podatkovno bazo, vrne status vnosa.


* Pridobi dogodek
    * ime: PridobiDogodek
    * imena in tipa parametrov: int IdDogodek
    * tip rezultata: Dogodek
    * pomen: Vrne dogodek, kateremu pripada ID.

* Odstrani dogodek
    * ime: OdstraniDogodek
    * imena in tipa parametrov: int IdDogodek
    * tip rezultata: int
    * pomen: Odstrani dogodek, kateremu pripada ID. Vrne status brisanja
    
* Pridobi dogodke v bljižini
    * ime: VrniDogodkeBlizu
    * imena in tipa parametrov: Uporabnik uporabnik, Skupina[] skupine, string lokacija
    * tip rezultata: Dogodek[]
    * pomen: Vrne dogodke v bljižini lokacije

* Pridobi dogodke skupine
    * ime: PridobiDogodkeSkupine
    * imena in tipa parametrov: int IdSkupine
    * tip rezultata: Dogodek[]
    * pomen: Vrne dogodke skupine s podanim ID-jem
    
* Izbris dogodkob od skupine
    * ime: IzbrisDogodkovOdSkupine
    * ime in tip parametra: int IdSkupine
    * tip rezultata: int
    * pomen: Dogodke, navezane na skupino s IDje, se izbrise, vrne status brisanja
    

#### 2.2.4 Podatki o dogodkih

Predstavlja kontrolni razred za delo z dogodki

#### Atributi


#### Nesamoumevne metode

* Vnos dogodka
    * ime: VnosDogodka
    * imena in tipa parametrov: Dogodek dogodek
    * tip rezultata: int
    * pomen:  Vnese dogodek v podatkovno bazo, vrne status vnosa.


* Pridobi dogodek
    * ime: PridobiDogodek
    * imena in tipa parametrov: int IdDogodek
    * tip rezultata: Dogodek
    * pomen: Vrne dogodek, kateremu pripada ID.

* Odstrani dogodek
    * ime: OdstraniDogodek
    * imena in tipa parametrov: int IdDogodek
    * tip rezultata: int
    * pomen: Odstrani dogodek, kateremu pripada ID. Vrne status brisanja
    

* Pridobi dogodke v bljižini
    * ime: VrniDogodkeBlizu
    * imena in tipa parametrov: Uporabnik uporabnik, Skupina[] skupine, string lokacija
    * tip rezultata: Dogodek[]
    * pomen: Vrne dogodke v bljižini lokacije

* Pridobi dogodke skupine
    * ime: PridobiDogodkeSkupine
    * imena in tipa parametrov: int IdSkupine
    * tip rezultata: Dogodek[]
    * pomen: Vrne dogodke skupine s podanim ID-jem


#### 2.2.5 Podatki o skupini 

Predstavlja kontrolni razred za delo s skupinami

#### Atributi

#### Nesamoumevne metode

* Vnos podatkov o novi skupini
    * ime: VnosPodatkovONoviSkupini
    * imena in tipa parametrov: Skupina skupina
    * tip rezultata: int
    * pomen: Vnese podatke o novi skupini, vrne status vnosa.

* Pridobi podatke
    * ime: PridobiVsePodatkeOSkupini
    * imena in tipa parametrov: int idSkupine
    * tip rezultata: Skupina
    * pomen: Vrne vse podatke o skupini za podan ID.
    
* Posodobi skupino
    * ime: PosodobiSkupino
    * imena in tipa parametrov: Skupina skupina1, Skupina skupina2
    * tip rezultata: int
    * pomen: Posodobi skupino shranjeno v skupina1 z podatki o skupini shranjeno v skupina2 in vrne posodobljeno skupino (torej skupina2). Vrne status posodobitve.
    
* Pregled sprememb 
    * ime: PregledSprememb
    * imena in tipa parametrov: Skupina skupina1, Skupina skupina2
    * tip rezultata: int
    * pomen: Če dodamo geslo, kjer ga prej ni bilo moramo odstrani vse uporabnike. Preverja tudi ali so vsi drui parametri pravilni
    
* Dodaj administratorja
    * ime: DodajAdiministratorjaSkupini
    * imena in tipa parametrov: Skupina skupina, int IdUporabnik
    * tip rezultata: int
    * pomen: Doda novega administratorja k skupini, vrne status vnosa.

* Vnos dogodka v skupino
    * ime: VnosDogodkaVSkupino
    * imena in tipa parametrov: Dogodek dogodek, int IdSkupina
    * tip rezultata: int

* Izbris skupine
    * ime: IzbrisSkupine
    * ime in tip parametra: int IdSkupina
    * tip rezultata: int
    * izbriše skupino in vse njene dogodke, vrne status brisanja
    

#### 2.2.6 Podatki o uporabniku

Predstavlja kontrolni razred za delo z uporabniki

#### Atributi

#### Nesamoumevne metode

* Pridobi dogodke za uporabnika
    * ime: PridobiDogodkeZaUporabnika
    * imena in tipi parametrov: int IdUporabnik
    * tip rezultata: Dogodek[]
    * pomen: Vrne seznam dogodkov za uporabnika z podanim ID-jem.
  
* Dodaj dogodek v seznam
    * ime: DodajDogodekVSeznam
    * imena in tipi parametrov: Dogodek dogodek
    * tip rezultata: void
    * pomen: Doda dogodek v seznam dogodkov.

* Pridobi dogodke v bljižini
    * ime: VrniDogodkeBlizu
    * imena in tipi parametrov: Uporabnik uporabnik, Skupina[] skupine, string lokacija
    * tip rezultata: Dogodek[]
    * pomen: Vrne dogodke v bljižini lokacije

* Dodaj nov dogodek
    * ime: DodajDogodek
    * ime in tip parametra: Dogodek dogodek
    * tip rezultata: int
    * pomen: Dodajanje novega dogodka


#### 2.2.7 Spletna stran

Predstavlja kontrolni razred za delo s spletno stranjo

#### Atributi


#### Nesamoumevne metode

* Prikaži urnik
    * ime: PrikaziUrnik
    * imena in tipa parametrov: Dogodek[] dogodki, Date dan
    * tip rezultata: void
    * pomen: prikaže urnik

* Naslednji teden
    * ime: naslednjiTeden
    * imena in tipa parametrov: void
    * tip rezultata: void
    * pomen: Le možna, če smo v pregedu urnika. Povečja shranjen datum za 1 teden in izvede PrikazUrnik(Dogodek[] dogodki, Date dan) z novim datumom
    
* Prikaži stran skupine
    * ime: prikaziStranSkupine
    * imena in tipa parametrov: Skupina skupina
    * tip rezultata: void
    * pomen: prikaže stran skupine

* Prikaz napake
    * ime: prikazNapake
    * imena in tipa parametrov: string napaka
    * tip rezultata: void
    * pomen: prikaže napako
    
* Sifriraj geslo
    * ime: sifrirajGeslo
    * imena in tipa parametrov: string geslo
    * tip rezultata: string
    
* Pregled urnika
    * ime: PregledUrnika
    * imena in tipa parametrov: void
    * tip rezultata: void
    
* Prikaži nastavitve skupine
    * ime: PrikazNastavitveSkupine
    * imena in tipi parametrov: Skupina skupina
    * tip rezultata: void

* Prikaži obrazec za dodajanje dogodka
    * ime: PrikazDodajanjaNovegaDogodka
    * imena in tipi parametra: void
    * tip rezultata: void
    
* Prikaži obrazec za dodajanje skupine
    * ime: PrikazDodajanjaNoveSkupine
    * imena in tipi parametra: void
    * tip rezultata: void
    
* Validiraj uporabniski vnos
    * ime: ValidirajUporabniskiVnos
    * ime in tip parametra: String vnos, int tip
    * tip rezultata: int

    
#### 2.2.8 Google maps API

Predstavlja kontrolni razred za delo z lokacijo s pomočjo google maps API

#### Atributi


#### Nesamoumevne metode

* Pridobi lokacijo
    * ime: pridobiLokacijo
    * imena in tipa parametrov: void
    * tip rezultata: string

* Izbira Lokacije
    * ime: pridobiLokacijo
    * imena in tipa parametrov: void
    * tip rezultata: string
    * pomen: Pridobimo lokacijo iz google maps API-ja glede izbiro na grafičnem vmesniku.
    
    
#### 2.2.9 Telefonski API

Predstavlja kontrolni razred za delo z lokacijo s pomočjo google maps API

#### Atributi


#### Nesamoumevne metode

* Potrdi uporabnika
    * ime: PotrdiUporabnika
    * imena in tipa parametrov: int TelStevilka
    * tip rezultata: int
    * pomen: Telefonski API poslje potrditveno kodo na podano stevilko, in vrne enako kodo za kasnejše preverjanje

## 3. Načrt obnašanja

* **Registracija**

    * Neregistrirani uporabnik se lahko registrira v naš sistem.
    
    ![Registracija](http://shrani.si/f/2v/AQ/4MmNLtBB/registracija.png)

* **Prijava**

    * Registriran uporabnik se lahko vpiše v svoj račun. Predstavljena sta osnovni tok v primeru uspešnega vpisa ter izjemni tok v primeru nedosegljivosti strežnika.
    
    ![Prijava](http://shrani.si/f/i/U4/4ab7jGlw/copy-of-prijava-2.png)
    
* **Potrditev uporabnika**

    * Registriran uporabnik lahko postane potrjen uporabnik, če potrdi svojo mobilno številko. Ob vpisu telefonske številke, uporabnik prejme SMS z kodo, s pomočjo katere ga sistem potrdi.
    
     ![Potrditev uporabnika](http://shrani.si/f/1S/fK/4iKGbWeK/potrditev.png)
     
     
* **Pregled urnika**

    * Registriran ali potrjen uporabnik pregleda svoj urnik.
    
    ![Pregled urnika](../img/PregledUrnikaOT.png)
    
    * Alternativni tok 1: Pregled urnika za prihodnji teden.
    
    ![Pregled urnika](../img/PregledUrnikaALT1.png)
    
    * Izjemni tok 1: Pregled urnika zaradi izpada sistema ni mogoč, prikaže se ustrezno sporočilo.
    
    ![Pregled urnika](../img/PregledUrnikaIZJ1.png)
    
* **Pregled skupin**

    * Registriranemu uporabniku se izpiše seznam skupin.
    
    ![Pregled skupin](http://shrani.si/f/h/kM/6DP32ZR/pregledskupin-1.png)
    
    
* **Pridružitev skupini**

    * Registriran uporabnik se lahko pridruži skupini, ki se mu prikažejo na seznamu glede na njegovo poizvedbo po le-teh.
    
    ![Pridružitev skupini](http://shrani.si/f/16/76/wd9qBga/copy-of-pridruzitev-skup.png)

    
* **Odjava iz skupine**

    * Registriran uporabnik, ki je vpisan v vsaj eno skupino, se lahko iz izbrane skupine odjavi.
    
    ![Odjava iz skupine](http://shrani.si/f/3S/ZZ/4uvHwprb/copy-of-odjava-1.png)
    
    
* **Prikaz dogodkov blizu uporabnika**

    * Registriran uporabnik lahko zahteva izpis dogodkov, ki se dogajajo v njegovi bližini. Dogodko so ali njegovi, pripadajo skupinam katerih je član ali so pa javni.
    
    ![Prikaz dogodkov blizu uporabnika](http://shrani.si/f/3u/rt/3mQ9VIdT/prikazdogodkov-1.png)
    
    
* **Pregled dogodkov**

    * Registriran ali potrjen uporabnik pregleda svoj urnik, ki se prikaže v obliki seznama dogodkov.
    
    ![Pregled dogodkov](../img/PregledDogodkov.png)
    
    * Izjemni tok 1: Pregled urnika zaradi izpada sistema ni mogoč, prikaže se ustrezno sporočilo.
    
    ![Pregled dogodkov](../img/PregledUrnikaIZJ1.png)
    
* **Vnos dogodka**

    * Registriran uporabnik lahko doda nov dogodek, viden samo njemu.
    
    ![Vnos dogodka](http://shrani.si/f/1P/Or/4LqfVTtZ/vnosdogodka-1.png)
    
* **Sprememba dogodkov**

    * Registriran ali potrjen uporabnik spremeni svoj dogodek
    
    ![Sprememba dogodkov](../img/SpremembaDogodka.png)
    
    * Izjemni tok 1: Nastavitve dogodka so neskladne ali prazne. Uporabniku se izpiše sporočilo o napaki.
    
    ![Sprememba dogodkov](../img/SpremembaDogodkaIZJ1.png)
    
    * Izjemni tok 2: Zaradi izpada sistema spremembe ni mogoče shraniti, prikaže se ustrezno sporočilo.
    
    ![Sprememba dogodkov](../img/SpremembaDogodkaIZJ2.png)
    

* **Odstranjevanje dogodka**

    * Registriran uporabnik lahko odstrani dogodek iz svojega urnika. Ta dogodek tako njemu nebo bil več viden, kljub temu, da dogodek še obstaja.
    
    ![Odstranjevanje dogodka](http://shrani.si/f/3o/BZ/4BGDGwL4/odstranjevanjedogodkov-2.png)
    

* **Ustvarjanje skupine**
 
    * Potrjen uporabnik ustvari novo skupino.
    * 
    ![Ustvarjanje skupine](../img/UstvarjanjeSkupineOT.png)
    
    * Izjemi tok 1: Polja za vnos skupine so prazna ob potrditv.
     
    ![Ustvarjanje skupine](../img/UstvarjanjeSkupineIZJ1.png)

    * Izjemni tok 2: Zaradi izpada aplikacije ustvarjanje skupine ni mogoče, prikaže se ustrezno sporočilo.
    
    ![Ustvarjanje skupine](../img/UstvarjanjeSkupineIZJ2.png)

* **Sprememba skupine**

    * Potrjen uporabnik spremeni določene nastavitve skupine, ime, opis po možnosti spreminja kratice fakultet za katere bi naj bil dogodek zanimiv in sprememba gesla.
    
    ![Sprememba skupine](../img/SpreminjanjeSkupineOT.png)
    
    * Alternativni tok 1: Uporabnik nastavi geslo, zato se vsi uporabniki v skupini odstranijo.
    
    ![Sprememba skupine](../img/SpreminjanjeSkupineALT1.png)
    
    * Izjemi tok 1: Zaradi izpada aplikacije sprememba ni mogoča, prikaže se ustrezno sporočilo.
     
    ![Sprememba skupine](../img/SpreminjanjeSkupineIZJ1.png)
    
    * Izjemni tok 2: Vnešene kratice fakultete ne obstajajo.
    
    ![Sprememba skupine](../img/SpreminjanjeSkupineIZJ2.png)
    

* **Odstranjevanje skupine**
 
    * Odstranjevanje skupine.
    
    ![Odstranjevanje skupine](../img/OdstranjevanjeSkupineOT.png)

    * Izjemni tok 1: Vnešeno geslo uporabnika je napačno.
    
    ![Odstranjevanje skupine](../img/OdstranjevanjeSkupineIZJ1.png)
    
    * Izjemni tok 2: Zaradi izpada aplikacije odstranjevanje ni mogoče, prikaže se ustrezno sporočilo.
    
    ![Odstranjevanje skupine](../img/OdstranjevanjeSkupineIZJ2.png)
    

* **Pregled dogodkov v skupini**

    * Registriran ali potrjen uporabnik, ki je del neke skupine, pregleda vse dogodke te skupine.
    
    ![Pregled dogodkov v skupini](../img/PrikazDogodkovSkupine.png)
    
    * Izjemi tok 1: Skupina nima dogodkov. Uporabniku se izpiše ustrezno sporočilo.
    
    ![Pregled dogodkov v skupini](../img/PrikazDogodkovSkupineIZJ1.png)
    
    * Izjemi tok 2: Zaradi izpada sistema pregled ni mogoč. Izpiše se ustrezno sporočilo.
    
    ![Pregled dogodkov v skupini](../img/PrikazDogodkovSkupineIZJ2.png)
    
    
    
* **Vnos dogodka v skupino**

    * Potrjen uporabnik doda njegovi skupini dogodek.
    
    ![Vnos dogodka v skupino](../img/VnosDogodkaVSkupinoOT.png)
    
    * Izjemni tok 1: Uporabnik pusti nujna polja prazna.
    
    ![Vnos dogodka v skupino](../img/VnosDogodkaVSkupinoIZJ1.png)
    
    * Izjemni tok 2: Uporabnik vnese napačne podatke.
    
    ![Vnos dogodka v skupino](../img/VnosDogodkaVSkupinoIZJ2.png)
    
    * Izjemni tok 3: Zaradi izpada aplikacije vnos ni mogoč, prikaže se ustrezno sporočilo.
    
    ![Vnos dogodka v skupino](../img/VnosDogodkaVSkupinoIZJ3.png)


* **Sprememba dogodka v skupini**

    * Administratos skupine spremeni nastavitve dogodka v skupini.
    
    ![Sprememba dogodka v skupini](../img/SpremembaDogodkaVSkupiniOT.png)
    
    * Izjemni tok 1: Nujni podatki manjkajo ali so napačni.
    
    ![Sprememba dogodka v skupini](../img/SpremembaDogodkaVSkupiniIZJ1.png)
    
    * Izjemni tok 2: Zaradi izpada aplikacije sprememba ni mogoča, prikaže se ustrezno sporočilo.
    
    ![Sprememba dogodka v skupini](../img/SpremembaDogodkaVSkupiniIZJ2.png)

* **Odstranjevanje dogodka iz skupine**

    * Administrator skupine lahko odstrani dogodek, vezan na skupino katere je administrator.
    
    ![Odstranjevanje dogodka iz skupine](http://shrani.si/f/n/j7/gOnu1JL/odstranjevanjedogodkaizs.png)



* **Dodajanje administratorja skupine**

    * Dodajanje administratorja skupini.
    
    ![Dodajanje administratorja skupine](../img/DodajanjeAdministratorjaSkupineOT.png)
    
    * Izjemi tok 1: Zaradi izpada sistema dodajanje ni mogoče, prikaže se ustrezno sporočilo.
    
    ![Dodajanje administratorja skupine](../img/DodajanjeAdministratorjaSkupineIZJ1.png)
    
    


