# Načrt sistema

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Aleksandar Jelić, Matevž Štinjek, Luka Glušič |
| **Kraj in datum** | Ljubljana, 22.4.2019 |



## Povzetek


V dokumentu smo napisali nacrt arhitekture, nacrt strukture z razrednimi diagrami, ter opisi razredov, in nacrt obnasanja.



## 1. Načrt arhitekture

![Razredni diagram ](../img/arhitektura.png)


## 2. Načrt strukture

### 2.1 Razredni diagram

![Razredni diagram ](../img/razredni_diagram.PNG)

### 2.2 Opis razredov

- Razred OBVEZNOST predstavlja upravljanje z obveznostmi.
- Razred AKTIVNOST predstavlja upravljanje z aktivnostmi.
- Razred KOLEDAR predstavlja upravljanje s koledarjem.
- Razreda UPORABNIK predstavljata upravljanje z uporabniki.
 
Vsi atrbuti razredov in metode razredov so vključene v razredni diagram.


## 3. Načrt obnašanja

###Diagrami zaporedja primerov uporabe

####Registracija
Kot gost na strani se želimo registrirati. Do vmesnika za registracijo dostopamo iz naslovne strani.
![Registracija](../img/registracija.PNG)

####Prijava
Kot gost na strani se želimo prijaviti. Do vmesnika za prijavo dostopamo iz naslovne strani.
![Prijava](../img/prijava.PNG)

####Ponastavitev gesla
Kot registriran, a neprijavljen uporabnik, se želimo ponastaviti svoje geslo. Ukaz za ponastavitev najdemo na vmesniku za prijavo, do katerega dostopamo iz naslovne strani.
![ponastavitev_gesla](../img/ponastavitev_gesla.PNG)

####Prikaz obveznosti
Kot prijavljen uporabnik želimo pregledati svoje trenutne obveznosti. Do seznama dostopamo iz dialoga za upravljanje z obveznostmi.
![obveznosti_prikaz](../img/obveznosti_prikaz.PNG)

####Dodajanje obveznosti
Kot prijavljen uporabnik želimo dodati novo obveznost. Dodamo jo v dialogu za upravljanje z obveznostmi.
![obveznosti_dodajanje](../img/obveznosti_dodajanje.PNG)

####Spreminjanje obveznosti
Kot prijavljen uporabnik želimo spremeniti obstojecčo obveznost. Spremenimo jo v dialogu za upravljanje z obveznostmi.
![obveznost_spremeni](../img/obveznost_spremeni.PNG)

####Brisanje obveznosti
Kot prijavljen uporabnik želimo izbrisati obstojecčo obveznost. Izbrišemo jo v dialogu za upravljanje z obveznostmi.
![obveznosti_brisanje](../img/obveznosti_brisanje.PNG)

####Dodajanje aktivnosti
Kot prijavljen uporabnik želimo obstoječo obveznost spremeniti v aktivnost. V modalnem oknu za dodajanje aktivnosti obveznosti podamo datum in čas začetka dela in planiran čas dela.
![aktivnost_dodajanje](../img/aktivnost_dodajanje.PNG)

####Spreminjanje aktivnosti
Kot prijavljen uporabnik želimo obstoječo aktivnost spremeniti. Najdemo jo v koledarju, nanjo kliknemo, in v modalnem oknu spremenimo vrednosti.
![aktivnost_spremeni](../img/aktivnost_spremeni.PNG)

####Brisanje aktivnosti
Kot prijavljen uporabnik želimo obstoječo aktivnost izbrisati. Najdemo jo v koledarju, nanjo kliknemo, in v modalnem oknu izberemo možnost za izbris.
![aktivnost_brisanje](../img/aktivnost_brisanje.PNG)

####Označevanje opravljenih aktivnosti
Kot prijavljen uporabnik želimo obstoječo aktivnost označiti za opravljeno. Najdemo jo v koledarju, nanjo kliknemo, in jo v modalnem oknu označimo za opravljeno
![aktivnost_opravljena](../img/aktivnost_opravljena.PNG)

####Pregled uporabnikov
Za primer uporabe pregled uporabnikov se na zacetku nahajamo na zaslonski maski za administratorja.

![pregled_uporabnikov](../img/pregled_uporabnikov.PNG)





