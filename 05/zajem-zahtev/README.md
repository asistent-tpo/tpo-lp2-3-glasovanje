# Dokument zahtev

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Aleksandar Jelić, Matevž Štinjek, Luka Glušič |
| **Kraj in datum** | Ljubljana, 7.4.2019 |



## Povzetek projekta

Pri funkcionalnih zahtevah smo določili osnovni tok desetim funkcionalnim zahtevam. Kjer je  bilo potrebno smo podali se izjemne tokove. Dolocili smo tudi prioritete (Must/Should/Could/Would have) in pogoje, ter definirali sprejemne teste.

Določili smo nefunkcionalne zahteve, ki so potrebne za učinkovito delovanje našega sistema. Te zahteve se navezujejo na varnost sistema, zanesljivost sistema, dostopnost sistema ter razširljivost sistema.
Za produkt smo najprej naredili okviren dizajn, potem pa ga dodelali in naredili v Sketchu.

Sestavljajo ga 3 glavne komponente: pregled nacrtovanih obveznosti (koledar), kreiranje novih obveznosti in dodajanje nenacrtovanih obveznosti v koledar (aka. nacrtovanje).


## 1. Uvod

Studentje imajo po bolonjskem sistemu mnogo sportnih obveznosti. Vecina jih je pogoj za opravljen predmet in s tem vpis v naslednji letnik, ostale pa pri tem sigurno pripomorejo.

Teh obveznosti je veliko, njihov datum zapadlosti pa ni zapisan na poenotenem mestu. Tako se lazje kaksna obveznost spregleda, kar lahko ima usodne posledice.

Zato smo razvili koncept za aplikacijo, na kateri bi se studentje lazje organizirali in bolj zanesljivo opravljali svoje obveznosti. Glavna komponenta aplikacije bi bil koledar, v katerem bi bile oznacene vse prihajajoce obveznosti.

Prvi del postopka bi bil kreiranje obveznosti z specificiranjem imena in opisa, tipa in predvidenega porabljenega casa in prioriteto. Drugi del predstavlja dodajanje obveznosti v koledar - v celoti ali le del. (Primer: ce porabimo za ucenje za izpit 10 ur, lahko dodamo obveznost v 5 delih po 2 uri). Tako kreirane kot vnesene obveznosti bi lahko urejal in brisal.

Sistem bo deloval na vseh napravah, da lahko student vedno peveri kaksen je plan dela. Sistem mora biti zanesljiv, varen in sposoben strezit vec tisoc studentom. 


## 2. Uporabniške vloge

### neregistriran uporabnik
- lahko se registrira v sistem

### registriran uporabnik (študent)
- lahko se prijavi v sistem
- lahko si ponastavi svoje geslo
- lahko upravlja z obveznostmi (dodajanje obveznosti, spreminjanje obveznosti, odstranjevanje obveznosti)
- lahko upravlja s koledarjem (dodajanje aktivnosti, spreminjanje aktivnosti, odstranjevanje aktivnosti)
- lahko označi opravljene aktivnosti

### administrator
- lahko se prijavi v sistem
- lahko si ponastavi svoje geslo
- lahko upravlja z uporabniki (dodajanje uporabnikov, spreminjanje uporabnikov, brisanje uporabnikov)



## 3. Slovar pojmov

#### uporabnik
- Oseba, ki uporabja sistem StraightAs.

#### neregistriran uporabnik
- Uporabnuk, ki dostopa do sistema in ni registriran.

#### registriran uporabnik
- Uporabnik, ki se je v sistem že registriral in se lahko prijavi s svojim uporabniškim imenom in geslom.

#### administrator
- Skrbnik sistema in upravljalec uporabniških profilov.

#### obveznost
- Dogodek ali dejavnost, ki jo ima uporabnik in jo lahko doda v sistem. Sestavljena je iz opisa, tipa in predvideno trajanje.

#### tip obveznosti
- Obveznosti je vedno določen tip, vrsta. Tip je lahko eden izmed naslednjih: kolokvij, izpit, učenje, domača naloga.

#### aktivnost
- Obveznost, ki je dodana v koledar. Aktivnosti se določi datum in čas začetka.



## 4. Diagram primerov uporabe

![Diagram primerov uporabe 1. del](../img/primeri_uporabe_1.PNG)
![Diagram primerov uporabe 2. del](../img/primeri_uporabe_2.PNG)


## 5. Funkcionalne zahteve

### Registracija

#### Povzetek funkcionalnosti
Neregistriran uporabnik se lahko registrira v sistem, lahko pa ga registrira tudi skrbnik sistema.

#### Osnovni tok
1. Neregistriran uporabnik v spletni aplikaciji klikne gumb "Registracija".
2. Vnese svoje osebne podatke (ime, priimek, e-poštni naslov).
3. Vpiše svoje novo geslo in ga nato potrdi s ponovnim vpisom.
4. Ko preveri, da so vnešeni podatki točni, klikne gumb "Registracija".
5. Klikne potrditveno povezavo, ki jo prejme na prej vnešen e-poštni naslov, ta pa ga preusmeri nazaj na spletno aplikacijo.

#### Izjemni tokovi
- Uporabnikov e-postni naslov je ze v bazi. Sistem prikaze obvestilo.
- Gesli, ki ju je vnesel uporabnik se ne ujemata. Sistem prikaze obvestilo.

#### Pogoji
Uporabnik ne sme biti registriran v sistem.

#### Sprejemni testi
Na strani "Registracija" vnesi svoje osebne podatke in geslo. Potrdi registracijo.

#### Prioriteta
Should have


### Prijava

#### Povzetek funkcionalnosti
Registriran uporabnik (študent) se lahko prijavi v sistem.

#### Osnovni tok
1. Registriran uporabnik v spletni aplikaciji klikne gumb "Prijava".
2. Vnese svoj e-poštni naslov in geslo, ter klikne gumb "Prijava".
3. Sistem preveri, če se vnešeno geslo ujema z e-poštnim naslovom in v primeru uspesne prijave uporabnika preusmeri na zacetno stran.

#### Izjemni tokovi
- Uporabnikov e-postni naslov se ne obstaja. Sistem prikaze obvestilo.
- E-postni naslov in geslo, ki ju je vnesel uporabnik se ne ujemata. Sistem prikaze obvestilo.

#### Pogoji
Uporabnik mora biti registriran v sistem.

#### Sprejemni testi
Na strani "Prijava" vnesi e-postni naslov in geslo. Potrdi prijavo.

#### Prioriteta
Must have


### Ponastavitev gesla

#### Povzetek funkcionalnosti
Registriran uporabnik (študent) si lahko ponastavi geslo.

#### Osnovni tok
1. Registriran uporabnik v spletni aplikaciji klikne gumb "Prijava".
2. Klikne gumb "Ponastavitev gesla".
3. Vnese svoj e-poštni naslov, ter klikne gumb "Potrdi".
4. Sistem preveri, če uporabnik z vnesenim e-poštnim naslovom obstaja in mu poslje povezavo za ponastavitev gesla.

#### Izjemni tokovi
- Uporabnikov e-postni naslov se ne obstaja. Sistem prikaze obvestilo.

#### Pogoji
Uporabnik mora biti registriran v sistem.

#### Sprejemni testi
Na strani "Ponastavitev gesla" vnesi e-postni naslov. Potrdi ponastavitev gesla.

#### Prioriteta
Could have


### Ustvarjanje obveznosti

#### Povzetek funkcionalnosti
Registriran uporabnik (študent) lahko ustvari novo obveznost.

#### Osnovni tok
1. Registriran uporabnik v spletni aplikaciji na zacetni strani klikne gumb "Ustvari novo obveznost".
2. Izbere tip obveznosti, ki je lahko eden izmed naslednjih: kolokvij, izpit, ucenje ali domaca naloga.
3. Vpise oznako obveznosti in njen opis.
4. Vpise tudi predvideno trajanje obveznosti, ki je podano kot stevilo ur in minut.
5. Ko preveri, da so vneseni podatki tocni, klikne gumb "Ustvari".
6. Ustvarjena obveznost je sedaj vidna v zavihku "Obveznosti".

#### Pogoji
Uporabnik mora biti prijavljen v sistem.

#### Sprejemni testi
Prijavi se kot student. Ustvari novo obveznost. Potrdi. Nova obveznost je vidna v zavihku "Obveznosti".

#### Prioriteta
Must have


### Urejanje obveznosti

#### Povzetek funkcionalnosti
Registriran uporabnik (študent) lahko uredi ustvarjeno obveznost.

#### Osnovni tok
1. Registriran uporabnik v spletni aplikaciji na zacetni strani izbere zavihek "Obveznosti".
2. Izbere obveznost, ki jo zeli urediti.
3. Klikne gumb "Uredi".
4. Na zaslonu enakemu kot pri ustvarjanju obveznosti, lahko spremeni zelene podatke.
5. Ko je koncal z urejanjem, klikne gumb "Potrdi".
6. Sistem uporabnika preusmeri na zacetno stran.

#### Pogoji
Uporabnik mora biti prijavljen v sistem.

#### Sprejemni testi
Prijavi se kot student. Izberi eno izmed ustvarjenih obveznosti in uredni njene parametre. Potrdi spremembe. Urejena obveznost je vidna med obveznostmi.

#### Prioriteta
Should have


### Izbris obveznosti

#### Povzetek funkcionalnosti
Registriran uporabnik (študent) lahko izbrise ustvarjeno obveznost.

#### Osnovni tok
1. Registriran uporabnik v spletni aplikaciji na zacetni strani izbere zavihek "Obveznosti".
2. Izbere obveznost, ki jo zeli izbrisati.
3. Klikne gumb "Izbrisi".
4. Sistem uporabnika preusmeri na zacetno stran.

#### Pogoji
Uporabnik mora biti prijavljen v sistem. Ustvarjeno mora imeti vsaj eno obveznost.

#### Sprejemni testi
Prijavi se kot student. Izberi eno izmed ustvarjenih obveznosti in jo oznaci za izbris. Potrdi izbris. Izbrisana obveznost ni vec vidna med obveznostmi.

#### Prioriteta
Should have


### Dodajanje obveznosti v koledar (ustvarjanje aktivnosti)

#### Povzetek funkcionalnosti
Registriran uporabnik (študent) lahko ustvarjeno obveznost doda v koledar (ustvari aktivnost).

#### Osnovni tok
1. Registriran uporabnik v spletni aplikaciji na zacetni strani izbere zavihek "Obveznosti".
2. Izbere obveznost, ki jo zeli dodati v koledar.
3. Klikne gumb "Dodaj".
4. Izbere datum zacetka aktivnosti in predvideno trajanje.
5. Klikne gumb "Dodaj".
6. Sistem uporabnika preusmeri na zacetno stran.

#### Pogoji
Uporabnik mora biti prijavljen v sistem. Ustvarjeno mora imeti vsaj eno obveznost.

#### Sprejemni testi
Prijavi se kot student. Izberi eno izmed ustvarjenih obveznosti in ustvari novo aktivnost. Ustvarjena aktivnost bi morala biti vidna v koledarju.

#### Prioriteta
Must have


### Urejanje aktivnosti

#### Povzetek funkcionalnosti
Registriran uporabnik (študent) lahko uredi ustvarjeno aktivnost.

#### Osnovni tok
1. Registriran uporabnik v spletni aplikaciji na zacetni strani v koledarju izbere aktivnost, ki jo zeli spremeniti.
2. Klikne gumb "Spremeni".
3. Na zaslonu enakemu kot pri ustvarjanju aktivnosti, lahko spremeni zelene podatke.
4. Ko je koncal z urejanjem, klikne gumb "Potrdi".
5. Sistem uporabnika preusmeri na zacetno stran.

#### Pogoji
Uporabnik mora biti prijavljen v sistem. Ustvarjeno mora imeti vsaj eno aktivnost.

#### Sprejemni testi
Prijavi se kot student. Izberi eno izmed ustvarjenih aktivnosti in uredi njene parametre. Potrdi spremembe. Urejena aktivnost bi morala biti vidna v koledarju.

#### Prioriteta
Should have


### Izbris aktivnosti

#### Povzetek funkcionalnosti
Registriran uporabnik (študent) lahko izbrise ustvarjeno aktivnost.

#### Osnovni tok
1. Registriran uporabnik v spletni aplikaciji na zacetni strani v koledarju izbere aktivnost, ki jo zeli izbrisati.
2. Klikne gumb "Izbrisi".
3. Sistem uporabnika preusmeri na zacetno stran.

#### Pogoji
Uporabnik mora biti prijavljen v sistem. Ustvarjeno mora imeti vsaj eno aktivnost.

#### Sprejemni testi
Prijavi se kot student. Izberi eno izmed ustvarjenih aktivnosti in jo oznaci za izbris. Potrdi izbris. Izbrisana aktivnost ni vec vidna v koledarju.

#### Prioriteta
Should have


### Samodejno ustvarjanje aktivnosti

#### Povzetek funkcionalnosti
Registriran uporabnik (študent) lahko s pomocjo umetne inteligence samodejno ustvari aktivnosti za obveznosti. 

#### Osnovni tok
1. Registriran uporabnik v spletni aplikaciji na zacetni strani klikne gumb "Samodejno ustvarjanje aktivnosti".
2. Ustvarjene aktivnosti so sedaj vidne v koledarju.

#### Pogoji
Uporabnik mora biti prijavljen v sistem.

#### Sprejemni testi
Prijavi se kot student. Pozeni sistem AI za ustvarjanje aktivnosti. Nove aktivnosti so sedaj vidne v koledarju.

#### Prioriteta
Would have

## 6. Nefunkcionalne zahteve

### Delovanje sistema na vseh napravah
Sistem mora delovati na vseh elektronskih napravah kot so računalnik, pametni telefon in tablica. Ker je sistem namenjen študentom, je pomembno, da lahko uporabljajo sistem tudi kadar so na poti. Tako lahko že na poti iz fakultete planirajo aktivnosti za naprej in tako prihranijo nekaj časa.

### Varnost podatkov
Sistem uporabniku ne sme omogočati dostopa do podatkov drugih uporabnikov. Prav tako ne sme omogočati brisanje in spreminjanje podatkov drugih uporabnikov. Neregistrirani uporabniki ne smejo imeti dostopa do nobenih podatkov v sistemu.

### Zanesljivost sistema
Sistem mora biti zanesljiv in na voljo najmanj 99,9 odstotkov časa. Uporabniki bodo lahko dostopali do svojih podatkov, koledarjev in aktivnosti ves čas.

### Hkratni dostop
Sistem mora biti zmožen streči najmanj 3000 hkratnim uporabnikom. S tem omogočimo da bo sistem deloval pravilno in tekoče tudi kadar ga bo uporabljalo veliko uporabnikov hkrati.

### Razširljivost podatkovne baze
Podatkovna baza mora biti narejena tako, da bo v prihotnosti po potrebi možna razširitev. To omogoča hitro razširitev podatkovne baze, če se bo sistem postal priljubljen in pridobil veliko število uporabnikov.




## 7. Osnutki zaslonskih mask

(( Designi niso koncni in imajo par napak ))

* Naslovna stran
![Flow00](../img/00.png)
* Prazen koledar v jutranjih urah
![Flow01](../img/01.png)
* Prazen koledar v delovnih urah
![Flow02](../img/02.png)
* Napolnjen koledar v delovnih ura
![Flow03](../img/03.png)
* Kreiranje nove obveznosti #1
![Flow04](../img/04.png)
* Kreiranje nove obveznosti #2
![Flow05](../img/05.png)
* Kreiranje nove obveznosti #3
![Flow06](../img/06.png)
* Kreiranje nove obveznosti #4
![Flow07](../img/07.png)
* Spet poln koledar v delovnih urah
![Flow08](../img/08.png)
* Dodajanje obveznosti, ki se nimajo dolocenega casa izvedbe v koledar
![Flow09](../img/09.png)
* Ji dolocimo cas in delez
![Flow10](../img/10.png)
* Koledar z dodano novo obveznostjo
![Flow11](../img/11.png)
* Obveznost ni v koledar dodana v celoti, zato je se vidna (2h left)
![Flow12](../img/12.png)

## 8. Vmesniki do zunanjih sistemov

Poleg registracije se bo uporabnik v sistem lahko prijavil z ze obstojecima racunoma (OAuth2):
* Google+ 
* Github

Komunikacija je zelo preprosta: aplikacija bo z uporabo Google+ ali Github APIja iz virnega streznika zahtevala podatke o uporabniku, ki jih bo prejela in jih uporabila za prikazovanje ustreznih podatkov prijavljenemu uporabniku.
