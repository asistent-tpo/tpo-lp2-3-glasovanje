# Načrt sistema

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Mihael Verček, Luka Žnidaršič, Peter Matičič, Lovro Suhadolnik |
| **Kraj in datum** | Ljubljana, april 2019 |



## Povzetek

V prvem delu dokumenta je predstavljen načrt arhitekture aplikacije in dva arhitekturna vzorca, ki bosta v kombinaciji uporabljena za izdelavo aplikacije, to  sta MVC (Model View Controler) in client-server ali odjemalec strežnik. Pri obeh arhitekturnih vzorcih na kratko predstavimo njihove prednosti in slabosti ter logični in razvijalski pogled na njih. V drugem poglavju je predstavljena struktura sistema. Začnemo z razrednim diagramom, ki vsebuje vsega skupaj 14 razredov in povezave med njimi. Diagramu sledijo opisi posameznih razredov v diagramu, njihovih atributov in metod. V tretjem in zadnjem poglavju je načrt obnašanja, ki prikazuje kako razredi sodelujejo med seboj.



## 1. Načrt arhitekture

Implementirali bomo spletno storitev, s preprosto arhitekturo. Arhitektura z logičnega vidika izgleda tako

![](../img/arch-logical-1.png)



Uporabniški vmesnik se povezuje na API. Ko uporabnik sproži zahtevo, jo uporabniški vmesnik posreduje na API. Ta pridobi in spreminja ustrezne podatke, ki jih vsebujejo podatkovni viri.

Če pogledamo aritekturo iz razvojnega vidika, dobimo tako sliko

![Razvojni model arhitekture](../img/arch-developer-1.png)

Uporabniški vmesnik je enoten za vse uporabnike. API vsebuje nadzorne razrede, ki omogočajo vse funkcionalnosti sistema. 

### 1.1 Arhitehturni vzorci

Sistem bomo zgradili na podlagi treh arhitekturnih vzorcev.

#### 1.1.1 Vzorec model pogled nadzornik - Model View Controller

Sledi logični vidik arhitekture našega sistema, ki sledi vzorcu MVC.

![Logični vidik vzorca MVC](../img/arch-mvc-logical.png)

Ko uporabnik zahteva podatke ali izvede akcijo na vmesniku, pogled pošlje podatke o tem v krmilnik. On je koordinator izvajanja akcije. 

Krmilniki podatke preko modelov - sami nikoli neposredno ne dostopajo do virov podatkov. Modeli vsebujejo metode, s katerimi se da podatke pridobiti in jih spreminjati. Vsebujejo tudi poslovno logiko sistema. Pri implementaciji se trudimo, da krmilniki ne obdelujejo podatkov - modeli jim morajo posredovati ustrezne podatke v ustrezni obliki, da jih je mogoče v isti obliki posredovati naprej pogledu. 

Pogledi oblikujejo zahtevane podatke tako, da jih lahko prikaže spletni brskalnik. Pogledov je lahko več, vsak se uporablja za določen namen. Krmilnik izbere pravega in ga vrne brskalniku.

Sledi še arhitektura iz razvijalskega vidika.

![](../img/arch-mvc-developer.png)

#### 1.1.2 Vzorec odjemalec strežnik - client server

Aplikacija je spletna storitev, ki se jo uporablja preko spletnega brskalnika. Za delovanje potrebuje spletni strežnik, ki vrača ustrezne podatke odjemalcu. 

![](../img/arch-sc-logical.png)

Spletni brskalnik, ki je pri uporabniku, se poveže na spletni strežnik, ki pridobi zahtevane podatke iz podatkovnih virov. 

Spletni strežnik in podatkovna baza sta lahko vsak na svojem fizičnem strežniku. Pri tem mora imeti spletni strežnik zagotovljeno povezavo do podatkovne baze in zunanjih sistemov. 

![](../img/arch-sc-developer.png)





## 2. Načrt strukture

### 2.1 Razredni diagram

![](../img/VOPC.png)

### 2.2 Opis razredov

#### User

Abstrakten razred, ki predstavlja splošnega uporabnika. Vsebuje osnovne atribute, ki so enaki za vse uporabnike

#### Atributi

- userId : string : unikaten niz znakov, ki predstavlja identifikator uporabnika
- e-mail : string : e-mail naslov uporabnika
- calendarId : string : unikatni identifikator uporabnikovega koledarja, ki ga pridobimo od Google koledarja
- googleAccessToken : string - žeton za dostop do Google Koledarja
- googleRefreshToken : string - žeton za ponovno pridobivanje zapadlega žetona za dostop
- googleAccessTokenValidUntil : DateTime - do kdaj velja žeton za dostop

#### Nesamoumevne metode

- isAdmin() : bolean : abstraktna metoda, ki se uporablja za preverjanje ali je nek uporabnik administrator ali ne

---

#### AdminUser

Razred, ki predstavlja specializacijo uprabnika v administratorskega uporabnika

#### Atributi

- Vsi atributi podedovani iz razreda User

#### Nesamoumevne metode

- isAdmin() : bolean : podedovana metoda, ki vrne vrednost true ker je ta uporabnik administrator

---

#### StudentUser

Razred, ki predstavlja specializacijo uprabnika v študentskega uporabnika

#### Atributi

- Vsi atributi podedovani iz razreda User
- apiUser : boolean : predstavlja ali je ta uporabnik registriran kot API uporabnik
- moodleLink : string : URL naslov moodle koledarja iz učilnice, ki se uporabi za pridobivanje aktivnosti

#### Nesamoumevne metode

- isAdmin() : bolean : podedovana metoda, ki vrne vrednost false ker ta uporabnik ni administrator
- isApiUser() : boolean : vrne vrednsot polja apiUser

------

#### Activity

Razred, ki predstavlja aktivnost. Implementiran mora biti na odjemalcu in strežniku.

#### Atributi

- userId : string : identifikator uporabnika, kateremu ta aktivnost pripada
- googleId : string : identifikator aktivnosti na uporabnikovem koledarju
- notes : string : opombe
- numberOfHours : integer : število ocenjenih in ob zaključku porabljenih ur
- activityFinished : boolean : polje označuje ali je aktivnost že zaključena
- globalActivityId : string : globalen identifikator aktivnosti za prepoznavanje enakih aktivnosti med uporabniki, izračuna se ob uvažanju iz učilnice
- source : enum (USER, STUDIS, MOODLE) : enumerator, ki predstavlja od kje izvira ta aktivnost (USER - ustvarjena s strani uporabnika, STUDIS - uvožena iz sistema studis, MOODLE - uvožena iz spletne učilnice)

#### Nesamoumevne metode

- getActivityId() : string : vrne kombinacijo userId in googleId
- getActivityUser() : string : vrne userId

------

#### ActivityRating

Razred, ki predstavlja globalno oceno aktivnosti

#### Atributi

- globalActivityID : string : globalen identifikator aktivnosti za prepoznavanje enakih aktivnosti med uporabniki, izračuna se ob uvažanju iz učilnice
- estimatedTimeAvg : double : povprečje ocen potrebnega časa za zaključek aktivnosti
- actualTimeAvg : double : povprečje dejanskih časov potrebnih za zaključek aktivnosti

#### Nesamoumevne metode

Metode so samoumevne, oblike getter / setter.

------

#### ActivityRatingController

Razred, ki omogoča pravilno izračunavanje in shranjevanje povprečij časov

#### Atributi

- /

#### Nesamoumevne metode

- addActualTimeAvg(int actualTime) : double : izračuna novo vrednost povprečja dejanskih časov z dodano vrednostjo actualTime, jo zapiše v ActivityRating in vrne novo vrednost povprečja dejanskih časov
- addEstimatedTimeAvg(int estTime) : double : izračuna novo vrednost povprečja ocen časov z dodano vrednostjo estTime, jo zapiše v ActivityRating in vrne novo vrednost povprečja ocen časov
- updateActualTimeAvg(int oldTime, int newTime) : double : izračuna novo vrednost povprečja dejanskih časov pri spremembi ene od vrednosti, izračun po enačbi (n * ActivityRating.getActualTimeAvg() - oldTime + newTime) / n, to vrednost zapiše v ActivityRating in vrne novo vrednost povprečja dejanskih časov
- updateEstimatedTimeAvg(int oldTime, int newTime) : double : izračuna novo vrednost povprečja ocen časov pri spremembi ene od vrednosti, izračun po enačbi (n * ActivityRating.getEstimatedTimeAvg() - oldTime + newTime) / n, to vrednost zapiše v ActivityRating in vrne novo vrednost povprečja ocen časov

------

#### ActivityController

Razred, ki omogoča pravilno delo z aktivnostmi

#### Atributi

- /

#### Nesamoumevne metode

- createEvent(string userId, string notes, int hours, enum source, DateTime start, DateTime end) : string : ustvari novo aktivnost, najprej izvede funkcijo GoogleCalendarServices.createEvent(start, end), da pridobi identifikator aktivnosti v koledarju, nato doda novo aktivnost v Activity in vrne identifikator aktivnosti (Activity.getActivityId())
- updateEvent(string activityId, string notes, int hours, enum source, DateTime start, DateTime end) : boolean : spremeni podatke o aktivnosti, po potrebi kliče GoogleCalendarServices.updateEvent(googleId, start, end), spremembe zapiše v Activity in vrne vrednost true če je posodabljanje uspelo, drugače false
- deleteEvent(string activityId) : boolean : odstrani aktivnost iz seznama, kliče GoogleCalendarServices.deleteEvent(googleId), vrne true če je odstranjevanje uspelo, drugače false
- finishEvent(string activityId, int hours) : boolean : nastavi aktivnost kot zaključeno, zapiše konče ure v Activity, posodobi čas zaključka v Google koledarju na trenutni čas, posodobi dejanski čas aktivnosti, vrne true, če je zaključevanje uspelo, drugače false
- getEvents(filter) : List : pridobi vse aktivnosti, ki usrezajo kriteriju filter

------

#### **UserController**

Razred, ki omogoča pravilno delo z uporabniki

#### Atributi

- /

#### Nesamoumevne metode

- createUser(user: User) : string

    Ustvari novega uporabnika (registracija)

    Uporabnika doda v bazo, zapiše ustrezne podatke za OAuth avtentikacijo.
    Izvede metodo GoogleCalendarServices.createCalendar(), ki ustvari nov Google koledar za uporabnika in shrani CalendarID.

    Vrne enolični identifikator novega uporabnika.

- modifyUser(user : User) : string

    Uredi podatke o uporabniku. S to metodo se da posodobiti povezavo do dogodkov iz Moodla

    Če je bila posodobitev uspešna, vrne enolični identifikator uporabnika.

- deleteUser(user : User): boolean

    Izbriši uporabnika. To metodo lahko izvede le administratorski uporabnik

    Vrne true, če je bil uporabnik uspešno izbrisan.

------

#### GoogleCalendarServices

Razred, ki omogoča delo s storitvami Google koledarja.

#### Atributi

/

#### Nesamoumevne metode

- createCalendar() : string 

    Usvari nov koledar v uporabnikovem Google računu. 
    Vrne enolični identifikator Google koledarja.

- createEvent(calendarid : string, activity : Activity) : string

    Ustvari nov dogodek v koledarju.
    Vrne enolični identifikator aktivnosti v Google koledarju.

- listEvents(calendarid : string) : List\<Activity\> 

    Vrne vse dogodke iz določenega Google koledarja

- getEvent(calendarid: string, activityid: string) : Activity

    Vrne določen dogodek iz Google koledarja.

- updateEvent(calendarid: string, activity: Activity) : boolean

    Posodobi dogodek. Vrne true, če je bila posodobitev uspešna, sicer false.

- deleteEvent(calendarid : string, activityid: string) : boolean

    Izbriše določen dogodek iz določenega koledarja. Vrne true, če se je dogodek uspešno izbrisal.



------

#### OauthServices

- Razred, ki skrbi za prijavo uporabnikov v sistem s protokolom OAuth.

#### Atributi

/

#### Metode

- validateLogin(username : string, password : string) : boolean

    Preveri prijavne podatke in vrne true, če so ustrezni in sistem lahko prijavi uporabnika.

------

#### UserHandler

- Razred v uporabniškem vmesniku, ki skrbi za vsa dejanja povezana z uporabniki.

#### Atributi

/

#### Metode

- validateLogin() : void

    Kliče se, ko uporabnik pritisne gumb **Prijava**. Podatke iz obrazca za prijavo pošlje na strežnik.

    Vmesnik prikaže ustrezno sporočilo in izvede naslednjo akcijo.

- register() : void

    Kliče se, ko uporabnik pritisne na gumb **Registracija**. Vmesnik pošlje podatke o novem uporabniku na strežnik in izvede ustrezno naslednjo akcijo.

- saveChanges() : void

    Kliče se, ko uporabnik pri ogledu svojega profila pritisne na gumb **Posodobi**. Podatki se pošljejo na strežnik in prikaže se ustrezno sporočilo.

- logout() : void

    Kliče se, ko uporabnik nekje v vmesniku izbere gumb **Odjava**. Sistem izbriše prijavni piškotek in odpre prijavni obrazec
    
- cancel() : void

    Kliče se, ko uporabnik pri ogledu svojega profila pritisne na gumb **Prekliči**. Spremembe se zavržejo in pogled uporabnika se zapre.

------

#### ActivityHandler

- Razred v uporabniškem vmesniku, ki skrbi za vsa dejanja povezana z eno aktivnostjo.

#### Atributi

/

#### Nesamoumevne metode

- addActivity(activity : Activity) : void

    Pripravi obrazec za dodajanje nove aktivnosti. Če je parameter activity null, pripravi prazen obrazec, kjer lahko uporabnik vnese aktivnost na roke. Če parameter ni prazen, odpre obrazec in izpolni polja, ki so na voljo v objektu iz parametra activity.

- saveActivity() : void

    Kliče se, ko uporabnik pritisne gumb **Shrani**. Lahko doda novo aktivnost ali shrani spremembe v obstoječi aktivnosti. Na strežnik pošlje objekt, ki vsebuje vse podatke aktivnosti iz obrazca. Če gre za novo, neobstoječo aktivnost, je parameter activityid prazen.

- cancel() : void

    Uporabnik pritisne gumb **Prekliči**. Zapre pogled trenutne aktivnosti in prikaže vse uporabnikove aktivnosti.

- finishActivity() : void

    Kliče se, ko uporabnik pritisne gumb **Zaključi aktivnost**. Prikaže se okno za dodajanje podatkov o zaključeni aktivnosti.



------

#### ViewHandler

- Razred uporabniškega vmesnika, ki prikazuje seznam aktivnosti (vseh, dnevnih).

#### Nesamoumevne metode

- addActivity() : void

    Kliče se, ko uporabnik pritisne gumb za dodajanje nove aktivnosti.
    Prikaže pojavno okno, kjer uporabnik izbere, ali bo aktivnost vpisal na roke,
    ali jo bo prenesel iz studisa, ali jo bo prenesel iz moodla. Če izbere iz Studisa ali Moodla, sistem kliče ustrezno metodo iz FacultyServiceHandler, če izbere da jo bo vpisal na roke pa kliče ActivityHandler.addActivity()

- getRecentData() : void

    Pridobi aktivnosti za današnji dan in jih prikaže na seznamu.

- getAllData() : void

    Pridobi vse aktivnosti in jih prikaže na seznamu.

- activityClicked() : void

    Ko uporabnik pritisne določeno aktivnost, sistem odpre pogled aktivnosti v razredu ActivityHandler

- googleAuthorize() : void

    Uporabi metode v google calendar API, ki opravijo OAuth2 avtorizacijo z Google računom.

    Nato shrani pridobljen access token in refresh token v bazo.

- googleGetWeeklyData(week : DateTime) : void

    Prikaže vgrajen (embeded) pogled Google koledarja za določen teden

    Sprejme parameter datumskega tipa in iz njega vzame podatek o tednu. Teden se začne s ponedeljkom.

- googleGetMonthlyData(month : DateTime) : void

    Prikaže vgrajen (embeded) pogled Google koledarja za določen mesec.

    Sprejme parameter datumskega tipa in iz njega vzame podatek o mesecu.

------

#### FacultyServices

- Razred, ki skrbi za pridobivanje aktivnosti iz študentskih sistemov. 

#### Atributi

- studentUsername : string
- studentEMailPassword : string

#### Nesamoumevne metode

- FacultyServices(username : string, password : string) 

    Konstruktor, ki prejme študentski e-mail in geslo študentskega maila in ju zapiše v nov objekt.

- getMoodleLink() : string

    Iz moodla pridobi trajno povezavo do dogodkov iz koledarja.

- getMoodleEvents(link : string) : Activity[]

    Iz povezave do Moodla prebere dogodke v iCal datoteki, jih razčleni in jih vrne v obliki seznama aktivnosti.

- getStudisEvents(link : string) : Activity[]

    S pomočjo uporabniškega imena in gesla se prijavi v Studis, prenesi dogodke v ICS datoteki, jih razčleni in vrni kot seznam aktivnosti
    
    

------

#### FacultyServicesHandler

- Razred uporabniškega vmesnika, ki skrbi za pridobivanje aktivnosti iz študentskih sistemov in avtorizacijo študentskega maila.

#### Nesamoumevne metode

- checkCookie() : void

    Preveri, če piškotka studentEMail in studentEMailPassword obstajata. Če ne obstajata, kliče metodo showCredentialsPopup()

- showCredentialsPopup() : void

    Prikaže pojavno okno, kjer uporabnik lahko vpiše svoj študentski email in geslo študentskega maila.

- getStudisActivities() : void

- getMoodleActivities() : void




## 3. Načrt obnašanja

#### Registracija
##### Registracija osnovni tok
![Registracija osnovni tok](../img/nacrt_obnasanja/registracija_osnovni_tok.jpg)
##### Registracija alternativni tok
![Registracija alternativni tok](../img/nacrt_obnasanja/registracija_alternativni_tok.jpg)
##### Registracija izjemni tokovi
![Registracija izjemni tok](../img/nacrt_obnasanja/registracija_izjemni_tokovi.jpg)

------

#### Vnos nove aktivnosti
##### Vnos nove aktivnosti osnovni tok
![Vnos nove aktivnosti osnovni tok](../img/nacrt_obnasanja/vnos_nove_aktivnosti.jpg)

------

#### Pregled dnevnega seznama opravil
##### Pregled dnevnega seznama opravil osnovni tok
![Pregled dnevnega seznama opravil osnovni tok](../img/nacrt_obnasanja/pregled_dnevnega_seznama_opravil_osnovni_tok.jpg)
##### Pregled dnevnega seznama opravil alternativni tok
![Pregled dnevnega seznama opravil alternativni tok](../img/nacrt_obnasanja/pregled_dnevnega_seznama_opravil_alternativni_tok.jpg)

------

#### Spreminjanje podatkov aktivnosti
##### Spreminjanje podatkov aktivnosti osnovni tok
![Spreminjanje podatkov aktivnosti osnovni tok](../img/nacrt_obnasanja/spreminjanje_podatkov_aktivnosti_osnovni_tok.jpg)
##### Spreminjanje podatkov aktivnosti alternativni tok
![Spreminjanje podatkov aktivnosti alternativni tok](../img/nacrt_obnasanja/spreminjanje_podatkov_aktivnosti_alternativni_tok.jpg)
##### Spreminjanje podatkov aktivnosti izjemni tok 1
![Spreminjanje podatkov aktivnosti izjemni tok 1](../img/nacrt_obnasanja/spreminjanje_podatkov_aktivnosti_izjemni_tok_1.jpg)
##### Spreminjanje podatkov aktivnosti izjemni tok 2
![Spreminjanje podatkov aktivnosti izjemni tok 2](../img/nacrt_obnasanja/spreminjanje_podatkov_aktivnosti_izjemni_tok_2.jpg)

------

#### Vpis opravljenega dela
##### Vpis opravljenega dela osnovni tok
![Vpis opravljenega dela osnovni tok](../img/nacrt_obnasanja/vpis_opravljenega_dela_osnovni_tok.jpg)
##### Vpis opravljenega dela izjemni tok 1
![Vpis opravljenega dela izjemni tok 1](../img/nacrt_obnasanja/vpis_opravljenega_dela_izjemni_tok_1.jpg)
##### Vpis opravljenega dela izjemni tok 2
![Vpis opravljenega dela izjemni tok 2](../img/nacrt_obnasanja/vpis_opravljenega_dela_izjemni_tok_2.jpg)

------

#### Pregled in izbris uporabnikov
##### Pregled in izbris uporabnikov osnovni tok
![Pregled in izbris uporabnikov osnovni tok](../img/nacrt_obnasanja/pregled_in_izbris_uporabnikov_osnovni_tok.jpg)
##### Pregled in izbris uporabnikov izredni tok
![Pregled in izbris uporabnikov izredni tok](../img/nacrt_obnasanja/pregled_in_izbris_uporabnikov_izredni_tok.jpg)

#### Prijava v sistem

Prijava študenta ali administratorja.
![](../img/diagram-prijava.png)

Prijava API uporabnika
![](../img/diagram-prijava-API.png)

------

#### Avtorizacija študentskega maila

![](../img/diagram-avtorizacija.png)

------

#### Ogled posamezne aktivnosti

![](../img/diagram-ogled-aktivnosti.png)

------

#### Pregled tedenskega, mesečnega koldarja

![](../img/diagram-ogled-aktivnosti.png)

------

#### Ocenjevanje težavnosti aktivnosti

![](../img/diagram-oceni-tezavnost.png)

------

#### Samodejno posodabljanje podatkov aktivnosti

``` python
def dodaj_nove(lokalne, nove):
	for aktivnost in nove:
		lokalne[aktivnost.id] = aktivnost
	return lokalne

for uporabnik in uporabniki:
	aktivnost_moodle = MoodleBridge.getAllOfUser(uporabnik)
	aktivnosti_studis = StudisBridge.getAllOfUser(uporabnik)
	aktivnosti_shranjene = SQLBridge.getAllOfUser(uporabnik)
	
	# posodobi podatke
	for key in aktivnosti_shranjene:
		if key in aktivnosti_moodle:
			aktivnosti_shranjene[key].updateData(aktivnosti_moodle[key])
			aktivnosti_moodle.remove(key)
		elif key in aktivnosti_studis
			aktivnosti_shranjene[key].updateData(aktivnosti_studis[key])
			aktvinosti_studis.remove(key)
			
	# dodaj nove
	aktivnosti_shranjene = dodaj_nove(aktivnosti_shranjene, aktivnosti_moodle)
	aktivnosti_shranjene = dodaj_nove(aktivnosti_shranjene, aktivnosti_studis)
	
	SQLBridge.updateActivitiesOfUser(uporabnik, aktivnosti_shranjene)
```

