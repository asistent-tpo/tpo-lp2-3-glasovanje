# Dokument zahtev

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Mihael Verček, Luka Žnidaršič, Peter Matičič, Lovro Suhadolnik |
| **Kraj in datum** | Ljubljana, 28. 3. 2019 |



## Povzetek projekta

Spletna storitev StraightAs je še en poskus implementacije merjenja časa, ki ga uporabnik porabi za svoje vsakodnevne aktivnosti. Namenjena je študentom fakultete za računalništvo in informatiko, za katere je značilno, da imajo veliko domačih nalog in sprotnih obveznosti. Za dobro organizacijo je pomembno, da človek za vsako aktivnost ve koliko časa potrebuje zanjo. Študenti bodo lahko na enostaven način prenesli svoje obveznosti neposredno iz Učilnice in Studisa, ter ocenili, koliko časa bodo za vsako porabili. Poleg svoje ocene, bodo videli tudi ocene drugih študentov, ki so vnesli enako aktivnost. Nato bodo lahko enostavno vnašali porabljen čas.  Svoje dnevne aktivnosti bodo lahko spremljali na dnevnem seznamu opravil, ki bo vgrajen v aplikacijo, vse aktivnosti pa bodo tudi sinhronizirane s storitvijo Google Calendar.



## 1. Uvod

Večina študentov je danes zelo obremenjenih in težko sledijo vsem obveznostim, ki se pojavljajo med študijem. Včasih imajo občutek da je vsak dan več obveznosti in se ne znajo soočiti z pravilno organizacijo le teh, poleg tega pa se pojavljajo še predmeti, ki svojih obveznosti ne objavijo na spletni učilnici in se le te ne prikažejo v njenem koledarju - predvsem naloge, ki se pišejo čez celo leto. Poleg vseh teh preglavic pa za določene obveznosti študentje nimajo najboljšega občutka koliko časa bodo zanje potrebovali. Nekatere so preproste in se končajo v uri ali dveh, za nekatere pa je potrebnih več dni trdega dela. Vsak se s temi težavami sooča na drugačen način, nekateri si na roke prepisujejo vse svoje obveznosti v koledar, ali pa uporablajo kakšno spletno aplikacijo, ki jim pomaga pri organizaciji. Vendar kljub vsemu še vedno težko ocenijo koliko časa bodo potrebovali za zaključek določene naloge. 

Naša aplikacij bi študentom omogočila preprost vpogled v to, koliko časa porabijo za določeno aktivnost. Ta podatek bi se pridobil iz socialnega vidika naše aplikacije, saj bi vsak študent pri vpisu aktivnosti podal svojo oceno časa, ki ga bo potreboval za opravljanje določene naloge in na tak način bi vsak študent videl povprečje ocen za to nalogo. Dodatno pa bi omogočili, da se aktivnosti shrani za vsako leto in za tiste, ki se ponovijo, bi študentom pokazali koliko dejanskega časa so porabile prejšnje generacie za opravljanje te naloge. Med samim izvajanjem neke aktivnosti si študent redno zapisuje, koliko časa je delal na njej, ta številka pa se ob zaključku povpreči z vsemi ostalimi in pripiše tej aktivnosti. Naša aplikacija pa bi dodanto reševala še en problem, namreč kot omenjeno ima vsak študent večino svojih rednih obveznosti (domače naloge, seminarske naloge, itd.) vpisanih v spletni učilnici, ter vse svoje končne obveznosti v sistemu Studis. Namesto da bi vsak študent posebej prepisoval podatke za vsako aktivnost, in poleg teka riskiral še kašno tiskarsko napako v imenu aktivnosti, kaj šele pri vpisovanju roka oddaje se bo sistem samodejno povezoval na obe platformi, ki vsebujejo vse te obveznosti in avtomasko prenesel vse obveznosti v študentov koledar. Poleg vsega tega, pa bo vsak študent lahko vnesel še svoje aktivnosti, ki bodo veljale samo zanj, na primer obisk zobozdravnika ali frizerja. Končni cilj je narediti preprost sistem za boljšo časovno organizacijo vsakega študenta.

Vsak študent se bo v aplikacijo registriral in prijavil s svojim študentskim e-mail naslovom preko google auth sistema. Pri registraciji bo vpisal svojo vpisno številko, ki bo shranjena na strežniku za preprostejši dostop do njegovega urnika in generirano povezavo iz spletne učilnice za prenos nalog iz nje. Po uspešni registraciji bo sistem prenesel vse dogodke iz spletne učilnice in jih shranil v koledar ter preusmeril uporabnika na stran z vsemi aktivnostmi. Od tu lahko uporabnik obišče več različnih pogledov. Prvi na katerem se že nahaja vsebuje vse shranjene aktivnosti razvrščene po datumu zaključka. Drugi možen pogled je dnevni pogled, ki prikazuje vse aktivnosti, ki jih mora študent opraviti v tistem dnevu, da si pa lažje predstavlja obseg števila aktivnosti pa sta na voljo tudi še tedenski in mesečni pogled na vse aktivnosti. Od tu lahko uporabnik ureja in dodaja aktivnosti. Ob kliku na neko aktivnost se uporabniku odpre pojavno okno, ki vsebuje vse podatke o aktivnosti, katere lahko tudi ureja. Tu ima tudi možnost zaključka aktivnosti, ki pa še dodatno pred dejanskim zaključkom še enkrat vpraša uporabnika ali je res porabil toliko ur kot jih je navedel, ali morda več. Po potrditvi se aktivnost za uporabnika zaključi, število porabljenih ur pa se posodobi v sistemu. V primeru da uporabnik ne želi zaključiti aktivnosti ampak samo urediti določene podatke ima tudi možnost shranjevanja sprememb ali zapiranja okna brez sprememb. Za dodajanje nove aktivnosti je uporabniku na voljo gumb dodaj aktivnost, ki uporabnika preusmeri na novo stran, kjer mora vpisati vse podatke o aktivnosti: naziv, začetni datum, končni datum, opombe in predviden čas za opravljanje. Po kliku shrani se ta nova aktivnost doda na uporabnikov koledar. 

Seveda mora biti tak sistem tudi odziven in v najslabših primerih omogočati delo vsem študentom fakultete, ki pa jih je bilo v letu 2018 preko 1600. To pomeni veliko obremenitev za strežnike in podatkovno bazo kar seveda posledično zahteva, da je naša aplikacija nezahtevna z viri na povezanega uporabnika. Cilj je omogočiti uprabniku čim hitrejše odgovore s strani strežnika, kjer je zgornja meja 500ms. Hkrati mora biti sistem tudi dosegljiv večino časa kjer bomo zahtevali da sistem deluje 99,98% časa. Poleg vseh odzivnostnih in zanesljivostnih zahtevm živimo v času kjer morajo biti podatki skrbno varovani, zato je velik poudarek na omejevanju dostopa do podatkov nepooblaščenim osebam. Kar pomeni da ne sme v nobene primeru en študent dobiti aktivnosti drugega študenta, kaj šele zunanji uporabnik dostopati do aktivnosti nekega študenta. Zato bo vsak uporabnik po prijavi dobil piškotek z omejenim časom veljavnosti, ki bo omogočal dostop le njegovim podatkom.

## 2. Uporabniške vloge

- **študent**

To je glavni uporabnik sistema. Je študent FRI in ima svoj študentski mail in pozna geslo študentskega maila. Do podatkov lahko dostopa preko spletnega vmesnika, lahko pregleduje in spreminja le svoje aktivnosti, lahko ocenjuje svoje aktivnosti. Lahko vpisuje delo za svoje aktivnosti. Vpisano ima vpisno številko.

- **neregistriran uporabnik**

Lahko se registrira, vpiše svoje podatke in avtorizira svoj študentski mail.

- **registriran uporabnik**

Uporabnik, ki ima svoj uporabniški račun. Lahko se prijavi v sistem. To so študent, API uporabnik in administrator, ki so se bodisi registrirali, bodisi je njihov uporabniški račun obstajal že prej, ali pa je bil v sistem ročno vnešen.

- **API uporabnik**

Registriran uporabnik, ki lahko počne enake stvari kot študent preko API-ja.

- **administrator**

Registriran uporabnik, ki lahko dostopa do vseh podatkov v sistemu in jih spreminja.



## 3. Slovar pojmov

* **sistem**: program StraightAs, njegov grafični in API vmesnik, njegova podatkovna baza
* **aplikacija**: grafični vmesnik sistema, spletna stran, preko katere študentje uporabljajo sistem.
* **aktivnost:** izpit, kolokvij, domača naloga, seminarska naloga ali katerakoli druga študentska obveznost, ki jo mora študent oddati ali se nanjo pripraviti
* **uporabnik**: človek ali računalniški program, ki uporablja sistem
* **uporabniški račun**: zapis v podatkovni bazi, kjer so zapisani uporabniško ime, zgoščena vrednost gesla in drugi podatki o uporabniku.
* **registracija**: postopek, s katerim neregistriran uporabnik pridobi svoj uporabniški račun, da lahko uporablja sistem.
* **avtorizacija**: postopek preverjanja študentskega e-maila in gesla, ki preveri, če se lahko sistem z njima prijavi v zunanje sisteme in iz njih pridobi podatke.
* **API**: programski vmesnik sistema, preko katerega lahko druge storitve dostopajo do podatkov
* **zaščitena povezava**: Povezava v API-ju, do katere lahko dostopajo le določeni prijavljeni uporabniki.
* **prijavljen uporabnik**: uporabnik, ki se je v sistem prijavil z uporabniškim imenom in geslom.
* **uporabniško ime**: e-mail naslov uporabniškega računa, ki ga enolično določa. Z njim se prijavi v sistem. To ime ni nujno enako študentskemu mailu.
* **študentski sistem**: sistem, ki ga študent uporablja za študijske namene: Učilnica, Studis, Urnik, ...
* **študentski mail**: e-poštni naslov, ki se uproablja za vpis v vse študentske sisteme
* **geslo študentskega maila**: tekst, ki ga uporabnik poleg študentskega maila uporabi za prijavo v vse študentske sisteme. **Študentsko geslo se ne shrani v podatkovni bazi sistema**, temveč ostane zapisano v **piškotku na odjemalcu**. Ko strežnik potrebuje dostop do študentskega sistema, se geslo študentskega maila tja prenese preko šifrirane povezave in se ne shrani.
* **napačno uporabniško ime**: uporabniško ime, ki ne določa nobenega uporabniškega računa
* **geslo**: več kot 7-mestni, neprazen niz črk, številk in simbolov
* **napačno geslo**: prazno geslo ali geslo, ki se ne ujema z geslom, ki je zapisano v uporabniškem računu.
* **prijazno sporočilo**: Človeku razumljiv tekst, ki ga vrne API in je namenjen prikazu v uporabniškem vmesniku
* **izpolnjena aktivnost**: pretekla aktivnost, ki ni več aktualna za študenta in se mu nanjo ni treba več pripravljati
* **kategorija aktivnosti**: izpit, seminarska naloga, domača naloga, kolokvij, ...





## 4. Diagram primerov uporabe

![Diagram primerov uporabe](./uml.png)

## 5. Funkcionalne zahteve

Pri sprejemnih testih se držimo naslednje notacije:

- Vrstica, ki testira osnovni tok, se začne z **O:**
- Vrstica, ki testira alternativni tok X, se začne z **AX:**
- Vrstica, ki testira izjemni tok X, se začne z **IX:**



#### Seznam funkcionalnosti

- (MUST) Prijava v sistem
- (MUST) Registracija
- (COULD) Avtorizacija študentskega maila
- (MUST) Vpis nove aktivnosti
- (MUST) Ogled posamezne aktivnosti
- (MUST) Pregled dnevnega seznama opravil
- (COULD) Pregled tedenskega, mesečnega koledarja
- (SHOULD) Spreminjanje podatkov aktivnosti
- (SHOULD) Ocenjevanje težavnosti aktivnosti
- (MUST) Vpis opravljenega dela
- (WOULD) Samodejno posodabljanje podatkov aktivnosti
- (MUST) Pregled in izbris uporabnikov



### [MUST] Prijava v sistem

#### Povzetek funkcionalnosti

Registriran uporabnik se lahko v sistem prijavi s svojim uporabniškim imenom in geslom. 

#### Osnovni tok

1. Študent ali administrator odpre sistem
2. Pokaže se mu prijavni obrazec, ki vsebuje polje za uporabniško ime in geslo
3. Študent ali administrator vnese uporabniško ime in geslo in pritisne gumb **PRIJAVA**.
4. Sistem prikaže sporočilo, da je bila prijava uspešna.

#### Alternativni tok 1

1. API uporabnik zahteva zaščiteno povezavo v vmesniku

2. V zahtevi poleg drugih parametrov vključi svoje uporabniško ime in geslo

3. Sistem vrne zahtevane podatke

#### Izjemni tokovi

1. Uporabnik vnese napačno uporabniško ime. 

    ​	Sistem prikaže sporočilo, da je vnešeno uporabniško ime ali geslo napačno.

2. Uporabniški račun je pravilen, geslo je napačno. 

    ​	Sistem prikaže sporočilo, da je vnešeno uporabniško ime ali geslo napačno.

3. Uporabniški račun, ga določa vnešeno uporabniško ime, je blokiran, zato se uporabnik ne more prijaviti.

    ​	Sistem prikaže sporočilo, da je uporabnik blokiran.

4. Uporabniško ime, ki je bilo poslano poleg API zahteve je napačno

    ​	Sistem vrne napako 400 in prijazno sporočilo, da so prijavni podatki napačni

5. Geslo, ki je bilo poslano poleg API zahteve je napačno.

    ​	Sistem vrne napako 400 in prijazno sporočilo, da so prijavni podatki napačni

#### Pogoj

- Prijavo v sistem lahko izvede le registriran uporabnik

#### Posledici

- Neprijavljen študent postane prijavljen študent in lahko začne uporabljati sistem
- API uporabnik prejme zahtevane podatke

#### Posebnosti

- Pri preverjanju gesla na strani strežnika, se izračuna njegova zgoščena vrednost in se primerja s tisto v bazi

#### Prioriteta:

- Must have. Brez prijave bi sistem razkrival občutljive uporabniške podatke

#### Sprejemni testi

- O: Študent odpre aplikacijo, vpiše obstoječe uporabniško ime in pravilno geslo, odpre se mu glavni pogled aplikacije
- A1: Zunanja storitev zahteva zaščiteno povezavo sistema, poleg zahteve pošlje uporabniško ime obstoječega uporabniškega računa in pravilno geslo, API vrne zahtevane podatke
- I1: Študent odpre aplikacijo, vpiše napačno uporabniško ime in neko geslo, sistem prikaže sporočilo o napaki
- I2: Študent odpre aplikacijo, vpiše pravilno uporabniško ime in napačno geslo, sistem prikaže sporočilo o napaki
- I3: Študent odpre aplikacijo, vpiše uporabniško ime uporabniškega računa, ki je bil blokiran, sistem prikaže sporočilo o napaki
- I4: Odpri program (recimo Postman), zahtevaj zaščiteno povezavo, podaj napačno uporabniško ime, sistem vrne napako 400 in prijazno sporočilo o napaki
- I5: Odpri program (recimo Postman), zahtevaj zaščiteno povezavo, podaj pravilno uporabniško ime in napačno geslo, sistem vrne napako 400 in prijazno sporočilo o napaki



### [MUST] Registracija

#### Povzetek funkcionalnosti

Neregistriran uporabnik se lahko registrira v sistem, da ga lahko začne uporabljati.

#### Osnovni tok

1. Neregistriran uporabnik odpre aplikacijo
2. Pritisne gumb **REGISTRACIJA**
3. Odpre se mu obrazec, kjer vpiše svoje novo uporabniško ime, geslo, ponovitev gesla, ime, priimek in vpisno številko
4. Pritisne gumb registracija
5. Sistem prikaže sporočilo, da je bila registracija uspešna
6. Presusmeri ga na stran s prijavo

#### Alternativni tok 1

1. Administrator se poveže na podatkovno bazo
2. Vnese svoje svoje uporabniško ime in geslo
3. Zažene proceduro, ki doda novega uporabnika in poda ustrezne parametre
4. Vpiše novo uporabniško ime, geslo, ponovitev gesla, ime, priimek in vpisno številko
5. Sistem vrne sporočilo, da se je postopek uspešno zaključil

#### Izjemni tokovi

1. Neregistriran uporabnik vpiše Uporabniško ime, ki že obstaja

    ​	Sistem izpiše sporočilo, da uporabniško ime že obstaja

2. Neregistriran uporabnik vnese niz, ki ni geslo

    ​	Sistem izpiše sporočilo, ki obvesti uporabnika, kakšno geslo se zahteva

3. Neregistriran uporabnik vnese ponovitev gesla, ki se ne ujema z geslom

    ​	Sistem izpiše sporočilo, da se gesli ne ujemata

4. Neregistriran uporabnik ne vnese uporabniškega imena, gesla, ponovitve gesla, imena, priimka ali vpisne številke

    ​	Sistem izpiše sporočilo, da so vsi podatki obvezni.

#### Pogoji

* Uporabnik ni registriran

#### Posledice

- Ustvari se uporabniški račun, ki ga lahko uporabnik uporabi za prijavo v sistem
- Neregistriran uporabnik postane študent

#### Posebnosti

- V bazi se shranjujejo zgoščena gesla in ne gesla sama
- Do strani je potrebno dostopati preko šifrirane povezave

#### Prioriteta: Must have

#### Sprejemni testi

- **O:** Neregistriran uporabnik odpre aplikacijo, pritisne gumb registracija, vnese uporabniško ime, ki še ne obstaja, ustrezno geslo, ponovitev gesla, svoje ime, priimek in svojo vpisno številko. Sistem vrne sporočilo, da je bila registracija uspešna, preusmeri ga na stran za prijavo.
- **A1:** Administrator odpre povezavo do baze, vpiše svoje uporabniško ime in geslo, zažene proceduro, kjer vpiše uporabniško ime, ki še ne obstaja, ustrezno geslo, ponovitev gesla, ime, priimek in vpisno številko študenta in izvede proceduro. Program vrne sporočilo o uspehu
- **I1:** Neregistriran uporabnik odpre aplikacijo, pritisne gumb registracija, vnese uporabniško ime, ki že obstaja, neko geslo, ponovitev tega gesla, ime, priimek in svojo vpisno številko. Sistem vrne sporočilo, da je uporabniško ime že zasedeno in pobriše polje za uporabniško ime.
- **I2:** Neregistriran uporabnik odpre aplikacijo, pritisne gumb registracija, vnese uporabniško ime, ki še ne obstaja, neustrezno geslo, ponovitev neustreznega gesla, svoje ime, priimek in svojo vpisno številko. Sistem vrne sporočilo, da geslo ni ustrezno in pobriše polji za geslo.
- **I3:** Neregistriran uporabnik odpre aplikacijo, pritisne gumb registracija, vnese uporabniško ime, ki še ne obstaja, neko geslo, ponovitev tega gesla, svoje ime, priimek in svojo vpisno številko. Sistem vrne sporočilo, da se gesli ne ujemata in pobriše polji za geslo.
- **I4:** Neregistriran uporabnik odpre aplikacijo, pritisne gumb registracija, ne izpolni kakega polja. Sistem vrne sporočilo, da so vsi podatki obvezni.



### [SHOULD] Avtorizacija študentskega maila

#### Povzetek funkcionalnosti

Študent lahko vpiše svoj študentski mail in s tem omogoči sistemu, da v njegovem imenu prenaša podatke iz študentskih sistemov.

#### Osnovni tok

1. Študent odpre okno za dodajanje aktivnosti
2. Sistem prikaže sporočilo, kjer sta polji za študentski mail in geslo študentskega maila
3. Študent vpiše študentski mail in geslo študentskega maila
4. Odjemalec v piškotke zapiše študentski mail in geslo študentskega maila
5. Odjemalec posreduje zahtevo strežniku
6. Sistem se s prejetimi podatki poveže v študentski sistem
7. Sistem iz študentskega sistema prenese zahtevane podatke

#### Alternativni tok 1

1. Študent odpre okno za dodajanje aktivnosti, brskalnik ima shranjen študentski mail in geslo študentskega maila
2. Odjemalec posreduje zahtevo strežniku
3. Sistem se s prejetimi podatki poveže v študentski sistem
4. Sistem iz študentskega sistema prenese zahtevane podatke

#### Izjemni tok

1. Študent vpiše študentski mail in geslo študentskega maila, ki nista pravilna

    Sistem obvesti študenta, da se ne more prijavit v študentski sistem in znova prikaže polja za vpis študentskega maila in gesla študentskega maila


#### Pogoji

- Študent zahteva dodajanje aktivnosti iz študentskega sistema

#### Posledice

- Sistem ima za čas ene zahteve dostop do študentskega sistema
- Študent lahko doda dogodke iz študentskega sistema

#### Posebnosti

- Študentski mail in geslo študentskega maila se ne shranita na strežniku

#### Prioriteta: Should have

Zelo dobro bi bilo, da bi to funkcionalnost imeli, ker je precej ključna, a se dogodke da vnašati tudi na roke, zato bi se aplikacijo do neke mere dalo uporabljati tudi brez te funkcionalnosti

#### Sprejemni testi

- **O:** Študent odpre aplikacijo, odpre pogled za dodajanje aktivnosti, izbere dodajanje iz zunanjega sistema, vnese študentski mail in geslo študentskega maila, pokaže se mu seznam aktivnosti iz učilnice in studisa
- **A1: ** Študent odpre aplikacijo, odpre pogled za dodajanje aktivnosti, izbere dodajanje iz zunanjega sistema, vnese študentski mail in geslo študentskega maila, pokaže se mu seznam aktivnosti iz učilnice in studisa. Študent znova odpre aplikacijo, odpre pogled za dodajanje aktivnosti, izbere dodajanje iz zunanjega sistema, sedaj mu ni potrebno znova vpisati študentskega maila in gesla študentskega maila, pokaže se mu seznam aktivnosti iz učilnice in studisa.
- **I1:** Študent odpre aplikacijo, odpre pogled za dodajanje aktivnosti, izbere dodajanje iz zunanjega sistema, vnese napačen študentski mail ali napačno geslo študentskega maila, ali poljubno kombinacijo obeh, pokaže se mu sporočilo, da se ne more povezati na zunanji sistem in znova se prikaže okno za vpis študentskega maila.



### [MUST] Vpis nove aktivnosti

#### Povzetek funkcionalnosti

Študent ali API uporabnik lahko doda novo aktivnost na seznam svojih aktivnosti.

#### Osnovni tok

1. Študent odpre aplikacijo
2. Pritisne na gumb **Dodaj aktivnost**
3. Odpre se izbirni pogled, kjer lahko izbere metodo za dodajanje nove aktivnosti
4. Izbere **Dodaj ročno**
5. Vnese ime aktivnosti in pričakovano število ur, ki jih bo porabil, da jo izpolni
6. Izbere kategorijo aktivnosti
7. Vnese datum, do kdaj mora aktivnost opraviti
8. Pritisne gumb Shrani
9. Sistem shrani aktivnost
10. Sistem doda aktivnost v Google Calendar
11. Sistem javi, da je bila aktivnost uspešno dodana
12. Sistem preusmeri študenta na stran z vsemi aktivnostimi

#### Alternativni tok 1

1. Študent odpre aplikacijo
2. Pritisne na gumb **Dodaj aktivnost**
3. Odpre se izbirni pogled, kjer lahko izbere metodo za dodajanje nove aktivnosti
4. Izbere **Dodaj iz Učilnice**
5. Sistem prikaže sporočilo o poteku dela
6. Sistem prikaže možne aktivnosti iz Učilnice
7. Iz seznama možnih aktivnosti izbere željeno aktivnost
8. Sistem odpre obrazec za ročno dodajanje aktivnosti in samodejno izpolni vsa polja na obrazcu
9. Študent pregleda izpolnjene vrednosti
10. Študent pritisne gumb Shrani
11. Sistem shrani aktivnost
12. Sistem doda aktivnost v Google Calendar
13. Sistem javi, da je bila aktivnost uspešno dodana
14. Sistem preusmeri študenta na stran z vsemi aktivnostimi

#### Alternativni tok 2

1. Študent odpre aplikacijo
2. Pritisne na gumb **Dodaj aktivnost**
3. Odpre se izbirni pogled, kjer lahko izbere metodo za dodajanje nove aktivnosti
4. Izbere **Dodaj iz Studisa**
5. Sistem prikaže sporočilo o poteku dela
6. Sistem prikaže možne aktivnosti iz Studisa
7. Iz seznama možnih aktivnosti izbere željeno aktivnost
8. Sistem odpre obrazec za ročno dodajanje aktivnosti in samodejno izpolni vsa polja na obrazcu
9. Študent pregleda izpolnjene vrednosti
10. Študent pritisne gumb Shrani
11. Sistem shrani aktivnost
12. Sistem doda aktivnost v Google Calendar
13. Sistem javi, da je bila aktivnost uspešno dodana
14. Sistem preusmeri študenta na stran z vsemi aktivnostimi



**Izjemni tokovi**

1. Avtorizacija študentskega maila ni uspela

    ​	Sistem študenta preusmeri na seznam vseh aktivnosti

2. Podatki o aktivnosti iz študentskega sistema študentu ne ustrezajo

    ​	Študent po izbiri aktivnosti popravi željena polja

3. Študent ne izpolni vseh podatkov, ki so zahtevani

    ​	Sistem javi napako, in študenta obvesti, kateri podatki so obvezni

4. Študent izpolni datum v napačni obliki

    ​	Sistem javi napako, da je datum v napačni obliki.

5. Sistem ni mogel prenesti podatkov iz študentskega sistema

    ​	Sistem javi napako in prikaže okno, kjer lahko študent znova zažene postopek.

#### Pogoji

- Študent mora biti prijavljen

#### Posledice

- Na seznamu vseh aktivnosti je nova aktivnost

#### Posebnosti

- Funkcionalnost uporablja avtorizacijo študentskega maila
- Pri API zahtevi se prijava preveri s funkcionalnostjo Prijava v sistem

#### Prioriteta: Must have

#### Sprejemni testi

- **O:** Prijavljen študent odpre aplikacijo, pritisne na gumb dodaj aktivnost, odpre se izbirni pogled, kjer lahko izbere metodo za dodajanje nove aktivnosti, izbere dodaj ročno, vnese ime aktivnosti in pričakovano število ur, ki jih bo porabil, da jo izpolni, izbere kategorijo aktivnosti, vnese datum, do kdaj mora aktivnost opraviti, pritisne gumb shrani, sistem javi, da je bila aktivnost uspešno dodana, sistem preusmeri študenta na stran z vsemi aktivnostimi
- **A1:** Prijavljen študent odpre aplikacijo, pritisne na gumb dodaj aktivnost, odpre se izbirni pogled, kjer lahko izbere metodo za dodajanje nove aktivnosti, izbere dodaj iz Učilnice, sistem prikaže možne aktivnosti iz učilnice, iz seznama možnih aktivnosti izbere željeno aktivnost, sistem odpre obrazec za ročno dodajanje aktivnosti in samodejno izpolni vsa polja na obrazcu, študent pregleda izpolnjene vrednosti, študent pritisne gumb shrani, sistem javi, da je bila aktivnost uspešno dodana, sistem preusmeri študenta na stran z vsemi aktivnostimi. Na pogledu je poleg prejšnjih tudi nova aktivnost.
- **A2:** Prijavljen študent odpre aplikacijo, pritisne na gumb Dodaj aktivnost, odpre se izbirni pogled, kjer lahko izbere metodo za dodajanje nove aktivnosti, izbere dodaj iz Studisa, sistem prikaže sporočilo o poteku dela, sistem prikaže možne aktivnosti iz Studisa, iz seznama možnih aktivnosti izbere željeno aktivnost, sistem odpre obrazec za ročno dodajanje aktivnosti in samodejno izpolni vsa polja na obrazcu, študent pregleda izpolnjene vrednosti in pritisne gumb Shrani. Sistem javi, da je bila aktivnost uspešno dodana in preusmeri študenta na stran z vsemi aktivnostimi. Na seznamu je poleg starih tudi nova aktivnost.
- **I1:** Študent pobriše piškotke v brskalniku. Odpre aplikacijo in pritisne na gumb dodaj aktivnost. Odpre se izbirni pogled, kjer lahko izbere metodo za dodajanje nove aktivnosti.  Izbere dodaj iz Učilnice, odpre se okno za vpis študentskega maila in gesla študentskega maila. Študent vpiše napačne podatke. Sistem javi, da se ne more povezati na študentski sistem. Sistem pobriše polji za vnos podatkov. Študent pritisne prekliči. Sistem ga preusmeri na seznam vseh aktivnosti.
- **I2:** Prijavljen študent odpre aplikacijo, pritisne na gumb Dodaj aktivnost, odpre se izbirni pogled, kjer lahko izbere metodo za dodajanje nove aktivnosti, izbere dodaj iz Učilnice, sistem prikaže sporočilo o poteku dela, sistem prikaže možne aktivnosti iz Učilnice, iz seznama možnih aktivnosti izbere željeno aktivnost, sistem odpre obrazec za ročno dodajanje aktivnosti in samodejno izpolni vsa polja na obrazcu, študent pregleda izpolnjene vrednosti. Popravi neustrezne vrednosti in pritisne gumb Shrani. Sistem javi, da je bila aktivnost uspešno dodana in preusmeri študenta na stran z vsemi aktivnostimi. Na seznamu je poleg starih tudi nova aktivnost.
- **I3:** Prijavljen študent odpre aplikacijo, pritisne na gumb Dodaj aktivnost, odpre se izbirni pogled, kjer lahko izbere metodo za dodajanje nove aktivnosti, izbere dodaj iz Studisa, sistem prikaže sporočilo o poteku dela, sistem prikaže možne aktivnosti iz Studisa, iz seznama možnih aktivnosti izbere željeno aktivnost, sistem odpre obrazec za ročno dodajanje aktivnosti in samodejno izpolni vsa polja na obrazcu, študent pregleda izpolnjene vrednosti in pobriše eno polje.  Pritisne gumb Shrani. Sistem javi napako, in študenta obvesti, kateri podatki so obvezni
- **I4:**  Prijavljen študent odpre aplikacijo, pritisne na gumb Dodaj aktivnost, odpre se izbirni pogled, kjer lahko izbere metodo za dodajanje nove aktivnosti, izbere dodaj iz Studisa, sistem prikaže sporočilo o poteku dela, sistem prikaže možne aktivnosti iz Studisa, iz seznama možnih aktivnosti izbere željeno aktivnost, sistem odpre obrazec za ročno dodajanje aktivnosti in samodejno izpolni vsa polja na obrazcu, študent pregleda izpolnjene vrednosti in v polje za končni datum doda par podpičij ali drugih neprimernih znakov.  Pritisne gumb Shrani. Sistem javi napako, in študenta obvesti, kateri podatki so obvezni
- **I5:** Na požarnem zidu onemogočimo dostop do učilnice. Prijavljen študent odpre aplikacijo, pritisne na gumb Dodaj aktivnost, odpre se izbirni pogled, kjer lahko izbere metodo za dodajanje nove aktivnosti, izbere dodaj iz Učilnice, sistem prikaže sporočilo o poteku dela, prikaže napako in obvesti študenta, da ni mogel dostopati do učilnice. Spodaj je gumb, kjer lahko študent znova poskusi komunikacijo.



### [MUST] Ogled posamezne aktivnosti

#### Povzetek funkcionalnosti

Študent ali API uporabnik lahko odpre in pregleda podatke posamezne aktivnosti na njegovem seznamu.

#### Osnovni tok

1. Študent odpre aplikacijo
2. Pritisne na **aktivnost v njegovem seznamu vseh aktivnosti**
3. Odpre se pogled, kjer so prikazani podatki o aktivnosti (naziv, opis, predvideno trajanje, poročilo dela)
4. Izbere **Zapri**
5. Sistem preusmeri študenta na stran z vsemi aktivnostimi

#### Alternativni tok 1

1. Študent odpre aplikacijo
2. Pritisne na **aktivnost v njegovem seznamu dnevnih aktivnosti**
3. Odpre se pogled, kjer so prikazani podatki o aktivnosti (naziv, opis, predvideno trajanje, poročilo dela)
4. Izbere **Zapri**
5. Sistem preusmeri študenta na stran z dnevnimi aktivnostimi



**Izjemni tokovi**

1. Študent dostopa do aktivnosti za katero nima dovoljenja

   ​	Sistem študentu napiše obvestilo in ga preusmeri na seznam vseh aktivnosti

2. Podatki o aktivnosti niso na voljo ali ne obstajajo

   ​	Sistem študentu napiše obvestilo in ga preusmeri na seznam vseh aktivnosti

#### Pogoji

- Študent mora biti prijavljen

#### Posledice

- Nobenih posledic

#### Posebnosti

- Pri API zahtevi se prijava preveri s funkcionalnostjo Prijava v sistem

#### Prioriteta: Must have

- Brez ogleda aktivnosti je sistem neuporaben

#### Sprejemni testi

- **O:** Prijavljen študent odpre aplikacijo, pritisne na aktivnost v seznamu vseh aktivnosti. Odpre se pogled, kjer lahko vidi vse podatke o izbrani aktivnosti (naziv, opis, predvideno trajanje, poročilo dela). Po kliku na gumb zapri sistem preusmeri študenta na stran z vsemi aktivnostimi
- **A1:** Prijavljen študent odpre aplikacijo, pritisne na aktivnost v seznamu dnevnih aktivnosti. Odpre se pogled, kjer lahko vidi vse podatke o izbrani aktivnosti (naziv, opis, predvideno trajanje, poročilo dela). Po kliku na gumb zapri sistem preusmeri študenta na stran z dnevnimi aktivnostimi
- **I1:** Študent naredi zahtevo za ogled aktivnosti, ki ni v njegovi lasti - dobi povezavo od aktivnosti drugega uporabnika. Sistem javi, da študent nima dostopa do te aktivnosti in ga preusmeri na seznam vseh aktivnosti.
- **I2:** Študent naredi zahtevo za ogled aktivnosti, ki jo je predhodno izbrisal ali ne obstaj. Sistem javi, da ta aktivnost ne obstaja in ga preusmeri na seznam vseh aktivnosti.



### [MUST] Pregled dnevnega seznama opravil

#### Povzetek funkcionalnosti

Študent ali API uporabnik lahko odpre seznam opravil, ki so predvidena za tisti dan.

#### Osnovni tok

1. Študent odpre aplikacijo
2. Pritisne na **dnevni seznam opravil**
3. Odpre se pogled, kjer so prikazane aktivnosti, ki so predvidene za tisti dan

**Izjemni tokovi**

1. Študent nima aktivnosti za tisti dan

   ​	Sistem študentu napiše obvestilo, da za ta dan nima predvidenih aktivnosti

#### Pogoji

- Študent mora biti prijavljen

#### Posledice

- Študen je na pogledu dnevni seznam opravil

#### Posebnosti

- Pri API zahtevi se prijava preveri s funkcionalnostjo Prijava v sistem

#### Prioriteta: Must have

- Brez ogleda dnevnih opravil je sistem skoraj neuporaben

#### Sprejemni testi

- **O:** Prijavljen študent odpre aplikacijo, pritisne na gumb dnenvi seznam opravil. Odpre se pogled, kjer lahko vidi vse aktivnosti predvidene za ta dan.
- **I1:** Študent izbere dan, na katerega nima nobenih aktivnosti. Sistem javi, da študent ta dan nima nobenih aktivnosti.



### [COULD] Pregled tedenskega, mesečnega koledarja

#### Povzetek funkcionalnosti

Študent ali API uporabnik lahko odpre koledarski pregled opravil ki mu prikazuje opravila za en teden ali za en mesec.

#### Osnovni tok

1. Študent odpre aplikacijo
2. Pritisne na **koledar**
3. Odpre se pogled, kjer so prikazane aktivnosti za tekoči teden
4. Študent pritisne na gumb **naprej** ali **nazaj** za prikaz naslednjih ali prejšnjih tednov

#### Alternativni tok 1

1. Študent odpre aplikacijo
2. Pritisne na **koledar**
3. Odpre se pogled, kjer so prikazane aktivnosti za tekoči teden
4. Pritisne na **mesečni pogled**
5. Odpre se pogled, kjer so prikazane aktivnosti za tekoči mesec
6. Študent pritisne na gumb **naprej** ali **nazaj** za prikaz naslednjih ali prejšnjih mesecev

**Izjemni tokovi**

1. Koledar ni na voljo

   ​	Sistem študentu napiše obvestilo, da trenutno njegov koledar ni na voljo

   

#### Pogoji

- Študent mora biti prijavljen

#### Posledice

- Študen je na pogledu ki mu prikazuje tedenski ali mesečni koledar

#### Posebnosti

- Koledar pridobljen preko integracije z Google Calendar
- Pri API zahtevi se prijava preveri s funkcionalnostjo Prijava v sistem

#### Prioriteta: Could have

- Uporabnik lahko še vedno normalno uporablja aplikacijo, tudi če ni te funkcionalnosti

#### Sprejemni testi

- **O:** Prijavljen študent odpre aplikacijo, pritisne na gumb koledar. Odpre se pogled, kjer lahko vidi vse aktivnosti predvidene za trenutni teden. Premakne se en teden naprej ali nazaj, kjer so prikazane aktivnosti tistega tedna.
- **A1**: Prijavljen študent odpre aplikacijo, pritisne na gumb koledar. Odpre se pogled, kjer lahko vidi vse aktivnosti predvidene za trenutni teden. Klikne na mesečni pogled, kjer lahko vidi vse aktivnosti predvidene za trenutni mesec. Premakne se en mesec naprej ali nazaj, kjer so prikazane aktivnosti tistega meseca.
- **I1:** Študent odpre koledar, ko ta ne deluje ali pa potekajo vzdrževalna dela. Sistem ga preusmeri nazaj in ga obvesti o nedosegljivosti koledarja.



### [SHOULD] Spreminjanje podatkov aktivnosti

#### Povzetek funkcionalnosti

Študent ali API uporabnik lahko odpre aktivnost in jo uredi.

#### Osnovni tok

1. Študent odpre aplikacijo
2. Pritisne na **aktivnost v njegovem seznamu aktivnosti**
3. Odpre se pogled, kjer so prikazani podatki o aktivnosti (naziv, opis, predvideno trajanje, poročilo dela)
4. Študent poljubno ureja podatke o aktivnosti
5. Izbere **Shrani**
6. Sistem zapiše spremembe v podatkovno bazo
7. Sistem obvesti študenta o uspešni spremembi podatkov
8. Sistem preusmeri študenta na stran z vsemi aktivnostimi

#### Alternativni tok 1

1. Študent odpre aplikacijo
2. Pritisne na **aktivnost v njegovem seznamu aktivnosti**
3. Odpre se pogled, kjer so prikazani podatki o aktivnosti (naziv, opis, predvideno trajanje, poročilo dela)
4. Študent poljubno ureja podatke o aktivnosti
5. Izbere **Zapri**
6. Sistem preusmeri študenta na stran z vsemi aktivnostimi

**Izjemni tokovi**

1. Študent ne izpolni vseh podatkov, ki so zahtevani

   ​	Sistem javi napako, in študenta obvesti, kateri podatki so obvezni

2. Študent izpolni datum v napačni obliki

   ​	Sistem javi napako, da je datum v napačni obliki.

   

#### Pogoji

- Študent mora biti prijavljen

#### Posledice

- Aktivnost, ki je bila urejana ima drugačne podatke

#### Posebnosti

- Pri API zahtevi se prijava preveri s funkcionalnostjo Prijava v sistem

#### Prioriteta: Should have

- Uporabnik lahko še vedno uporablja aplikacija brez te funkcionalnosti vendar mora za spremembo aktivnosti to izbrisati in nato ponovno dodati

#### Sprejemni testi

- **O:** Prijavljen študent odpre aplikacijo, pritisne na aktivnost, odpre se pogled, kjer lahko uredi ime aktivnosti in pričakovano število ur, ki jih bo porabil, da jo izpolni, uredi kategorijo aktivnosti, vnese datum, do kdaj mora aktivnost opraviti, pritisne gumb shrani, sistem javi, da je bila aktivnost uspešno popravljena, sistem preusmeri študenta na stran z vsemi aktivnostimi
- **A1:** Prijavljen študent odpre aplikacijo, pritisne na aktivnost, odpre se pogled, kjer lahko uredi ime aktivnosti in pričakovano število ur, ki jih bo porabil, da jo izpolni, uredi kategorijo aktivnosti, vnese datum, do kdaj mora aktivnost opraviti, pritisne gumb zapri, sistem javi, da spremembe niso bile zapisane, sistem preusmeri študenta na stran z vsemi aktivnostimi
- **I1:** Prijavljen študent odpre aplikacijo, pritisne na aktivnost, odpre se pogled, kjer lahko študent ureja vse podatke o aktivnosti. Študent pobriše eno polje, pritisne gumb Shrani. Sistem javi napako, in študenta obvesti, kateri podatki so obvezni.
- **I2:** Prijavljen študent odpre aplikacijo, pritisne na aktivnost, odpre se pogled, kjer lahko študent ureja vse podatke o aktivnosti. V polje za končni datum doda par podpičij ali drugih neprimernih znakov.  Pritisne gumb Shrani. Sistem javi napako, in študenta obvesti, kakšen je pravilen format.



### [SHOULD] Ocenjevanje težavnosti aktivnosti

#### Povzetek funkcionalnosti

Študent ali API uporabnik lahko oceni težavnost aktivnosti

#### Osnovni tok

1. Študent dodaja ali ureja aktivnost
2. Med prikazom podatkov aktivnosti je globalno povprečje ocene težavnosti trenutne aktivnosti.
3. Študent določi svojo oceno aktivnosti
4. Izbere **Shrani**
5. Sistem izračuna novo povprečje ocene težavnosti
6. Sistem obvesti študenta o uspešnem zapisu podatkov

**Izjemni tokovi**

1. Študent izpolni oceno v napačni obliki

   ​	Sistem javi napako, da je ocena v napačni obliki.

#### Pogoji

- Študent mora biti prijavljen

#### Posledice

- Aktivnost dobi drugo težavnost, popravljeno povprečje težavnosti se shrani v podatkovno bazo

#### Posebnosti

- Pri API zahtevi se prijava preveri s funkcionalnostjo Prijava v sistem

#### Prioriteta: Should have

- Uporabnik lahko še vedno uporablja aplikacijo vendar brez te funkcionalnosti ne vidi ocen težavnosti posameznih aktivnosti

#### Sprejemni testi

- **O:** Prijavljen študent odpre aplikacijo, izbere dodaj aktivnost ali uredi aktivnost. Nastavi svojo oceno težavnosti in pritisne shrani, sistem javi, da je bila aktivnost uspešno popravljena/dodana, sistem preusmeri študenta na stran z vsemi aktivnostimi
- **I1:** Prijavljen študent odpre aplikacijo, izbere dodaj aktivnost ali uredi aktivnost. Nastavi svojo oceno težavnosti ter doda nekaj neveljavnih znakov. Pritisne gumb Shrani. Sistem javi napako, in študenta obvesti, kakšen je pravilen format.



### [MUST] Vpis opravljenega dela za aktivnost

#### Povzetek funkcionalnosti

Študent ali API uporabnik ob zaključku aktivnosti vpiše koliko dela je dejansko opravil za zaključek aktivnosti

#### Osnovni tok

1. Študent odpre aplikacijo
2. Pritisne na **aktivnost v njegovem seznamu aktivnosti**
3. Odpre se pogled, kjer so prikazani podatki o aktivnosti (naziv, opis, predvideno trajanje, poročilo dela)
4. Izbere **Zakjluči**
5. Odpre se okno v katerega študent vpiše št. porabljenih ur za opravljanje aktivnosti.
6. Izbere **Potrdi**
7. Sistem zapiše spremembe v podatkovno bazo
8. Sistem obvesti študenta o uspešnem zaključku aktivnosti
9. Sistem preusmeri študenta na stran z vsemi aktivnostimi

**Izjemni tokovi**

1. Študent ne izpolni vseh podatkov, ki so zahtevani

   ​	Sistem javi napako, in študenta obvesti, kateri podatki so obvezni

2. Študent izpolni ure v napačni obliki

   ​	Sistem javi napako, da so ure v napačni obliki.

   

#### Pogoji

- Študent mora biti prijavljen

#### Posledice

- Aktivnost je zaključena

#### Posebnosti

- Pri API zahtevi se prijava preveri s funkcionalnostjo Prijava v sistem

#### Prioriteta: Must have

- Glavna ideja aplikacije ne deluje brez te funkcionalnosti

#### Sprejemni testi

- **O:** Prijavljen študent odpre aplikacijo, pritisne na aktivnost, odpre se pogled, kjer vidi vse podatke o aktivnosti, pritisne gumb zaključi. Odpre se okno v katero študent vnese število ur in pritisne gumb potrdi. Sistem javi, da je bila aktivnost uspešno zaključena, sistem preusmeri študenta na stran z vsemi aktivnostimi
- **I1:** Prijavljen študent odpre aplikacijo, pritisne na aktivnost, odpre se pogled, kjer študent vidi vse podatke o aktivnosti. Študent pritisne gumb Shrani, ne vpiše ur in pritisne gumb Potrdi. Sistem javi napako, in študenta obvesti, kateri podatki so obvezni.
- **I2:** Prijavljen študent odpre aplikacijo, pritisne na aktivnost, odpre se pogled, kjer študent vidi vse podatke o aktivnosti. Študent pritisne gumb Shrani, v ure vpiše številko in nekaj drugih neveljavnih znakov in pritisne gumb Potrdi. Sistem javi napako, in študenta obvesti, katere oblike morajo biti podatki.



### [WOULD] Samodejno posodabljanje podatkov aktivnosti

#### Povzetek funkcionalnosti

Sistem lahko periodično pregleduje aktivnosti iz študentskih sistemov in jih primerja z lokalnimi aktivnostmi. Če se razlikujejo, jih lahko posodobi.

#### Osnovni tok

1. Sistem zažene funkcijo za pregledovanje študentskih sistemov
2. Sprehodi se čez vse uporabnike
3. Za vsakega uporabnika prenese aktivnosti iz vseh študentskih sistemov, ki jih je uporabnik v preteklosti uporabil
4. Iz baze prenese vse aktivnosti študenta
5. Med seboj uskladi lokalne aktivnosti in aktivnosti, ki jih je prenesel iz študentskih sistemov.
6. Sprehodi se čez vse pare aktivnosti. Če se podatki paroma razlikujejo, prepiše lokalne podatke z oddaljenimi.

#### Izjemni tokovi:

1. Procedura se nekje ustavi zaradi napake

    ​	Procedura se izvaja v transakciji. Sistem povrne stanje v stanje pred spremembami. V sistemski dnevnik se izpiše sporočilo. Administratorju se na mail pošlje sporočilo o napaki.

#### Pogoji

- Uporabnik je omogočil samodejno posodabljanje aktivnosti

#### Posledice

- Podatki v sistemu so vedno skladni s podatki v oddaljenem sistemu.
- Študent je obveščen o spremembah rokov in opisov.

#### Posebnosti

- Potrebno bo pridobiti API dostop do Studisa in Učilnice ter shranjevati ključe za dostop.
- Potrebno bo razširiti funkcionalnost avtorizacije študentskega maila
- Če bo študentov veliko, bo procedura potrebovala veliko časa. Implementirati jo bo treba tako, da se bo lahko izvajala na več strežnikih hkrati. To za seboj potegne druge izzive.

**Prioriteta**: Would have. Te funkcionalnosti ne bomo implementirali.

#### Sprejemni testi

- **O:** Administrator se prijavi v sistem. Spremeni končni datum ene izmed aktivnosti. Ročno zažene posodabljanje podatkov iz študentskih sistemov. Končni datum aktivnosti je spet tak kot je bil na začetku.
- **I1:** Administrator zapre dostop do sistema, da ne pride do sprememb. Administrator se prijavi v bazo in naredi dump podatkov. Ročno zažene posodabljanje podatkov iz študentskih sistemov. Ubije proces procedure. Znova naredi dump podatkov in primerja datoteki. Datoteki se ujemata. 



### [MUST] Pregled in izbris uporabnikov

#### Povzetek funkcionalnosti

Administrator lahko pregleduje in briše določene uporabnike

#### Osnovni tok

1. Administrator odpre aplikacijo
2. Pritisne na **uporabniki**
3. Odpre se pogled, kjer je prikazan seznam vseh uporabnikov, čas njihove zadnje prijave in gumb za izbris
4. Izbere **Izbriši**
5. Odpre se okno ki ga vpraša če res želi izbrisati študenta
6. Izbere **Potrdi**
7. Sistem izbriše študenta in vse njegove aktivnosti iz baze
8. Sistem obvesti študenta preko e pošte o izbrisu njegovega računa s strani administratorja
9. Sistem preusmeri administratorja na stran s pregledom uporabnikov

**Izjemni tokovi**

1. Študent ne obstaja

   ​	Sistem javi napako, in administratorja obvesti o neuspelem izbrisu

   

#### Pogoji

- Administrator mora biti prijavljen

#### Posledice

- Študent je izbrisan

#### Posebnosti

- Pri API zahtevi se prijava preveri s funkcionalnostjo Prijava v sistem

#### Prioriteta: Must have

- Brez teh funkcionalnosti administrator ne more upravljati z uporabniki

#### Sprejemni testi

- **O:** Prijavljen administrator odpre aplikacijo, klikne uporabniki, izbere poljubnega študenta iz seznama in pri njem klikne izbriši. Sistem izpiše obvestilo, da je uporabnik uspešno izbrisan
- **I1:** Prijavljen administrator odpre aplikacijo, klikne uporabniki, izbere študenta, ki ga je v sosednjem zavihku že izbrisal, in pri njem klikne izbriši. Sistem izpiše obvestilo, da uporabni ni izbrisan, ker ne obstaja.



## 6. Nefunkcionalne zahteve

### Zahteve izdelka

1. *[Razširljivost]* **Sistem mora biti sposoben na enkrat podpirati vsaj 1000 uporabnikov**
	###### Opis: 
	Sistem mora biti sposoben na enkrat obdelati večje število uporabnikov. 
	###### Utemeljitev:
	Lahko pričakujemo veliko porast uporabe naše storitve med izpitnimi roki oz. v kolokvijskem obdobju. Porast lahko pričakujemo tudi ob koncu izpitnega obdobja oz. vsakič, ko bodo objavljeni rezultati preverjanja znanja.
	
2. *[Učinkovitost]* **Uporabniški vmesnik mora biti razumljiv, pregleden, obarvan glede na prioriteto in napisan na največ petih pogledih**
	###### Opis:
	Uporabniški vmesnik mora biti oblikovan na pregleden način z hitrim pregledom nad vsemi podatki. Elementi morajo biti ustrezno obarvani in pregledno razpostavljeni. Število pogledov mora biti čim manjše in ne sme presegati pet pogledov.
	###### Utemeljitev:
	Lahek pregled nad vsemi podatki je namen naše storitve, zato je učinkovitost naše storitve in želja po uporabi neposredno povezana z funkcionalnostjo hitrega dostopa do podatkov.
	
3. *[Učinkovitost]* **Sposoben mora biti opraviti vsaj 95% zahtev v pod treh sekundah**
	###### Opis:
	Spletna stran mora biti odzivna in delovati dovolj učinkovito da ne moti uporabnika med uporabo.
	
4. *[Zanesljivost]* **Programska oprema mora imeti vsaj 99,5% pravilno opravljenih operacij**
	###### Opis:
	Velika večina naših notranjih operacij mora biti izvedena brez zapletov.
	
5. *[Varnost]* **Uporabnik ne sme biti sposoben zahtevati podatke o drugih uporabnikih**
	###### Opis:
	Aplikacija ne sme imeti možnosti zahtevati podatkov o drugih uporabnikov prek vmesnika ali prek klicov API.
	###### Utemeljitev:
	Ta zahteva se navezuje na 2. zahtevo zunanjih zahtev.
	
6. *[Razpoložljivost]* **Vzdrževalni poseg ne sme trajati dlje od dveh ur in mora ob nedosegljivosti prikazati ustrezen razlog za nedosegljivost in podati okviren čas ko bo storitev spet operabilna**
	###### Opis:
	Vdrževanje ne sme pretirano zmotiti dela uporabnikov.
	
7. *[Izvedba]* **Spreminjanje oz. dodajanje prioritete ne sme trajati dlje od 0,5 sekunde in mora biti v tem času uporabniku ustrezno grafično prikazano**
	###### Opis:
	Zahteva po dodajanju in urejanju prioriteta naj bo hitra in ustrezna.
	###### Utemeljitev:
	Večja omejitev nebi omogočala brezhibne izkušnje in bi odvračala uporabnike od uporabljanja storitve. 
	

### Organizacijske zahteve
1. *[Razvojne zahteve]* **Za procesni razvojni sistem uporabite pristop Scrum**
	###### Opis:
	[Scrum](https://en.wikipedia.org/wiki/Scrum_(software_development)) je agilno ogrodje za uporavljanje s časom in delovanjem ekipe. Narejena je za ekipe velikosti treh do devet članov, ki razbijejo delo na časovna obdobja, ki se jih reče sprint. En sprint ne sme trajati dlje od enega meseca in je običajno dolg okoli dveh tednov. Napredku se sledi s pomočjo dnevnih kratkih sestankov, ki trajajo okoli petnajst minut in olajšajo organizacijo projekta.
	
2. *[Zahteve okolja]* **Uporabniki sistema se morajo prijaviti z e-poštnim naslovom in geslom, ki ju uporabljajo za dostop do spletne učilnice**
	###### Opis:
	Za dostop do naše aplikacije morajo uporabniki uporabiti e-poštni naslov in geslo, ki so ju dobili od univerze in s katerimi dostopajo do svoje e-pošte in učilnice.
	###### Utemeljitev:
	Ta pristop nam omogoča, da brez težav preverimo veljavnost prijave in nam olajša pridobivanje podatkov.

### Zunanje zahteve
1. *[Etične zahteve]* **Sistem ne sme vračati kakršnih koli podatkov o drugih uporabnikih**
	###### Opis:
	Sistem mora varovati podatke o ocenah, rokih, nalogah, urnikih, ipd. uporabnikov, saj jih morda niso pripravljeni deliti z drugimi.
	###### Utemeljitev:
	Deljenje takih podatkov lahko negativno vpliva na druge vidike življenja uporabnika.
	
2. *[Zakonodajne zahteve]* **Sistem mora slediti splošni uredbi o varstvu podatkov**
	###### Opis:
	[GDPR](https://eur-lex.europa.eu/legal-content/SL/TXT/HTML/?uri=CELEX:32016R0679&from=en) je uredba Evropske unije, ki definira zahteve o varovanju osebnih podatkov znotraj Evropske Unije.

## 7. Prototipi vmesnikov

### Vmesniki do zunanjih sistemov
	
##### Klic na sistem STUDIS
Sistem studis bi moral ponujati klic funkcije, ki sprejme vpisno številko in nam z API klicom vrne objekte, ki vsebujejo podatke podane spodaj, v obliki CSV, ločene z vejico. 
Ti objekti bi predstavljali razpisane izpitne roke.
- Naziv predmeta
- Zaporedna številka, ki označuje kateri v katerem roku poteka (1. 2. ali 3.)
- Datum izpita
- Razpisana ura
- Prostor v katerem poteka izpit
- Izvajalec premeta
	
##### Klic na sistem e-učilnica
Sistem e-učilnice bi moral ponujati funkcijo, ki z API klicom vrne objekte, ki vsebujejo podatke podane spodaj, v obliki .CSV, ločene z vejico.
Ti objekti bi predstavljali razna preverjanja, domače naloge, kolokvije, itd.
- Naziv aktivnosti
- Datum odprtja aktivnosti
- Datum zapadlosti aktivnosti
- Ura zapadlosti aktivnosti
- Naziv predmeta
- Opis aktivnosti
	
##### Klic na Gooogle Calendar
Klic na sistem Google Calendar bi nam omogočal izvoz vseh naših aktivnosti v njihovo storitev (podobno, kot je to mogoče v sistemu STUDIS)
Uporabili bi sledeči [klic na Googlove storitve](https://developers.google.com/calendar/v3/reference/events/import) s pomočjo datotek .CSV.

### Mock up

##### Prijavni zaslon
![Prijavni zaslon](../img/Project_Mockup-1.png)
##### Registracija
![Registracija](../img/Project_Mockup-2.png)
##### Pregled vseh aktivnosti
![Pregled vseh aktivnosti](../img/Project_Mockup-3.png)
##### Dnevni pogled v koledarju
![Dnevni pogled v koledarju](../img/Project_Mockup-4.png)
##### Tedenski pogled v koledarju
![Tedenski pogled v koledarju](../img/Project_Mockup-5.png)
##### Mesečni pogled v koledarju
![Mesečni pogled v koledarju](../img/Project_Mockup-6.png)
##### Dodajanje nove aktivnosti
![Dodajanje nove aktivnosti](../img/Project_Mockup-7.png)
##### Pregled/urejanje aktivnosti
![Pregled/urejanje aktivnosti](../img/Project_Mockup-8.png)
##### Neobvezni vnos dodatnih ur pri končavanju aktivnosti
![Neobvezni vnos dodatnih ur pri končavanju aktivnosti](../img/Project_Mockup-9.png)
