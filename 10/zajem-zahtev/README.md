# Dokument zahtev

| **Naziv projekta** | StraightAs |
|:---|:---|
| **Člani projektne skupine** | Vid Rijavec, Emir Hasanbegović, Andrej Martinovič, Aljaž Markovič |
| **Kraj in datum** | Ljubljana, April 2019 |

## Povzetek projekta

Spletna aplikacija StraightAs bo poenostavila organizacijo vsakodnevnih aktivnosti študentov in jim omogočala lažje in bolj natančno sledenje dogodkom. Interakcija uporabnikov s sistemom je razdeljena na 4 vloge: neregistrirani uporabnik, registrirani uporabnik, urednik in administrator. Uporabniki se bodo lahko registrirali, logirali in urejali svoj urnik, uvoziti pa bo možno tudi dogodke iz drugih aplikacij ter urnike inštitucij - te bodo upravljali in dodajali uredniki. Najprej bodo razvite funkcionalnosti ročne interakcije s sistemom, kasneje pa bodo dodani bolj kompleksni sistemi za uvoz podatkov. Sistem zahteva visoko zanesljivost in dobro odzivnost, kar bo doseženo z uporabo MEAN sklada tehnologij. Robustnost zalednega sistema bo zagotovljena z varnostnimi kopijami, dodajanje nove izvorne kode pa bo potekalo s pomočjo sprejemnih in integracijskih testov, kar pripomore k pravilnosti implementacije.

## 1. Uvod
Problemska domena je preobremenjenost študentov, ki so pod stresom, saj si morajo zapomniti veliko različnih terminov študijskih in obštudijskih obveznosti. Poleg tega že obstajajo spletne storitve, ki omogočajo pregled in urejanje dogododkov, vendar ti žal niso zbrani na enem mestu. 
Namen aplikacije StraightAs je torej zbiranje vseh dogodkov študentov na eno mesto. Tako bi študenti imeli lažji pregled nad svojim urnikom in si s tem svoj čas učinkovitejše organizirali. Aplikacija bi temu primerno dala možnost uporavljanja nad dogodki (dodajanje in brisanje ter pregled nad dogodki, prijava na javen dogodek, razvrščanje dogodov po prioriteti, spreminjanje terminov izbranih dogodkov). Kot vemo obstaja veliko aplikacij, ki omenjene funkcionalnosti že omogočajo, zato smo se odločili, da bi uporabnikom omogočili tudi avtomatski uvoz že obstoječih urnikov. Prav tako bodo poleg študentov uporabniki aplikacije tudi uredniki in administratorji. Uredniki bodo imeli pravico do uvoza urnikov inštitucij in možnost dodajanja javnih dogodkov. Vlogo urednika bo registriranim uporabnikom dodelil administrator, ki bo imel pregled nad vsemi uporabniki in dostop do vseh funkcionalnosti aplikacije. Neregistriranim uporabnikom bo na voljo dostop do javnih dogodkov ,prijava s pomočjo socialnih omrežji in registracija. 

## 2. Uporabniške vloge

* **neregistrirani uporabnik** (dostop do registracije, vpogled v uporabnost spletne storitve, dostop do javnega koledarja in novic)
* **registrirani uporabnik** (lahko ureja svoj urnik in uvaža podatke preko APIjev za urnike)
* **urednik** (ureja uvoz urnikov in dogodkov, ki jih lahko registrirani uporabniki uporabljajo)
* **administrator** (ima dostop do vsega, vzdržuje sistem, rešuje probleme)

## 3. Slovar pojmov

**ZVOP-1-UPB1:** Zakon o varstvu osebnih podatkov (uradno prečiščeno besedilo).

**MEAN:** Sklad tehnologij, ki ga sestavlja podatkovna baza MongoDB, ogrodje ExpressJS, NodeJS in AngularJS.

**MVC:**  Arhitekturni stil, ki ga sestavljajo model, pogled in krmilnik.

**Captcha:** Program, katerega namen je ločiti uporabnika od računalnika. Njegov namen je ščititi spletne strani.

**COBIT/ISACA:** Ogrodje, ki ga je naredilo podjetje ISACA, katerega namen je definirati generične procese za vodenje IT.

**Črna škatla:** Izraz za sistem, za katerega ne poznamo notranjo zgradbo (uporablja se lahko tudi za testiranje).

**Registriran uporabnik:** Študent

## 4. Diagram primerov uporabe

![usecase](../img/usecase.png)

## 5. Funkcionalne zahteve

| **Naziv zahteve** | Registracija |
| :-------------------- | :------------------------------- |
| **Povzetek funkcionalnosti** | Neregistrirani uporabnik se lahko registrira v sistem, njegovi podatki se dodajo v podatkovno bazo. |
|**Osnovni tok** |
|1.|Neregistrirani uporabnik izbere funkcionalnost Registracija.
|2.|Sistem prikaže seznam zahtevanih podatkovnih polj.
|3.|Neregistrirani uporabnik v celoti izpolni polja.
|4.|Neregistrirani uporabnik potrdi svojo izbiro.
|5.|Sistem prikaže sporocilo o uspešni registraciji.
|**Alternativni tok** |
|1.|Administrator izbere funkcionalnost Dodaj novega uporabnika.
|2.|Sistem prikaže seznam zahtevanih podatkovnih polj.
|3.|Administrator v celoti izpolni polja.
|4.|Administrator potrdi svojo izbiro.
|5.|Sistem prikaže sporocilo o uspešni registraciji novega uporabnika.
| **Izjemni tok** |  |
| |Neregistrirani uporabnik je poskusil uporabiti racun za e-pošto, ki je že registriran v podatkovni bazi. Sistem prikaže ustrezno obvestilo.
| |Neregistrirani uporabnik je neustrezno izpolnil vnosni obrazec. Sistem prikaže ustrezno obvestilo.
| **Pogoji** |  |
| |Uporabnik ni prijavljen v sistem.
| **Posledice** | Neregistrirani uporabnik pridobi naziv Registrirani uporabnik in vse njegove funkcionalnosti.|
| **Posebnosti** | Vnosna polja imajo sintakticno in semanticno vrednost in morajo biti natancno dolocena ter ustrezajo predhodno dolocenemu standardu. |

------

| **Naziv zahteve** | Prijava |
| :-------------------- | :------------------------------- |
| **Povzetek funkcionalnosti** | Registrirani uporabnik se lahko prijavi v sistem in v tej seji pridobi dostop do sistema. |
|**Osnovni tok** |
|1.|Registrirani uporabnik izbere funkcionalnost Prijava. 
|2.|Sistem prikaže zahtevani podatkovni polji za e-naslov in geslo.
|3.|Registrirani uporabnik v celoti izpolni polja.
|4.|Registrirani uporabnik potrdi svojo izbiro.
|5.|Sistem prikaže sporocilo o uspešni prijavi.
| **Alternativni tok** |  |
|1. | Neregistrirani uporabnik izbere funkcionalnost Prijava, s pomocjo socialnega omrežja.
|2. | Neregistrirani uporabnik izpolni vnosna polja.
|3. | Sistem prikaže sporocilo o uspešni prijavi.
| **Izjemni tok** |  |
| |Registrirani uporabnik je poskusil uporabiti racun za e-pošto, ki še ni registriran v podatkovni bazi. Sistem prikaže ustrezno obvestilo.
| |Registrirani uporabnik je vpisal neustrezno geslo ali e-naslov. Sistem prikaže ustrezno obvestilo.
| **Pogoji** |  |
| |Uporabnik ni prijavljen v sistem.
| |Uporabnik mora izpolniti Captcha sistem.
| **Posledice** | Registrirani uporabnik v tej seji pridobi dostop do sistema.|
| **Posebnosti** | Vnosno polje za geslo je potrebno enkriptirati. |

------



| **Naziv zahteve** | Dodajanje novega urednika |
| :-------------------- | :------------------------------- |
| **Povzetek funkcionalnosti** | Administrator ima pravico dodajanja novih urednikov v sistem. S tem posodobi pravice dolocenega registriranega uporabnika, da lahko dodaja nove urnike in dogodke v sistem. |
|**Osnovni tok** |
|1.|Administrator izbere izbere funkcionalnost Dodajanje novega urednika.
|2.|Sistem prikaže seznam registriranih uporabnikov.
|3.|Administrator uporabi funkcijo iskanja za izbiro željenega registiranega uporabnika.
|4.|Sistem posodobi podatkovno bazo in prikaže sporocilo o uspešni transakciji.
| **Izjemni tok** |  |
| |Registrirani uporabnik je že urednik, zato ponovno dodajanje pravic ne uspe. Sistem prikaže ustrezno obvestilo.
| **Pogoji** |  |
| |Novega urednika lahko doda le Administrator.
| **Posledice** | Registrirani uporabnik pridobi naziv Urednik in vse njegove funkcionalnosti.|

-----

| **Naziv zahteve** | Dodajanje dogodka v sistem |
| :-------------------- | :------------------------------- |
| **Povzetek funkcionalnosti** | Urednik doda nov dogodek v sistem, ki je javno viden vsem. |
|**Osnovni tok** |
|1.|Urednik izbere funkcionalnost Dodajanje dogodkov v sistem. 
|2.|Sistem prikaže zahtevana vnosna polja, ki jih mora urednik ustrezno izpolniti.
|3.|Urednik ustrezno izpolni vsa vnosna polja.
|4.|Urednik potrdi dodajanje dogodka.
|5.|Sistem prikaže sporocilo o uspešni prijavi.
|6.|Sistem javno prikaže novo dodani dogodek, do katerega lahko dostopajo vsi.
| **Izjemni tok** |  |
| |Urednik je neustrezno izbral datum. Dogodek se je zgodil v preteklosti.
| |Urednik je pomankljivo izpolnil zahtevana vnosna polja, pri cemer ga sistem opozori.
| |Urednik je želel ustvariti dogodek, ki je predhodno že bil dodan v sistem.
| **Pogoji** |  |
| |Oseba mora biti prijavljena v sistem in imeti pravice tipa Urednik.
| |Dogodek ni bil predhodno dodan v sistem.
| **Posledice** | Dogodek je javno objavljen in je dostopen s strani vseh uporabnikov, pri cemer se lahko vsi registrirani uporabniki nanj prijavijo.|

-----

| **Naziv zahteve** | Dodajanje urnika za inštitucijo |
| :-------------------- | :------------------------------- |
| **Povzetek funkcionalnosti** | Urednik lahko doda celotni javni urnik za neko inštitucijo, npr. urnik predavanj in vaj vsakega predmeta na fakulteti. |
|**Osnovni tok** |
|1.|Urednik izbere funkcionalnost Dodajanje urnika za inštitucijo. 
|2.|Sistem prikaže izbiro rocnega dodajanja urnika ali avtomatski uvoz.
|3.|Urednik izbere možnost 'rocno dodajanje urnika'.
|4.|Urednik ustrezno doda dogodke na tedenski urnik.
|5.|Urednik potrdi dodajanje urnika.
|6.|Sistem prikaže sporocilo o uspešnem dodajanju urnika.
|7.|Sistem javno prikaže nov urnik, ki ga lahko registrirani uporabniki dodajo v svoj urnik in ga spreminjajo.
|**Alternativni tok** |
|1.|Urednik izbere funkcionalnost Dodajanje urnika za inštitucijo. 
|2.|Sistem prikaže izbiro rocnega dodajanja urnika ali avtomatski uvoz.
|3.|Urednik izbere možnost 'avtomatski uvoz'.
|4.|Sistem uvozi podatke z izbrane inštitucije.
|5.|Urednik pregleda in popravi avtomatsko uvožene podatke.
|6.|Urednik potrdi dodajanje urnika.
|7.|Sistem prikaže sporocilo o uspešnem dodajanju urnika.
|8.|Sistem javno prikaže nov urnik, ki ga lahko registrirani uporabniki dodajo v svoj urnik in ga spreminjajo.
| **Izjemni tok** | |
| |Urednik je poskusil uporabiti avtomatski uvoz za inštitucijo, ki jo sistem ne podpira. Sistem prikaže ustrezno obvestilo in preusmeri urednika na rocno dodajanje urnika.
| **Pogoji** |  |
| |Oseba mora biti prijavljena v sistem in imeti pravice tipa Urednik.
| **Posledice** | Urnik je javno objavljen in dostopen s strani vseh uporabnikov, pri cemer ga lahko  registrirani uporabniki dodajo na svoj urnik in ga spreminjajo.|

------

| **Naziv zahteve** | Prijava na dogodek |
| :-------------------- | :------------------------------- |
| **Povzetek funkcionalnosti** | Registrirani uporabnik se lahko prijavi na dogodek. Dogodek je dodan na njegov urnik. |
|**Osnovni tok** |
|1.|Registrirani uporabnik izbere funkcionalnost Prijava na dogodek. 
|2.|Sistem registriranemu uporabniku doda dogodek na njegov urnik.
|3.|Sistem prikaže sporocilo o uspešni prijavi na dogodek.
| **Izjemni tok** |  |
| |Registrirani uporabnik se želi prijaviti na dogodek, ki ne obstaja vec.
| |Registrirani uporabnik se želi prijaviti na dogodek, ki se je zgodil v preteklosti.
| **Pogoji** |  |
| |Dogodek mora biti predhodno dodan v sistem.
| |Registrirani uporabnik ni že prijavljen na dogodek.
| **Posledice** | Registriranemu uporabniku se doda dogodek na urnik. V sistemu se zabeleži njegova prijava na dogodek. |
| **Posebnosti** | Dogodek mora imeti vsaj eno prosto mesto, da se lahko nanj Urednik ali Registrirani uporabnik prijavi. |

 ------

| **Naziv zahteve** | Razvršcanje dogodkov po prioriteti|
| :-------------------- | :------------------------------- |
| **Povzetek funkcionalnosti** | Registrirani uporabnik lahko razvrsti svoje dogodke po prioriteti. S tem se spremenijo podatki izbranih dogodkov v podatkovni bazi. |
|**Osnovni tok** |
|1.|Registrirani uporabnik izbere funkcionalnost Razvršcanje dogodkov po prioriteti. 
|2.|Sistem prikaže uporabnikove dogodke in omogoci njihovo urejanje.
|3.|Registrirani uporabnik oznaci želen dogodek.
|4.|Registrirani uporabnik izbere prioriteto za oznacen dogodek.
|5.|Registrirani uporabnik potrdi svojo izbiro.
|6.|Sistem prikaže dogodke v ustreznih barvah.
| **Izjemni tok** |  |
| | Registirani uporabnik dodeli oznacemu dogodku prioriteto, ki je enaka obstojeci. Sistem prikaže ustrezno obvestilo.
| **Pogoji** |  |
| |Uporabnik je prijavljen v sistem.
| |Dogodek ne sme preteci
| **Posledice** | Registriranemu uporabniku se dogodki ustrezno obarvajo.|


------

| **Naziv zahteve** | Urejanje terminov dogodkov|
| :-------------------- | :------------------------------- |
| **Povzetek funkcionalnosti** | Registrirani uporabnik lahko spremeni termine izbranih dogodkov. S tem se spremenijo ustrezni podatki v podatkovni bazi. |
|**Osnovni tok** |
|1.|Registrirani uporabnik izbere funkcionalnost Urejanje terminov dogodkov. 
|2.|Sistem prikaže uporabnikove dogodke in omogoci njihovo urejanje.
|3.|Registrirani uporabnik oznaci dogodek kateremu želi spremeniti termin.
|4.|Registrirani uporabnik izpolni vnosna polja.
|5.|Sistem prikaže ponovno prikaže uporabnikove dogodke.
| **Izjemni tok** |  |
| |Registirani uporabnik dodeli oznacemu dogodku termin, ki je enak obstojecemu. Sistem prikaže ustrezno obvestilo.
| **Pogoji** |  |
| |Uporabnik je prijavljen v sistem.
| |Dogodek ni že pretekel.
| **Posledice** | Registriranemu uporabniku se dogodki ponovno razvrstijo po casu.|

------

| **Naziv zahteve** |Brisanje dogodkov|
| :-------------------- | :------------------------------- |
| **Povzetek funkcionalnosti** | Registrirani uporabnik lahko odstrani dogodkek iz svojega urnika. Temu ustrezno se posodobijo podatki v podatkovni bazi. |
|**Osnovni tok** |
|1.|Registrirani uporabnik na dogodku izbere funkcionalnost Izbris. 
|2.|Sistem prikaže pojavitveno okno za potrditev izbrisa.
|3.|Registrirani uporabnik potrdi izbris.
|4.|Sistem ponovno prikaže uporabnikove dogodke.
|**Alternativni tok** |
|1.|Registrirani uporabnik izbere funkcionalnost Brisanje dogodkov. 
|2.|Sistem na vsakem dogodku prikaže potditveno polje za oznacbo izbrisa.
|3.|Registrirani uporabnik izbere dogodke, ki jih želi izbrisati.
|4.|Registrirani uporabnik potrdi izbris.
|5.|Sistem ponovno prikaže uporabnikove dogodke.
| **Pogoji** |  |
| |Uporabnik je prijavljen v sistem.
| **Posledice** | Registriranemu uporabniku se prikaže urnik brez izbrisanega dogodka.| 


------

| **Naziv zahteve** | Uvoz urnikov iz drugih urniških aplikacij |
| :-------------------- | :------------------------------- |
| **Povzetek funkcionalnosti** | Registrirani uporabnik lahko v svoj urnik uvozi dogodke iz obstojecih urniških aplikacij in jih spreminja. |
|**Osnovni tok** |
|1.|Registrirani uporabnik izbere funkcionalnost Uvoz urnikov iz drugih urniških aplikacij. 
|2.|Sistem prikaže vse podprte urniške aplikacije.
|3.|Registrirani uporabnik oznaci željeno aplikacijo.
|4.|Registrirani uporabnik se prijavi v izbrano aplikacijo.
|5.|Sistem uvozi dogodke iz izbrane aplikacije.
|6.|Registrirani uporabnik pregleda in popravi uvožene podatke.
|7.|Sistem prikaže okno za potdilo uvoza.
|8.|Registrirani uporabnik potrdi svojo izbiro.
|9.|Sistem prikaže nove dogodke na urniku.
| **Izjemni tok** |  |
| | Uvoz podatkov iz izbrane aplikacije ne uspe. Sistem prikaže ustrezno obvestilo.
| **Pogoji** |  |
| |Uporabnik je prijavljen v sistem.
| **Posledice** | Registriranemu uporabniku se na urnik dodajo dogodki iz druge urniške aplikacije.|
| **Posebnosti** | Vsaka aplikacija ima svoj nacin shranjevanja podatkov, zato je potrebna locena implementacija za vsako. |

------

#### Prioritete identificiranih funkcionalnosti

**Must have**:

* Prijava
* Registracija
* Dodajanje dogodka v sistem
* Brisanje dogodkov
* Urejanje terminov dogodkov

**Should have**:

* Prijava na dogodek
* Dodajanje novega urednika

**Could have**:

* Dodajanje urnika za inštitucijo
* Razvršcanje dogodkov po prioriteti

**Would have**:

* Uvoz urnikov iz drugih urniških aplikacij


#### Sprejemni testi

| **Funcionalnost** | **Zacetno stanje sistema** | **Vhod** | **Izhod** |
| :---------------- |  :------------------------ | :------- | :-------- |
| Registracija | Neregistrirani uporabnik se želi registrirati, zato se nahaja na strani za registracijo. | Uporabnik izpolni obrazec za registracijo.  | Sistem ustrezno posodobi podatkovno bazo in obvesti uporabnika o uspešni registraciji. Neregistrirani uporabnik pridobi funkcionalnosti aplikacije, ki pripadajo registriranemu. |
| Registracija | Administrator želi dodati novega uporabnika, zato izbere funkcionalnost Dodaj novega uporabnika. | Administrator izpolni vnosna polja, ki mu jih je sistem ponudil. | Sistem ustrezno posodobi podatkovno bazo. Dodan uporabnik pridobi naziv 'registrirani uporabnik'. Sistem administratorja obvesti o uspešni registraciji novega uporabnika.    |
| Registracija | Neregistrirani uporabnik se želi registrirati, zato se nahaja na strani za registracijo. V primeru da registracijo želi izvesti Administrator, mora ta izbrati funkcionalnost Dodaj novega uporabnika. | Neregistrirani uporabnik ali administrator izpolni obrazec za registracijo vendar pri vnosu zahtevanih polj uporabi obstojeci e-mail naslov.| Sistem obvesti uporabnika o neuspešni registraciji in mu ponudi možnost ponovnega vnosa polj za registracijo. |
| Prijava | Registrirani uporabnik se želi prijaviti, zato se nahaja na strani za prijavo. | Registrirani uporabnik pravilno izpolni obrazec za prijavo. | Sistem prikaže sporocilo o uspešni prijavi in uporabnika presmeri na zacetno stran aplikacije.  |
| Prijava | Neregistrirani uporabnik se želi prijaviti s pomocjo obstojecega racuna za izbrano socialno omrežje, zato na strani za prijavo izbere to možnost. | Uporabnik izpolni obrazec za prijavo prek socialnega omrežja.  | Sistem prikaže sporocilo o uspešni prijavi in uporabnika preusmeri na zacetno stran aplikacije. |
|Prijava|Registrirani ali neregistrirani uporabnik se želi prijaviti v sistem, zato se nahaja na strani za prijavo.| Registrirani ali neregistrirani uporabnik vstavi neustrezno geslo ali e-poštni naslov. | Sistem prikaže sporocilo o neuspešni prijavi in ponudi možnost ponovnega vnosa polj za prijavo.| 
|Dodajanje novega urednika|Administrator želi dodati v sistem novega urednika, zato izbere funkcionalnost Dodajanje novega urednika.| Administrator iz seznama registriranih uporabnikov izbere tistega, ki mu želi dodati pravice urednika. | Sistem prikaže sporocilo o uspešnem dodajanju urednika. Registrirani uporabnik pridobi naziv Urednik in vse njegove funkcionalnosti.|
|Dodajanje novega urednika|Administrator želi dodati v sistem novega urednika, zato izbere funkcionalnost Dodajanje novega urednika.| Administrator iz seznama registriranih uporabnikov izbere tistega, ki mu želi dodati pravice urednika, vendar ta že ima vlogo urednika. | Sistem prikaže sporocilo o neuspešnem dodajanju urednika in mu ponovno ponudi možnost za izbiro urednika.|
|Dodajanje dogodka v sistem| Urednik želi dodati nov dogodek v sistem, ki bo javno viden vsem, zato izbere funkcionalnost Dodajanje dogodkov v sistem. | Sistem poda uredniku vnosna polja, ki jih ta pravilno izpolni. | Ko urednik izpolni vnosna polja in jih sistem preveri, se prikaže pojavno okno, ki urednika obvesti o uspešnem vnosu dogodka. Aplikacija nato javno prikaže novo dodani dogodek, do katerega lahko dostopajo vsi. Prav tako se lahko vsi registrirani uporabniki prijavijo nanj.|
|Dodajanje dogodka v sistem| Urednik si želi dodati nov dogodek, ki bo javno viden vsem, zato izbere funkcionalnost Dodajanje dogodkov v sistem.|Sistem poda uredniku vnosna polja, ki jih ta nepravilno izpolni. Dogodek se je že zgodil v preteklosti ali pa je urednik pomanjkljivo izpolnil vnosni obrazec.| Sistem ob napaki ustrezno opozori urednika in mu ponudi možnost za ponovno dodajanje dogodka v sistem.|
|Dodajanje urnika za inštitucijo| Urednik si želi dodati urnik za doloceno inštitucijo. Temu primerno izbere funkcionalnost Dodajanje urnika za inštitucijo.|Ob izbiri rocnega dodajanja urnika urednik ustrezno doda dogodke na tedenski urnik. | Sistem prikaže sporocilo o uspešnem dodajanju urnika. Poleg tega ga javno prikaže. Registrirani uporabniki ga lahko dodajo v svoj urnik in spreminjajo.|
|Dodajanje urnika za inštitucijo|Urednik si želi dodati nov urnik za doloceno inštitucijo, zato izbere funkcionalnost Dodajanje urnika za inštitucijo.| Ob izbiri avtomatskega uvoza sistem prebere javno dostopne podatke urnika izbrane inštitucije.| Sistem prikaže sporocilo o uspešnem dodajanju urnika.  Poleg tega ga javno prikaže. Registrirani uporabniki ga lahko dodajo v svoj urnik in spreminjajo.|
|Dodajanje urnika za inštitucijo|Urednik si želi dodati nov urnik za doloceno inštitucijo, zato izbere funkcionalnost Dodajanje urnika za inštitucijo.|Ob izbiri avtomatskega uvoza sistem ne podpira avtomatskega uvoza za doloceno inštitucijo.|Sistem prikaže ustrezno obvestilo in preusmeri urednika na rocno dodajanje urnika.|
|Prijava na dogodek|Registrirani uporabnik se želi prijaviti na izbran dogodek, zato izbere funkcionalnost Prijava na dogodek.|Izmed dogodkov, ki so javno dostopni, registrirani uporabnik oznaci tistega na katerega se ta želi prijaviti.| Sistem registriranemu uporabniku doda dogodek na urnik.|
|Prijava na dogodek|Registrirani uporabnik se želi prijaviti na izbran dogodek, zato izbere funkcionalnost Prijava na dogodek|Izmed dogodkov, ki so javno dostopni registrirani uporabnik oznaci tistega na katerega se ta želi prijavi. Ob tem se izkaže, da ni prostih mest ali da se je izbran dogodek zgodil že v preteklosti.| Sistem uporabnika ustrezno obvesti o napaki in mu ponudi možnost ponovne prijave na dogodek. |
|Razvršcanje dogodkov po prioriteti| Registrirani uporabnik želi dogodke razvrstiti po prioriteti, zato izbere funkcionalnost Razvršcanje dogodkov po prioriteti.| Sistem uporabniku ponudi njegove dogodke in omogoci njihovo urejanje. Uporabnik izbere dogodek in oznaci želeno prioriteto.| Sistem uporabnika preusmeri na stran, kjer se prikažejo vsi uporabnikovi dogodki. Posamezen dogodek se glede na priroriteto oznaci z ustrezno barvo.|
|Razvršcanje dogodkov po prioriteti|Registrirani uporabnik si želi dogodke razvrstiti po prioriteti, zato izbere funkcionalnost Razvršcanje dogodkov po prioriteti.| Sistem uporabniku ponudi njegove dogodke in omogoci njihove urejanje. Uporabnik nato oznaci dogodek kateremu želi spremeniti prioriteto, vendar se ob tem izkaže, da je izbrana prioriteta enaka obstojeci. Do napake lahko pride tudi ce je izbran dogodek že pretekel.| Sistem uporabnika ustrezno obvesti o napaki in mu ponudi ponovno možnost izbire prioritete.|
|Urejanje terminov dogodkov| Registrirani uporabnik si želi urediti termine dogodkov, zato izbere funkcionalnost Urejanje terminov dogodkov.| Sistem ponudi uporabniku njegove dogodke in mu omogoci urejanje njihovih terminov. Uporabnik izbere dogodek kateremu želi spremeniti termin in uspešno izpolni vnosna polja.| Sistem uporabnika obvesti o uspešnem urejanju termina in ga ponovno preusmeri na stran, kjer si lahko ogleda svoj posodobljen urnik.|
|Urejanje terminov dogodkov| Registrirani uporabnik si želi urediti termine dogodkov, zato izbere funkcionalnost Urejanje terminov dogodkov.| Sistem ponudi uporabniku njegove dogodke in mu omogoci urejanje njihovih terminov. Uporabnik izbere dogodek kateremu želi spremeniti termin, a se izkaže, da je ta že pretekel.| Sistem uporabnika obvesti o neuspešni posodobitvi termina in mu ponudi možnost ponovne izbire ustreznega dogodka.|
|Brisanje dogodkov|Registrirani uporabnik si želi odstraniti dogodek iz svojega urnika, zato izbere funkcionalnost Izbris.|Sistem ponudi uporabniku možnost izbire dogodka, ki ga želi izbrisati. Uporabnik nato oznaci dogodek, ki ga želi izbrisati in potrdi svojo izbiro.| Sistem uporabnika obvesti o uspešnem izbrisu dogodka in ga preusmeri na posodobljen urnik.|
|Brisanje dogodkov| Registrirani uporabnik želi odstraniti dogodek iz svojega urnika, zato izbere funkcionalnost Izbris, ki omogoca brisanje vecih dogodkov.| Sistem ponudi uporabniku možnost izbire dogodkov, ki jih želi izbrisati, tako da na posamezen dogodek pripne potrditveno polje za oznacbo izbrisa.| Sistem uporabnika obvesti o uspešnem izbrisu vecih dogodkov in ga preusmeri na posodbljen urnik.|
|Uvoz urnikov iz drugih urniških aplikacij| Registrirani uporabnik si želi aplikacijo integrirati z obstojecimi aplikacijami, ki nudijo možnost predogleda in urejanje urnikov, zato izbere funkcionalnost Uvoz urnikov iz drugih urniških aplikacij.| Sistem uporabniku ponudi možnost izbire podprtih urniških aplikacij. Uporabnik nato izbere željeno aplikacijo. Naša aplikacija nato uspešno prebere podatke izbrane aplikacije| Sistem uporabnika obvesti o uspešnem uvozu urnika in ga preusmeri  na stran, kjer lahko pregleda in popravi uvožene podatke.|
|Uvoz urnikov iz drugih urniških aplikacij| Registrirani uporabnik si želi aplikacijo integrirati z obstojecimi aplikacijami, ki ponujajo možnost predogleda in urejanje urnikov, zato izbere funkcionalnost Uvoz urnikov iz drugih urniških aplikacij.|Sistem uporabniku ponudi možnost izbire podprtih urniških aplikacij. Uporabnik nato izbere željeno aplikacij, vendar pri branju podatkov pride do nepricakovanih napak.| Sistem uporabnika obvesti o napaki in ga preusmeri na stran, kjer lahko ponovno izbere aplikacijo iz katere želi uvoziti podatke.



## 6. Nefunkcionalne zahteve

**ZAHTEVE IZDELKA**

1. Ob vsaki spremembi podatkov znotraj podatkovne baze, se mora ustrezno posodobiti varnostna kopija.
2. Sistem mora biti dosegljiv vsaj 95 odstotkov časa v letu, kar znaša 438 ur časa prekinitve.
3. Sistem mora biti zmožen prenesti vsaj 500 hkratnih uporabnikov.
4. Sistem mora biti zmožen zaznati in omejiti zlonamernega uporabnika, ki namerno poskuša obremeniti storitve (večkratno pošiljanje zahtevkov, ki jih mora sistem procesirati).
5. Sistem mora zagotoviti šifrirano komunikacijo med uporabnikom in storitvijo.
6. Sistem ne sme omogočati uporabniku dostop do podatkov, za katere ni pooblaščen.
7. Sistem mora biti dosegljiv na javno dostopnem spletnem naslovu.

**ORGANIZACIJSKE ZAHTEVE**

8. Zgradba sistema mora ustrezati MVC arhitekturnemu stilu in mora vsebovati MEAN sklad tehnologij.
9. Pred posodobitvijo sistema mora ta opraviti vse teste enot, ki preverijo delovanje posameznih modulov znotraj izvorne kode. Temu
   nato sledijo integracijski testi, ki so namenjeni preverjanju prenosa podatkov med posameznimi komponentami.
   V primeru uspešno opravljenih testov se izvede končno testiranje s črno škatlo, kjer se preveri delovanje celotne integrirane aplikacija. 

**ZUNANJE ZAHTEVE**

10. Hranjenje in uporabljanje osebnih podatkov uporabnikov se mora ravnati po ZVOP-1-UPB1 (Zakon o varstvu osebnih podatkov).
11. Varovanje podatkov ustrezno sledijo standardom COBIT/ISACA ter dopolnilom, kot so IT Control Objectives,
    COBIT Mappings, COBIT Control Practices in IT Assurance Guide Using COBIT.


## 7. Prototipi vmesnikov

Celoten sistem v interaktivni obliki je dosegliv na [povezavi](https://www.figma.com/proto/0nwguFNm4xZhEh2ERbcVKKsL/StraightAs?node-id=1%3A3&scaling=scale-down).

Funkcionalnost: Prijava

![login](../img/login.png)

------

Funkcionalnost: Registracija

![register](../img/register.png)

------

Funkcionalnost: Urejanje terminov dogodkov

![urnik](../img/urnik.png)

------

Funkcionalnost: Uvoz urnikov iz drugih urniških aplikacij

![import](../img/import.png)

------

Funkcionalnost: Dodajanje dogodka v sistem

![event](../img/event.png)

------

Funkcionalnost: Dodajanje novega urednika

![admin](../img/admin.png)

------

Funkcionalnost: Registracija

![adduser](../img/adduser.png)

------

Funkcionalnost: Dodajanje urnika za inštitucijo

![addtimetable](../img/addtimetable.png)

------

Sistemski vmesniki:

* Google Calendar

* FRI Urnik

* meetup.com



