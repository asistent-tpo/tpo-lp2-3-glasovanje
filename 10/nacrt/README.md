# Načrt sistema

| **Naziv projekta** | StraightAs |
|:---|:---|
| **Člani projektne skupine** | Vid Rijavec, Emir Hasanbegović, Andrej Martinovič, Aljaž Markovič |
| **Kraj in datum** | Ljubljana, April 2019 |



## Povzetek

Prvi del dokumenta zajema načrt arhiktekture sistema. Ta zajema enostaven, neformalen diagram v katerem so predstavljene vse potrebne entitete in razmerja med njimi. V začetku je bilo potrebno proučiti okolje v katerem aplikacija sodeluje, zato je bilo potrebno poiskati glavne akterje. Ti so urednik (skrbi za uvoz urnikov in javnih dogodkov), registrirani uporabnik (lahko ureja svoj urnik in uvaža podatke prek API-jev za urnike), administrator (ima dostop do celotnega sistema in ga vzdržuje) ter neregistrirani uporabnik (ima na voljo registracijo in vpogled do javno dosegljivih dogodkov ter vpogled v uporabnost spletne storitev - opis funkcionalnosti, ki jih aplikacija nudi). Ne smemo pozabiti tudi na zunanje deležnike, s katerimi naš sistem sodeluje (MeetUp, Google Calendar in FRI urnik). Nato je predstavljen vidik sodelovanja. Na tem mestu smo najprej identficirali diagrame primera uporabe in diagrame zaporedij za posamezne funkcionalnosti. Na koncu pa smo pripravili še strukturni načrt, ki opisuje organizacijo sistema v smislu komponent, ki ta sistem sestavljao. Narisali smo razredni diagram pri čemer smo si pomagali z arhikteturnim slogom MVC (model, pogled, krmnilnik). Ta namreč omogoča sistem strukturirati v tri ločene komponente, ki med seboj sodelujejo in se uporablja, ko obstaja več načinov prikaza in interakcije s podatki, kar povsem ustreza našemu sistemu. 



## 1. Načrt arhitekture

### 1.1 Logični pogled

![logicnipogled](../img/logicni_pogled.png)

### 1.2 Razvojni pogled

![razvojnipogled](../img/razvojni_pogled.png)

## 2. Načrt strukture

### 2.1 Razredni diagram

![class](../img/RazredniDiagram.png)

### 2.2 Opis razredov

#### Personalizacija urnika

Registrirani uporabnik lahko za potrebe urejanja urnika (urejanje prioritete dogodkov, terminov, brisanje,..) uporablja pogled z imenom PersonalizacijaUrnika. 

#### Upravljanje javnih urnikov

Kot je že omenjeno, urednik lahko ureja uvoze iz inštitucij ali pa ročno dodaja javne dogodke na katere se drugi uporabniki lahko prijavijo. Temu primerno vsebuje pogled metodo dodajJavniUrnik, ki kot parameter sprejme ime inštitucije in objekt urnik na katerega urednik želi uvozit dogodke.

#### Upravljanje javnih dogodkov

Uporabniški vmesnik z imenom UpravljanjeJavnihDogodkov omogoča ročno dodajanje dogodkov na določen urnik in je predsvsem namenjena entiteti Urednik. Za potrebe lažjega vnosa javnih dogodkov je tu še prisoten deležnik MeetUp API, s pomočjo katerega lahko urednik dostopa do prihajajočih dogodkov. Prav tako pa lahko prek te zaslonske maske uporabnik doda dogodke z določene inštitucije.

#### Administrativni vmesnik

Ključni akter naše aplikacije je Administrator, ki ima pregled nad celotnim sistemom, kar mu omogoča zaslonska maska z imenom AministrativniVmesnik. Ta tudi komunicira s krmnilnikom z imenom NadzorniSistem. Ena izmed ključnih funkcionalnosti, ki je le administratorjem dostopna pa je dodajanje urednika, kar mu omogoča metoda dodajPraviceUporabniku.

### Prijava

Ko registrirani uporabnik želi uporabljati aplikacijo StraightAs se mora ta seveda prijaviti. To mu omogoča vmesnik z imenom Prijava.

### Registracija

Ko neregistrirani uporabnik želi uporabljati aplikacijo StraightAs se ta mora najprej registrirati, na ta način pridobi vse funkcionalnosti registriranega uporabnika.

#### Registrirani uporabnik

Je eden izmed glavnih akterjev v aplikaciji. Ima možnost pregleda ter urejanja svoje urnika. Prav tako lahko uvozi urnik iz drugih aplikacij. Njegove metode podeduje razred Urednika. Z razredom Uporabnik upravlja razred Upravljalnik Uporabnikov, ki prav tako generira pogled prijave in registracije. 

#### Atributi
* **Atribut 1**
    * **ime:** id,
    * **podatkovni tip:** long,
    * **pomen:** enoličen ključ razreda,
    * **zaloga vrednosti:** neomejena

* **Atribut 2**
    * **ime:** email,
    * **podatkovni tip:** String

* **Atribut 3**
    * **ime:** geslo,
    * **podatkovni tip:** String

* **Atribut 4**
    * **ime:** random,
    * **podatkovni tip:** int,
    * **pomen:** varnost

#### Nesamoumevne metode

* **Metoda 1**
    * **ime:** pridobiJWT,
    * **parametri:** String email, String geslo,
    * **tip rezultata:** String,
    * **pomen:** avtorizacija

#### Urednik

Eden izmed glavnih akterjev v sistemu. Podeduje razred Registrirani Uporabnik. Poleg podedovanih funkcionalnosti lahko ustvarja javne dogodke in urnike ter uvaža urnike inštitucij. 


#### Administrator

Eden izmed glavnih akterjev v sistemu. Podeduje razred Urednik. Poleg podedovanih funkcionalnosti lahko registrira uporabnike, ustvari urednike ter ima celoten pregled nad sistemom.  


#### Dogodek

Je ena izmed osrednjih dveh entitet. Z entiteto upravljata razreda Urejevalnik Dogodkov ter Urejevalnik urnikov. Lahko pripada večim entitetam Urnik.

#### Atributi

* **Atribut 1**
    * **ime:** dogodekID,
    * **podatkovni tip:** long,
    * **pomen:** enoličen ključ,
    * **zaloga vrednosti:** neomejena
* **Atribut 2**
    * **ime:** naziv,
    * **podatkovni tip:** String
* **Atribut 3**
    * **ime:** termin,
    * **podatkovni tip:** Object
* **Atribut 4**
    * **ime:** opis,
    * **podatkovni tip:** String

#### Nesamoumevne metode

* **Metoda 1**
    * **ime:** preveriVeljavnost,
    * **parametri:** String urnikID, String institucija,
    * **tip rezultata:** String

#### Urnik

Je ena izmed osrednjih dveh entitet. Z entiteto upravlja razred Urejevalnik urnikov. Vsebuje množico entitet Urnik. Pripada enemu razredu Registrirani Uporabnik.

#### Atributi

* **Atribut 1**
    * **ime:** urnikID,
    * **podatkovni tip:** long,
    * **pomen:** enoličen ključ,
    * **zaloga vrednosti:** neomejena
 * **Atribut 2**
    * **ime:** naziv,
    * **podatkovni tip:** String

#### Nadzorni Sistem

Predstavlja krmilnik, ki povezuje razrede Registrirani Uporabnik, Urednik ter Administrator z vmesnikom Administrativni Vmesnik. 

#### Nesamoumevne metode

* **Metoda 1**
    * **ime:** posodobiUporabnika2Urednik,
    * **parametri:** String email,
    * **tip rezultata:** Void,
    * **pomen:** spremeni Registriranega Uporabnika v Urednika

#### Upravljalnik Uporabnikov

Predstavlja krmilnik, ki povezuje razred Registrirani Uporabnik z vmesnikomoma Prijava in Registracija. 

#### Nesamoumevne metode

* **Metoda 1**
    * **ime:** preveriPolja,
    * **parametri:** String email, String geslo, String r_geslo,
    * **tip rezultata:** String,
    * **pomen:** preverba pravilnosti polj pri registraciji
    
* **Metoda 2**
    * **ime:** preveriPolja,
    * **parametri:** String email, String geslo,
    * **tip rezultata:** String,
    * **pomen:** preverba pravilnosti polj pri prijavi

#### Urejevalnik dogodkov

Predstavlja krmilnik, ki povezuje razred Dogodek z vmesnikoma Upravljanje Javnih Dogodkov in Personalizacija Urnika.

#### Nesamoumevne metode

* **Metoda 1**
    * **ime:** pridobiSeznamMeetupDogodkov,
    * **parametri:** ,
    * **tip rezultata:** Object[],
    * **pomen:** pridobi seznam uvoženih dogodkov prek Meetup API

* **Metoda 2**
    * **ime:** preveriVeljavnostDogodka,
    * **parametri:** String naziv, Object termin, String opis,
    * **tip rezultata:** String,
    * **pomen:** preveri ali je dogodek že pretekel

* **Metoda 3**
    * **ime:** dodajDogodke,
    * **parametri:** long urnikID, String institucija, Object[] dogodki,
    * **tip rezultata:** String,
    * **pomen:** uvozi dogodke urnika inštitucije v podan urnik

#### Urejevalnik Urnikov

Predstavlja krmilnik, ki povezuje razreda Urnik in Dogodek z vmesnikoma Upravljanje Javnih Urnikov in Personalizacija Urnika.

#### Nesamoumevne metode

* **Metoda 1**
    * **ime:** dodajUrnikReq,
    * **parametri:** long urnikID, String institucija,
    * **tip rezultata:** Object[],
    * **pomen:** na API inštitucije pošlje zahtevek za urnik 

* **Metoda 1**
    * **ime:** dodajUrnikReq,
    * **parametri:** long urnikID, String institucija,
    * **tip rezultata:** Object[],
    * **pomen:** na inštitucijo pošlje zahtevek za urnik 

## 3. Načrt obnašanja

#### Registracija

![reg](../img/Registracija.png)

#### Prijava

![prij](../img/Prijava.png)

#### Dodajanje novega urednika

![novured](../img/DodajanjeNovegaUrednika.png)

#### Dodajanje dogodka v sistem

##### Ročno

![doddog2](../img/Part1RocnoDodajanjeDogodkov.png)

##### Prek strani Meetup.com

![doddog1](../img/DodajanjeNovegaDogodka.png)

##### Prek inštitucije

![doddog3](../img/Part2AvtomatskoDodajanjeDogodkov.png)

#### Dodajanje urnika za inštitucijo

![uvoz](../img/UvozIzZunanjeAplikacije.png)

#### Prijava na dogodek

![prijdog](../img/PrijavaNaDogodek.png)

#### Razvrščanje dogodkov po prioriteti

![priority](../img/RazvrščanjeDogodkovPoPrioriteti.png)

#### Urejanje terminov dogodkov

![termin](../img/SpreminjanjeTermina.png)

#### Brisanje dogodkov

![brisanje](../img/BrisanjeDogodkov.png)

#### Uvoz urnikov iz drugih urniških aplikacij

![autournik](../img/AvtomatskoDodajanjeUrnikov.png)

-------

#### Diagram stanj OAuth2

![oauth](../img/OAuth2-Sequence.png)