# Načrt sistema

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Roni Likar, Deni Cerovac, Luka Železnik, Jan Ivanovič |
| **Kraj in datum** | 23.4.2019, Ljubljana |



## Povzetek

Naloga dokumenta je predstaviti načrt sistema, v kateremu zapišemo pogled oziroma perspektivo le tega.  To dosežemo s podrobnejšo predstavitvijo načrta arhitekture sistema, njegove strukture ter obnašanja. Najprej začnemo s predstavitvijo oziroma dokumentiranjem arhitekture sistema, pri kateremu si pomagamo z uporabo diagramov, ki prikazujejo entitete in razmerja med njimi. Te predstavimo v obliki izbranega arhitekturnega vzorca, ki opisuje sistem kot organiziran sklop sodelujočih komponent. Nato nadaljujemo s sestavo načrta strukture sistema, ki je podrobneje opisan z razrednim diagramom. Temu sledi natančni opis posameznih razredov in povezav med njimi. Izdelan diagram na grobo prikaže sturkturo bodoče podatkovne baze. Po končani izdelavi le tega, predstavimo načrt obnašanja sistema, ki je izdelan na podlagi diagrama primerov uporabe. Za vsak primer uporabe iz tega diagrama je narisan diagram zaporedja. Znotraj posameznega diagrama zaporedja pa so predstavljeni tokovi dogodkov posamezne funkcionalnosti.



## 1. Načrt arhitekture

Zato ker se ukvarjamo s spletnim informacijskim sistemom, smo se pri načrtovanju arhitekture sistema odločili uporabiti arhitekturni vzorec model-pogled-krmilnik (MVC). Ta ločuje predstavitev in interakcijo od sistemskih podatkov. Strukturiran je v tri logične komponente, ki medsebojno sodelujejo. Uporaba le tega podbira predstavitev istih podatkov na različne načine s spremembami v eni predstavitvi, ki so potem prikazane v vseh. Ker so naše razredne komponente med seboj tesno povezane in odvisne druga od druge, pride ta lastnost vzorca zelo prav.

Arhitektura vsebuje logični pogled sistema, saj prikazuje glavne abstrakcije v sistemu v obliki razredov ki so poimenovani v treh komponentah vzorca MVC. Poleg tega pa prikazuje tudi razdelitev programske opreme v komponente, ki jih bo potrebno kasneje razvijati.

![ToDo diagram](../img/mvc.png)

## 2. Načrt strukture

### 2.1 Razredni diagram

![Razredni diagram](../img/Razredni1.png)


![Razredni diagram](../img/Razredni2.png)

### 2.2 Opis razredov

-----------------------------------------
#### Entity Predmet

* predstavlja tabelo predmetov v podatkovni bazi.

#### ATRIBUTI

* id:
    * integer

* naziv:
    - String

* casIzvajanja:
    - String

* pomembniDatumi:
    - String [][]
	
#### METODE:

* poleg samoumevnih metod vsebuje tudi metode:
	- String [] posodobiPredmet(podatki) - zbriše predmet ali pa posodobi vrednosti atributov predmeta

-----------------------------------------

#### Entity Fakulteta 

* predstavlja tabelo fakultet v podatkovni bazi.

#### Atributi
* id
    - Integer
		
* naziv
    - String
    - naziv predstavlja ime fakultee
		
* smeri
    - String[]
    - smeri predstavljajo mozne smeri na faksu
		
#### Metode:
* poleg samoumevnih metod vsebuje tudi metode:
    - boolean vnesiSpremembe(podatki) - posreduje spremembe, ki so prisle iz google koledarja na podatkovno bazo .

-----------------------------------------

#### Entity Redovalnica 
* predstavlja tabelo fakultet v podatkovni bazi.

#### Atributi

* id
	- integer
			
* ocenePredmetov 
	- tabela tipa OcenePredmetov[]
	
	
#### Metode

* poleg samoumevnih metod vsebuje tudi metode:
	- Redovalnica posodobiPredmet(predmetId, podatki) - posodobi ali zbrise predmet in spremembe uveljavi v bazi

-----------------------------------------

#### Entity Koledar

* predstavlja tabelo fakultet v podatkovni bazi.

#### Atributi
* id
	- Iinteger
		
* obveznosti
	- tebela tipa obveznost[]
		
#### Metode

* poleg samoumevnih metod vsebuje tudi metode:
		
	- Koledar dodajObveznostVKoledar(obveznost) - pošlje novo obvetnost ki se doda v bazo.
	- boolean urediObveznost() - obveznost posodobimo in spremembe nato uveljavimo v bazi.
	- boolean izbrisiObveznost() - obveznost izbrisemo in spremembe nato uveljavimo v bazi. 

-----------------------------------------

#### Entity TODOlist

* predstavlja tabelo fakultet v podatkovni bazi.

#### Atributi:
* id
	- Iinteger
		
* pomnik
	- tabela tipa Opomnik []
			
#### Metode:

* poleg samoumevnih metod vsebuje tudi metode:
	- Opomnik [] zaključiOpomnik()	- označi opomnik kot zaključen, vendar je še vedno viden
	- Opomnik [] ustvariOpomnik()	- ustvari nov opomnik, ki ga nato doda v bazo

-----------------------------------------
#### Entity Urnik
* predstavlja tabelo fakultet v podatkovni bazi.

#### Atributi:
* id
	- Iinteger
		
* aktivnosti
	- tabela tipa Aktivnost []
			
#### Metode:
* poleg samoumevnih metod vsebuje tudi metode:		
	- void ustvariNovoAktivnost() 	-	novo aktivnost ki smo jo ustvarili shrani v bazo.
	- urnik posodobiUrnik()			-	zbriše ali posodobi predmet v urniku in spremembe uveljavi v bazi
----------------------------------------

#### Entity Redovalnica 
* Objekt razreda Redovalnica v podatkovni bazi. 

#### Atributi:
* id
	- Iinteger		
* ocenePredmetov 
	- OcenePredmetov[]
	- tabela z zaželenimi ocenami, trenutnimi ocenami in nazivom predmeta
			
#### Metode:
1.	Redovalnica pridobiRedovalnico(redovalnicaId) – Vrne redovalnico s podanim ID-jem
2.	Predmet pridobiPodatkePredmeta(predmetId) – Vrne podatke predmeta s podanim ID-jem
3.	Redovalnica posodobiPredmet(predmetId, podatkiPredmeta) – Posodobi predmet s podanim ID-jem

----------------------------------------

#### View KoledarView

* predstavlja prikaz koledarja.

#### ATRIBUTI

Razred ne vsebuje atributov.
	
#### METODE:
1.	void displayKoledar() – Prikaže koledar na strani
2.	void updateDisplay()
3.	void displayOptionMenu() – Prikaže opcijski meni
4.	void mesecniPogled() – Zamenja tedenski pogled na mesečni
5.	void dodajObveznost() – Začne postopek dodajanja obveznosti v koledar
6.	void urediObveznost() – Začne postopek urejanja obveznosti
7.	void prikaziNapako()
8.	void dodajObveznostVKoledar()

-----------------------------------------
#### View PotrditevView

* predstavlja prikaz za potrjevanje profesorjev.

#### ATRIBUTI

Razred ne vsebuje atributov.
	
#### METODE:
1.	void preglejProsnje() – Administratorju prikaže prošnje za profesorje
2.	void updateDisplay()
3.	void izberiProsnjo() – Prikaže podrobnosti izbrane prošnje
4.	void potrdiProsnjo() – Potrdi prošnjo za profesorja

-----------------------------------------
#### View RedovalnicaView

* predstavlja prikaz redovalnice.

#### ATRIBUTI

Razred ne vsebuje atributov.
	
#### METODE:
1.	void urediPredmet() – Začne postopek urejanja predmeta
2.	void prikaziVnosnoPolje()
3.	void prikaziNapako()
4.	void potrdiSpremembe()
5.	void prikaziRedovalnico()

-----------------------------------------
#### View TODOlistView

* predstavlja prikaz TO-DO liste.

#### ATRIBUTI

Razred ne vsebuje atributov.
	
#### METODE:

1.	void prikaziTodo()
2.	void zakljuciOpomnik() – Zraven izbranega opomnika naredi kljukico
3.	void dodajOpomnik()
4.	void potrdi() – Ustvari TO-DO vnos
5.	opomnikPopUp() – Prikaže se pop-up okno za dodajanje na TO-DO listo

-----------------------------------------
#### View VpisVPredmetView

* predstavlja prikaz vpisa v predmet.

#### ATRIBUTI

Razred ne vsebuje atributov.
	
#### METODE:

1.	void prikaziPredmete()
2.	void displayFakultete()
3.	void displayLetnike()
4.	void displayPredmete()
5.	void displayPrenosPredmet() – Prikaže pop-up za prenos podatkov v urnik
6.	void updateDisplay()


-----------------------------------------
#### View PredmetiView

* predstavlja prikaz predmetov profesorja.

#### ATRIBUTI

Razred ne vsebuje atributov.
	
#### METODE:

1.	void dodajObveznost()
2.	void prikaziVnosnoPolje()
3.	void dodajObveznostVPredmet()
4.	void prikaziPredmete()
5.	void prikaziMojePredmete()
6.	void urediPredmet()
7.	void potrdiPosodobitev()


-----------------------------------------
#### View PrijavaView

* predstavlja prikaz prijave v sistem.

#### ATRIBUTI

Razred ne vsebuje atributov.
	
#### METODE:

1.	void prikaziPrijavo()
2.	void displayPrijava()

-----------------------------------------
#### View RegistracijaView

* predstavlja prikaz registracije v sistem.

#### ATRIBUTI

Razred ne vsebuje atributov.
	
#### METODE:

1.	void prikaziRegistracijo()
2.	void registracija() – Registrira uporabnika v sistem
3.	void displayRegistracija()
4.	void prikaziToDo()
5.	void prikaziNapako()

-----------------------------------------
#### View RedovalnicaView

* Objekt razreda Redovalnica predstavlja prikaz redovalnice.

#### ATRIBUTI

* Razred ne vsebuje atributov.
	
#### METODE:

1.	void pridobiRedovalnico() – Pošlje zahtevek o prikazu redovalnice
2.	void posodobiPredmet() – Pošlje zahtevek o posodobitvi predmeta
3.	void prikaziNapako() 
4.	void potrdiSpremembe() – Ob potrditvi pošlje Controller-ju podatke za spremembo predmeta
5.	void prikaziRedovalnico() – Prikaže redovalnico na zaslonu

-----------------------------------------
#### View UrnikView

* predstavlja prikaz urnika.

#### ATRIBUTI

Razred ne vsebuje atributov.
	
#### METODE:

1.	void prikaziUrnik()
2.	void dodajAktivnost()
3.	void potrdiPodatke() – Potrdi nove podatke
4.	void displayUrnik()
5.	void prikaziVnosnoPolje()
6.	void displayNapakaIzpolniVsaZahtevanaPolja()
7.	void displaySistemNotResponding()
8.	void posodobiAktivnost() – Posodobi podatke aktivnosti
9.	void potrdiPosodobitev()
10.	void prikaziNapako()

-----------------------------------------
#### API GoogleAPI

* predstavlja Googlov Calendar API.

#### ATRIBUTI

Razred ne vsebuje atributov.
	
#### METODE:

1. boolean patch() – Pošlje http request Google APIju za posodobitev koledarja
2. boolean insert() - Ustvari nov koledar
3. boolean delete()
4. boolean get()
-------------------------------------

#### Controller Urnik

* Objekt razreda Urnik-controller upravlja z uporabniško interakcijo na podlagi razreda Urnik-boundry in določene interakcije prenese v stopnjo nižje in sicer v razred Urnik-entity. 

#### ATRIBUTI

Razred ne vsebuje atributov.
	
#### METODE:

Poleg samoumevnih metod, ponuja tudi:

1.	String preveriVnosnaPolja(podatkiPolja) – preveri če so vsa potrebna vnosna polja vnesena in vrne String, ki javi status oblike String
2.	Urnik ustvariNovoAktivnost() – vrne posodobljen urnik
3.	Urnik posodobiAktivnosti(aktivnost) – vrne posodobljen urnik
4.	String noResponse() – vrne string, ki opisuje problem z sistemom

-------------------------------------

#### Controller Predmet

* Objekt razreda Predmet-controller upravlja z uporabniško interakcijo na podlagi razreda PredmetiView-boundry in določene interakcije prenese v stopnjo nižje in sicer v razred Predmet-entity. 

#### ATRIBUTI

Razred ne vsebuje atributov.
	
#### METODE:

Poleg samoumevnih metod, ponuja tudi:

1.	Fakulteta[] vrniFakultete() – vrne tabelo tipa Fakultete
2.	Letniki[] izberiFakulteto() – vrne tabelo tipa Letniki
3.	boolean potrdiPrenost() – vrne spremenljivko tipa boolean
4.	boolean dodajObveznostVPredmet() – vrne spremenljivko tipa boolean
5.	String pridobiPodatkePredmeta(predmetId) – na podlagi predmetId vrne podrobnejše informacije o predmetu v tabeli tipa String
6.	Predmet prikaziMojePoredmete(userId) - na podlagi spremenljivke userId vrne objekt Predmet
7.	String preveriVnosnaPolja(podatkiPolja) – na podlagi podatkov, ki jih dobimo v argumentu preverimo če so vnesena vsa zahtevana polja in na podlagi tega vrnemo status oblike String
8.	Predmet posodobiPredmet(podatkiPredmeta) – vrne posodobljen objekt tipa Predmet na podlagi podatkov, ki jih dobimo z funkcijo

-------------------------------------

#### Controller To-Do

* Objekt razreda TO-DO controller upravlja z uporabniško interakcijo na podlagi razreda TO-DOView boundry in določene interakcije prenese v stopnjo nižje in sicer v razred TO-DO entity.

#### ATRIBUTI

Razred ne vsebuje atributov.
	
#### METODE:

Poleg samoumevnih metod, ponuja tudi:

1.	Opomnik[] vrne tabelo tipa Opomnik, tabela vsebuje vse opomnike uporabnika
2.	Opomnik[] vrne tabelo tipa Opomnik, tabela vsebuje vse opomnike uporabnika, vključno z na novo zaključenimi
3.	Opomnik[] vrne tabelo tipa Opomnik, tabela vsebuje vse opomnike uporabnika, vključno z na novo ustvarjenimi

-------------------------------------

#### Controller Registracija

* Objekt razreda Registracija controller upravlja z uporabniško interakcijo na podlagi razreda RegistracijaView boundry in določene interakcije prenese v stopnjo nižje in sicer v razred Uporabnik entity. 

#### ATRIBUTI

Razred ne vsebuje atributov.
	
#### METODE:

Poleg samoumevnih metod, ponuja tudi:

1.	boolean potrdiProsnjo(podatkiOProsnji) – vrnemo spremenljivko tipa boolean, ta nam pove ali smo prosnjo potrdili ali zavrnili

-------------------------------------

#### Controller Prijava

* Objekt razreda Prijava controller upravlja z uporabniško interakcijo na podlagi razreda PrijavaView boundry in določene interakcije prenese v stopnjo nižje in sicer v razred Uporabnik entity. 

#### ATRIBUTI

Razred ne vsebuje atributov.
	
#### METODE:

Poleg samoumevnih metod, ponuja tudi:

1.	boolean prijava() – vrne spremenljivko tipa boolean, ta nam pove ali je bila prijava uspesna ali zavrnjena

-------------------------------------

#### Controller Koledar

* Objekt razreda Koledar controller upravlja z uporabniško interakcijo na podlagi razreda KoledarView boundry in določene interakcije prenese v stopnjo nižje in sicer v razred Koledar entity. 

#### ATRIBUTI

* mesecniPogled
	- boolean
	
#### METODE:

Poleg samoumevnih metod, ponuja tudi:

1.	Koledar dodajObveznostiVKoledar(): vrne objekt Koledar, ki ga bo prikazalo uporabniku na zaslonu
2.	boolean UrediObveznost(): vrne spremenljivko tipa boolean, vrednost določi glede uspešnosti urejanja
3.	boolean IzbrisiObveznost(): vrne spremenljivko tipa boolean, vrednost določi glede uspešnosti brisanja

-------------------------------------

#### Controller Redovalnica 

* Objekt razreda Redovalnica Controler predstavlja prikaz redovalnice.

#### ATRIBUTI

* Razred ne vsebuje atributov.
	
#### METODE:

Poleg samoumevnih metod, ponuja tudi:

1.	Redovalnica pridobiRedovalnico(redovalnicaId) – Vrne redovalnico s podanim ID-jem
2.	Predmet pridobiPodatkePredmeta(predmet) - Vrne podatke predmeta s podanim ID-jem
3.	String[]  preveriVnosnaPolja(podatkiPolja) – Preveri, če so bili podatki pravilno vpisani
4.	Redovalnica posodobiPredmet(predmetId, podatkiPredmeta) – Posodobi podatke o predmetu in vrne posodobljeno redovalnico

-------------------------------------


## 3. Načrt obnašanja

#### Prijava

![Prijava diagram](../img/prijavaDiagram.PNG)


--------------

#### Registracija

![Registracija diagram](../img/registracijaDiagram.png)

--------------

#### Dodaj obveznost v koledar

![Koledar diagram](../img/koledarDiagram.PNG)

![Koledar diagram](../img/koledar-alt.PNG)

-----------

#### Uredi/Izbriši obveznost v koledarju

![Koledar diagram](../img/koledar-posodobi.PNG)

![Koledar diagram](../img/koledar-izbrisi.PNG)

--------------

#### Dodaj predmet v seznam

#####Osnovni tok: 

![ToDo diagram](../img/Prof_dodajPredmet.png)

--------------

#### Uredi/Izbriši predmet v seznamu

#####Osnovni tok: 

![ToDo diagram](../img/Prof_posodobiPredme.png)

--------------

#### Dodaj opomnik v ToDo

#####Osnovni tok: 

![ToDo diagram](../img/Todo_osTok.png)

#####Izjemni tok: 
![ToDo diagram](../img/Todo_izTok.png)

--------------

#### Zaključi opomnik v ToDo

#####Osnovni tok: 

![ToDo diagram](../img/TodoZak_osTok.png)

--------------

#### Dodaj aktivnost v urnik

#####Osnovni tok: 

![dodaj_aktivnost-v-urnik_os](../img/TPO-dodaj_aktivnost-v-urnik_os.png)

#####Alternativni tok: 

![dodaj_aktivnost_v_urnik_alt](../img/TPO-dodaj_aktivnost_v_urnik_alt.png)

--------------

#### Uredi/Izbriši aktivnost iz urnika

#####Osnovni tok: 

![uredi_zbrisi_aktivnost_v_urniku_os](../img/TPO-uredi_zbrisi_aktivnost_v_urniku_os.png)

#####Alternativni tok: 

![uredi_zbrisi_aktivnost_v_urniku_alt](../img/TPO-uredi_zbrisi_aktivnost_v_urniku_alt.png)

--------------

#### Odjava

![Odjava diagram](../img/odjava.PNG)

--------------

#### Dodaj predmet v redovalnico

#####Osnovni tok: 

![dodaj_predmet_v_redovalnico_os](../img/TPO-dodaj_predmet_v_redovalnico_os.png)


--------------

#### Uredi/Izbriši predmet iz redovalnice 

#####Osnovni tok: 

![uredi_zbrisi_predmet_iz_redovalnice_os](../img/TPO-uredi_zbrisi_predmet_iz_redovalnice_os.png)

--------------

#### Vpiši me v predmet

![VpisVP](../img/tpo3-14.png)

--------------

#### Potrditev prošnje

![PotP](../img/tpo3-15.png)

--------------

#### Mesečni pregled urnika

![MPUr](../img/tpo3-16.png)

--------------

#### Izbira datuma na koledarju

![IDkol](../img/tpo3-17.png)

--------------















