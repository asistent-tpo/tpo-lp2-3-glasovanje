# Načrt sistema

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Blaž Černi, Grega Dvoršak, Matic Tempfer, Sergej Munda |
| **Kraj in datum** | Ljubljana, 14. 4. 2019 |



## Povzetek

Dokument bo obsegal načrt arhitekture, načrt strukture ter načrt obnašanja. Sprva smo načrt arhitekture opisali s pomočjo logičnega pogleda, ki bo dal malce bolj abstrakten pogled na samo arhitekturo, ki jo bo naša aplikacija uporabljala. Torej na arhitekturo MVC, oziroma model-pogled-krmilnik. Drugi pgoled je razvojni pogled, ki prikazuje, kako je programska oprema sestavljena pri razvoju. V načrtu strukture, so z razrednim diagramom točno prikazani vsi razredi, ter entitete, ki bodo vključene pri našemu sistemu, temu pa sledi še malce bolj podroben opis teh razredov.Zadnji načrt, torej načrt obnašanja, pa jasneje prikazuje za vse naše funkcionalnosti kako se le-te obnašajo pri uporabi s pomočjo diagramov zaporedja, za najboljšo vizualno predstavo.



## 1. Načrt arhitekture

### 1.1 Logični pogled
![Arhitekturni logični diagram](../img/ArhitekturniLogicni.png)

### 1.2 Razvojni pogled
![Arhitekturni razvojni diagram](../img/ArhitekturniRazvojni.png)


## 2. Načrt strukture

### 2.1 Razredni diagram

![Razredni diagram](../img/razredni_diagram.png)


### 2.2 Opis razredov

### Vloga
* Vloga uporabnika v sistemu

#### Atributi
* long id
* String tip, zaloga vrednosti {Študent, Učitelj, Skrbnik}



### Uporabnik
* Predstavlja uporabnike ki se lahko prijavijo v sistem.

#### Atributi
* long id
* String ime
* String priimek
* String uporabniskoIme
* String geslo
* Vloga vloga
* boolean aktiviran

#### Nesamoumevne metode

* `void blokirajUporabnika(Uporabnik uporabnik)`
Metoda prejme uporabnika in mu nastavi polje aktiviran na false. Po tem se uporabnik ne bo mogel več prijaviti.

### Obveznost

* Namenjena hranjenju obveznosti za uporabnike.

#### Atributi
* long id
* String ime
* String opis
* String datumZacetka
* String datumKonca
* boolean arhiviran
* Integer prioriteta, zaloga vrednosti {1, 2, 3, 4, 5}
* Uporabnik uporabnik


#### Nesamoumevne metode

* `List<Obveznost> pridobiObveznostiZaUporabnika(Uporabnik uporabnik)` Kot argument prejme uporabnika in vrne vse obveznosti, ki pripadaja podanemu uporabniku.

## 3. Načrt obnašanja

#### Registracija
![Diagram registracija](../img/zaporedje_registracija.png)

#### Prijava
![Diagram prijava](../img/zaporedje_prijava.png)

#### Odjava
![Diagram zaporedja odjava](../img/odjava.png)

#### Pregled seznama obveznosti
![Diagram pregled seznama obveznosti](../img/pregled_seznama_obveznosti.png)

#### Dodajanje obveznosti
![Diagram zaporedja dodajanje](../img/zaporedje_dodaj_obveznost.png)

#### Urejanje obveznosti
![Diagram zaporedja urejanje](../img/zaporedje_uredi_obveznost.png)

#### Brisanje obveznosti
![Diagram zaporedja brisanje](../img/zaporedje_izbriši_obveznost.png)

#### Obnovitev obveznosti iz arhiva
![Diagram zaporedja arhiv obnovi](../img/zaporedje_arhiv_obnovi.png)

#### Brisanje obveznosti iz arhiva
![Diagram zaporedja arhiv izbriši](../img/zaporedje_arhiv_izbriši.png)

#### Podrobnosti obveznosti
![Diagram zaporedja podrobnosti obveznosti](../img/Podrobnosti_obveznosti.png)

#### Google Calendar
![Diagram zaporedja google calendar](../img/GoogleCalendar.png)

#### Graf zasedenost
![Razredni diagram](../img/graf-zasedenosti-zaporedja.png)

#### Blokiraj zasedenost
![Razredni diagram](../img/blokiraj-uporabnika-zaporedja.png)