# Dokument zahtev

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Blaž Černi, Grega Dvoršak, Matic Tempfer, Sergej Munda |
| **Kraj in datum** | Ljubljana, 30. 3. 2019 |



## Povzetek projekta

Aplikacija StraightAs bo rešila problem stresa študentov in tudi učiteljev ki bodo tako imeli boljši pregled nad svojimi obveznostmi.
S pomočjo uporabniških vlog, smo jasneje definirali kakšni tipi uporabnikov bodo lahko uporabljali našo aplikacijo in dostopali do podatkov v podatkovni bazi.
Slovar pojmov bo razrešil vse dvome glede nejasnih pojmov, ki bodo uporabljeni skozi celoten dokument. Da bo aplikacija lažja za razumevanje smo uporabili  diagram primerov uporabe, 
s katerim smo predstavili funkcionalnosti, ki so podrobneje opisane v funkcionalnih zahtevah. Opisali smo tudi vse vrste nefunkcionalnih zahtev(zahteve izdelka, organizacijske, zunanje), 
da pa bo še bolj jasen izgled aplikacije pa smo naredili osnutke zaslonskih mask.


## 1. Uvod

Aplikacija StraightAs je namenjena študentom in učiteljem v Sloveniji in jim ponuja sistem, ki omogoča učinkovito beleženje in sledenje svojim obveznostim. Študent si poleg predavanj in vaj lahko beleži roke za oddajo nalog, izpitne roke, datume kolokvijev,učitelj pa lahko te roke postavlja ima pregeld nad vesmi aktivnostmi pri predmetih, ki jih izvaja, si beleži datume sestankov in tako dalje. Aplikacija zato razbremenjuje uporabnike stresa in jim nudi pregled vseh obveznosti na enem mestu. Uporabnik lahko določi tudi prioritete za obveznosti, kar še izboljša njegovo izkušnjo. Vse to omogoča boljšo produktivnost in učinkovitejše opravljanje obveznosti. Aplikacija je spletna in je tako na voljo na vseh vrstah naprav. 
Aplikacija StraightAS bo omogočala uporabo tudi brez registracije un prijave, torej neprijavljenim uporabnikom. V tem primeru bodo imeli dostop le do funkcionalnosti, ki bo izpisala seznam obveznosti, ki splošno veljajo za nek predmet, brez možnosti urejanja, dodajanja ali brisanja.
Prvi funkcionalnosti, s katerimi se bo neprijavljen, oziroma neregistran uporabnik srečal, bosta funkcionalnost Registracija in pa Prijava. Prva izmed teh funkcionalnosti bo omogočala novim uporabnikom, da se bodo registrirali in si s tem odprli možnost do vseh funkcionalnosti, ki jih aplikacija ponuja.
S pomočjo slednje funkcionalnosti, torej Prijave, pa bo uporabnik potrdil, da je že registriran in s tem dobil dostop do svojih obveznosti, jih urejal, brisal dodajal itd. Prijavljen uporabnik, se bo lahko tudi odjavil in s tem bil preusmerjen nazaj na stran za prijavo.
Glavne funkcionalnosti, ki bodo prijavljenemu uporabniku omogočale, da bo dejansko imel kakšne konkretne koristi od same aplikacije, pa bodo dodajanja, brisanje in pa urejanje obveznosti. Prijavljen uporabnik bo torej lahko dodal novo obveznost na svoj seznam obveznosti, zbrisal že obstoječo, ali pa jo preuredil. Prijavljen uporabnik bo imel tudi možnost arhiva. Ko bo torej izbrisal obveznost, bo sprva šla v arhiv iz katere bo lahko obveznost obnovil ali pa jo trajno izbrisal tudi iz arhiva.Vsekakor bo imel tudi možnost, da z izborom določene obveznosti preveri podrobnosti izbrane obveznosti, katere mu pri izpisu seznama obveznosti, niso prikazane. 
Aplikacija ima tudi funkcionalnost, ki uporabniku omogoča pregled nad vsemi njegovimi obveznostimi, prav tako tudi s pomočjo zunanjega sistema Google Calendar, omogoča uporabniku po dnevih
pregled nad celotnim mesecem, na kateri dan so kakšne obveznosti.Funkcionalnost Graf zasedenosti bo omogočala prijavljenemu učitelju, da lahko vidi, kako so zasedeni študentje po dnevih ali tednih. Skrbnik bo imel tudi možnost, da uporabnika v primeru zlorabe aplikacije blokira.  Nefunkcionalne zahteve naše aplikacie bodo imele poudarek še posebej na zahtevah izdelka. V prvi vrsti bo pomembno, da bo aplikacija dosegljiva dovolj odstotkov časa, da bo varna in da bo preprečevala nepooblaščene dostope do podatkov v podatkovni bazi. Prav tako bo pomembno, da bo dosegljiva na javno dostopnem spletnem naslovu in omogočila streči najmanj določeno število hkratnih prijavljenih uporabnikov. 
Vsekakor pa so pomembne tudi organizacijske zahteve in pa zunanje zahteve, še posebej tiste povezane z interoperabilnostjo. Aplikacija bo morala teči na vseh HTML brskalnikih in na vseh modernih napravah, torej na osebnem računalniku, tabličnem računalniku ali pametnem telefonu. 
Prav tako pa bo morala biti etično nesporna, temu primerne bodo morale biti tudi slikovne ikone, ki jih bo aplikacija uporabljala.

## 2. Uporabniške vloge

* **Neprijavjen uporabnik** lahko samo vidi obveznosti, ki veljajo splošno za nek predmet. Ne more si urejati ali dodajati rokov ali določiti prioritet.
* **Študent** ima na voljo vse obveznosti za predmet, kot na primer roke za oddajo nalog, izpitne roke in druge pomembne datume. Lahko si določa prioritete in se odjavlja od aktivnosti. Določene akcije mora potrditi učitelj.
* **Učitelj** je tisti, ki določa roke za oddajo nalog, izpitne roke in datume kolokvijev, zato mora potrditi odjavo in prijavo študenta. Lahko si dodaja tudi sestanke, in dogodke kot na primer konference, ki se jih želi udeležiti. Lahko določi prioritete obveznosti.
* **Skrbnik** ima vse pravice študenta in učitelja, lahko vpisuje nove študente in dodeljuje predmete učiteljem ter vzdržuje šifrante predmetov, študentov in učiteljev.


## 3. Slovar pojmov

* **Sistem** - aplikacija StraightAs
* **Obveznost** - beseda, ki zajema vse zadeve, ki so študentu ali pa učitelju pomembne in jih smatra kot nekaj, kar je od njega zahtevano(npr. predavanja, vaje, naloge itd...)
* **Izpit** - končni preizkus znanja, ki ga izvede učitelj, udeležijo pa se ga študentje
* **Predmet** - glavna aktivnost, ki jih študent na Fakulteti obiskuje. Vsak predmet ima svoje obveznosti, ki si jih študent dodaja
* **Naloga** - ena izmed študentovih obveznosti, ponavadi povezana z določenim predmetom
* **Izpitni rok** - ponavadi prvi, drugi, tretji(izpitni rok). Ponazarja skupino izpitov, ki se opravljajo v določenem časovnem razmaku.
* **Kolokvij** - vmesni preizkus znanja, ki ga izvede učitelj, udeležijo pa se ga študentje. Ponavadi pogoj ali pa nadomestilo izpita.
* **Sestanek** - posvetovanje skupine večih učiteljev
* **Funkcionalnost** - eden izmed načinov, na katerega lahko študent ali učitelj Aplikacijo StraightAs uporabi
* **Prioriteta za obveznost** - prednost določene obveznosti pred drugimi, zaradi pomembnosti. Prioriteto si določi uporabnik sam.
* **Registracija** - vpis podatkov novega uporabika v podatkovno bazo. Predpogoj za prijavo in dostop do ostalih funkcionalnosti
* **Prijava** - primerjanje podatkov vpisanih v podatkovni bazi, z podatki, ki jih uporabnik vnese v obrazec za prijavo. Predpogoj za urejanje, brisanje, dodajanje novih obveznosti.
* **Dodajanje obveznosti** - možnost, da si študent ali učiterj dodaja nove obveznosti
* **Urejanje obveznosti** - možnost, da si študent ali učitelj ureja že obstoječe obveznosti
* **Brisanje obveznosti** - možnost, da študent ali učitelj izbriše že obstoječe obveznosti
* **Prijavljen uporabnik** - študent ali učitelj
* **Odjavljen uporabnik** - uporabnik, ki je sice registiran, vendar v tem trenutku ni prijavljen.
* **Graf zasedenosti** - možnost, da učitelj pogleda zasedenost študentov
* **reCAPTCHA** - zunanji sistem za dodatno varnost pri prijavi in registraciji
* **KeyCloak** - zunanji sistem za dodatno varnost pri prijavi in registraciji
* **Moderne naprave** - opisuje skupino naprav kot so osebni računalnik, tablični računalnik, pametni telefon itd.
* **Pooblaščen dostop** - uporabnik ima pravice za dostop do določenih podatkov v podatkovni bazi
* **Nepooblaščen dostop** - uporabnik nima pravic za dostop do določenih podatkov v podatkovni bazi
* **Local storage** - prostor za podatke, ki so shranjeni in ostanejo na voljo po izhodu iz aplikacije

## 4. Diagram primerov uporabe

![Diagram primerov uporabe](../img/diagram_primerov_uporabe.png)

## 5. Funkcionalne zahteve

V tem razdelku podrobno opišite posamezne funkcionalnosti, ki jih vaša aplikacija ponuja svojim uporabnikom. Za vsako funkcionalnost navedite naslednje podatke:

### Registracija uporabnika
#### Povzetek funkcionalnosti
Neregistriran uporabnik se lahko registrira v sistem. Registrira se samo enkrat.

#### Osnovni tok
1. Neregistriram uporabnik izbere funkcionalnost Registracija.
2. Aplikacija prikaže registracijsi obrazec, ki ga neregistriram uporabnik mora izpolniti.
3. Neregistriram uporabnik izpolni obrazec.
4. Neregistriram uporabnik odda obrazec.
5. Aplikacija posreduje obrazec zunanjemu sistemu reCAPTCHA.
6. reCAPTCHA potrdi obrazec.
7. Aplikacija posreduje obrazec zunanjemu sistemu Keycloak, kjer se vpišejo uporabnikovi podatki.
8. Keycloak odgovori, da je registracija uspešna
9. Aplikacija prikaže sporočilo o uspešni registraciji.

#### Izjemni tokovi
* Neregistriran uporabnik je nepravilno izpolnil polja v obrazcu. Aplikacija prikaže ustrezno obvestilo.
* Elektronski naslov, s katerim se hoče uporabnik registrirat je že v bazi zunanjega sistema Keycloak. Aplikacija prikaže ustrezno obvestilo.
* Način izpolnjevanja obrazca sproži opozorilo iz zunanjega sistema reCAPTCHA. Aplikacija zaklene možnost registracija za 20 minut in pošlje obvestilo administratorju.

#### Pogoji 
Uporabnik mora biti neregistriran.

#### Posledice
Uporabnik je registriran in prijavljen v aplikacijo.

#### Posebnosti
?

#### Prioriteta - Must have

#### Sprejemni testi
| Primer uporabe   | Funkcija, ki se testira   | Začetno stanje sistema   | Vhod   | Pričakovan rezultat   |
| -------------- | ----------------------- | ---------------------- | ---- | ------------------- |
| Registracija uporabnika | pošiljanje obrazca sistemu Keycloack | Uporabnika ni v bazi | Izpoljnen registracijski obrazec | Uporabnik je v bazi |
| Nepravilno izpolnjena polja v obrazcu | Preverjanje pravilnosti izpolnjenih polj | Prazna polja | Izpolnjenja polja obrazca | Uporabniku se prikaže obvestilo o nepravilno izpolnjenih obrazcih |
| Že obstoječ email | Pošiljanje obrazca sistemu KeyCloak | Email je že v bazi | Že obstoječ email | Obvestilo, da email že obstaja v bazi zunanjega sistema KeyCloak |
| Registracija uporabnika | Pošiljanje obrazca sistemu reCAPTCHA | Neizpolnjen obrazec | Izpolnjen reCAPTCHA obrazec(nepravilno) | Možnost registracije zakljenjena za 20 minut |

### Prijava uporabnika
#### Povzetek funkcionalnosti
Neprijavljen uporabnik se lahko prijavi v aplikacijo. To je začetna stran.

#### Osnovni tok
1. Neprijavljen uporabnik izbere funkcionalnost Prijava.
2. Aplikacija prikaže prijavni obrazec, ki ga Neprijavljen uporabnik mora izpolniti.
3. Neprijavljen uporabnik izpolni obrazec.
4. Neprijavljen uporabnik odda obrazec.
5. Aplikacija posreduje obrazec zunanjemu sistemu reCAPTCHA.
6. reCAPTCHA potrdi obrazec.
7. Aplikacija posreduje obrazec zunanjemu sistemu Keycloak, kjer se preverijo uporabnikovi podatki.
8. Keycloak odgovori, da je prijava uspešna.
9. Aplikacija prikaže stran za prijavljene uporabnike.

#### Izjemni tokovi
* Neprijavljen uporabnik je nepravilno izpolnil polje za geslo v obrazcu. Aplikacija prikaže ustrezno obvestilo in ponudi možnost obnovitve gesla.
* Elektronski naslov, s katerim se hoče uporabnik prijaviti je še ni v bazi zunanjega sistema Keycloak. Aplikacija prikaže ustrezno obvestilo in ponudi možnost registracije.
* Več zaporednih neuspešnih poskusov prijave sproži opozorilo iz zunanjega sistema reCAPTCHA. Aplikacija zaklene možnost prijave za 20 minut in pošlje obvestilo administratorju.

#### Pogoji 
Uporabnik mora biti registriran in neprijavljen.

#### Posledice
Uporabnik je prijavljen v aplikacijo.

#### Posebnosti
?

#### Prioriteta - Must have

#### Sprejemni testi
| Primer uporabe   | Funkcija, ki se testira   | Začetno stanje sistema   | Vhod   | Pričakovan rezultat   |
| --------------   | -----------------------   | ----------------------   | ----   | -------------------   |
| Prijava uporabnika | Pošiljanje obrazca sistemu Keycloack | Uporabnika ni ppriavljen | Izpoljnen prijavitveni obrazec | Uporabnik je v priavljen |
| Prijava uporabnika | Preverjanje pravilonsti izpolnjenih polj | Prazno polje | Izpolnjenja polje za geslo| Uporabniku se prikaže obvestilo o nepravilno izpolnjenem polju za geslo |
| Prijava uporabnika | Pošiljanje obrazca sistemu KeyCloak | Emaila še ni v bazi | Email, ki še ni v bazi | Obvestilo, da email še ne obstaja v bazi zunanjega sistema KeyCloak in možnost registracije |
| Prijava uporabnika | Pošiljanje obrazca sistemu reCAPTCHA | Neizpolnjen obrazec | Večkrat nepravilno izpolnjen obrazec | Možnost prijave zakljenjena za 20 minut |


### Dodajanje obveznosti
#### Povzetek funkcionalnosti
Uporabnik s pravicami učitelja ali študenta doda novo obveznost.

#### Osnovni tok
1. Prijavljen uporabnik s pravicami učitelja ali študenta izbere funkcionalnost Dodaj na strani za prijavljene uporabnike.
2. Aplikacija prikaže obrazec za dodajanje, ki ga mora isti uporabnik izpolniti.
3. Ta uporabnik izpolni obrazec z nazivom obveznosti, datumom začetka in konca obveznosti, opcijskim časom v dnevu, opcijskim opisom in opcijsko prioriteto (lahko visoka, srednja ali nizka).
4. Ta uporabnik odda obrazec.
5. Aplikacija posreduje obrazec zunanjemu sistemu reCAPTCHA.
6. reCAPTCHA potrdi obrazec.
7. Podatki se pošljejo v podatkovno bazo aplikacije
8. Aplikacija prikaže stran za prijavljene uporabnike z posodobljenimi podatki.

#### Alternativni tok 1
1. Prijavljen uporabnik s pravicami učitelja ali študenta izbere funkcionalnost Dodaj na strani za prijavljene uporabnike.
2. Aplikacija prikaže obrazec za dodajanje, ki ga mora isti uporabnik izpolniti.
3. Ta uporabnik izpolni obrazec z nazivom obveznosti, datumom začetka in konca obveznosti, opcijskim časom v dnevu, opcijskim opisom in opcijsko prioriteto (lahko visoka, srednja ali nizka).
4. Ob dodajanju datuma začetka in konca obveznosti je uporabnik, ki dodaja obveznost izbral datum konca, ki je pred datumom začetka. Nastavi se tako, da je datum začetka prestavljen na isti dan kot datum konca in obveznost traja samo ta dan.
5. Ta uporabnik odda obrazec.
6. Aplikacija posreduje obrazec zunanjemu sistemu reCAPTCHA.
7. reCAPTCHA potrdi obrazec.
8. Podatki se pošljejo v podatkovno bazo aplikacije
9. Aplikacija prikaže stran za prijavljene uporabnike z posodobljenimi podatki.

#### Alternativni tok 2
1. Prijavljen uporabnik s pravicami učitelja ali študenta izbere funkcionalnost Dodaj na strani za prijavljene uporabnike.
2. Aplikacija prikaže obrazec za dodajanje, ki ga mora isti uporabnik izpolniti.
3. Ta uporabnik izpolni obrazec z nazivom obveznosti, datumom začetka in konca obveznosti, opcijskim časom v dnevu, opcijskim opisom in opcijsko prioriteto (lahko visoka, srednja ali nizka).
4. Ob dodajanju časa v dnevu obveznosti je uporabnik, ki dodaja obveznost izbral čas konca, ki je pred časom začetka. Nastavi se tako, da je čas začetka prestavljen na čas konca, čas konca pa je premaknjen za 30 minut naprej.
5. Ta uporabnik odda obrazec.
6. Aplikacija posreduje obrazec zunanjemu sistemu reCAPTCHA.
7. reCAPTCHA potrdi obrazec.
8. Podatki se pošljejo v podatkovno bazo aplikacije
9. Aplikacija prikaže stran za prijavljene uporabnike z posodobljenimi podatki.

#### Alternativni tok 3
1. Prijavljen uporabnik s pravicami učitelja ali študenta izbere funkcionalnost Dodaj na strani za prijavljene uporabnike.
2. Aplikacija prikaže obrazec za dodajanje, ki ga mora isti uporabnik izpolniti.
3. Ta uporabnik izpolni obrazec z nazivom obveznosti, datumom začetka in konca obveznosti, opcijskim časom v dnevu, opcijskim opisom in opcijsko prioriteto (lahko visoka, srednja ali nizka).
4. Ob dodajanju časa v dnevu obveznosti je uporabnik, ki dodaja obveznost izbral samo čas začetka. V tem primeru se čas konca postavi 30 minut po času začetka.
5. Ta uporabnik odda obrazec.
6. Aplikacija posreduje obrazec zunanjemu sistemu reCAPTCHA.
7. reCAPTCHA potrdi obrazec.
8. Podatki se pošljejo v podatkovno bazo aplikacije
9. Aplikacija prikaže stran za prijavljene uporabnike z posodobljenimi podatki.

#### Alternativni tok 4
1. Prijavljen uporabnik s pravicami učitelja ali študenta izbere funkcionalnost Dodaj na strani za prijavljene uporabnike.
2. Aplikacija prikaže obrazec za dodajanje, ki ga mora isti uporabnik izpolniti.
3. Ta uporabnik izpolni obrazec z nazivom obveznosti, datumom začetka in konca obveznosti, opcijskim časom v dnevu, opcijskim opisom in opcijsko prioriteto (lahko visoka, srednja ali nizka).
4. Ob dodajanju časa v dnevu obveznosti je uporabnik, ki dodaja obveznost izbral samo čas konca. V tem primeru je čas začetka enak času začetka.
5. Ta uporabnik odda obrazec.
6. Aplikacija posreduje obrazec zunanjemu sistemu reCAPTCHA.
7. reCAPTCHA potrdi obrazec.
8. Podatki se pošljejo v podatkovno bazo aplikacije
9. Aplikacija prikaže stran za prijavljene uporabnike z posodobljenimi podatki.

#### Izjemni tokovi
* Če se uporabnik, ki dodaja obveznost, odloči, da ne bo dodal ničesar, ima na voljo funcionalnost Prekliči, kar ga vrne na prejšnjo stran z nespremenjenimi podatki.

#### Pogoji 
Uporabnik mora biti registriran in prijavljen.

#### Posledice
Uporabniku se zabeleži dodana obveznost, ki jo lahko vidi, ureja ali briše.

#### Posebnosti
Če opcijski podatki niso vpisani, ne bodo navedeni.

#### Prioriteta - Must have

#### Sprejemni testi
| Primer uporabe   | Funkcija, ki se testira   | Začetno stanje sistema   | Vhod   | Pričakovan rezultat   |
| --------------   | -----------------------   | ----------------------   | ----   | -------------------   |
| Dodajanje obveznosti za uporabnika | Pošiljanje podatkov v podatkovno bazo| Obstječi podatki o uporabniku | Izpoljnen obrazec za dodajanje | Uporabnik ima zabeleženo novo obveznost |
| Preverjanje pravilne nastavitve, če je datum konca prej od datuma začetka | Smiselno nastavljane datumov | Obstječi podatki o uporabniku | Izpoljnen obrazec za dodajanje, kjer je datum konca prej od datuma začetka | Uporabnik ima zabeleženo novo obveznost s popravljeim datumom|
| Preverjanje pravilne nastavitve, če je čas konca v pred časom začetka | Smiselno nastavljanje časa| Obstječi podatki o uporabniku | Izpoljnen obrazec za dodajanje kjer je čas konca prej od časa začetka | Uporabnik ima zabeleženo novo obveznost s popravljenim časom |
| Preverjanje pravilne nastavitve, če je izbran samo čas začetka | Smiselno nastavljanje časa| Obstječi podatki o uporabniku | Izpoljnen obrazec za dodajanje kjer je izbran samo čas začetka | Uporabnik ima zabeleženo novo obveznost s popravljenim časom |
| Preverjanje pravilne nastavitve, če je izbran samo čas konca | Smiselno nastavljanje časa| Obstječi podatki o uporabniku | Izpoljnen obrazec za dodajanje kjer je izbran samo čas konca | Uporabnik ima zabeleženo novo obveznost s popravljenim časom |
| Preklic dodajanja obveznosti za uporabnika | Povrnitev prvotnih podatkov podatkov v podatkovni bazi| Obstječi podatki o uporabniku | Preklic pri izpoljnevanju obrazca za dodajanje | Vse mora biti enako, kot pred dodajanjem |

### Urejanje obveznosti
#### Povzetek funkcionalnosti
Uporabnik s pravicami učitelja ali študenta ureja  svojo obstoječo obveznost.

#### Osnovni tok
1. Prijavljen uporabnik s pravicami učitelja ali študenta izbere obstoječo obveznost na strani za prijavljene uporabnike.
2. Aplikacija prikaže obrazec za urejanje, ki ima vnešene podatke, ki so bili shranjeni ob dodajanju te obveznosti.
3. Ta uporabnik uredi obrazec z morebitnim novim nazivom obveznosti, datumom začetka in konca obveznosti, opcijskim časom v dnevu, opcijskim opisom in opcijsko prioriteto (lahko visoka, srednja ali nizka).
4. Ta uporabnik odda obrazec.
5. Aplikacija posreduje obrazec zunanjemu sistemu reCAPTCHA.
6. reCAPTCHA potrdi obrazec.
7. Podatki se posodobijo v podatkovni bazi aplikacije
8. Aplikacija prikaže stran za prijavljene uporabnike z posodobljenimi podatki.

#### Alternatovni tok 1
1. Prijavljen uporabnik s pravicami učitelja ali študenta izbere obstoječo obveznost na strani za prijavljene uporabnike.
2. Aplikacija prikaže obrazec za urejanje, ki ima vnešene podatke, ki so bili shranjeni ob dodajanju te obveznosti.
3. Ta uporabnik uredi obrazec z morebitnim novim nazivom obveznosti, datumom začetka in konca obveznosti, opcijskim časom v dnevu, opcijskim opisom in opcijsko prioriteto (lahko visoka, srednja ali nizka).
4. Ob urejanju datuma začetka in konca obveznosti je uporabnik, ki ureja obveznost izbral datum konca, ki je pred datumom začetka. Nastavi se tako, da je datum začetka prestavljen na isti dan kot datum konca in obveznost traja samo ta dan.
5. Ta uporabnik odda obrazec.
6. Aplikacija posreduje obrazec zunanjemu sistemu reCAPTCHA.
7. reCAPTCHA potrdi obrazec.
8. Podatki se posodobijo v podatkovni bazi aplikacije
9. Aplikacija prikaže stran za prijavljene uporabnike z posodobljenimi podatki.

#### Alternatovni tok 2
1. Prijavljen uporabnik s pravicami učitelja ali študenta izbere obstoječo obveznost na strani za prijavljene uporabnike.
2. Aplikacija prikaže obrazec za urejanje, ki ima vnešene podatke, ki so bili shranjeni ob dodajanju te obveznosti.
3. Ta uporabnik uredi obrazec z morebitnim novim nazivom obveznosti, datumom začetka in konca obveznosti, opcijskim časom v dnevu, opcijskim opisom in opcijsko prioriteto (lahko visoka, srednja ali nizka).
4. Ob urejanju časa v dnevu obveznosti je uporabnik, ki ureja obveznost izbral čas konca, ki je pred časom začetka. Nastavi se tako, da je čas začetka prestavljen na čas konca, čas konca pa je premaknjen za 30 minut naprej.
5. Ta uporabnik odda obrazec.
6. Aplikacija posreduje obrazec zunanjemu sistemu reCAPTCHA.
7. reCAPTCHA potrdi obrazec.
8. Podatki se posodobijo v podatkovni bazi aplikacije
9. Aplikacija prikaže stran za prijavljene uporabnike z posodobljenimi podatki.

#### Alternatovni tok 3
1. Prijavljen uporabnik s pravicami učitelja ali študenta izbere obstoječo obveznost na strani za prijavljene uporabnike.
2. Aplikacija prikaže obrazec za urejanje, ki ima vnešene podatke, ki so bili shranjeni ob dodajanju te obveznosti.
3. Ta uporabnik uredi obrazec z morebitnim novim nazivom obveznosti, datumom začetka in konca obveznosti, opcijskim časom v dnevu, opcijskim opisom in opcijsko prioriteto (lahko visoka, srednja ali nizka).
4. Ob urejanju časa v dnevu obveznosti je uporabnik, ki ureja obveznost izbral samo čas začetka. V tem primeru se čas konca postavi 30 minut po času začetka.
5. Ta uporabnik odda obrazec.
6. Aplikacija posreduje obrazec zunanjemu sistemu reCAPTCHA.
7. reCAPTCHA potrdi obrazec.
8. Podatki se posodobijo v podatkovni bazi aplikacije
9. Aplikacija prikaže stran za prijavljene uporabnike z posodobljenimi podatki.

#### Alternatovni tok 4
1. Prijavljen uporabnik s pravicami učitelja ali študenta izbere obstoječo obveznost na strani za prijavljene uporabnike.
2. Aplikacija prikaže obrazec za urejanje, ki ima vnešene podatke, ki so bili shranjeni ob dodajanju te obveznosti.
3. Ta uporabnik uredi obrazec z morebitnim novim nazivom obveznosti, datumom začetka in konca obveznosti, opcijskim časom v dnevu, opcijskim opisom in opcijsko prioriteto (lahko visoka, srednja ali nizka).
4. Ob urejanju časa v dnevu obveznosti je uporabnik, ki ureja obveznost izbral samo čas konca. V tem primeru je čas začetka enak času začetka.
5. Ta uporabnik odda obrazec.
6. Aplikacija posreduje obrazec zunanjemu sistemu reCAPTCHA.
7. reCAPTCHA potrdi obrazec.
8. Podatki se posodobijo v podatkovni bazi aplikacije
9. Aplikacija prikaže stran za prijavljene uporabnike z posodobljenimi podatki.

#### Izjemni tokovi
* Če se uporabnik, ki dodaja obveznost, odloči, da ne bo  spremenil ničesar, ima na voljo funcionalnost Prekliči, kar ga vrne na prejšnjo stran z nespremenjenimi podatki.

#### Pogoji 
Uporabnik mora biti registriran in prijavljen. Mora obstajati vsaj ena obveznost, ki se lahko ureja.

#### Posledice
Uporabniku se posodobijo podatki izbrane obveznosti, ki jo lahko vidi, ureja ali briše.

#### Posebnosti
Če opcijski podatki niso vpisani, ne bodo navedeni.

#### Prioriteta - Must have

#### Sprejemni testi
| Primer uporabe   | Funkcija, ki se testira   | Začetno stanje sistema   | Vhod   | Pričakovan rezultat   |
| --------------   | -----------------------   | ----------------------   | ----   | -------------------   |
| Urejanje obveznosti za uporabnika | Posodabljanje podatkov v podatkovni bazi| Obstječi podatki o uporabniku | Posodobljen obrazec za dodajanje | Uporabnik ima posodobljene podatke o izbrani obveznosti |
| Preverjanje pravilne nastavitve, če je pri urejanju datum konca prej od datuma začetka | Smiselno nastavljane datumov | Obstječi podatki o uporabniku | Posodobljen obrazec za dodajanje, kjer je datum konca prej od datuma začetka | Uporabnik ima posodobljene podatke o izbrani obveznosti s popravljeim datumom|
| Preverjanje pravilne nastavitve, če je pri urejanju čas konca v pred časom začetka | Smiselno nastavljanje časa| Obstječi podatki o uporabniku | Posodobljen obrazec za dodajanje kjer je čas konca prej od časa začetka | Uporabnik ima posodobljene podatke o izbrani obveznosti s popravljenim časom |
| Preverjanje pravilne nastavitve, če je pri urejanju izbran samo čas začetka | Smiselno nastavljanje časa| Obstječi podatki o uporabniku | Posodobljen obrazec za dodajanje kjer je izbran samo čas začetka | Uporabnik ima posodobljene podatke o izbrani obveznosti s popravljenim časom |
| Preverjanje pravilne nastavitve, če je pri urejanju izbran samo čas konca | Smiselno nastavljanje časa| Obstječi podatki o uporabniku | Posodobljen obrazec za dodajanje kjer je izbran samo čas konca | Uporabnik ima posodobljene podatke o izbrani obveznosti s popravljenim časom |
| Preklic urejanja obveznosti za uporabnika | Povrnitev prvotnih podatkov podatkov v podatkovni bazi| Obstječi podatki o uporabniku | Preklic pri posodabljanju obrazca za dodajanje | Vse mora biti enako, kot pred urejanjem |

### Brisanje obveznosti

#### Povzetek funkcionalnosti
Uporabnik s pravicami učitelja ali študenta izbriše svojo obstoječo obveznost in podatke o njej lahko najde v arhivu.

#### Osnovni tok
1. Prijavljen uporabnik s pravicami učitelja ali študenta izbere funkcionalnost Izbriši za obstoječo obveznost na strani za prijavljene uporabnike.
2. Aplikacija prikaže okno, ki vsebuje vprašanje, če je uporabnik prepričan o izbrisu z opozorilom, da bodo podatki premaknjeni v arhiv.
3. Ta uporabnik potrdi izbris.
4. Podatki o obveznosti se arhivirajo.
5. Podatki se posodobijo v podatkovni bazi aplikacije
6. Aplikacija prikaže stran za prijavljene uporabnike, kjer ni več izbrisane obveznosti.

#### Izjemni tokovi
* Če se uporabnik, ki briše obveznost, odloči, obveznosti ne bo izbrisal lahko v oknu, ki je v osnovnem toku pod točko 2 ibere možnost Prekliči, kar povzroči, da aplikacija prikaže stran za prijavljene uporabnike z nespremenjenimi podatki.

#### Pogoji 
Uporabnik mora biti registriran in prijavljen. Mora obstajati vsaj ena obveznost, ki se lahko izbriše.

#### Posledice
podatki izbrane obveznosti so premaknjeni v arhiv, obveznost pa je izbrisana.

#### Posebnosti
/

#### Prioriteta - Must have

#### Sprejemni testi
| Primer uporabe   | Funkcija, ki se testira   | Začetno stanje sistema   | Vhod   | Pričakovan rezultat   |
| --------------   | -----------------------   | ----------------------   | ----   | -------------------   |
| Brisanje obveznosti za uporabnika in premik v arhiv| Posodabljanje podatkov v podatkovni bazi z brisanjem| Obstječi podatki o uporabniku | izbrana obveznost| Obveznost je izbrisana, podatki o njej so v arhivu|
| Preklic brisanja obveznosti za uporabnika | Povrnitev prvotnih podatkov podatkov v podatkovni bazi| Obstječi podatki o uporabniku | Preklic pri potrjevanju brisanja izbrane obveznosti | Vse mora biti enako, kot pred skorajšnjim brisanjem |

### Pregled seznama obveznosti


#### Povzetek funkcionalnosti

Prijavljen uporabnik ima lahko pregled nad svojimi obveznostmi


#### Osnovni tok

1. Uporabniku se po prijavi prikaže stran za prijavljene uporabnike
2. Prijavljen uporabnik s pravicami učitelja ali študenta ima na tej strani pregled na svojimi obveznostmi in možnost dodajanja novih
3. Prijavljen uporabnik s pravicami učitelja ali študenta izbere določeno obveznost
4. Aplikacija tega uporabnika preusmeri na funkcionalnost Podrobnosti obveznosti


**Alternativni tok 1**

1. Uporabniku se po prijavi prikaže stran za prijavljene uporabnike
2. Prijavljen uporabnik s pravicami učitelja ali študenta ima na tej strani pregled na svojimi obveznostmi in možnost dodajanja novih
3. Prijavljen uporabnik s pravicami učitelja ali študenta izbere določeno obveznost
4. Prijavljen uporabnik izbere opcijo izbrisa izbrane obveznost
5. Aplikacija tega uporabnika preusmeri na funkcionalnost Brisanje obveznosti

**Alternativni tok 2**

1. Uporabniku se po prijavi prikaže stran za prijavljene uporabnike
2. Prijavljen uporabnik s pravicami učitelja ali študenta ima na tej strani pregled na svojimi obveznostmi in možnost dodajanja novih
3. Prijavljen uporabnik s pravicami učitelja ali študenta izbere določeno obveznost
4. Prijavljen uporabnik izbere opcijo urejanja izbrane obveznost
5. Aplikacija tega uporabnika preusmeri na funkcionalnost Urejanje obveznosti

**Alternativni tok 3**

1. Uporabniku se po prijavi prikaže stran za prijavljene uporabnike
2. Prijavljen uporabnik s pravicami učitelja ali študenta ima na tej strani pregled na svojimi obveznostmi in možnost dodajanja novih
3. Prijavljen uporabnik s pravicami učitelja ali študenta izbere opcijo dodajanja nove obveznosti
5. Aplikacija tega uporabnika preusmeri na funkcionalnost Dodajanje obveznosti

**Izjemni tokovi**

* Uporabnik nima dodane še nobene obveznosti, zato je seznam prazen

#### Pogoji

Uporabnik mora biti registriran in prijavljen.


#### Posledice

Prijavljen uporabnik vidi svoje obveznosti

#### Posebnosti

Brez posebnosti


#### Prioriteta - Must have



#### Sprejemni testi
| Primer uporabe   | Funkcija, ki se testira   | Začetno stanje sistema   | Vhod   | Pričakovan rezultat   |
| --------------   | -----------------------   | ----------------------   | ----   | -------------------   |
| Pregled nad obveznostmi | Pridobivanje podatkov iz podatkovne baze | Prazen seznam obveznosti| Prijavljen uporabnik | Izpis vseh obveznosti prijavljenega uporabnika|
| Izbira opcije dodajanja obveznosti | Funkcionalonost dodajanja obveznosti | Obveznosti, ki so bile prisotne že prej | Nova obveznost | Dodana nova obveznost |
| Izbira opcije izbrisa obveznosti | Funkcionalnost izbrisa obveznosti | Obveznosti, ki so bile prisotne že prej | Izbrana obveznost | Izbrisana obveznost |
| Izbira opcije urejanja obveznosti | Funkcionalnost urejanja obveznosti | Že obstoječ opis obveznosti | Izbrana obveznost | Preurejena obveznost |
| Pregled nad obveznostmi(prazen seznam) | Pridobivanje podatkov iz podatkovne baze | Prazen seznam obveznosti | Prijavljen uporabnik | Prazen seznam obveznosti |


### Google calendar


#### Povzetek funkcionalnosti

Prijavljen uporabnik lahko preko Google koledarja vidi na koledarju označene datume svojih obveznosti


#### Osnovni tok

1. Prijavljen uporabnik s pravicami učitelja ali študenta izbere funkcionalnost Google Calendar na strani za prijavljene uporabnike
2. Aplikacija prijavljenega uporabika preusmeri na zunanjo storitev Google Calendar, kjer se mora prijaviti s svojim Google računom
3. Prijavljen uporabnik izpolni obrazec za prijavo(vpiše svoj email in geslo)
4. Google Calendar potrdi obrazec
5. Aplikacija prikaže zunanjo storitev Google Calendar

**Alternativni tok 1**

1. Prijavljen uporabnik s pravicami učitelja ali študenta izbere funkcionalnost Google Calendar na strani za prijavljene uporabnike
2. Aplikacija prijavljenega uporabika preusmeri na zunanjo storitev Google Calendar, kjer se mora prijaviti s svojim Google računom
3. Prijavljen uporabnik izpolni obrazec za prijavo(vpiše svoj email in geslo)
4. Google Calendar prijavljenemu uporabniku javi, da je geslo napačno
5. Prijavljen uporabnik ponovno vnese email in svoje(pravilno) geslo
6. Google Calendar potrdi obrazec
7. Aplikacija prikaže zunanjo storitev Google Calendar

**Alternativni tok 2**

1. Prijavljen uporabnik s pravicami učitelja ali študenta izbere funkcionalnost Google Calendar na strani za prijavljene uporabnike
2. Aplikacija prijavljenega uporabika preusmeri na zunanjo storitev Google Calendar, kjer se mora prijaviti s svojim Google računom
3. Prijavljen uporabnik izpolni obrazec za prijavo(vpiše svoj email in geslo)
4. Google Calendar prijavljenemu uporabniku javi, da email ne obstaja
5. Prijavljen uporabnik ponovno vnese svoje email(pravi) in geslo
6. Google Calendar potrdi obrazec
7. Aplikacija prikaže zunanjo storitev Google Calendar


**Izjemni tokovi**

* Prijavljen uporabnik pozabi svoje geslo za Google račun, zato prekliče prijavo v Google, aplikacija uporabnika preusmeri nazaj na pregled obveznosti

#### Pogoji

Uporabnik mora biti registriran in prijavljen in imeti Google račun


#### Posledice

Uporabnik vidi na Google koledarju, katere obveznosti so ob določenih dnevih

#### Posebnosti

Če uporabnik nima Google računa, si ga mora najprej ustvariti.


#### Prioriteta - Would have



#### Sprejemni testi
| Primer uporabe   | Funkcija, ki se testira   | Začetno stanje sistema   | Vhod   | Pričakovan rezultat   |
| --------------   | -----------------------   | ----------------------   | ----   | -------------------   |
| Pregled nad obveznostmi v Google koledarju | Posredovanje podatkov zunanjemu sistemu | Obstoječe obveznosti v Google Calendarju | Izpolnjen obrazec za prijavo v Google Calendar | Obveznosti po datumih v Google Calendarju |
| Pregled nad obveznostmi v Google koledarju(napačno geslo) | Posredovanje podatkov zunanjemu sistemu | Obstoječe obveznosti v Google Calendarju | Izpolnjen obrazec za prijavo v Google Calendar | Obveznosti po datumih v Google Calendarju |
| Pregled nad obveznostmi v Google koledarju(napačni email) | Posredovanje podatkov zunanjemu sistemu | Obstoječe obveznosti v Google Calendarju | Izpolnjen obrazec za prijavo v Google Calendar | Obveznosti po datumih v Google Calendarju |
| Prekinjena prijava v Google račun | Preklic zunanjega sistema in vrnitev na pregled obveznosti | Obstoječ pregled obveznosti | Izbira preklica povezave na Google Calendar | Vrnitev na obstoječ pregled obveznosti|


### Podrobnosti obveznosti


#### Povzetek funkcionalnosti

Prijavljen uporabnik lahko preveri podrobnosti izbrane obveznosti


#### Osnovni tok

1. Prijavljenemu uporabniku se po prijavi prikaže stran za prijavljene uporabnike
2. Prijavljen uporabnik s pravicami učitelja ali študenta ima na tej strani pregled na svojimi obveznostmi 
3. Prijavljen uporabnik s pravicami učitelja ali študenta izbere določeno obveznost
4. Aplikacija temu uporabniku prikaže podrobnosti obveznosti, torej naziv, čas, opis, datum začetka, datum konca ter prioriteto
5. Prijavljen uporabnik lahko klikne na gumb nazaj, aplikacija ga preusmeri na pregled vseh obveznosti



**Izjemni tokovi**

* Prijavljen uporabnik pri dodajanju obveznosti ni definiral katerega izmed neobveznih polj, zato mu funkcionalnost Podrobnosti obveznosti prikaže le tiste podatke, ki so bili pri dodajanju obveznosti podani

#### Pogoji

Prijavljen uporabnik mora biti registriran in prijavljen.


#### Posledice

Prijavljen uporabnik vidi podrobnosti svoje obveznosti

#### Posebnosti

Prijavljen uporabnik mora pri dodajanju obveznosti izpolniti vas polja, če želi imeti vse podrobnosti o obveznosti.


#### Prioriteta - Must have



#### Sprejemni testi
| Primer uporabe   | Funkcija, ki se testira   | Začetno stanje sistema   | Vhod   | Pričakovan rezultat   |
| --------------   | -----------------------   | ----------------------   | ----   | -------------------   |
| Podrobnosti obveznosti | Pridobivanje podatkov iz podatkovne baze | Funkcionalnost pregled obveznosti | Izbrana obveznost| Prikaz vseh podrobnosti o obveznosti |
| Podrobnosti obveznosti | Pridobivanje podatkov iz podatkovne baze | Funkcionalnost pregled obveznosti | Izbrana obveznost | Prikaz le določenih podrobnosti o obveznosti |


### Graf zasedenosti


#### Povzetek funkcionalnosti

Prijavljen učitelj lahko vidi, kako so zasedeni študentje po dnevih ali tednih


#### Osnovni tok

1. Prijavljenemu uporabniku se po prijavi prikaže stran za prijavljene uporabnike
2. Prijavljen uporabnik s pravicami učitelja iz menija izbere graf zasedenosti
3. Aplikacija temu uporabniku prikaže graf po dnevih, pokaže zasedenost študentov na določeni dan

#### Alternatovni tok 1

1. Prijavljenemu uporabniku se po prijavi prikaže stran za prijavljene uporabnike
2. Prijavljen uporabnik s pravicami učitelja iz menija izbere graf zasedenosti
4. Prijavljen uporabnik pritisne na gumb teden
3. Aplikacija temu uporabniku prikaže graf po tednih, pokaže zasedenost študentov na določen teden

**Izjemni tokovi**

* Ni vnešenih še nobenih obveznosti grafi so prazni

#### Pogoji

Prijavljen uporabnik mora biti registriran in prijavljen kot učitelj.


#### Posledice

Prijavljen uporabnik vidi zasedenost po dnevih ali tednih.

#### Posebnosti

Dodanih more biti že nekaj obveznosti, da se lahko vidi zasedenost.


#### Prioriteta - Could have


#### Sprejemni testi
| Primer uporabe   | Funkcija, ki se testira   | Začetno stanje sistema   | Vhod   | Pričakovan rezultat   |
| --------------   | -----------------------   | ----------------------   | ----   | -------------------   |
| Graf zasedenosti | Pridobivanje podatkov iz podatkovne baze | Graf zasedenosti | Dnevi | Prikaz grafa deljenega po dnevih |
| Graf zasedenosti | Pridobivanje podatkov iz podatkovne baze | Graf zasedenosti | Tedni | Prikaz grafa deljenega po tednih |



### Blokiranje uporabnika


#### Povzetek funkcionalnosti

Prijavljen kot skrbnik lahko vidi uporabnike in jih blokira v primeru zlorabe aplikacije


#### Osnovni tok

1. Prijavljenemu uporabniku se po prijavi prikaže stran za prijavljene uporabnike
2. Prijavljen uporabnik s pravicami skrbnika iz menija izbere uporabniki
3. Aplikacija temu uporabniku prikaže vse uporabnike
4. Prijavljen uporabnik s pritiskom na gumb blokiraj
5. Aplikacija mu pokaže pojavno okno z obvestilom "Ali res želite blokirati uporabnika" in gumbi ne in da
6. Prijavljen uporabnik izbere gumb da
7. Aplikacija odvzame dostop izbranemu uporabniku
8. Aplikacija temu uporabniku prikaže vse uporabnike

#### Alternatovni tok 1

1. Prijavljenemu uporabniku se po prijavi prikaže stran za prijavljene uporabnike
2. Prijavljen uporabnik s pravicami skrbnika iz menija izbere uporabniki
3. Aplikacija temu uporabniku prikaže vse uporabnike
4. Prijavljen uporabnik s pritiskom na gumb blokiraj
5. Aplikacija mu pokaže pojavno okno z obvestilom "Ali res želite blokirati uporabnika" in gumbi ne in da
6. Prijavljen uporabnik izbere gumb ne
7. Aplikacija ne odvzame dostop izbranemu uporabniku
8. Aplikacija temu uporabniku prikaže vse uporabnike


**Izjemni tokovi**

* Blokiranje svojega računa

#### Pogoji

Prijavljen uporabnik mora biti registriran in prijavljen kot skrbnik.


#### Posledice

Izbran uporabnik se ne more več prijaviti

#### Posebnosti

Izbran uporabnik je trenutno prijavljen


#### Prioriteta - Could have


#### Sprejemni testi
| Primer uporabe   | Funkcija, ki se testira   | Začetno stanje sistema   | Vhod   | Pričakovan rezultat   |
| --------------   | -----------------------   | ----------------------   | ----   | -------------------   |
| Blokiranje uporabnika | Pridobivanje podatkov iz podatkovne baze | Seznam uporabnikov | Blokiraj | Prikaz pojavnega okna |
| Blokiranje uporabnika | Pridobivanje podatkov iz podatkovne baze | Pojavno okno | Da | Uporabnik je blokiran |
| Blokiranje uporabnika | Pridobivanje podatkov iz podatkovne baze | Pojavno okno | Ne | Uporabnik ni blokiran|


### Obnovitev obveznosti iz arhiva
#### Povzetek funkcionalnosti
Uporabnik s pravicami šudenta ali učitelja po izbrisu svoje obveznisti lahko to obveznost obnovi iz arhiva.

#### Osnovni tok
1. Pijavljen uporabnik izbere funkcionalnost Arhiv.
2. V arhivu je seznam izbrisanih obveznosti z nazivom in datumom izbrisa, iz česar uporabik izbere finkcionalnost Obnovi pri obveznosti, ki jo želi obnoviti.
3. Pojavno okno uporabnika vpraša o potrditvi obnovitve
4. Po potrditvi aplikacija še vedno prikazuje arhiv, pojavi pa se tudi obvestilo o uspešni obnovitvi.
5. Če se uporabnik vrne na glavno stran za prijavljene uporabnike, lahko vidi med ostalimi tudi obnovljeno obveznost z enakimi podatki, kot pred njenim izbisom.

#### Izjemni tokovi
* Pri potrjevanju obnovitve uporabnik prekliče proces

#### Pogoji 
Uporabnik mora biti prijavljen in mora imeti v svojem arhivu vsaj eno obveznost, ki jo je izbrisal.

#### Posledice
Obveznost, ki je bila izbisana je obnovljena in se prikaže v aplikaciji kot pred izbrisom.

#### Posebnosti
/

#### Prioriteta - Could have

#### Sprejemni testi
| Primer uporabe   | Funkcija, ki se testira   | Začetno stanje sistema   | Vhod   | Pričakovan rezultat   |
| -------------- | ----------------------- | ---------------------- | ---- | ------------------- |
| Obnovitev izbrisane obveznosti iz arhiva uporabnika | Obnavljanje podatkov iz metapodatkov shranjenih v 'local storage' | Obstoječi podatki o uporabniku | Izbrani podatki iz arhiva | Obveznost je znova na voljo, podatki so v podatkovni bazi |
| Preklic obnovitve obveznosti iz arhiva uporabnika | Povrnitev prvotnih podatkov v podatkovni bazi in 'local storage' | Obstječi podatki o uporabniku | Preklic pri potrjevanju obnovitve obveznosti iz arhiva | Vse mora biti enako, kot pred skorajšnjo obnovitvijo |

### Trajen izbris obveznosti iz arhiva
#### Povzetek funkcionalnosti
Uporabnik s pravicami šudenta ali učitelja po izbrisu svoje obveznisti lahko to obveznost tudi trajno izbriše iz arhiva.

#### Osnovni tok
1. Pijavljen uporabnik izbere funkcionalnost Arhiv.
2. V arhivu je seznam izbrisanih obveznosti z nazivom in datumom izbrisa, iz česar uporabik izbere finkcionalnost Izbriši pri obveznosti, ki jo želi trajno ibrisati.
3. Pojavno okno uporabnika vpraša o potrditvi izbrisa z opozorilom, da bo trajno izgubil podatke. 
4. Po potrditvi aplikacija še vedno prikazuje arhiv, pojavi pa se tudi obvestilo o uspešnem izbrisu.

#### Izjemni tokovi
* Pri potrjevanju trajnega izbrisa uporabnik prekliče proces

#### Pogoji 
Uporabnik mora biti prijavljen in mora imeti v svojem arhivu vsaj eno obveznost, ki jo je izbrisal.

#### Posledice
Podatki o obveznosti, ki je bila v arhivu so trajno izbrisani in niso več na voljo.

#### Posebnosti
/

#### Prioriteta - Could have

#### Sprejemni testi
| Primer uporabe   | Funkcija, ki se testira   | Začetno stanje sistema   | Vhod   | Pričakovan rezultat   |
| -------------- | ----------------------- | ---------------------- | ---- | ------------------- |
| Trajni izbris obveznosti iz arhiva uporabnika | Brisanje metapodatkov shranjenih v 'local storage' | Obstoječi podatki o uporabniku | Izbrani podatki iz arhiva | Podatki o obveznosti so trajno izbrisani |
| Preklic izbrisa obveznosti iz arhiva uporabnika | Ohranjanje  podatkov v 'local storage' | Obstječi podatki o uporabniku | Preklic pri potrjevanju izbrisa obveznosti iz arhiva | Vse mora biti enako, kot pred skorajšnjim trajnim izbisom |

### Odjava uporabnika
#### Povzetek funkcionalnosti
Prijavljen uporabnik se lahko odjavi

#### Osnovni tok
1. Pijavljen uporabnik izbere funkcionalnost odjava
2. Aplikacija tega uporabnika odjavi
3. Odjavljen uporabnik je tako preusmerjen iz pregleda njegovih obveznosti na funkcionalnost Prijava, kjer se lahko ponovno prijavi

#### Izjemni tokovi
* Pri izbiri odjave prijavljen uporabnik izbere opcijo prekliči in odjavo prekine

#### Pogoji 
Uporabnik mora biti prijavljen 

#### Posledice
Uporabnik je odjavljen

#### Posebnosti
/

#### Prioriteta - Must have

#### Sprejemni testi
| Primer uporabe   | Funkcija, ki se testira   | Začetno stanje sistema   | Vhod   | Pričakovan rezultat   |
| -------------- | ----------------------- | ---------------------- | ---- | ------------------- |
| Odjava uporabnika | Sprememba pravic iz prijavljenega uporabnika, na odjavljenega uporabnika | Prijavljen uporabnik na pregledu svojih obevznosti | Izbrana odjava | Odjavljen uporabnik in preusmeritev na funkcionalnost Prijava |
| Odjava uporabnika(preklic) | Preklic odjave | Prijavljen uporabnik | Preklic pri odjavi | Prijavljen uporabnik ostane prijavljen na pregledu svojih obveznosti |


## 6. Nefunkcionalne zahteve

**Zahteve izdelka**

* Aplikacija StraightAs mora biti na voljo 99,9 odstotkov časa
* Aplikacija StraightAs mora biti varna, v smislu, da ne sme uporabniku omogočiti dostop do podatkov, za katere nima pooblaščenega dostopa
* Samo skrbnik lahko vpisuje nove študente ter vzdržuje šifrante predmetov, študentov in učiteljev
* Aplikacija StraightAs mora biti dosegljiva na javno dostopnem spletnem naslovu
* Aplikacija StraightAs mora biti zmožna streči najmanj 1000 hkratno prijavljenim uporabnikom
* Aplikacija StraightAs mora vsebovati 5 GB prostora v 'local storage' za potrebe arhiva 

**Organizacijske zahteve**

* Aplikacija StraightAs mora biti  primerna za vse študente in učitelje po Sloveniji
* Zapisi v podatkovno bazo, bodo shranjeni pod časom, ki bo ustrezal časovnem pasu, v katerem se nahaja Slovenija

**Zunanje zahteve**

* Aplikacija StraightAs mora biti prilagojena za uporabo na vseh napravah(PC, tablični računalnik, pametni telefon...)  
* Aplikacijski uporabniški vmesnik mora biti prilagojen za katerikoli HTML brskalnik  
* Aplikacija StraightAs ne sme uporabljati slikovnih ikon, ki bi jih kdorkoli od morebitnih uporabnikov smatral za žaljive  



## 7. Prototipi vmesnikov

### Zaslonske maske

#### Maska registracije
![Maska registracije](../img/register_wireframe.png)
#### Maska prijave
![Maska prijave](../img/login_wireframe.png)
#### Maska pregleda obveznosti
![Maska pregled](../img/pregled_wireframe.png)
#### Maska podrobnosti obveznosti
![Maska podrobnosti](../img/podrobnosti_wireframe.png)
#### Maska dodajanja obveznosti
![Maska dodajanje](../img/dodajanje_wireframe.png)
#### Maska urejanja obveznosti
![Maska urejanje](../img/urejanje_wireframe.png)
#### Maska brisanja obveznosti
![Maska brisanje](../img/brisanje_wireframe.png)
#### Maska koledarja obveznosti
![Maska koledar](../img/koledar_wireframe.png)
#### Maska obnavjanja obveznosti iz arhiva
![Maska obnavlajnje](../img/obnovi_wireframe.png)
#### Maska brisanja obveznosti iz arhiva
![Maska izbrisanje](../img/izbris_wireframe.png)
#### Maska blokiranja osebe
![Maska blokiranje](../img/blokiranje_wireframe.png)
#### Maska zasedenosti študentov
![Maska zasedenost](../img/zasedenost_wireframe.png)
#### Maska odjave uporabnikov
![Maska odjava](../img/odjava_wireframe.png)


### Vmesniki do zunanjih sistemov
 
 **KeyCloak**
 Sprejme podatke za vpis, uporabniško ime in geslo, vrne pa access in refresh token.
  
Poslati je potrebno:
* grand_type: "password"
* client_id
* username
* password

Pri uspešnem odgovoru dobimo:
* access_token
* refresh_token

**reCAPTCHA**

Uporablja se za varnost pred "Brute-force attack", poslje nam sliko z znaki in hash-om ko se registriramo ali prijavimo preveri ali smo vnesli enake znake

CAPTCHA create:
* vrne sliko in hash znakov na sliki.

CAPTCHA comparison:
* poslati je potrebno vnesene znake
* preveri ali so enaki kot generiran hash
* odgovori ali je prav ali ne

**Google Calendar**

Uporaba za prikaz koledarja.

View Calendar:
* pošljemo zahtevek za koledar
* vrne koledar

Scroll date:
* pošljemo datum
* vrne nam koledar za izbran datum
 