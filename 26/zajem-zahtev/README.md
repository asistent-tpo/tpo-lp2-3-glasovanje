# Dokument zahtev

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Jan Adamič, Jan Lovšin, Mihael Rajh, Dejan Sinic |
| **Kraj in datum** | Ljubljana, 07. 04. 2019 |



## Povzetek projekta

Dokument vsebuje natančen opis vlog v aplikaciji, podrobno opredeljene ciljne funkcionalnosti, njihovi tokovi uporabe ter sprejemni testi. Ti podatki so strnjeni v prikazu diagrama primerov uporabe. Poleg tega vsebuje še zasnovane zaslonske maske vsake funkcionalnosti. Vsebovan je še slovar z uporabljenimi izrazi, in opredelitev vmesnika do zunanjega sistema.

## 1. Uvod

Študentje se vsakodnevno srečujejo s težavami pri organizaciji časa med opravljanjem fakultetnih obveznosti. Posledično trpijo njihove ocene, razpoloženje in socialni stiki. Pogostokrat si je težko zapomniti vse obveznosti, saj imajo pri vsakem predmetu izpitne roke, oddajne roke za domače naloge, datume kolokvijev in še veliko ostalih aktivnosti poleg teh. To problematiko poskušamo rešiti z našo aplikacijo StraightAs, ki je namenjena predvsem študentom za lažji pregled nad vsemi aktivnostmi na enem mestu. Aplikacijo lahko uporabljajo tudi osebe, ki niso študenti, a imajo podobne preglavice s časovno organizacijo vseh njihovih obveznosti. Uporabnikom nudimo dodajanje enkratnih obveznosti, ki imajo specifičen datum, do katerega morajo biti opravljene, kot tudi ponovljive obveznosti, kot so predavanja in vaje na fakulteti, ki se tedensko ponavljajo. Uporabniki tako pridobijo pregled nad urnikom, na katerem so razvidne vse njihove obveznosti in roki za specifičen dan. Poleg tega lahko aktivnosti tudi večkrat ocenijo, in si s tem beležijo njihov sproten uspeh. Posebej nudimo pregled nad ocenami, kjer je jasno razvidno, kateri aktivnosti pripada določena ocena. Posebni, privilegirani uporabniki pa so predstavniki fakultet, ko smo jih sami preverili in zanje zagotovili, da res zastopajo določeno fakulteto. Ti lahko dodajajo tudi javne aktivnosti, kot so predavanja in izpiti, katere lahko nato ostali uporabniki le izberejo s seznama z minimalnim trudom. Po želji uporabnika si lahko obveznosti tudi uvozi iz Google koledarja, prav tako pa se lahko tudi v samo aplikacijo prijavi kar z računom Google. Dodaten pogled, ki ga nudimo uporabniku, je TO-DO seznam, kjer so navedene vse njegove neopravljene obveznosti. Med njimi si lahko izbere tiste, ki se mu trenutno zdijo najbolj kritične, in jih doda med trenutne obveznosti. Skupno je naš cilj študentom olajšati organizacijske preglavice s preprosto in intuitivno aplikacijo.

## 2. Uporabniške vloge

#### Gost
Gost je uporabnik, ki na spletni aplikaciji StraightAs ni prijavljen. Gost je kdorkoli, ki je namenoma ali po naključju obiskal spletno stran, vendar na njej nima uporabniškega računa, oz. v tega trenutno ni prijavljen. Gost se lahko prijavi ali registrira.

#### Študent
Študent je uporabnik, ki je na spletni aplikaciji StraightAs prijavljen v svoj uporabniški račun. Študent je glavni uporabnik aplikacije, ki lahko dostopa do večine njenih funkcionalnosti. Študent ne potrebuje predhodnega znanja o sistemu ali posebnih dovoljenj, študent lahko postane vsak gost, ki se je na spletni strani registriral in prijavil. Posledično lahko vlogo študenta pridobi tudi oseba, ki dejansko ne obiskuje fakultete. Študentu lahko vlogo odvzame administrator z izbrisom uporabniškega računa. Uporabniški račun si lahko izbriše tudi sam.

#### Predstavnik fakultete
Predstavnik fakultete je študent, ki je bil povzdignjen v to vlogo s strani administratorja. Predstavniku fakultete so na voljo vse funkcionalnosti, do katerih ima dostop študent, poleg tega pa lahko izvaja funkcije, kot je določanje javnih aktivnosti v povezavi z določeno fakulteto, in pošiljanje javnih obvestil (broadcastov) vsem, ki so si dodali neko njihovo aktivnost. Administrator, ki določi študenta kot predstavnika fakultete zagotavlja, da je ta oseba resnični študent fakultete, ki jo predstavljala, in odgovorna. Izjemoma lahko to vlogo pridobi tudi profesor fakultete. Predstavniku fakultete lahko vlogo odvzame administrator, bodisi s znižanjem na vlogo študenta, bodisi z izbrisom uporabniškega računa.

#### Administrator
Administrator je uporabniška vloga namenjena le osebam, ki aplikacijo vzdržujejo in moderirajo. Začetni administratorji so vsi sodelujoči pri izdelavi in testiranju aplikacije. Nove administratorje lahko določijo že obstoječi administratorji. Administratorju vloge ne more odstraniti nihče, razen sam. V sistemu mora vedno obstajati vsaj en administrator. Administrator lahko izbriše uporabniški račun študenta ali predstavnika fakultete, in jih s tem poniža v goste. Administrator lahko študenta povzdigne v predstavnika fakultete, ali pa predstavnika fakultete zniža v študenta.


## 3. Slovar pojmov

* aktivnost: Aktivnost je osnovna podatkovna enota aplikacije, s katero lahko uporabnik manipulira. Gre za predstavitev kakršnekoli aktivnosti ali obveznosti v realnem življenu z določenim terminom.

* ponovljiva aktivnost: Aktivnost, ki je ponovljive narave. Gre za predavanja, vaje ipd., ki se dodajo na urnik, in se ponavljajo tedensko.

* enkratna aktivnost: Aktivnost, ki so enkratne narave. Gre za izpite, domače naloge ipd., katerih termin oz. rok se doda na urnik, a se ne ponavljajoo tedensko.

* dogodek: Dogodek je osnovna podatkovna enota Google Calendar, ki se lahko uvozi v aplikacijo StraightAs.

* uporabnik: Uporabnik je oseba, ki uporablja aplikacijo StraightAs in ima določeno eno izmed vlog. V sklopu funkcionalnih zahtev so te vloge omejene na tiste, ki so kot akterji omenjene v povzetku zahteve. Kjer vloge niso razvidne iz konteksta, sta mišljeni predvsem vlogi študenta in predstavnika fakultete.


## 4. Diagram primerov uporabe


https://creately.com/diagram/ju6t3s902/ogXWn01nOQfIjg2YHSitEMMGaw%3D

Povezava do UML diagrama.

![](https://i.imgur.com/TxjiO33.jpg)



## 5. Funkcionalne zahteve

### Registracija.

#### Povzetek funkcionalnosti

Gost se lahko registrira in si s tem ustvari uporabniški račun.


#### Osnovni tok

1. Gost izbere funkcionalnost Registracija.
2. Sistem prikaže pojavno okno z vnosnimi polji za ime, priimek, uporabniško ime, geslo, potrditev gesla in naslov elektronske pošte.
3. Gost ustrezno vnese zahtevane podatke.
4. Sistem omogoči gumb za potrditev registracije.
5. Gost potrdi registracijo.
6. Sistem preusmeri gosta na prijavo in prikaže sporočilo o uspešni registraciji.


#### Izjemni tokovi
* Gost ne vnese ustreznega uporabniškega imena, gesla, ali naslova elektronske pošte. Sistem izpiše opozorilo in ne omogoči gumba za potrditev registracije.
* Gesli, ki jih je gost vnesel, se ne ujemata. Sistem izpiše ustrezno opozorilo o neuspešni registraciji.
* Uporabniško ime ali naslov elektronske pošte, ki ju je gost navedel, sta že v uporabi. Sistem izpiše ustrezno opozorilo o neuspešni registraciji.


#### Pogoji

Pri uporabi funkcionalnosti Registracija, gost v sistem ne sme biti prijavljen. Če je prijavljen v obstoječi uporabniški račun, ga preusmeri na stran Pregled aktivnosti.


#### Posledice

Če se gost uspešno registrira, se v sistemu ustvari nov uporabniški račun.


#### Posebnosti

Pri realizaciji funkcionalnosti Registracija je potrebno upoštevati standarde izdane z inštituta NIST.


#### Prioriteta

Must have.


#### Sprejemni testi

* Sprejemni test MH01-01
    * Funkcija: Testiranje osnovnega toka pravilne registracije.
    * Začetno stanje: Pred začetkom uporabnik ni prijavljen v uporabniški račun (deluje kot gost).
    * Vhod: Gost izbere funkcionalnost registracija, ustrezno vnese zahtevane podatke in potrdi registracijo.
    * Pričakovani rezultat: V sistemu se ustvari nov uporabniški račun na podlagi podatkov, ki so bili vnešeni. Gost je preusmerjen na začetno stran. Sistem izpiše sporočilo o uspešni registraciji.

* Sprejemni test MH01-02
    * Funkcija: Testiranje izjemnega toka neustreznega gesla.
    * Začetno stanje: Pred začetkom uporabnik ni prijavljen v uporabniški račun (deluje kot gost).
    * Vhod: Gost izbere funkcionalnost registracija in ustrezno izpolni podatke o uporabniškem imenu, elektronskemu naslovu in potrditvi gesla. V polje za geslo vnese presledek, šest črk, tabulator in dve številki. Zadnje tri znake izbriše in vnese dva znaka, ki nista ascii. Izbriše nazadnje vnešena znaka in vnese tri presledke. Nato vnese še eno črko.
    * Pričakovani rezultat: Sistem odobri potrditev registracije šele po zadnjem vnosu.

* Sprejemni test MH01-03
    * Funkcija: Testiranje izjemnega toka neustreznega elektronskega naslova.
    * Začetno stanje: Pred začetkom uporabnik ni prijavljen v uporabniški račun (deluje kot gost).
    * Vhod: Gost izbere funkcionalnost registracija, ustrezno vnese podatke o uporabniškem imenu in geslu. V polje za elektronski naslov vnese zaporedje številk in črk. Nato vnese znake '@.si'. Izbriše zadnje tri znake in vnese zaporedje petih črk in piko. Nato vnese znake 'com'.
    * Pričakovani rezultat: Sistem odobri potrditev registracije šele po zadnjem vnosu.

* Sprejemni test MH01-04
    * Funkcija: Testiranje izjemnega toka neujemajočih gesel.
    * Začetno stanje: Pred začetkom uporabnik ni prijavljen v uporabniški račun (deluje kot gost).
    * Vhod: Gost izbere funkcionalnost registracija, ustrezno vnese podatke o uporabniškem imenu in elektronskemu naslovu. V polja za geslo vnese dva različna, ustrezna gesla in potrdi registracijo.
    * Pričakovani rezultat: Sistem izpiše sporočilo o neuspešni registraciji zaradi neujemajočih gesel.

* Sprejemni test MH01-05
    * Funkcija: Testiranje izjemnega toka že uporabljenega uporabniškega imena in elektronskega naslova.
    * Začetno stanje: Pred začetkom uporabnik ni prijavljen v uporabniški račun (deluje kot gost). Na voljo ima podatke o elektronskemu naslovu in uporabniškemu imenu že obstoječega uporabniškega računa.
    * Vhod: Gost izbere funkcionalnost registracija in ustrezno vnese podatke o geslu. V polja za elektronski naslov in uporabniško ime vnese že uporabljeni vrednosti in potrdi registracijo.
    * Pričakovani rezultat: Sistem izpiše sporočilo o neuspešni registraciji zaradi že uporabljenega uporabniškega imena ter elektronskega naslova

---

### Prijava.

#### Povzetek funkcionalnosti

Gost se lahko prijavi in si s tem spremeni vlogo.


#### Osnovni tok

1. Gost izbere funkcionalnost prijava.
2. Prikaže se mu obrazec za vnos uporabniškega imena in gesla.
3. Gost vnese podatke.
4. Sistem omogoči gumb za prijavo.
5. Gost klikne na gumb prijava.
6. Ob uspešni prijavi je gost preusmerjen na pregled aktivnosti, če se je prijavil v račun študenta ali predstavnika fakultete. Če se je prijavil v račun administratorja, ga preusmeri na stran pregleda uporabnikov.

#### Alternativni tok 1

1. Gost izbere funkcionalnost prijava.
2. Gost izbere prijavo z Google računom.
3. Sistem preusmeri gosta na stran Google.
4. Gost se vpiše v svoj Google račun.
5. Če je to gostova prva prijava s tem računom, sistem zanj ustvari nov uporabniški račun z vlogo študenta.
6. Ob uspešni prijavi je gost preusmerjen na pregled aktivnosti z ustrezno uporabniško vlogo.

#### Izjemni tokovi

* Gost ne vnese obstoječega uporabniškega imena. Sistem izpiše ustrezno opozorilo.
* Gost ne vnese pravilnega gesla povezanega s podanim uporabniškim imenom. Sistem izpiše ustrezno opozorilo.

#### Pogoji

Na uporabljeni napravi ne sme biti prijavljen noben uporabnik.


#### Posledice

Uporabnik je prijavljen v uporabniški račun na aplikaciji.


#### Posebnosti

Realizacija funkcionalnosti potrebuje uporabo protokola OAuth.


#### Prioritete identificiranih funkcionalnosti

Must have.


#### Sprejemni testi

* Sprejemni test MH02-01
    * Funkcija: Testiranje osnovnega toka prijave študenta.
    * Začetno stanje: Pred začetkom uporabnik ni prijavljen v uporabniški račun (deluje kot gost). Na voljo ima podatke o elektronskemu naslovu in uporabniškemu imenu že obstoječega uporabniškega računa študenta.
    * Vhod: Gost izbere funkcionalnost prijava in ustrezno vnese podatke o uporabniškemu imenu geslu ter potrdi prijavo.
    * Pričakovani rezultat: Gost je prijavljen v ustrezni uporabniški račun in s tem pridobi vlogo študenta. Kot študent je preusmerjen na pregled aktivnosti. Sistem izpiše sporočilo o uspešni prijavi.

* Sprejemni test MH02-02
    * Funkcija: Testiranje osnovnega toka prijave administratorja.
    * Začetno stanje: Pred začetkom uporabnik ni prijavljen v uporabniški račun (deluje kot gost). Na voljo ima podatke o elektronskemu naslovu in uporabniškemu imenu že obstoječega uporabniškega računa administratorja.
    * Vhod: Gost izbere funkcionalnost prijava in ustrezno vnese podatke o uporabniškemu imenu geslu ter potrdi prijavo.
    * Pričakovani rezultat: Gost je prijavljen v ustrezni uporabniški račun in s tem pridobi vlogo administratorja. Administrator je preusmerjen na pregled uporabnikov. Sistem izpiše sporočilo o uspešni prijavi.

* Sprejemni test MH02-03
    * Funkcija: Testiranje alternativnega toka prve prijave z Google računom.
    * Začetno stanje: Pred začetkom uporabnik ni prijavljen v uporabniški račun (deluje kot gost). Ima dostop do Google računa, s katerim se v sistem še ni prijavil.
    * Vhod: Gost izbere funkcionalnost prijava in izbere prijavo z računom Google. Gost se uspešno prijavi v Google račun.
    * Pričakovani rezultat: Gost je prijavljen v ustrezni uporabniški račun in s tem pridobi drugo vlogo. Gost je preusmerjen na pregled aktivnosti. Sistem izpiše sporočilo o uspešni prijavi. V sistemu se ustvari nov uporabniški račun na podlagi podatkov Google računa.

* Sprejemni test MH02-04
    * Funkcija: Testiranje alternativnega toka neprve prijave z Google računom.
    * Začetno stanje: Pred začetkom uporabnik ni prijavljen v uporabniški račun (deluje kot gost). Ima dostop do Google računa, s katerim se je v sistem že prijavil.
    * Vhod: Gost izbere funkcionalnost prijava in izbere prijavo z računom Google. Gost se uspešno prijavi v Google račun.
    * Pričakovani rezultat: Gost je prijavljen v ustrezni uporabniški račun in s tem pridobi drugo vlogo. Gost je preusmerjen na pregled aktivnosti. Sistem izpiše sporočilo o uspešni prijavi. V sistemu se ne ustvari nov uporabniški račun.

* Sprejemni test MH02-05
    * Funkcija: Testiranje izjemnega toka prijave z neobstoječim uporabniškim imenom.
    * Začetno stanje: Pred začetkom uporabnik ni prijavljen v uporabniški račun (deluje kot gost). Ima dostop do uporabniškega imena, ki v sistemu ne obstaja.
    * Vhod: Gost izbere funkcionalnost prijava in vnese neobstoječe uporabniško ime in ustrezno geslo. Potrdi prijavo. 
    * Pričakovani rezultat: Sistem izpiše sporočilo o neuspešni prijavi zaradi neobstoječega uporabniškega imena.

* Sprejemni test MH02-06
    * Funkcija: Testiranje izjemnega toka prijave z nepravilnim geslom.
    * Začetno stanje: Pred začetkom uporabnik ni prijavljen v uporabniški račun (deluje kot gost). Ima dostop do uporabniškega imena in gesla že obstoječega računa.
    * Vhod: Gost izbere funkcionalnost prijava in vnese uporabniško ime in nepravilno geslo. Potrdi prijavo. 
    * Pričakovani rezultat: Sistem izpiše sporočilo o neuspešni prijavi zaradi nepravilnega gesla.

---

### Odjava.

#### Povzetek funkcionalnosti

Študent, predstavnik fakultete ali administrator se lahko odjavi. S tem preide v vlogo gosta.

#### Osnovni tok

1. Uporabnik izbere funkcionalnost odjava.
2. Uporabnik potrdi odjavo.
3. Sistem uporabnika preusmeri na začetno stran.

#### Pogoji

Na uporabljeni napravi mora biti uporabnik prijavljen v uporabniški račun.


#### Posledice

Uporabnik je odjavljen iz uporabniškega računa na aplikaciji in preusmerjen na začetno stran.


#### Prioritete identificiranih funkcionalnosti

Must have.


#### Sprejemni testi

* Sprejemni test MH03-01
    * Funkcija: Testiranje osnovnega toka odjave.
    * Začetno stanje: Uporabnik je prijavljen v uporabniški račun.
    * Vhod: Uporabnik izbere funkcionalnost odjava in odjavo potrdi.
    * Pričakovani rezultat: Uporabnik je iz aplikacije odjavljen in preusmerjen na začetno stran.


---

### Dodajanje aktivnosti.


#### Povzetek funkcionalnosti

Študent ali predstavnik fakultete doda novo aktivnost na svoj uporabniški račun.


#### Osnovni tok

1. Uporabnik izbere funkcionalnost Dodajanje aktivnosti.
2. Sistem uporabnika preusmeri na stran za dodajanje aktivnosti.
3. Uporabnik vnese ime, datum in uro, opombe, ter trajanje aktivnosti. Poleg tega izbere, ali je ta aktivnost ponovljiva ali enkratna. Če je aktivnost enkratna, določi še, ali naj gre na to-do seznam.
4. Ko uporabnik izpolni vsa polja, sistem omogoči potrditev aktivnosti.
5. Uporabnik potrdi aktivnosti.
6. Sistem shrani dodano aktivnost na račun uporabnika, in ga preusmeri na pregled aktivnosti.

#### Alternativni tok

1. Uporabnik izbere funkcionalnost Dodajanje aktivnosti.
2. Sistem uporabnika preusmeri na stran za dodajanje aktivnosti.
3. Uporabnik pod dodajanjem javne aktivnosti izbere fakulteto.
4. Sistem mu prikaže seznam javnih aktivnosti na tej fakulteti.
5. Uporabnik s seznama izbere aktivnost. Če je aktivnost enkratna, določi še, ali naj gre na to-do seznam. Izbiro potrdi.
6. Sistem shrani dodano aktivnost na račun uporabnika, in ga preusmeri na pregled aktivnosti.


#### Pogoji

Uporabnik je prijavljen v sistem kot študent ali predstavnik fakultete.


#### Posledice

Uporabniku se shrani dodana aktivnost.


#### Prioritete identificiranih funkcionalnosti

Must have.


#### Sprejemni testi

* Sprejemni test MH04-01
    * Funkcija: Testiranje osnovnega toka dodajanja aktivnosti.
    * Začetno stanje: Uporabnik je prijavljen v uporabniški račun.
    * Vhod: Uporabnik izbere funkcionalnost dodajanje aktivnosti. Vnese zahtevane podatke. Aktivnost potrdi.
    * Pričakovani rezultat: Uporabnik je preusmerjen na pregled aktivnosti. Na urniku je izpisana dodana aktivnost.

* Sprejemni test MH04-02
    * Funkcija: Testiranje alternativnega toka dodajanja javne aktivnosti.
    * Začetno stanje: Uporabnik je prijavljen v uporabniški račun.
    * Vhod: Uporabnik izbere funkcionalnost dodajanje aktivnosti. Izbere dodajanje javne aktivnosti in izbere fakulteto. Nato izbere eno izmed aktivnosti na tej fakulteti. Aktivnost potrdi.
    * Pričakovani rezultat: Uporabnik je preusmerjen na pregled aktivnosti. Na pregledu aktivnosti je izpisana dodana aktivnost.


---

### Pregled aktivnosti.


#### Povzetek funkcionalnosti

Študent ali predstavnik fakultete lahko pregleda ponovljive aktivnosti na tedenskem urniku. Poleg tega ima na voljo tudi pregled vseg enkratnih aktivnosti v skupnem seznamu.

#### Osnovni tok

1. Uporabnik izbere pregled aktivnosti.
2. Sistem uporabnika preusmeri na stran za pregled aktivnosti in navede, katere izmed njih ima dodane.


#### Pogoji

Uporabnik je prijavljen v sistem kot študent ali predstavnik fakultete.


#### Posledice

Uporabniku se izpiše pregled aktivnosti. Ponovljive aktivnosti se izpišejo na urniku, enkratne aktivnosti pa na seznamu.


#### Prioritete identificiranih funkcionalnosti

Must have.

#### Sprejemni testi

* Sprejemni test MH05-01
    * Funkcija: Testiranje alternativnega toka dodajanja javne aktivnosti.
    * Začetno stanje: Uporabnik je prijavljen in se nahaja na strani za pregled ocen.
    * Vhod: Uporabnik izbere funkcionalnost pregled aktivnosti.
    * Pričakovani rezultat: Uporabnik je preusmerjen na pregled aktivnosti. Na pregledu aktivnosti so izpisane vse aktivnosti, ki si jih je dodal.

---

### Izbris aktivnosti.

#### Povzetek funkcionalnosti

Študent ali predstavnik fakultete lahko odstrani dodano aktivnost.

#### Osnovni tok

1. Uporabnik ob izbrani aktivnosti pritisne gumb za odstranitev aktivnosti.
2. Sistem uporabniku prikaže zaslon za potrditev.
3. Uporabnik potrdi odstranitev aktivnosti.

#### Pogoji

Uporabnik je prijavljen v sistem kot študent ali predstavnik fakultete.

#### Posledice

Uporabniku se za izbrano aktivnost vpiše ocena.

#### Prioritete identificiranih funkcionalnosti

Must have.


#### Sprejemni testi

* Sprejemni test MH06-01
    * Funkcija: Testiranje osnovnega toka izbrisa aktivnosti.
    * Začetno stanje: Uporabnik je prijavljen in se nahaja na strani, ki prikazuje aktivnosti.
    * Vhod: Uporabnik izbere gumb za izbris ob aktivnosti in potrdi.
    * Pričakovano stanje: Aktivnost se uspešno odstrani s seznama aktivnosti. Uporabnik dobi sporočilo o uspešnem izbrisu.

---

### Izbris lastnega računa.


#### Povzetek funkcionalnosti

Študent, predstavnik fakultete ali administrator si lahko izbriše račun.

#### Osnovni tok za študenta in predstavnika fakultete

1. Uporabnik izbere funkcionalnost Izbris lastnega računa.
2. Pritisne gumb za ostranitev.
3. Sistem pokaže dodatno okno za vpis gesla.
4. Uporabnik ponovno vpiše geslo.
5. Uporabnik potrdi izbris računa.
6. Sistem uporabnika izbriše iz sistema in ga preusmeri na začetno stran.

#### Izjemni tokovi
* Edini administrator si poskusi izbrisati račun. Sistem izbris zavrne.

#### Pogoji

Uporabnik mora biti prijavljen v sistem.

#### Posledice

Odstranitev določenega uporabnika iz sistema.

#### Prioritete identificiranih funkcionalnosti

Must have.

#### Sprejemni testi

* Sprejmeni test MH07-01
    *  Funkcija: Testiranje osnovnega toka za izbris računa.
    *  Začetno stanje: Uporabnik je že registriran in prijavljen.
    *  Vhod: Uporabnik izbere funkcionalnost Izbris računa, vnese svoje geslo in potrdi.
    *  Pričakovani rezultati: V sistemu se izbriše uporabnik. Uporabnika se odjavi in preusmeri na začetno stran, kjer dobi sporočilo o uspešni odstranitvi.

* Sprejmeni test MH07-02
    *  Funkcija: Testiranje izjemnega toka za izbris računa edinega administratorja.
    *  Začetno stanje: Edini administrator je prijavljen v sistem.
    *  Vhod: Administrator izbere funkcionalnost Odstranitev uporabnika.
    *  Pričakovani rezultati: Sistem ne prikaže okna za vpis gesla. Administrator dobi primerno sporočilo o neuspehu. V sistemu se administratorja ne izbriše.

---

### Vnos ocene.

#### Povzetek funkcionalnosti

Študent ali predstavnik fakultete lahko vnese svoje ocene.

#### Osnovni tok

1. Uporabnik izbere funkcionalnost Vnos ocene.
2. Sistem uporabniku prikaže zaslon za vnos ocene.
3. Uporabnik izbere obstoječo aktivnost, vnese podatek o oceni, opombo, in ali želi funkcionalnost odstraniti.
4. Sistem uporabniku omogoči potrditev.
5. Uporabnik potrdi oceno.
6. Sistem uporabnika preusmeri na zaslon za pregled ocen, kjer je dodana vnešena ocena.

#### Pogoji

Uporabnik je prijavljen v sistem kot študent ali predstavnik fakultete.


#### Posledice

Uporabniku se za izbrano aktivnost vpiše ocena.


#### Prioritete identificiranih funkcionalnosti

Should have.


#### Sprejemni testi

* Sprejemni test SH01-01
    * Funkcija: Testiranje osnovnega toka pravilnega vnosa ocene.
    * Začetno stanje: Uporabnik je prijavljen in se nahaja na strani, ki prikazuje aktivnosti.
    * Vhod: Uporabnik izbere funkcionalnost vnosa ocene, vnese zahtevane podatke in potrdi.
    * Pričakovano stanje: V sistem se vpiše ocena. Uporabnik je preusmerjen na stran za pregled ocen in dobi obvestilo o uspešnem vnosu.

---

### Odstranitev ocene.

#### Povzetek funkcionalnosti

Študent ali predstavnik fakultete lahko izbriše svoje ocene.

#### Osnovni tok

1. Uporabnik se nahaja na strani pregleda ocen.
2. Uporabnik ob oceni pritisne na gumb za odstranitev.
3. Sistem uporabniku prikaže zaslon za potrditev.
4. Uporabnik potrdi odstranitev.

#### Pogoji

Uporabnik je prijavljen v sistem kot študent ali predstavnik fakultete.


#### Posledice

Uporabniku se za izbrano aktivnost ocena izbriše.


#### Prioritete identificiranih funkcionalnosti

Should have.


#### Sprejemni testi

* Sprejemni test SH02-01
    * Funkcija: Testiranje osnovnega toka odstranitve ocene.
    * Začetno stanje: Uporabnik je prijavljen in se nahaja na strani, ki prikazuje ocene aktivnosti.
    * Vhod: Uporabnik pritisne na gumb za odstranitev ob aktivnosti. 
    * Pričakovano stanje: V sistemu se ocena izbriše. Uporabnik dobi obvestilo o izbrisu.

---

### Pregled ocen.


#### Povzetek funkcionalnosti

Študent ali predstavnik fakultete lahko pregleda vse svoje ocene.

#### Osnovni tok

1. Uporabnik izbere funkcionalnost Pregled ocen.
2. Na zaslonu ima prikazane aktivnosti in njihove ocene.

#### Pogoji

Uporabnik je prijavljen v sistem.

#### Posledice

Prikaz uporabnikovih ocen.

#### Prioritete identificiranih funkcionalnosti

Should have.

#### Sprejemni testi

* Sprejemni test SH03-01
    * Funkcija: Testiranje osnovnega toka pregleda ocen
    * Začetno stanje: Pred začetkom je študent prijavljen.
    * Vhod: Študent izbere funkcionalnost pregled ocen.
    * Pričakovani rezultat: Študentu se prikaže seznam predmetov in njihovih ocen.

---

### Ustvarjanje javne aktivnosti.


#### Povzetek funkcionalnosti

Predstavnik fakultete doda novo javno aktivnost za eno izmed njegovih fakultet.


#### Osnovni tok

1. Uporabnik izbere funkcionalnost Ustvarjanje javne aktivnosti.
2. Sistem uporabnika preusmeri na stran za ustvarjanje javne aktivnosti.
4. Uporabnik vnese ime, datum in uro, opombe, ter trajanje aktivnosti. Poleg tega izbere, ali je ta aktivnost ponovljiva ali enkratna, in kateri izmed njegovih fakultet pripada.
5. Ko uporabnik izpolni vsa polja, sistem omogoči potrditev aktivnosti.
6. Uporabnik potrdi aktivnosti.
7. Sistem shrani dodano aktivnost k izbrani fakulteti, in ga preusmeri na pregled aktivnosti.

#### Pogoji

Uporabnik je prijavljen v sistem kot predstavnik fakultete.


#### Posledice

K izbrani fakulteti se doda javna aktivnost.


#### Prioritete identificiranih funkcionalnosti

Should have.

#### Sprejemni testi

* Sprejemni test SH04-01
    * Funkcija: Testiranje osnovnega toka ustvarjanja javne aktivnosti.
    * Začetno stanje: Uporabnik je prijavljen v uporabniški račun predstavnika fakultete.
    * Vhod: Uporabnik izbere funkcionalnost ustvarjanje javne aktivnosti. Vnese zahtevane podatke in aktivnost potrdi. Uporabnik izbere dodajanje aktivnosti. Pod dodajanjem javne aktivnosti izbere fakulteto, h kateri je dodal aktivnost, in preveri seznam aktivnosti.
    * Pričakovani rezultat: Na seznamu aktivnosti fakultete se nahaja dodana aktivnost.

---

### Pregled uporabnikov.


#### Povzetek funkcionalnosti

Administrator lahko pregleda seznam uporabnikov in njihovih vlog.


#### Osnovni tok

1. Administrator pritisne gumb pregled uporabnikov.
2. Sistem administratorja preusmeri na stran za pregled uporabnikov.

#### Pogoji

Administrator mora biti prijavljen v sistem.

#### Posledice

Administrator je preusmerjen na stran za pregled uporabnikov.

#### Prioritete identificiranih funkcionalnosti

Should have.

#### Sprejemni testi

* Sprejemni test SH05-01
    * Funkcija: Testiranje osnovnega toka pregleda uporabnikov.
    * Začetno stanje: Pred začetkom je v sistem prijavljen administrator, na zaslonu za dodajanje novega administratorja.
    * Vhod: Administrator pritisne na gumb za pregled uporabnikov.
    * Pričakovani rezultat: Administrator je preusmerjen na stran za pregled uporabnikov. Dodajanje novega administratorja se prekliče.


---

### Urejanje statusov uporabnikov.


#### Povzetek funkcionalnosti

Administrator lahko dodeli ali odvzame vlogo predstavnika fakultete.


#### Osnovni tok

1. Administrator se nahaja na pregledu uporabnikov.
2. Poišče in izbere določenega uporabnika.
3. Administrator nato izbere dodelitev ali odvzem vloge predstavnika fakultete.
4. Pokaže se dodatni gumb za potrditev.
5. Administrator potrdi.

#### Pogoji

Administrator mora biti prijavljen v sistem. Ciljni uporabnik mora biti bodisi študent bodisi predstavnik fakultete.

#### Posledice

Ciljnemu uporabniku se dodeli ali odvzame vloga predstavnika fakultete.

#### Prioritete identificiranih funkcionalnosti

Should have.

#### Sprejemni testi

* Sprejemni test SH06-01
    * Funkcija: Testiranje osnovnega toka dviga študenta na predstavnika fakultete.
    * Začetno stanje: Pred začetkom je v sistem prijavljen administrator.
    * Vhod: Administrator izbere študenta, izbere funkcionalnost sprememba statusa, pritisne gumb za dvig na predstavnika fakultete in potrdi.
    * Pričakovani rezultat: V sistemu se dvigne status študenta na predstavnika fakultete. Administrator dobi obvestilo o uspešnem povzdvigu.

* Sprejemni test SH06-02
    * Funkcija: Testiranje osnovnega toka znižanja predstavnika fakultete na študenta.
    * Začetno stanje: Pred začetkom je v sistem prijavljen administrator.
    * Vhod: Administrator izbere študenta, izbere funkcionalnost sprememba statusa, pritisne gumb za znižanje na študenta in potrdi.
    * Pričakovani rezultat: V sistemu se zniža status predstavnika fakultete na študenta. Administrator dobi obvestilo o uspešnem znižanju.


---

### Odstranitev uporabnika.


#### Povzetek funkcionalnosti

Administrator lahko odstrani poljubnega uporabnika.

#### Osnovni tok

1. Administrator se nahaja na pregledu uporabnikov.
2. Administrator poišče in izbere določenega uporabnika.
3. Pri izbranem študentu pritisne gumb za odstranitev.
4. Sistem pokaže dodatno okno za vpis gesla.
5. Administrator vpiše geslo in odstranitev potrdi.
6. Sistem administratorju izpiše sporočilo o uspešni odstranitvi uporabnika.

#### Pogoji

Administrator mora biti prijavljen v sistem.

#### Posledice

Odstranitev določenega uporabnika.

#### Prioritete identificiranih funkcionalnosti

Should have.

#### Sprejemni testi

* Sprejmeni test SH07-01
    *  Funkcija: Testiranje osnovnega toka za odstranjevanje uporabnika.
    *  Začetno stanje: Administrator je prijavljen v sistem.
    *  Vhod: Administrator izbere uporabnika, izbere funkcionlanost Odstranitev uporabnika, vnese geslo in odstranitev potrdi.
    *  Pričakovani rezultati: V sistemu se izbriše uporabnik. Administrator dobi obvestilo o uspešno odstranitvi.
    
---

### Ustvarjanje administratorja.


#### Povzetek funkcionalnosti

Administrator lahko ustvari račun novega administratorja.

#### Osnovni tok

1. Administrator izbere funkcionalnost Ustvarjanje administratorja.
2. Administrator izpolni polja za uporabniško ime, geslo in potrditev gesla.
3. Sistem omogoči potrditev ustvarjanja administratorja.
4. Administrator potrdi ustvarjanje administratorja.
5. Sistem administratorju izpiše sporočilo o uspešnem dodajanju administratorja.

#### Izjemni tokovi
* Administrator ne vnese ustreznega uporabniškega imena ali gesla. Sistem izpiše opozorilo in ne omogoči gumba za potrditev.
* Gesli, ki jih je administrator vnesel, se ne ujemata. Sistem izpiše ustrezno opozorilo o neuspešni stvaritvi administratorja.
* Uporabniško ime, ki ga je administrator navedel, je že v uporabi. Sistem izpiše ustrezno opozorilo o neuspešni stvaritvi administratorja.

#### Pogoji

Administrator mora biti prijavljen v sistem.

#### Posledice

V sistemu se ustvari nov administrator.

#### Prioritete identificiranih funkcionalnosti

Should have.

#### Sprejemni testi

* Sprejmeni test SH08-01
    *  Funkcija: Testiranje osnovnega toka za dodajanje administratorja.
    *  Začetno stanje: Administrator je prijavljen v sistem.
    *  Vhod: Administrator izbere funkcionalnost ustvarjanje administratorja, vnese ustrezne podatke in potrdi odločitev.
    *  Pričakovani rezultati: V sistemu se ustvari nov administrator. Prijavljenemu administratorju se izpiše sporočilo o uspešni stvaritvi administratorja, in je preusmerjen na pregled uporabnikov.

* Sprejemni test SH08-02
    * Funkcija: Testiranje izjemnega toka neustreznega gesla.
    * Začetno stanje: Administrator je prijavljen v sistem.
    * Vhod: Administrator izbere funkcionalnost ustvarjanje administratorja in vnese ustrezno uporabniško ime. V polje za geslo vnese presledek, šest črk, tabulator in dve številki. Zadnje tri znake izbriše in vnese dva znaka, ki nista ascii. Izbriše nazadnje vnešena znaka in vnese tri presledke. Nato vnese še eno črko.
    * Pričakovani rezultat: Sistem odobri potrditev stvaritve administratorja šele po zadnjem vnosu.

* Sprejemni test SH08-03

    * Funkcija: Testiranje izjemnega toka neujemajočih gesel.
    * Začetno stanje: Administrator je prijavljen v sistem.
    * Vhod: Administrator izbere funkcionalnost ustvarjanje administratorja in vnese ustrezno uporabniško ime. V polji za geslo vnese dva različna, ustrezna gesla in potrdi stvaritev administratorja.
    * Pričakovani rezultat: Sistem izpiše sporočilo o neuspešni stvaritvi administratorja zaradi neujemajočih gesel.

* Sprejemni test SH08-04

    * Funkcija: Testiranje izjemnega toka že uporabljenega uporabniškega imena.
    * Začetno stanje: Administrator je prijavljen v sistem in pozna že uporabljeno uporabniško ime.
    * Vhod: Administrator izbere funkcionalnost ustvarjanje administratorja in ustrezno vnese podatke o geslu. V polje za uporabniško ime vnese že uporabljeno vrednost in potrdi stvaritev administratorja.
    * Pričakovani rezultat: Sistem izpiše sporočilo o neuspešni stvaritvi administratorja zaradi že uporabljenega uporabniškega imena.
    
---

### Urejanje profila.


#### Povzetek funkcionalnosti

Študent, predstavnik fakultete in administrator lahko urejajo lastni profil - ime, priimek, uporabniško ime in fakulteta, na kateri se nahajajo.

#### Osnovni tok

1. Uporabnik izbere funkcionalnost Urejanje profila.
2. Prikaže se mu obrazec za urejanje podatkov.
3. Uporabnik ustrezno uredi svoje podatke.
4. Po urejanju pritisne gumb za potrditev.
5. Uporabniku se zapre obrazec.

#### Izjemni tokovi

* Uporabnik vnese že uporabljeno uporabniško ime. Sistem izpiše opozorilo in ne spremeni podatkov.

#### Pogoji

Uporabnik mora biti prijavljen v sistem, da mu je ta funkcionalnost na voljo.

#### Posledice

V sistemu se posodobijo podatki določenega uporabnika.

#### Prioritete identificiranih funkcionalnosti

Could have.


#### Sprejemni testi

* Sprejemni test CH01-01
    * Funkcija: Testiranje osnovnega toka urejenja profila.
    * Začetno stanje: Uporabnik je prijavljen v svoj uporabniški račun.
    * Vhod: Uporabnik izbere funkcionalnost uredi profil in ustrezno uredi podatke ter potrdi odločitev.
    * Pričakovani rezultat: Podatki se shranijo v sistem, uporabnik dobi obvestilo o uspehu in je preusmerjen na pregled aktivnosti.

* Sprejemni test CH01-02
    * Funkcija: Testiranje izjemnega toka že uporabljenega uporabniškega imena.
    * Začetno stanje: Uporabnik je prijavljen v svoj uporabniški račun in pozna že uporabljeno uporabniško ime.
    * Vhod: Uporabnik izbere funkcionalnost urejanje profila. V polje za uporabniško ime vnese že uporabljeno vrednost. Spremembo potrdi.
    * Pričakovani rezultat: Sistem izpiše sporočilo o neuspešni spremembi uporabniškega imena. Podatki v sistemu se ne posodobijo, uporabnik ni preusmerjen.

---

### Pregled TO-DO seznama.


#### Povzetek funkcionalnosti

Študent ali predstavnik fakultete lahko preglejuje TO-DO seznam neopravljenih enkratnih aktivnosti, in določa seznam trenutno prioritetnih aktivnosti.

#### Osnovni tok

1. Uporabnik izbere funkcionalnost TO-DO seznam.
2. Sistem ga preusmeri na pregled TO-DO seznama. Uporabniku se prikaže seznam neopravljenih enkratnih aktivnosti v dveh sklopih.
3. Uporabnik lahko aktivnost v kategoriji 'ostale aktivnosti' doda med trenutne.
4. Sistem aktivnost doda med trenutne aktivnosti in odstrani iz ostalih aktivnosti.
5. Uporabnik lahko odstrani aktivnost iz kategorije 'trenutne aktivnosti'.
6. Sistem aktivnost odstrani iz trenutnih aktivnosti in jo doda med ostale aktivnosti.

#### Pogoji

Uporabnik mora biti prijavljen in imeti dodano vsaj eno neopravljeno enkratno aktivnost.


#### Posledice

Uporabnik lahko pregleda vse svoje nezaključene enkratne aktivnosti. Spreminja jim lahko prioriteto.


#### Prioritete identificiranih funkcionalnosti

Could have.

#### Sprejemni testi

* Sprejemni test CH02-01
    * Funkcija: Testiranje osnovnega toka pregleda TO-DO seznama.
    * Začetno stanje: Uporabnik je prijavljen v svoj uporabniški račun in ima dodano vsaj eno neopravljeno enkratno aktivnost.
    * Vhod: Uporabnik izbere funkcionalnost pregled TO-DO seznama. Neopravljeno aktivnost doda med trenutne aktivnosti. 
    * Pričakovani rezultat: Sistem uporabniku prikaže njegov TO-DO seznam. Neopravljena aktivnost se doda med trenutne aktivnosti.


---
### Uvozi Google Calendar.


#### Povzetek funkcionalnosti

Študent ali predstavnik fakultete lahko uvozi dogodke iz Google Calendar. 

#### Osnovni tok

1. Uporabnik se nahaja na pregledu aktivnosti. Izbere aktivnost Uvozi Google Calendar, in izbere 'uvozi neposredno'.
2. Uporabnik je preusmerjen na Google Calendar, kjer se prijavi.
3. Sistem uvozi vse bodoče dogodke z njegovega koledarja.
4. Uporabnik je preusmerjen na pregled aktivnosti, kjer so dodane aktivnosti iz Google Calendar. Izpiše se sporočilo o uspehu.


#### Alternativni tok 1

1. Uporabnik se nahaja na pregledu aktivnosti. Izbere aktivnost Uvozi Google Calendar, in izbere 'uvozi iz datoteke'.
2. Uporabnik na uporabljeni napravi izbere datoteko z izvoženimi Google Calendar dogodki.
3. Sistem uvozi vse bodoče dogodke iz datoteke.
4. Uporabnik je preusmerjen na pregled aktivnosti, kjer so dodane aktivnosti iz Google Calendar. Izpiše se sporočilo o uspehu.


#### Pogoji

* Uporabnik je prijavljen.
* Uporabnik ima dostop do Google Calendar z vsaj enim dogodkom (osnovni tok).
* Uporabnik ima datoteko dogodkov iz Google Calendar, ki ne sme presegati velikosti 5MB (alternativni tok 1).

#### Posledice

* Študentu se po uspešno uvoženih dogodkih le ti prikažejo na pregledu aktivnosti.

#### Posebnosti


* Datoteka mora ustrezati standardu RFC 5545 (Alternativni tok 1)


#### Prioritete identificiranih funkcionalnosti

Would have.

#### Sprejemni testi

* Sprejmeni test WH01-01
    *  Funkcija: Testiranje osnovnega toka uvoza dogodkov iz Google Calendar
    *  Začetno stanje: Študent je prijavljen in je na začetni strani. Ima dostop do Google Calendar računa z vnešenimi dogodki.
    *  Vhod: Študent izbere Google Calendar Import in se avtenticira v googlovem prijavnem oknu.
    *  Pričakovani rezultati: Študentu se na pregled aktivnosti dodajo dogodki.

* Sprejmeni test WH01-02
    *  Funkcija: Testiranje alternativnega toka uvoza dogodkov iz Google Calendar
    *  Začetno stanje: Študent je prijavljen in je na začetni strani. Ima dostop do Google Calendar datoteke z izvoženimi dogodki.
    *  Vhod: Študent izbere Google Calendar Import in naloži ustrezno datoteko.
    *  Pričakovani rezultati: Študentu se na pregled aktivnosti dodajo dogodki.
    
---
### Obveščanje.


#### Povzetek funkcionalnosti

Predstavnik fakultete in administrator lahko pošiljata obvestila. Administrator lahko obvestilo pošlje komurkoli, predstavnik fakultete pa vsem, ki so naročeni na njegovo aktivnost.

#### Osnovni tok

1. Uporabnik izbere funkcionalnost Obveščanje.
2. Sistem ga preusmeri na stran za vnos obvestila.
3. Uporabnik izbere ciljno skupino uporabnikov in vnese obvestilo.
4. Sistem odobri potrditev.
5. Uporabnik potrdi obvestilo.
6. Sistem odpošlje obvsetilo vsem navedenim uporabnikom. Prijavljen uporabnik je preusmerjen na pregled aktivnosti (predstavnik fakultete) oziroma pregled uporabnikov (administrator).

#### Pogoji

* Prijavljen mora biti predstavnik fakultete ali administrator.
* Predstavnik fakultete mora imeti vsaj en predmet, na katerega je nekdo prijavljen.


#### Posledice

Študenti, ki so bili izbrani za poslano obvestilo, prejmejo obvestilo na elektronsko pošto. 


#### Prioritete identificiranih funkcionalnosti

Would have.

#### Sprejemni testi

* Sprejemni test WH02-01
    * Funkcija: Testiranje osnovnega toka pošiljanja obvestil s strani predstavnika fakultete.
    * Začetno stanje: Predstavnik fakultete je prijavljen in ima odprt pregled aktivnosti.
    * Vhod: Predstavnik fakultete izbere gumb pošlji obvestilo, ga izpolni in potrdi.
    * Pričakovani rezultat: Na email študentov prijavljenih na dotični predmet pride obvestilo.

* Sprejemni test WH02-02
    * Funkcija: Testiranje osnovnega toka pošiljanja obvestil s strani administratorja.
    * Začetno stanje: Administrator je prijavljen in ima odprt pregled uporabnikov.
    * Vhod: Administrator izbere gumb pošlji obvestilo, ga izpolni in potrdi.
    * Pričakovani rezultat: Na email izbranih študentov pride obvestilo.

---


## 6. Nefunkcionalne zahteve

#### Zahteve izdelka
* Zasebnost podatkov: Nihče v sistemu ne sme imeti dostopa do uporabnikovega prijavljenega elektronskega naslova ali gesla, niti uporabnik sam.
* Konsistentnost podatkov: Samo uporabnik sam lahko dodaja ali odstranjuje svoje aktivnosti. Dodajanje ali odstranjevanje aktivnosti na računih drugih uporabnikov ne sme vplivati na njegove dodane podatke.
* Zanesljivost delovanja: Aplikacija mora delovati in biti na voljo uporabnikom vsaj 99.5% časa.
* Uporabnost: Aplikacija mora delovati kot predvideno, brez večjih motenj ali upočasnitve z do 100 sočasnimi uporabniki.
* Odzivnost: Aplikacija mora biti na napravi z internetno podatkovno povezavo vsaj 1MB/s odzivna z maksimalnim časom čakanja 2s.

#### Organizacijske zahteve
* Issue tracking: Pri izdelavi aplikacije bo razvojna ekipa uporabljala aplikacijo Jira, specifično njene funkncionalnosti Project and issue tracking, kjer si bodo sproti beležili izvedeno delo in delo, ki se še mora izvesti. Prav tako bo vsak commit na strani Bitbucket vseboval ustrezen opis spremembe.
* Backlog management: Pri izdelavi aplikacije bo razvojna ekipa uporabljala backlog management, s katerim bodo imeli pregled nad zahtevanimi funkcionalnostmi in njihovimi prioritetami, ter jih redno posodabljali glede na potek razvoja. 


#### Zunanje zahteve
* Interface: Sistemski vmesnik z **Google Calendar** bo dobro opredeljen z navedeno uporabo API klicev ter poslanimi in pridobljenimi podatki.
* GDPR: Aplikacija bo sledila predpisom o suverenosti podatkov izdanih v sklopu regulacij GDPR.


## 7. Prototipi vmesnikov


#### Vmesnik z Google Calendar

Povezava med našim in zunanjim sistemom je ena sama: povezava s sistemom Google Calendar. Google Calendar ponuja API klic Events: list, kot navedeno na spletni stranu https://developers.google.com/calendar/v3/reference/events/list. API je dosegljiv na naslovu https://www.googleapis.com/calendar/v3/calendars/calendarId/events z metodo GET, klic pa mora biti brez telesa in v glavi vsebovati sledeči podatek calendarID, za katerega se uporabi ključna beseda "primary". API klic vrne sledeče podatke:

{
  "kind": "calendar#events",
  "etag": etag,
  "summary": string,
  "description": string,
  "updated": datetime,
  "timeZone": string,
  "accessRole": string,
  "defaultReminders": [
    {
      "method": string,
      "minutes": integer
    }
  ],
  "nextPageToken": string,
  "nextSyncToken": string,
  "items": [
    events Resource
  ]
}

API klic se uporabi pri uporabi glavnega toka funkcionalnosti Uvozi Google API.

#### Zaslonske maske

#### Zaslonske maske

##### Prijava
![](../img/zaslonske_maske/1_prijava.PNG)

---
##### Registracija
![](../img/zaslonske_maske/2_registracija.PNG)

---
##### Pregled aktivnosti
![](../img/zaslonske_maske/3_pregled_aktivnosti.PNG)

---
##### Dodajanje aktivnosti
![](../img/zaslonske_maske/4_dodaj_aktivnost.PNG)

---
##### TO-DO lista
![](../img/zaslonske_maske/5_to-do_lista.PNG)

---
##### Ocene
![](../img/zaslonske_maske/6_ocene.PNG)

---
##### Uredi profil
![](../img/zaslonske_maske/7_uredi_profil.PNG)

---
##### Ustvari javno aktivnost
![](../img/zaslonske_maske/8_ustvari_javno_aktivnost_predstavnik.PNG)

---
##### Obvescanje
![](../img/zaslonske_maske/9_obvescanje_predstavnik.PNG)

---
##### Iskanje uporabnika 
![](../img/zaslonske_maske/10_iskanje_uporabnika_administrator.PNG)

---
##### Urejanje uporabnika
![](../img/zaslonske_maske/11_edit_uporabnika_administrator.PNG)

---
##### Registracija admina
![](../img/zaslonske_maske/12_registracija_admina_administrator.PNG)

---
##### Brisanje računa
![](../img/zaslonske_maske/13_brisanje_racuna_administrator.PNG)
