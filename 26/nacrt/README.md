# Načrt sistema

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Jan Adamič, Jan Lovšin, Mihael Rajh, Dejan Sinic |
| **Kraj in datum** |Ljubljana, 22.4.2019 |



## Povzetek

Dokument vsebuje načrt arhitekture projekta StraightAs, kjer smo predstavili arhitekturo in podali več diagramov. Nadaljnje imamo predstavljen načrt strukture, podan razredni diagram in opisane razrede, njihove atribute in nesamoumevne metode z opisano funkcionalnostjo. Zadnji del dokumenta predstavlja načrt obnašanja, kjer imamo diagrame zaporedja za vsako aktivnost in za vse tokove.

## 1. Načrt arhitekture

Za projekt je bila izbrana arhitektura MVC. Arhitektura je sestavljena treh komponent: model, pogled in krmilnik. Model upravlja z podatkovno bazo, pogled upravlja z prikazovanjem podatkov, krmilnik pa je vez med pogledom in modelom. Struktura modela je podrobneje prikazana z dodatnim diagramom.

![](../img/diagrami/Blokovni_diagram.png)

![](../img/diagrami/Paketni_diagram.png)

## 2. Načrt strukture

### 2.1 Razredni diagram

Razredni diagram vsebuje razrede Administrator, Študent ter Predstavnik fakultete, ki dedujejo razred Uporabnik. Študent ima reference na razred To-do list ter razred Aktivnosti. Razred Javna aktivnost pa deduje razred Aktivnost, kateri ima referenco na razred Ocena.

![](../img/diagrami/Razredni_diagram.png)

#### Ime razreda: Uporabnik

Razred Uporabnik predstavlja osnovne podatke uporabnika aplikacije.

#### Atributi

    uporabniskoIme: String
    ime: String
    priimek: String
    geslo: String
    email: String

#### Nesamoumevne metode

    prijava(String uporabniskoIme, String geslo): boolean
-če je kombinacija "uporabniskoIme" in "geslo" pravilna se vpiši v sistem sicer vrni sporočilo o neuspešni prijavi.

    odjava()
-odjavi uporabnika iz sistema.
    
    urediProfil(String uporabniskoIme, String ime, String priimek, String geslo, String email): boolean
-če so parametri "uporabniskoIme", "ime", "priimek", "geslo", "email" veljavni jih zamenjaj z obstoječimi.

    preveriGeslo(String geslo): boolean
-če je parameter "geslo" veljaven vrni "true" sicer "false".

    poisciUporabnika(String ime, String priimek): Uporabnik[]
-vrni vse uporabnike, ki imajo atribut ime = "ime" in priimek = "priimek".

---
#### Ime razreda: Student

Razred Student deduje razred Uporabnik in ponuja večino funkcionalnosti.

#### Atributi

    vpisnaStevilka: String
    izobrazevalnaUstanova: String

#### Nesamoumevne metode

    dodajAktivost(String imeAktivnosti, String izobrazevalnaUstanova, Date datumAktivnosti, Time pricetekAktivnosti, Time konecAktivnosti, String opomba, boolean ponovljivo, boolean dodajNaToDoListo): boolean
-če so parametri "imeAktivnosti", "datumAktivnosti", "pričetekAktivnosti", "konecAktivnosti", "opomba" veljavni, dodaj aktivnost in vrni "true" sicer "false".

    odstraniAktivnost(Aktivnost aktivnost)
-izbriše objekt "aktinovst".

    pridobiAktivnosti() Aktivnost[]
-pridobi seznam vseh aktivnosti. Če aktinovsti ni pridobi prazen seznam.

    vnosOcene(Aktivnost aktivnost, String opomba, int ocena): boolean
-če ima parameter "ocena" zalogo vrednosti [1,10] vnesi oceno in vrni "true" sicer "false".

    izbrisRacun()
-izbriši uporabnikov svoj racun.

    odstraniOceno(Ocena ocena)
-izbriše objekt "ocena".

    pridobiOcene(): Ocena[]
-pridobi seznam vseh aktivnosti. Če aktinovsti ni pridobi prazen seznam.

    uvoziKoledaer(): Object
-uvozi koledar preko Google API

---
#### Ime razreda: Predstavnik fakultete

Razred Predstavnik fakultete deduje razred Student in ima dodatne atribute in funkcionalnosti.

#### Atributi

    vloga: String

#### Nesamoumevne metode

    ustvariJavnoAktivnost(String imeAktivnosti, String izobrazevalnaUstanovaAktivnosti, String izobrazevalnaUstanova, Date datumAktivnosti, Time pricetekAktivnosti, Time konecAktivnosti, String opomba, boolean ponovljivo, boolean dodajNaToDoListo): boolean
-če so parametri "imeAktivnosti", "izobrazevalnaUstanovaAktivnosti", "izodatumAktivnosti", "pricetekAktivnosti", "konecAktivnosti", "opomba" veljavni, dodaj aktivnost in vrni "true" sicer "false".

    posljiObvestilo(String ciljnaSkupina, String sporocilo): boolean
-če sta parametra "ciljnaSkupina" in "sporocilo" poslji sporočilo in vrni "true" sicer "false".
    
---
#### Ime razreda: Administrator

Razred Administrator deduje razred Uporabnik in ima dodatne pravice.

#### Nesamoumevne metode

    odstranitevUporabnika(Uporabnik uporabnik)
-izbriše objekt "uporabnik".
    
    registrirajAdministratorja(String uporabniskoIme, String ime, String priimek, String geslo, String email): boolean
-če so parametri "uporabniskoIme", "ime", "priimek", "geslo", "email" veljavni, registriraj novega administratorja in vrni "true" sicer "false".

    spremeniStatusUporabnika(Uporabnik uporabnik, String status)
-spremeni status "uporabnika" v "status".
    
---
#### Ime razreda: To-do list

Razred To-do list predstavlja vse trenutne in ostale obveznosti in ponuja možost za pomikanje trenutne v ostale obveznosti in obratno.

#### Atributi



#### Nesamoumevne metode

    dodajMedTrenutne(Aktivnost aktivnost)
-aktivnosti spremeni parameter "aktivnost.trenutnaAktivnost" na "true" in kliči posodobiListo().

    odstraniIzTrenutnih(Aktinvost aktivnost)
-aktivnosti spremeni parameter "aktivnost.trenutnaAktivnost" na "false" in kliči posodobiListo().

    posodobiListo(): void
-posodobi list aktivnosti in razdeli na trenutne ter ostale obveznosti glede na parameter "aktivnost.trenutnaAktivnost".

---
#### Ime razreda: Aktivnost

Razred Aktivnost vsebuje podatke o posamezni aktivnosti.

#### Atributi

    imeAktivnosti: String
    izobrazevalnaUstanova: String
    datumAktivnosti: Date
    pricetekAktivnosti: Time
    konecAktinovsti: Time
    opomba: String
    ponovljivo: boolean
    dodajNaToDoListo: boolean
    trenutnaAktivnost: boolean

#### Nesamoumevne metode

    urediAktivnost(String imeAktivnosti, String izobrazevalnaUstanova, Date datumAktivnosti, Time pricetekAktivnosti, Time konecAktivnosti, String opomba, boolean ponovljivo, boolean dodajNaToDoListo): boolean
-če so parametri "imeAktivnosti", "datumAktivnosti", "pričetekAktivnosti", "konecAktivnosti", "opomba" veljavni, uredi aktivnost in vrni "true" sicer "false".
    
---
#### Ime razreda: Javna aktivnost

Razred Javna aktivnost deduje razred Aktivnost in vsebuje dodatne parametre.

#### Atributi

    izobrazevalnaUstanovaAktivnosti: String
---
#### Ime razreda: Ocena

Razred Ocena vsebuje podatke o posamezni oceni.

#### Atributi

    opomba: String
    ocena: int

#### Nesamoumevne metode

    urediOceno(Aktivnost aktivnost, String opomba, int ocena): boolean
-če ima parameter "ocena" zalogo vrednosti [1,10] vnesi oceno in vrni "true" sicer "false".

## 3. Načrt obnašanja


#### Prijava

![](../img/diagrami/Prijava.png)

#### Registracija

![](../img/diagrami/Registracija.png)

#### Odjava

![](../img/diagrami/Odjava.png)

#### Dodajanje aktivnosti

![](../img/diagrami/Dodajanje_aktivnosti.png)

#### Pregled aktivnosti


![](../img/diagrami/Pregled_aktivnosti.png)

#### Izbris aktivnosti

![](../img/diagrami/Izbris_aktivnosti.png)

#### Izbris lastnega računa

![](../img/diagrami/Izbris_lastnega_racuna.png)

#### Vnos ocene

![](../img/diagrami/Vnos_ocene.png)

#### Odstranitev ocene

![](../img/diagrami/Odstranitev_ocene.png)

#### Pregled ocen

![](../img/diagrami/Pregled_ocen.png)

#### Urejanje profila

![](../img/diagrami/Urejanje_profila.png)

#### Pregled TO-DO seznama

![](../img/diagrami/Pregled_TO-DO_seznama.png)

#### Uvoz iz Google Calendar

![](../img/diagrami/Uvoz_iz_Google_Calendar.png)

#### Ustvarjanje javne aktivnosti

![](../img/diagrami/ustvarjanje_javne_aktivnosti.png)

#### Pregled uporabnikov

![](../img/diagrami/pregled_uporabnikov.png)

#### Urejanje statusov uporabnikov

![](../img/diagrami/urejanje_statusov_uporabnikov.png)

#### Ustvarjanje administratorja

![](../img/diagrami/ustvarjanje_administratorja.png)

#### Odstranitev uporabnika

![](../img/diagrami/odstranitev_uporabnika.png)

#### Obveščanje

![](../img/diagrami/obvescanje.png)
