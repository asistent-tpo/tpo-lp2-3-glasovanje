# Dokument zahtev

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Tine Fajfar, Lovro Habjan, Tadej Hudobivnik, Oton Pavlič |
| **Kraj in datum** | Ljubljana, 7.4.2019 |



## Povzetek projekta

Aplikacija StraightAs je namenjena študentom z glavnim namenom organizacije časa in opravil. Aplikacija bo ponujala več različnih uporabniških profilov, med katerimi lahko uporabniki preprosto prehajajo. Glavne funkcije aplikacije so kreacija in urejanje seznamov opravil, pregled in prijava na kategorije dogodkov in pregled in prijava različnih dogodkov, ki pripadajo kategorijam in  urejanje in pregled urnika, kjer bo uporabnik videl obveznosti, dogodke in roke oddaj nalog in datume izpitov in kolokvijev ter bo lahko dodajal poljubne aktivnosti, kot sta rekreacija ali čas za učenje. Za določene uporabnike bo aplikacija nudila avtomatično razporejanje opravil. Poleg tega bo aplikacija omogočala pregled ponudbe študentske prehrane in pošiljanje elektronskih sporočil profesorjem in asistentom, vodila pa bo tudi statistiko za uporabnika.
Aplikacija bo na voljo prek spleta in bo načrtovana tako, da je uporabna na napravah vseh velikosti in konstantno na voljo.


## 1. Uvod

Veliko študentov se sooča s stresom, pomankanjem motivacije in občutki preobremenjenosti zaradi velike količine sprotnih obveznosti – oddaj raznih nalog, datumov kolokvijev in izpitnih rokov. Za večino je razlog pomankanje organiziranosti, kot je pregled in sledenje vsem obveznostim, izdelava načrta dela za posamezen teden, učinkovita razdelitev prostega časa in postavljanje kratkoročnih ter dolgoročnih ciljev. Aplikacija StraightAs bo omogočila pomoč pri organizaciji uporabnikovega časa in mu pomagala slediti študijskim obveznostim in šolskem ter obšolskim dogodkom in nudila pregled nad študentskim življenjem.

Aplikacija bo podpirala več različnih uporabnikov, ki bodo lahko uporabljali različne funkcionalnosti aplikacije.
Neprijavljen uporabnik bo lahko ustvarjal, urejal in pregledoval sezname opravil. Lahko si bo zapisoval nakupovalni seznam, zapiske, opombe, datume oziroma karkoli, kar si bo želel. Poleg tega bo lahko dodajal aktivnosti na urnik – naj bodo za predavanja, šport ali prosti čas. Neprijavljen uporabnik se bo vedno lahko registriral in postal prijavljen uporabnik z dodatnimi funkcionalnostmi.
Prijavljen uporabnik bo poleg seznama opravil lahko povezal svoj študentski urnik z urnikom aplikacije. Tako bo na urniku avtomatično videl vaje in predavanja ter oddaje nalog in datume kolokvijev in izpitiov. Nanj bo, tako kot neprijavljen uporabnik, lahko tudi dodajal svoje aktivnosti, naj bo to čas za učenje, prosti čas, rekreacija ali čas za kosilo. Poleg tega bo lahko pregledoval in potrjeval udeležbo na dogodkih. Če bo potrdil udeležbo, se mu bo dogodek prikazal na svojem urniku. Dogodki bodo razporejeni v kategorije, na katere se bo prijavljen uporabnik lahko prijavljal in odjavljal. S tem bomo omogočili, da uporabnik pregleduje le tiste dogodke, ki ga zanimajo. Poleg dogodkov in urnika bo prijavljen uporabnik imel dostop do pregleda ponudbe v restavracijah, kjer nudijo študentsko prehrano. Tako se bo študent lahko že vnaprej odločil, kje in kaj bo jedel. Prijavljen študent bo imel možnost vpogleda v statistiko uporabe aplikacije. Videl bo, koliko ur je porabil za posamezne aktivnosti in kategorije, katerih dogodkov se je udeležil in katere kategorije prevladujejo v njegovem študentskem življenju.
Prijavljen uporabnik bo lahko postal povezan uporabnik v primeru, če poveže račun s svojim Facebook profilom. V tem primeru mu bodo na voljo dogodki, ki jih objavljajo Facebook strani, ki jih je všečkal. Poleg tega bo na urniku videl rojstne dneve svojih Facebook prijateljev.
Prijavljeni ali povezani uporabnik bo lahko s kreditno kartico in plačilom postal premium uporabnik. Ta bo lahko svojim aktivnostim dajal prioriteto. Omogočena mu bo tudi ocena časa, ki mu jo vzame vsaka aktivnost (kot je priprava na izpit) in v kombinaciji s prioriteto imel možnost avtomatske razporeditve časa, kjer bo aplikacija sama predlagala najbolj optimalen razpored aktivnosti v tednu. Poleg tega bo premium uporabnik lahko direktno iz aplikacije pošiljal elektronska sporočila pedagogom na svoji fakulteti. Več možnosti bo imel tudi na področju dogodkov, kjer bo lahko dodajal nove kategorije dogodkov in ustvarjal ter urejal dogodke. Premium uporabniki bodo lahko povezali svoje urnike in sezname opravil med seboj, si jih delili in jih skupaj urejali.
Poleg različnih vrst uporabnikov bomo imeli tudi administratorje, ki bodo skrbeli za aplikacijo. Imeli bodo dostop do vmesnika z ukazno vrstico, prek katerega bodo imeli nadzor nad celotno aplikacijo. Administratorji se bodo lahko povezali kot različni uporabniki in v njihovem imenu izvajali ukaze v sistemu.

Od aplikacije se pričakuje, da bo dostopna na javnem naslovu in dosegljiva 99% časa, odzivna v realnem času in z majhno porabo pomnilnika. Pričakuje se, da je hranjenje uporabniških podatkov varno in dostop dovoljen le pooblaščenim osebam. Hranjenje in upravljanje podatkov mora biti po zakonu GDPR. Pričakuje se, da je grafični vmesnik prilagojen za vse velikosti naprav.


## 2. Uporabniške vloge

* **Neprijavljen uporabnik** - Je najbolj osnoven uporabnik aplikacije StraightAs. Lahko se nadaljno registrira in posledično postane prijavljen uporabnik. Poleg registracije lahko še pregleduje svoj urnik, dodaja aktivnosti, odstranuje aktivnosti, dodaja zapis na to-do listo in odstranjuje zapise s to-do liste.
* **Prijavljen uporabnik** - Deduje vse funkcionalnosti neprijavljenega uporabnika. Poleg tega se lahko še prijavi v samo StraightAs aplikacijo, prijavlja in odjavlja iz kategorije dogodkov, dodeljuje in odstranjuje priroritete na aktivnostih, ureja aktivnosti,  poveže svoj urnik s študijskim urnikom, povezuje svoj račun z ostalimi zunanjimi vmesniki, dodaja in odstranjuje oceno porabe časa na aktivnost, pošilja email sporočila, potrjuje svojo udeležbo ali se odjavi od dogodka, in nadgradi svoj račun v premium račun. 
* **Povezan uporabnik** - Deduje vse funkcionalnosti prijavljenega uporabnika razen povezave svojega računa z ostalimi zunanjimi vmesniki. Poleg tega lahko še izbira/spremlja nove FB strani oziroma jih odstrani. 
* **Premium uporabnik** - Deduje vse funckionalnosti prijavljenega uporabnika razen nadgradnje računa v premium račun, saj to že je. Poleg tega lahko še objavlja in odstranjuje dogodke, spreminja prioritete na aktivnostih, ureja dogodke, predlaga nove kategorije dogodkov, pregleduje študentsko življenje (statistiko), pregleduje ponudbo v izbrani restavraciji, poveže ali prekine svoj profil z drugim profilom v StraightAs in vklopi ali izklopi funkcionalnost avtomatske razporeditve časa.
* **Administrator** - Je skrbnik celotnega sistema. Lahko se zgolj poveže s študentskim urnikom. 


## 3. Slovar pojmov

* **Aktivnost** - Je vsaka stvar, ki se nahaja na urniku. Ima določen čas začetka in konca, ter s tem tudi trajanja. Lahko ima tudi lokacijo in oceno koliko časa, je potrebno nameniti za pripravo na to aktivnost.
* **Urnik** - Je 2D tabelica, ki razporedi aktivnosti glede na dneve in nato še po urah v posameznem dnevu.
* **Študentski urnik** - Je FRIjev urnik nekega študenta, ki ga bo prijavljen, povezan, premium uporabnik ali administrator povezal s svojim urnikom, tako da se bodo na njem pojavile aktivnosti.
* **Dogodek** - Ima svoj čas, opis, naslov/lokacijo, ... Je nekaj, na kar se lahko prijavljen, povezan ali premium uporabnik aplikacije StraightAs označijo in s tem potrdijo svojo udeležbo. Dogodek je prikazan v pregledu dogodkov.
* **Prioriteta** - Označitev stopnje pomembnosti posamezne aktivnosti.
* **To-do list** - Je seznam nekih dejavnosti, ki jih želimo opraviti v bližnji ali daljni prihodnosti. 
* **Kategorija** - Je skupina večih dogodkov, ki so si med seboj zelo podobni.
* **Facebook stran** - Je stran na Facebooku, kjer so zabeleženi dogodki.
* **Profil uporabnika** - So lastnosti nekega uporabnika, torej njegov urnik, seznam dogodkov, ...
* **Sistem** - Gre za aplikacijo StraightAs.
* **Pedagog** - Je oseba, ki poučuje nek predmet na fakulteti.


## 4. Diagram primerov uporabe

![UML diagram](../img/UML.JPG)

## 5. Funkcionalne zahteve

| **Prijava v aplikacijo StraightAs** | #0 |
| :------------------------------- | :-------------------------------|
| **Povzetek** 	| Prijavljen, povezan, premium uporabnik in administrator se lahko prijavi v aplikacijo StraightAs. |
| **Pogoji** 	| Vloga prijavljenega, povezanega, premium uporabnika in administratorja. Račun mora obstajati (kar pomeni, da je že registriran v sistem). |
| **Posledice** | Študentski urnik in urnik sta povezana. Termini vaj in predavanj iz Študentskega urnika se prikažejo tudi v urniku, za obdobje tekočega semestra glede na študijski koledar univerze. |
| **Posebnosti** 	| / |
| **Prioriteta** | Must have |
| **1. Osnovni tok** | 1. Prijavljen, povezan, premium uporabnik ali administrator pritisne na gumb "Prijava"". |
|                    | 2. Prijavljen, povezan, premium uporabnik ali administrator vnese zahtevane podatke (uporabniško ime in geslo). |
|                    | 3. Sistem preveri vnesene podatke. |
|                    | 4. Sistem prijavljenega, povezanega, premium uporabnika ali administratorja obvesti o uspešni prijavi v sistem in ga pošlje na začetno stran. |
| **Sprejemni test 0.1.1**  | |
| ***Funkcija, ki se testira*** | Prijava v sistem- |
| ***Začetno stanje sistema*** | Prijavljen, povezan, premium uporabnik ali administrator še ni prijavljen v sistem. |
| ***Vhod***                    | Vnos uporabniškega imena in gesla. |
| ***Pričakovan rezultat***     | Sistem uporanbika obvesti o uspešni prijavi. |
| **1. Izjemni tok** | Prijavljen, povezan, premium uporabnik ali administrator vnese napačne podatke. Sistem javi napako. |
| **Sprejemni test 1.3.1**  | |
| ***Funkcija, ki se testira*** | Obvestilo o napačno vnešenih prijavnih podatkih. |
| ***Začetno stanje sistema*** | Prijavljen, povezan, premium uporabnik ali administrator še ni prijavljen v sistem. |
| ***Vhod***                    | Napačno uporabniško ime ali napačno geslo. |
| ***Pričakovan rezultat***     | Neuspešna prijava. Obvestilo o napačnih podatkih. | 


| **Povezava s študentskim urnikom** | #1 |
| :------------------------------- | :-------------------------------|
| **Povzetek** 	| Prijavljen uporabnik, povezan uporabnik, premium uporabnik ali administrator lahko pridobijo svoj študentski urnik. Z vpisno številko povežejo svoj študentski urnik z urnikom. |
| **Pogoji** 	| Vloga prijavljenega uporabnika, povezanega uporabnika, premium uporabnika ali administratorja. Študentski urnik ni povezan. Uporabnik ima v profil vnešeno vpisno številko. Uporabnik obiskuje Fakulteto za računalništvo in informatiko. |
| **Posledice** | Študentski urnik in urnik sta povezana. Termini vaj in predavanj iz Študentskega urnika se prikažejo tudi v urniku, za obdobje tekočega semestra glede na študijski koledar univerze. |
| **Posebnosti** 	| / |
| **Prioriteta** | Should have |
| **1. Osnovni tok** | 1. Uporabnik izbere funkcionalnost Povezava s študentskim urnikom |
|                    | 2. Sistem pridobi študentski urnik |
|                    | 3. Sistem prikaže sporočilo o uspešni povezavi študentskega urnika |
| **Sprejemni test 1.1.1**  | |
| ***Funkcija, ki se testira*** | Povezava uporabniškega računa s študentskim urnikom fakultete prek uporabniškega vmesnika |
| ***Začetno stanje sistema*** | Uporabniški račun ni povezan s študentskim urnikom |
| ***Vhod***                    | Vpisna številka uporabnika |
| ***Pričakovan rezultat***     | Uporabniški račun je povezan s študentskim urnikom | 
| **1. Alternativni tok** | 1. Administrator izbere uporabnika |
|                         | 2. Izbere funkcionalnost Povezava s študentskim urnikom |
|                         | 3. Vnese naslov do strani in vse potrebne podatke, ki vrne urnik podane vpisne številke. |
|                         | 4. Sistem pridobi urnik |
|                         | 5. Sistem prikaže obvestilo o uspešni izvedbi |
| **Sprejemni test 1.2.1**  | |
| ***Funkcija, ki se testira*** | Povezava uporabniškega računa s študentskim urnikom s pomočjo CLI |
| ***Začetno stanje sistema*** | Uporabniški račun ni povezan s študentskim urnikom, administrator ima vse potrebne podatke |
| ***Vhod***                    | Pravilna http zahteva za komunikacijo s strežnikom, ki vsebuje študentski urnik |
| ***Pričakovan rezultat***     | Uporabniški račun je povezan s študentskim urnikom | 
| **1. Izjemni tok** | Uporabnik izbere funkcionalnost Povezava s študentskim urnikom, pri čemer pride do napake v sistemu. Sistem prikaže ustrezno sporočilo. |
| **Sprejemni test 1.3.1**  | |
| ***Funkcija, ki se testira*** | Povezava s študentskim urnikom |
| ***Začetno stanje sistema*** | Uporabniški račun ni povezan s študentskim urnikom |
| ***Vhod***                    | Vpisna številka uporabnika |
| ***Pričakovan rezultat***     | Prikaz obvestila o napaki v sistemu | 
| **2. Izjemni tok** | Uporabnik izbere funkcionalnost Povezava s študentskim urnikom, pri čemer študent ne obiskuje UL FRI. Sistem prikaže ustrezno sporočilo. |
| **Sprejemni test 1.3.2**  | |
| ***Funkcija, ki se testira*** | Povezava s študentskim urnik za uporabnika, ki ne obiskuje UL FRI |
| ***Začetno stanje sistema*** | Uporabniški račun ni povezan s študentskim urnikom |
| ***Vhod***                    | Vpisna številka uporabnika |
| ***Pričakovan rezultat***     | Prikaz obvestila o nepodprti funkcionalnosti | 
| **3. Izjemni tok** | Administrator izbere uporabnika, in Povezavo s študentskim urnikom. Vnese neveljavno http zahtevo. Sistem prikaže ustrezno sporočilo. |
| **Sprejemni test 1.3.3**  | |
| ***Funkcija, ki se testira*** | Neveljavna http zahteva s strani administratorja |
| ***Začetno stanje sistema*** | Uporabniški račun ni povezan s študentskim urnikom |
| ***Vhod***                    | Neveljavna http poizvedba |
| ***Pričakovan rezultat***     | Prikaz obvestila o napaki v sistemu | 
| **4. Izjemni tok** | Administrator izbere uporabnika, in Povezavo s študentskim urnikom. Vnese veljavno http zahtevo. Pri razčlenjevanju podatkov iz dokumenta pride do napake. Sistem prikaže ustrezno sporočilo. |
| **Sprejemni test 1.3.1**  | |
| ***Funkcija, ki se testira*** | Izpis obvestila ob napaki pri razčlenjevanju datoteko |
| ***Začetno stanje sistema***  | Uporabniški račun ni povezan s študentskim urnikom |
| ***Vhod***                    | Nepričakovana oblika dokumenta |
| ***Pričakovan rezultat***     | Prikaz obvestila o napaki pri razčlenjevanju | 


| **Registracija uporabnika** | #2 |
| :------------------------------- | :-------------------------------|
| **Povzetek** 	| Neregistriran uporabnik se lahko registrira v aplikacijo StraightAs. Neregistriran uporabnik vnese zahtevane podatke in postane prijavljen uporabnik. |
| **Pogoji** 	| V aplikaciji StraightAs še ni registriranega uporabnika z enakimi zahtevanimi podatki. |
| **Posledice** | Neregistriran uporabnik postane prijavljen. |
| **Posebnosti** 	| / |
| **Prioriteta** | Must have |
| **1. Osnovni tok** | 1. Neregistriran uporabnik izbere funkcionalnost Registracija uporabnika |
|                    | 2. Uporabnik vnese zahtevane podatke |
|                    | 3. Sistem uporabnika z vnešenimi podatki doda v bazo |
|                    | 4. Sistem uporabnika obvesti o uspešni registraciji |
| **Sprejemni test 2.1.1**  | |
| ***Funkcija, ki se testira*** | Registracija uporabnika prek uporabniškega vmesnika |
| ***Začetno stanje sistema***  | Uporabniku je na voljo grafični vmesnik za registracijo |
| ***Vhod***                    | Podatki o uporabniku |
| ***Pričakovan rezultat***     | Uporabnik je registriran in dobi vlogo prijavljenega uporabnika | 
| **1. Alternativni tok** | 1. Prijavi se kot administrator |
|                         | 2. Odpri CLI |
|                         | 3. V bazo vnesi novega uporabnika z vsemi zahtevanimi podatki |               
| **Sprejemni test 2.2.1**  | |
| ***Funkcija, ki se testira*** | Vnos novega uporabnika v bazo prek CLI |
| ***Začetno stanje sistema***  | V bazi ni registriranega uporabnika s podatki X |
| ***Vhod***                    | Pravilno oblikovan stavek za dodajanje uporabnika s podatki X |
| ***Pričakovan rezultat***     | Uporabnik je dodan v bazo uporabnikov in ima vlogo Prijavljenega uporabnika | 
| **1. Izjemni tok** | Uporabnik izbere funkcionalnost Registracija, pri čemer ne vnese vseh zahtevanih podatkov. Sistem prikaže ustrezno sporočilo. |
| **Sprejemni test 2.3.1**  | |
| ***Funkcija, ki se testira*** | Prikaz obvestila pri nepopolno izpolnjenih podatkih pri registraciji |
| ***Začetno stanje sistema***  | Uporabniku je na voljo grafični vmesnik za registracijo |
| ***Vhod***                    | Nepopolni podatki za registracijo |
| ***Pričakovan rezultat***     | Sistem registracijo zavrne in prikaže ustrezno sporočilo | 
| **2. Izjemni tok** | Uporabnik izbere funkcionalnost Registracija in vnese podatke, ki v bazi že obstajajo. Sistem prikaže ustrezno sporočilo. |
| **Sprejemni test 2.3.2**  | |
| ***Funkcija, ki se testira*** | Prikaz obvestila pri poizkusu registracije s podatki, ki v bazi že obstajajo |
| ***Začetno stanje sistema***  | Uporabniku je na voljo grafični vmesnik za registracijo |
| ***Vhod***                    | Podatki za registracijo, ki v bazi že obstajajo |
| ***Pričakovan rezultat***     | Sistem registracijo zavrne in prikaže ustrezno sporočilo | 
| **3. Izjemni tok** | Prijavi se kot Administrator in odpri CLI. Izvedi stavek, s katerim dodaš uporabnika. V bazi že obstaja uporabnik s temi podatki, zato sistem prikaže ustrezno sporočilo. |
| **Sprejemni test 2.3.3**  | |
| ***Funkcija, ki se testira*** | Prikaz obvestila pri poizkusu registracije prek CLI s podatki, ki v bazi že obstajajo |
| ***Začetno stanje sistema***  | Administrator ima odprt CLI, v bazi že obstajajo podatki X |
| ***Vhod***                    | Pravilno strukturiran stavek, za vnos novega uporabnika s podatki X |
| ***Pričakovan rezultat***     | Sistem registracijo zavrne in v CLI prikaže ustrezno sporočilo | 


| **Povezava uporabnika** | #3 |
| :------------------------------- | :-------------------------------|
| **Povzetek** 	| Prijavljen uporabnik lahko svoj račun poveže s storitvijo Facebook. Svoj StraightAs profil uporabnik poveže s svojim profilom na storitvi Facebook. |
| **Pogoji** 	| Vloga prijavljenega uporabnika. Uporabnik ime Facebook račun. Vzpostavljena je internetna povezava. |
| **Posledice** | Uporabnikov StraightAs račun je povezan s Facebook računom. Uporabnik pridobi vlogo Povezanega uporabnika. |
| **Posebnosti** 	| / |
| **Prioriteta** | Must have |
| **1. Osnovni tok** | 1. Prijavljen uporabnik izbere Povezava uporabnika |
|                    | 2. Sistem uporabniku prikaže okno za komunikacijo s Facebook sistemom |
|                    | 3. Facebook vrne odgovor o uspešni prijavi |
|                    | 4. Sistem uporabnika obvesti o uspešni povezavi |
| **Sprejemni test 3.1.1**  | |
| ***Funkcija, ki se testira*** | Povezava uporabnika prek uporabniškega vmesnika |
| ***Začetno stanje sistema***  | Uporabniku je na voljo gumb za povezavo s Facebookom |
| ***Vhod***                    | Odgovor Facebook API |
| ***Pričakovan rezultat***     | Uporabnik je povezan s svojim Facebook računom in dobi vlogo povezanega uporabnika | 
| **1. Izjemni tok** | Prijavljen uporabnik izbere Povezava uporabnika, pri čemer ni vzpostavljene internetne povezave. Sistem prikaže ustrezno sporočilo. |
| **Sprejemni test 3.3.1**  | |
| ***Funkcija, ki se testira*** | Internetna povezava ni vzpostavljena |
| ***Začetno stanje sistema***  | Uporabniku je na voljo gumb za povezavo s Facebookom |
| ***Vhod***                    | Timeout error |
| ***Pričakovan rezultat***     | Sistem obvesti uporabnika o napaki z ustreznim sporočilom | 
| **2. Izjemni tok** | Prijavljen uporabnik izbere Povezava uporabnika. Prijava v Facebook je neuspešna. Sistem prikaže ustrezno sporočilo. |
| **Sprejemni test 3.3.2**  | |
| ***Funkcija, ki se testira*** | Neuspešna povezava uporabnika prek uporabniškega vmesnika |
| ***Začetno stanje sistema***  | Uporabniku je na voljo gumb za povezavo s Facebookom |
| ***Vhod***                    | Odgovor Facebook API o neuspešni povezavi |
| ***Pričakovan rezultat***     | Sistem obvesti uporabnika o napaki z ustreznim sporočilom | 


| **Nadgradi račun v premium** | #4 |
| :------------------------------- | :-------------------------------|
| **Povzetek** 	| Povezan uporabnik lahko nadgradi svoj račun v premium. S tem dobi na voljo več funkcionalnosti. |
| **Pogoji** 	| Vloga povezanega uporabnika. Uporabnik ima Mastercard kartico, s katero lahko plača znesek v višini najmanj 5 eur. Vzpostavljena je internetna povezava. |
| **Posledice** | Uporabnikov StraightAs račun postane Premium. Uporabnik pridobi na voljo nove funkcionalnosti. |
| **Posebnosti** 	| / |
| **Prioriteta** | Must have |
| **1. Osnovni tok** | 1. Uporabnik izbere funkcionalnost Nadgradi račun v premium |
|                    | 2. Uporabnik vnese zahtevane podatke o svoji Mastercard kartici |
|                    | 3. Mastercard vrne sitemu StraightAs odgovor o uspešni preverbi kartice |
|                    | 4. Sistem uporabnika obvesti o uspešni nadgradnji |
| **Sprejemni test 4.1.1**  | |
| ***Funkcija, ki se testira*** | Preverba podatkov o uporabnikovi Mastercard kartici |
| ***Začetno stanje sistema***  | Uporabniku je na voljo grafični uporabniški vmesnik za vnos podatkov |
| ***Vhod***                    | Veljavni podatki o kartici, ki jih vnese uporabnik |
| ***Pričakovan rezultat***     | Uspešna poizvedba prek Mastercard API. Uporabnikova vloga je spremenjena v Premium uporabnik | 
| **1. Izjemni tok** | Uporabnik izbere Nadgradi račun v premium. Vnese neveljavne podatke o mastercard kartici. Sistem prikaže ustrezno sporočilo. |
| **Sprejemni test 4.3.1**  | |
| ***Funkcija, ki se testira*** | Prikaz obvestila o neveljavni kartici. |
| ***Začetno stanje sistema***  | Uporabniku je na voljo grafični uporabniški vmesnik za vnos podatkov |
| ***Vhod***                    | Neveljavni podatki o kartici, ki jih vnese uporabnik |
| ***Pričakovan rezultat***     | Mastercard API vrne odgovor o neveljavni kartici. Uporabniku se prikaže primerno obvestilo | 
| **2. Izjemni tok** | Uporabnik izbere Nadgradi račun v premium. Vnese veljavne podatke o mastercard kartici, s katero ne more plačati zneska 5 eur. Sistem prikaže ustrezno sporočilo. |
| **Sprejemni test 4.3.2**  | |
| ***Funkcija, ki se testira*** | Prikaz obvestila o nezmožnosti plačila s kartico. |
| ***Začetno stanje sistema***  | Uporabniku je na voljo grafični uporabniški vmesnik za vnos podatkov |
| ***Vhod***                    | Veljavni podatki o kartici, katera pa ni zmožna plačati zneska 5 eur |
| ***Pričakovan rezultat***     | Mastercard API vrne odgovor o nezmožnosti plačila zneska. Uporabniku se prikaže primerno obvestilo | 
| **3. Izjemni tok** | Uporabnik izbere Nadgradi račun v premium. Vnese podatke o mastercard kartici. Potrdi vnos. Sistem nima dostopa do interneta, zato prikaže ustrezno sporočilo. |
| **Sprejemni test 4.3.3**  | |
| ***Funkcija, ki se testira*** | Prikaz obvestila o prekinjeni internetni povezavi |
| ***Začetno stanje sistema***  | Uporabniku je na voljo grafični uporabniški vmesnik za vnos podatkov, sistem je brez povezave v internet |
| ***Vhod***                    | Podatki o kartici |
| ***Pričakovan rezultat***     | Timeout error. Uporabniku se prikaže primerno obvestilo | 


| **Izbira novih Facebook strani** | #5 |
| :------------------------------- | :-------------------------------|
| **Povzetek** 	| Povezani uporabnik lahko izbere nove Facebook strani, če želi biti obveščen o njihovih dogodkih. |
| **Pogoji** 	| Vloga povezanega uporabnika. Facebook strani morajo obstajati. |
| **Posledice** | Povezani uporabnik bo obveščen, ko bo izbrana stran objavila nov Facebook dogodek. |
| **Posebnosti** 	| / |
| **Prioriteta** | Should have |
| **1. Osnovni tok** | 1. Povezani uporabnik izbere funkcionalnost Izbira novih Facebook strani |
|                    | 2. Uporabniku je prikazan uporabniški vmesnik, kamor vnese www.facebook.com/pg/{id-strani}/ kjer namest {id-strani} vnese ime ali id strani.  |
|                    | 3. Sistem stran doda na seznam spremljanih strani |
|                    | 4. Sistem prikaže obvestilo o uspešno izvedeni zahtevi |
| **Sprejemni test 5.1.1**  | |
| ***Funkcija, ki se testira*** | Prikaz uporabniškega vmesnika za vnos URL povezave |
| ***Začetno stanje sistema***  | Prikaz seznama spremljanih Facebook strani |
| ***Vhod***                    | Uporabnik pritisne gumb za dodajo nove strani |
| ***Pričakovan rezultat***     | Uporabniku se prikaže vnosno polje, kamor lahko vnese povezavo do strani | 
| **Sprejemni test 5.1.2**  | |
| ***Funkcija, ki se testira*** | Dodaja nove Facebook |
| ***Začetno stanje sistema***  | Uporabniku je prikazan obrazec za vnos URL naslova |
| ***Vhod***                    | Pravilen URL naslov |
| ***Pričakovan rezultat***     | Obvestilo o uspešni dodaji in seznam spremljanih strani, ki vključuje tudi ravno dodano | 
| **1. Izjemni tok** | Sistem ne zna obdelati podanega URL naslova in prikaže sporočilo o napaki. |
| **Sprejemni test 5.3.1**  | |
| ***Funkcija, ki se testira*** | Prikaz obvestila o neveljavnem URL naslovu. |
| ***Začetno stanje sistema***  | Uporabniku je na voljo vnosno polje |
| ***Vhod***                    | Neveljaven URL naslov |
| ***Pričakovan rezultat***     | Sistem prekine obdelavo zahteve. Uporabniku se prikaže primerno obvestilo | 
| **2. Izjemni tok** | V sistemu pride do napake pri komunikaciji s storitvijo Facebook. Sistem prikaže obvestilo o neuspešno izvedeni zahtevi. |
| **Sprejemni test 5.3.2**  | |
| ***Funkcija, ki se testira*** | Prikaz obvestila o neuspešni komunikaciji s Facebookom. |
| ***Začetno stanje sistema***  | Uporabniku je na voljo vnosno polje |
| ***Vhod***                    | URL naslov do Facebook strani |
| ***Pričakovan rezultat***     | Sistem prekine obdelavo zahteve. Uporabniku se prikaže primerno obvestilo | 


| **Odstranjevanje Facebook strani** | #6 |
| :------------------------------- | :-------------------------------|
| **Povzetek** 	| Povezani uporabnik lahko odstrani Facebook strani, ki jih spremlja. |
| **Pogoji** 	| Povezan uporabnik spremlja vsaj eno Facebook stran. |
| **Posledice** | Povezan uporabnik strani ne spremlja več. |
| **Posebnosti** 	| / |
| **Prioriteta** | Should have |
| **1. Osnovni tok** | 1. Uporabnik izbere funkcionalnost Odstranjevanje Facebook strani |
|                    | 2. Sistem uporabniku prikaže seznam strani, ki jih trenutno spremlja  |
|                    | 3. Uporabnik izbere stran (ali več strani), ki jih želi odstraniti |
|                    | 4. Uporabnik potrdi izbiro in strani s tem odstrani |
|                    | 5. Uporabniku se prikaže osvežen seznam strani, ki jih spremlja |
| **Sprejemni test 6.1.1**  | |
| ***Funkcija, ki se testira*** | Odstranjevanje spremljane Facebook strani |
| ***Začetno stanje sistema***  | Prikaz seznama spremljanih Facebook strani |
| ***Vhod***                    | Izbira strani in potrditev izbire |
| ***Pričakovan rezultat***     | Seznam strani, ki ne prikazuje več izbrisanih | 
| **1. Izjemni tok** | Ob komunikaciji s podatkovno bazo pride do napake v sistemu. Sistem uporabnika obvesti o napaki. |
| **Sprejemni test 6.3.1**  | |
| ***Funkcija, ki se testira*** | Prikaz obvestila o napaki v sistemu. |
| ***Začetno stanje sistema***  | Prikaz seznama spremljanih Facebook strani |
| ***Vhod***                    | Izbira strani in potrditev izbire |
| ***Pričakovan rezultat***     | Nespremenjen seznam strani in obvestilo uporabniku o interni napaki | 


|**Dodajanje aktivnosti** | #7 | 
| :------------------------------- | :-------------------------------|
|**Povzetek** | Neprijavljeni, prijavljeni, povezani ali premium uporabnik lahko doda aktivnost na svoj urnik. |
| **Pogoji** | Vloga neprijavljenega, prijavljenega, povezanega ali premium uporabnika. |
| **Posledice** | Dodane aktivnosti so vidne na urniku prijavljenega, povezanega ali premium uporabnika. |
| **Posebnosti** | / |
| **Prioriteta** | Must have |
| **1. Osnovni tok** | 1. Neprijavljeni, prijavljeni, povezani ali premium uporabnik pritisne gumb "Dodaj aktivnost". |
|					 | 2. Neprijavljeni, prijavljeni, povezani ali premium uporabnik vnese podatke o aktivnosti: ime aktivnosti, začetni in končni čas aktivnosti, lokacijo, prioriteto, kategorijo. |
|					 | 3. Neprijavljeni, prijavljeni, povezani ali premium uporabnik potrdi kreacijo nove aktivnosti. |
|                    | 4. Sistem prikaže sporočilo o uspešnem vnosu, aktivnost je vidna na urniku. |
| **Sprejemni test 7.1.1** | |
| ***Funkcija, ki se testira*** | Dodajanje aktivnosti v urnik. |
| ***Začetno stanje sistema*** | Prikazan je vmesnik za vnos nove aktivnosti. |
| ***Vhod*** | Vpis podatkov za novo aktivnost: ime, čas začetka, čas konca, je enkraten ali tedenski dogodek, lokacija, prioriteta in kategorija. |
| ***Pričakovan rezultat*** | Prikaže se sporočilo, ki uporabnika obvesti o uspešnem vnosu. Na urniku se pokaže aktivnost. |
| **1. Izjemni tok** | Neprijavljeni, prijavljeni, povezani ali premium uporabnik vnese začetni čas, ki je poznejši od končnega časa. Sistem ga obvesti o napaki. |
| **Sprejemni test 7.2.1** | |
| ***Funkcija, ki se testira*** | Prikaz obvestila o napaki. |
| ***Začetno stanje sistema*** | Prikazan je vmesnik za vnos nove aktivnosti. |
| ***Vhod*** | Čas začetka, ki je večji kot čas konca. |
| ***Pričakovan rezultat*** | Nespremenjen urnik, obvestilo o interni napaki. |
| **2. Izjemni tok** | Neprijavljeni, prijavljeni, povezani ali premium uporabnik ne vnese vseh podatkov o aktivnosti. |
| **Sprejemni test 7.3.1** | |
| ***Funkcija, ki se testira*** | Prikaz obvestila o napaki. |
| ***Začetno stanje sistema*** | Prikazan je vmesnik za vnos nove aktivnosti. |
| ***Vhod*** | Nepopolni podatki za novo aktivnost. |
| ***Pričakovan rezultat*** | Nespremenjen urnik, obvestilo o interni napaki. |


|**Odstranjevanje aktivnosti** | #8 | 
| :------------------------------- | :-------------------------------|
|**Povzetek** |Neprijavljeni, prijavljeni, povezani ali premium uporabnik lahko izbriše obstoječo aktivnost na svojem urniku.  |
| **Pogoji** | Vloga neprijavljenega, prijavljenega, povezanega ali premium uporabnika, aktivnost mora obstajati. |
| **Posledice** | Odstanjena aktivnost z urnika. |
| **Posebnosti** | / |
| **Prioriteta** | Must have |
| **1. Osnovni tok** | 1. Neprijavljeni, prijavljeni, povezani ali premium uporabnik izbere željeno aktivnost na urniku. |
|					 | 2. Neprijavljeni, prijavljeni, povezani ali premium uporabnik izbere opcijo "izbriši aktivnost". |
|					 | 3. Neprijavljeni, prijavljeni, povezani ali premium uporabnik potrdi operacijo. |
|                    | 4. Sistem izbriše aktivnost. |
| **Sprejemni test 8.1.1** | |
| ***Funkcija, ki se testira*** | Odtranjevanje aktivnosti iz urnika. |
| ***Začetno stanje sistema*** | Prikazan urnik. |
| ***Vhod*** | Klic za odstranitev aktivnosti. |
| ***Pričakovan rezultat*** | Prikaže se obvestilo o uspešni odstranitvi aktivnosti. Aktivnost ni več vidna na urniku. |
| **1. Izjemni tok** | Neprijavljeni, prijavljeni, povezani ali premium uporabnik ne more izbrisati aktivnosti, ker ta ne obstaja.  Sistem ga obvesti o napaki. |
| **Sprejemni test 8.2.1** | |
| ***Funkcija, ki se testira*** | Prikaz obvestila o napaki. |
| ***Začetno stanje sistema*** | Prikazan urnik. |
| ***Vhod*** | Klic za odstranitev aktivnosti, ki ne obstaja več. |
| ***Pričakovan rezultat*** | Posodobljen urnik z ažurnimi podatki. Obvestilo o napaki. |


|**Urejanje aktivnosti** | #9 | 
| :------------------------------- | :-------------------------------|
|**Povzetek** | Neprijavljen uporabnik, prijavljen uporabnik, povezan uporabnik ali premium uporabnik lahko uredi obstoječo aktivnost. |
| **Pogoji** | Vloga neprijavljenega uporabnika, prijavljenega uporabnika, povezanega uporabnika ali premium uporabnika, aktivnost mora obstajati. |
| **Posledice** | Spremenjena aktivnost na urniku. |
| **Posebnosti** | / |
| **Prioriteta** | Must have |
| **1. Osnovni tok** | 1. 1. Neprijavljen, prijavljen uporabnik, povezan uporabnik ali premium uporabnik izbere aktivnost na urniku. |
|					 | 2. Neprijavljen, prijavljen uporabnik, povezan uporabnik ali premium uporabnik izbere možnost "Uredi aktivnost". |
|					 | 3. Neprijavljen uporabnik, prijavljen uporabnik, povezan uporabnik ali premium uporabnik popravi podatke o aktivnosti. |
|                    | 4. Sistem vnese popravljene podatke. |
|                    | 5. Prikaže se obvestilo u uspešnem vnosu. |
| **Sprejemni test 9.1.1** | |
| ***Funkcija, ki se testira*** | Urejanje aktivnosti na urniku. |
| ***Začetno stanje sistema*** | Prikazan je vmesnik za urejanje aktivnosti. |
| ***Vhod*** | Spremenjeni podatki o aktivnosti. |
| ***Pričakovan rezultat*** | Prikaže se obvestilo o uspešni spremembi aktivnosti. Spremembe se odražajo na urniku. |
| **1. Izjemni tok** | Neprijavljen uporabnik, prijavljen uporabnik, povezan uporabnik ali premium uporabnik vnese začetni čas, ki je poznejši od končnega časa. Sistem ga obvesti o napaki. |
| **Sprejemni test 9.2.1** | |
| ***Funkcija, ki se testira*** | Obvestilo o napaki. |
| ***Začetno stanje sistema*** | Prikazan je vmesnik za urejanje aktivnosti. |
| ***Vhod*** | Spremenjeni podatki o aktivnosti, kjer je začetni čas večji od končnega časa. |
| ***Pričakovan rezultat*** | Nespremenjena aktivnost. Obvestilo o napaki. |


|**Pošiljanje e-mail sporočila** | #10 | 
| :------------------------------- | :-------------------------------|
|**Povzetek** | Premium uporabnik lahko pošlje elektronsko sporočilo pedagogom na svoji fakulteti. |
| **Pogoji** | Vloga Premium uporabnika. |
| **Posledice** | Poslano elektronsko sporočilo. |
| **Posebnosti** | Potreben seznam elektronskih naslovov pedagogov na fakulteti. |
| **Prioriteta** | Would have |
| **1. Osnovni tok** | 1. Premium uporabnik pritisne gumb "pošlji e-mail". |
|					 | 2. Premium uporabniku se odpre novo okno za sestavo sporočila. |
|					 | 3. Iz seznama pedagogov izbere prejemnika. |
|                    | 4. Premium uporabnik napiše sporočilo in pritisne gumb "pošlji". |
|                    | 5. Sistem pošlje elektronsko sporočilo na prejemnikov elektronski naslov. |
| **Sprejemni test 10.1.1** | |
| ***Funkcija, ki se testira*** | Pošiljanje elektronskega sporočila. |
| ***Začetno stanje sistema*** | Prikazan vmesnik za sestavo elektronskega sporočila. |
| ***Vhod*** | Klic za oddajo elektronskega sporočila s podatki. |
| ***Pričakovan rezultat*** | Poslano elektronsko sporočilo. Obvestilo o uspehu. |
| **1. Izjemni tok** | Zaradi izpada povezave sistem ne more poslati elektronskega sporočila. Sistem premium uporabnika obvesti o napaki. |
| **Sprejemni test 10.2.1** | |
| ***Funkcija, ki se testira*** | Obvestilo o napaki zaradi izpada povezave. |
| ***Začetno stanje sistema*** | Prikazan vmesnik za sestavo elektronskega sporočila. |
| ***Vhod*** | Klic za oddajo elektronskega sporočila s podatki. |
| ***Pričakovan rezultat*** | Elektronsko sporočilo ni poslano. Obvestilo o napaki. |
| **2. Izjemni tok** | Prejemnikov elektronski naslov ne obstaja in pošiljanje elektronskega sporočila ne uspe. Sistem premium uporabnika obvesti o napaki. |
| **Sprejemni test 10.3.1** | |
| ***Funkcija, ki se testira*** | Obvestilo o napaki zaradi neveljavnega prejemnika. |
| ***Začetno stanje sistema*** | Prikazan je vmesnik za sestavo elektronskega sporočila. |
| ***Vhod*** | Klic za oddajo elektronskega sporočila z neveljavnim prejemnikom. |
| ***Pričakovan rezultat*** | Elektronsko sporočilo ni poslano. Obvestilo o napaki. |


|**Pregled ponudbe v izbrani restavraciji** | #11 | 
| :------------------------------- | :-------------------------------|
|**Povzetek** | Prijavljen uporabnik, povezan uporabnik ali premium uporabnik lahko pregleduje ponudbo v restavracijah, ki nudijo študentske bone. |
| **Pogoji** | Vloga prijavljenega, povezanega ali premium uporabnika. |
| **Posledice** | Pregled ponudbe v izbrani restvraciji. |
| **Posebnosti** | Dostop do podatkov o ponudbi restavracij. |
| **Prioriteta** | Would have |
| **1. Osnovni tok** | 1. Prijavljen, povezan ali premium uporabnik izbere razdelek "Ponudba". |
|					 | 2. Prijavljen, povezan ali premium uporabnik iz seznama izbere restavracijo. |
|					 | 3. Prijavljenemu, povezanemu ali premium uporabniku se odpre ponudba izbrane restavracije. |
| **Sprejemni test 11.1.1** | |
| ***Funkcija, ki se testira*** | Izbira restavracije prek seznama. |
| ***Začetno stanje sistema*** | Seznam restavracij. |
| ***Vhod*** | Izbira restavracije iz seznama. |
| ***Pričakovan rezultat*** | Odprejo se podrobnosti o ponudbi izbrane restavracije. |
| **1. Izjemni tok** | Sistem ne more pridobiti podatkov o seznamu restavracij. Sistem javi napako. |
| **Sprejemni test 11.2.1** | |
| ***Funkcija, ki se testira*** | Obvestilo o napaki o pridobivanju podatkov o seznamu restavracij. |
| ***Začetno stanje sistema*** | Prva stran aplikacije. |
| ***Vhod*** | Klik na razdelek "Ponudba". |
| ***Pričakovan rezultat*** | Nespremenjena prva stran. Obvestilo o napaki pri pridobivanju seznama restavracij. |
| **2. Izjemni tok** | Sistem ne more pridobiti podatkov o seznamu ponudbe izbrane restavracije. Sistem javi napako. |
| **Sprejemni test 11.3.1** | |
| ***Funkcija, ki se testira*** | Obvestilo o napaki o pridobivanju podatkov ponudbe. |
| ***Začetno stanje sistema*** | Seznam restavracij. |
| ***Vhod*** | Klik na restavracijo v seznamu. |
| ***Pričakovan rezultat*** | Nespremenjen seznam. Obvestilo o napaki pri pridobivanju ponudbe. |


|**Prijava na kategorijo dogodkov v StraightAs** | #12 | 
| :------------------------------- | :-------------------------------|
|**Povzetek** | Prijavljen uporabnik, povezan uporabnik ali premium uporabnik se lahko prijavi v kategorijo dogodkov. |
| **Pogoji** | Vloga prijavljenega uporabnika, povezanega uporabnika ali premium uporabnika. Uporabnik v tej kategoriji predhodno ni prijavljen. |
| **Posledice** | Prijavljen, povezan ali premium uporabnik bo prijavljen v kategorijo dogodkov in bo od sedaj te spremljal. |
| **Posebnosti** | / |
| **Prioriteta** | Must have |
| **1. Osnovni tok** | 1. Prijavljen uporabnik, povezan uporabnik ali premium uporabnik pritisne gumb "Prijavi me v kategorijo dogodkov". |
|					 | 2. Prijavljen uporabnik, povezan uporabnik ali premium uporabnik izbere željeno kategorijo iz spustnega menija ponujenih kategorij. |
|					 | 3. Prijavljen uporabnik, povezan uporabnik ali premium uporabnik potrdi svojo odločitev s klikom na gumb "Potrdi". |
|                    | 4. Sistem prikaže sporočilo o uspešni prijavi na željeno kategorijo in uporabniku začne prikazovati dogodke s te kategorije. |
| **Sprejemni test 12.1.1** | |
| ***Funkcija, ki se testira*** | Prijava na kategorijo dogodkov prek uporabniškega vmesnika. |
| ***Začetno stanje sistema*** | Prijavljen, povezan ali premium uporabnik  v kategorijo dogodkov še ni prijavljen. |
| ***Vhod*** | Izbira kategorije dogodka prek spustnega menija in potrditev. |
| ***Pričakovan rezultat*** | Prikaže se sporočilo o uspešni prijavi na kategorijo dogodkov. |
| **2. Alternativni tok** | 1. Prijavljen uporabnik, povezan uporabnik ali premium uporabnik pritisne gumb "Prijavi me v kategorijo dogodkov". |
|                         | 2. Prijavljen uporabnik, povezan uporabnik ali premium uporabnik ne najde željene kategorije med  ponujenimi kategorijami dogodkov. |
|                         | 3. Prijavljen uporabnik, povezan uporabnik ali premium uporabnik zaključi iskanje po kategorijah dogodkov. |
| **3. Alternativni tok** | 1. Prijavljen uporabnik, povezan uporabnik ali premium uporabnik svoje podatke in želje sporoči administratorju. |
|                         | 2. Administrator tega najde in ga izbere. |
|                         | 3. Administrator izbere funkcionalnost Prijava na kategorijo dogodkov v StraightAs. |
|                         | 4. Sistem prikaže obvestilo o uspešni izvedbi. |
| **Sprejemni test 12.3.1** | |
| ***Funkcija, ki se testira*** | Uporabnik je v kategorijo dogodkov prek CLI. |
| ***Začetno stanje sistema*** | Uporabnik v kategorijo dogodkov še ni prijavljen. |
| ***Vhod*** | Vpis kategorije dogodka in potrditev. |
| ***Pričakovan rezultat*** | Prikaže se sporočilo o uspešni prijavi na kategorijo dogodkov. |
| **1. Izjemni tok** | Sistem ne more pridobiti podatkov z API-ja in ne more generirati seznama kategorij. |
| **Sprejemni test 12.4.1** | |
| ***Funkcija, ki se testira*** | Obvestilo o napaki pri pridobivanju podatkov z API-ja. |
| ***Začetno stanje sistema*** | Prijavljen, povezan ali premium uporabnik  v kategorijo dogodkov še ni prijavljen. |
| ***Vhod*** | Vpis kategorije dogodka in potrditev. |
| ***Pričakovan rezultat*** | Obvestilo o napaki pri pridobivanju dogodkov. |


 **Odjava iz kategorije dogodkov v StraightAs** | #13 |
| :------------------------------- | :-------------------------------|
| **Povzetek** 	| Prijavljen uporabnik, povezan uporabnik ali premium uporabnik se lahko odjavi iz kategorije dogodkov. |
| **Pogoji** 	| Vloga prijavljenega uporabnika, povezanega uporabnika ali premium uporabnika. Uporabnik mora biti v to kategorijo predhodno prijavljen. |
| **Posledice** | Prijavljen uporabnik, povezan uporabnik ali premium uporabni se odjavi iz kategorije dogodkov in le te ne bo več spremljal. |
| **Posebnosti** 	| / |
| **Prioriteta** | Must have |
| **1. Osnovni tok** | 1. Prijavljen uporabnik, povezan uporabnik ali premium uporabnik pritisne gumb "Odjavi me iz kategorije dogodkov" |
|                    | 2. Prijavljen uporabnik, povezan uporabnik ali premium uporabnik izbere željeno kategorijo iz spustnega menija ponujenih kategorij. |
|                    | 3. Prijavljen uporabnik, povezan uporabnik ali premium uporabnik potrdi svojo odločitev s klikom na gumb "Potrdi". |
|                    | 4. Sistem prikaže sporočilo o uspešni odjavi iz željene kategorije in uporabniku preneha prikazovati dogodke s te kategorije. |
| **Sprejemni test 13.1.1**  | |
| ***Funkcija, ki se testira*** | Odjava iz kategorije dogodkov prek uporabniškega vmesnika |
| ***Začetno stanje sistema***  | Prijavljen uporabnik, povezan uporabnik ali premium uporabnik je v kategorijo dogodkov prijavljen |
| ***Vhod***                    | Izbira kategorije dogodka prek spustnega menija in potrditev |
| ***Pričakovan rezultat***     | Prikaže se sporočilo, da je sedaj uspešno odjavljen iz kategorije dogodkov | 
| **1. Alternativni tok** | 1. Prijavljen uporabnik, povezan uporabnik ali premium uporabnik svoje podatke in želje sporoči administratorju. |
|                    	  | 2. Administrator tega najde in ga izbere. |
|                    	  | 3. Administrator izbere funkcionalnost Odjava iz kategorije dogodkov v StraightAs. |
|                    	  | 4. Sistem prikaže obvestilo o uspešni izvedbi. |
| **Sprejemni test 13.2.1**  | |
| ***Funkcija, ki se testira*** | Odjava iz kategorije dogodkov prek CLI. |
| ***Začetno stanje sistema***  | Prijavljen uporabnik, povezan uporabnik ali premium uporabnik je v kategorijo dogodkov prijavljen |
| ***Vhod***                    | Vpis kategorije dogodka in potrditev|
| ***Pričakovan rezultat***     | Prikaže se sporočilo, ki uporabniku pokaže, da je sedaj uspešno odjavljen iz kategorije dogodkov. | 
| **1. Izjemi tok** | Prijavljen uporabnik, povezan uporabnik ali premium uporabnik se ne more odjaviti iz kategorije dogodkov, ker ta ne obstaja - istočasno se je izbrisala iz sistema. Sistem ga obvesti o napaki. |
| **Sprejemni test 13.3.1**  | |
| ***Funkcija, ki se testira*** | Odjava iz kategorije dogodkov prek uporabniškega vmesnika. |
| ***Začetno stanje sistema***  | Prijavljen uporabnik, povezan uporabnik ali premium uporabnik je v kategorijo dogodkov prijavljen. |
| ***Vhod***                    | Izbira kategorije dogodka prek spustnega menija in potrditev |
| ***Pričakovan rezultat***     | Prikaže se sporočilo, da se s kategorije dogodkov ni mogel odstraniti, saj ta ne obstaja. | 


| **Objava novega dogodka v StraightAs** | #14 |
| :------------------------------- | :-------------------------------|
| **Povzetek** 	|  Premium uporabnik lahko objavi nov dogodek |
| **Pogoji** 	| Vloga premium uporabnika. Enak dogodek ne sme biti že kreiran. Vnosno polje za datum, uro in opis dogodka morajo biti izpolnjeni. |
| **Posledice** | Nov dogodek bo objavljen in na voljo prijavljenim uporabnikom, povezanim uporabnikom ali premium uporabnikom. |
| **Posebnosti** 	| / |
| **Prioriteta** | Must have |
| **1. Osnovni tok** | 1. Premium uporabnik pritisne gumb "Objavi nov dogodek" |
|                    | 2. Premium uporabnik izbere željeno kategorijo iz spustnega menija ponujenih kategorij v katero želi objaviti nov dogodek. |
|                    | 3. Premium uporabnik potrdi svojo odločitev kategorije s klikom na gumb "Potrdi".  
|					 | 4. Premium uporabnik doda nov dogodek v kategorijo. |
|                    | 5. Sistem prikaže sporočilo o uspešno dodanem dogodku v kategorijo. Ta dogodek bo sedaj viden uporabnikom, ki so prijavljeni na to kategorijo dogodkov. |
| **Sprejemni test 14.1.1**  | |
| ***Funkcija, ki se testira*** | Objava novega dogodka prek uporabniškega vmesnika |
| ***Začetno stanje sistema***  | Dogodka še ne obstjaa in ga še ni v nobeni kategoriji |
| ***Vhod***                    | Izbira kategorije dogodka prek spustnega menija, vpis podatkov o dogodku in potrditev |
| ***Pričakovan rezultat***     | Prikaže se sporočilo, da je uspešno objavil nov dogodek | 
| **1. Alternativni tok** | 1. Premium uporabnik podatke o dogodku sporoči administratorju. |
|                    	  | 2. Administrator ta dogodek najde in ga izbere. |
|                    	  | 3. Administrator izbere funkcionalnost Objava novega dogodka v StraightAs. |
|                    	  | 4. Sistem prikaže obvestilo o uspešni izvedbi. |
| **Sprejemni test 14.2.1**  | |
| ***Funkcija, ki se testira*** | Objava novega dogodka prek CLI |
| ***Začetno stanje sistema***  | Dogodka še ne obstjaa in ga še ni v nobeni kategoriji |
| ***Vhod***                    | Vpis novega dogodka in podatkov v pravo kategorijo in potrditev |
| ***Pričakovan rezultat***     | Prikaže se sporočilo, da je uspešno objavil nov dogodek. | 


| **Odstranjevanje dogodka v StraightAs** | #15 |
| :------------------------------- | :-------------------------------|
| **Povzetek** 	|  Premium uporabnik lahko odstrani nek dogodek |
| **Pogoji** 	| Vloga premium uporabnika. Dogodek, ki ga želimo odstraniti mora predhodno obstajati. |
| **Posledice** | Dogodek, ki je bil na voljo bo sedaj izbrisan in ne bo več viden. Prav tako se bo odstranil iz urnikov vseh prijavljenih, povezanih in premium uporabnikov, ki so imeli označeno prisotnost na dogodku. |
| **Posebnosti** 	| / |
| **Prioriteta** | Must have |
| **1. Osnovni tok** | 1. Premium uporabnik pritisne gumb "Odstrani dogodek" |
|                    | 2. Premium uporabnik izbere željeno kategorijo iz spustnega menija ponujenih kategorij iz katere želi odstraniti dogodek. |
|                    | 3. Premium uporabnik potrdi svojo odločitev kategorije s klikom na gumb "Potrdi". | 
|					 | 4. Premium uporabnik odstrani dogodek iz kategorije. |
|                    | 5. Sistem prikaže sporočilo o uspešno odstranjenem dogodku iz kategorije. Ta dogodekne bo več viden uporabnikom, ki so prijavljeni na to kategorijo dogodkov. |
| **Sprejemni test 15.1.1**  | |
| ***Funkcija, ki se testira*** | Odstranjevanje obstoječega dogodka prek uporabniškega vmesnika |
| ***Začetno stanje sistema***  | Dogodek obstaja v neki kategoriji |
| ***Vhod***                    | Izbira kategorije dogodka prek spustnega menija, odstranitev željenega dogodka in potrditev |
| ***Pričakovan rezultat***     | Prikaže se sporočilo, ki prikaže, da je uspešno odstranil dogodek | 
| **1. Alternativni tok** | 1. Premium uporabnik pritisne gumb "Odstrani dogodek" |
|                    	  | 2. Premium uporabnik izbere željeno kategorijo iz spustnega menija ponujenih kategorij iz katere želi odstraniti dogodek. |
|                    	  | 3. Premium uporabnik potrdi svojo odločitev kategorije s klikom na gumb "Potrdi".  Istočasno je že nek drugi uporabnik odstranil dogodek. |
|                    	  | 4. Sistem javi sporočilo, da dogodka ni mogoče odstraniti. |
| **Sprejemni test 15.2.1**  | |
| ***Funkcija, ki se testira*** | Odstranjevanje obstoječega dogodka prek uporabniškega vmesnika |
| ***Začetno stanje sistema***  | Dogodek obstaja v neki kategoriji, a ga je istočasno že nekdo drug odstranil. |
| ***Vhod***                    | Izbira kategorije dogodka prek spustnega menija, odstranitev željenega dogodka in potrditev |
| ***Pričakovan rezultat***     | Prikaže se sporočilo, da dogodka ni mogoče odstraniti saj ta ne obstaja. | 
| **2. Alternativni tok** | 1. Premium uporabnik sporoči svoje podatke administratorju. |
|                    	  | 2. Administrator ročno s pomočjo CLI najde dogodek in ga odstrani. |
|                    	  | 3. Administrator sporoči premium uporabniku, da je dogodek uspešno odstranil.  |
| **Sprejemni test 15.2.2**  | |
| ***Funkcija, ki se testira*** | Odstranjevanje obstoječega dogodka prek CLI |
| ***Začetno stanje sistema***  | Dogodek obstaja v neki kategoriji |
| ***Vhod***                    | Vpis kategorije dogodka, odstranitev željenega dogodka in potrditev |
| ***Pričakovan rezultat***     | Prikaže se sporočilo, da je uspešno odstranil dogodek | 


| **Urejanje dogodka v StraightAs** | #16 |
| :------------------------------- | :-------------------------------|
| **Povzetek** 	|  Premium uporabnik lahko ureja nek dogodek |
| **Pogoji** 	| Vloga premium uporabnika. Dogodek mora predhodno obstajati. |
| **Posledice** | Premium uporabnik lahko uredi dogodek. Sprememba bo sedaj vidna vsem prijavljenim, povezanim ter premium uporabnikom. |
| **Posebnosti** 	| / |
| **Prioriteta** | Could have |
| **1. Osnovni tok** | 1. Premium uporabnik izbere funkcionalnost Urejanje dogodka. |
|                    | 2. Sistem premium uporabniku prikaže seznam vseh kategorij. Premium uporabnik eno izmed teh izbere. |
|                    | 3. Premium uporabnik izbere dogodek, ki ga želi urediti v tej kategoriji. | 
|					 | 4. Premium uporabnik potrdi/končna urejanje dogodka. |
|                    | 5. Dogodek je sedaj urejen. |
| **Sprejemni test 16.1.1**  | |
| ***Funkcija, ki se testira*** | Urejanje dogodka prek uporabniškega vmesnika |
| ***Začetno stanje sistema***  | Nek obstoječ dogodek s podatki |
| ***Vhod***                    | Izbira pravega dogodka preko spustnega menija in ureditev |
| ***Pričakovan rezultat***     | Prikaže se sporočilo, ki prikaže, da je uspešno uredil dogodek | 
| **1. Alternativni tok** | 1. Premium uporabnik podatke in želje o dogodku sporoči administratorju. |
|                    	  | 2. Administrator najde kategorijo, v kateri se ta dogodek nahaja. Izbere omenjen dogodek, ki ga želi spremeniti. |
|                    	  | 3. Administrator izbere funkcionalnost Urejanje dogodka v StraighAs. |
|                    	  | 4. Sistem prikaže obvestilo o uspešni izvedbi. |
| **Sprejemni test 16.2.1**  | |
| ***Funkcija, ki se testira*** | Urejanje dogodka preko CLI |
| ***Začetno stanje sistema***  | Nek obstoječ dogodek s podatki |
| ***Vhod***                    | Vpis dogodka, sprememba podatkov in potrditev |
| ***Pričakovan rezultat***     | Prikaže se sporočilo, ki prikaže, da je uspešno uredil dogodek. | 
| **1. Izjemni tok** | 1. Premium uporabnik izbere funkcionalnost Urejanje dogodka. |
|                    | 2. Sistem ne more pridobiti podatkov z API-ja. |
|                    | 3. Sistem posledično ne more generirati seznama dogodkov in posledično ne pridemo do željenega dogodka. |
| **Sprejemni test 16.3.1**  | |
| ***Funkcija, ki se testira*** | Urejanje dogodka prek uporabniškega vmesnika |
| ***Začetno stanje sistema***  | Nek obstoječ dogodek s podatki |
| ***Vhod***                    | Izbira pravega dogodka preko spustnega menija in ureditev |
| ***Pričakovan rezultat***     | Prikaže se sporočilo, da aplikacija ne mora dostopati do podatkov. | 


| **Predlog nove kategorije dogodkov v StraightAs** | #17 |
| :------------------------------- | :-------------------------------|
| **Povzetek** 	|  Premium uporabnik lahko predlaga nov tip kategorije dogodkov. Nova kategorija bo dodana v seznam. |
| **Pogoji** 	| Vloga premium uporabnika. Kategorija predhodno ne sme obstajati.  |
| **Posledice** | Nova kategorija bo dodana in vidna med seznamom trenutno že obstoječih. |
| **Posebnosti** 	| / |
| **Prioriteta** | Could have |
| **1. Osnovni tok** | 1. Premium uporabnik izbere funkcionalnost Predlog nove kategorije dogodkov. |
|                    | 2. Sistem premium uporabniku prikaže okno, kamor vpiše nov naziv kategorije. |
|                    | 3. Premium uporabnik potrdi/konča vpis s klikom na gumb potrdi. | 
|					 | 4. Nova kategorija dogodkov je sedaj vidna. |
| **Sprejemni test 17.1.1**  | |
| ***Funkcija, ki se testira*** | Predlog nove kategorije preko uporabniškega vmesnika |
| ***Začetno stanje sistema***  | Nova kategorija še ne obstaja v seznamu trenutno obstoječih kategorij |
| ***Vhod***                    | Vpis podatkov za novo kategorijo in potrditev. |
| ***Pričakovan rezultat***     | Prikaže se sporočilo, ki prikaže, da je bila nova kategorija uspešno dodana | 


| **Dodajanje ocene porabe časa na aktivnost** | #18 |
| :------------------------------- | :-------------------------------|
| **Povzetek** 	|  Prijavljen, povezan ali premium uporabnik lahko določi oceno trajanja glede na izkušnje in intuicijo |
| **Pogoji** 	| Vloga prijavljenega, povezanega ali premium uporabnika. Ocena porabe casa na aktivnost se ne sme obstajati, da se jo lahko doda. |
| **Posledice** | Ocena porabe casa za aktivnost se shrani v sistem. |
| **Posebnosti** 	| / |
| **Prioriteta** | Could have |
| **1. Osnovni tok** | 1. Prijavljen, povezan ali premium uporabnik ob aktivnosti klikne gumb "dodaj oceno porabe časa" |
|                    | 2. Prijavljenemu, povezanemu ali premium uporabniku se prikaže okno za vnos ocene časa. |
|                    | 3. Prijavljen, povezan ali premium uporabnik potrdi vpisano oceno časa | 
|					 | 4. Sistem posodobi oceno porabe časa |
|                    | 5. Prijavljenemu, povezanemu ali premium uporabniku se prikaže sporocčlo o uspešnem vnosu. |
| **Sprejemni test 18.1.1**  | |
| ***Funkcija, ki se testira*** | Dodajanje ocene porabe časa prek uporabniškega vmesnika |
| ***Začetno stanje sistema***  | Aktivnost nima dodeljene ocene porabe časa |
| ***Vhod***                    | Vpis ocene porabe časa aktivnosti in potrditev |
| ***Pričakovan rezultat***     | Aktivnosti dodana vnešena ocena porabe časa | 


| **Odstranjevanje ocene porabe časa na aktivnost** | #19 |
| :------------------------------- | :-------------------------------|
| **Povzetek** 	|  Dodano oceno porabe časa prijavljen, povezan ali premium uporabnik lahko tudi odstrani. |
| **Pogoji** 	| Vloga prijavljenega, povezanega in premium uporabnika. Ocena porabe časa na aktivnost mora biti dodana, da se jo lahko odstrani. |
| **Posledice** | Ocena porabe časa za aktivnost se izbriše. |
| **Posebnosti** 	| / |
| **Prioriteta** | Could have |
| **1. Osnovni tok** | 1. Prijavljen, povezan ali premium uporabnik ob aktivnosti klikne gumb "Odstrani oceno porabe časa". |
|                    | 2. Sistem izbriše oceno porabe časa. |
|                    | 3. Prijavljenemu, povezanemu ali premium uporabniku se prikaže sporočilo o uspešnem izbrisu. | 
| **Sprejemni test 19.1.1**  | |
| ***Funkcija, ki se testira*** | Odstranjevanje ocene porabe časa prek uporabniškega vmesnika |
| ***Začetno stanje sistema***  | Aktivnost ima dodeljeno oceno porabe časa |
| ***Vhod***                    | Klik na izbris ocene porabe časa |
| ***Pričakovan rezultat***     | Aktivnost je sedaj brez ocene porabe časa | 


| **Urejanje ocene porabe časa na aktivnost** | #20 |
| :------------------------------- | :-------------------------------|
| **Povzetek** 	| Premium uporabnik lahko oceno porabe casa na aktivnost tudi spremeni |
| **Pogoji** 	| Vloga premium uporabnika. Ocena porabe casa na aktivnost mora biti dodana, da se jo lahko spreminja. |
| **Posledice** | Ocena porabe casa za aktivnost se spremeni na novo vrednost. |
| **Posebnosti** 	| / |
| **Prioriteta** | Could have |
| **1. Osnovni tok** | 1. Premium uporabnik ob aktivnosti klikne gumb ""spremeni oceno porabe časa" |
|                    | 2. Premium uporabniku se prikaže okno za vnos nove ocene časa. |
|                    | 3. Premium uporabnik potrdi vpisano oceno časa. |
|                    | 4. Sistem posodobi oceno porabe časa. |
|                    | 5. Premium uporabniku se prikaže sporočilo o uspešnem vnosu. |
| **Sprejemni test 20.1.1**  | |
| ***Funkcija, ki se testira*** | Spreminjanje ocene porabe casa prek uporabniskega vmesnika |
| ***Začetno stanje sistema***  | Aktivnost ima dodeljeno oceno porabe časa |
| ***Vhod***                    | Vpis nove ocene porabe časa aktivnosti in potrditev |
| ***Pričakovan rezultat***     | Izbrisana stara ocena porabe časa, aktivnosti dodana vnesena nova ocena porabe časa | 
| **1. Izjemni tok** | Ob komunikaciji s podatkovno bazo pride do napake v sistemu. Sistem uporabnika obvesti o napaki. |
| **Sprejemni test 20.3.1**  | |
| ***Funkcija, ki se testira*** | Prikaz obvestila o napaki v sistemu. |
| ***Začetno stanje sistema***  | Aktivnost ima dodeljeno oceno porabe časa |
| ***Vhod***                    | Vpis nove ocene porabe časa aktivnosti in potrditev |
| ***Pričakovan rezultat***     | Nespremenjena oceno porabe časa in obvestilo uporabniku o interni napaki | 


| **Pregled študentskega življenja** | #21 |
| :------------------------------- | :-------------------------------|
| **Povzetek** 	| Prijavljen, povezan ali premium uporabnik lahko prelgeduje statistiko uporabe aplikacije |
| **Pogoji** 	| Vloga prijavljenega, povezanega ali premium uporabnika |
| **Posledice** | Pregled statistike. Prijavljen, povezan ali premium uporabnik vidi koliko ur je porabil za študij, študijske dejavnosti, obštudijske dejavnosti, na katerih dogodkih je bil, katere so njegove najbolj aktivne kategorije. |
| **Posebnosti** 	| / |
| **Prioriteta** | Could have |
| **1. Osnovni tok** | 1. Prijavljen, povezan ali premium uporabnik odpre razdelek "Pregled študentskega življenja" |
|                    | 2. Prijavljen, povezan ali premium uporabniku se prikažejo osnovni statistični podatki. |
|                    | 3. Prijavljen, povezan ali premium uporabnik iz osnovnih podatkov vidi podrobnosti po razdelkih (porabljene ure, število nalog, število udeleženih dogodkov, glavne kategorije). |
| **Sprejemni test 21.1.1**  | |
| ***Funkcija, ki se testira*** | Pregled statistike |
| ***Začetno stanje sistema***  | začetna stran premium uporabnika |
| ***Vhod***                    | Izbira razdelka "Pregled študentskega življenja" |
| ***Pričakovan rezultat***     | Prikaže se nova stran s statistiko o uporabniku - število ur na kategorijo, število nalog, kolokvijev in izpitov, povprečno število aktivnosti na teden, najpopularnejše kategorije, število udeleženih dogodkov | 
| **1. Izjemni tok** | Ob komunikaciji s podatkovno bazo pride do napake v sistemu. Sistem uporabnika obvesti o napaki. |
| **Sprejemni test 21.3.1**  | |
| ***Funkcija, ki se testira*** | Prikaz obvestila o napaki v sistemu. |
| ***Začetno stanje sistema***  | začetna stran premium uporabnika |
| ***Vhod***                    | Izbira razdelka "Pregled študentskega življenja"  |
| ***Pričakovan rezultat***     | Obvestilo uporabniku o interni napaki, še vedno se prikazuje začetna stran premium uporabnika | 


| **Dodajanje prioritete na aktivnosti** | #22 |
| :------------------------------- | :-------------------------------|
| **Povzetek** 	| Aktivnosti lahko prijavljen uporabnik, povezan uporabnik ali premium uporabnik doloci prioriteto, aktivnosti z vijo prioriteto so uporabniko bolj pomembne. |
| **Pogoji** 	| Vloga prijavljenega uporabnika, povezanega uporabnika ali premium uporabnika. Prioriteta se ne sme biti dodeljena, da se jo lahko dodeli. |
| **Posledice** | Prioriteta aktivnosti se doda v sistem |
| **Posebnosti** 	| / |
| **Prioriteta** | Must have |
| **1. Osnovni tok** | 1. Prijavljen uporabnik, povezan uporabnik ali premium uporabnik ob aktivnosti klikne gumb "dodaj prioriteto" |
|                    | 2. Prijavljenemu uporabniku, povezanemu uporabniku ali premium uporabniku se prikaze okno za vnos prioritete |
|                    | 3. Prijavljen uporabnik, povezan uporabnik ali premium uporabnik potrdi vpisano prioriteto |
|                    | 4. Sistem posodobi prioriteto za to aktivnost. |
|                    | 5.  Prijavljenemu uporabniku, povezanemu uporabniku ali premium uporabniku se prikaže sporočilo o uspešno dodani prioriteti |
| **Sprejemni test 20.1.1**  | |
| ***Funkcija, ki se testira*** | Dodajanje prioritete prek uporabniskega vmesnika |
| ***Začetno stanje sistema***  | Aktivnost nima dodeljene prioritete |
| ***Vhod***                    | Vpis prioritete aktivnosti in potrditev |
| ***Pričakovan rezultat***     | Aktivnosti dodana vnesena prioriteta | 
| **1. Izjemni tok** | Aktivnost ima že dodeljeno prioriteto |
| **Sprejemni test 20.3.1**  | |
| ***Funkcija, ki se testira*** | Prikaz obvestila o aktivnosti, ki že obstaja |
| ***Začetno stanje sistema***  | Aktivnost ima dodeljeno prioriteto |
| ***Vhod***                    | Vpis nove prioritete aktivnosti in potrditev |
| ***Pričakovan rezultat***     | Nespremenjena prioriteta in obvestilo uporabniku, da prioriteta že obstaja | 
| **2. Izjemni tok** | Ob komunikaciji s podatkovno bazo pride do napake v sistemu. Sistem uporabnika obvesti o napaki. |
| **Sprejemni test 20.3.2**  | |
| ***Funkcija, ki se testira*** | Prikaz obvestila o napaki v sistemu. |
| ***Začetno stanje sistema***  | Aktivnost nima dodeljene prioritete |
| ***Vhod***                    | Vpis prioritete aktivnosti in potrditev |
| ***Pričakovan rezultat***     | Nespremenjena prioriteta aktivnosti in obvestilo uporabniku o interni napaki | 


| **Odstranjevanje prioritete na aktivnosti** | #23 |
| :------------------------------- | :-------------------------------|
| **Povzetek** 	| Dodano prioriteto lahko prijavljen uporabnik, povezan uporabnik ali premium uporabnik tudi odstrani |
| **Pogoji** 	| Vloga prijavljenega uporabnika, povezanega uporabnika ali premium uporabnika. Prioriteta za aktivnost mora biti dodana, da se jo lahko spreminja. |
| **Posledice** | Prioriteta za aktivnost se izbrise. |
| **Posebnosti** 	| / |
| **Prioriteta** | Must have |
| **1. Osnovni tok** | 1. Prijavljen uporabnik, povezan uporabnik ali premium uporabnik ob aktivnosti klikne gumb "odstrani prioriteto" |
|                    | 2. Sistem pizbriše prioriteto za to aktivnost. |
|                    | 3.  Prijavljenemu uporabniku, povezanemu uporabniku ali premium uporabniku se prikaže sporočilo o uspešno izbrisani prioriteti |
| **Sprejemni test 20.1.1**  | |
| ***Funkcija, ki se testira*** | Odstranjevanje prioritete prek uporabniskega vmesnika |
| ***Začetno stanje sistema***  | Aktivnost ima dodeljeno prioriteto |
| ***Vhod***                    | Klik na izbris prioritete |
| ***Pričakovan rezultat***     | Aktivnost brez prioritete | 
| **2. Izjemni tok** | Ob komunikaciji s podatkovno bazo pride do napake v sistemu. Sistem uporabnika obvesti o napaki. |
| **Sprejemni test 20.3.2**  | |
| ***Funkcija, ki se testira*** | Prikaz obvestila o napaki v sistemu. |
| ***Začetno stanje sistema***  | Aktivnost ima dodeljeno prioriteto |
| ***Vhod***                    | Klik na izbris prioritete |
| ***Pričakovan rezultat***     | Nespremenjena prioritete aktivnosti in obvestilo uporabniku o interni napaki | 


| **Spreminjanje prioritete na aktivnosti** | #24 |
| :------------------------------- | :-------------------------------|
| **Povzetek** 	| Premium uporabnik lahko prioriteto za aktivnost tudi spremeni |
| **Pogoji** 	| Vloga premium uporabnika. Prioriteta za aktivnost mora biti dodana, da se jo lahko spreminja. |
| **Posledice** | Prioriteta za aktivnost se spremeni na novo vrednost. |
| **Posebnosti** 	| / |
| **Prioriteta** | Should have |
| **1. Osnovni tok** | 1. Premium uporabnik ob aktivnosti klikne gumb "spremeni prioriteto" |
|                    | 2. Premium uporabniku se prikaze okno za vnos prioritete |
|                    | 3. Premium uporabnik potrdi vpisano prioriteto |
|                    | 4. Sistem posodobi prioriteto za to aktivnost. |
|                    | 5.  Premium uporabniku se prikaže sporočilo o uspešno dodani prioriteti |
| **Sprejemni test 20.1.1**  | |
| ***Funkcija, ki se testira*** | Spreminjanje prioritete prek uporabniskega vmesnika |
| ***Začetno stanje sistema***  | Aktivnost ima dodeljeno prioriteto |
| ***Vhod***                    | Vpis nove prioritete aktivnosti in potrditev |
| ***Pričakovan rezultat***     | Aktivnosti dodana posodobljena prioriteta | 
| **2. Izjemni tok** | Ob komunikaciji s podatkovno bazo pride do napake v sistemu. Sistem uporabnika obvesti o napaki. |
| **Sprejemni test 20.3.2**  | |
| ***Funkcija, ki se testira*** | Prikaz obvestila o napaki v sistemu. |
| ***Začetno stanje sistema***  | Aktivnost ima dodeljeno prioriteto |
| ***Vhod***                    | Vpis nove prioritete aktivnosti in potrditev |
| ***Pričakovan rezultat***     | Nespremenjena prioriteta aktivnosti in obvestilo uporabniku o interni napaki | 


|**Prekinitev povezave StraightAs profila s StraightAs profilom drugega uporabnika** | #25 | 
| :------------------------------- | :-------------------------------|
|**Povzetek** | Premium uporabnik lahko prekine povezavo do drugega profila premium uporabnika. |
| **Pogoji** | Vloga premium uporabnika, obstoječa povezava med dvema premium uporabnikoma. |
| **Posledice** | Izbrisana povezava med premium uporabnikoma - urnika in seznam opravil nista več deljena. |
| **Posebnosti** | / |
| **Prioriteta** | Would have |
| **1. Osnovni tok** | 1. Premium uporabnik odpre profil drugega premium uporabnika. |
|					 | 2. Premium uporabnik pritisne gumb "prekini povezavo". |
|					 | 3. Sistem izbriše povezavo. |
|                    | 4. Oba premium uporabnika sta obveščena o prekinitvi povezave. |
| **Sprejemni test 25.1.1** | |
| ***Funkcija, ki se testira*** | Prekinitev povezave prek gumba. |
| ***Začetno stanje sistema*** | Obstoječa povezava med dvema premium uporabnikoma. |
| ***Vhod*** | Pritisk gumba "Prekini povezavo". |
| ***Pričakovan rezultat*** | Prekinjena povezava. Urnik in seznami opravil niso več deljeni med prej povezanima premium uporabnikoma. |
| **2. Alternativni tok** | 1. Eden od povezanih premium uporabnikov postane prijavljen ali povezan uporabnik. |
|                         | 2. Sistem zazna spremembo in sam izbriše povezavo. |
|                         | 3. Sistem obvesti premium uporabnika o spremembi. |
| **Sprejemni test 25.2.1** | |
| ***Funkcija, ki se testira*** | Samodejna prekinitev povezave. |
| ***Začetno stanje sistema*** | Obstoječa povezava med dvema premium uporabnikoma. |
| ***Vhod*** | Premium uporabnik postane prijavljen uporabnik ali povezan uporabnik. |
| ***Pričakovan rezultat*** | Prekinjena povezava. Urnik in seznami opravil niso več deljeni med prej povezanima premium uporabnikoma. |
| **1. Izjemni tok** | Po tem, ko premium uporabnik prisitne gumb "prekini povezavo", sistemu ne uspe izbrisati povezave. Sistem zabeleži napako. |
| **Sprejemni test 25.3.1** | |
| ***Funkcija, ki se testira*** | Obvestilo o napaki pri ročni prekinitvi povezave. |
| ***Začetno stanje sistema*** | Obstoječa povezava med dvema premium uporabnikoma. |
| ***Vhod*** | Pritisk gumba "Prekini povezavo". |
| ***Pričakovan rezultat*** | Povezava ni prekinjena. Obvestilo o napaki pri poskusu prekinitve povezave. |
| **2. Izjemni tok** | Po tem, ko eden od premium uporabnikov postane prijavljen uporabnik ali povezan uporabnik, sistemu ne uspe samodejno izbrisati povezave. Sistem zabeleži napako. |
| **Sprejemni test 25.4.1** | |
| ***Funkcija, ki se testira*** | Obvestilo o napaki pri samodejni prekinitvi povezave. |
| ***Začetno stanje sistema*** | Obstoječa povezava med dvema premium uporabnikoma. |
| ***Vhod*** | Premium uporabnik postane prijavljen uporabnik ali povezan uporabnik. |
| ***Pričakovan rezultat*** | Povezava ni prekinjena. Obvestilo o napaki pri poskusu samodejne prekinitve povezave. |


|**Povezava StraightAs profila s StraightAs profilom drugega uporabnika** | #26 | 
| :------------------------------- | :-------------------------------|
|**Povzetek** | Premium uporabnik lahko poveže svoj urnik in svoje sezname opravil z drugim premium uporabnikom. |
| **Pogoji** | Vloga premium uporabnika. |
| **Posledice** | Ustvarjena povezava med premium uporabnikoma - ta dva si delita urnik in sezname opravil. |
| **Posebnosti** | / |
| **Prioriteta** | Would have |
| **1. Osnovni tok** | 1. Premium uporabnik odpre profil drugega premium uporabnika. |
|					 | 2. Premium uporabnik klikne gumb "poveži". |
|					 | 3. Drugi premium uporabnik je obveščen o prošnji za povezavo. |
|                    | 4. Drugi premium uporabnik potrdi prošnjo. |
| **Sprejemni test 26.1.1** | |
| ***Funkcija, ki se testira*** | Povezava dveh premium uporabnikov. |
| ***Začetno stanje sistema*** | Dva nepovezana premium uporabnika. |
| ***Vhod*** | Pritisk gumba "Poveži". |
| ***Pričakovan rezultat*** | Obvestilo o prošnji za povezavo drugemu premium uporabniku. Potrditev prošnje. Obvestilo potrditve prošnje prvemu premium uporabniku. Deljen urnik in seznami opravil med premium uporabnikoma. |
| **1. Izjemni tok** | Premium uporabnik pritisne gumb "Poveži". Drugi premium uporabnik je obveščen o prošnji za povezavo, a jo zavrne. Premium uporabnik dobi obvestilo o zavrnitvi. |
| **Sprejemni test 26.2.1** | |
| ***Funkcija, ki se testira*** | Obvestilo o zavrnitvi prošnje za povezavo. |
| ***Začetno stanje sistema*** | Dva nepovezana premium uporabnika. |
| ***Vhod*** | Pritisk gumba "Poveži" prvega premium uporabnika, zavrnitev drugega premium uporabnika. |
| ***Pričakovan rezultat*** | Obvestilo prvemu premium uporabniku o zavrnitvi prošnje za povezavo. |


|**Pregled urnika** | #27 | 
| :------------------------------- | :-------------------------------|
|**Povzetek** | Neprijavljeni, prijavljeni, povezani ali premium uporabnik lahko pregleduje urnik aktivnosti. |
| **Pogoji** | Vloga neprijavljenega, prijavljenega, povezanega ali premium uporabnika. Urnik mora obstajati. |
| **Posledice** | Neprijavljeni, prijavljeni, povezani ali premium uporabnik vidi urnik. |
| **Posebnosti** | / |
| **Prioriteta** | Must have |
| **1. Osnovni tok** | 1. Neprijavljeni, prijavljeni, povezani ali premium uporabnik klikne razdelek "Urnik". |
|					 | 2. Sistem preusmeri neprijavljenega, prijavljenega, povezanega ali premium uporabnikna novo stran, kjer se prikaže urnik aktivnosti. |
| **Sprejemni test 27.1.1** | |
| ***Funkcija, ki se testira*** | Pregled urnika. |
| ***Začetno stanje sistema*** | Prva stran aplikacije. |
| ***Vhod*** | Klik na razdelek "Urnik". |
| ***Pričakovan rezultat*** | Prikaz urnika neprijavljenega, prijavljenega, povezanega ali premium uporabnika. |
| **1. Izjemni tok** | Pridobivanje podatkov iz zalednega sistema ne uspe. Sistem sporoči napako. |
| **Sprejemni test 27.2.1** | |
| ***Funkcija, ki se testira*** | Obvestilo o napaki pri pridobivanju podatkov. |
| ***Začetno stanje sistema*** | Prva stran aplikacije. |
| ***Vhod*** | Klik na razdelek "Urnik". |
| ***Pričakovan rezultat*** | Nespremenjena prva stran. Obvestilo o napaki pri pridobivanju podatkov. |


|**Odjava od dogodka** | #28 | 
| :------------------------------- | :-------------------------------|
|**Povzetek** | Prijavljen, povezan ali premium uporabnik lahko odstrani svojo prisotnost na dogodku. Dogodek se s tem odstrani iz njegovega urnika. |
| **Pogoji** | Na dogodek, od katerega se prijavljen, povezan ali premium uporabnik želi odjaviti, mora biti prijavljen. |
| **Posledice** | Dogodek se odstrani iz njegovega urnika. |
| **Posebnosti** | / |
| **Prioriteta** | Must have |
| **1. Osnovni tok** | 1. Prijavljen, povezan ali premium uporabnik odpre stran dogodka. |
|					 | 2. Prijavljen, povezan ali premium uporabnik prisitne gumb "Odstrani udeležbo". |
|                    | 3. Dogodek je izbrisan iz urnika. |
| **Sprejemni test 28.1.1** | |
| ***Funkcija, ki se testira*** | Odjava od dogodka prek gumba "Odstrani udeležbo". |
| ***Začetno stanje sistema*** | Prijavljen, povezan ali premium uporabnik je prijavljen na dogodek. |
| ***Vhod*** | Klik na gumb "Odstrani udeležbo". |
| ***Pričakovan rezultat*** | Odjava od dogodka. Aktivnost, ki predstavlja dogodek, je izbrisana z urnika. |
| **2. Alternativni tok** | 1. Prijavljen, povezan ali premium uporabnik odpre urnik. |
|                         | 2. Prijavljen, povezan ali premium uporabnik pritisne aktivnost, ki predstavlja dogodek. |
|                         | 3. Prijavljen, povezan ali premium uporabnik pritisne gumb "izbirši". |
|                         | 4. Dogodek je izbrisan iz urnika. |
| **Sprejemni test 28.2.1** | |
| ***Funkcija, ki se testira*** | Odjava od dogodka prek izbrisa aktivnosti na urniku, ki predstavlja dogodek. |
| ***Začetno stanje sistema*** | Prijavljen, povezan ali premium uporabnik je prijavljen na dogodek. |
| ***Vhod*** | Klic za izbris aktivnosti na urniku, ki predstavlja dogodek. |
| ***Pričakovan rezultat*** | Odjava od dogodka. Aktivnost, ki predstavlja dogodek, je izbrisana z urnika. |
| **1. Izjemni tok** | Klicu za odstranitev udeležbe na dogodku ne uspe. Sistem javi napako. |
| **Sprejemni test 28.3.1** | |
| ***Funkcija, ki se testira*** | Obvestilo o napaki pri odstranitvi udeležbe na dogodku. |
| ***Začetno stanje sistema*** | Prijavljen, povezan ali premium uporabnik je prijavljen na dogodek. |
| ***Vhod*** | Klik na gumb "Odstrani udeležbo". |
| ***Pričakovan rezultat*** | Nespremenjen urnik. Prijavljen, povezan ali premium uporabnik je še vedno prijavljen na dogodek. Obvestilo o interni napaki. |
| **2. Izjemni tok** | Ob klicu za odstranitev udeležbe na dogodku sistemu ne uspe izbrisati dogodka z urnika. Sistem javi napako. |
| **Sprejemni test 28.4.1** | |
| ***Funkcija, ki se testira*** | Obvestilo o napaki pri odstranitvi dogodka z urnika. |
| ***Začetno stanje sistema*** |  Odjava od dogodka prek izbrisa aktivnosti na urniku, ki predstavlja dogodek. |
| ***Vhod*** | Klic za izbris aktivnosti na urniku, ki predstavlja dogodek. |
| ***Pričakovan rezultat*** | Dogodek je še vedno na urniku. Obvestilo o napaki pri odjavi od dogodka. |


| **Dodajanje zapisa na To-do list** | #29 |
| :------------------------------- | :-------------------------------|
| **Povzetek** 	| Neprijavljen, prijavljen, povezan in premium uporabnik lahko dodajo zapis na To-do list. S tem si shrani stvar, ki jo mora opraviti, le-ta pa ni časovno določena. |
| **Pogoji** 	| Vloga neprijavljenega, prijavljenega, povezanega in premium uporabnika. |
| **Posledice** | Zapis je dodan na To-do list. |
| **Posebnosti** 	| / |
| **Prioriteta** | Must have |
| **1. Osnovni tok** | 1. Neprijavljen, prijavljen, povezan ali premium uporabnik izbere funkcionalnost Dodajanje zapisa na To-do list. |
|                    | 2. Neprijavljenemu, prijavljenemu, povezanemu in premium uporabniku se prikaže okno, kjer lahko vnese naslov in opis. |
|                    | 3. Neprijavljen, prijavljen, povezan in premium uporabnik shrani zapis. |
|                    | 4. Sistem prikaže zapis na seznamu To-do. |
| **Sprejemni test 29.1.1**  | |
| ***Funkcija, ki se testira*** | Dodajanje zapisa na seznam To-do |
| ***Začetno stanje sistema***  | Vnosno polje za naslov in opis zapisa |
| ***Vhod***                    | Naslov in opis zapisa |
| ***Pričakovan rezultat***     | Posodobljen seznam zapisov, ki vsebuje tudi ravno dodanega | 


| **Odstranjevanje zapisa iz To-do lista** | #30 |
| :------------------------------- | :-------------------------------|
| **Povzetek** 	| Neprijavljen, prijavljen, povezan in premium uporabnik lahko odstranijo zapis na To-do list. Zapis izgine iz njegovega To-do list. |
| **Pogoji** 	| Vloga neprijavljenega, prijavljenega, povezanega in premium uporabnika. To-do list mora vsebovati vsaj en zapis. |
| **Posledice** | Zapis je odstranjen iz To-do lista. |
| **Posebnosti** 	| / |
| **Prioriteta** | Must have |
| **1. Osnovni tok** | 1. Neprijavljen, prijavljen, povezan ali premium uporabnik izbere funkcionalnost Dodajanje zapisa na To-do list. |
|                    | 2. Neprijavljenemu, prijavljenemu, povezanemu in premium uporabniku se prikaže okno, kjer lahko vnese naslov in opis. |
|                    | 3. Neprijavljen, prijavljen, povezan in premium uporabnik shrani zapis. |
|                    | 4. Sistem prikaže zapis na seznamu To-do. |
| **Sprejemni test 30.1.1**  | |
| ***Funkcija, ki se testira*** | Odstranjevanje zapisa iz seznama To-do |
| ***Začetno stanje sistema***  | Prikaz zapisov na seznamu To-do |
| ***Vhod***                    | Izbira elementa iz seznama |
| ***Pričakovan rezultat***     | Posodobljen seznam zapisov, ki ne prikazuje več izbrisanega | 


| **Vklop funkcionalnosti Avtomatska razporeditev časa** | #31 |
| :------------------------------- | :-------------------------------|
| **Povzetek** 	| Premium uporabnik lahko vklopi avtomatsko razporeditev časa. Njegove aktivnosti in poraba časa na urniku se avtomatsko razporedijo.  |
| **Pogoji** 	| Vloga premium uporabnika, funkcionalnost Avtomatsko razporejanje časa mora biti izklopljena |
| **Posledice** | Funkcionalnost Avtomatsko razporejanje časa je vkljopljena |
| **Posebnosti** 	| / |
| **Prioriteta** | Should have |
| **1. Osnovni tok** | 1. Premium uporabnik izbere funkcionalnost Avtomatsko razporejanje časa |
|                    | 2. Sistem funkcionalnost označi kot aktivno. |
|                    | 3. Sistem izvede postopek avtomatske razporeditve časa. |
| **Sprejemni test 31.1.1**  | |
| ***Funkcija, ki se testira*** | Vklop Avtomatske razporeditve časa |
| ***Začetno stanje sistema***  | Avtomatska razporeditev časa je izklopljena, uporabniku je prikazan gumb funkcionalnosti |
| ***Vhod***                    | Pritisk na gumb opisane funkcionalnosti |
| ***Pričakovan rezultat***     | Funkcionalnost je vklopljena, izvede se avtomatska razporeditev časa | 


| **Izklop funkcionalnosti Avtomatska razporeditev časa** | #32 |
| :------------------------------- | :-------------------------------|
| **Povzetek** 	| Premium uporabnik lahko izklopi avtomatsko razporeditev časa. Sistem ne bo več samodejno organiziral urnika uporabnika.   |
| **Pogoji** 	| Vloga premium uporabnika, funkcionalnost Avtomatsko razporejanje časa mora biti vklopljena |
| **Posledice** | Funkcionalnost Avtomatsko raporejanje časa je izklopljena |
| **Posebnosti** 	| / |
| **Prioriteta** | Should have |
| **1. Osnovni tok** | 1. Premium uporabnik izbere funkcionalnost Avtomatsko razporejanje časa |
|                    | 2. Sistem funkcionalnost označi kot neaktivno |
| **Sprejemni test 31.1.1**  | |
| ***Funkcija, ki se testira*** | Izklop Avtomatske razporeditve časa |
| ***Začetno stanje sistema***  | Avtomatska razporeditev časa je vklopljena, uporabniku je prikazan gumb funkcionalnosti |
| ***Vhod***                    | Pritisk na gumb opisane funkcionalnosti |
| ***Pričakovan rezultat***     | Funkcionalnost je izklopljena, sistem ne izvaja več funkcionalnosti | 


## 6. Nefunkcionalne zahteve

* Zahteve izdelka
** Aplikacija je dosegljiva 99% časa na letni ravni
** Aplikacija je dostop na javnem spletnem naslovu
** Storitev deluje na brskalnikih Firefox in Chrome
** Storitev je kompatibilna z verzijami brskalnikov od 1.1.2018
** Storitev deluje v realnem času. Največji dovoljen odzivni čas je 5000ms.
** Sistem uporabniku ne dovoli dostopa do podatkov, za katere ni izrecno pooblaščen

* Zunanje zahteve
** Hranjenje in upravljanje z osebnimi podatki poteka po zakonu GDPR
** Grafični vmesnik mora biti prilagojen za naprave velikosti minimalno 350x650px

* Organizacijske zahteve
** Razvijalci uporabljajo sistem Git in Bitbucket za verzioniranje
** Razvojna ekipa deluje po principu SCRUM

## 7. Osnutki zaslonskih mask

![UML diagram](../img/Login-Register.png)

Slika pokriva povezavo med neprijavljenimi uporabniki in registracijo, ter med prijavljenimi, povezanimi in premium uporabniki ter prijavo in povezovanje s Facebook računom

![UML diagram](../img/Facebook_strani.png)

Slika pokriva povezavo povezanim in premium uporabnikom, ter pregledom in dodajanjem facebook strani

![UML diagram](../img/Dogodki_premium.png)

Slika pokriva povezavo med premium uporabnikom in ogledom ter urejanjem dogodkov, spreminjanjem kategorij dogodkov

![UML diagram](../img/Dogodki.png)

Slika pokriva povezavo med prijavljenim in povezanim uporabnikom ter ogledom dogodkov

![UML diagram](../img/Aktivnosti_premium.png)

Slika pokriva povezavo med premium uporabnikom ter ogledom, urejanjem, dodajanjem aktivnosti, prav tako z oceno porabe časa za aktivnost ter prioritetami aktivnosti

![UML diagram](../img/Aktivnosti.png)

Slika pokriva povezavo med prijavljenim in povezanim uporabnikom ter ogledom aktivnosti

![UML diagram](../img/Email.png)

Slika pokriva povezavo med premium uporabnikom in pošiljanjem e-mail sporočila

![UML diagram](../img/CLI.png)

Slika pokriva povezavo med administratorjem ter CLI

![UML diagram](../img/Urnik.png)

Slika pokriva povezavo med prijavljenim, povezanim in premium uporabnikom ter ogledom urnika in povezovanjem študijskega urnika

![UML diagram](../img/To-do_list.png)

Slika pokriva povezavo med neprijavljenim, prijavljenim, povezanim ter premium uporabnikom ter to-do seznamom

![UML diagram](../img/Restavracije.png)

Slika pokriva povezavo med prijavljenim, povezanim in premium uporabnikom ter pregledom ponudbe restavracij

![UML diagram](../img/Pregled.png)

Slika pokriva povezavo med prijavljenim, povezanim in premium uporabnikom ter pregledom študentskega življenja

![UML diagram](../img/Povezi_profil.png)

Slika pokriva povezavo med premium uporabnikom ter povezavo StraightAs profilov

![UML diagram](../img/Nadgradi.png)

Slika pokriva povezavo med povezanim uporabnikom ter nadgradnjo računa v premium

## 8. Prototipi vmesnikov

* FRI
** Naš sistem naredi poizvedbo z naslovom https://urnik.fri.uni-lj.si/timetable/fri-2018_2019-letni-1-13/allocations_ical?student=[vpisna številka], kjer [vpisna številka] nadomestimo z vpisno številko uporabnika.
**Odgovor nam vrne .ical datoteko z zapisi o obveznostih uporabnika na fakulteti. Vsak zapis vsebuje naslednje vrstice. BEGIN, SUMMARY, DTSTART, DTEND, DTSTAMP, UID, RRULE, DESCRIPTION, LOCATION, END

* Mastercard
** Sistem StraightAs pošlje zahtevo po izvedbi transakcije v obliki: { "BillPayAccountValidation": {"RppsId": "zahtevanaVrednost", "BillerId": "zahtevanaVrednost", "AccountNumber": "zahtevanaVrednost", "TransactionAmount": "zahtevanaVrednost", "CustomerIdentifier1": "opcijskaVrednost", "CustomerIdentifier2": "opcijskaVrednost", "CustomerIdentifier3": "opcijskaVrednost", "CustomerIdentifier4": "opcijskaVrednost", "ResponseString": ""}}
** Mastercard pošlje odgovor v obliki:
** Sistem StraightAs pošlje zahtevo po izvedbi transakcije v obliki: { "BillPayAccountValidation": {"RppsId": "zahtevanaVrednost", "BillerId": "zahtevanaVrednost", "AccountNumber": "zahtevanaVrednost", "TransactionAmount": "zahtevanaVrednost", "CustomerIdentifier1": "opcijskaVrednost", "CustomerIdentifier2": "opcijskaVrednost", "CustomerIdentifier3": "opcijskaVrednost", "CustomerIdentifier4": "opcijskaVrednost", "ResponseString": ""}}, kjer ResponseString vrne podatek o tem, ali je račun veljaven za transakcijo ali je prišlo do napake.

* Facebook login
** Sistem StraightAs sproži zahtevo po prijavi s povezavo https://connect.facebook.net/sl_SI/sdk.js#xfbml=1&version=v3.2&appId={app-id}, kjer {app-id} nadomestimo z id številko naše aplikacije. Za vse nadaljne korake prijave Facebook poskrbi avtomatsko.
** Po zaključku korakov prijave lahko dostopamo do response objekta, ki je oblike {status: 'connected', authResponse: {accessToken: '...', expiresIn:'...', signedRequest:'...', userID:'...'}}

* Facebook dogodki na straneh
** StraightAs sistem naredi poizvedbo do prihajajočih dogodkov https://graph.facebook.com/v2.1/{page-id}/events?time_filter=upcoming&access_token={app-id}|{secret-token}
** Facebook vrne seznam prihajajočih dogodkov v json obliki: {"data": [], "paging": {}}, kjer "data" vsebuje seznam dogodkov.

* Študentska prehrana
** StraightAs bo zahteval html stran s povezavo https://www.studentska-prehrana.si/sl/restaurant
** Dobljen html dokument v elementu "div" z id-jem "restaurant-list" vsebuje seznam vseh restavracij, ki ponujajo možnost koriščenja bonov. Dokument bomo ročno razčlenili in podatke prikazali uporabniku.
