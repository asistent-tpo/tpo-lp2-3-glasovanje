﻿# Načrt sistema

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Tine Fajfar, Lovro Habjan, Tadej Hudobivnik, Oton Pavlič|
| **Kraj in datum** | Ljubljana, 22.4.2019 |



## Povzetek

Načrt sistema za aplikacijo StraightAs sloni na arhitekturnem vzorcu model-pogled-krmilnik (MVC), kjer sistem organiziramo na model, ki skrbi za sistemske podatke in za operacije nad podatkovno bazo, pogled, ki opredeljuje predstavitev podatkov uporabniku in krmilnik, ki skrbi za uporabniško interakcijo, obdelavo zahtev, odgovorov in podatkov. Po arhitekturnem vzorcu se tudi deli razvoj z razdelitvijo dela med podatkovno bazo, zalednim sistemom in grafičnim vmesnikom. Sistem je strukturiran iz večih razredov. Mejni razredi komunicirajo z zunanjimi sistemi, kontrolni razredi nadzorujejo tok podatkov iz zunanjih sistemov, entitetni razredi pa predstavljajo hrbtenico aplikacije. Glavni gradniki aplikacije so razredi Uporabnik, Dogodek, Urnik in Aktivnost.  Načrt obnašanja je predstavljen z diagrami zaporedja.

## 1. Načrt arhitekture


![Logični in razvojni model](../img/modela.png)


## 2. Načrt strukture

### 2.1 Razredni diagram

![Razredni diagram](../img/ClassDiagram.png)

### 2.2 Opis razredov


#### Ime razreda: Uporabnik

* Entitetni razred predstavlja uporabnika, ki uporablja aplikacijo StraightAs.

##### Atributi

* id
	* Tip: String

* ime
	* Tip: String

* geslo
	* Tip: String

* povezaniProfili
	* Tip: seznam Uporabnikov
	* Pomen: seznam hrani vse uporabnike, s katerim si uporabnik deli profil

* statusUporabnika
	* Tip: Integer
	* Pomen: atribut hrani status uporabnika 
	* Zaloga vrednosti: 0 (neprijavljen), 1 (prijavljen), 2 (povezan), 3 (premium)

##### Nesamoumevne metode

* prikažiOpravila()
	* Parametri: void
	* Rezultat: seznam Opravil
	* Pomen: Prikaže opravila uporabnika.
	
* prijava()
	* Parametri: uporabniškoIme (tip String), geslo (tip String)
	* Rezultat: Boolean
	* Pomen: Prijavi uporabnika.

* registracija()
	* Parametri: uporabniškoIme (tip String), geslo (tip String)
	* Rezultat: Boolean
	* Pomen: Registrira novega uporabnika.

* nadgradiVPremium()
	* Parametri:
	* Rezultat: void
	* Pomen: Nadgradi uporabnikov račun v premium uporabnika.

* pridobiUrnik()
	* Parametri: id (String)
	* Rezultat: void
	* Pomen: Metoda poišče urnik uporabnika in ga prikaže.

* povežiUporabnika()
	* Parametri: idPrvegaUporabnika (tip String), idDrugegaUporabnika (tip String)
	* Rezultat: Boolean
	* Pomen: Poveže profila dveh uporabnikov s podanima id. Ob uspehu vrne "true", ob neuspehu "false".

* prikažiNapako()
	* Parametri: sporočilo (tip String)
	* Rezultat: void
	* Pomen: Zapiše napako v sistem, uporabniku javi napako.

* prikažiStatistiko()
  * Parametri:
  * Rezultat: void
  * Pomen: Metoda izračuna in prikaže statistiko uporabnika.
    
#### Ime razreda: Urnik

* Entitetni razred, predstavlja urnik uporabnika, hrani aktivnosti.

##### Atributi

* id
	* Tip: String
	
* aktivnosti
	* Tip: seznam Aktivnosti
	* Pomen: Seznam hrani vse uporabnikove aktivnosti, ki so na urniku.
	
* autoRazpČasa
	* Tip: Boolean
	* Pomen: Označuje, če je vklopljeno avtomatsko razporejanje časa

##### Nesamoumevne metode

* prikažiUrnik()
	* Parametri:
	* Rezultat: void
	* Pomen: Prikaže urnik na zaslon
	
* dodajAktivnost()
	* Parametri: naslov (tip String), opis (tip String), datum (tip Date), trajanje (tip Double), prioriteta (tip Integer)
	* Rezultat: void
	* Pomen: Ustvari novo aktivnost in jo doda na urnik.

* izbrišiAktivnost()
	* Parametri: id (tip String)
	* Rezultat: void
	* Pomen: Izbriše aktivnost z urnika.

* urediAktivnost()
	* Parametri: id (tip String), naslov (tip String), opis (tip String), datum (tip Date), trajanje (tip Double), prioriteta (tip Integer)
	* Rezultat: void
	* Pomen: Posodobi aktivnost na urniku z novimi podatki.
	
* vklopiAutoRazpČasa()
	* Parametri:
	* Rezultat: void
	* Pomen: Metoda nastavi atribut autoRazpČasa na "true".
	
* izklopiAutoRazpČasa()
	* Parametri:
	* Rezultat: void
	* Pomen: Metoda nastavi atribut autoRazpČasa na "false".

    
#### Ime razreda: Krmilnik za urnik

* Kontrolni razred, posrednik med aplikacijo in mejni razredom.

##### Atributi


##### Nesamoumevne metode

* vnesiŠtudentskiUrnik()
	* Parametri: vpisnaŠt (tip Integer)
	* Rezultat: seznam aktivnosti (aktivnost - tip Aktivnost)
	* Pomen: Kliče metodo v mejnem razredu, od nje dobi rezultat in ga vrne entitetnemu razredu.
    
#### Ime razreda: Zunanji vmesnik za urnik

* Mejni razred, ki komunicira z zunanjim sistemom FRI

##### Atributi


##### Nesamoumevne metode

* pridobiŠtudentskiUrniK()
	* Parametri: vpsinaŠt (tip Integer)
	* Rezultat: seznam aktivnosti (aktivnost - tip Aktivnost)
	* Pomen: metoda iz zunanjega sistema FRI pridobi podatke o študentskem urniku študenta s podano vpisno številko in pretvori pridobljene podatke v seznam aktivnosti.
	
    
#### Ime razreda: Aktivnost

* Entitetni razred, predstavlja osnovni element na urniku.

##### Atributi

* id
	* Tip: String

* naslov
	* Tip: String

* opis
	* Tip: String

* datum
	* Tip: Date
	* Pomen: začetni datum aktivnosti

* trajanje
	* Tip: Double
	* Pomen: trajanje aktivnosti

* prioriteta
	* Tip: Integer

* ocenaČasa
	* Tip: Integer
	*Pomen: ocena, koliko bo aktivnost trajala.


##### Nesamoumevne metode

* dodajOcenoČasaAktivnosti()
	* Parametri: idAktivnosti (tip String), ocena (tip Integer)
	* Rezultat: void
	* Pomen: Doda oziroma posodobi atribut ocenaČasa.

* odstraniOcenoČasaAktivnosti()
	* Parametri: idAktivnosti (tip String)
	* Rezultat: void
	* Pomen: Resetira atribut ocenaČasa.
    
    
#### Ime razreda: Dogodek

* Entitetni razred, predstavlja dogodek, za katerega uporabnik lahko potrdi udeležbo.

##### Atributi

* id
	* Tip: String

* ime
	* Tip: String

* datum
	* Tip: Date

* trajanje
	* Tip: Double

* opis
	* Tip: String

* kategorija
	* Tip: String
	* Pomen: Ime kategorije, v katero spada dogodek
	

##### Nesamoumevne metode

* ustvariDogodek()
	* Parametri: ime (tip String), opis (tip String), datum (tip Date), trajanje (tip Double), kategorija (tip String)
	* Rezultat: Boolean
	* Pomen: Ustvari nov dogodek

* izbrišiDogodek()
	* Parametri: id (tip String)
	* Rezultat : void
	* Pomen: Izbriše dogodek s podanim id

* urediDogodek()
	* Parametri: ime (tip String), opis (tip String), datum (tip Date), trajanje (tip Double), kategorija (tip String)
	* Rezultat: Boolean
	* Pomen: Uredi dogodek, vnese nove vrednosti.

* ustvariNovoKategorijo()
	* Parametri: imeKategorije (tip String)
	* Rezultat: void
	* Pomen: Ustvari novo kategorijo.

* prijavaNaKategorijoDogodkov()
	* Parametri: imeKategorije (tip String)
	* Rezultat: Boolean
	* Pomen: Prijava na kategorijo dogodkov.

* odjavaNaKategorijoDogodkov()
	* Parametri: imeKategorije (tip String)
	* Rezultat: Boolean
	* Pomen: Odjava od kategorije dogodkov.
	
* prikažiNapako()
	* Parametri: sporočilo (tip String)
	* Rezultat: void
	* Pomen: Zapiše napako v sistemu, javi napako uporabniku.
    
    
#### Ime razreda: Krmilnik za Facebook

* Koncept iz problemske domene, ki ga razred predstavlja.

##### Atributi

* appId
	* Tip: String
	* Pomen: Id aplikacije, uporabljena za komunikacijo s zunanjim sistemom Facebook.
	
* secretToken:
	* Tip: String
	* Pomen: Žeton za dostop do zunanjega sistema Facebook.

##### Nesamoumevne metode

* prijava()
	* Parametri:
	* Rezultat: Boolean
	* Pomen: Uporabniku omogoči prijavo na Facebook, kar ustvari povezavo med računoma.
	
* dodajStran()
	* Parametri:
	* Rezultat: Boolean
	* Pomen: Kliče mejni razred, ki doda Facebook stran na uporabnikov seznam strani, katerim uporabnik sledi.

* pridobiDogodke()
	* Parametri:
	* Rezultat: seznam Dogodkov
	* Pomen: Metoda s pomočjo mejnega razreda pridobi podatke o dogodkih, ki so objavljeni na zunanjemu sistemu Facebook.
	
* odstaniStran()
	* Parametri: id (tip String)
	* Rezultat: Boolean
	* Pomen: Kliče mejni razred, ki odstrani Facebook stran iz uporabnikovega seznama strani, katerim uporabnik sledi.
    
    
#### Ime razreda: Zunanji vmesnik za Facebook

* Mejni razred, komunicira z zunanjim sistemom Facebook

##### Atributi


##### Nesamoumevne metode

* prijavaNaFb()
	* Parametri: appId (tip String)
	* Rezultat: Boolean
	* Pomen: Uporabnik se prijavi na Facebook.

* dodajStranFb()
	* Parametri: appId (tip String), secretToken (tip String)
	* Rezultat: Boolean
	* Pomen: Doda Facebook stran na uporabnikov seznam strani, katerim uporabnik sledi.
	
* pridobiDogodkeFb()
	* Parametri: appId (tip String), secretToken (tip String)
	* Rezultat: Seznam dogodkov (dogodek - tip Dogodek)
	* Pomen: Metoda iz zunanjega sistema Facebook pridobi podatke o prihajajočih dogodkih in jih preuredi v seznam dogodkov.

* odstarniStranFb()
	* Parametri: appId (tip String), secretToken (tip String)
	* Rezultat: Boolean
	* Pomen: Odstrani Facebook stran iz uporabnikovega seznama strani, katerim uporabnik sledi.
    
#### Ime razreda: Opravilo

* Entitetni razred, predstavlja osnovni element za gradnjo seznama opravil.

##### Atributi

* id
    * Tip: String
* opis
	* Tip: String
	* Pomen: opis opravila

##### Nesamoumevne metode

* dodajOpravilo()
	* Parametri: opis (tip String)
	* Rezultat: void
	* Pomen: Ustvari novo opravilo in ga doda na seznam opravil

* izbiršiOpravilo()
	* Parametri: id (tip String)
	* Rezultat: void
	* Pomen: Izbriše opravilo s podanim id iz seznama opravil.
    
#### Ime razreda: Restavracija

* Entitetni razred, vsebuje podatke o restavraciji in o jedilniku, ki ga restavracija ponuja.

##### Atributi

* id
	* Tip: String
	
* ime
	* Tip: String
	* Pomen: ime restavracije

* doplačilo
	* Tip: Double
	* Pomen: Doplačilo v € pri uporabi študentskega bona.
	* Zaloga vrednosti: 0 - 4.37

* naslov
	* Tip: String
	* Pomen: Naslov restavracije


##### Nesamoumevne metode

* pridobiSeznamRestavracij()
	* Parametri:
	* Rezultat: Seznam restavracij
	* Pomen: Pridobi seznam restavracij, tako da zahtevo posreduje razredu Zunanji vmesnik za restavracijo.
	
* pridobiJedilnik()
	* Parametri: restavracija (tip Restavracija)
	* Rezultat: Seznam Stringov
	* Pomen: Pridobi podatke o jedilniku izbrane restavracije, tako da zahtevo posreduje razredu Zunanji vmesnik za restavracijo.
	
* javiNapako()
	* Parametri: sporočilo (tip String)
	* Rezultat: void
	* Pomen: Zapiše napako v sistemu, javi napako uporabniku.
        
        
#### Ime razreda: Zunanji vmesnik za restavracijo

* Zunanji razred, skrbi za komunikacijo z zunanjim sistemom, ki skrbi za informacije o študentski prehrani

##### Atributi


##### Nesamoumevne metode

* pridobiRestavracije()
	* Parametri:
	* Rezultat: seznam Restavracij
	* Pomen: Metoda pridobi podatke s klicom na zunanji sistem za študentsko prehrano, razčleni podatke in jih preoblikuje v seznam restavracij.

*  pridobiJedilnik():
    * Parametri: restavracija (tip Restavracija)
    * Rezultat: seznam Stringov
    * Pomen: Metoda pridobi podatke o jedilnik s klicom na zunanji sistem za študentsko prehrano, razčleni podatke in jih preoblikuje v seznam restavracij.
    
    
#### Ime razreda: Elektronsko sporočilo

* Entitetni razred predstavlja implementacijo pošiljanja elektronskih sporočil.

##### Atributi


##### Nesamoumevne metode

* pošljiSporočilo()
    * Parametri: naslov (tip String), telo (tip String)
    * Rezultat: void
    * Pomen: Metoda s pomočjo parametrov sestavi elektronsko sporočilo (pošiljatelj: noreply@straightAs.com) in ga pošlje prejemniku.
    
* javiNapako()
	* Parametri: sporočilo (tip String)
	* Rezultat: void
	* Pomen: Zapiše napako v sistemu, javi napako uporabniku.
    
    
#### Ime razreda: Krmilnik za MasterCard

* Kontrolni razred, predstavlja vmesni sloj med entitetnimi razredi in mejnim razredom, ki skrbi za komunikacijo s zunanjim sistemom.

##### Atributi


##### Nesamoumevne metode

* izvediTransakcijo()
	* Parametri: številkaRačuna (tip integer), znesek (tip integer)
	* Rezultat: Boolean
	* Pomen: Metoda prenese podatke razredu, ki skrbi za komunikacijo s zunanjim sistemom. Če je transakcija uspešna, metoda vrne vrednost "true", v nasportnem primeru "false".
    
#### Ime razreda: Zunanji vmesnik za MasterCard

* Mejni razred, skrbi za komunikacijo z zunanjim sistemom MasterCard

##### Atributi

* rppsId
	* Tip: integer
	* Pomen: id Rpps izvora
	
* billerId
	* Tip: integer
	* Pomen: id izdajatelja računa
	

##### Nesamoumevne metode

* izvediTransakcijo()
	* Parametri: štRačuna (tip integer), znesek (tip integer)
	* Rezultat: Boolean
	* Pomen: Metoda sproži klic za izvedbo transakcije v zunanjem sistemu. Iz odgovora zunanjega sistema vrne "true", če je transakcija uspela in "false", če ni.



## 3. Načrt obnašanja

Funkcionalnost 0.1.1

![Funkcionalnost 0.1.1](../img/0_1_1.png)

Funkcionalnost 0.3.1

![Funkcionalnost 0.3.1](../img/0_3_1.png)

Funkcionalnost 1.1

![Funkcionalnost 1.1](../img/1_1.png)

Funkcionalnost 1.3.1

![Funkcionalnost 1.3.1](../img/1_3_1.png)

Funkcionalnost 1.3.2, 1.3.3 in 1.3.4

![Funkcionalnost 1.3.234](../img/1_3_234.png)

Funkcionalnost 2.1.1

![Funkcionalnost 2.1.1](../img/2_1_1.png)

Funkcionalnost 2.3.1

![Funkcionalnost 2.3.1](../img/2_3_1.png)

Funkcionalnost 3.1.1

![Funkcionalnost 3.1.1](../img/3_1_1.png)

Funkcionalnost 3.3.1

![Funkcionalnost 3.3.1](../img/3_3_1.png)

Funkcionalnost 3.3.2

![Funkcionalnost 3.3.2](../img/3_3_2.png)

Funkcionalnost 4.1.1

![Funkcionalnost 4.1.1](../img/4_1_1.png)

Funkcionalnost 4.3.1

![Funkcionalnost 4.3.1](../img/4_3_1.png)

Funkcionalnost 4.3.3

![Funkcionalnost 4.3.3](../img/4_3_3.png)

Funkcionalnost 5.1.1

![Funkcionalnost 5.1.1](../img/5_1_1.png)

Funkcionalnost 5.3.1

![Funkcionalnost 5.3.1](../img/5_3_1.png)

Funkcionalnost 5.3.2

![Funkcionalnost 5.3.2](../img/5_3_2.png)

Funkcionalnost 6.1.1

![Funkcionalnost 6.1.1](../img/6_1_1.png)

Funkcionalnost 6.3.1

![Funkcionalnost 6.3.1](../img/6_3_1.png)

Funkcionalnosti 7.1.1, 7.2.1 in 7.3.1

![Funkcionalnosti 7.1.1, 7.2.1, 7.3.1](../img/7/7.png)

Funkcionalnosti 8.1.1, 8.2.1

![Funkcionalnosti 8.1.1, 8.2.1](../img/8/8.png)

Funkcionalnosti 9.1.1, 9.2.1

![Funkcionalnosti 9.1.1, 9.2.1](../img/9/9.png)

Funkcionalnost 10.1.1

![Funkcionalnost 10.1.1](../img/10/10-1.png)

Funkcionalnosti 10.2.1, 10.3.1

![Funkcionalnosti 10.2.1, 10.3.1](../img/10/10-2.png)

Funkcionalnost 11.1.1

![Funkcionalnost 11.1.1](../img/11/11-1.png)

Funkcionalnost 11.2.1

![Funkcionalnost 11.2.1](../img/11/11-2.png)

Funkcionalnost 11.3.1

![Funkcionalnost 11.3.1](../img/11/11-3.png)

Funkcionalnost 12.1.1

![Funkcionalnost 12.1.1](../img/12-20/12_0.png)

Funkcionalnost 12.2.1

![Funkcionalnost 12.2.1](../img/12-20/12_2.png)

Funkcionalnost 13.1.1

![Funkcionalnost 13.1.1](../img/12-20/13_0.png)

Funkcionalnost 13.3.1

![Funkcionalnost 13.3.1](../img/12-20/13_1.png)

Funkcionalnost 14.1.1

![Funkcionalnost 14.1.1](../img/12-20/14_0.png)

Funkcionalnost 15.1.1

![Funkcionalnost 15.1.1](../img/12-20/15_0.png)

Funkcionalnost 15.2.1

![Funkcionalnost 15.2.1](../img/12-20/15_1.png)

Funkcionalnost 16.1.1

![Funkcionalnost 16.1.1](../img/12-20/16_0.png)

Funkcionalnost 16.3.1

![Funkcionalnost 16.3.1](../img/12-20/16_1.png)

Funkcionalnost 17.1.1

![Funkcionalnost 17.1.1](../img/12-20/17_0.png)

Funkcionalnost 18.1.1

![Funkcionalnost 18.1.1](../img/12-20/18_0.png)

Funkcionalnost 19.1.1

![Funkcionalnost 19.1.1](../img/12-20/19_0.png)

Funkcionalnost 20.1.1

![Funkcionalnost 20.1.1](../img/12-20/20_0.png)

Funkcionalnost 20.3.1

![Funkcionalnost 20.3.1](../img/12-20/20_1.png)

Funkcionalnost 21.1.1, 21.3.1

![Funkcionalnost 21](../img/21.jpg)

Funkcionalnost 22.1.1, 22.3.1, 22.3.2

![Funkcionalnost 22](../img/22.jpg)

Funkcionalnost 23.1.1, 23.3.2

![Funkcionalnost 23](../img/23.jpg)

Funkcionalnost 24.1.1, 24.3.2

![Funkcionalnost 24](../img/24.jpg)

Funkcionalnost 25.1.1, 25.2.1, 25.3.1, 25.4.1

![Funkcionalnost 25](../img/25.jpg)

Funkcionalnost 26.1.1, 26.2.1

![Funkcionalnost 26](../img/26.jpg)

Funkcionalnost 27.1.1, 27.2.1

![Funkcionalnost 27](../img/27.jpg)

Funkcionalnost 28.1.1, 28.2.1, 28.3.1, 28.4.1

![Funkcionalnost 28](../img/28.jpg)

Funkcionalnost 29.1.1

![Funkcionalnost 29](../img/29.jpg)

Funkcionalnost 30.1.1

![Funkcionalnost 30](../img/30.jpg)

Funkcionalnost 31.1.1

![Funkcionalnost 31](../img/31.jpg)

Funkcionalnost 32.1.1

![Funkcionalnost 32](../img/32.jpg)




