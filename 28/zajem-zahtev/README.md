﻿# Dokument zahtev

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Maj Šavli, Jan Šturm, Urban Rupnik in Mitja Brezovnik |
| **Kraj in datum** | Ljubljana, 7.4.2019 |



## Povzetek projekta

V prvem delu dokumenta zajema zahtev so navedene uporabniške vloge, ki bodo nastopale v okviru aplikacije, temu pa sledi slovar terminoloških izrazov, kjer je vsak izmed izrazov definiran. Nato sledi diagram primerov uporabe, kjer so iz visokonivojskega pogleda vidne glavne funkcionalnosti, ki jih bo posamezna vloga lahko uporabljala. Diagram vsebuje tudi vse zunanje sisteme, katere bo aplikacija uporabljala.  Naslednji del dokumenta sestavlja podroben opis vsake izmed funkcionalnosti, upodobljene na diagramu primera uporabe. V okviru opisa so za vsako funkcionalnost navedeni tudi sprejemni testi, glavni in alternativni tokovi ter prioriteta funkcionalnosti. Opisu funkcionalnih zahtev sledi opis nefunkcionalnih zahtev, ki morajo biti izpolnjene v okviru projekta in končnega izdelka. Zadnji del dokumenta pa je sestavljen iz definicije vmesnika do zunanjega sistema in videoposnetka, ki prikazuje okviren izgled končne aplikacije.

## 1. Uvod

Naša aplikacija bo pomagala študentom pri organizaciji njihovih opravkov/obveznosti/aktivnosti. Na glavni strani bo študent imel pregled nad obveznostmi, ki jih bo lahko uredil po datumu poteka ali po prioriteti. Seveda ima možnost tudi ustvariti novo in urejanje obstoječe obveznosti. Poleg obveznosti bo za boljšo organiziranost viden tudi manjši koledar. Obveznosti bo lahko potrjeval ali brisal. Študent ima možnost tudi ustvarjanja in urejanja urnikov. Če ima študijski informacijski sistem fakultete, na katero je študent vpisan, dostopen REST-API, bo študent v razdelku Pregled ocen lahko videl vse njegove ocene izpitov, kolokvijev in sprotnih obveznosti, ki so bile pridobljene iz študijskega informacijskega sistema. Če bo rabil kakršnokoli pomoč, ima študent možnost pošiljanja sporočil podpori. Podpora ima pregled nad vsemi študenti, s katerimi je že komunicirala, s klikom na kateregakoli izmed njih pa se mu odpre zgodovina sporočil in okno za ustvarjanje novega sporočila. Naša aplikacijo uporablja tudi skrbnik, ki bo imel možnost pregleda nad in brisanja študentov. Nekatere od teh funkcionalnosti niso popolnoma potrebne za delovanje aplikacije, izboljšajo pa uporabniško izkušnjo oz. učinkovitost delovanja aplikacije v primeru velikega števila uporabnikov.

## 2. Uporabniške vloge

Naša aplikacijo uporabljajo uporabniki, ki se delijo na naslednje vloge:

* **neregistriran uporabnik**, ki vidi le začetno stran in se lahko registrira
* **študent**, ki lahko dodaja in ureja urnike in obveznosti, ima pregled nad njegovimi ocenami ter, če potrebuje pomoč, možnost kontaktiranja **podpore**
* **podpora**, ki lahko odgovarja študentom na sporočila in jih pregleduje
* **skrbnik**, ki ima pregled nad študenti in jih lahko briše, če je to potrebno


## 3. Slovar pojmov

**Obveznost**
Aktivnost oz. opravilo, ki si študent želi opraviti.

**Urnik**
Po urah vnaprej določen potek dneva.

**Ocena**	
Celo število z intervala [5, 10], ki odraža stopnjo uspešnosti opravljanja izpita ali sprotnih obveznosti.

**Sporočilo**
Besedilo z informativno vrednostjo, ki ga pošiljatelj pošlje prejemniku preko sistema. 

**Podatkovna baza**
Sklop zbirke medsebojno povezanih dokumentov, namenjeno ažuriranju, razvrščanju, iskanju in urejanju podatkov v zbirki.

**Fakulteta**
Izobraževalna visokošolska ustanova, navadno kot enota univerze, namenjena izobraževanju mladih.

**Predmet**
Učno področje, ki je vsebinsko določeno s predmetnikom in učnim načrtom s strani fakultete.

**Registracija**
Aktivnost preko katere postane neregistrirani uporabnik, član sistema. Za registracijo je potrebno navesti e-naslov, uporabniško ime, geslo in potrditveno geslo.

**Prijava**
Aktivnost preko katere registriran uporabnik vstopi v sistem. Študent se prijavi s svojim uporabniškim imenom in geslom.

**Uporabniško ime**
Študentski elektronski naslov študenta, katerega uporabi za prijavo v sistem.

**Geslo**
Geslo, ki ga študent uporablja za prijavo v njegov študijski informacijski sistem in ga bo uporabljal za prijavo v aplikacijo.

**Uporabnik**
Oseba ki je v aplikacijo prijavljena in jo aktivno uporablja.

**Študent**
Oseba, ki je vpisana v določen letnik visokošolskega oziroma univerzitetnega šolanja. Vsak študent je vpisan na določeno fakulteto.

**Elektronski naslov**
Unikaten naslov za elektronsko pošto ki pripada vsakemu študentu ki obiskuje fakulteto, v obliki abXXXX@student.uni-lj.si, kjer sta ab prvi črki imena in priimka ter XXXX unikatno število.

**Logotip**
Ikona oz. slika, ki odraža unikaten simbol aplikacije.

**Spustni meni**
Gradnik uporabniškega vmesnika, ki se ob kliku razširi in vsebuje razne gumbe.

**REST API**
Aplikacijski vmesnik, ki uporablja HTTP zahteve za pridobivanje, ažuriranje, nalaganje in brisanje podatkov.

**Študijski informacijski sistem**
Sistem določene fakultete, namenjen študentom, kjer je njihov elektronski indeks in  funkcionalnost prijavljanja na izpite

**Digitalno potrdilo**
Digitalni dokument, ki zagotvalja pristnost uporabnika

## 4. Diagram primerov uporabe

![Diagram primerov uporabe](../img/uml-diagram.png)

## 5. Funkcionalne zahteve

### Pregled nad obveznostmi

#### Študent lahko pregleduje svoje prihajajoče obveznosti, urejene po prioriteti in/ali roku poteka. 

#### Osnovni tok

1.  Študenta aplikacija po prijavi usmeri na osnovno stran, kjer ima pregled nad vsemi svojimi obveznostmi.

#### Alternativni tok(ovi)

**Alternativni tok 1**

1. Študent se nahaja na delu aplikacije, ki ni pregled obveznosti.
2. Študent klikne na logotip aplikacije v levem zgornjem kotu zaslona.
3. Prikaže se domača stran, kjer ima študent pregled nad vsemi svojimi obveznostmi.

**Izjemni tok 1**

* Pri prikazovanju seznama vseh obveznosti se zgodi napaka pri branju podatkov iz podatkovne baze, nakar se študentu na ekran izpiše ustrezno poročilo o napaki.

#### Pogoji

* Pri funkcionalnosti **Pregled nad obveznostmi** mora biti uporabnik v sistem prijavljen kot študent.  Če ni prijavljen, ali pa je prijavljen z drugo vlogo, mu ta funkcionalnost ni na voljo.

#### Posledice

* Pri funkcionalnosti **Pregled nad obveznostmi**  ni poleg prikaza vseh obveznosti študenta nobenih posledic.

#### Posebnosti

* Funkcionalnost **Pregled nad obveznostmi**  ne zahteva nobenih posebnosti glede dodatne opreme ali posebnih postopkov.

#### Prioritete identificiranih funkcionalnosti

* Funkcionalnosti **Pregled nad obveznostmi**  je bila določena prioriteta **Must Have**, ker je to temeljna funkcionalnost aplikacije.

#### Sprejemni testi

| Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
| :------------- | :------------ | :----------- | :-------------------------- | :------------------------- |
|Prikaz obveznosti|Seznam obveznosti je prazen|Klik na gumb prijava|Prikaz obvestila o praznemu seznamu obveznosti|
|Preusmerjanje na seznam obveznosti|Študent je na pogledu, ki ni pregled vseh obveznosti|Klik na logotip v levem zgornjem delu zaslona|Prikaz seznama obveznosti|
|Obvestilo o napaki|Napaka na podatkovni bazi|Klik na logotip/gumb prijava|Prikaz obvestila o napaki v podatkovni bazi|


### Vnos nove obveznosti


#### Študent lahko vnese novo obveznost v seznam njegovih obveznosti. 

#### Osnovni tok

1. Študent klikne na gumb _Ustvari novo obveznost_
2. Sistem prikaže obrazec za vnos podatkov o obveznosti
3. Študent vpiše podatke in potrdi obrazec, ki se zatem zapre
4. Študentu se izpiše obvestilo o uspešnem kreiranju obveznosti, katera se doda na seznam vseh obveznosti

#### Alternativni tok(ovi)

**Alternativni tok 1**

1. Študent iz spustnega menija izbere opcijo _Ustvari novo obveznost_
2. Sistem prikaže obrazec za vnos podatkov o obveznosti
3. Študent vpiše podatke in potrdi obrazec, ki se zatem zapre
4. Študentu se izpiše obvestilo o uspešnem kreiranju obveznosti, katera se doda na seznam vseh obveznosti

**Izjemni tok 1**

* Pri potrjevanju obrazca študent ne vpiše vseh potrebnih oz. veljavnih podatkov, zato se obrazec ne shrani, sistem pa mu izpiše ustrezno sporočilo

#### Pogoji

* Pri funkcionalnosti **Vnos nove obveznosti** mora biti uporabnik v sistem prijavljen kot študent.  Če ni prijavljen, ali pa je prijavljen z drugo vlogo, mu ta funkcionalnost ni na voljo.
* Pri osnovnem toku se mora študent nahajati na osnovni strani, tj. pregled obveznosti

#### Posledice

* Po potrditvi obrazca se obveznost shrani v bazo in je takoj vidna na seznamu obveznosti študenta

#### Posebnosti

* Funkcionalnost **Vnos nove obveznosti**  ne zahteva nobenih posebnosti glede dodatne opreme ali posebnih postopkov.

#### Prioritete identificiranih funkcionalnosti

* Funkcionalnosti **Vnos nove obveznosti**  je bila določena prioriteta **Must Have**, ker je to temeljna funkcionalnost aplikacije.

#### Sprejemni testi

| Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
| :------------- | :------------ | :----------- | :-------------------------- | :------------------------- |
|Prikaz obrazca za kreiranje obveznosti|Pogled vse obveznosti|Klik na gumb _Ustvari novo obveznosti_|Prikaže se obrazec za kreiranje nove obveznosti|
|Shranjevanje ustvarjene obveznosti|Prazen obrazec za kreiranje obveznosti|Izpolnjen obrazec in klik na gumb _Shrani_|Shranjena obveznost v podatkovno bazo in posodobljen seznam obveznosti (obveznost vidna na seznamu vseh obveznosti)|
|Obvestilo o neveljavnem obrazcu|Prazen obrazec za kreiranje obveznosti|Neveljavno izpolnjen obrazec in klik na gumb _Shrani_|Študentu se izpiše ustrezno sporočilo o napaki na obrazcu|


### Brisanje obstoječe obveznosti

#### Študent lahko izbriše obstoječo obveznost iz seznama vseh njegovih obveznosti

#### Osnovni tok

1. Študent klikne na gumb _Odstrani obveznost_, ki se nahaja poleg izbrane obveznosti
2. Sistem prikaže potrditveni obrazec
3. Študent klikne na gumb _Odstrani_
4. Študentu se izpiše obvestilo o uspešnem brisanju obveznosti, katera se odstrani s seznama vseh obveznosti

#### Pogoji

* Pri funkcionalnosti **Brisanje obsteječe obveznosti** mora biti uporabnik v sistem prijavljen kot študent.  Če ni prijavljen, ali pa je prijavljen z drugo vlogo, mu ta funkcionalnost ni na voljo.

#### Posledice

* Po brisanju obveznosti se ta obveznost izbriše iz baze in ni več vidna na seznamu obveznosti študenta

#### Posebnosti

* Funkcionalnost **Brisanje obstoječe obveznosti**  ne zahteva nobenih posebnosti glede dodatne opreme ali posebnih postopkov.

#### Prioritete identificiranih funkcionalnosti

* Funkcionalnosti **Brisanje obstoječe obveznosti**  je bila določena prioriteta **Must Have**, ker je to temeljna funkcionalnost aplikacije.

#### Sprejemni testi

| Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
| :------------- | :------------ | :----------- | :-------------------------- | :------------------------- |
|Brisanje obveznosti|Neprazen seznam obveznosti|Klik na gumb _Odstrani obveznost_ poleg želene obveznosti|Obveznost se izbriše iz podatkovne baze in ni več vidna na seznamu vseh obveznosti študenta|



### Potrjevanje obstoječe obveznosti

#### Študent lahko potrdi obstoječo obveznost iz seznama vseh njegovih obveznosti

#### Osnovni tok

1. Študent lahko klikne na gumb _Potrdi obveznost_, ki se nahaja poleg izbrane obveznosti
2. Študentu se izpiše obvestilo o uspešnem potrjevanju obveznosti, katera se odstrani s seznama vseh obveznosti in doda na seznam opravljenih obveznosti

#### Pogoji

* Pri funkcionalnosti **Potrjevanje obsteječe obveznosti** mora biti uporabnik v sistem prijavljen kot študent.  Če ni prijavljen, ali pa je prijavljen z drugo vlogo, mu ta funkcionalnost ni na voljo.

#### Posledice

* Po potrjevanju obveznosti ta ni več vidna na seznamu obveznosti študenta. Vidna pa je na seznamu opravljenih obveznosti.

#### Posebnosti

* Funkcionalnost **Potrjevanje obstoječe obveznosti**  ne zahteva nobenih posebnosti glede dodatne opreme ali posebnih postopkov.

#### Prioritete identificiranih funkcionalnosti

* Funkcionalnosti **Potrjevanje obstoječe obveznosti**  je bila določena prioriteta **Must Have**, ker je to temeljna funkcionalnost aplikacije.

#### Sprejemni testi

| Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
| :------------- | :------------ | :----------- | :-------------------------- | :------------------------- |
|Potrditev obveznosti|Neprazen seznam obveznosti|Klik na gumb _Potrdi obveznost_ poleg želene obveznosti|Obveznost se premakne na seznam opravljenih obveznosti in ni več vidna na seznamu vseh obveznosti študenta |



### Prijava

#### Študent, Skrbnik in Podpora se lahko prijavijo v sistem z uporabniškim imenom in geslom.

#### Osnovni tok - Študent

1. Študent na začetni strani klikne na gumb _Prijava_
2. Pojavi se obrazec za uporabniško ime in geslo
3. Študent vpiše uporabniško ime in geslo
4. Študent je vpisan v sistem in je preusmerjen na seznam vseh njegovih obveznosti 

#### Osnovni tok - Skrbnika

1. Skrbnik na začetni strani klikne na gumb _Prijava_
2. Pojavi se obrazec za uporabniško ime in geslo
3. Skrbnik vpiše uporabniško ime in geslo
4. Skrbnik je vpisan v sistem in je preusmerjen na njegovo nadzorno ploščo, kjer ima na voljo funkcionalnost iskanja in brisanja uporabnikov 

#### Osnovni tok - Podpora

1. Podpora na začetni strani klikne na gumb _Prijava_
2. Pojavi se obrazec za uporabniško ime in geslo
3. Podpora vpiše uporabniško ime in geslo
4. Podpora je vpisan v sistem in je preusmerjen na pregled njegovih sporočil

**Izjemni tok(ovi)**

* Pri potrjevanju obrazca študent, podpora oz. skrbnik ne vpišejo vseh potrebnih podatkov oz. je vpisano uporabniško ime ali geslo neveljavno, zato se obrazec ne obdela, sistem pa jim izpiše ustrezno sporočilo o napaki

#### Pogoji

* Pri funkcionalnosti **Prijava** mora biti študent registriran (podpora in skrbik sta v podatkovno bazo vnešena ročno, brez registracije).  Če ni registriran, se ne more prijaviti.

#### Posledice

* Študent, podpora oz. skrbnik je prijavljen v sistem.

#### Posebnosti

* Funkcionalnost **Prijava**  ne zahteva nobenih posebnosti glede dodatne opreme ali posebnih postopkov.

#### Prioritete identificiranih funkcionalnosti

* Funkcionalnosti **Prijava**  je bila določena prioriteta **Must Have**, ker je to temeljna funkcionalnost aplikacije.

#### Sprejemni testi

**Za vloge: Študent, Skrbnik in Podpora**

| Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
| :------------- | :------------ | :----------- | :-------------------------- | :------------------------- |
|Prijavni obrazec|Študent neprijavljen|Klik na gumb _Prijava_|Prikaz obrazca za prijavo|

**Za vlogo : Študent**

| Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
| :------------- | :------------ | :----------- | :-------------------------- | :------------------------- |
|Preusmeritev ob uspešni prijavi|Študent neprijavljen, prazen obrazec za prijavo|Izpolnjen obrazec za prijavo, klik na gumb _Prijava_|Študent prijavljen v sistem in prikaz njegovih obveznosti|

**Za vlogo : Skrbnik**

| Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
| :------------- | :------------ | :----------- | :-------------------------- | :------------------------- |
|Preusmeritev ob uspešni prijavi|Skrbnik neprijavljen, prazen obrazec za prijavo|Izpolnjen obrazec za prijavo, klik na gumb _Prijava_|Skrbnik prijavljen v sistem in prikaz njegove nadzorne plošče|

**Za vlogo : Podpora**

| Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
| :------------- | :------------ | :----------- | :-------------------------- | :------------------------- |
|Preusmeritev ob uspešni prijavi|Podpora neprijavljen, prazen obrazec za prijavo|Izpolnjen obrazec za prijavo, klik na gumb _Prijava_|Podpora prijavljen v sistem in pregled prejetih sporočil|



### Registracija

#### Neregistriran uporabnik se lahko registrira v sistem.

#### Osnovni tok - Študent

1. Neregistriran uporabnik na začetni strani klikne na gumb _Registracija_
2. Pojavi se obrazec, kamor mora neregistriran uporabnik vnesti vpisno številko, študentski elektronski naslov in geslo, ki sta enaka študentskemu naslovu in geslu, ki ga neregistriran uporabnik (kot študent) uporablja za dostop do svojega študijskega informacijskega sistema (za nadaljnji dostop do elektronskega indeksa) 
3. Neregistriran uporabnik vpiše vse potrebne podatke
4. Neregistriran uporabnik potrdi obrazec
5. Sistem obdela poslan obrazec in neregistriranemu uporabniku prikaže obvestilo o uspešnosti registracije

**Izjemni tok(ovi)**

* Pri izpolnjevanju obrazca neregistriran uporabnik ne vpiše vseh potrebnih podatkov,  sistem izpiše ustrezno sporočilo o napaki
* Vpisani podatki neveljavni, zato se obrazec ne obdela,  sistem izpiše ustrezno sporočilo o napaki
* Uporabnik je že v bazi, zato sistem izpiše ustrezno sporočilo o napaki.

#### Pogoji

* Študijski sistem, katerega uporablja neregistriran uporabnik (kot študent), mora imeti dostopen REST API, za dostop do elektronskega indeksa

#### Posledice

* Študent je registriran v sistem (dodan v bazo uporabnikov), se lahko prijavi in uporablja aplikacijo

#### Posebnosti

* Za funkcionalnost **Registracija** se moramo dogovoriti, kateri študijski informacijski sistemi imajo javno dostopen REST API, da to upoštevamo pri obdelavi obrazcev registracije. Študenti, katerih informacijski študijski sistemi nimajo javno dostopnega  API-ja, bodo lahko uporabljali vse funkcionalnosti, razen pregleda ocen, o tem pa bodo na spletnem vmesniku tudi ustrezno obveščeni.

#### Prioritete identificiranih funkcionalnosti

* Funkcionalnosti **Registracija**  je bila določena prioriteta **Must Have**, ker je to temeljna funkcionalnost aplikacije.

#### Sprejemni testi

| Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
| :------------- | :------------ | :----------- | :-------------------------- | :------------------------- |
|Obrazec za registracijo|Študent se nahaja na začetni strani|Klik na gumb _Registracija_|Prikaz obrazca za registracijo|
|Obvestilo o napačno izpolnjenem obrazcu za registracijo|Prazen obrazec za registracijo|Neveljavno izpolnjen obrazec za registracijo in  klik na gumb _Registracija_|Študentu se izpiše ustrezno sporočilo o napaki na obrazcu|
|Obvestilo o manjkajočih podatkih v obrazcu za registracijo|Prazen obrazec za registracijo|Delno izpolnjen obrazec za registracijo in  klik na gumb _Registracija_|Študentu se izpiše ustrezno sporočilo o napaki na obrazcu|
|Obvestilo, da uporabnik že obstaja v bazi|Prazen obrazec za registracijo|Izpolnjen obrazec za registracijo in  klik na gumb _Registracija_|Študentu se izpiše ustrezno sporočilo o napaki na obrazcu|


### Pregled sporočil

#### Študent lahko pregleduje sporočila, ki jih je poslal/prejel od podpore. 

#### Osnovni tok- Študent

1.  Študent klikne na gumb _Pregled sporočil_.
2.  Sistem prikaže seznam sporočil, ki sta si jih izmenjala Študent in Podpora

#### Osnovni tok- Podpora

1. Podpora se prijavi v sistem
2.  Sistem prikaže seznam Študentov, s katerimi je Podpora do sedaj že komunicirala.
3.  Podpora izbere enega izmed Študentov, ki so na voljo v levem stranskem menu-ju.
4.  Sistem desno od  stranskega menu-ja  se prikaže seznam vseh sporočil za izbranega Študenta. Vsako sporočilo vsebuje datum in čas nastanka. 

**Izjemni tok 1**

* Pri prikazovanju seznama vseh obveznosti se zgodi napaka pri branju podatkov iz podatkovne baze, nakar se študentu na ekran izpiše ustrezno poročilo o napaki.

#### Pogoji

* Pri funkcionalnosti **Pregled sporočil** mora biti uporabnik v sistem prijavljen kot Študent/Podpora.  Če ni prijavljen, ali pa je prijavljen z drugo vlogo, mu ta funkcionalnost ni na voljo.


#### Posledice

* Pri funkcionalnosti **Pregled sporočil**  ni poleg prikaza vseh sporočil študenta/podpore nobenih posledic.

#### Posebnosti

* Študent ima na voljo le en kontakt (Podpora), zato so vsa sporočila zbrana na enem seznamu. 
* Podpora lahko komunicira z več kontakti (Študenti), vendar samo s tistimi, ki so kadarkoli kontaktirali Podporo. Sporočila so grupirana glede na Študenta. Podpora mora za pregled specifičnega pogovora prvo izbrati določenega študenta v levem stranskem menu-ju.

#### Prioritete identificiranih funkcionalnosti

* Funkcionalnosti **Pregled sporočil**  je bila določena prioriteta **Should Have**. Funkcionalnost ni nujna za delovanje aplikacije. Če jo ne bi bilo, pa bi mogoče okrnila uporabniško izkušnjo.

#### Sprejemni testi- Študent

| Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
| :------------- | :------------ | :----------- | :-------------------------- | :------------------------- |
|Izpis sporočil za kontakt podpora| Seznam obveznosti |Klik na gumb _Pregled sporočil_| Seznam sporočil za kontakt podpora|

#### Sprejemni testi- Podpora

| Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
| :------------- | :------------ | :----------- | :-------------------------- | :------------------------- |
|Izpis seznama kontaktov (študentov)|Podpora ni prijavljen v sistem|Podpora se prijavi v sistem|Prikaz seznama vseh kontaktov, s katerimi je podpora komunicirala|
|Izpis sporočil za izbrani kontakt| Podpora je na pogledu _Pregled sporočil_, vidi seznam kontaktov |Izbira študenta iz seznama kontaktov v levem stranskem menu-ju| Sistem prikaže seznam sporočil za izbrani kontakt na desni strani|


### Ustvarjanje novega sporočila

#### Študent in podpora lahko pošiljata sporočila drug drugemu.

#### Osnovni tok- Študent

1. Študent klikne na gumb _Novo sporočilo_
2. Študent vnese besedilo v vnosno polje za pošiljanje sporočil.
3. Študent sporočilo pošlje s klikom na gumb _Pošlji sporočilo_

#### Osnovni tok- Podpora

1. Podpora izbere kontakt (Študenta) v levem stranskem menu-ju
2. Podpora vnese poljubno sporočilo v vnosno polje za pošiljanje sporočil.
3. Podpora sporočilo pošlje s klikom na gumb _Pošlji sporočilo_

#### Izjemni tok(ovi)

* Študent oz. podpora pustita pri pošiljanju sporočila prazno vnosno polje

#### Pogoji

* Za uporabo funkcionalnosti **Ustvarjanje novega sporočila** je pogoj, da  je oseba prijavljena z vlogo Študent/Podpora.  Podpora lahko izbere kontakt (Študenta) samo v primeru, da je Študent Podporo že predhodno kontaktiral, sicer Študenta ni na seznamu kontaktov.

#### Posledice

* Ko je sporočilo poslano, se zatem shrani v bazo in je takoj vidno na seznamu sporočil Študenta/Podpore.

#### Posebnosti

* Funkcionalnost **Ustvarjanje novega sporočila**  ne zahteva nobenih posebnosti glede dodatne opreme ali posebnih postopkov.

#### Prioritete identificiranih funkcionalnosti

* Funkcionalnosti **Ustvarjanje novega sporočila**  je bila določena prioriteta **Should Have**. Funkcionalnost ni nujna za delovanje aplikacije. Če jo ne bi bilo, pa bi mogoče okrnila uporabniško izkušnjo.

#### Sprejemni testi

| Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
| :------------- | :------------ | :----------- | :-------------------------- | :------------------------- |
|Preverjanje vnosa| Obrazec s praznim vnosnim poljem za besedilo | Klik na gumb _Pošlji sporočilo_| Študentu oz. podpori se izpiše obvestilo o neveljavnem vnosem polju|

#### Sprejemni testi- Študent

| Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
| :------------- | :------------ | :----------- | :-------------------------- | :------------------------- |
|Pošiljanje sporočila| Seznam sporočil Študenta| Vnos besedila v tekstovno polje. Po vnesenem besedilu se izvede klik na gumb _Pošlji sporočilo_| Sporočilo se doda na obstoječi seznam sporočil v klepetu in pošlje Podpori|


#### Sprejemni testi- Podpora

| Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
| :------------- | :------------ | :----------- | :-------------------------- | :------------------------- |
|Pošiljanje sporočila| Seznam kontaktov Podpore|Izbira Študenta iz seznama kontaktov in vnos besedila v tekstovno polje. Po vnesenem besedilu se izvede klik na gumb _Pošlji sporočilo_| Sporočilo se doda na obstoječi seznam sporočil in pošlje Študentu|

-----
### Pregled ocen

#### Študent lahko pregleduje ocene, ki jih aplikacija avtomatsko pridobi iz študentskega informacijskega sistema. 

#### Osnovni tok

1.  Študent klikne na gumb _Pregled ocen_.
2.  Sistem prikaže seznam ocen, ki  jih  je študent dobil v tekočem študijskem letu v obliki tabele

**Izjemni tok 1**

-   Študentu je onemogočen gumb _Pregled ocen_ hkrati pa mu sistem prikaže ustrezno obvestilo , če naša aplikacija ni povezana na študijski informacijski sistem te fakultete.

#### Pogoji

* Uporabnik mora biti v sistem prijavljen kot Študent, hkrati pa mora biti študijski informacijski sistem podprt s strani naše aplikacije, sicer mu ta funkcionalnost ni na voljo.

#### Posledice

* Pri funkcionalnosti **Pregled ocen**  ni poleg prikaza vseh ocen študent nobenih posledic.

#### Posebnosti

* Funkcionalnost **Pregled ocen**  ne zahteva nobenih posebnosti glede dodatne opreme ali posebnih postopkov.

#### Prioritete identificiranih funkcionalnosti

* Funkcionalnosti **Pregled ocen**  je bila določena prioriteta **Should Have**. Funkcionalnost ni nujna za delovanje aplikacije. Če jo ne bi bilo, pa bi mogoče okrnila uporabniško izkušnjo.

#### Sprejemni testi

| Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
| :------------- | :------------ | :----------- | :-------------------------- | :------------------------- |
|Izpis ocen Študenta| Študent v kateremkoli pogledu aplikacije | Klik na gumb _Pregled ocen_| Sistem prikaže vse ocene študenta, v obliki tabele, po letih in predmetih|
|Izpis ocen onemogočen| Študent v kateremkoli pogledu aplikacije, aplikacija ni povezaana z informacijskim sistemom njegove fakultete| Klik na gumb _Pregled ocen_| Prkaz obvestila o onemogočenem dostopu do pregleda ocen, ker študijski informacijski sistem fakultete ni podprt. 

### Pregled potrjenih obveznosti

#### Študent lahko pregleda obveznosti, ki jih je že opravil oz. potrdil.

#### Osnovni tok - Študent

1. Študent klikne na razdelek _Opravljene obveznosti_
2. Sistem mu prikaže seznam vseh opravljenih obveznosti, urejenih po datumu zapadlosti

#### Pogoji

* Uporabnik mora v sistem biti prijavljen kot Študent. Če ni, mu ta funkcionalnost ni na voljo.

#### Posledice

* Pri funkcionalnosti **Pregled potrjenih obveznosti**  ni poleg prikaza okna z vsemi opravljenimi obveznostmi študenta nobenih večjih posledic.

#### Posebnosti

* Funkcionalnost **Pregled potrjenih obveznosti**  ne zahteva nobenih posebnosti glede dodatne opreme ali posebnih postopkov.

#### Prioritete identificiranih funkcionalnosti

* Funkcionalnosti **Pregled potrjenih obveznosti**  je bila določena prioriteta **Wont Have This Time**. Funkionalnost ni ključna za uporabo aplikacije, zdi pa se nam dobra kot ideja za nadaljnjo izdajo.

#### Sprejemni testi

| Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
| :------------- | :------------ | :----------- | :-------------------------- | :------------------------- |
|Prikaz seznama opravljenih obveznosti|Uporabnik prijavljen kot študent, na kateremkoli pogledu aplikacije|Klik na gumb _Pregled potrjenih obveznosti_|Sistem študentu prikaže seznam opravljenih obveznosti, urejenih po datumu zapadlosti v novem pogledu|

### Pregled nad urniki

#### Študent ima lahko pregled nad svojimi urniki.

#### Osnovni tok - Študent

1. Študent klikne na razdelek _Pregled urnikov_
2. Sistem mu v novem pogledu prikaže njegove urnike v obliki tabel

#### Pogoji

* Uporabnik mora v sistem biti prijavljen kot Študent. Če ni, mu ta funkcionalnost ni na voljo.

#### Posledice

* Pri funkcionalnosti **Pregled urnikov**  ni poleg prikaza urnika nobenih večjih posledic.

#### Posebnosti

* Funkcionalnost **Pregled urnikov**  ne zahteva nobenih posebnosti glede dodatne opreme ali posebnih postopkov.

#### Prioritete identificiranih funkcionalnosti

* Funkcionalnosti **Pregled urnikov**  je bila določena prioriteta **Must Have**, ker je temeljna funkcionalost aplikacije.

#### Sprejemni testi

| Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
| :------------- | :------------ | :----------- | :-------------------------- | :------------------------- |
|Prikaz urnika|Uporabnik prijavljen kot študent, na kateremkoli pogledu aplikacije, urnik že ima ustvarjen|Klik na gumb _Pregled urnikov_|Sistem študentu v novem pogledu prikaže njegove urnike v obliki tabel|
|Prikaz urnika|Uporabnik prijavljen kot študent, na kateremkoli pogledu aplikacije, urnika še nima ustvarjenega|Klik na gumb _Pregled urnikov_|Sistem študentu v novem pogledu prikaže obvestilo, da urnika še nima ustvarjenega|

### Urejanje urnika

#### Študent  lahko ureja svoj obstoječi urnik.

#### Osnovni tok - Študent

1. Študent klikne na razdelek _Pregled urnikov_
2. Študent klikne na gumb poleg urnika _Uredi urnik_
3. Sistem mu prikaže urnik v pogledu z orodji, s katerimi ureja urnik
4. Študent izbere poljubno orodje in premika obveznosti, jim spremeni čas, jih izbriše, kreira, itd.
5. Ko študent konča z urejanjem urnika klikne na gumb _Shrani urnik_
6. Sistem urnik shrani in ga posodobljenega študentu prikaže 

#### Izjemni tok(ovi)

* Študent v polje za trajanje/uro vnese neveljaven podatek, zato mu sistem prikaže majhno sporočilo o neveljavnosti obrazca
* Študent pusti vnosno polje za ime obveznosti prazno, zato mu sistem prikaže majhno sporočilo o neveljavnosti obrazca

#### Pogoji

* Uporabnik mora v sistem biti prijavljen kot Študent. Če ni, mu ta funkcionalnost ni na voljo.
* Študent mora pred urejanjem imeti urnik prvo ustvarjen

#### Posledice

* Nov urnik se shrani v bazo in je študentu viden takoj po shranjevanju

#### Posebnosti

* Funkcionalnost **Urejanje urnika**  ne zahteva nobenih posebnosti glede dodatne opreme ali posebnih postopkov.

#### Prioritete identificiranih funkcionalnosti

* Funkcionalnosti **Urejanje urnika**  je bila določena prioriteta **Must Have**, ker je temeljna funkcionalost aplikacije.

#### Sprejemni testi

| Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
| :------------- | :------------ | :----------- | :-------------------------- | :------------------------- |
|Prikaz urnika (z orodji)|Uporabnik prijavljen kot študent, na pogledu _Pregled urnikov_, urnik že ima ustvarjen|Klik na gumb _Urejanje urnika_|Sistem študentu v novem pogledu prikaže njegov urnik v obliki tabele, poleg urnika pa vsa orodja za urejanje in gumb _Shrani urnik_|
|Orodja za urejanje|Uporabnik prijavljen kot študent, na pogledu _Urejanje urnika_|Brisanje obveznosti z urnika|Obveznost se odstrani z urnika|
|Orodja za urejanje|Uporabnik prijavljen kot študent, na pogledu _Urejanje urnika_|Klik na gumb _Ustvarjanje obveznosti na urniku_|Sistem prikaže majhen obrazec, kamor študent vpiše termin, ime in opis obveznosti|
|Orodja za urejanje|Uporabnik prijavljen kot študent, na pogledu _Pregled urnikov_|Klik na gumb _Uredi obveznost_ poleg želene obveznosti na urniku|Sistem prikaže majhen obrazec s podatki izbrane obveznosti, ki jih študent lahko spremeni|
|Shranjevanje urnika|Uporabnik prijavljen kot študent, na pogledu _Urejanje urnika_|Klik na gumb _Shrani urnik_|Sistem urnik shrani v bazo in ga posodobljenega prikaže študentu v pogledu _Pregled urnikov_|
|Preverjanje vnosnega polja|Obrazec za urejanje obstoječe obveznosti, prazno oz. neveljavno vnosno polje|Klik na gumb _Shrani obveznost_|Sistem prikaže ustrezno sporočilo o neveljavnosti obrazca|

### Ustvarjanje novega urnika

#### Študent lahko ustvari nov urnik.

#### Osnovni tok - Študent

1. Študent klikne na razdelek _Pregled urnikov_
2. Študent urnika še nima ustvarjenega
3. Pod obvestilom, da študent še nima ustvarjenega urnika, klikne na gumb _Ustvari nov urnik_
4. Sistem mu v pogledu _Uredi urnik_ prikaže prazen urnik
5. Študent urnik uredi po želji
6. Študent klikne na gumb _Shrani urnik_
7. Sistem urnik shrani in ga posodobljenega prikaže študentu

#### Alternativni tok 1

1. Študent v spustnem meniju klikne gumb _Ustvari nov urnik_
2. Študent urnika še nima ustvarjenega
3. Pod obvestilom, da študent še nima ustvarjenega urnika, klikne na gumb _Ustvari nov urnik_
4. Sistem mu v pogledu _Uredi urnik_ prikaže prazen urnik
5. Študent urnik uredi po želji
6. Študent klikne na gumb _Shrani urnik_
7. Sistem urnik shrani in ga posodobljenega prikaže študentu
 
 #### Alternativni tok 2 (študent ima že kreiran urnik)

1. Študent v spustnem meniju klikne gumb _Ustvari nov urnik_
4. Sistem mu v pogledu _Uredi urnik_ prikaže prazen urnik
5. Študent urnik uredi po želji
6. Študent klikne na gumb _Shrani urnik_
7. Sistem urnik shrani in ga posodobljenega prikaže študentu

#### Pogoji

* Uporabnik mora v sistem biti prijavljen kot Študent. Če ni, mu ta funkcionalnost ni na voljo.

#### Posledice

* Pri funkcionalnosti **Ustvari nov urnik**  ni poleg prikaza praznega urnika v pogledu _Uredi urnik_  nobenih večjih posledic.

#### Posebnosti

* Funkcionalnost **Ustvari nov urnik**  ne zahteva nobenih posebnosti glede dodatne opreme ali posebnih postopkov.

#### Prioritete identificiranih funkcionalnosti

* Funkcionalnosti **Ustvari nov urnik**  je bila določena prioriteta **Must Have**, ker je temeljna funkcionalost aplikacije.

#### Sprejemni testi

| Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
| :------------- | :------------ | :----------- | :-------------------------- | :------------------------- |
|Prikaz praznega urnika|Uporabnik prijavljen kot študent, na pogledu _Pregled urnikov_, urnika še nima ustvarjenega|Klik na gumb _Ustvari nov urnik_|Sistem študentu v pogledu _Uredi urnik_ prikaže prazen urnik za urejanje|
|Prikaz praznega urnika|Uporabnik prijavljen kot študent, na kateremkoli pogledu aplikacije, urnika še nima ustvarjenega|Klik na gumb _Ustvari nov urnik_ iz spustnega menija|Sistem študentu v pogledu _Uredi urnik_ prikaže prazen urnik za urejanje|
|Prikaz praznega urnika|Uporabnik prijavljen kot študent, na kateremkoli pogledu aplikacije, urnik že ima ustvarjen|Klik na gumb _Ustvari nov urnik_ iz spustnega menija|Sistem študentu v pogledu _Uredi urnik_ prikaže prazen urnik za urejanje|
|Kreiranje novega urnika|Uporabnik prijavljen kot študent, nov urnik izpolnjen|Klik na gumb _Kreiraj urnik_|Sistem kreira in shrani urnik v bazo, študenta pa vrne na pogled _Pregled urnikov_, kjer je nov urnik že viden|


### Pregled uporabnikov

#### Skrbnik ima pregled nad vsemi uporabniki, registriranih v sistem.

#### Osnovni tok

1.  Skrbnik klikne na gumb _Pregled uporabnikov_.
2. Sistem prikaže seznam vseh uporabnikov, registriranih v sistem.

#### Izjemni tokovi

*	Seja prijavljenega skrbnika je potekla, pri čemer je preusmerjen na prijavno stran.
*	Pri dostopu do podatkov iz podatkovne baze, pride do napake. Skrbniku se prikaže ustrezno pojavitveno okno z referenčno napako.

#### Pogoji

* Pri funkcionalnosti **Pregled uporabnikov** mora biti uporabnik v sistem prijavljen kot skrbnik.  Če ni prijavljen, ali pa je prijavljen z drugo vlogo, mu ta funkcionalnost ni na voljo.

#### Posledice

* Pri funkcionalnosti **Pregled uporabnikov**  ni poleg prikaza vseh uporabnikov nobenih posledic.

#### Posebnosti

* Funkcionalnost **Pregled uporabnikov**  ne zahteva nobenih posebnosti glede dodatne opreme ali posebnih postopkov.

#### Prioritete identificiranih funkcionalnosti

* Funkcionalnosti **Pregled uporabnikov**  je bila določena prioriteta **Could Have**, saj nam ta funkcionalnost omogoča pregled in nadzor za upravljanje z uporabniki v sitemu. A tudi brez nje bo sistem služil svojemu namenu.

#### Sprejemni testi


| Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
| :------------- | :------------ | :----------- | :-------------------------- | :------------------------- |
|Prikaz seznama vseh uporabnikov|Seznam uporabnikov je prazen|Klik na gumb _Pregled uporabnikov_|Prikaz obvestila o praznemu seznamu uporabnikov|
|Preusmerjanje na seznam uporabnikov|Skrbnik je na pogledu, ki ni pregled vseh obveznosti|Klik na gumb _Pregled uporabnikov_|Prikaz seznama uporabnikov|
|Obvestilo o napaki|Napaka na podatkovni bazi|Klik na gumb _Pregled uporabnikov_|Prikaz obvestila o napaki v podatkovni bazi|
|Potekla seja| Skrbnik je na poljubnem pogledu neaktiven že dalj časa| Klik na gumb _Pregled uporabnikov_ | Preusmeritev na prijavno stran z obvestilom o poteku seje

### Brisanje uporabnikov

#### Skrbnik ima možnost brisanja, kateregakoli uporabnika iz sistema.


#### Osnovni tok


1.  Skrbnik klikne na gumb _Pregled uporabnikov_.
2. Sistem prikaže seznam vseh uporabnikov, registriranih v sistem.
3. Skrbnik izbere enega izmed uporabnikov
4. Sistem prikaže opcije/ukaze, ki jih lahko izvrši nad uporabnikom
5.	Skrbnik klikne na gumb _X_
6.	Skrbnik potrdi svojo izbiro
7.	Sistem prikaže sporočilo o uspešnem izbrisu uporabnika

#### Izjemni tokovi


*	Seja prijavljenega skrbnika je potekla, pri čemer je preusmerjen na prijavno stran.
*	Uporabnik je že bil (pravkar/simultano, morebiti s strani drugega skrbnika) izbrisan iz sistema. Sistem javi ustrezno sporočilo o napaki.

	
#### Pogoji


* Pri funkcionalnosti **Brisanje uporabnikov** mora biti uporabnik v sistem prijavljen kot skrbnik.  Če ni prijavljen, ali pa je prijavljen z drugo vlogo, mu ta funkcionalnost ni na voljo.
* Pred izvršitvijo funkcionalnosti **Brisanje uporabnikov**, se mora uspešno izvesti funkcionalnost **Pregled uporabnikov** za dostop do funkcij brisanja

#### Posledice


* Po uspešnem izbrisu uporabnika, je uporabnik izbrisan iz sistema in pripadajoče podatkovne baze, kar je takoj vidno na seznamu vseh uporabnikov

####	Posebnosti


* Funkcionalnost **Brisanje uporabnikov** ne zahteva nobenih posebnosti glede dodatne opreme ali posebnih postopkov.
 
#### Prioritete identificiranih funkcionalnosti


* Funkcionalnosti **Brisanje uporabnikov**  je bila določena prioriteta **Could Have**, ker bo sistem brez nje tudi deloval, bo pa uporabna funkcija za nadaljnji razvoj, ko bo uporabnikov veliko in bo upravljanje uporabnikov bolj potrebno.

#### Sprejemni testi


| Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovan rezultat |
| :------------- | :------------ | :----------- | :-------------------------- | :------------------------- |
|Izbris uporabnika|Skrbnik je na pogledu seznama uporabnikov|Klik na gumb _X_|Sporočilo o izbrisu in vrnitev na pogled seznama uporabnikov|
|Obvestilo o napaki|Napaka na podatkovni bazi|Klik na gumb _X_|Prikaz obvestila o napaki v podatkovni bazi|
|Potekla seja| Skrbnik je na pogledu seznama uporabnikov neaktiven že dalj časa| Klik na gumb _X_ | Preusmeritev na prijavno stran z obvestilom o poteku seje


## 6. Nefunkcionalne zahteve


#### Zahteve izdelka


*	Sistem mora biti dosegljiv na javno dostopnem spletnem naslovu.
*	Sistem mora biti na voljo najmanj 99,8 odstotkov časa.
*	Samo skrbnik sistema lahko izbriše uporabnika iz baze podatkov
*	Pri uporabi podatkovne baze, čas nalaganja strani z uporabniškega vidika ne sme biti daljši od 2s.
*	Omejitev prisotnosti na spletni strani mora biti dovolj prilagodljiva, da lahko podpira 80 uporabnikov

#### Organizacijske zahteve

*	Uporabniki sistema se morajo za pridobitev uporabniškega računa identificirati z digitalnim potrdilom (Sigen-CA itd.).
*	Za varnost podatkov bomo uporabili standard TDE (Transparent Data Encryption) nad podatkovno bazo.

#### Zunanje zahteve.


*	Aplikacija je regulirana tako, da spoštuje GNU GPL Version 2 (General Public License). Tako da bo brezplačna in odprto dostopna vsem uporabnikom.
*	Upošteval se bo tudi "Etični kodeks za mladinsko delo" (ACT), kjer se bomo držali smernic za ohranjanje zaupnosti in strokovne meje.



## 7. Prototipi vmesnikov



#### Vmesniki do zunanjih sistemov

Naš sistem bo v okviru funkcionalnosti **Pregled ocen** komuniciral z Študijskim informacijskim sistemom. 
Ta sistem bo našemu posredoval ocene vseh predmetov za posameznega študenta.

**funkcija**: status_ocen(student)

**vhod**: šifra studenta

**izhod**: Datoteka CSV, kjer vsaka vrstica predstavlja en predmet. 
Vsak predmet pa vsebuje podatke o študentovih ocenah in opravljenih obveznostih so ločeni s podpičji. To so:

*	šifra predmeta
*	ime predmeta
*	izvajalec
*	letnik opravljanja
*	ocena sprotnih obveznosti
*	ocena izpita
*	končna ocena

#### Za ogled, kliknite na sliko

[![Zaslonske maske](../img/zaslonske_maske.png)](https://youtu.be/bx-VRyx-pbI)
