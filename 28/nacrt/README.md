﻿# Načrt sistema

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Maj Šavli, Jan Šturm, Mitja Brezovnik in Urban Rupnik |
| **Kraj in datum** | Ljubljana, 22.4.2019 |



## Povzetek

Na začetku dokumenta, pod točko 1, se nahaja načrt arhitekture, kjer je predstavljen logičen in razvojni pogled arhitekture aplikacije, ki jo bomo izvedli po arhitekturnem vzorcu MVC. Logični pogled je zelo poenostavljen pogled nad tem, katere so glavne komponente aplikacije oz. kako je aplikacija sestavljena po MVC arhitekturi, razvojni pogled pa prikazuje podrobnejšo sestavo komponent z vidika razvijalca programske opreme. Sledi razredni diagram, ki po UML predstavlja glavne razrede oz. objekte aplikacije, ki so v nadaljnjem dokaj podrobno opisani. Naslednji in tudi zadnji del pa sestavljajo diagrami zaporedja. Za vsak osnovni in vse alternativne tokove vsakega primera uporabe je prikazan diagram zaporedja in pripadajoč opis.


## 1. Načrt arhitekture

### 1.1 Logični pogled

![Logični pogled](../img/diagrami_lp3/logicni_pogled.png)


### 1.2 Razvojni pogled

![Razvojni pogled](../img/diagrami_lp3/razvojni_pogled.png)


## 2. Načrt strukture

### 2.1 Razredni diagram

![Razredni diagram](../img/diagrami_lp3/razredni_diagram.png)

### 2.2 Opis razredov

### Uporabnik

Uporabnik je oseba, ki uporablja naš sistem.

#### Atributi

* uporabnisko_ime
	* podatkovni tip: email
* geslo
	* podatkovni tip: String
	* zaloga vrednosti: poljuben niz znakov dolžine [3,...,32]
* vloga
	* podatkovni tip: int
	* zaloga vrednosti: vrednost iz cele množice [1,2,3].
		* 1 - skrbnik
		* 2 - podpora
		* 3 - študent

#### Nesamoumevne metode

* **avtentikacija()**
*parametri*: {uporabnisko_ime: email, geslo: String}
*tip rezultata*: boolean
*pomen*: S pomočjo enoličnega uporabniškega imena in gesla, preverimo resničnost prijavnih podatkov.

### Študent

Vrsta uporabnika, kateremu je sistem namenjen. Lahko dodaja in ureja urnike in obveznosti, ima pregled nad njegovimi ocenami in možnost pošiljanja sporočil.

#### Atributi

* vpisna_st
	* podatkovni tip: int
	* zaloga vrednosti: število dolžine 8

#### Nesamoumevne metode

 * **ustvariŠtudenta()**
*parametri*:  {uporabnisko_ime: email, geslo: String}
*tip rezultata*: void

 * **izbrišiŠtudenta()**
*parametri*:  {vpisna_st: int}
*tip rezultata*:  void

* **preveriRegistracijskePodatke()**
*parametri*:  {uporabnisko_ime: email, geslo: String, ponovitev_gesla: String}
*tip rezultata*: boolean
*pomen*: preveri, če so vnešeni podatki v pravilnem oz. dovoljenem formatu

### Aktivnost urnika

Predstavlja posamezne aktivnosti, ki sestavljajo urnik.

#### Atributi

* naslov
	* podatkovni tip: String
* opis
	* podatkovni tip: String
* dan
	* podatkovni tip: int
	* zaloga vrednosti: vrednost iz cele množice [1,..,7]
	* pomen: predstavlja dan v tednu, kjer predstavlja 1- ponedeljek, 2- torek itd.
* trajanje_od
	* podatkovni tip: int
	* zaloga vrednosti: vrednost iz cele množice [0,...,24]
	* pomen: predstavlja uro začetka aktivnosti na urniku.
* trajanje_do
	* podatkovni tip: int
	* zaloga vrednosti: vrednost iz cele množice [0,...,24]
	* pomen: predstavlja uro konca aktivnosti na urniku.
* barva
	* podatkovni tip: int
	* zaloga vrednosti: vrednosti množice [1,...,10]
	* pomen: predstavlja posamezno barvo aktivnosti predstavljene na urniku.
			* 1 - rdeča
			* 2 - zelena
			* 3 - modra
			* 4 - rumena
			* 5 - vijolična
			* 6 - oranžna
			* 7 - črna
			* 8 - bela
			* 9 - rjava
			* 10 - roza
* st_aktivnosti
	* podatkovni tip: int
	* pomen: enolični identifikator aktivnosti

#### Nesamoumevne metode

* **posodobiAktivnost()**
*parametri*: {naslov: String, opis: String, trajanje_od: int, trajanje_do: int, barva: int}
*tip rezultata*: void
*pomen*: Posodobi spremenjene podatke v aktivnosti.

* **izbrišiAktivnost()**
*parametri*: {st_aktivnosti: int}
*tip rezultata*: void

* **ustvariAktivnost()**
*parametri*: {naslov: String, opis: String, trajanje_od: int, trajanje_do: int, barva: int}
*tip rezultata*: void

### Urnik

Predstavlja urnik študenta, s katerim lahko upravlja in si ga prilagodi glede na lasten kurikulum.

#### Atributi

* st_urnika
	* podatkovni tip: int
	* pomen: (skupaj z vpisno število študenta) enolični identifikator urnika
* aktivnosti 
	* podatkovni tip: ArrayList < Aktivnost urnika >
* opis_urnika
	* podatkovni tip: String

#### Nesamoumevne metode

* Metodi **vrniUrnike()** in **shraniUrnik()** sta samo *getter* in *setter* razreda 

### Obveznosti

Predstavlja študentove zadolžitve, ki jih mora opraviti v študijskem letu. Tiste, ki jih mora zagotovo opraviti in noče pozabiti, si jih bo shranil v sistem.

#### Atributi

* naslov
	* podatkovni tip: String
* opis
	* podatkovni tip: String
* datum
	* podatkovni tip: date
	* pomen: predstavlja datum, do katerega mora biti zadolžitev opravljena.
* čas
	* podatkovni tip: time
	* pomen: predstavlja uro, do katerega mora biti zadolžitev opravljena.
* tip
	* podatkovni tip: int
	* zaloga_vrednosti: celo število iz množice [1,...,7], kjer posamezno število pomeni:
		* 1 - pisni izpit
		* 2 - ustni izpit
		* 3 - kolokvij
		* 4 - domača naloga
		* 5 - seminarska naloga
		* 6 - kviz
		* 7 - drugo
* prioriteta
	* podatkovni tip: int
	* zaloga vrednosti: celo število iz množice [1,2,3,4,5], kjer 1 pomeni najmanjšo prioriteto in 5 največjo.

* st_obveznosti
	* podatkovni tip: int
	* zaloga_vrednosti: celo število
	* pomen: skupaj z vpisno število študenta enolično identificira obveznost

#### Nesamoumevne metode

* **ustvariObveznost()**
*parametri*: {naslov: String, opis: String, datum: date, čas: time, tip: int, prioriteta: int}
*tip rezultata*: void

* **odstraniObveznost()**
*parametri*: {id: int}
*tip rezultata*: void

* **potrdiObveznost()**
*parametri*: {id: int}
*tip rezultata*: boolean

### Sporočilo

Predstavlja sporočilo, ki ga lahko pošlje študent drugemu uporabniku. Bodisi drugemu študentu ali pa kot prijava težave skrbniku ali podpori.

#### Atributi

* čas
	* podatkovni tip: time
	* pomen: čas ko je bilo sporočilo poslano
* datum
	* podatkovni tip: date
	* pomen: datum ko je bilo sporočilo poslano
* vsebina
	* podatkovni tip: String
* tema
	* podatkovni tip: String
	* pomen: "Naslov", ki ga prejemnik vidi kot glavo sporočila.

#### Nesamoumevne metode

* Metoda **ustvariSporočilo()** je samo *setter* razreda

### Pogovor

Je sestavljen iz sporočil, ki sestavljajo pogovor med pošiljateljem in prejemnikom.

#### Atributi

* pogovor
	* podatkovni tip: ArrayList < Sporočilo >
	* pomen: Polje sporočil predstavlja zaporedje izmenjavanja informacij med pošiljateljem in prejemnikom.

#### Nesamoumevne metode

* Metodi **vrniPogovor()** in **posodobiPogovor** sta samo *getter* in *setter* razreda

 * **vrniSeznamŠtudentov()**
*parametri*: /
*tip rezultata*: ArrayList < Študent > 
*pomen*: vrne polje študentov, ki sodelujejo v pogovoru.

### Kontrolni razred

Ima vlogo kontrolerja v arhitekturi MVC. Preko njega so ločeni pogledi zaslonskih mask od podatkov razredov.

#### Atributi

Kontrolni razred ne vsebuje svojih atributov.

#### Nesamoumevne metode

 * **vrniUrnike()**
*parametri*: {vpisna_st: int}
*tip rezultata*: Urnik 
*pomen*: preko vpisne številke lahko pridobimo ustrezen urnik za študenta in ga posredujemo pogledu.

 * **vrniObveznosti()**
*parametri*: {vpisna_st: int}
*tip rezultata*:  ArrayList < Obveznost > 
*pomen*:  preko vpisne številke lahko pridobimo ustrezne obveznosti študenta in jih posredujemo pogledu.

 * **preveriRegistracijskePodatke()**
*parametri*: {uporabnisko_ime: email, geslo: String, ponovitev_gesla: String}
*tip rezultata*: boolean
*pomen*:  metoda pošlje v pregled podatke razredu, katera nato vrne ustrezen odgovor [true, false].

 * **vrniSeznamSporočil()**
*parametri*: {vpisna_st: int}
*tip rezultata*:  ArrayList < Pogovor > 
*pomen*:  preko vpisne številke lahko pridobimo ustrezne pogovore študenta in jih posredujemo pogledu za prikaz.

 * **vrniSeznamŠtudentov()**
*parametri*: {vpisna_st: int}
	*tip rezultata*:  ArrayList < Študent > 
*pomen*:  "sprehodimo" se do podatkov v razredu in posredujemo pogledu seznam vseh študentov.

 * **izbrišiŠtudenta()**
*parametri*: {vpisna_st: int}
*tip rezultata*:  void
*pomen*:  metoda iz pogleda dobi "ukaz" za izbris študenta, krmilnik se nato preusmeri v ustrezen entitetni razred, kateri nato izbriše študenta.

 * **pošljiSporočilo()**
*parametri*: {vpisna_st: int, vsebina: String, tema: String, vpisna_st_prejemnika: int}
*tip rezultata*:  void
*pomen*: pogled preko te medote posreduje podatke entitetnemu razredu, ki nato posodobi pogovor z poslanim sporočilom "ustvariSporočilo".

 * **vrniOceneŠtudenta()**
*parametri*: {vpisna_st: int}
*tip rezultata*:  CSV file 
*pomen*:  ko pogled zahteva prikaz ocen študenta, se kliče ta metoda, ki dostopa do študijskega informacijskega sistema in posreduje pridobljeno CSV datoteko z ocenami.

 * **shraniUrnik()**
*parametri*: {vpisna_st: int, st_urnika, opis_urnika: String, aktivnosti: ArrayList < Aktivnost urnika >}
*tip rezultata*:  void
*pomen*:  ko pride do zahteve za shranjen urnik, se pošljejo podatki do entitetnega razreda Urnik, kateri nato shrani urnik z ustreznimi podatki.

 * **ustvariNovoObveznost()**
*parametri*: {vpisna_st: int, String, opis: String, datum: date, čas: time, tip: int, prioriteta: int}
*tip rezultata*:  void
*pomen*:  ko želi uporabnik ustvariti novo obveznost, se podatki iz pogleda posredujejo sem, kjer jih nato krmilnik preusmeri do entitetnega razreda Obveznost. Tam se nato ustvari nova obveznost.

 * **potrdiObveznost()**
*parametri*: {vpisna_st: int, String, opis: String, datum: date, čas: time, tip: int, prioriteta: int}
*tip rezultata*:  boolean
*pomen*:  podatki se iz krmilnika posredujejo do entitetnega razreda Obveznosti, kjer se nato v metodi "potrdiObveznosti()" na strani razreda, preveri veljavnost podatkov in pošlje rezultat nazaj sem, ta metoda pa ga posreduje pogledu.

 * **odstraniObveznost()**
*parametri*: {vpisna_st: int, naslov: String}
*tip rezultata*:  void
*pomen*:  ko uporabnik na nivoju pogleda zahteva izbris Obveznosti, se ukaz posreduje sem v krmilnik, ki nato pošlje zahtevo v entitetni razred, kjer se zahteva izvrši.

 * **preveriPrijavnePodatke()**
*parametri*: {vpisna_st: int, uporabnisko_ime: email, geslo: String}
*tip rezultata*:  boolean
*pomen*:  metoda pošlje v pregled podatke razredu, katera nato vrne ustrezen odgovor pogledu.

 * **prijava()**
*parametri*: {uporabnisko_ime: email, geslo: String}
*tip rezultata*:  boolean
*pomen*:  ko se želi uporabnik prijaviti v sistem, se podatki preko krmilnika posredujejo razredu, kjer se izvrši avtentikacija prijave, in če je prijava uspešna se pogledu posreduje true - uspešna prijava ali false - neuspešna prijava.

### SV Študijski informacijski sistem

Študijski informacijski sistem je namenjen zunanjemu dostopu do ocen študentov.

#### Atributi

SV Študijski informacijski sistem ne vsebuje svojih atributov.

#### Nesamoumevne metode

 * **vrniOceneŠtudenta()**
*parametri*: {vpisna_st: int}
*tip rezultata*: CSV file
*pomen*: izvede klic na rest api s podatki študenta, ki vrne njegove ocene v csv obliki

 * **preveriDostop()**
*parametri*: {vpisna_st: int}
*tip rezultata*:  boolean
*pomen*: preveri če ima študijski informacijski sistem, ki ga uporablja študent, dostopen rest api, za dostop do njegovih ocen, in če ima študent dostop do njega (torej, če je njegovo geslo in uporabniško ime enako, kot ga uporablja za dostop do tega sistema).

### ZM sporočila

Zaslonska maska, ki vsebuje metode za delo z sporočili.

#### Atributi

Zaslonska maska sporočila ne vsebuje svojih atributov.

#### Nesamoumevne metode

 * **prikažiSeznamSporočil()**
*parametri*: {vpisna_st: int, pogovor: Pogovor}
*tip rezultata*: void

 * **prikažiVnosnoPolje()**
*parametri*: /
*tip rezultata*:  void
*pomen*: uporabniku pokaže vnosno masko za vnos sporočila.

 * **pošljiSporočilo()**
*parametri*:{vpisna_st: int, vsebina: String, tema: String, vpisna_st_prejemnika: int}
*tip rezultata*:  void
*pomen*: v obstoječ pogovor med pošiljateljem in prejemnikom se doda novo sporočilo, če predhodnjih sporočil med njima ni bilo, se ustvari nov pogovor in znotraj njega sporočilo.

### ZM obveznosti

Zaslonska maska, ki vsebuje metode za delo z obveznostmi.

#### Atributi

Zaslonska maska obveznosti ne vsebuje svojih atributov.

#### Nesamoumevne metode

 * **prikažiPregledObveznosti()**
*parametri*: {vpisna_st: int}
*tip rezultata*: void
*pomen*: metoda sprejme študenta, preko katere dobimo njegove obveznosti, ki jih nato prikažemu uporabniku.

 * **prikažiOcene()**
*parametri*: {vpisna_st: int}
*tip rezultata*: void

 * **ustvariNovoObveznost()**
*parametri*: {vpisna_st: int, String, opis: String, datum: date, čas: time, tip: int, prioriteta: int}
*tip rezultata*: void

 * **potrdiObveznost()**
*parametri*:  {id_obveznosti: int}
*tip rezultata*: boolean
*pomen*: metoda potrdi veljavnost obveznosti.

 * **preveriObrazecZaNovoAktivnost()**
*parametri*: {naslov: String, opis: String, trajanje_od: int, trajanje_do: int, barva: int}
*tip rezultata*: boolean
*pomen*: metoda preveri če so vsi vnosni podatki pravilni in v pravem formatu.

 * **preveriObrazecZaNovoObveznost()**
*parametri*: {naslov: String, opis: String, datum: date, čas: time, tip: int, prioriteta: int}
*tip rezultata*: boolean
*pomen*: metoda preveri če so vsi vnosni podatki pravilni in v pravem formatu.

### ZM urniki

Zaslonska maska, ki vsebuje metode za delo z urniki.

#### Atributi

Zaslonska maska urniki ne vsebuje svojih atributov.

#### Nesamoumevne metode

 * **prikažiUrnike()**
*parametri*: {vpisna_st: int}
*tip rezultata*: void
*pomen*: metoda sprejme študenta, preko katerega dobimo njegov urnik, ki jih nato prikažemu uporabniku.

 * **prikažiUrnikZaUrejanje()**
*parametri*: {vpisna_st: int, st_urnika: int}
*tip rezultata*: void

 * **urediAktivnostUrnika()**
*parametri*: {vpisna_st: int, st_urnika: int , st_aktivnosti, naslov: String, opis: String, trajanje_od: int, trajanje_do: int, barva: int}
*tip rezultata*: void

 * **shraniUrnik()**
*parametri*: {vpisna_st: int, st_urnika, opis_urnika: String, aktivnosti: ArrayList < Aktivnost urnika >}
*tip rezultata*: void

 * **prikažiObrazecZaNovoAktivnostUrnika()**
*parametri*: {vpisna_st: int, st_urnika: int}
*tip rezultata*: void

 * **ustvariNovoAktivnostUrnika()**
*parametri*: {vpisna_st: int, st_urnika: int}
*tip rezultata*: void

 * **prikažiPrazenUrnikZaUrejanje()**
*parametri*: {vpisna_st: int}
*tip rezultata*: void
*pomen*: uporabniku se prikaže prazen urnik za urejanje.

 * **preveriVnosnoPoljeAktivnostiUrnika()**
*parametri*: {naslov: String, opis: String, trajanje_od: int, trajanje_do: int, barva: int}
*tip rezultata*: void
*pomen*: metoda preveri če so vsi vnosni podatki pravilni in v pravem formatu.

### ZM skrbnik

Zaslonska maska, ki vsebuje metode, ki jih uporablja skrbnik pri svojem delovanju.

#### Atributi

Zaslonska maska skrbnik ne vsebuje svojih atributov.

#### Nesamoumevne metode

 * **prikažiSeznamŠtudentov()**
*parametri*: {uporabnisko_ime: email, vloga: int}
*tip rezultata*: void
*pomen*: metoda prek parametrov ugotovi, če ima uporabnik zadostne pravice za dostop do seznama študentov in nato rezultat prikaže na zaslon.

 * **prikažiModalnoOknoZaIzbris()**
*parametri*: {vpisna_st: int, uporabnisko_ime: email, vloga: int}
*tip rezultata*: void
*pomen*: metoda prek parametrov ugotovi, če ima uporabnik zadostne pravice za izbris uporabnika in po uspešni avtentikaciji prikaže modalno okno.

 * **potrdiIzbris()**
*parametri*: {vpisna_st: int, uporabnisko_ime: email, vloga: int}
*tip rezultata*: void
*pomen*: metoda prek parametrov ugotovi, če ima uporabnik zadostne pravice za izbris uporabnika in ga po uspešni avtentikaciji izbriše.

### ZM Podpora

Zaslonska maska, ki vsebuje metode, ki jih uporablja podpora pri svojem delovanju.

#### Atributi

Zaslonska maska podpora ne vsebuje svojih atributov.

#### Nesamoumevne metode

 * **prikažiSeznamŠtudentov()**
*parametri*: {uporabnisko_ime: email, vloga: int}
*tip rezultata*: void
*pomen*: metoda prek parametrov ugotovi, če ima uporabnik zadostne pravice za dostop do seznama študentov in nato rezultat prikaže na zaslon.

 * **izberiŠtudenta()**
*parametri*: {vpisna_st: int, uporabnisko_ime: email, vloga: int}
*tip rezultata*: void
*pomen*: metoda prek parametrov ugotovi, če ima uporabnik zadostne pravice za dostop do seznama študentov in nato prikaže študentove podatke na zaslon.

 * **pošljiSporočilo()**
*parametri*: {vpisna_st: int, vsebina: String, tema: String, vpisna_st_prejemnika: int}
*tip rezultata*: void
*pomen*: preko te metode lahko podpora pošilja sporočila in nudi tehnično podporo ostalim uporabnikom.

 * **prikažiPogovorSŠtudentom()**
*parametri*: {vpisna_st: int, uporabnisko_ime: email}
*tip rezultata*: void

### ZM Uporabnik

Zaslonska maska, ki vsebuje metode, ki jih uporablja uporabnik za uporabniku prijazno delo z sistemom.

#### Atributi

Zaslonska maska uporabnik ne vsebuje svojih atributov.

#### Nesamoumevne metode

 * **prikažiRegistracijskiObrazec()**
*parametri*: /
*tip rezultata*: void
*pomen*: preko te metode se uporabniku prikaže obrazec za registracijo.

 * **potrdiRegistracijo()**
*parametri*: {uporabnisko_ime: email, geslo: String, ponovitev_gesla: String}
*tip rezultata*: void
*pomen*: metoda po uspešni avtentikaciji potrdi registracijo.

 * **prikažiPrijavniObrazec()**
*parametri*: /
*tip rezultata*: void
*pomen*: preko te metode se uporabniku prikaže obrazec za prijavo.

 * **potrdiPrijavo()**
*parametri*: {uporabnisko_ime: email, geslo: String}
*tip rezultata*: void
*pomen*: metoda po uspešni avtentikaciji potrdi prijavo.

 * **preveriPrijavniObrazec()**
*parametri*: {uporabnisko_ime: email, geslo: String}
*tip rezultata*: boolean
*pomen*: metoda preveri, če uporabnik z takšnim uporabniškim imenom in geslom obstaja v bazi in če je avtentikacija pravilna, ga vpiše.

 * **preveriRegistracijskiObrazec()**
*parametri*: {uporabnisko_ime: email, geslo: String, ponovitev_gesla: String}
*tip rezultata*: boolean
*pomen*: metoda preveri, če se vsa vnosna polja za registracijo skladajo z pravili in so v pravilnem formatu. Če je vse uspešno, je uporabnik uspešno registriran, drugače se prikaže obvestilo.

 * **preveriVnosnoPoljeSporočila()**
*parametri*: {vpisna_st: int, vsebina: String, Tema: String}
*tip rezultata*: boolean
*pomen*: metoda preveri, če se vsa vnosna polja sporočila pravilna.

 * **prikažiObvestilo()**
*parametri*: /
*tip rezultata*: void
*pomen*: metoda glede na uspešnost/neuspešnost avtentikacije prikaže ustrezno obvestilo.

## 3. Načrt obnašanja

### Brisanje obstoječe obveznosti

![Brisanje obstoječe obveznosti Osnovni tok](../img/diagrami_lp3/brisanje_obstoječe_obveznosti_osnovni_tok.png)

Slika prikazuje diagram zaporedja za osnovni tok pri povezavi med vlogo Študent in funkcionalnostjo Brisanje obstoječe obveznosti. Študent na zaslonski maski Obveznosti izbere funkcionalnost Odstrani obveznost. Nato se kliče metoda odstraniObveznost(), ki iz podatkovne baze izbriše izbrano Obveznost. Po uspešnem brisanju, se uporabniku Študent na zaslonski maski Obveznosti prikaže obvestilo o uspešnem brisanju in nato pregled posodobljenih obveznosti. 
#
### Izbris uporabnikov
![Izbris uporabnikov osnovni tok Skrbnik](../img/diagrami_lp3/izbris_uporabnikov_osnovni_tok.png)

Slika prikazuje diagram zaporedja za osnovni tok pri povezavi med vlogo Skrbnik in funkcionalnostjo Izbris uporabnikov. Skrbniku se na zaslosnki maski Skrbnik prikaže modalno okno za izbris uporabnikov. Ko Skrbnik izbere uporabnika za izbris, se kliče metoda izbrisŠtudenta() ki iz podatkovne baze izbriše izbranega Študenta. Po uspešnem brisanju, se uporabniku Skrbnik na zaslonski maski Skrbnik prikaže obvestilo o uspešnem brisanju in nato posodobljenih seznam študentov. 
#
### Potrjevanje obstoječe obveznosti
![Potrjevanje obstoječe obveznosti osnovni tok](../img/diagrami_lp3/potrjevanje_obstoječe_obveznosti_osnovni_tok.png)

Slika prikazuje diagram zaporedja za osnovni tok pri povezavi med vlogo Študent in funkcionalnostjo Potrjevanje obstoječe obveznosti. Študent na zaslonski maski Obveznosti izbere funkcionalnost Potrdi obveznost nato se kliče metoda potrdiObveznost(), ki v podatkovni bazi spremeni atribut izbrane Obveznosti. Po uspešni potrditvi obveznosti, se uporabniku Študent na zaslonski maski Obveznosti prikaže obvestilo o uspešnem potrjevanju in nato posodobljen pregled obveznosti. 
#
### Pregled obveznosti
![Pregled Obveznosti Osnovni tok](../img/diagrami_lp3/pregled_obveznosti_osnovni_tok.png)

Slika prikazuje diagram zaporedja za osnovni tok pri povezavi med vlogo Študent in funkcionalnostjo Pregled obveznosti. Študent na zaslonski maski Obveznosti izbere funkcionalnost Pregled obveznost nato se kliče metoda vrniObveznosti(), ki iz podatkovne baze vrne seznam obveznosti. Uporabniku Študent se na zaslonski maski Obveznosti prikaže seznam vrnjenih obveznosti. 
#
###  Pregled ocen
![Pregled ocen izjemni tok študent](../img/diagrami_lp3/pregled_ocen_izjemni_tok.png)

Slika prikazuje diagram zaporedja za izjemni tok pri povezavi med vlogo Študent in funkcionalnostjo Pregled ocen. Za prikaz ocen uporabniku Študent, se na zaslonski maski Študent s klicem metode vrniOceneŠtudenta() prikažejo njegove ocene. Metoda vrniOceneŠtudenta() se preko sistemskega vmesnika Študijski informacijski sistem poveže na študijski informacijski sistem. Ob neuspešnem dostopu, se uporabniku Študent na zaslonski maski prikaže obvestilo o neuspešnem dostopu.

![Pregled ocen osnovni tok študent](../img/diagrami_lp3/pregled_ocen_osnovni_tok.png)

Slika prikazuje diagram zaporedja za osnovni tok pri povezavi med vlogo Študent in funkcionalnostjo Pregled ocen. Za prikaz ocen uporabniku Študent, se na zaslonski maski Študent s klicem metode vrniOceneŠtudenta() prikažejo njegove ocene.  Metoda vrniOceneŠtudenta() se preko sistemskega vmesnika Študijski informacijski sistem poveže na študijski informacijski sistem. Po uspešnem dostopu študijski informacijski sistem vrne ocene študenta, ki se nato z metodo prikažiOcene() prikažejo uporabniku Študent na zaslonski maski Študent.
#
### Pregled sporočil
![Pregled sporočil osnovni tok podpora](../img/diagrami_lp3/pregled_sporočil_osnovni_tok_podpora.png)

Slika prikazuje diagram zaporedja za osnovni tok pri povezavi med vlogo Podpora in funkcionalnostjo Pregled sporočil. Uporabniku Podpora, se na zaslonski maski Podpora s klicem metode vrniSeznamŠtudentov() najprej prikaže seznam vseh študentov, s katerimi so bila že izmenjana sporočila. Kontrolni razred z metodo vrniSeznamŠtudentov iz podakovne baze Pogovor vrne seznam študentov, ki so kontaktirali podporo. Na zaslonski maski se z metodo prikažiSeznamŠtudentov() prikaže uporabniku Podpora vrnsjen seznam študentov. Uporabnik Podpora na zaslonski maski Podpora izbere študenta za prikaz njegovega seznama sporočil. Kliče se metoda vrniSeznamSporočil(), nato Kontrolni razred kliče metodo vrniPogovor(), ki iz podatkovne baze Pogovor vrne pogovor izbranega študenta. Ko podatkovna baza vrne pogovor, se le ta z metodo priakžiSeznamSporočil() prikaže uporabniku Podpora na zaslonski maski Podpora.

![Pregled sporočil osnovni tok študent](../img/diagrami_lp3/pregled_sporočil_osnovni_tok_študent.png)

Slika prikazuje diagram zaporedja za osnovni tok pri povezavi med vlogo Študent in funkcionalnostjo Pregled sporočil. Uporabniku Študent se za prikaz sporočil na zaslonski maski Študent kliče metoda vrniSeznamSporočil(), nato Kontrolni razred kliče metodo vrniPogovor(), ki iz podatkovne baze Pogovor vrne pogovor študenta. Ko podatkovna baza vrne pogovor, se le ta z metodo priakžiSeznamSporočil() prikaže uporabniku Študent na zaslonski maski Študent.
#
### Pregled uporabnikov
![Pregled uporabnikov osnovni tok Skrbnik](../img/diagrami_lp3/Pregled uporabnikov osnovni tok Skrbnik.png)

Slika prikazuje diagram zaporedja za osnovni tok pri povezavi med vlogo Skrbnik in funkcionalnostjo Pregled uporabnikov. Za prikaz študentov se kliče metoda prikažiSeznamŠtudentov(), nato pa kontrolni razred kliče metodo vrniSeznamŠtudentov)(), ki iz podatkovne baze vrne seznam vseh študentov. Ob uspešnem klicu se uporabniku Skrbnik na zaslonski maski Skrbnik prikaže seznam študentov.

![Pregled uporabnikov osnovni tok Skrbnik](../img/diagrami_lp3/pregled_uporabnikov_osnovni_tok.png)

Slika prikazuje diagram zaporedja za osnovni tok pri povezavi med vlogo Študent in funkcionalnostjo Pregled urnikov. Uporabniku Študent se za prikaz urnikov na zaslonski maski Urniki kliče metoda vrniUrnike(), nato Kontrolni razred kliče metodo vrniUrnike(), ki iz podatkovne baze Urnik vrne urnike prijavljenega študenta. Ko podatkovna baza vrne urnike, se z metodo prikažiUrnike() prikažejo uporabniku Študent na zaslonski maski Urniki.
#
### Pregled urnikov
![Pregled urnikov osnovni tok študent](../img/diagrami_lp3/pregled_urnikov_osnovni_tok.png)

Slika prikazuje diagram zaporedja za izjemni tok pri povezavi med vlogo Študent, Podpora ter Skrbnik in funkcionalnostjo Prijava. Na zaslonski maski se najprej z metodo prikažiPirjavniObrazec() prikaže obrazec za prijavo. Ko uporabnik potrdi prijavo, se najprej z metodo preveriPrijavniObrazec() preveri polja na obrazcu nato se kliče metoda preveriPrijavnePodatke(). Kontrolni razred z metodo avtentikacija() preveri podatke o uporabniku v podatkovni bazi Uporabnik. Ob neujemanju uporabniškega imena in gesla, se vrne obvestilo, ki se prikaže z metodo prikažiObvestilo() na zaslonski maski.
#
### Prijava
![Prijava izjemni tok 2 za vse tri vloge](../img/diagrami_lp3/prijava_izjemni_tok_2_za_vse_tri.png)

Slika prikazuje diagram zaporedja za izjemni tok pri povezavi med vlogo Študent, Podpora ter Skrbnik in funkcionalnostjo Prijava. Na zaslonski maski se najprej z metodo prikažiPrijavniObrazec() prikaže obrazec za prijavo. Ko uporabnik potrdi prijavo, se najprej z metodo preveriPrijavniObrazec() preveri polja na obrazcu. Če so polja na obrazcu neustrezno izpolnjena, se uporabniku z metodo prikažiObvestilo() prikaže obvestilo o neustreznosti podatkov na obrazcu.

![Prijava izjemni tok za vse tri vloge](../img/diagrami_lp3/prijava_izjemni_tok_za_vse_tri.png)
 
Slika prikazuje diagram zaporedja za izjemni tok pri povezavi med vlogo Študent, Podpora ter Skrbnik in funkcionalnostjo Prijava. Na zaslonski maski se najprej z metodo prikažiPrijavniObrazec() prikaže obrazec za prijavo. Ko uporabnik potrdi prijavo, se najprej z metodo preveriPrijavniObrazec() preveri polja na obrazcu. Če so polja na obrazcu neustrezno izpolnjena, se uporabniku z metodo prikažiObvestilo() prikaže obvestilo o neustreznosti podatkov na obrazcu.

![Prijava Osnovni tok Podpora](../img/diagrami_lp3/prijava_osnovni_tok_podpora.png)

Slika prikazuje diagram zaporedja za osnovni tok pri povezavi med vlogo Podpora in funkcionalnostjo Prijava. Na zaslonski maski se najprej z metodo prikažiPrijavniObrazec() prikaže obrazec za prijavo. Ko uporabnik Podpora potrdi prijavo, se najprej z metodo preveriPrijavniObrazec() preveri polja na obrazcu nato se kliče metoda preveriPrijavnePodatke(). Kontrolni razred z metodo avtentikacija() preveri podatke o uporabniku v podatkovni bazi Uporabnik. Če so podatki ustrezni, kontrolni razred kliče metodo prijava(), ki uporabnika Podpora prijavi v sistem. Ko je uporabnik Podpora uspešno prijavljen, se mu na zaslonski maski Podpora s klicem metode prikažiSeznamŠtudentov() prikaže seznam študentov.

![Prijava Osnovni tok Skrbnik](../img/diagrami_lp3/prijava_osnovni_tok_skrbnik.png)

Slika prikazuje diagram zaporedja za osnovni tok pri povezavi med vlogo Skrbnik in funkcionalnostjo Prijava. Na zaslonski maski se najprej z metodo prikažiPrijavniObrazec() prikaže obrazec za prijavo. Ko uporabnik Skrbnik potrdi prijavo, se najprej z metodo preveriPrijavniObrazec() preveri polja na obrazcu nato se kliče metoda preveriPrijavnePodatke(). Kontrolni razred z metodo avtentikacija() preveri podatke o uporabniku v podatkovni bazi Uporabnik. Če so podatki ustrezni, kontrolni razred kliče metodo prijava(), ki uporabnika Skrbnik prijavi v sistem. Ko je uporabnik Skrbnik uspešno prijavljen, se mu na zaslonski maski Srbnik s klicem metode prikažiSeznamŠtudentov() prikaže seznam študentov.

![Prijava Osnovni tok Študent](../img/diagrami_lp3/prijava_osnovni_tok_študent.png)

Slika prikazuje diagram zaporedja za osnovni tok pri povezavi med vlogo Študent in funkcionalnostjo Prijava. Na zaslonski maski se najprej z metodo prikažiPrijavniObrazec() prikaže obrazec za prijavo. Ko uporabnik Študent potrdi prijavo, se najprej z metodo preveriPrijavniObrazec() preveri polja na obrazcu nato se kliče metoda preveriPrijavnePodatke(). Kontrolni razred z metodo avtentikacija() preveri podatke o uporabniku v podatkovni bazi Uporabnik. Če so podatki ustrezni, kontrolni razred kliče metodo prijava(), ki uporabnika Študent prijavi v sistem. Ko je uporabnik Študent uspešno prijavljen, se mu na zaslonski maski Obveznosti s klicem metode prikažiPregledObveznosti() prikaže seznam njegovih obveznosti.
#
### Registracija
![Registracija izjemni tok 1](../img/diagrami_lp3/registracija_izjemni_tok_1.png)

Slika prikazuje diagram zaporedja za izjemni tok pri povezavi med neprijavljenim uporabnikom in funkcionalnostjo Registracija. Na zaslonski maski Uporabnik se najprej z metodo prikažiRegistracijskiObrazec() prikaže obrazec za registracijo. Ko uporabnik potrdi prijavo, se najprej z metodo preveriRegistracijskePodatke() preveri polja na obrazcu, nato se kliče metoda preveriRegistracijskePodatke(). Kontrolni razred z metodo preveriRegistracijskePodatke() preveri podatke o uporabnikih v podatkovni bazi Študent. Če uporabnik v bazi Študent že obstaja, se vrne obvestilo, ki se neprijavljenemu uporabniku prikaže z metodo prikažiObvestilo() na zaslonski maski Uporabnik.

![Registracija izjemni tok 2](../img/diagrami_lp3/registracija_izjemni_tok_2.png)

Slika prikazuje diagram zaporedja za izjemni tok pri povezavi med neprijavljenim uporabnikom in funkcionalnostjo Registracija. Na zaslonski maski Uporabnik se najprej z metodo prikažiRegistracijskiObrazec() prikaže obrazec za registracijo. Ko uporabnik potrdi prijavo, se najprej z metodo preveriRegistracijskePodatke() preveri polja na obrazcu. Če so podatki na obrazcu neveljavni, se vrne obvestilo, ki se neprijavljenemu uporabniku prikaže z metodo prikažiObvestilo() na zaslonski maski Uporabnik.

![Registracija odnosvni tok](../img/diagrami_lp3/registracija_osnovni_tok.png)

Slika prikazuje diagram zaporedja za osnovni tok pri povezavi med neprijavljenim uporabnikom in funkcionalnostjo Registracija. Na zaslonski maski Uporabnik se najprej z metodo prikažiRegistracijskiObrazec() prikaže obrazec za registracijo. Ko uporabnik potrdi prijavo, se najprej z metodo preveriRegistracijskePodatke() preveri polja na obrazcu, nato se kliče metoda preveriRegistracijskePodatke(). Kontrolni razred z metodo preveriRegistracijskePodatke() preveri podatke o uporabnikih v podatkovni bazi Študent. Če uporabnik še ne obstaja, se v podatkovni bazi Študent, z metodo ustvariŠtudenta(), ustvari novega uporabnika z podatki iz obrazca. Po uspešnemu dodajanju uporabnika, se mu prikaže obvestilo z metodo prikažiObvestilo() na zaslonski maski Uporabnik.
#
### Urejanje urnika
![Urejanje urnika izjemni tok študent vse tri alternative](../img/diagrami_lp3/urejanje_urnika_vse_alternative.png)

Slika prikazuje diagram zaporedja za izjemni tok pri povezavi med uporabnikom Študent in funkcionalnostjo Urejanje urnika. Na zaslonski maski Urniki se najprej z metodo prikažiUrnikZaUrejanje() prikaže urnik ki ga uporabnik Študent želi urejati. Če uporabnik Študent želi dodati novo aktivnost urnika, se mu z metodo prikažiObrazecZaNovoAktivnostUrnika() prikaže obrazec na zaslonski maski Urniki, kamor vnese podatke o novi aktivnosti. Ko uporabnik Študent potrdi podatke, se kliče metoda preveriVnosnoPoljeAktivnostiUrnika(), ki preveri podatke obrazca. Če so podatki neveljavni, se uporabniku Študent na zaslonski maski Urniki prikaže obvestilo z metodo prikažiobvestilo().

![Urejanje urnika osnovni tok študent alternativa 1](../img/diagrami_lp3/urejanje_urnika_alternative_1.png)

Slika prikazuje diagram zaporedja za osnovni tok pri povezavi med uporabnikom Študent in funkcionalnostjo Urejanje urnika (urejanje aktivnosti). Na zaslonski maski Urniki se najprej z metodo prikažiUrnikZaUrejanje() prikaže urnik ki ga uporabnik Študent želi urejati. Če uporabnik Študent želi urediti že obstoječo aktivnost urnika, se mu z metodo urediAktivnost() prikaže obrazec na zaslonski maski Urniki, kjer spremeni podatke o aktivnosti. Ko uporabnik Študent potrdi podatke, se kliče metoda preveriVnosnoPoljeAktivnostiUrnika(), ki preveri podatke obrazca. Če so podatki ustrezni, se kliče metoda shraniUrnik(). Kontrolni razred nato kliče metodo posodobiAktivnost(), ki v podatkovni bazi posodobi podatke o aktivnosti urnika. Po uspešnem urejanju aktivnosti urnika, se uporabniku Študent na zaslonski maski Urniki prikaže posodobljen urnik z metodo prikažiUrnike().

![Urejanje urnika osnovni tok študent alternativa 2](../img/diagrami_lp3/urejanje_urnika_alternative_2.png)

Slika prikazuje diagram zaporedja za osnovni tok pri povezavi med uporabnikom Študent in funkcionalnostjo Urejanje urnika (brisanje aktivnosti). Na zaslonski maski Urniki se najprej z metodo prikažiUrnikZaUrejanje() prikaže urnik ki ga uporabnik Študent želi urejati. Če uporabnik Študent želi izbrisati že obstoječo aktivnost urnika, se kliče metoda izbrišiAktivnost(). Nato se kliče metoda shraniUrnik(), ki v iz podatkovne baze z metodo izbrišiAktivnost() izbriše aktivnost urnika. Po uspešnem brisanju aktivnosti urnika, se uporabniku Študent na zaslonski maski Urniki prikaže posodobljen urnik z metodo prikažiUrnike().

![Urejanje urnika osnovni tok študent alternativa 3](../img/diagrami_lp3/urejanje_urnika_alternative_3.png)

Slika prikazuje diagram zaporedja za osnovni tok pri povezavi med uporabnikom Študent in funkcionalnostjo Urejanje urnika (dodajanje aktivnosti). Na zaslonski maski Urniki se najprej z metodo prikažiUrnikZaUrejanje() prikaže urnik ki ga uporabnik Študent želi urejati. Če uporabnik Študent želi dodati novo aktivnost urnika, se mu z metodo prikažiObrazecZaNovoAktivnosturnika() prikaže obrazec na zaslonski maski Urniki, kjer vnese podatke o novi aktivnosti. Ko uporabnik Študent potrdi podatke, se kliče metoda ustvariNovoAktivnost(), ki preveri podatke obrazca z metodo preveriObrazecZaNovoAktivnost(). Če so podatki ustrezni, se kliče metoda shraniUrnik(). Kontrolni razred nato kliče metodo ustvariAktivnost(), ki v podatkovni bazi doda podatke o novi aktivnosti urnika. Po uspešnem dodajanju aktivnosti urnika, kontrolni razred kliče metodo shraniUrnik(), ki shrani posodobljen urnik v podatkovno bazo Urnik. Ko se urnik uspešno shrani se uporabniku Študent na zaslonski maski Urniki prikaže posodobljen urnik z metodo prikažiUrnikzaUrejanje().
#
### Ustvarjanje novega sporočila
![Ustvarjanje novega sporočila izjemni tok](../img/diagrami_lp3/ustvarjanje_novega_sporočila_izjemni_tok.png)

Slika prikazuje diagram zaporedja za izjemni tok pri povezavi med uporabnikom Študent in funkcionalnostjo Ustvarjanje novega sporočila. Na zaslonski maski Podpora se najprej z metodo prikažiVnosnoPolje() prikaže polje za vnos sporočila, ki ga uporabnik Študent želi poslati. Ko uporabnik Študent potrdi sporočilo, se kliče metoda pošljiSporočilo(). Za preverjanje vnosnega polja sporočila, se kliče metoda preveriVnosnoPoljeSporočila(). Če je polje neustrezno izpolnjeno, se uporabniku Študent na zaslonski maski Podpora z metodo prikažiObvestilo() prikaže obvestilo o neustrezno izpolnjenem vnosnem polju.

![Ustvarjanje novega sporočila osnovni tok Podpora](../img/diagrami_lp3/ustvarjanje_novega_sporočila_osnovni_tok.png)

Slika prikazuje diagram zaporedja za osnovni tok pri povezavi med uporabnikom Podpora in funkcionalnostjo Ustvarjanje novega sporočila. Na zaslonski maski Podpora se najprej z metodo prikažiVnosnoPolje() prikaže polje za vnos sporočila, ki ga uporabnik Podpora želi poslati. Ko uporabnik Podpora potrdi sporočilo, se kliče metoda pošljiSporočilo(). Za preverjanje vnosnega polja sporočila, se kliče metoda preveriVnosnoPoljeSporočila(). Če je polje ustrezno izpolnjeno, kontrolni razred kliče metodo pošljiSporočilo(), ki po potrditvi vnese sporočilo v podatkovno bazo Sporočilo z metodo ustvariSporočilo(). Po vnosu v podatkovno bazo, se sporočilo še doda v pogovor z metodo posodobiPogovor() v podatkovno bazo Pogovor. Po uspešnem dodajanju v podatkovno bazo, se uporabniku Podpora na zaslonski maski Podpora prikaže celoten pogovor z metodo prikažiPogovorSŠtudentom().
![Ustvarjanje novega sporočila osnovni tok Študent](../img/diagrami_lp3/ustvarjanje_novega_sporočila_osnovni_tok_študent.png)

Slika prikazuje diagram zaporedja za osnovni tok pri povezavi med uporabnikom Študent in funkcionalnostjo Ustvarjanje novega sporočila. Na zaslonski maski Sporočila se najprej z metodo prikažiVnosnoPolje() prikaže polje za vnos sporočila, ki ga uporabnik Študent želi poslati. Ko uporabnik Podpora potrdi sporočilo, se kliče metoda pošljiSporočilo(). Za preverjanje vnosnega polja sporočila, se kliče metoda preveriVnosnoPoljeSporočila(). Če je polje ustrezno izpolnjeno, kontrolni razred kliče metodo pošljiSporočilo(), ki po potrditvi vnese sporočilo v podatkovno bazo Sporočilo z metodo ustvariSporočilo(). Po vnosu v podatkovno bazo, se sporočilo še doda v pogovor z metodo posodobiPogovor() v podatkovno bazo Pogovor. Po uspešnem dodajanju v podatkovno bazo, se uporabniku Študent na zaslonski maski Sporočila prikaže seznam sporočil z metodo prikažiSeznamSporočil().
#
### Ustvarjanje novega urnika
![Ustvarjanje novega urnika osnovni tok študent](../img/diagrami_lp3/ustvarjanje_novega_urnika_osnovni_tok.png)

Slika prikazuje diagram zaporedja za osnovni tok pri povezavi med uporabnikom Študent in funkcionalnostjo Ustvarjanje novega urnika. Z metodo vrniUrnike(), kontrolni razred vrne iz podatkovne baze vse urnike študenta. Če študent urnikov nima, se mu z metodo prikažiObvestilo() prikaže obvestilo o tem da urnika še nima. Nato se uporabniku Študent na zaslonski maski Urniki z metodo prikažiPrazenUrnikZaUrejanje() prikaže prazen urnik ki ga lahko uporabnik Študent uredi. Za urejanje urnika je postopek identičen kot pri funkcionalnosti Urejanje urnika.
#
### Vnos nove obveznosti
![vnos_nove_obveznosti_izjemni_tok](../img/diagrami_lp3/vnos_nove_obveznosti_izjemni_tok.png)

Slika prikazuje diagram zaporedja za izjemni tok pri povezavi med uporabnikom Študent in funkcionalnostjo Ustvarjanje nove obveznosti. Z metodo prikažiObrazecZaNovoObveznost() se uporabniku Študent na zaslonski maski Obveznosti prikaže obrazec za vnos podatkov o novi obveznosti. Ko uporabnik Študent potrdi podatke, se z metodo preveriObrazecZaNovoObveznost preveri podatke na obrazcu. Če so podatki neveljavni se prikaže uporabniku Študent obvestilo o neveljavnem obrazcu.

![vnos_nove_obveznosti_osnovni_tok](../img/diagrami_lp3/vnos_nove_obveznosti_osnovni_tok.png)

Slika prikazuje diagram zaporedja za osnovni tok pri povezavi med uporabnikom Študent in funkcionalnostjo Ustvarjanje nove obveznosti. Z metodo prikažiObrazecZaNovoObveznost() se uporabniku Študent na zaslonski maski Obveznosti prikaže obrazec za vnos podatkov o novi obveznosti. Ko uporabnik Študent potrdi podatke, se z metodo preveriObrazecZaNovoObveznost preveri podatke na obrazcu. Če so podatki veljavni, se kliče metoda ustvariNovoObveznost(). Kontrolni razred z metodo ustvariObveznost() v podatkovni bazi Obveznost doda novo obveznost. Po uspešnem dodajanju obveznosti, se uporabniku Študent na zaslonski maski Obveznosti z metodo prikažiPregledObveznosti() prikaže posodobljen seznam obveznosti.
