# Načrt sistema

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Martin Božič, Nik Hrovat, Aljaž Nunčič, Amadej Jankovič |
| **Kraj in datum** | Ljubljana, 20.4.2019 |



## Povzetek

Dokument predstavlja načrt rešitve za projekt z nazivom StraightAs. V 1. poglavju je predstavljen načrt arthitekture, kjer z diagrama arhitekture 
vidimo logično razdelitev aplikacije v tri nivoje, podprt pa je tudi razvojni vidik, saj so navedene tudi komponente posameznega nivoja in jih bomo kasneje tako lahko razdelili med posamezne razvijalce. V 2. poglavju je narejen
načrt strukture, kjer vidimo v točki 2.1 Razredni diagram projekta, v točki 2.2 je podroben opis razredov, ki nastopajo v rešitvi. Pri tem ima vsak razred zapisano svoje ime, atribute
in nesamoumevne metode. Pri tem so atributi razdeljeni še na ime atributa, njegov podatkovni tip, pomen in zalogo vrednosti, nesamoumevne metode pa na ime metode, imena in tipe parametrov, 
tip rezultata in pomen. Sledi 3. točka oz. poglavje, kjer je opisana vsaka funkcionalnost z načrtom obnašanja sistema.


## 1. Načrt arhitekture

![NacrtArhitekture](../img/diagrami-lp3/MVC_arhitektura.PNG)

[povezava do original slike](../img/diagrami-lp3/MVC_arhitektura.PNG)

## 2. Načrt strukture

### 2.1 Razredni diagram


![RazredniDiagram](../img/diagrami-lp3/KoncniRazredniDiagram2.png)

[povezava do original slike](../img/diagrami-lp3/KoncniRazredniDiagram2.png)


### 2.2 Opis razredov

*Opomba:
Profesor in animator sta v diagramu razredov za lazje razumevanje in v trenutni verziji aplikacije nimata nobenih dodatnih atributov in metod poleg tistih iz Registriranega uporabnika, zato se ta dva razreda ne bosta implementirala v zacetni verziji.


#### Registriran uporabnik

* Koncept iz problemske domene, ki ga razred predstavlja je registriran uporabnik.

##### Atributi


* ime atributa: 
    *    ID
* podatkovni tip atributa:
    *    int
* pomen atributa:
    *    identificira vsakega posameznega registriranega uporabnika
* zaloga vrednosti:
    *    interval naravnih števil
---------
* ime atributa: 
    *    ime
* podatkovni tip atributa:
    *    String
* pomen atributa:
    *    osebno ime registriranega uporabnika
* zaloga vrednosti:
    *    Črke abecede
---------
* ime atributa: 
    *    priimek
* podatkovni tip atributa:
    *    String
* pomen atributa:
    *    priimek registriranega uporabnika
* zaloga vrednosti:
    *    Črke abecede
---------
* ime atributa: 
    *    email
* podatkovni tip atributa:
    *    String
* pomen atributa:
    *    E-mail naslov uporabnika, s katerim se registrira in ga uporablja za prijavo v sistem
* zaloga vrednosti:
    *    `(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])`
    * vir: https://stackoverflow.com/a/201378
---------
* ime atributa: 
    *    geslo
* podatkovni tip atributa:
    *    String
* pomen atributa:
    *    Uporablja se za avtentikacijo posameznika ob prijavi v sistem, skupaj z njegovim E-mail naslovom.
* zaloga vrednosti:
    *    Alfanumerični znaki
---------
* ime atributa: 
    *    vloga
* podatkovni tip atributa:
    *    String
* pomen atributa:
    *    Vloga, ki je prirejena uporabniku, glede na njo pa ima na voljo različne funkcionalnosti sistema
* zaloga vrednosti:
    *    Zaloga vrednosti je en niz izmed slednjih: ["Študent", "Profesor", "Animator"]
--------
##### Nesamoumevne metode


* ime metode: 
    *    registrirajUporabnika
* imena in tipe parametrov:
    *    ime : String
    *    priiemk : String
    *    email : String
    *    geslo : String
    *    vloga : String
* tip rezultata:
    *    boolean
* pomen:
    *    Metoda opravi registracijo uporabnika v sistem
--------
* ime metode: 
    *    registrirajUporabnikaGoogle()
* imena in tipe parametrov:
    *    /
* tip rezultata:
    *    boolean
* pomen:
    *    Metoda opravi registracijo uporabnika v sistem z njegovim Google računom
--------
* ime metode: 
    *    prijava
* imena in tipe parametrov:
    *    email : String
    *    geslo : String
* tip rezultata:
    *    boolean
* pomen:
    *    Metoda opravi prijavo uporabnika v sistem
--------
* ime metode: 
    *    googlePrijava()
* imena in tipe parametrov:
    *    /
* tip rezultata:
    *    boolean
* pomen:
    *    Metoda opravi prijava uporabnika v sistem z njegovim Google računom
--------


#### Študent

* Koncept iz problemske domene, ki ga razred predstavlja je študent.

##### Atributi

* ime atributa: 
    *    naslovUrnika
* podatkovni tip atributa:
    *    String
* pomen atributa:
    *    označuje spletni naslov študentovega urnika
* zaloga vrednosti:
    *    Črke in znaki izbranega jezika
---------


##### Nesamoumevne metode

* ime metode: 
    *    vrniNaslovUrnika
* imena in tipe parametrov:
    *    /
* tip rezultata:
    *    String
* pomen:
    *    vrne spletni naslov urnika za določenega študenta
--------

#### Predmet

* Koncept iz problemske domene, ki ga razred predstavlja je predmet.

##### Atributi

* ime atributa: 
    *    id
* podatkovni tip atributa:
    *    int
* pomen atributa:
    *    identificira vsak posamezen predmet
* zaloga vrednosti:
    *    interval naravnih števil
---------
* ime atributa: 
    *    naslov
* podatkovni tip atributa:
    *    String
* pomen atributa:
    *    Označuje naslov predmeta v človeku razumljivem jeziku
* zaloga vrednosti:
    *    Črke abecede
---------
* ime atributa: 
    *    ciljnaSkupina
* podatkovni tip atributa:
    *    String
* pomen atributa:
    *    Označuje ciljno skupino predmeta 
* zaloga vrednosti:
    *    Črke abecede
---------
* ime atributa: 
    *    idLastnika
* podatkovni tip atributa:
    *    int
* pomen atributa:
    *    identificira nosilca predmeta (Profesorja, ki je predmet ustvaril)
* zaloga vrednosti:
    *    interval pozitivnih naravnih števil
---------
* ime atributa: 
    *    idstudentov
* podatkovni tip atributa:
    *    int[]
* pomen atributa:
    *    Polje id-jev, ki identificirajo študente prijavljene na predmet
* zaloga vrednosti:
    *    polje z vrednostmi na intervalu naravnih števil
--------


##### Nesamoumevne metode

* ime metode: 
    *    dodajPredmet
* imena in tipe parametrov:
    *    naslov : String
    *    ciljnaSkupina : String
    *    IdLastnika : int
* tip rezultata:
    *    boolean
* pomen:
    *    Metoda doda predmet  z imenom naslov na seznam uporabnikov, specificiranih s parametrom ciljna skupina in nosilcem predmeta, ki je referenciran z IdPredmeta
--------
* ime metode: 
    *    vrniPredmete
* imena in tipe parametrov:
    *    /
* tip rezultata:
    *    Predmet[]
* pomen:
    *   vrne polje vseh predmetov
--------
* ime metode: 
    *    vrniMojePredmete
* imena in tipe parametrov:
    *    idUporabnika
* tip rezultata:
    *    Predmet[]
* pomen:
    *   vrne polje vseh predmetov, na katere je prijavljen študent oz. je profesor lastnik
--------
* ime metode: 
    *    prijaviNaPredmet
* imena in tipe parametrov:
    *    IdPredmeta : int
    *    IdStudenta : int
* tip rezultata:
    *    boolean
* pomen:
    *   prijavi študenta z Idjem IdStudenta na predmet z idjem IdPredmeta
--------
#### Zasebna aktivnost

* Koncept iz problemske domene, ki ga razred predstavlja je zasebna aktivnost.

##### Atributi

* ime atributa: 
    *    naslov
* podatkovni tip atributa:
    *    String
* pomen atributa:
    *    Naslov aktivnosti, ki je uporabniku razumljiv
* zaloga vrednosti:
    *    Črke in znaki izbranega jezika
---------

* ime atributa: 
    *    prioriteta
* podatkovni tip atributa:
    *    int
* pomen atributa:
    *    Označuje kako pomembna je posamezna zasebna aktivnost, kar je še posebej uporabno pri prikazovanju in sortiranju zasebnih aktivnosti
* zaloga vrednosti:
    *    Interval pozitivnih naravnih števil med 1 in 10
---------

* ime atributa: 
    *    čas konca
* podatkovni tip atributa:
    *    Date
* pomen atributa:
    *    Označuje datum končanja zasebne aktivnosti
* zaloga vrednosti:
    *    veljavni datum, napisan po vzorcu: dan-mesec-leto
---------

* ime atributa: 
    *    idLastnika
* podatkovni tip atributa:
    *    int
* pomen atributa:
    *    Indentifikator lastnika (Študenta), ki je unikaten za vsakega lastnika
* zaloga vrednosti:
    *    pozitivna cela števila
---------


##### Nesamoumevne metode

* ime metode: 
    *    dodajZasebnoAktivnost
* imena in tipe parametrov:
    *    naslov : String
    *    prioriteta : int
    *    časkonca : Date
    *    IDlastnika : int
* tip rezultata:
    *    boolean
* pomen:
    *    Doda novo zasebno aktivnost uporabniku z indentifikatorjem IDlastnika
--------

* ime metode: 
    *    vrniZasebneAktivnosti
* imena in tipe parametrov:
    *    IDlastnika : int
* tip rezultata:
    *    List
* pomen:
    *    Vrne vse zasebne aktivnosti uporabnika z indentifikatorjem IDlastnika
--------

* ime metode: 
    *    vrniZasebneAktivnostiNaDan
* imena in tipe parametrov:
    *    datumkonca : Date
    *    IDlastnika : int
* tip rezultata:
    *    List
* pomen:
    *    Vrne vse tiste zasebne aktivnosti uporabnika z indentifikatorjem IDlastnika in tiste, ki imajo atribut časkonca enak parametru datumkonca
--------
 

#### Predmetna aktivnost

* Koncept iz problemske domene, ki ga razred predstavlja je predmetna aktivnost.

##### Atributi

* ime atributa: 
    *    naslov
* podatkovni tip atributa:
    *    String
* pomen atributa:
    *    Naslov predmetne aktivnosti, ki je uporabniku razumljiv
* zaloga vrednosti:
    *    Črke in znaki izbranega jezika
---------

* ime atributa: 
    *    datumkonca
* podatkovni tip atributa:
    *    Date
* pomen atributa:
    *    Označuje datum končanja predmetne aktivnosti
* zaloga vrednosti:
    *    veljavni datum, napisan po vzorcu: dan-mesec-leto
---------

* ime atributa: 
    *    ciljnaSkupina
* podatkovni tip atributa:
    *    String
* pomen atributa:
    *    Niz, ki označuje ciljno skupino uporabnikov, na katere se predmetna aktivnost nanaša
* zaloga vrednosti:
    *    Črke in znaki izbranega jezika
---------

* ime atributa: 
    *    idPredmeta
* podatkovni tip atributa:
    *    int
* pomen atributa:
    *    Indentifikator predmeta, ki je unikaten za vsak različen predmet
* zaloga vrednosti:
    *    pozitivna cela števila
---------


##### Nesamoumevne metode

* ime metode: 
    *    dodajPredmetnoAktivnost
* imena in tipe parametrov:
    *    naslov : String
    *    ciljnaSkupina : String
    *    datumKonca : Date
    *    idPredmeta : int
* tip rezultata:
    *    boolean
* pomen:
    *    Doda novo predmetno aktivnost predmetu z indentifikatorjem enakim idPredmeta
--------

* ime metode: 
    *    vrniPredmetneAktivnosti
* imena in tipe parametrov:
    *    IDpredmeta : int
* tip rezultata:
    *    List
* pomen:
    *    Vrne vse predmetne aktivnosti predmeta z indentifikatorjem enakim IDpredmeta
--------

* ime metode: 
    *    vrniPredmetneAktivnostiNaDan
* imena in tipe parametrov:
    *    datumkonca : Date
    *    IDpredmeta : int
* tip rezultata:
    *    List
* pomen:
    *    Vrne vse tiste predmetne aktivnosti predmeta z indentifikatorjem enakim IDpredmeta in tiste, ki imajo atribut datumkonca enak parametru datumkonca
--------
 

#### Družabni dogodek

* Koncept iz problemske domene, ki ga razred predstavlja je družabni dogodek.

##### Atributi

* ime atributa: 
    *    ID
* podatkovni tip atributa:
    *    int
* pomen atributa:
    *    ID družabnega dogodka
* zaloga vrednosti:
    *    Interval naravnih števil
---------

* ime atributa: 
    *    ime
* podatkovni tip atributa:
    *    String
* pomen atributa:
    *    Ime družabnega dogodka, ki je uporabniku razumljiv
* zaloga vrednosti:
    *    Črke in znaki izbranega jezika
---------

* ime atributa: 
    *    časizvedbe
* podatkovni tip atributa:
    *    Date
* pomen atributa:
    *    Označuje datum izvedbe družabnega dogodka
* zaloga vrednosti:
    *    Veljavni datum, napisan po vzorcu: dan-mesec-leto
---------

* ime atributa: 
    *    krajizvedbe
* podatkovni tip atributa:
    *    Naslov
* pomen atributa:
    *    Označuje kraj izvedbe družabnega dogodka
* zaloga vrednosti:
    *    Črke in znaki izbranega jezika
---------

* ime atributa: 
    *    opis
* podatkovni tip atributa:
    *    Text
* pomen atributa:
    *    Označuje podrobnejši opis družabnega dogodka
* zaloga vrednosti:
    *    Črke in znaki izbranega jezika
---------

* ime atributa: 
    *    IdStudentov
* podatkovni tip atributa:
    *    int[]
* pomen atributa:
    *    V njem so shranjeni vsi ID-ji študentov, ki pridejo na družabni dogodek
* zaloga vrednosti:
    *    Interval naravnih števil
---------

##### Nesamoumevne metode

* ime metode: 
    *    dodaDruzabniDogodek
* imena in tipe parametrov:
    *    ime : String
    *    opis : Text
    *    časizvedbe : Date
    *    krajizvedbe : Naslov
* tip rezultata:
    *    boolean
* pomen:
    *    Doda nov družabni dogodek
--------

* ime metode: 
    *    vrniPrijavljeneDruzabneDogodke
* imena in tipe parametrov:
    *    IdStudenta : int
* tip rezultata:
    *    List
* pomen:
    *    Vrne prijavljene družabne dogodke za posameznega študenta
--------

* ime metode: 
    *    vrniPrijavljeneDruzabneDogodkeNaDan
* imena in tipe parametrov:
    *    IdStudenta : int
    *    datumIzvedbe : Date
* tip rezultata:
    *    List
* pomen:
    *    Vrne vse tiste družabne dogodke uporabnika z indentifikatorjem IdStudenta, ki imajo atribut casIzvedbe enak parametru datumIzvedbe
--------

* ime metode: 
    *    vrniDruzabneDogodke
* imena in tipe parametrov:
    *    Ni parametrov
* tip rezultata:
    *    Dogodek
* pomen:
    *    Vrne družabne dogodke
--------

* ime metode: 
    *    prijaviNaDruzabniDogodek
* imena in tipe parametrov:
    *    IdDruzabniDogodek : int
    *    IdStudent : int
* tip rezultata:
    *    boolean
* pomen:
    *    Študent z IdStudent se prijavi na družabni dogodek z ID-jem IdDruzabniDogodek
--------
 

#### Predmetni forum

* Koncept iz problemske domene, ki ga razred predstavlja je predmetni forum.

##### Atributi

* ime atributa: 
    *    sprocila
* podatkovni tip atributa:
    *    txt[]
* pomen atributa:
    *    V njem so shranjena sporočila uporabnkov na forum
* zaloga vrednosti:
    *    Črke in znaki izbranega jezika
---------

* ime atributa: 
    *    IdAvtor
* podatkovni tip atributa:
    *    int
* pomen atributa:
    *    To je ID avtorja sporočila na forumu
* zaloga vrednosti:
    *    Interval naravnih števil
---------

* ime atributa: 
    *    IdPredmeta
* podatkovni tip atributa:
    *    int
* pomen atributa:
    *    To je ID prdmeta, o katerem se pogovarjajo na forumu
* zaloga vrednosti:
    *    Interval naravnih števil
---------


##### Nesamoumevne metode

* ime metode: 
    *    vrniForumPredmeta
* imena in tipe parametrov:
    *    IdPredmeta : int
* tip rezultata:
    *    Forum
* pomen:
    *    Vrne forum predmeta z ID-jem IdPredmeta
--------

* ime metode: 
    *    objaviSporocilo
* imena in tipe parametrov:
    *    sporocilo : Text
* tip rezultata:
    *    boolean
* pomen:
    *    Objavi sporočilo sporocilo na forum
--------
 


## 3. Načrt obnašanja

OPOMBA: vmesnik je zaslonska maska in pripadajoča krmilna logika

### Registracija

#### Diagram zaporedja za osnovni tok

![RegistracijaOsnovniTok](../img/diagrami-lp3/registracijaOT.png)

[povezava do original slike](../img/diagrami-lp3/registracijaOT.png)

#### Diagram zaporedja za alternativni tok 1

![RegistracijaAlternativniTok1](../img/diagrami-lp3/registracijaAT1.png)

[povezava do original slike](../img/diagrami-lp3/registracijaAT1.png)

#### Diagram zaporedja za alternativni tok 2 - izjemni tok

![RegistracijaAlternativniTok2](../img/diagrami-lp3/registracijaAT2.png)

[povezava do original slike](../img/diagrami-lp3/registracijaAT2.png)

#### Diagram zaporedja za alternativni tok 3 - izjemni tok

![RegistracijaAlternativniTok3](../img/diagrami-lp3/registracijaAT3.png)

[povezava do original slike](../img/diagrami-lp3/registracijaAT3.png)


### Prijava

#### Diagram zaporedja za osnovni tok

![PrijavaOsnovniTok](../img/diagrami-lp3/prijavaOT.png)

[povezava do original slike](../img/diagrami-lp3/prijavaOT.png)

#### Diagram zaporedja za alternativni tok 1 - izjemni tok

![PrijavaAlternativniTok1](../img/diagrami-lp3/prijavaAT1.png)

[povezava do original slike](../img/diagrami-lp3/prijavaAT1.png)

#### Diagram zaporedja za alternativni tok 2

![PrijavaAlternativniTok2](../img/diagrami-lp3/prijavaAT2.png)

[povezava do original slike](../img/diagrami-lp3/prijavaAT2.png)

#### Diagram zaporedja za alternativni tok 3 - izjemni tok

![PrijavaAlternativniTok3](../img/diagrami-lp3/prijavaAT3.png)

[povezava do original slike](../img/diagrami-lp3/prijavaAT3.png)

### Kreiranje zasebne aktivnosti

#### Diagram zaporedja za osnovni tok

![KreiranjeZasebneAktivnosti](../img/diagrami-lp3/kreiranjeZasebneAktivnostiOT.png)

[povezava do original slike](../img/diagrami-lp3/kreiranjeZasebneAktivnostiOT.png)

### Kreiranje predmeta

#### Diagram zaporedja za osnovni tok

![KreiranjePredmetneAktivnosti](../img/diagrami-lp3/kreiranjePredmetaOT.png)

[povezava do original slike](../img/diagrami-lp3/kreiranjePredmetaOT.png)

### Kreiranje predmetne aktivnosti

#### Diagram zaporedja za osnovni tok

![KreiranjePredmetneAktivnosti](../img/diagrami-lp3/kreiranjePredmetneAktivnostiOT.png)

[povezava do original slike](../img/diagrami-lp3/kreiranjePredmetneAktivnostiOT.png)

### Kreiranje Družabnega dogodka

#### Diagram zaporedja za osnovni tok

![KreiranjePredmetneAktivnosti](../img/diagrami-lp3/kreiranjeDružabnegaDogodkaOT.png)

[povezava do original slike](../img/diagrami-lp3/kreiranjeDružabnegaDogodkaOT.png)

### Pregled osebnih aktivnosti

#### Diagram zaporedja za osnovni tok

![KreiranjePredmetneAktivnosti](../img/diagrami-lp3/pregledOsebnihAktivnostiOT.png)

[povezava do original slike](../img/diagrami-lp3/pregledOsebnihAktivnostiOT.png)

#### Diagram zaporedja za alternativni tok 1

![PrijavaAlternativniTok2](../img/diagrami-lp3/pregledOsebnihAktivnostiAT1.png)

[povezava do original slike](../img/diagrami-lp3/pregledOsebnihAktivnostiAT1.png)

#### Diagram zaporedja za alternativni tok 2

![PrijavaAlternativniTok2](../img/diagrami-lp3/pregledOsebnihAktivnostiAT2.png)

[povezava do original slike](../img/diagrami-lp3/pregledOsebnihAktivnostiAT2.png)

### Prijava na predmet

#### Diagram zaporedja za osnovni tok

![KreiranjePredmetneAktivnosti](../img/diagrami-lp3/prijavaNaPredmetOT.png)

[povezava do original slike](../img/diagrami-lp3/prijavaNaPredmetOT.png)

#### Diagram zaporedja za alternativni tok 1

![PrijavaAlternativniTok2](../img/diagrami-lp3/prijavaNaPredmetAT1.png)

[povezava do original slike](../img/diagrami-lp3/prijavaNaPredmetAT1.png)

### Pregled študentskega urnika

#### Diagram zaporedja za osnovni tok

![pregledStudentskegaUrnika](../img/diagrami-lp3/pregledStudentskegaUrnikaOT.png)

[povezava do original slike](../img/diagrami-lp3/pregledStudentskegaUrnikaOT.png)

### Pregled predmetnega foruma

#### Diagram zaporedja za osnovni tok

![pregledPredmetnegaForuma](../img/diagrami-lp3/predmetniForumOT.png)

[povezava do original slike](../img/diagrami-lp3/predmetniForumOT.png)

### Pregled voznega reda LPP

#### Diagram zaporedja za osnovni tok

![pregledVoznegaRedaLpp](../img/diagrami-lp3/pregledVoznegaRedaLppOT.png)

[povezava do original slike](../img/diagrami-lp3/pregledVoznegaRedaLppOT.png)

### Prijava na druzabni dogodek

#### Diagram zaporedja za osnovni tok

![prijavaNaDruzabniDogodekOT](../img/diagrami-lp3/prijavaNaDruzabniDogodekOT.png)

[povezava do original slike](../img/diagrami-lp3/prijavaNaDruzabniDogodekOT.png)

#### Diagram zaporedja za alternativni tok 1

![prijavaNaDruzabniDogodekAT1](../img/diagrami-lp3/prijavaNaDruzabniDogodekAT1.png)

[povezava do original slike](../img/diagrami-lp3/prijavaNaDruzabniDogodekAT1.png)
