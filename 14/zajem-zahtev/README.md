# Dokument zahtev

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Nik Hrovat, Martin Božič, Aljaž Nunčič, Amadej Jankovič |
| **Kraj in datum** |  Ljubljana, 3.4.2019 |



## Povzetek projekta

Naša rešitev je namenjena vsem študentom, ki si želijo lažji in enostavnejši nadzor nad svojimi aktivnostmi in obveznostmi. Tako lahko vidijo svoje urnike, 
prihajajoče kolokvije, domače naloge, izpite in druge šolske obveznosti. Poskrbeli smo tudi za zabavo, saj lahko študentje vidijo tudi prihajajoče zabavne dogodke, 
kot so razne zabave, stand-up komedije, kulturne prireditve in ostale zanimivosti, s katerimi si lahko študentje krajšajo svoj prosti čas. Aplikacijo bodo 
uporabljali tudi profesorji, ki bodo lahko dodajali nove šolske obveznosti in animatorji, ki bodo skrbeli, da študentom ne bo dolgčas, saj jih lahko vabijo 
na številne zabavne dogodke.



## 1. Uvod

### Povzetek uvoda


Aplikacija bo reševala problem stresa študentov, saj bo na enem mestu prikazala in omogočala interakcijo z vsemi študentovimi obveznostmi. Spletna aplikacjia bo prilagojena tako za osebne
računalnike, tablice, kot tudi mobilne telefone. Študent bo tako imel na enem mestu vse svoje obveznosti tako študijske kot prostočasne. Obveznosti si bodo lahko razvrščali glede na prioriteto
ali datum končanja, prav tako pa bodo študentom delo lahko nalagali profesorji preko svojega uporabniškega računa. Za zmanjševanje stresa med študenti bodo poskrbeli animatorji, ki bodo organizirali družabne
dogodke in jih objavili preko naše spletne aplikacije.

### Tipi uporabnikov

* Študent
* Profesor
* Animator
* Neregistriran uporabnik


### Strnjene glavne funkcionalnosti

* Prijava (must have), 
* registracija (must have),
* kreiranje zasebne aktivnosti (must have),
* kreiranje predmeta (must have),
* kreiranje predmetne aktivnosti (must have),
* kreiranje družabnega dogodka (must have),
* pregled osebnih aktivnosti (must have),
* prijava na predmet (must have),
* prijava na družabni dogodek (must have)
* pregled študentskega urnika (should have),
* predmetni forum (could have),
* pregled voznega reda LPP (would have)

### Strnjen opis nefunkcionalnih zahtev

Aplikacija bo delovala po strogih etičnih načelih - ne bo zbirala odvečnih podatkov o uporabnikih, bo v skladu z GDPR zakonodajo in ZVOP-2 zakonom o varstvu osebnih podatkov, ki veljajta v Republiki Sloveniji.
Zgrajena bo v 3-nivojski arhitekturi s pomočjo MEAN sklada, delovati pa bo morala na vseh glavnih brskalnikih. Sposobna bo streči vsebino zadostnemu številu uporabnikom in bila bo dovolj hitra in odzivna, da bodo
uporabniki imeli prijetno uporabniško izkušnjo. Nedostopnost (nedelovanje) aplikacije mora biti čim manjša.

## 2. Uporabniške vloge


Študent - lahko kreira zasebne aktivnosti, pregleduje osebne aktivnosti, se prijavi na predmet in/ali družabni dogodek, lahko pregleduje urnik (should have),
sodeluje v predmetnem forumu (could have) in pregleduje vozni red LPP (would have).

Profesor - lahko ustvari predmet in predmetne aktivnosti, sodeluje v predmetnem forumu (could have) in pregleduje vozni red LPP (would have).

Animator - lako ustvari družabne dogodke in pregleduje vozni red LPP (would have).

Neregistriran uporabnik - lahko se registrira kot študent, profesor ali animator.


## 3. Slovar pojmov


1.  MEAN sklad - Skupek tehnologij za programiranje aplikacije: 
    * MongoDB je dokumentno usmerjena NoSQL podatkovna baza (MongoDB Inc. 2007),
    * Express je Node.js ogrodje za razvoj spletnih aplikacij in podpira tudi MVC arhitekturni slog (Node.js Foundation 2009a),
    * Angular je strukturirano ogrodje za razvoj dinamičnih spletnih aplikacij in spada v skupino MVW (Model-View-Whatever works for you) (Google 2010),
    * Node.js je zagonsko okolje, ki izvaja JavaScript kodo izven brskalnika (Node.js Foundation 2009b).
3. Uporabnik - Tisti, ki uporablja spletno stran aplikacije.
3. Registriran uporabnik - uporabnik, ki že izvede registracijo 
2. Študent - Tip uporabnika, ki lahko kreira zasebne aktivnosti, se prijavlja na predmet in družabne dogodke.
3. Profesor - Tip uporabnika, ki lahko kreira predmet in predmetne aktivnosti.
8. Animator - Tip uporabnika, ki lahko kreira družabne dogodke.
3. Aktivnost - Zajema osebne, zasebne, predmetne aktivnosti in družabne dogodke.
4. Osebna aktivnost - To so zasebne aktivnosti, predmetne aktivnosti prijavljenih predmetov in prijavljeni družabni dogodki.
5. Predmetna aktivnost - Vrsta aktivnosti, ki jih lahko ustvari profesor, vidne pa so tako profesorju kot študentu, ki je prijavljen na njo.
7. Zasebna aktivnost - Vrsta aktivnosti, ki jih lahko ustvari študent, vidne pa so samo njemu.
6. Družabni dogodek - Vrsta aktivnosti, ki jih lahko ustvari animator, vidne pa so drugim študentom.
5. Predmet - Ustvari ga profesor, vsebuje pa predmetne aktivnosti, ki so dodeljene študentom.
7. Prioriteta - Število, ki določa kako pomembna je aktivnost za uporabnika, zaloga vrednosti za prioriteto je med 1 in 10, kjer je 1 predstavlja najbolj pomembno in 10 najmanj pomembno aktivnost.
8. Prioriteta funkcionalnosti:
    * Must have: Gre za eno od glavnih funkcionalnosti sistema. Če jo izpustimo, bo sistem preveč okrnjen, da bi ga lahko smiselno uporabljali.
    * Should have: Funkcionalnost je pomembna, a ne neobhodno potrebna. Če jo izpustimo, bomo sistem še vedno lahko uporabljali, čeprav bo uporabniška izkušnja vsaj delno okrnjena.
    * Could have: Funkcionalnost je sicer zaželena, vendar pa ne bo pretirane škode, če jo izpustimo.
    * Would have (ali Won’t have this time): Zanimiva ideja, a je v tem trenutku ne bomo realizirali. Morda v naslednji izdaji.
9. LPP - Ljubljanski potniški promet




## 4. Diagram primerov uporabe

![Diagram primerov uporabe](../img/Diagram_primerov_uporabe.png)


## 5. Funkcionalne zahteve


### Registracija


#### Povzetek funkcionalnosti


Neregistriran uporabnik se lahko registrira. Pri tem mora v obrazec za registracijo, ki mu ga ponudi sistem, vpisati obvezne podatke, kot so: ime, priimek, email in geslo. Neregistriran
uporabnik mora tudi izbrati s katero uporabniško vlogo se bo prijavil.

#### Osnovni tok

1. Neregistriran uporabnik pride na spletno stran in izbere zavihek "Registracija"
2. Sistem mu prikaže možnost registracije z vnosnim obrazcom ali možnost registracije z google računom.
2. Obrazec ima obvezna polja Ime, Priimek, Email, Geslo in Uporabniška vloga.
3. Neregistriran uporabnik izbere vnosni obrazec.
3. Neregistriran uporabnik vnese podatke in izbere gumb "Registracija"
4. Sistem izvede registracijo Neregistriranega uporabnika in Neregistriran uporabnik postane Študent ali Profesor ali Animator.
5. Sistem novega uporabnika obvesti o uspešnem kreiranju računa.

#### Alternativni tokovi

##### Alternativni tok 1

1. Neregistriran uporabnik pride na spletno stran in izbere "Registracija"
2. Sistem mu prikaže možnost registracije z vnosnim obrazcom ali možnost registracije z google računom.
3. Neregistriran uporabnik izbere možnost registracije z google računom.
2. Sistem komunicira z googlovim zunanjim sistemom za avtentikacijo.
3. Sistem neregistriranemu uporabniku prikaže obrazec za dopolnitev dodatnih zahtevanih parametrov.
3. Neregistriran uporabnik vnese podatke in izbere gumb "Registracija"
5. Sistem novega uporabnika obvesti o uspešnem kreiranju računa.

##### Alternativni tok 2 - izjemni tok

1. Že registriran uporabnik pride na spletno stran in izbere zavihek "Registracija"
2. Sistem mu prikaže možnost registracije z vnosnim obrazcom ali možnost registracije z google računom.
3. Obrazec ima obvezna polja Ime, Priimek, Email, Geslo in Uporabniška vloga.
4. Registriran uporabnik izbere vnosni obrazec.
5. Registriran uporabnik vnese podatke in izbere gumb "Registracija"
6. Sistem že registriranega uporabnika obvesti o že ob obstoječem računu in ga preusmeri na prijavni zavihek.

##### Alternativni tok 3 - izjemni tok

1. Že registriran uporabnik pride na spletno stran in izbere "Registracija"
2. Sistem mu prikaže možnost registracije z vnosnim obrazcom ali možnost registracije z google računom.
3. Registriran uporabnik izbere možnost registracije z google računom.
2. Sistem komunicira z googlovim zunanjim sistemom za avtentikacijo.
3. Sistem registriranemu uporabniku prikaže obrazec za dopolnitev dodatnih zahtevanih parametrov.
3. Registriran uporabnik vnese podatke in izbere gumb "Registracija"
5. Sistem že registriranega uporabnika obvesti o že ob obstoječem računu in ga preusmeri na prijavni zavihek.

#### Pogoji


Pogoj, da se Uporabnik lahko registrira je, da ni že registriran z E-mail naslovom, ki ga namerava uporabiti za registracijo.

#### Posledice


Rezultat osnovnega toka funkcionalnosti je registriran uporabnik, ki mu je dodeljena ena izmed naštetih vlog: Študent, Profesor ali Animator.

#### Posebnosti

Realizacija funkcionalnosti po alternativnem toku 1 zahteva klic na zunanji vmesnik googlovih servis za avtentikacijo, za kasnejšo lažjo prijavo z enotnim googlovim računom.

#### Prioritete identificiranih funkcionalnosti


Funkcionalnost je prioriteta MUST have.

#### Sprejemni testi

##### Testira se osnovni tok

1. Neregistriran uporabnik klikne na zavihek registracija na spletni strani.
2. Sistem mu prikaže možnost registracije z vnosnim obrazcom ali možnost registracije z google računom.
3. Neregistriran uporabnik izbere vnosni obrazec.
4. V obvezna polja vpiše email "janez.novak@email.com", ime "Janez", priimek "Novak", geslo "ToJeNaj_mOc_NeJ_sE_gEsLo9876543210" in izbere uporabniško vlogo Študent.
5. Neregistrian uporabnik klikne na gumb "Registracija"
5. Sistem novega uporabnika obvesti o uspešnem kreiranju računa.
6. Neregistriran uporabnik postane registriran uorabnik - v tem primeru Študent z imenom Janez, priimkom Novak in geslom ToJeNaj_mOc_NeJ_sE_gEsLo9876543210 in emailom "janez.novak@email.com".

##### Testira se alternativni tok 1

1. Neregistriran uporabnik klikne na zavihek registracija na spletni strani. Uporabnik ima uporabniški račun pri Google z email naslovom mojster.miha@gmail.com in geslom "HIDDEN"
2. Sistem mu prikaže možnost registracije z vnosnim obrazcom ali možnost registracije z google računom.
3. Neregistriran uporabnik izbere možnost registracije z google računom.
4. Sistem komunicira z googlovim zunanjim sistemom za avtentikacijo. Uporabnik se prijavi v googlov račun z mojster.miha@gmail.com in geslom.
5. Uporabnik vnese dodatne zahtevane parametre ime in priimek - Miha in Mojster in uporabniško vlogo Študent.
5. Neregistrian uporabnik klikne na gumb "Registracija"
5. Sistem novega uporabnika obvesti o uspešnem kreiranju računa.
6. Neregistriran uporabnik postane registriran uorabnik - v tem primeru Študent z imenom Miha, priimkom Mojster in geslom ter emailom od google računa. Geslo se ne shrani v našo bazo podatkov.


##### Testira se alternativni tok 2

1. Že registriran uporabnik klikne na zavihek registracija na spletni strani. Uporabnik je z email naslovom "janez.novak@email.com" že registriran v aplikacijo.
2. Sistem mu prikaže možnost registracije z vnosnim obrazcom ali možnost registracije z google računom.
3. Registriran uporabnik izbere vnosni obrazec.
4. V obvezna polja vpiše email "janez.novak@email.com", ime "Janez", priimek "Novak", geslo "ToJeNaj_mOc_NeJ_sE_gEsLo9876543210" in izbere uporabniško vlogo Študent.
5. Registriran uporabnik klikne na gumb "Registracija"
6. Sistem že registriranega uporabnika obvesti o že ob obstoječem računu in ga preusmeri na prijavni zavihek.


##### Testira se alternativni tok 3

1. Že registriran uporabnik klikne na zavihek registracija na spletni strani. Uporabnik ima uporabniški račun pri Google z email naslovom mojster.miha@gmail.com in geslom "HIDDEN" in je že registriran v našo aplikacijo preko googlovega računa.
2. Sistem mu prikaže možnost registracije z vnosnim obrazcom ali možnost registracije z google računom.
3. Registriran uporabnik izbere možnost registracije z google računom.
4. Sistem komunicira z googlovim zunanjim sistemom za avtentikacijo. Uporabnik se prijavi v googlov račun z mojster.miha@gmail.com in geslom.
5. Registriran uporabnik vnese dodatne zahtevane parametre ime in priimek - Miha in Mojster in uporabniško vlogo Študent.
5. Registriran uporabnik klikne na gumb "Registracija"
5. Sistem že registriranega uporabnika obvesti o že ob obstoječem računu in ga preusmeri na prijavni zavihek.

´Testiranje za osnovni tok in alternativne tokove se ponovi še z vlogami profesor in animator, kjer se kreira ustrezna uporabniška vloga´



### Prijava


#### Povzetek funkcionalnosti

Študent, Profesor ali Animator se lahko prijavijo v sistem. Ob kliku na gumb prijava se jim prikaže obrazec za vnos E-mail naslov in gesla, ki so ga navedli ob registraciji.

#### Osnovni tok

1. Registriran študent/profesor/animator pride na zavihek "Prijava"
2. Sistem mu prikaže vnosni obrazec z obveznimi vnosnimi polji "Email naslov" in "Geslo" ter možnost prijave z google računom
3. Študent/profesor/animator vnese podatke in izbere gumb "Prijava"
4. Sistem izvede Prijavo registriranega uporabnika.
5. Sistem uporabnika obvesti o uspešni prijavi in ga preusmeri na osnovno stran prijavljenega uporabnika.


#### Alternativni tokovi

##### Alternativni tok 1 - izjemni tok

1. Neregistriran študent/profesor/animator pride na zavihek "Prijava" in se želi vpisati z email naslovom, ki še ni registriran
2. Sistem mu prikaže vnosni obrazec z obveznimi vnosnimi polji "Email naslov" in "Geslo" ter možnost prijave z google računom
3. Študent/profesor/animator vnese podatke in izbere gumb "Prijava"
4. Sistem uporabnika obvesti o neusprešni prijavi in ponovno prikaže prijavni obrazec

##### Alternativni tok 2 

1. Registriran študent/profesor/animator pride na zavihek "Prijava"
2. Sistem mu prikaže vnosni obrazec z obveznimi vnosnimi polji "Email naslov" in "Geslo" ter možnost prijave z google računom
3. Študent/profesor/animator izbere prijavo z google računom
4. Sistem uporabnika preusmeri na googlov prijavni obrazec
5. Študent/profesor/animator vnese email in geslo in izbere gumb "Prijava"
6. Sistem izvede Prijavo registriranega uporabnika.
7. Sistem uporabnika obvesti o uspešni prijavi in ga preusmeri na osnovno stran prijavljenega uporabnika.

##### Alternativni tok 3 - izjemni tok

1. Neregistriran študent/profesor/animator pride na zavihek "Prijava" in se želi vpisati z email naslovom, ki še ni registriran
2. Sistem mu prikaže vnosni obrazec z obveznimi vnosnimi polji "Email naslov" in "Geslo" ter možnost prijave z google računom
3. Študent/profesor/animator izbere prijavo z google računom
4. Sistem uporabnika preusmeri na googlov prijavni obrazec
5. Študent/profesor/animator vnese email in geslo in izbere gumb "Prijava"
6. Sistem uporabnika obvesti o neusprešni prijavi in ponovno prikaže prijavni obrazec


#### Pogoji

Pogoj za osnovni tok je registriran uporabnik, za alternativni tok pa še neregistriran uporabnik.

#### Posledice

Posledica osnovnega toka funkcionalnosti je prijavljen uporabnik.

#### Posebnosti

Funkcionalnost zahteva klic na googlov servis za avtentikacijo.


#### Prioritete identificiranih funkcionalnosti

Funkcionalnost je prioriteta MUST have.

#### Sprejemni testi

    
##### Testira se osnovni tok
    
1. Registriran študent/profesor/animator z email naslovom "leo.messi@gmail.com" in geslom "LaLiga" pride na zavihek "Prijava"
2. Sistem mu prikaže vnosni obrazec z obveznimi vnosnimi polji "Email naslov" in "Geslo" ter možnost prijave z google računom
3. Študent/profesor/animator vnese email "leo.messi@gmail.com" in geslo "LaLiga" in izbere gumb "Prijava"
4. Sistem izvede Prijavo registriranega uporabnika.
5. Sistem uporabnika obvesti o uspešni prijavi in ga preusmeri na osnovno stran prijavljenega uporabnika.
    
#### Alternativni tokovi
    
##### Testira se alternativni tok 1  

1. Neregistriran študent/profesor/animator pride na zavihek "Prijava" in se želi vpisati z email naslovom "leo.messi@gmail.com" in geslom "LaLiga", ki še ni registriran
2. Sistem mu prikaže vnosni obrazec z obveznimi vnosnimi polji "Email naslov" in "Geslo" ter možnost prijave z google računom
3. Študent/profesor/animator vnese email "leo.messi@gmail.com" in geslo "LaLiga" in izbere gumb "Prijava"
4. Sistem uporabnika obvesti o neusprešni prijavi in ponovno prikaže prijavni obrazec

##### Testira se alternativni tok 2 

1. Registriran študent/profesor/animator z googlovim email naslovom "leo.messi@gmail.com" in geslom "LaLiga" pride na zavihek "Prijava"
2. Sistem mu prikaže vnosni obrazec z obveznimi vnosnimi polji "Email naslov" in "Geslo" ter možnost prijave z google računom
3. Študent/profesor/animator izbere prijavo z google računom
4. Sistem uporabnika preusmeri na googlov prijavni obrazec
5. Študent/profesor/animator vnese email "leo.messi@gmail.com" in geslo "LaLiga" in izbere gumb "Prijava"
6. Sistem izvede Prijavo registriranega uporabnika.
7. Sistem uporabnika obvesti o uspešni prijavi in ga preusmeri na osnovno stran prijavljenega uporabnika.

##### Testira se alternativni tok 3  

1. Neregistriran študent/profesor/animator pride na zavihek "Prijava" in se želi vpisati z googlovim email naslovom "leo.messi@gmail.com" in geslom "LaLiga", ki še ni registriran
2. Sistem mu prikaže vnosni obrazec z obveznimi vnosnimi polji "Email naslov" in "Geslo" ter možnost prijave z google računom
3. Študent/profesor/animator izbere prijavo z google računom
4. Sistem uporabnika preusmeri na googlov prijavni obrazec
5. Študent/profesor/animator vnese email "leo.messi@gmail.com" in geslo "LaLiga" in izbere gumb "Prijava"
6. Sistem uporabnika obvesti o neusprešni prijavi in ponovno prikaže prijavni obrazec
    
    
### Kreiranje zasebne aktivnosti

#### Povzetek funkcionalnosti

Študent lahko kreira novo zasebno aktivnost. Pri tem določi naslov aktivnosti, prioriteto in čas do kdaj mora biti zasebna aktivnost končana.

#### Osnovni tok

1. Prijavljen študent izbere zavihek "Osebne aktivnosti".
2. Prijavljen študent klikne gumb "Kreiraj novo zasebno aktivnost".
3. Sistem mu prikaže vnosni obrazec, z obveznimi polji Naslov, Priorite, Čas konca
4. Študent vnese podatke in izbere Dodaj
5. Sistem doda novo zasebno aktivnost med obstoječe zasebne aktivnosti
6. Sistem obvesti uporabnika o uspešno dodani novi zasebni aktivnosti

#### Pogoji

Prijavljen uporabnik kot študent.

#### Posledice

Med obstoječe zasebne aktivnosti je dodana nova zasebna aktivnost.

#### Posebnosti

Realizacija funkcionalnosti ne zahteva posebnosti.

#### Prioritete identificiranih funkcionalnosti

Funkcionalnosti je prioriteta MUST have.

#### Sprejemni testi

##### Testira se osnovni tok

1. V aplikacijo je prijavljen študent.
2. Študent izbere zavihek "Osebne aktivnosti".
3. Študent klikne gumb "Kreiraj novo zasebno aktivnost".
4. Sistem mu prikaže vnosni obrazec, z obveznimi polji Naslov, Priorite, Čas konca
5. Študent vnese za Naslov "Pojdi v knjižnico", Priorito "3", Čas konca "16.4.2019 17:00" in izbere Dodaj
6. Sistem doda novo zasebno aktivnost med obstoječe zasebne aktivnosti
7. Sistem obvesti uporabnika o uspešno dodani novi zasebni aktivnosti
8. Med osebnimi aktivnostmi je vidna tudi nova zasebna aktivnost "Pojdi v knjižnico" s priorito "3" in časom konca "16.4.2019 17:00"


### Kreiranje predmeta


#### Povzetek funkcionalnosti

Profesor lahko kreira nov predmet. Pri tem določi naslov predmeta in komu je predmet namenjen (ciljna skupina).


#### Osnovni tok

1. Profesor izbere zavihek "Kreiraj nov predmet".
2. Sistem mu pokaže vnosni obrazec z obveznima poljema Naslov in Ciljna skupina.
3. Profesor vnese podatke in izbere "Dodaj".
4. Sistem doda nov predmet med obstoječe predmete.
5. Sistem obvesti profesorja o usprešno dodanem novem predmetu.


#### Pogoji

Uporabnik je prijavljen kot profesor.


#### Posledice

Med obstoječe predmete je dodan nov predmet.


#### Posebnosti

Funkcionalnost ne zahteva nobenih posebnosti.


#### Prioritete identificiranih funkcionalnosti

Funkcionalnost je prioriteta MUST have.


#### Sprejemni testi

##### Testira se osnovni tok

1. V aplikacijo je prijavljen profesor.
2. Profesor izbere zavihek "Kreiraj nov predmet".
3. Sistem mu pokaže vnosni obrazec z obveznima poljema Naslov in Ciljna skupina.
4. Profesor za Naslov vnese "RIS" in za Ciljno skupino "Poslušalci predmeta Razvoj informacijskih sistemov" in klikne "Dodaj".
5. Sistem doda nov predmet med obstoječe predmete.
6. Sistem obvesti profesorja o usprešno dodanem novem predmetu.
7. Med predmete je tudi nov predmet z Naslovom "RIS" in Ciljno skupino "Poslušalci predmeta Razvoj informacijskih sistemov".


### Kreiranje predmetne aktivnosti


#### Povzetek funkcionalnosti

Profesor lahko kreira novo predmetno aktivnost. Pri tem določi naslov aktivnosti, komu je namenjena in rok, do katerega mora biti predmetna aktivnost končana.


#### Osnovni tok

1. Profesor izbere "Kreiraj novo predmetno aktivnost".
2. Sistem mu pokaže vnosni obrazec z obveznimi polji Naslov, Ciljna skupina, Datum konca ter spustni seznam z njegovimi predmeti
3. Profesor vnese podatke in izbere gumb "Dodaj"
4. Sistem doda novo predmetno aktivnost med obstoječe predmetne aktivnosti
5. Sistem obvesti profesorja o uspešno dodani novi predmetni aktivnosti

#### Pogoji

Uporabnik prijavljen kot profesor, ki ima vsaj en predmet.

#### Posledice

Med obstoječe predmetne aktivnosti je dodana nova predmetna aktivnost, ki jo lahko vidijo vsi študenti prijavljeni v ta predmet.


#### Posebnosti

Ta funkcionalnost nima nobene posebnosti.


#### Prioritete identificiranih funkcionalnosti

Ta funkcionalnost je prioriteta MUST have.


#### Sprejemni testi

##### Testira se osnovni tok

1. V aplikacijo je prijavljen profesor, ki ima predmet "RIS"
1. Profesor izbere "Kreiraj novo predmetno aktivnost".
2. Sistem mu pokaže vnosni obrazec z obveznimi polji Naslov, Datum konca ter spustni seznam z njegovimi predmeti
3. Profesor vnese Naslov "Specifikacija zahtev", Datum konca "17.4.2019 17:00" ter na spustnem seznamu izbere predmet "RIS" in klikne gumb "Dodaj"
4. Sistem doda novo predmetno aktivnost "Specifikacija zahtev" med obstoječe predmetne aktivnosti
5. Sistem obvesti profesorja o uspešno dodani novi predmetni aktivnosti
6. Vsem študentom se med osebne aktivnosti doda predmetna aktivnost "Specifikacija zahtev"


### Kreiranje družabnega dogodka


#### Povzetek funkcionalnosti

Animator lahko kreira družabni dogodek, mu določi ime, opis, čas in kraj izvedbe. 


#### Osnovni tok

1. Animator klikne na zavihek "Kreiraj nov družabni dogodek".
2. Sistem mu prikaže vnosni obrazec za nov družabni dogodek.
3. V polja "Naslov" vpiše naslov dogodka, v polje "opis" doda opis, izbere čas in kraj izvedbe dogodka.
4. Animator klikne "Dodaj".
5. Sistem obvesti uporabnika o uspešnem kreiranju novega družabnega dogodka.


#### Pogoji

V aplikacijo mora biti prijavljen animator.

#### Posledice

Na seznam družabnih dogodkov se doda nov družabni dogodek.


#### Posebnosti

Pri tej funkcionalnosti ni nobenih posebnosti.

#### Prioritete identificiranih funkcionalnosti

Funkcionalnost je prioriteta MUST have.

#### Sprejemni testi

##### Testira se osnovni tok

1. V aplikacijo je prijavljen animator.
2. Animator klikne na zavihek "Kreiraj nov družabni dogodek".
3. Sistem mu prikaže vnosni obrazec za nov družabni dogodek.
4. V polja "Naslov" vpiše "Škisova tržnica", v polje "opis" doda opis "Škisova je kul", izbere čas "9.5.2019 12:00" in kraj izvedbe dogodka "Kardeljeva ploščad".
5. Animator klikne "Dodaj".
6. Sistem obvesti uporabnika o uspešnem kreiranju novega družabnega dogodka.
7. V bazi je shranjen nov družabni dogodek "Škisova tržnica"

### Pregled osebnih aktivnosti


#### Povzetek funkcionalnosti

Študent lahko pregleduje svoje osebne aktivnosti v tabeli, kjer jih lahko razvršča na podlagi datuma končanja ali prioritete. Vse aktivnosti so vidne tudi v koledarju.

#### Osnovni tok

1. Ko se uporabnik prijavi, se mu na osnovnem zaslonu prikaže seznam osebnih aktivnosti in koledar.
2. Uporabnik klikne na "prioriteta"
3. Seznam osebnih aktivnosti se razporedi po prioriteti od najpomembnejše do najmanj pomembne
 
#### Alternativni tokovi

##### Alternativni tok 1: 
1. Ko se uporabnik prijavi, se mu na osnovnem zaslonu prikaže seznam osebnih aktivnosti in koledar.
2. Uporabnik klikne na "datum končanja"
3. Seznam osebnih aktivnosti se razporedi glede na datum končanja od osebne aktivnosti, ki se zaključi najprej, do najkasneje.


##### Alternativni tok 2: 
1. Ko se uporabnik prijavi, se mu na osnovnem zaslonu prikaže seznam osebnih aktivnosti in koledar.
2. Uporabnik klikne na dan v koledarju
3. Prikažejo se mu osebne aktivnosti, ki so na ta dan, oziroma se na ta dan zaključijo.


#### Pogoji

Prijavljen je študent. Da se prikažejo osebne aktivnosti, jih mora imeti.

#### Posledice

Ta aktivnost nima izrecnih posledic.

#### Posebnosti

Ta funkcionalnost nima posebnosti.

#### Prioritete identificiranih funkcionalnosti

Funkcionalnost je prioriteta MUST have.

#### Sprejemni testi


##### Testira se osnovni tok

1. Prijavljen je študent z desetimi osebnimi aktivnostmi
2. Uporabnik klikne na "prioriteta"
3. Seznam desetih osebnih aktivnosti se razporedi po prioriteti od najpomembnejše do najmanj pomembne

##### Testira se alternativni tok 1

1. Prijavljen je študent z desetimi osebnimi aktivnostmi
2. Uporabnik klikne na "datum končanja"
3. Seznam desetih osebnih aktivnosti se razporedi glede na datum končanja od osebne aktivnosti, ki se zaključi najprej, do najkasneje.

##### Testira se alternativni tok 2

1. Prijavljen je študent z desetimi osebnimi aktivnostmi, študent ima osebno aktivnost "Oddaj RIS nalogo", ki se konča na dan 3.4.2019
2. Uporabnik klikne na dan 3.4.2019 v koledarju
3. Prikaže se mu osebna aktivnost, ki se ta dan zaključi ("Oddaj RIS nalogo").

### Prijava na predmet


#### Povzetek funkcionalnosti

Študent se lahko prijavi na predmet, ki ga je objavil profesor. S tem bo študent pridobival aktivnosti v povezavi s tem predmetom.

#### Osnovni tok

1. Študent klikne na zavihek "Moji predmeti".
2. Študent klikne gumb "Dodaj".
3. Sistem prikaže študentu seznam vseh predmetov, ki so jih objavili profesorji.
4. Študent med predmeti izbere predmet in klikne na gumb "Dodaj".
5. Sistem javi uporabniku uspešna prijava na predmet.

#### Alternativni tok - izjemni tok

1. Študent klikne na zavihek "Moji predmeti".
2. Študent klikne gumb "Dodaj".
3. Sistem prikaže študentu obvestilo, da ni na voljo noben predmet in da naj osebno kontaktira profesorja.

#### Pogoji

Prijavljen mora biti študent. Za osnovni tok morajo biti na voljo predmeti, pri alternativnem toku pa predmetov ni.

#### Posledice

Pri osnovnem toku, je študent na koncu prijavljen na predmet in pridobi predmetne aktivnosti v seznam osebnih aktivnosti.

#### Posebnosti

Pri tej funkcionalnosti ni nobenih posebnosti.

#### Prioritete identificiranih funkcionalnosti

Funkcionalnost je prioriteta MUST have.

#### Sprejemni testi

##### Testira se osnovni tok

1. Študent je prijavljen v aplikacijo. Med predmeti je vnešenih 10 predmetov, med njimi tudi "Diskretne strukture", s predmetno aktivnostjo "1. D.N"
2. Študent klikne na zavihek "Moji predmeti".
3. Študent klikne gumb "Dodaj".
4. Sistem prikaže študentu seznam desetih predmetov, ki so jih objavili profesorji.
5. Študent med predmeti izbere predmet "Diskretne strukture" in klikne na gumb "Dodaj".
6. Sistem javi uporabniku uspešna prijava na predmet.
7. V seznam študentovih predmetov se vpiše predmet "Diskretne strukure" in med osebne aktivnosti se mu vpiše predmetna aktivnost "1. D.N"

##### Testira se alternativni tok

1. Študent je prijavljen v aplikacijo. Med predmeti ni vnešenih predmetov.
2. Študent klikne na zavihek "Moji predmeti".
3. Študent klikne gumb "Dodaj".
4. Sistem prikaže študentu obvestilo, da ni na voljo noben predmet in da naj osebno kontaktira profesorja.

### Prijava na družabni dogodek


#### Povzetek funkcionalnosti

Študent se lahko prijavi na družabni dogodek, ki ga je objavil adnimator. Družabni dogodek se shrani med ostale osebne dogodke

#### Osnovni tok

1. Študent klikne na zavihek "Družabni dogodki".
2. Sistem prikaže študentu seznam vseh družabnih dogodkov, ki so jih objavili animatorji.
3. Študent med družabnimi dogodki izbere družabni dogodek in klikne na gumb "Prijavi se na družabni dogodek".
4. Sistem javi uporabniku uspešna prijava na družabni dogodek.

#### Alternativni tok - izjemni tok

1. Študent klikne na zavihek "Družabni dogodki".
2. Sistem prikaže študentu obvestilo, da ni na voljo noben družabni dogodek in da naj osebno kontaktira animatorja.

#### Pogoji

Prijavljen mora biti študent. Za osnovni tok morajo biti na voljo družabni dogodki, pri alternativnem toku pa družabnih dogodkov ni.

#### Posledice

Pri osnovnem toku, je študent na koncu prijavljen na družabni dogodek, ki se vpiše v seznam njegovih osebnih aktivnosti.

#### Posebnosti

Pri tej funkcionalnosti ni nobenih posebnosti.

#### Prioritete identificiranih funkcionalnosti

Funkcionalnost je prioriteta MUST have.

#### Sprejemni testi

##### Testira se osnovni tok

1. Študent je prijavljen v aplikacijo. Med družabnimi dogodki je vnešenih 10 družabnih dogodkov, med njimi tudi "Škisova tržnica".
2. Študent klikne na zavihek "Družabni dogodki".
3. Sistem prikaže študentu seznam desetih družabnih dogodkov, ki so jih objavili animatorji.
4. Študent med družabnimi dogodki izbere družabni dogodek "Škisova tržnica" in klikne na gumb "Prijavi se na družabni dogodek".
5. Sistem javi uporabniku uspešna prijava na družabni dogodek.
6. V seznam osebnih aktivnosti se mu vpiše družabni dogodek "Škisova tržnica".

##### Testira se alternativni tok

1. Študent je prijavljen v aplikacijo. Med družabnimi dogodki ni vnešenih družabnih dogodkov.
2. Študent klikne na zavihek "Družabni dogodki".
3. Sistem prikaže študentu obvestilo, da ni na voljo noben družabni dogodek in da naj osebno kontaktira animatorja.


### Pregled študentskega urnika


#### Povzetek funkcionalnosti

Študenti lahko pregledujejo svoje urnike.

#### Osnovni tok

1.	Prijavljen Študent pride na spletno stran in izbere zavihek „Moj urnik“
2.	Sistem mu prikaže novo stran z njegovim urnikom
3.	Sistem komunicira z zunanjim urniškim sistemom fakultet
4.	Sistem uporabniku prikaže njegov tedenski urnik

#### Pogoji

Prijavljen uporabnik kot Študent.


#### Posledice

Uporabniku se prikaže njegov urnik.


#### Posebnosti

Funkcionalnost “Pregled študentskega urnika” zahteva klic na zunanji urniški sistem fakultet.

#### Prioritete identificiranih funkcionalnosti

Prioriteta funkcionalnosti je SHOULD have.

#### Sprejemni testi

#### Testira se osnovni tok.

1.  Študent je prijavljen v aplikacijo in izbere „Moj urnik“
2.	Sistem mu prikaže novo stran z njegovim urnikom, na katerem so prikazana predavanja in vaje predmetov, na katere je prijavljen.
3.	Sistem komunicira z zunanjim urniškim sistemom fakultet.
4.  Študentu se prikaže urnik


### Predmetni forum


#### Povzetek funkcionalnosti

Profesor in Študent lahko uporabljajo forum predmeta v katerega so prijavljeni.

#### Osnovni tok

1.	Prijavljen Študent/Profesor pride na spletno stran in izbere zavihek  „Predmeti“
2.	Sistem mu prikaže seznam predmetov na katere je prijavljen
3.	Študent/Profesor izbere forum željenega predmeta
4.	Študent/Profesor na forumu objavi sporočilo
5.  Sistem obvesti študenta/profesorja o uspešno objavljenem sporočilu
6.	Prijavljeni študentje v predmet in profesor dobijo obvestilo, da je nekdo objavil na forumu predmeta


#### Pogoji

Prijavljen uporabnik kot Študent ali Profesor.


#### Posledice

Uporabniku objavi vprašanje v obliki sporočila na forum in dobi odgovor nanj.

#### Posebnosti

Funkcionalnost „Predmetni forum“ ne zahteva nobenih dodatnih posebnosti.

#### Prioritete identificiranih funkcionalnosti

Prioriteta funkcionalnosti je COULD have.

#### Sprejemni testi

##### Testira se osnovni tok.

1.	Prijavljen študent/profesor pride na spletno stran in izbere zavihek  „Predmeti“
2.	Sistem mu prikaže seznam 10 predmetov na katere je prijavljen
3.	Študent/Profesor izbere predmet Tehnologije Programske Opreme in odpre forum predmeta
4.	Študent/Profesor na forumu zastavi vprašanje: „Skupina 06 nam je ponudila 12 točk za 3 toaste vsakemu članu skupine. Medtem v nekaterih skupinah protestirajo, da takšen način politike ni vredu. Zanima me, če lahko sprejmem ponudbo brez posledic koruptivne komisije in če morda kdo ponuja več?“ (Vse ali nič) 
5.	Študent/Profesor pritinse gumb objavi in sporočilo se objavi. 
6.  Sistem obvesti študenta/profesorja o uspešno objavljenem sporočilu
7.	Prijavljeni študentje v predmet in profesor dobijo obvestilo, da je nekdo objavil na forumu predmeta
8.	Objavljena vprašanja in odgovori so vidni vsem, ki so prijavljeni v predmet


### Pregled voznega reda LPP


#### Povzetek funkcionalnosti


Študent, Profesor in Animator lahko pregledujejo vozne rede avtobusovv Ljubljani, ki vozijo v okviru Ljubljanskega potniškega prometa.

#### Osnovni tok

1.	Prijavljen Študent/Profesor/Animator pride na spletno stran in izbere zavihek  „LPP“
2.	Sistem mu prikaže polje za vnos imena avtobusne postaje
3.	Študent vnese ime avtobusne postaje in izbere prikaži
4.	Sistem komunicira z zunanjim vmesnikom sistema družbe LPP
5.	Sistem uporabniku prikaže vozne rede v naslednjih nekaj urah


#### Pogoji

Prijavljen uporabnik kot Študent/Profesor/Animator.


#### Posledice

Uporabniku se prikaže vozni red postaje, ki jo je vnesel.


#### Posebnosti

Funkcionalnost “Pregled voznega reda LPP” zahteva klic na zunanji vmesnik družbe LPP za prikaz voznih redov.

#### Prioritete identificiranih funkcionalnosti

Prioriteta funkcionalnosti je WOULD have.

#### Sprejemni testi

#### Testira se osnovni tok.

1.	Študent/Profesor/Animator je prijavljen v aplikacijo. Ob 14:00 izbere zavihek „LPP“
2.	Sistem mu prikaže polje za vnos imena avtobusne postaje
3.	Študent/Profesor/Animator vnese ime postaje „Pošta“
4.	Sistem komunicira z zunanjim vmesnikom sistema družbe LPP
5.	Sistem mu prikaže odhode avtobusov v naslednji uri , v obliki: 14:05 - 6, 14:50 – 2, 14:55 - 18


## 6. Nefunkcionalne zahteve


* ORGANIZACIJSKE:

    1. Razvojna : Spletna aplikacija mora biti razvita s pomočjo uporabe MEAN sklada.
    2. Razvojna : Podatkovna baza mora biti postavljena na mLab, preostanek aplikacije pa deployan na heroku

* ZUNANJE:

    3. Etične : Spletna aplikacija od uporabnikov zbira samo nujno potrebne podatke za delovanje sistema. (Prijavne podatke uporabnika in podatke o aktivnostih, predmetih)
    4. Zakonodajne: Spletna aplikacija mora biti implementirana skladno z zakonodajo GDPR.

* ZAHTEVE IZDELKA:

    5. Zasebne aktivnosti so lahko vidne samo uporabniku, ki je zasebno aktivnost ustvaril.
    6. Sistem mora biti zmožen streči najmanj 1000 hkratnim uporabnikom.
    7. Sistem uporabniku ne sme omogočiti dostopa do podatkov, za katere ni izrecno pooblaščen
    8. Sistem mora delovati na vseh glavnih brskalnikih - Chrome, Firefox, Safari, Edge
    9. Sistem mora biti dostopen 99,9% časa

## 7. Prototipi vmesnikov

### Zaslonske maske

Uporabnik se lahko registrira preko vnosnega obrazca:

![registracija-navadna](../img/Wireframes/Registracija-navadna.PNG)

ali pa z računom google:
![registracija-google](../img/Wireframes/registracija-google.PNG)

Uporabnik se lahko prijavi preko vnosnega obrazca:
![prijava](../img/Wireframes/prijava.PNG)

ali pa z računom google:
![prijava_google](../img/Wireframes/prijava_google.PNG)

Profesor lahko kreira predmet:
![profesor-kreiranje-predmeta](../img/Wireframes/profesor-kreiranje-predmeta.PNG)

Profesor lahko kreira predmetno aktivnost:
![profesor-kreiranje-predmetne-aktivnosti](../img/Wireframes/profesor-kreiranje-predmetne-aktivnosti.PNG)

Profesor lahko uporablja forum:
![profesor-forum](../img/Wireframes/profesor-forum.PNG)

Študent lahko kreira zasebno aktivnost:
![student-kreiraj-zasebno-aktivnost](../img/Wireframes/student-kreiraj-zasebno-aktivnost.PNG)

Študent lahko pregleduje osebne aktivnosti:
![student-pregled-osebnih-aktivnosti](../img/Wireframes/student-pregled-osebnih-aktivnosti.PNG)

Študent se lahko prijavi na predmet:
![student-prijava-na-predmet](../img/Wireframes/student-prijava-na-predmet.PNG)

Študent se lahko prijavi na družabni dogodek:
![student-prijava-na-druzabni-dogodek](../img/Wireframes/student-prijava-na-druzabni-dogodek.PNG)

Animator lahko kreira družabni dogodek:
![animator-kreiranje-družabnega-dogodka](../img/Wireframes/animator-kreiranje-družabnega-dogodka.PNG)

Študent lahko uporablja forum:
![student-forum](../img/Wireframes/student-forum.PNG)

Študent lahko uporablja LPP:
![student-lpp](../img/Wireframes/student-lpp.PNG)

Študent lahko pregleduje urnik:
![student-urnik](../img/Wireframes/student-urnik.PNG)


### Sistemski vmesniki

#### Google servis za avtentikacijo

Ta servis bomo uporabili za lažjo registracijo ter nadalnje prijave v našo spletno aplikacijo. Pri tem se uporabi api klic na Google servis.

Pri klicu na Googlov servis za avtentikacijo, bo ta vrnil:

* ID - identifikator uporabnika
* Full Name - polno ime uporabnika
* Given Name - ime uporabnika
* Family Name - priimek uporabnika
* Image URL - spletni naslov do uporabnikove slike
* Email - email naslov uporabnika

Več informacij z razlago vseh pridobljenih polj je na spletni strani : https://developers.google.com/identity/sign-in/web/

#### LPP vozni redi

Za seznam prihodov avtobusov na izbrano avtobusno postajališče bomo na spletni naslov http://data.lpp.si/timetables/liveBusArrival poslali get zahtevek
z station_int_id (slovar med imenom postajališča in station_int_id bo shranjen diraktno v aplikaciji ali pa pridobljen z get zahtevkom na http://data.lpp.si/stations/getAllStations).

Pomembni podatki pri pridobivanju informacij o prihodih avtobusov na postajališča:

* route_number - številka linije
* route_name - ime linije
* eta - šas do prihoda avtobusa v minutah 
* generated - časovna oznaka, kdaj smo pridobili podatke

Več informacij z razlago vseh pridobljenih polj je na spletni strani : http://jozefstefaninstitute.github.io/LPPServer/

#### Urniški sistem fakultet

Tukaj bi do podatkov za urnik dostopali preko URL naslova, ki bi ga vnesel uporabnik ter nato pridobili podatke iz same spletne strani ali pa bi za pridobivanje informacij o urniku pridobili API-je posameznih fakultet.
Fakulteta za računalništvo in informatiko žal nima javno dostopnega API-ja za urnik.

Pri tem bi pridobili informacije o predmetu, izvajalcu in kraju (učilnici) izvajanja predmeta.

