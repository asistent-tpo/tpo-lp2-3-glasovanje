# Načrt sistema

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Filip Zupančič, Alan Hadžić, Peter Bohanec in Tom Gornik |
| **Kraj in datum** | Ljubljana, 19.4.2019 |



## Povzetek

Dokument vsebuje načrt sistema za aplikacijo StraightAs. Dokument je razdeljen na tri dele. V prvem delu je opredeljen načrt arhitekture, ki je narejen po vzorcu Model-krmilnik-pogled. Predstavljena sta dva pogleda na arhitekturo, logični in razvojni. V nadaljevanju je predstavljen načrt strukture sistema. Načrt strukture je predstavljen z razrednim diagramom, kateremu sledi opis vseh uporabljenih razredov, torej njihovih atributov in metod. Razredi so razdeljeni v tri skupine: kontrolni razredi (oznaka K), poslovni razredi in mejni razredi (oznaki ZM in SV). Pri opisu metod je izpuščen opis nekaterih samoumevnih razrednih metod, kot so metode getter in setter. Zadnji del dokumenta obsega diagrame zaporedja za vsak tok dogodkov vseh funkcionalnih zahtev, ki so opredeljene v dokumentu zajema zahtev.




## 1. Načrt arhitekture

![mvc](../img/MVC.PNG)

* MVC diagram, ki naše razrede organizira v tri smislene celine: Pogledi, Krmilniki in Modeli. 
Pogledi so naši mejni (angl. boundry) razredi preko kateih komuniciramo z sistemom.
Krmilniki so razredi, ki skrbijo za izvajanje poslovne logike sistema.
Modeli so razredi, ki skrbijo za obvladovanje podatkov.  

![logicni](../img/LogicniPogled.PNG)

* Razvojni pogled na sistem prikazuje diagram optimalnih rasporeditev razredov za implementacijo med razvijalcima.

## 2. Načrt strukture

### 2.1 Razredni diagram

![logicni](../img/razredni_diagram.PNG)

* Razredni diagram prikazuje odnose in razmerja med razredima sistema.

### 2.2 Opis razredov

### ZM_VstopnaStran

Mejni (boundary) razred ZM_VstopnaStran predstvajla prvo stran aplikacije. Uporabnik tu se lahko tu prijavi ali pa se preusmeri v registracijo. Implementira funkcionalnost Prijava.

#### Atributi

* vnosnoPolje_email: String (input_text)
    * Gre za vnosno polje v katerega uporabnik vpiše svoj email, ki služi kot identifikator.
	
* vnosnoPolje_geslo: String (input_text)
    * Gre za vnosno polje v katerega uporabnik vpiše geslo za vstop v svoj profil.

#### Nesamoumevne metode

* prijava(email, geslo): void
    * Začne prijavo uporabnika v sistem. Email in geslo pošlje v bazo za preverjanje.

* registracija(): void
    * Preusmeri na nov spletni naslov, kjer lahko novi uporabnik izvede registracijo v sistem.
	
* ponastaviPolja(): void
    * Zbriše vsebino v poljih za vpis emaila in gesla.

	
### ZM_Registracija

Mejni (boundary) razred ZM_Registracija predstvajla stran aplikacije na kateri se lahko novi uporabnik. Implementira funkcionalnost Registracija.

#### Atributi

* vnosnoPolje_ime: String (input_text)
    * Gre za vnosno polje v katerega uporabnik vpiše svoje ime. 
	
* vnosnoPolje_priimek: String (input_text)
    * Gre za vnosno polje v katerega uporabnik vpiše svoj priimek.

* vnosnoPolje_email: String (input_text)
    * Gre za vnosno polje v katerega uporabnik vpiše svoj email, ki služi kot identifikator.
	
* vnosnoPolje_geslo: String (input_text)
    * Gre za vnosno polje v katerega uporabnik vpiše svoje geslo.

* vnosnoPolje_potrditevGesla: String (input_text)
    * Ponoven vpis gesla. Namen je preprečiti pomoten vpis gesla. Vnosno polje.
	
* vnosnoPolje_organizacija: String (input_text)
    * Gre za vnosno polje v katerega uporabnik vpiše del katere organizacije je.
	
* izborMoznosti_vloga : boolean (select)
    * Izbor vloge uporavnika v sistemu. Izbira med študentom, pedakoškim delavcem in zunanjim akterjem.

#### Nesamoumevne metode

* prekici(): void
    * Preusmeri na vstopno stran.

* registracija(ime, priimek, email, geslo, potrditvenoGeslo, organizacija, vloga): void
    * Pošlji podatke o novem uporabniku v bazo, kjer se bo izdelal nov uporanik.
	
* ponastaviPolja(): void
    * Zbriše vsebino v poljih za vpis podatkov.
    
### ZM_Nabiralnik

Mejni (boundary) razred ZM_Nabiralnik implementira logiko zaslonske maske za funkcionalnost Pregled nabiralnika in urejanje TODO seznama.

#### Atributi

* vnosnoPolje_iskalnik: String (input_text)
    * Gre za vnosno polje iskalnika po nabiralniku. Vsebina vnosnega polja se uporabi za iskanje.

#### Nesamoumevne metode

* prikaziPodrobnosti(long id): void
    * Metoda je implementirana s klikom na ime kateregakoli dogodka na zaslonski maski. Kot parameter se uporabi ime dogodka, ki se je kliknil.

* dodaj(long id): void
    * Metoda je implementirana z gumbom "dodaj" ob vsakem dogodku v nabiralniku. Klik na gumb sproži dodajanje dogodka na TODO seznam.

* brisiIzNabiralnika(long id): void
    * Metoda je implementirana z gumbom "X" ob vsakem dogodku v nabiralniku. Klik na gumb sproži brisanje dogodka iz nabiralnika.

* brisiIzTODO(long id): void
    * Metoda je implementirana z gumbom "X" ob vsakem dogodku na TODO seznamu. Klik na gumb sproži brisanje dogodka s TDOO seznama.


### ZM_PodrobnostiDogodka

Razred ZM_PodrobnostiDogodka je mejni (boundary) razred. Predstavlja zaslonsko masko za funkcionalnost Pregled podrobnosti dogodka.

#### Atributi

* vnosnoPolje_povabila: String(text_view)
    * Vnosno polje za vnašanje uporabnikov za povabila.

* vnosnoPolje_komentar: String(text_view)
    * Vnosno polje za vnašanje novega komentarja na dogodek.

#### Nesamoumevne metode

* prijavi(): void
* 
    * Metoda je implementirana z gumbom "Prijavi". Klik na gumb sproži postopek prijave spornega dogodka.

* povabi(): void
    * Metoda je implementirana z gumbom "Povabi prijatelja". Klik na gumb sproži logiko za povabilo prijatelja na dogodek.

* objaviKomentar(): void
    * Metoda je implementirana z gumbom "Dodaj komentar". Klik na gumb sproži logiko za objavo komentarja.

### ZM_UpravljanjeDogodkovUporabnikovInOznak

Razred ZM_upravljanjeDogodkovUporabnikovInOznak je mejni (boundary) razred. Predstavlja zaslonsko masko za funkcionalnost Upravljanje z dogodki in uporabniki. Namenjena se samo moderatorju.

#### Atributi

* vnosnoPolje_iskalnikOznak: String(text_view)
    * Gre za vnosno polje iskalnika po seznamu oznak. Vsebina vnosnega polja se uporabi za iskanje oznak.

* vnosnoPolje_iskalnikUporabnikov: String(text_view)
    * Gre za vnosno polje iskalnika po seznamu uporabnikov. Vsebina vnosnega polja se uporabi za iskanje uporabnikov.
	
* vnosnoPolje_iskalnikDogodkov: String(text_view)
    * Gre za vnosno polje iskalnika po seznamu dogodkov. Vsebina vnosnega polja se uporabi za iskanje dogodkov.

#### Nesamoumevne metode
* pridobiPodrobnostiODogodku(long id): void
    * Metoda je implementirana s klikom na ime kateregakoli dogodka na zaslonski maski. Kot parameter se uporabi dogodek, ki se je kliknil.

* potrditevUporabnika(long id): void
    * Metoda je implementirana z gumbom "POTRDI" ob vsakem nepotrjenem uporabniku v seznamu uporabnikov. Kot parameter se uporabi uporabnik, ki se je kliknil.
	
* brisiUporabnika(long id): void
    * Metoda je implementirana z gumbom "X" ob vsakem uporabniku v seznamu uporabnikov. Klik na gumb sproži brisanje uporabnika iz seznama uporabnikov.

* brisiOznake(long id): void
    * Metoda je implementirana z gumbom "X" ob vsaki oznaki v seznamu oznak. Klik na gumb sproži brisanje oznake iz seznama oznak.

* brisiDogodka(long id): void
    * Metoda je implementirana z gumbom "X" ob vsakem dogodku v seznamu dogodkov. Klik na gumb sproži brisanje dogodka iz seznama dogodkov.

### ZM_UstvariDogodek

Mejni (boundary) razred ZM_UstvariDogodek implementira logiko zaslonske maske za funkcionalnost ustvarjanje dogodka.

#### Atributi

* idUporabnika: long
	* id uporabnika ki želi ustvariti dogodek.

* ime: String (input_text)
    * Vnosno polje za ime dogodka.
	
* opis: String (input_text)
    * Vnosno polje za opis dogodka.
	
* oznake: String (input_text)
    * Vnosno polje za oznake dogodka.
	
* lokacija: String (input_text)
    * Vnosno polje za lokacijo dogodka.
	
* od: Datum (input_text)
    * Vnosno polje za datum in čas začetka dogodka.

* do: Datum (input_text)
	* Vnosno polje za datum in čas konca dogodka.

* prioriteta: Boolean (check_box)
	* Check box za določitev prioritete (true/false).
	
#### Nesamoumevne metode

* dodajDogodek(long idUporabnika, String ime, String opis, String oznake, String lokacija, Date od, Date do, boolean prioriteta): void
    * Metoda je implementirana z gumbom "Ustvari". Kot parameter se uporabi vsebina vnosnih polj, ki jih sprejme uporabnik.

* prekličiUrejanje(): void
    * Metoda je implementirana z gumbom "Prekliči". Klik na gumb sproži, da se urejanje oz. ustvarjanje prekine in zaslonska maska zapre.

### ZM_UrediDogodek

Mejni (boundary) razred ZM_UrediDogodek implementira logiko zaslonske maske za funkcionalnost urejanje dogodka.

#### Atributi

* idUporabnika: long
	* id uporabnika ki želi ustvariti dogodek.
	
* idDogodka: long
	* id dogodka.

* ime: String (input_text)
    * Vnosno polje za ime dogodka.
	
* opis: String (input_text)
    * Vnosno polje za opis dogodka.
	
* oznake: String (input_text)
    * Vnosno polje za oznake dogodka.
	
* lokacija: String (input_text)
    * Vnosno polje za lokacijo dogodka.
	
* od: Datum (input_text)
    * Vnosno polje za datum in čas začetka dogodka.

* do: Datum (input_text)
	* Vnosno polje za datum in čas konca dogodka.

* prioriteta: Boolean (check_box)
	* Check box za določitev prioritete (true/false).
	
#### Nesamoumevne metode

* posodobiDogodek(long idUporabnika, long idDogodka, String ime, String opis, String oznake, String lokacija, Date od, Date do, boolean prioriteta): void
    * Metoda je implementirana z gumbom "Posodobi". Kot parameter se uporabi vsebina vnosnih polj, ki jih sprejme uporabnik.

* prekličiUrejanje(): void
    * Metoda je implementirana z gumbom "Prekliči". Klik na gumb sproži, da se urejanje oz. ustvarjanje prekine in zaslonska maska zapre.

* pridobiPodatkeODogodku(long idDogodka): Objekt
    * Metoda pridobi podatke o dogodku iz baze.	

### ZM_PregledUstvarjenihDogodkov

Mejni (boundary) razred ZM_Pregled_ustvarjenih_dogodkov implementira logiko zaslonske maske za funkcionalnost pregleda ustvarjenih dogodkov, ki ga koristijo pedagoški in zunanji sodelavci.

#### Atributi
	
#### Nesamoumevne metode

* urediDogodek(long id): void
    * Metoda je implementirana z gumbom "UREDI". Klik na gumb odpre ZM_Ustvari_uredi_dogodek. Parameter metode je id dogodka, s katerim se dogodek lahko identificira.

* ustvariDogodek(): void 
    * Metoda je implementirana z gumbom "USTVARI". Klik na gumb odpre ZM_Ustvari_uredi_dogodek.
	
* izbrišiDogodek(long id): void
    * Metoda je implementirana z gumbom "X". Klik na gumb sproži, da se dogodek izbriše iz baze. Parameter metode je id dogodka, s katerim se dogodek lahko identificira.

* pridobiDogodke(long idUporabnika): seznam Objektov(ime, oznake, idDogodka)
    * Metoda vrne osnovne podatke o vseh dogodkih, ki jih je uporabnik ustvaril.	
	
### ZM_Oznaka

Razred, ki po prikazoval seznam vseh oznak in seznam prijavljenih oznak.

#### Atributi

* email: string
    * email uporabnika
* vseOznake: Oznaka[]
    * seznam vseh oznak
* uporabniskeOznake: Oznaka[]
    * seznam oznak na katerih je uporabnik prijavljen

#### Nesamoumevne metode

* prikaziOznake(email): void
    * prikazi vse oznake in oznake uporabnika
* dodajOznako(email, oznaka): boolean
    * prijavi uporabnika na oznako
* odstraniOznako(email, oznaka): boolean
    * odstrani uporabniško prijavo na oznako

### ZM_Odjava

Razred, ki bo prikazoval koledar s dogodkima uporabnika in TODO seznam dogodkov uporabnika. 

#### Atributi
    
#### Nesamoumevne metode

* odjaviUporabnika(long id): void
    * Metoda je implementirana z gumbom "DA". Odjavi uporabnika.
	
* preusmeri() : void
	* Metoda je impelmetirana z gumbom "NE". Vrne uporabnika na domačo stran.


### ZM_Koledar

Razred, ki bo prikazoval koledar s dogodkima uporabnika in TODO seznam dogodkov uporabnika. 

#### Atributi

* email: String
    * email prijavljenega uporabnika

    
#### Nesamoumevne metode

* prikaziKoledarInTODO(): void
    * metoda zahteva uporabniške dogodke 
    * metoda prezentira dogodke v koledarju in TODO seznamu

* prikaziPodrobnosti(long id): void
    * Metoda je implementirana s klikom na ime kateregakoli dogodka na zaslonski maski. Kot parameter se uporabi ime dogodka, ki se je kliknil.

### SV_OpenWeatherMapAPI

Sistemski vmesnik SV_OpenWeatherMapAPI je mejni (boundary) razred. Poskrbi za pridobivanje podatkov o vremenu preko spletne strani OpenWeatherMap.

#### Atributi

* API_KEY: String
    * Vrednost API ključa

* bazniNaslov: String
    * bazni URL naslov za pridobivanje podatkov

#### Nesamoumevne metode

* GetByCityId(int id): JASON
    * Vrne pet dnevno vremensko napoved za vsake tri ure v JASON formatu.
	
### K_UstvariDogodek

Kontrolni (control) razred K_UstvariDogodek obdela zahtevo za ustvarjanje novega dogodka, mu določi id in doda nov dogodek v bazo.

#### Atributi

* idDogodka: long
    * Id dogodka za zapis v bazo in poznejšo identifikacijo.
	
* idUporabnika: long
    * Id uporabnika, ki je ustvaril dogodek.

* ime: String 
    * Ime dogodka.
	
* opis: String 
    * Opis dogodka.
	
* oznake: String
    * Oznake dogodka.
	
* lokacija: String
    * Lokacija dogodka.
	
* od: Datum 
    * Datum in čas začetka dogodka.

* do: Datum 
	* Datum in čas konca dogodka.

* prioriteta: Boolean 
	* Določitev prioritete (true/false).
	
#### Nesamoumevne metode

* ustvariDogodek(Long idDogodka, Long idUporabnika, String ime, String opis, String oznake, String lokacija, Date od, Date do, boolean prioriteta): void
    * Metoda ustvari dogodek in zahteva zapis v bazi.

### K_UrediDogodek

Kontrolni (control) razred K_UrediDogodek obdela zahtevo za posodabljanje in urejanje dogodka in zahteva zapis sprememb v bazi.

#### Atributi

* idDogodka: long
    * Id dogodka za zapis v bazo in poznejšo identifikacijo.
	
* idUporabnika: long
    * Id uporabnika, ki je ustvaril dogodek.

* ime: String 
    * Ime dogodka.
	
* opis: String 
    * Opis dogodka.
	
* oznake: String
    * Oznake dogodka.
	
* lokacija: String
    * Lokacija dogodka.
	
* od: Datum 
    * Datum in čas začetka dogodka.

* do: Datum 
	* Datum in čas konca dogodka.

* prioriteta: Boolean 
	* Prioriteta (true/false).
	
#### Nesamoumevne metode

* posodobiDogodek(Long idDogodka, Long idUporabnika, String ime, String opis, String oznake, String lokacija, Date od, Date do, boolean prioriteta): void
    * Metoda ustvari dogodek in zahteva zapis v bazi.

* pridobiPodatkeODogodku(Long idDogodka) : Objekt
	* Metoda vrne podatke o dogodku.
	
### K_PregledUstvarjenihDogodkov

Kontrolni (control) razred K_Pregled_ustvarjenih_dogodkov implementira pridobivanje dogodkov iz baze in .

#### Atributi

* idDogodka: long
    * Id uporabnika, ki je ustvaril dogodek.

* idUporabnika: long
    * Id uporabnika, ki je ustvaril dogodek.

* ime: String 
    * Ime dogodka.	
	
* oznake: String
    * Oznake dogodka.	
	
#### Nesamoumevne metode

* pridobiDogodke(long idUporabnika): seznam Objektov(long idDogodka, String ime, String oznake)
    * Metoda je pridobi vse dogodke, ki jih je ustvaril določen uporabnik in njihove osnovne informacije iz baze.	

### K_Koledar

Razred obdela seznam dogodkov uporabnika, za lažjo predstavitev dogodkov.

#### Atributi

* dogodki_uporabnika: Dogodek[] 
    * Hrani seznam dogodkov uporabnika
* email: String
    * Hrani uporabniški email
    
#### Nesamoumevne metode

* vrniDogodkeUporabnika(email): Dogodki[]
    * Prodobi seznam dogodkov uporabnika in jih obdela za lažjo predstavitev.

### K_Nabiralnik

Kontrolni razred K_Nabiralnik implementira osrednjo logiko za funkcionalnost Pregled nabiralnika in TODO seznama.

#### Atributi

* idUporabnika: String
    * Enolični identifikator trenutnega uporabnika, torej njegov e-mail.

* narocnine: Oznaka[]
    * Hrani oznake, na katere je uporabnik naročen.

* dogodkiNabiralnik: Dogodek[]
    * Dogodki za prikaz v nabiralniku.

* dogodkiTODO: Dogodek[]
    * Dogodki za prikaz v TODO seznamu.

#### Nesamoumevne metode

* pridobiNabiralnik(): Dogodek[]
    * Metoda poskrbi, da se pridobijo dogodki za prikaz v nabiralniku. Poskrbi tudi, da so v primerni obliki za prikaz na zaslonski maski.

* pridobiTODO(): Dogodek[]
    * Metoda poskrbi, da se pridobijo dogodki za prikaz na TODO seznamu. Poskrbi tudi, da so v primerni obliki za prikaz na zaslonski maski.

* pridobiPovabila(): Dogodek[]
    * Metoda poskrbi za logiko, da se v nabiralniku prikažejo tudi dogodki na katere je bil uporabnik povabljen s strani drugih uporabnikov.

* dodajDogodek(long id):
    * Poskrbi za to, da se dogodek iz nabiralnika doda na TODO seznam. Pripravi torej vse potrebno pred klicem ustrezne metode v poslovnem razredu.

* odstraniIzNabiralnika(long id):
    * Poskrbi za to, da se dogodek iz nabiralnika odstrani. Pripravi torej vse potrebno pred klicem ustrezne metode v poslovnem razredu.

* odstraniIzTODO(long id):
    * Poskrbi za to, da se dogodek s TODO seznama odstrani. Pripravi torej vse potrebno pred klicem ustrezne metode v poslovnem razredu.


### K_Oznake

Razred ki poskrbi za upravljanje z oznakam uporabnika.

#### Atributi

* email: String
    * email uporabnika
* vseOznake: Oznaka[]
    * seznam vsez oznak
* oznakeUporabnika: Oznaka[]
    * seznam vseh oznak na katere je uporabnik prijavljen

#### Nesamoumevne metode

* vrniVseOznake(): Oznaka[]
    * preveri zahtevo in pridobi seznam oznak, seznam oznak obdeluje za lažji prikaz
* vrniOznakeUporabnika(email): Oznaka[]
    * preveri zahtevo in pridobi seznam oznak uporabnika, seznam oznak obdeluje na lažji prikaz 
* dodajOznako(email, id_oznaka): boolean
    * preveri zahtevo in v doda oznako uporabniku.
* odstraniOznako(email, id_oznaka): boolean
    * preveri oznako in odstrani oznako uporabniku.

### K_PodrobnostiDogodka

Razred K_PodrobnostiDogodka je kontrolni razred, ki implementira osrednjo logiko za funkcionalnost Pregled podrobnosti dogodka.

#### Atributi

* idUporabnika: String
    * Enolični identifikator trenutnega uporabnika, torej njegov e-mail.

* idDogodka: long
    * Enolični identifikator dogodka, katerega podrobnosti prikazujemo.

#### Nesamoumevne metode

* pridobiPodatkeODogodku(): void
    * Metoda pripravi vse potrebno za iskanje informacij o dogodku preko poslovnega razreda v bazi.

* pridobiKomentarjeZaDogodek(): Komentar[]
    * Metoda pripravi parametre za poizvedbo preko poslovnega razreda Komentar o komentarjih, ki se nanašajo na dogodek.

* pridobiVremeZaDogodek(): JASON
    * Metoda pripravi ustrezne parametre za poizvedbo preko sistemskega vmesnika OpenWeatherMapAPI o vremenu na kraju dogodka.

* prijaviDogodek(): void
    * Metoda implementira osrednjo logiko za prijavo spornega dogodka moderatorju. Zahtevo posreduje naprej poslovnemu razredu, ki v bazo zapiše prijavo.

* komentira(): void
    * Metoda posreduje vsebino novega komentarja, ki ga pridobi z zaslonske maske.

* povabiNaDogodek(): void
    * Metoda implementira osrednjo logiko za povabilo prijatelja na dogodek. Zahtevo posreduje naprej poslovnemu razredu, ki v bazo zapiše prijavo.

### Dogodek

Razred Dogodek je poslovni (entity) razred, ki predstavlja dogodek kateri se v sistemu ustvari, oz. si ga študent lahko doda v koledar ali TO-DO seznam.

#### Atributi

* Id_dogodek: long
    * Gre za peeprosto šifro dogodka, ki je število
* Ime: String(30)
    * Ime dogodka, kot ga določi njegov lastnik
    * Tip je String, ki ne sme biti daljši od 30 znakov
* Prioriteta: boolean
    * Prioriteta, kot jo določi lastnik dogodka
    * Zaloga vrednosti je torej logična vrednost (je pomemben / ni pomemben)
* Datum_od: Date
    * Datum začetka dogodka, kot ga določi lastnik dogodka
    * Podatkovni tip je Date (DD:MM:YYYY:HH:MM)
* Datum_do: Date
    * Datum konca dogodka, kot ga določi lastnik dogodka
    * Podatkovni tip je Date (DD:MM:YYYY:HH:MM)
* Prijavljen: boolean
    * Označuje ali je bil dogodek prijavljen moderatorju s strani študenta
* Lokacija: String
    * Označuje kraj dogajanja dogodka.
    * Izbira lokacije dogodka bodo omejena zgolj na spodnje lokacije: Vse fakultete Univerze v Ljubljani,  Ljubljana, Maribor, Koper, Celje
* Lastnik: String
    * Označuje e-mail uporabnika, ki je lastnik dogodka, torej uporabnika, ki je dogodek ustvaril.

* Oznake_dogodek: long[]
    * Identifikatorji oznak s katerimi je dogodek povezan.
    
    
#### Nesamoumevne metode

Razred Dogodek poleg metod getter in setter za atribute vsebuje še naslednje metode.


* zbrišiDogodek(long id): void
    * Metoda zbriše dogodek z Id_dogodek id iz baze.

* dodajDogodek(long id): void
    * Metoda doda dogodek z Id_dogodek id v bazo.

* vrniVse(long id): Dogodek
    * Vrne dogodek z Id_dogodek id.

* VrniKomentarjeZaDogodek(): long[]
    * Vrne identifikacijske številke vseh komentarjev za dogodek.


### Uporabnik

Poslovni (entity) razred Uporanik hrani podatke o uporabnikih v sistemu.

#### Atributi

* Ime: String
    * Ime uporabnika
* Priimek: String
    * Priimek uporabnika

* Email: String
    * Email, ki je enolični identifikator uporabnika.

* Geslo: String
    * Geslo uporabnika.

* Vloga: String
    * Vloga uporabnika. Zaloga vrednosti je omejena na student, pe_delavec, zun_akter in moderator.

* Nabiralnik: long[]
    * Tabela identfikacijskih številk dogodkov, ki so vidne v uporabnikovem nabiralniku. 

* jePotrjen: boolean
    * Atribut označuje ali je bil uporabnik potrjen s strani moderatorja za registracijo. Pri študentih je ta atribut vedno nastaljen na true.

* Nabiralnik: long[]
    * Atribut hrani identifikatorje dogodekov, ki so vidni uporabniku v njegovem nabiralniku.

* TODO_seznam: long[]
    * Atribut hrani identifikatorje dogodekov, ki so na uporabnikovem TODO seznamu.

* trenutnoPrijavljen: boolean
	*Atribut označuje ali je uporabnik trenutno prijavljen.

#### Nesamoumevne metode

Razred Uporabnik poleg metod getter in setter za atribute vsebuje še naslednje metode.

* vrniDogodke(String email): long[]
    * Vrne vse identifikacijske številke dogodkov, ki jih je ustvaril uporabnik z E-mailom email.

* dodajNaTODO(long dogodek): void
    * Doda dogodek z identifikatorjem dogodek na TODO seznam uporabnika.

* brisiNabiralnik(long dogodek): void
    * Zbriše dogodek z identifikatorjem dogodek iz uporabnikovega nabiralnika.

* brisiTODO(long dogodek): void
    * Zbriše dogodek z identifikatorjem dogodek iz uporabnikovega TODO seznama.
	
* poisciUporabnika(string email, string geslo) : boolean
	* Pogleda ali se email in geslo ujemata s podatki tega uporabnika.
	
* zahtevaZaPotrditev(long id) : boolean
	* Od moderatorja zahteva potrditev, da je ima lahko vlogo pedagoškega delavca ali zunanjega akterja. Poderator ob zahteku dobi odadtno možnost na seznamu uporabnikov.


### Povabilo

Poslovni (entity) razred Povabilo hrani podatke o povabilih v sistemu.

#### Atributi

* Id_povabilo: long
    * Enolična identifikacijska številka povabila.
* Id_vabi String
    * Email uporabnika, ki vabi drugega uporabnika na dogodek.

* Id_povabljenca: String
    * Email povabljenca na dogodek

* Id_dogodek: long
    * Id dogodka na katerega se povabilo navezuje.


#### Nesamoumevne metode

Razred Povabilo poleg metod getter in setter za atribute vsebuje še naslednje metode.

* vrniPovabila(String id): long[]
    * Metoda vrne identifikacijske številke vseh povabil za uporabnika z e-mailom id.

* dodaj(long dogodek, long vabi, long povabljenec): void
    * Doda povabilo na dogodek z identifikatorjem dogodek. Metoda sprejme tudi identifikatorja tistega, ki vabi in povabljenca.


### Oznaka

Poslovni (entity) razred Oznaka hrani podatke o oznakah za dogodke v sistemu.

#### Atributi

* Id_oznaka: long
    * Enolični identifikator oznake v sistemu.

* Ime: String(10)
    * Ime oznake. Atribut je tipa String, ki ne presega dolžine 10 znakov.


#### Nesamoumevne metode

* vrniOznakeUporabnika(email): Oznaka[]
    * vrne seznam oznak uporabnika.


### Komentar

Poslovni (entity) razred Komentar hrani podatke o komentarjih za dogodke v sistemu.

#### Atributi

* Id_komentar: long
    * Enolični identifikator komentarja v sistemu.

* Id_dogodek: long
    * Enolični identifikator dogodka na katerega se komentar navezuje.

* Vsebina: String(140)
    * Vsebina komentarja. Dolžina je omejena na 140 znakov.


#### Nesamoumevne metode

Razred Komentar poleg metod getter in setter za atribute vsebuje še naslednje metode.

* dodaj(long dogodek, String vsebina): void
    * Doda komentar na dogodek z identifikatorjem dogodek in vsebino vsebina.

## 3. Načrt obnašanja

Spodaj so prikazani diagrami zaporedja in ostali diagrami za posamezne primere uporabe.

### Prijava
#### Osnovni tok
![Prijava - osnovni tok](../img/Prijava1.png)

* Neprijavljen uporabnik, ki pride na vstopno stran ima možnost prijave ali registracije(opisna kasneje). Uporabnik, ki že je registriran vpiše email(enolično določa uporabnika) in geslo. Potem klikne gumb Prijava. Priajva se izvede, tako da se email in geslo preneseta v bazo. Izvede se iskanje in preverjanje uporabnika. Če je iskanje uspešno, se uporbniku nastavi atribut prijavljen na true in poišče kakšno vlogo ima uporabnik. Glede na uporabnikovo vlogo se odpre različna domača stran. Študnetu se odpre nabiralnik, pedagošeku delavcu oziroma zunanjemu akterju se odpre upravitelj dogodkov in moderatorju se odpre nadzorna platforma(urpavljalec dogodkov, uporabnikov in oznak).

#### Alternativni tok - napačen vnos podatkov
![Prijava - alt1](../img/Prijava2.png)

* V primeru napačnega ali praznega obrazca se izpiše obvestilo o napaki in ponastavi vnosna polja. Napako se ugotovi, tako da med preverjenjem ne najde nobenega zadetka.

#### Alternativni tok - neavtoriziran dostop
![Prijava - alt2](../img/Prijava3.png)

* V primeru, da neprijavljen uporabnik želi dostopati do katerekoli notranje strani (katerakoli strani, ki ni vstopna ali registracijska stran; v primeru je nabrilanik) preden je prijavljen, bo preusmerjen na vstopno stran. 

### Odjava
#### Osnovni tok
![Odjava - osnovni tok](../img/Odjava1.png)

* Prijavljen uporabnik se iz sistema odjavi s klikom na gumb "odjava" v orodni vrstici. Preden se odajvi mora na novi strani za odjavo svojo odjavo še potrditi s klikom na gumb "DA". Tako se v sistemu zabeleži, da uporabnik ni več prijavljen. Prav tako se izvede preusmeritev na vstopno stran. Nasprotno, lahko uporabnik klikne gumb "NE" in se tako vrne na domačo stran (ista stran, ki jo obišče, ko se prijavi; v primeru je nabiralnik (za uporabnika: študent)).

### Registracija
#### Osnovni tok
![Registracija - osnovni tok](../img/Registracija1.png)

* Neprijavljen uporabnik, ki pride na vstopno stran ima možnost prijave ali registracije. Če uporabnik še ni registriran v sistem in to želi storiti klikne na gumb Registracija, ki ga preusmeri na stran, kjer se lahko registrira. Novi uporabnik se registrira tako, da vnese svoje podatke (ime, priimek, email, organizacija, geslo, ponovno geslo in vlogo (v diagramu je to skrajšano v "podatki")). Če se uporabnik registrira v vlogo pedagoškega delavca ali zunanjega akterja, ga mora pred uspešno registracijo potrditi še moderator.

#### Alternativni tok - že uporabljen email naslov
![Registracija - alt1](../img/Registracija2.png)

* V primeru, da je mail že uporabljen se registracija ustavi in polja spraznejo. Uporabnik mora začeti od začetka. To se ponavlja dokler ne vpiše emaila, ki še ni zavzet.

#### Alternativni tok - napačno potrditveno geslo
![Registracija - alt2](../img/Registracija3.png)

* V primeru, da je geslo za preverjanje napačno se registracija ustavi in polja spraznejo. Uporabnik mora začeti od začetka. To se ponavlja dokler uporabnik obakrat ne vpiše istega gesla.

### Upravljanje z dogodki, uporabniki in oznakami
![Moderator - osnovni tok](../img/Moderator1.png)

* Moderator upravlja sistem iz svojega upravitelja dogodkov, uporabnikov in oznak. Moderator mora prav tako potrditi uporabnike, ki želijo biti v določenih vlogah. Moderator lahko preglejuje in briše dogodke, uporabnike in oznake. Na vrhu seznama so dogodki, ki so bili prijavljeni in uporabniki, ki jih je treba potrditi.

### Pregled podrobnosti dogodka

![Podrobnosti o dogodku - osnovni](../img/Podrobnosti1.png)


* Študent na zaslonski maski za pregled nabiralnika in TODO seznama (ZM_nabiralnik) klikne kateri koli dogodek in odpre se novo okno z njegovimi podrobnostmi. Preko kontrolnega razreda K_PodrobnostiDogodka in poslovnega razreda Dogodek se pridobijo podrobnosti dogodka iz baze. Podatki se prikažejo na zaslonski maski  ZM_PodrobnosiDogodka. Preko kontrolnega razreda in sistemskega vmesnika SV_OpenWeatherMapAPI se pridobi podatke o vremenu.

![Podrobnosti o dogodku - alternativni](../img/Podrobnosti2.png)

* Diagram zaporedja je zelo podoben prejšnemu, vendar študent do podrobnosti dogodka dostopa preko zaslonske maske ZM_Koledar.

### Ustvarjanje novega dogodka
![Ustvarjanje dogodkov - osnovni tok](../img/ustvarjanje-dogodkov.png)

* Študent na zaslonski maski za prikaz podrobnosti dogodka (ZM_PodrobnosiDogodka) ali na zaslonski maski nabiralnik (ZM_Nabiralnik) klikne na gumb DODAJ in sproži se metoda dodajDogodek() iz kontrolnega razreda dogodek. Potem ko študent vpiše podatke, poslovni razred Dogodek poskrbi, da se nov dogodek zapiše v bazo. Po uspešnem vpisu v bazo, se na zaslonski maski izpiše potrdilo o ustvarjenem dogodku. Zaslonska maska za ustvarjanje dogodka (ZM_Ustvari dogodek) se zapre.

### Urejanje dogodka
![Urejanje dogodkov - osnovni tok](../img/urejanje-dogodkov.png)

* Študent na zaslonski maski za pregled ustvarjenih dogodkov(ZM_PregledUstvarjenihDogodkov) ali na zaslonski maski nabiralnik (ZM_Nabiralnik) klikne na gumb DODAJ in sproži se metoda dodajDogodek() iz kontrolnega razreda dogodek. Potem ko študent vpiše podatke, poslovni razred Dogodek poskrbi, da se nov dogodek zapiše v bazo. Po uspešnem vpisu v bazo, se na zaslonski maski izpiše potrdilo o ustvarjenem dogodku. Zaslonska maska za ustvarjanje dogodka (ZM_UstvariDogodek) se zapre.

### Pregled ustvarjenih dogodkov
![Pregled ustvarjenih dogodkov - osnovni tok](../img/pregled-ustvarjenih-dogodkov.png)

* Pregled ustvarjenih dogodkov se izvaja na zaslonski maski ZM_PregledUstvarjenihDogodkov. Uporanik ima možnost pregleda dogodkov, brisanje in urejanje posameznega dogodka, ter ustvarjanje novega dogodka. 

### Prijava spornega dogodka

![Prijava dogodka](../img/Prijava_dogodka.png)

* Študent na zaslonski maski za prikaz podrobnosti dogodka (ZM_PodrobnosiDogodka) klikne na gumb Prijavi in sproži se metoda prijaviDogodek() iz kontrolnega razreda K_PodrobnostiDogodka. Poslovni razred Dogodek poskrbi, da se prijava zapiše v bazo. Po uspešnem vpisu v bazo, se na zaslonski maski izpiše potrdilo o prijavi.

### Povabilo uporabnika na dogodek

![Povabilo uporabnika na dogodek](../img/Povabilo.png)

* Preko zaslonske maske ZM_PodrobnosiDogodka se pridobijo podatki o povabljencih in s pritiskom na gumb "Povabi Prijatelja" se sroži metoda povabi() na zaslonski maski. Kontrolni razred nato preveri zahtevo in z metodo povabiNaDogodek() preda parametre poslovnemu razredu Povabilo, ki povabilo zapiše v bazo. Na zaslonski maski se ob uspešni izvedbi izpiše potrditev.

### Komentiranje dogodka

![Komentiranje dogodka](../img/Komentar.png)

* Preko zaslonske maske ZM_PodrobnosiDogodka se pridobi vsebina komentarja in s pritiskom na gumb "Dodaj komentar" se sroži metoda objaviKomentar() na zaslonski maski. Kontrolni razred nato preveri zahtevo in z metodo komentiraj() preda parametre poslovnemu razredu Komentar, ki komentar zapiše v bazo. Na zaslonski maski se ob uspešni objavi komentarja izpiše potrditev.

### Pregled nabiralnika in urejanje TODO seznama.

![Nabiralnik - osnovni tok](../img/Nabiralnik3.png)

* Kontrolni razred K_nabiralnik ob vstopu na stran za pregled nabiralnika sproži metodo pridobiNabiralnik() in preko poslovnih razredov Uporabnik in Dogodek pridobi dogodke za prikaz v nabiralniku. Z metodo pridobiPovabila() preko poslovnih razredov Uporabnik in Povabilo pridobi dogodke na katere je bil študent povabljen iz baze. Prav tako ob vstopu na stran kontrolni razred preko omenjenih poslovnih razredov pridobi še dogodke za prikaz na TODO seznamu z metodo pridobiTODO(). Ob kliku na gumb "dodaj" ob katerem koli dogodku v nabiralniku na zaslonski maski se sproži dodajanje dogodka na TODO seznam.

![Nabiralnik - alternativni tok](../img/Nabiralnik1.png)

* Diagram prikazuje alternativni tok, kjer s klikom na simbol X ob katerem koli dogodku v nabiralniku študent sproži brisanje tega dogodka iz nabiralnika s klici ustreznih metod, kot je prikazano na diagramu.

![Nabiralnik - alternativni tok2](../img/Nabiralnik2.png)

* Diagram prikazuje alternativni tok, kjer s klikom na simbol X ob katerem koli dogodku na TODO seznamu študent sproži brisanje tega dogodka s TODO seznama s klici ustreznih metod, kot je prikazano na diagramu.

### Pregled koledarja in TODO seznama

![Kolendar_-_osnovni_tok](../img/diagram_zaporedja_pregled_kolendarja_in_todo.PNG)

* Diagram prikazuje osnovni pristop na stran, ki nam prikazuje koledar in TODO seznam. Ter potek pridobivanja potrebnih podatkov.

### Pregled oznak

![oznake](../img/diagram_zaporedja_oznake.PNG)

* Diagram prikazuje potek pregleda, dodajanja in brisanja oznak.