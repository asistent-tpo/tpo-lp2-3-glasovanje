﻿# Dokument zahtev

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Filip Zupančič, Alan Hadžić, Peter Bohanec in Tom Gornik |
| **Kraj in datum** | Ljubljana, 3.4.2019 |



## Povzetek projekta

Dokument je sestavljen iz uvoda, v katerem je opisan problem in domena, ter predlagana rešitev. Orisano je tudi ogrodje aplikacije. Uporabniške vloge vpeljejo 4 glavne tipe uporabnikov aplikacije in na kratko opišejo njihovo vlogo. Sledi slovar pojmov, v katerem so razložene besede iz katerih pomen ni neposredno razviden. Diagram primerov uporabe je grafični prikaz akterjev in funkcionalnih zahtev, ki so v nadaljevanju tudi podrobneje opisane. Za vsako funkcionalno zahtevo je naveden osnovni tok, poleg tega pa tudi alternativni tokovi, pogoji, posledice, posebnosti, prioriteta in sprejemni testi. Nefunkcionalne zahteve smo razdelili v tri glavne skupine. Zahteve izdelka, organizacijske zahteve in zunanje zahteve. Za konec smo v prototipe vmesnikov vključili zaslonske maske in opis vmesnika OpenWeatherMap API, za pridobivanje podatkov o vremenu.

## 1. Uvod


V zadnjem času se ljudje v razvitem svetu soočamo z vse večjimi pričakovanji in zahtevami ljudi okoli sebe in družbe na splošno. Po navadi so to pričakovanja glede uspeha pri delu ali študiju, zdravju, socialnem življenju, hobijih ... Ta trditev še posebej drži za slovenske študente, saj imajo veliko število opravkov, obveznosti in drugih aktivnosti, ki jih morajo izpolniti. Te aktivnosti in dogodki so velikokrat pogojeni z določenim rokom, ki določa do kdaj mora biti aktivnost zaključena. To je za študente, ki naj bi bili predvsem osredotočeni na učenje in študji, lahko zelo stresno in skoraj zagotovo zelo naporno. Študenti si morajo poleg študnjiskih obveznosti, kot so datumi oddaje domačih in seminarskih nalog ter datumi kolokvijev in izpitov, zapomniti in v svoj koledar dodati še druge aktivnosti in dogodke, ki so namenjeni njim in jih zanimajo. Velika obremenjenost, ki jo študenti dožvljajo ob tem, lahko povzroča stres in negativno vpliva na zdravje in počutje študentov.

Da naslovimo in rešimo ta problem, bomo razvili aplikacijo imenovano StraightAs. Gre za spletno platformo, katere glavni namen je pomagati študentom pri organiziranju njihovega koledarja in TO-DO seznama, hkrati pa poskrbeti, da bo študnet vedno obveščen o aktivnosti, ki ga zanimajo. Naš cilj je narediti kvalitetno aklikacijo, ki bo priročna in predvsem hitro dostopna. Da bomo te cilje dosegli, nam zagotovlja motivirana in izkušena ekipa programerjev in oblikalcev ter ta dokument, ki bo služil kot temelj za nadaljno delo na aplikaciji. 

Glavni uporabnik aplikacije bo študent. Poleg študenta pa računamo, da bodo aplikacijo uporabljali še moderator, pedagoški delavec in zunanji sodelavec.

Aplikacija bo poleg osnovnih funkcionalisti spletnih aplikacij z uporabniškimi računi, kot so registracija, prijava in odjava, vsebovala tri glavne elemente. To so koledar, TO-DO seznam in seznam aktulanih dogodkov (nabiralnik), ki jih uporabnik še nima v koledaru ali na TO-DO seznamu. Vsak uporabnik se bo glede na svoje potrebe, želeje in obveznosti prijavil na oznake (#). Vsak ustvarjen dogodek bo označen z vsaj eno oznako. Uporabniku se bodo novi dogodki, ki imajo vsaj eno oznako na katero je ta uporabnik naročen, pokazali v njegovem nabiralniku. Iz tega seznama si bo lahko izbral ali bo dogodek dodal na svoj seznam in koledar ali ga bo odstranil. 

Vsak uporabnik bo imel možnost, da ustvari nove dogodke. Dogodek je lahko zaseben (vidi in upravlja ga samo uporabnik, ki ga je ustvaril) ali pa javen. Če je dogodek javen, mu mora uporabnik določiti oznako. Z oznako uporabnik opiše tip oziroma skupino dogodka. Vsak dogodek ima lahko več oznak. Uporabnik lahko prav tako ustvari nove oznake in ureja svoje dogodke ter direktno povabi svojega prijatelja na njegov dogodek.

Računi uporabniko, ki bodo registrani kot zunanji sodelavec in pedagoški delavec bodo lahko dostopali do ustvarjanja novih dogodkov, pregleda ustvarjenih dogodkov in spreminjanja obstoječega dogodka. Namen teh dveh vlog je samo dodanje dogodkov (na primer izpitnih rokov) in ne uporaba koledarja ali TO-DO seznama. 

Po izbranih dogodkih lahko uporabnik uporablja TO-DO seznam in koledar, da si organizira čas in dnevni urnik. S tem izpolnemo glavni cilj aplikcaije.

Aplikacija bo stilsko dovršena, varna pred vdori, robustna na število uporabnikov, po evropskem standaru GDPR in predvsem prijazna uporabniku.



## 2. Uporabniške vloge

**Študent**
Študentu je namenjenih največ funkcionalnosti v aplikaciji. Omogočene so mu naslednje funkcionalnosti: Ustvarjanje dogodkov, spreminjanje ustvarjenih dogodkov, pregled nabiralnika in urejanje TO-DO seznama, pregled koledarja in TO-DO seznama, naročanje na oznake ter pregled podrobnosti dogodka, v sklopu katere lahko prijavi sporen dogodek moderatorju, ga komentira oz. nanj povabi prijatelja. Študent se lahko seveda tudi odjavi iz sistema.

**Zunanji akter**
Zunanjemu akterju je v aplikaciji omogočeno ustvarjanje novih dogodkov, pregled ustvarjenih dogodkov in spreminjanje obstoječega dogodka. Zunanji akter se lahko tudi odjavi iz sistema.

**Pedagoški delavec**
Pedagoškemu delavcu so v aplikaciji omogočene enake funkcionalnosti kot zunanjemu akterju.

**Moderator**
Moderator skrbi za ustreznost vsebin in nemoteno delovanje aplikacije. Omogočena mu je funkcionalnost upravljanja z uporabniki in dogodki. Moderator se lahko tudi odjavi iz sistema.

**Neprijavljen uporabnik**
Neprijavljenemu uporabniku je omogočena prijava v sistem in registracija.

## 3. Slovar pojmov

| | |
|:---|:---|
| **Termin** | **Definicija** |
|**Dogodek**| Ustvstijo ga lahko študenti, zunanji akterji ali pedagoški delavci. Določen je z imenom, opisom, trajanjem in oznakami. Lahko je javen ali zaseben.|
| **Glavni uporabnik** |  Uporabnik za katerega je aplikacija primarno izdelana. V primeru aplikacije StraightAs je to študent iz Slovenije. Vse funkcionalnosti in pomožne vloge so implementirane zato, da zagotvljajo kvalitetno izkušnjo za glavnega uporabnika. |
| **Nabiralnik**| (angl. feed) je prostor kamor prijahajajo dogodki z oznakami, na katere je študent naročen.  |
| **Neprijavljen uporabnik** | Vsak obiskovalec aplikacije, ki še ni avtenticiran ali pa prvič obišče aplikacijo.|
| **OpenWeatherMap API** | je spletna storitev ki omogoča pridobivanje podatkov o vremenu preko http protokola. |
| **Oznaka** | (angl. tag) je beseda, ki povezuje sorodne dogogdke. Označene so z #.|
| **Pedagoški delavec** |Je oseba, ki sodeluje v pedagoškem procesu na fakulteti, torej izvajalec nekega predmeta. Lahko je bodisi profesor ali asistent.|
| **Predmet** | Predmet, ki se izvaja na fakulteti s strani pedagoških delavcev, obiskujejo pa ga študenti.|
| **Sistem** | Spletna aplikacija, ki ponuja storitve uporabnikom. |  
| **Študent** | Študent je oseba, ki izobražuje v določenem programu na fakulteti. Ima aktiven študentski status. |
| **Tip dogodka**| Dogodek je lahko javen ali privaten. Privaten dogodek lahko ustvari samo študent in je viden le njemu. Javen dogodek je viden vsem študentom. |
| **TO-DO** | Iz angleščine, pomeni "za narediti". |  
| **Zunanji akter** | Je lahko predstavnik nekega podjetja ali organizacije (npr. Športnega društva), ki želi v aplikaciji deliti zanimive dogodke za študente. |



## 4. Diagram primerov uporabe

![Diagram primerov uporabe](../img/usecase.png)


## 5. Funkcionalne zahteve


### Prijava v sistem


#### Povzetek funkcionalnosti

Neprijavljen uporabnik se lahko prijavi v sistem. 

#### Osnovni tok

* Uporabnik obišče spletno vstopno stran sistema.
* Uporabnik vpiše uporabniško ime in geslo v vnosna polja.
* Uporabnik pritisne na gumb “Prijava”.
* Uporabnika se preusmeri na začetno stran glede na uporabniško vlogo.


#### Alternativni tokovi

Neavtorizian uporabnik dostopa do strani, ki zahteva avtorizacijo

* Neprijavljen uporabnik želi dostopati do strani, ki ni vstopna.
* Sistem ga preusmeri na vstopno stran.
* Neprijavljen uporabnik vpiše uporabniško ime in geslo v vnosna polja.
* Neprijavljen uporabnik pritisne na gumb “Prijava”.
* Uporabnika se preusmeri na začetno stran glede na uporabniško vlogo.

Napačen vnos uporabniškega imena in gesla

* Neprijavljen uporabnik je na vstopni strani.
* Neprijavljen uporabnik vpiše napačno ime in/ali geslo.
* Uporabnik dobi opozorilo o napačnem vnosu uporabniškega imena ali gesla.
* Vnosna polja se ponastavijo.


#### Pogoji

* Uporabnik ni prijavljen v sistemu.


#### Posledice

* Uporabnik ni prijavljen v sistem, lahko se poskuša prijaviti še enkrat.


#### Posebnosti

* Funkcionalnost ne vsebuje nobenih posebnosti.

#### Prioriteta

* Prijava v sistem je Must have funkcionalnost.

#### Sprejemni test

| | | | |
|:---|:---|:---|:---|
| **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod** | **Pričakovani rezultat** |
| **Prijava v sistem - osnovni tok** | Uporabnik s vhodnimi podatki je že registriran v sistemu, vendar ni prijavljen. | E-mail: test@test.si, geslo: testgeslo | Uporabnik test@test.si je prijavljen v sistem.|
| **Prijava v sistem - neavtoriziran dostop** | Sistem ne prikatuje ničesar | Uporabnik želi dostopati na stran aplikacije, ki ni prijavna. | Uporabnik je preusmerjen na stran za prijavo.  |
| **Prijava v sistem - napačno geslo** | Sistem prikazuje prijavno stran, ki je že izpolnjena z vhodnimi podatki.  | E-mail:test@test.si, geslo: geslo | Uporabnik ni prijavljen, izpiše se sporočilo o neuspeli prijavi.  |



### Odjava iz sistema


#### Povzetek funkcionalnosti

Funkcionalnost omogoča študentu, zunanjemu akterju, pedagoškemu delavcu ali moderatorju odjavo iz sistema.

#### Osnovni tok

* Uporabnik pritisne gumb odjava na orodni vrstci.
* Uporabniku se prikaže okence Odjava.
* Uporabnik pritisne "Da" in se odjavi iz sistema.
* **Osnovni tok je enak za vse naštete uporabniške vloge**


#### Alternativni tok(ovi)

* Funkcionalnost nima alternativnih tokov.


#### Pogoji

* Uporabnik mora biti priavljen v sistem.


#### Posledice

* Uporabnika se preusmeri na vstopno stran.
* Uporabniku se odvzamejo pravice dostopov do strani, ki zahtevajo avtentikacijo.

#### Posebnosti

* Funkcionalnost ne vsebuje nobenih posebnosti.

#### Prioriteta

* Odjava iz sistema je Must have funkcionalnost.

#### Sprejemni test

| | | | |
|:---|:---|:---|:---|
| **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod** | **Pričakovani rezultat** |
| **Odjava iz sistema - osnovni tok** | Uporabnik je prijavljen v sistemu| Uporabnik pritisne gumb odjava in odjavo potrdi. | Uporabnik je odjavljen iz sistema.|


### Registracija v sistem


#### Povzetek funkcionalnosti

Funkcionalnost omogoča neprijavljenemu uporabniku registracijo v sistem.


#### Osnovni tok

* Neprijavljen uporabnik obišče spletno vstopno stran sistema.
* Pritisne na gumb “Registracija”
* Prikaže se stran za vnos osebnih podatkov 
* Uporabnik vnese podatke: ime, priimek, email, geslo, potrditveno geslo, uporabniško vlogo (študent, pedagoški delavec ali zunanji akter), ime organizacije (opcijsko).
* Pritisne na gumb “Registracija”.
* Če uporabnik izbere uporabniško vlogo pedagoški delavec ali zunanji akter, mora registracijo potrditi moderator.
* Uporabnika se preusmeri na vstopno stran.


#### Alternativni tok(ovi)

Neprijavljen Uporabnik vnese email, ki je že registriran v sistemu 

* Uporabnik obišče spletno vstopno stran sistema.
* Uporabnik pritisne na gumb “Registracija”
* Uporabniku se prikaže stran za vnos osebnih podatkov 
* Uporabnik vnese pravilne osebne podatke, vnešen email je že registriran v sistemu.
* Uporabnik pritisne na gumb “Registracija”.
* Uporabnika se opozori da uporabnik z obstoječim emailom že obstaja
* Vnosna polja se ponastavijo
* Uporabnik vnese pravilne osebne podatke in email, ki je ne registriran v sistemu.
* Uporabnik pritisne na gumb “Registracija”.
* Uporabnika se preusmeri na vstopno stran.

Neprijavljen uporabnik vnese različno geslo in potrditveno geslo

* Uporabnik obišče spletno vstopno stran sistema.
* Uporabnik pritisne na gumb “Registracija”
* Uporabniku se prikaže stran za vnos osebnih podatkov 
* Uporabnik vnese pravilne osebne podatke in ne vnese enako geslo in potrditveno geslo.
* Uporabnika se opozori da se geslo in potrditveno geslo ne ujemata.
* Vnosna polja za gesla se ponastavijo.
* Uporabnik vnese pravilne osebne podatke in enako geslo in potrditveno geslo.
* Uporabnik pritisne na gumb “Registracija”.
* Uporabnika se preusmeri na vstopno stran.


#### Pogoji

* Uporabnik ni registran v sistemu.


#### Posledice

* Uporabnik je registriran v sistem.


#### Posebnosti

* Funkcionalnost nima posebnosti.



#### Prioriteta

* Registracija je Must have funkcionalnost.

#### Sprejemni test

| | | | |
|:---|:---|:---|:---|
| **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod** | **Pričakovani rezultat** |
| **Registracija - osnovni tok** | Sistem je na strani za regisracijo, neprijavljen uporabnik z vhodnimi podatki se želi registrirati |Ime: Janez, priimek: Novak, e-mail: janez.novak@test.si, uprabniška vloga: študent, geslo: geslo123, potrditev gesla: geslo123| Neprijavljeni uporabnik je registriran v sistem. |
| **Registracija - E-mail že obstaja** | Sistem je na strani za regisracijo, neprijavljen uporabnik z vhodnimi podatki, ki že obstajajo v sistemu se želi registrirati. |Ime: Janez, priimek: Novak, e-mail: janez.novak@test.si, uprabniška vloga: študent, geslo: geslo123, potrditev gesla: geslo123| Sistem prikaže opozorilo in zavrne registracijo |
| **Registracija - neujemanje gesel** | Sistem je na strani za regisracijo, neprijavljen uporabnik z vhodnimi podatki se želi registrirati. |Ime: Janez, priimek: Novak, e-mail: janez.novak@test.si, uprabniška vloga: študent, geslo: geslo123, potrditev gesla: g123| Sistem prikaže opozorilo in zavrne registracijo. |


### Upravljanje z dogodki in uporabniki

#### Povzetek funkcionalnosti
Moderator lahko upravlja z dogodki in oznakami ter uporabniki. Moderator lahko potrdi novo registrirane uporabnike (zunanje akter in pedagoške delavce) ter zbriše dogodke, oznake ali uporabnike.

#### Osnovni tok
* Moderatorju je omogočen trodelni prikaz. V enem delu so dogodki, v drugem uporabniki, v tretjem pa oznake.
* V delu, kjer so dogodki so vidni vsi dogodki v sistemu. Na vrhu so tisti, ki so jih študenti prijavili kot sporne. Omogočeno je tudi iskanje po dogodkih.
* Moderator zbriše katerikoli dogodek s klikom na gumb s simbolom X.

#### Alternativni tok
Potrditev registriracije

* V delu, kjer so prikazani uporabniki, lahko moderator odobri ali zavrne registracijo pedagoškemu delavcu ali zunanjemu akterju s klikom na potrditveni ali zavrnitveni gumb.
* Omogočeno je tudi iskanje po uporabnikih.


Izbris oznake

* V delu, kjer so zbrane vse oznake, lahko moderator izbriše oznako s klikom na gumb s simbolom X.
* Omogočeno je tudi iskanje po oznakah.

#### Pogoji

* Moderator je prijavljen v sistem.


#### Posledice

* Dogodek, uporabnik ali oznaka so izbrisani. Registracija pedagoškega delavca ali je potrjena oziroma zavrnjena


#### Posebnosti

* Funkcionalnost nima posebnosti.



#### Prioriteta

* Upravljanje z dogodki in uporabniki je Must have funkcionalnost.

#### Sprejemni test

| | | | |
|:---|:---|:---|:---|
| **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod** | **Pričakovani rezultat** |
| **Registracija - osnovni tok** | Moderator je prijavljen v sistem. Na zaslonu so prikazani dogodki, uporabniki in oznake vsak v svojem delu. |Moderator zbriše sporen dogodek Zabava 123| Dogodek Zabava 123 je zbrisana iz celotne aplikacije|
| **Registracija - Potrditev registracije** | Moderator je prijavljen v sistem. Na zaslonu so prikazani dogodki, uporabniki in oznake vsak v svojem delu |Moderator potrdi registracijo pedagoškemu delavcu Janezu Novaku| Janez Novak je uspešno registriran. |
| **Registracija - Izbris oznake** | Moderator je prijavljen v sistem. Na zaslonu so prikazani dogodki, uporabniki in oznake vsak v svojem delu |Moderator izbriše oznako "#Zabava"| Oznaka "#Zabava" je zbrisana pri vseh dogodkih. |


### Ustvarjanje dogodka


#### Povzetek funkcionalnosti

Študent, pedagoški delavec in zunanji akter lahko ustvarijo nov dogodek.

#### Osnovni tok

Študent

* Odpre se okno za vnos novega dogodka.
* Študent mora vpisati ime dogodka, kratek opis, datum začetka, datum konca.
* Določiti mora oznake za dogodek.
* Označiti mora ali je dogodek privaten (videl ga bo samo on) ali javen (vidijo ga vsi).
* Vnesti mora tudi prioriteto dogodka.
* Na dogodek lahko neposredno povabi tudi druge uporabnike aplikacije.
* Dogodek se avtomatsko vnese na TO-DO seznam in koledar študenta.

Pedagoški delavec in zunanji akter

* Odpre se okno za vnos novega dogodka.
* Študent mora vpisati ime dogodka, kratek opis, datum začetka, datum konca.
* Določiti mora oznake za dogodek.
* Pedagoški delavec in zunanji akter lahko ustvarita samo javne dogodke.
* Vnesti mora tudi prioriteto dogodka.
* Na dogodek lahko neposredno povabi tudi druge uporabnike aplikacije.


#### Alternativni tok

* Funkcionalnost nima alternativnih tokov.

#### Pogoji

* Uporabnik mora biti priavljen v sistem.


#### Posledice

* Dogodek je ustvarjen. Če ga ustvari študent se dogodek prenese neposredno na TO-DO seznam in koledar.

#### Posebnosti

* Funkcionalnost ne vsebuje nobenih posebnosti.

#### Prioriteta

* Ustvarjanje dogodka je Must have funkcionalnost.

#### Sprejemni test

| | | | |
|:---|:---|:---|:---|
| **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod** | **Pričakovani rezultat** |
| **Ustvarjanje dogodka - osnovni tok** | Prijavljeni študent odpre okno za dodajanje dogodka. | Vnese naslednje podatke: Ime: Izpit TPO, Opis: Izpit pri predmetu TPO, Od: 20.6.2019 - 11:00 Do: 20.6.2019 - 12:00, oznake: #Predmeti #TPO, Tip: javen, Pomembno: DA | Dogodek je ustvarjen in v študentovem koledarju/TO-DO seznamu.|

### Pregled ustvarjenih dogodkov

#### Povzetek funkcionalnosti

Zunanji akter in pedagoški delavec lahko pregledata seznam predhodno ustvarjenih dogodkov.

#### Osnovni tok

* Na prvi strani po prijavi v sistem zunanji akter ali pedagoški delavec vidi seznam dogodkov, ki jih je sam predhodno ustvaril.
* Omogočeno je tudi iskanje po dogodkih na vrhu seznama.
* Vsak dogodek ima ob sebi gumb za urejanje določenega dogodka.

#### Alternativni tok

Funkcionalnost nima alternativnega toka.

#### Pogoji

* Pedagoški delavec ali zunanji akter je prijavljen v sistem s svojim e-mailom in geslom.

#### Posledice

* Pedagoški delavec ali zunanji akter vidi seznam ustvarjenih dogodkov.


#### Posebnosti

* Funkcionalnost ne vsebuje nobenih posebnosti.

#### Prioriteta

* Pregled ustvarjenih dogodkov je Must have funkcionalnost.

#### Sprejemni test

| | | | |
|:---|:---|:---|:---|
| **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod** | **Pričakovani rezultat** |
| **Pregled ustvarjenih dogodkov - osnovni tok** | Pedagoški delavec ali zunanji akter je prijavljen v sistem s svojim e-mailom in geslom. | Obišče svojo začetno stran sistema. | Viden je seznam predhodno ustvarjenih dogodkov. |




### Spreminjanje ustvarjenih dogodkov

#### Povzetek funkcionalnosti

Zunanji akter, pedagoški delavec in študent lahko spreminjajo (jih posodobijo) dogodke, ki so jih predhodno ustvarili.

#### Osnovni tok

Pedagoški delavec, zunanji akter

* Iz seznama ustvarjenih dogodkov izbere enega izmed dogodkov in klikne na gumb za urejanje.
* Odpre se urejevalno okno, kjer lahko vidi trenutne lastnosti dogodka, lahko jih tudi ureja
* Ko zaključi urejanje, lahko shrani spremembe s pritiskom na gumb "Shrani" ali jih zavrže s pritiskom na gumb "Prekliči"

Študent

* Iz TO-DO seznama študent izbere dogodek, ki ga je sam predhodno ustvaril in klikne na gumb za urejanje.
* Odpre se urejevalno okno, kjer lahko vidi trenutne lastnosti dogodka, lahko jih tudi ureja
* Ko zaključi urejanje, lahko shrani spremembe s pritiskom na gumb "Shrani" ali jih zavrže s pritiskom na gumb "Prekliči"

#### Alternativni tok

Funkcionalnost nima alternativnega toka.

#### Pogoji

* Uporabnik (Pedagoški delavec, zunanji akter ali študent) je prijavljen v sistem in je predhodno že ustvaril dogodek.

#### Posledice

* Dogodek je posodobljen v celotni aplikaciji.

#### Posebnosti

* Funkcionalnost ne vsebuje nobenih posebnosti.

#### Prioriteta

* Spreminjanje dogodka je Must have funkcionalnost.

#### Sprejemni test

| | | | |
|:---|:---|:---|:---|
| **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod** | **Pričakovani rezultat** |
| **Spreminjanje dogodka - osnovni tok** | Pedagoški delavec je prijavljen v sistem s svojim e-mailom in geslom. | Pedagoški delavec iz seznama izbrre dogodek "TPO izpit" in mu v urejevalnem oknu zamenja datum začetka iz 20.6.2019 ob 11h na 20.6.2019 ob 12h. | Dogodke "TPO izpit" se posodobi v celotnem sistemu. |



### Pregled podrobnosti dogodka

#### Povzetek funkcionalnosti

Študent lahko pregleda podrobnosti dogodka, bodisi iz nabiralnika, TO-DO seznama ali koledarja, tako da nanj klikne.

#### Osnovni tok

* Študent obišče svoj nabiralnik dogodkov.
* Klikne na ime kateregakoli dogodka
* Odpre se mu okno s podrobnostmi o dogodku.
* Poleg imena, opisa, časa, oznak, pomembnosti in tipa je na voljo tudi vremenska napoved za čas dogodka, če je do njega manj kot 5 dni.
* Vremenska napoved se pridobi iz zunanjega sistema Open Weather Map API.
* Študent lahko dogodek prijavi moderatorju, ga komentira ali nanj povabi drugegega uporabnika.

#### Alternativni tok

Podrobnosti o dogodku iz TO-DO seznama ali koledarja

* Študent obišče svoj TO-DO seznam ali koledar
* Klikne na ime kateregakoli dogodka
* Odpre se mu okno s podrobnostmi o dogodku.
* Poleg imena, opisa, časa, oznak, pomembnosti in tipa je na voljo tudi vremenska napoved za čas dogodka, če je do njega manj kot 5 dni.
* Vremenska napoved se pridobi iz zunanjega sistema Open Weather Map API.
* Študent lahko dogodek prijavi moderatorju, ga komentira ali nanj povabi drugegega uporabnika.

#### Pogoji

* Študent je prijavljen v aplikacijo s svojim e-mailom in geslom. V nabiralniku, TO-DO seznamu ali koledarju ima vsaj en dogodek.

#### Posledice

* Študent pregleda podrobnosti dogodka in zapre okence.


#### Posebnosti

* Funkcionalnost uporablja zunanji sistem Open Weather Map API.

#### Prioriteta

* Pregled podrobnosti dogodka je Must have funkcionalnost.

#### Sprejemni test

| | | | |
|:---|:---|:---|:---|
| **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod** | **Pričakovani rezultat** |
| **Pregled podrobnosti dogodka - osnovni tok** | Študent ima v nabiralniku dogodek "TPO izpit" | Študent klikne na ime dogodka "TPO izpit" | Odpre se okence s podrobnostmi dogodka. |
| **Pregled podrobnosti dogodka - alternativni tok** | Študent ima na TO-DO seznamu dogodek "TPO izpit" | Študent klikne na ime dogodka "TPO izpit" | Odpre se okence s podrobnostmi dogodka. |

### Prijava spornih dogodkov

#### Povzetek funkcionalnosti

Študent lahko v okencu s podrobnostmi o dogodku prijavi sporen dogodek moderatorju.

#### Osnovni tok

* V okencu s podrobnostmi o dogodku je gumb "Prijavi"
* Študent lahko nanj klikne in odpre se še potrditveni dialog.
* Če študent potrdi prijavo, se dogodek prijavi kot sporen.
* Moderatorju se dogodek prikaže kot sporen.

#### Alternativni tok

Funkcionalnost nima alternativnih tokov.

#### Pogoji

* Dogodek obstaja in odprto je okence z njegovimi podrobnostmi.

#### Posledice

* Dogodek je v aplikaciji prijavljen kot sporen in je kot tak viden moderatorju.

#### Posebnosti

* Funkcionalnost ne vsebuje nobenih posebnosti.

#### Prioriteta

* Prijava spornega dogodka je Should have funkcionalnost.

#### Sprejemni test

| | | | |
|:---|:---|:---|:---|
| **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod** | **Pričakovani rezultat** |
| **Prijava spornega dogodka - osnovni tok** | Študent pregleduje podrobnosti dogodka "Zabava 123" | Pritisne gumb "Prijavi" in prijavo tudi potrdi z dialogom. | Dogodek "Zabava 123" je prijavljen kot sporen. |

### Povabilo uporabnika na dogodek

#### Povzetek funkcionalnosti

Študent lahko v okencu s podrobnostmi o dogodku nanj povabi drugega študenta.

#### Osnovni tok

* V okencu s podrobnostmi je vnosno polje "Povabi".
* V vnosno polje študent vnese e-mail naslove študentov, ki jih želi povabiti na ta dogodek.
* E-mail naslovi morajo biti ločeni s podpičjem
* Po končanem vnosu študent klikne gumb "Povabi".
* Sistem izpiše obvestilo o uspešnem povabilu.

#### Alternativni tok

Funkcionalnost nima alternativnega toka.

#### Pogoji

* Dogodek obstaja in odprto je okence z njegovimi podrobnostmi.

#### Posledice

* Povabljeni študenti dobijo dogodek na vrh svojega nabiralnika, tudi če niso naročeni na njegove oznake.


#### Posebnosti

* Funkcionalnost ne vsebuje nobenih posebnosti.

#### Prioriteta

* Povabilo uporabnika na dogodek je Could have funkcionalnost.

#### Sprejemni test

| | | | |
|:---|:---|:---|:---|
| **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod** | **Pričakovani rezultat** |
| **Povabilo uporabnika na dogodek - osnovni tok** | Študent pregleduje podrobnosti dogodka "Zabava 123" | Študent vnese e-mail test2@test.si; test3@test.si v vnosno polje za povabilo in potrdi povabila. | Študenta  test2@test.si in test3@test.si dobita dogodek "Zabava 123" na vrh svojega nabiralnika.|

### Komentiranje dogodka

#### Povzetek funkcionalnosti

Študent lahko v okencu s podrobnostmi o dogodku komentira omenjeni dogodek in vidi druge komentarje.

#### Osnovni tok

* Na dnu okenca s podrobnostmi o dogodku je prostor za komentarje in vnosno polje komentarja.
* Študent lahko prebere obstoječe komentarje.
* Pod obstoječimi komentarji je vnosno polje kamor lahko študent vpiše svoj nov komentar.
* Pritisne tipko Enter.
* Komentar se doda za zadnjim obstoječim komentarjem.

#### Alternativni tok

Funkcionalnost nima alternativnega toka.

#### Pogoji

* Dogodek obstaja in odprto je okence z njegovimi podrobnostmi.

#### Posledice

* Nov komentar se doda med ostale komentarje na dnu okenca s podrobnostmi.


#### Posebnosti

* Funkcionalnost ne vsebuje nobenih posebnosti.

#### Prioriteta

* Povabilo uporabnika na dogodek je Would have funkcionalnost.

#### Sprejemni test

| | | | |
|:---|:---|:---|:---|
| **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod** | **Pričakovani rezultat** |
| **Komentiranje dogodka - osnovni tok** | Študent pregleduje podrobnosti dogodka "Zabava 123" | Študent vnese nov komentar z vsebino  "Kul!!" | Komentar "Kul!!" se pojavi med ostalimi komentarji za zadnjim komentarjem.|

### Pregled nabiralnika in urejanje TO-DO seznama

#### Povzetek funkcionalnosti

Študent lahko pregleda svoj nabiralnik dogodkov in doda dogodke na svoj TO-DO seznam in koledar.

#### Osnovni tok

Dodajanje dogodka iz nabiralnika na TO-DO seznam

* Zaslon je razdeljen na dva dela.
* V levem delu se nahaja nabiralnik dogodkov.
* Na vrhu nabiralnika so zbrani dogodki na katere je bil prijavljeni študent povabljen.
* Nižje v nabiralniku so podani dogodki z oznakami na katere je študent naročen.
* V nabiralniku so dogodki urejeni glede na pomembnost.
* Vsak dogodek gumb "Dodaj".
* Z gumbom "Dodaj" študent doda dogodek na svoj TO-DO seznam oz. koledar.
* V desnem delu je prikazan TO-DO seznam z dogodki urejenimi po pomembnosti.

#### Alternativni tokovi

Brisanje dogodka iz nabiralnika

* Zaslon je razdeljen na dva dela.
* V levem delu se nahaja nabiralnik dogodkov.
* Na vrhu nabiralnika so zbrani dogodki na katere je bil prijavljeni študent povabljen.
* Nižje v nabiralniku so podani dogodki z oznakami na katere je študent naročen.
* V nabiralniku so dogodki urejeni glede na pomembnost.
* Vsak dogodek ima gumb X, s katerim študent zbriše dogodek iz svojega nabiralnika.

Brisanje dogodka iz TO-DO seznama

* Na TODO seznamu v desnem delu zaslona je poleg vsakega dogodka gumb X.
* Če študent nanj klikne, se dogodek odstrani iz TO-DO seznama in koledarja.



#### Pogoji

* Študent je prijavljen v sistem in v njegovem nabiralniku so dogodki.

#### Posledice

* Ob dodajanju določenega dogodka na TO-DO seznam se ta prestavi iz nabiralnika na TO-DO seznam.
* Dogodek se zbriše iz nabiralnika.
* Dogodek se zbriše iz TO-DO seznama.

#### Posebnosti

* Funkcionalnost nima posebnosti.

#### Prioriteta

* Pregled nabiralnika in urejanje TO-DO seznama je Must have funkcionalnost.

#### Sprejemni test

| | | | |
|:---|:---|:---|:---|
| **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod** | **Pričakovani rezultat** |
| **Dodajanje na TO-DO seznam - osnovni tok** | Študent ima v nabiralniku dogodek "Zabava 123" v nabiralniku. | Študent klikne "Dodaj" poleg dogodka "Zabava 123" | Dogodek "Zabava 123" se prestavi iz nabiralnika na TO-DO seznam na desni strani zaslona. |
| **Brisanje dogodka iz nabiralnika - alternativni tok** | Študent ima v nabiralniku dogodek "Zabava 123" v nabiralniku. | Študent klikne simbol X poleg dogodka "Zabava 123" | Dogodek "Zabava 123" se zbriše iz nabiralnika. |
| **Brisanje dogodka iz TO-DO seznama - alternativni tok** | Študent ima na TO-DO seznamu dogodek "Zabava 123" v nabiralniku. | Študent klikne simbol X poleg dogodka "Zabava 123" na TO-DO seznamu. | Dogodek "Zabava 123" se zbriše iz TO-DO seznama. |

### Pregled koledarja in TO-DO seznama

#### Povzetek funkcionalnosti

Študent lahko pregleda svoj koledar in TO-DO seznam.

#### Osnovni tok

* Sistem odpre stran, kjer je zaslon razdeljen na dva dela
* V levem delu zaslona je koledar z vnešenimi dogodki.
* V desnem delu zaslona je TO-DO seznam, enak kot pri prejšnji funkcionalnosti.
* S klikom na katerikoli dogodek, se odpre okence s njegovimi podrobnostmi.

#### Alternativni tok

Funkcionalnost nima alternativnega toka.

#### Pogoji

* Študent je prijavljen v sistem in ima dogodke na koledarju oz. TO-DO seznamu.

#### Posledice

* Funkcionalnost nima eksplicitnih posledic, služi zgolj za prikaz koledarja oz. TO-DO seznma

#### Posebnosti

* Funkcionalnost ne vsebuje nobenih posebnosti.

#### Prioriteta

* Pregled koledarja in TO-DO seznama je Must have funkcionalnost.

### Sprejemni test

| | | | |
|:---|:---|:---|:---|
| **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod** | **Pričakovani rezultat** |
| **Pregled koledarja in TO-DO seznama - osnovni tok** | Študent je prijavljen v aplikaciji. | Študent odpre stran s koledarjem in TO-DO seznamom  | Prikaže se koldar in TO-DO seznam. |

### Naročanje na oznake

#### Povzetek funkcionalnosti

Študent se lahko naroči na oznake, ki so zabeležene v seznamu ali prekine naročanino na neko oznako.

#### Osnovni tok

* Sistem odpre stran, kjer je zaslon razdeljen na dva dela.
* Na levi strani so vse oznake, ki so zabeležene v aplikaciji.
* Seznam vseh oznak se posodobi avtomatsko, ko se ustvari dogodek z novo oznako.
* Na desni strani so vse oznake na katere je študent naročen.
* Na levi strani lahko študent klikne potrditveni simbol (plus) poleg oznake na katero se želi naročiti.
* Oznaka se prestavi na desni del zaslona.

#### Alternativni tok

Prekinitev naročnine na oznako

* Sistem odpre stran, kjer je zaslon razdeljen na dva dela.
* Na levi strani so vse oznake, ki so zabeležene v aplikaciji.
* Seznam vseh oznak se posodobi avtomatsko, ko se ustvari dogodek z novo oznako.
* Na desni strani so vse oznake na katere je študent naročen.
* Študent na desni strani zaslona poleg določene oznake klikne simbol -.
* Oznaka se premakne na levi del zaslona.


#### Pogoji

* Študent je prijavljen v sistem in sistem prikazuje stran za naročanje na oznake.
* V sistemu so že oznake.

#### Posledice

* Študent se naroči na izbrano oznako.
* Študent preneha z naročnino na izbrano oznako.

#### Posebnosti

* Funkcionalnost ne vsebuje nobenih posebnosti.

#### Prioriteta

* Naročanje na oznake je Must have funkcionalnost.

### Sprejemni test

| | | | |
|:---|:---|:---|:---|
| **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod** | **Pričakovani rezultat** |
| **Naročanje na oznake - osnovni tok** | Sistem prikazuje stran za naročanje na oznake.  | Študent klikne znak + poleg oznae #FRI na levi strani zaslona.  | Oznaka se prestavi na desno stran in študent je naročen na oznako #FRI. |
| **Prekinitev naročnine na oznako - alternativni tok** | Sistem prikazuje stran za naročanje na oznake. Študent je naročen na oznako #FRI.  | Študent na desni strani zaslona klikne simbol - poleg oznake #FRI.  | Oznaka se prestavi na levo stran zaslona in študent ni več naročen na oznako #FRI. |

## 6. Nefunkcionalne zahteve

##### Zahteve izdelka 

Varnost: 

* Sistem mora biti zaščiten nepooblaščenega dostopa do pooblaščenih virov. Uporabom sistemov za avtentikacijo in avtorizacijo.
* Uporabnik lahko dostopa, ureja in briše samo lastne podatke in podatke katere je ustvaril.
* Moderator je edini akter, ki ima pravico pregleda nad vsemi javnimi dogodki, oznakami in uporabniki
* Moderator ima pravico brisanja: oznak, dogodka in uporabnika. Moderator lahko izbriše katerikoli uporabniški račun razen moderaterskega.
* Moderator ima funkcijo potrjevanja uporabniških računov: pedagoškega delavca in zunanjega akterja.

Uporabnost:

* Uporabniške iskušnje pri uporabi storitvah morajo biti intuitivne, razumljive in sodobnega izgleda. Čas učenja uporabe sistema ne sme presegati več kot 15min.
* Za katerokoli akcijo uporabnik ne sme proizvesti več kot 7 pritiskov gumba.
* Sistem mora omogočati hkratno povezavo vsaj 200-tim uporabnikom. Odzivni časi nesmejo presegati več kot 2 sekunde.
* Odzivni časi za brskanje med oznakami in dogodkima nesme trajati več kot 2 sekunde v normalnih obremenitvah.
  
Zanesljivost:

* V primeru napake znotraj sistema je potrebno napake ustrezno obravnavati in beležiti zgodovino in uzrok napak v predvidene datoteke. Sistem ne sme prenehati z delom če pride do napake.
* Vnosna okna morajo preprečiti uporabniku nedovoljene znake in nize. Uporabnika se mora ustrezno obavestiti o dovoljenih vnosnih znakih.
* V primeru neuspešne operacije zaradi napake pri shranjevanju ali obdelavi podatkov je treba uporabnika ustrezno obavestiti o napaki in napako ustrezno obravnavati in povrniti stanje sistema.

Razpoložljivost:

* Sistemske storitve so razpoložljive in lahko dostopne vsem uporabnikom v ketermkoli trenutku. 
* Obdobje nedelovanja zaradi vzdrževanja ne sme presegati več kot 2 uri. V primeru nedelovanja katerekoli od storitev zardi vzdrževanja se uporabniku mora prikazati obavestilo o ne delovanju.

Izvedba:

* Nalaganje katerekoli strani in podatkov ne sme presegati več kot 2 sekunde pri normalni obremeniti sistema in povezave.

Razširljivost:

* Sistem mora biti skalabilan in pri večjem številu povpraševanju po storitvah bi skalirali sistem uporabom dodatnih strežnikov ali povečanjem zmogljivosti strežnika.

##### Organizacijske zahteve

* Račun pedagoškega delavca ali zunanjega akterja mora moderator najprej odobriti, predan se navedeni računi lahko začnejo uporabljati.
* Račun moderatora mora biti v naprej pripravljen in ni možno ustvarjanje novih računov z moderatorskim pravicami.

##### Zunanje zahteve

Interoperabilnost:

* Dostop do sistema je mogoč preko spletnih brskalnikov FireFox zadnja verzija in Google Chrome zadnja verzija. 	
* Sistem bo lahko deloval na OS Red Hat Enterprise Linux 6.5.

Zakonodaja: 

* Upravljanje uporabniškim podatkima je skaldno z uredbom Evropske unije o varstvu podatkov (GDPR).


## 7. Prototipi vmesnikov

### Zaslonske maske za vlogo: Neprijavljen uporabnik

![Stran za prijavo](../img/prijava.PNG)

* Ta zalonska maska se uporablja v primeru uporabe prijave v sistem.

![Stran za registracijo](../img/registracija.PNG)

* Ta zaslonska maska se uporablja v primeru uporabe registracije.

### Zaslonske maske za vlogo: Študent

![Stran za dogodke](../img/dogodki.PNG)

* Ta zaslonska maska se uporablja v primeru uporabe pregleda nabiralnika in to-do seznama.

![Stran za koledar](../img/koledar.PNG)

* Ta zaslonska maska se uporablja v primeru uporabe pregled kolendarja in to-do seznama.

![Stran za oznake](../img/oznake.PNG)

* Ta zaslonska maska se uprablja v primeru uporabe naročanje na oznake.

### Zaslonske maske za vlogo: Pedagoški delavec ali zunanji akter

![Stran za dogodke za nestudente](../img/nestudenti_dogodki.PNG)

* ta zaslonska maska se uprablja v primeru uporabe pregleda ustvarjenih dogodkov.

### Zaslonske maske za vlogo: Moderator

![Stran za moderatorja](../img/moderator.PNG)

* ta zaslonska maska se uprablja v primeru uporabe Uporavljanja z uporabniki, dogodki in oznakami.

### Zaslonske maske za vlogo: Skupno 

![Stran za podrobnosti dogodka](../img/dogodek_podrobnosti.PNG)

* ta zaslonska maska se uprablja v primeru uporabe prijave spornega dogodka, komentiranje dogodka, pregleda podrobnosti dogodka in povabilo uporabnika na dogodek. Komentiranje, prijava in povabila so omogočena zgolj uporabniški vlogi študent.

![Stran za ustvarjanje in posodabljanje dogodka](../img/dogodek_ustvari_posodobi.PNG)

* ta zaslonska maska se uprablja v primeru uporabe ustvarjanja dogodkov in spreminjanja ustvarjenega dogodka. Omejena za vloge: študent, pedagoški delavec in zunanji akter.

![Stran za odjavo](../img/odjava.PNG)

* ta zaslonska maska se uprablja v primeru uporabe odjave iz sistema.


##### Vmesnik OpenWeatherMap API, za pridobivanje podatkov o vremenu v naslednjih petih dni v razmiku od tri ure. Uporabljamo v primeru, pregleda podrobnosti dogodka. Primer:

API KLIC:
http://api.openweathermap.org/data/2.5/forecast?zip=1000,SI&appid=XXXXXXXXXXXXXXXXXXXXXX 

ZIP: 
Poštna številka mesta in oznaka države
1000 = Ljubljana
SI = SLovenija

APPID:
API KEY za dostop do api-ja.

ODGOVOR:

```
{
    "cod": "200",
    "message": 0.0056,
    "cnt": 40,
    "list": [
        {
            "dt": 1554649200,
            "main": {
                "temp": 285.62,
                "temp_min": 284.341,
                "temp_max": 285.62,
                "pressure": 1005.1,
                "sea_level": 1005.1,
                "grnd_level": 924.46,
                "humidity": 65,
                "temp_kf": 1.28
            },
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04d"
                }
            ],
            "clouds": {
                "all": 80
            },
            "wind": {
                "speed": 1.31,
                "deg": 114.503
            },
            "rain": {},
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2019-04-07 15:00:00"
        },

    ...    

```
