# Dokument zahtev

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Aleksandar Hristov, Andrija Vučković, Jernej Vrhunc in Robi Markač |
| **Kraj in datum** | Zagreb, 7.4.2019 |



## Povzetek projekta

Z uvedbo bolonjskega sistema v Sloveniji se je povečala količina sprotnega dela in zmanjšal čas za izvedbo tega dela, kar
je privedlo do tega, da se veliko študentov sooča s stresom in problemom težke glave, saj si morajo zapomniti veliko rokov za oddajo, izpite,
kolokvije in vse ostale sprotne obveznosti. Naš cilj je razviti spletno aplikacijo, ki bo pomagala slediti, planirati in izpolnjevati te
študijske obveznosti. Spletni vmesnik bo v obliko koledarja ali seznama omogočal jasen pregled nad vsemi obveznostmi, ki jih bo uporabnik vnesel,
prioritetno določanje in opozarjanje prihajajočih obveznosti, ter beleženje rezultatov opravljenih obveznosti.
Z pomočjo aplikacije se bo tako študent organizirano in učinkovito spopadal z vsemi študijskimi obveznostmi.



## 1. Uvod

Glavni namen te aplikacije je rešiti problem organizacije velike količine študijskih obveznosti s katerimi se srečujejo študenti,
saj so posledice neuspešno ali zamujeno opravljene obveznosti lahko usodne. V aplikacijo se bo uporabnik mogel prijavit oz. 
registrirat če še nima računa, saj se ga bo tako lahko profiliralo in prikazalo ustrezne podatke(obveznosti,preverjanja, itd..).
Uporabnik bo lahko vnesel obveznost, preverjanje in urnik, kateri se mu bojo prikazovali na koledarju. V aplikaciji bodo
štiri vrste uporabniških računov, zraven navadnega uporabnika bodo še:
 * profesor, ki lahko dodaja preverjanje glede na predmet
 * urednik, ki lahko ureja preverjanja in predmete
 * administrator, ki lahko dodaja in ureja račune, ter dodaja in ureja predmete in urnik
Osrednji del aplikacije bo koledar, na katerem se bodo prikazovale obveznosti, preverjanje in urnik. Koledar bo lahko prikazan
v dnevni, tedenski, mesečni in letni obliki. Uporabnik bo imel tudi možnost kreiranja "TODO liste", v katero bo lahko izbral še
neopravljene obveznosti in preverjanja in jih sortiral po prioriteti ter datumu.





## 2. Uporabniške vloge

Aplikacija StraightAs bo imela dve roli:
* UPORABNIK - pod katerega spadajo vsi bodoči aktivni uporabniki aplikacije, ki se šolajo (študenti, dijaki, itd..), katerim bodo na voljo funkcionalnosti:
    * Registracija in vpis
    * Koledar in seznam vseh obveznosti
    * Označevanje obveznosti glede na prioriteto
    * Dodajanje obveznosti
    * Dodajanje urnika
    * prijava na predmet
    * kreiranje TODO liste
* ADMINISTRATOR - to je sistemski uporabnik, ki ima nasljednje funkcionalnosti nad vsemi uporabniki:
    * Kreiranje, urejanje in brisanje uporabniškega računa
    * Dodajanje predmeta
    * Dodajanja urnika
* UREDNIK - to je sistemski uporabnik, ki ima nasljednje funkcionalnosti nad vsemi uporabniki:
    * Urejanje preverjanja
    * Dodajanje predmeta
    * Urejanje predmeta
* PROFESOR - to je uporabnik z vlogo profesorja, ki ima nasljednje funkcionalnosti nad vsemi uporabniki:
    * Dodajanje preverjanja
    

## 3. Slovar pojmov

* Predmet - to je šolski predmet, ki ga vodi profesor in se uporabnik lahko prijavi nanj
* Preverjanje - to je kakršnokoli šolsko preverjanje znanja, ki ga opravlja uporabnik
* Obveznost - to je kakršnokoli dogodek ali stvar, ki jo mora uporabnik izpolnit v bližnji prihodnosti.
* Urnik - šolski urnik, katerega lahko ima uporabnik
* Račun oz Uporabniški račun - to je identifikator uporabnika, da podlagi katerega selektiramo prikaz podatkov
* TODO lista - seznam še neizpolnjenih obveznosti in preverjanj
* Koledar - to je šolski koledar, na katerem so prikazane vse obveznosti, preverjanja in urnik uporabnika



## 4. Diagram primerov uporabe

![Use Case Diagram](../img/UseCaseDiagram.png)

## 5. Funkcionalne zahteve

### 1. Registracija

#### Povzetek funkcionalnosti

Uporabnik se lahko registrira na spletni strani z vnosom potrebnih podatkov.

#### Osnovni tok

**UPORABNIK**

1. Uporabnik izbere funkcionalnost _Registracija_.
2. Sistem pokaže polja za vnos, ki so potrebna za registracijo.
3. Uporabnik vnese potrebne podatke za registracijo in s klikom na gumb potrdi vnos.
4. Sistem prikaže sporočilo o uspešni registraciji.

#### Izjemni tokovi
+ Uporabnik se ne more registrirati, saj je elektronski naslov že zaseden. Sistem pokaže ustrezno obvestilo.
+ Uporabnik se ne more registrirati, saj je uporabniško ime že zasedeno. Sistem pokaže ustrezno obvestilo.
+ Uporabnik se ne more registrirati, saj gesli nista enaki. Sistem pokaže ustrezno obvestilo.
+ Uporabnik se ne more registrirati, saj geslo ni pravilnega formata. Sistem pokaže ustrezno obvestilo.

#### Pogoji

Ni nobenih pogojev.

#### Posledice

1. Število uporabnikov se v bazi poveča za 1.

#### Posebnosti

1. Unikaten email
2. Unikatno uporabniško ime

#### Prioritete identificiranih funkcionalnosti

**MUST HAVE**

#### Sprejemni testi

+ Uporabnik naj vpiše v ustrezna polja svoje ime, priimek, elektronski naslov, uporabniško ime, geslo in še enkrat ponovi geslo ter potrdi registracijo.
+ Uporabnik naj vpiše obstoječi elektronski naslov.
+ Uporabnik naj vpiše že obstoječe uporabniško ime.
+ Uporabnik naj vpiše različni gesli.

### 2. Vpis

#### Povzetek funkcionalnosti

Uporabnik ali Administrator se lahko vpišeta na spletni strani, tako da vneseta uporabniško ime in geslo.

#### Osnovni tok

**UPORABNIK**

1. Uporabnik izbere funkcionalnost _Vpis_.
2. Sistem pokaže polja za vnos, ki so potrebna za vpis.
3. Uporabnik vnese potrebne podatke za vpis in s klikom na gumb potrdi vnos.
4. Sistem prikaže privzeto stran od uporabnika.

**ADMINISTRATOR**

1. Administrator izbere funkcionalnost _Vpis_.
2. Sistem pokaže polja za vnos, ki so potrebna za vpis.
3. Administrator vnese potrebne podatke za vpis in s klikom na gumb potrdi vnos.
4. Sistem prikaže privzeto stran od administratorja.

#### Izjemni tokovi
+ Uporabnik ali administrator se ne more vpisati v sistem, zaradi napačnega uporabniškega imena.
+ Uporabnik ali administrator se ne more vpisati v sistem, zaradi napačnega gesla.
+ Uporabnik ali administrator se ne more vpisati v sistem, zaradi napačnega uporabniškega imena in gesla.

#### Pogoji

+ Uporabnik se mora najprej registrirati

#### Posledice

1. Število trenutnih uporabnikov se poveča za 1.

#### Posebnosti

Ni posebnosti.

#### Prioritete identificiranih funkcionalnosti

**MUST HAVE**

#### Sprejemni testi

+ Uporabnik naj vpiše v polji svoje uporabniško ime in geslo.
+ Uporabnik naj vpiše napačno uporabniško ime.
+ Uporabnik naj vpiše napačno geslo.
+ Uporabnik naj vpiše napačno uporabniško ime in geslo.
+ Administrator naj vpiše v polji svoje uporabniško ime in geslo.
+ Administrator naj vpiše napačno uporabniško ime.
+ Administrator naj vpiše napačno geslo.
+ Administrator naj vpiše napačno uporabniško ime in geslo.

### 3. Koledar - dan

#### Povzetek funkcionalnosti

Uporabnik lahko pregleda svoj koledar za točno določen dan.

#### Osnovni tok

**UPORABNIK**

1. Uporabnik izbere funkcionalnost _Koledar - dan_.
2. Sistem pokaže koledar za izbran dan.

#### Alternativni tokovi

**Alternativni tok 1**

1. Uporabnik izbere funkcionalnost _Koledar - teden_.
2. Uporabnik klikne za prikaz funkcionalnosti _Koledar - dan_.


#### Izjemni tokovi
Ni izjemnih tokov.

#### Pogoji

+ Uporabnik se mora najprej vpisati.

#### Posledice

Ni posledic.

#### Posebnosti

Ni posebnosti.

#### Prioritete identificiranih funkcionalnosti

**SHOULD HAVE**

#### Sprejemni testi

+ Uporabnik naj izbere željeni dan v letu.
+ Uporabnik naj izbere funkcionalnost _Koledar - teden_ in naj nato klikne _Koledar - dan_.

### 4. Koledar - mesec

#### Povzetek funkcionalnosti

Uporabnik lahko pregleda svoj koledar za točno določen mesec.

#### Osnovni tok

**UPORABNIK**

1. Uporabnik izbere funkcionalnost _Koledar - mesec_.
2. Sistem pokaže koledar za izbran mesec.

#### Alternativni tokovi

**Alternativni tok 1**

1. Uporabnik izbere funkcionalnost _Koledar - leto_.
2. Uporabnik klikne za prikaz funkcionalnosti _Koledar - mesec_.

**Alternativni tok 1**

1. Uporabnik izbere funkcionalnost _Koledar - dan_.
2. Uporabnik klikne za prikaz funkcionalnosti _Koledar - mesec_.

#### Izjemni tokovi
Ni izjemnih tokov.

#### Pogoji

+ Uporabnik se mora najprej vpisati.

#### Posledice

Ni posledic.

#### Posebnosti

Ni posebnosti.

#### Prioritete identificiranih funkcionalnosti

**MUST HAVE**

#### Sprejemni testi

+ Uporabnik naj izbere željeni mesec v letu.
+ Uporabnik naj izbere funkcionalnost _Koledar - leto_ in naj nato klikne _Koledar - mesec_.
+ Uporabnik naj izbere funkcionalnost _Koledar - dan_ in naj nato klikne _Koledar - mesec_.

### 5. Koledar - leto

#### Povzetek funkcionalnosti

Uporabnik lahko pregleda svoj koledar za točno določeno leto.

#### Osnovni tok

**UPORABNIK**

1. Uporabnik izbere funkcionalnost _Koledar - leto_.
2. Sistem pokaže koledar za izbrano leto.

#### Alternativni tokovi

**Alternativni tok 1**

1. Uporabnik izbere funkcionalnost _Koledar - mesec.
2. Uporabnik klikne za prikaz funkcionalnosti _Koledar - leto_.

#### Izjemni tokovi
+ Ni izjemnih tokov.

#### Pogoji

+ Uporabnik se mora najprej vpisati.

#### Posledice

+ Ni posledic.

#### Posebnosti

+ Ni posebnosti.

#### Prioritete identificiranih funkcionalnosti

**MUST HAVE**

#### Sprejemni testi

+ Uporabnik naj izbere željeno leto.
+ Uporabnik naj izbere funkcionalnost _Koledar - mesec_ in naj nato klikne _Koledar - leto_.

### 6. Koledar - teden

#### Povzetek funkcionalnosti

Uporabnik lahko pregleda svoj koledar za točno določen teden.

#### Osnovni tok

**UPORABNIK**

1. Uporabnik izbere funkcionalnost _Koledar - teden_.
2. Sistem pokaže koledar za izbrano leto.

#### Alternativni tokovi

**Alternativni tok 1**

1. Uporabnik izbere funkcionalnost _Koledar - mesec.
2. Uporabnik klikne za prikaz funkcionalnosti _Koledar - teden_.

**Alternativni tok 2**

1. Uporabnik izbere funkcionalnost _Koledar - dan.
2. Uporabnik klikne za prikaz funkcionalnosti _Koledar - teden_.

#### Izjemni tokovi

Ni izjemnih tokov.

#### Pogoji

+ Uporabnik se mora najprej vpisati.

#### Posledice

+ Ni posledic.

#### Posebnosti

+ Ni posebnosti.

#### Prioritete identificiranih funkcionalnosti

**MUST HAVE**

#### Sprejemni testi

+ Uporabnik naj izbere željeno leto.
+ Uporabnik naj izbere funkcionalnost _Koledar - mesec_ in naj nato klikne _Koledar - teden_.
+ Uporabnik naj izbere funkcionalnost _Koledar - dan_ in naj nato klikne _Koledar - teden_.

### 7. Dodaj urnik

#### Povzetek funkcionalnosti

Uporabnik lahko ročno dodaja predmete v svoj koledar, tako da vpiše potrebne podatke. Lahko pa to namesto njega naredi Administrator.

#### Osnovni tok

**UPORABNIK**

1. Uporabnik izbere funkcionalnost _Dodaj urnik_.
2. Sistem pokaže obrazec v katerega je treba vnesti predmet, tip, dan, ura, trajanje, predavalnica, naslov in opis predmeta.
3. Uporabnik potrebne podatke vpiše.
4. Uporabnik s klikom na gumb potrdi podatke.
5. Sistem pokaže sporočilo o uspešnem dodajanju na koledar.

**ADMINISTRATOR**

1. Administrator izbere funkcionalnost _Dodaj urnik_.
2. Sistem pokaže obrazec v katerega je treba vnesti predmet, tip, dan, ura, trajanje, predavalnica, naslov in opis predmeta ter katerim Uporabnikom želi dodati predmet na koledarju.
3. Administrator potrebne podatke vpiše.
4. Administrator s klikom na gumb potrdi podatke.
5. Sistem pokaže sporočilo o uspešnem dodajanju na koledar.

#### Alternativni tokovi

**Alternativni tok 1**

1. Administrator izbere funkcionalnost _Dodaj urnik_.
2. Sistem pokaže obrazec v katerega je treba vnesti predmet, tip, dan, ura, trajanje, predavalnica, naslov in opis predmeta ter katerim Uporabnikom želi dodati predmet na koledarju.
3. Administrator potrebne podatke vpiše in izbere točno določenega Uporabnika.
4. Administrator s klikom na gumb potrdi podatke.
5. Sistem pokaže sporočilo o uspešnem dodajanju na koledar.

**Alternativni tok 2**

1. Urnik se uvozi s pomočjo FRI urnik API-ja.

#### Izjemni tokovi

+ Uporabnik je že v preteklosti dodal isti predmet.
+ Uporabnikov koledar že usebuje predmet na isti dan ob isti uri (popolno oz. delno prekrivanje predmetov).
+ Administrator je že v preteklosti dodal isti predmet istemu Uporabniku.
+ Administrator je utipkal neobstoječe uporabniško ime
+ Administrator/Uporabnik vpiše napačno vpisno številko v FRI-jev API za urnik

#### Pogoji

+ Uporabnik se mora najprej vpisati.
+ Administrator se mora najprej vpisati

#### Posledice

+ Eden ali več zapisov v bazi.

#### Posebnosti

+ FRI-jev API za urnik mora delovati.

#### Prioritete identificiranih funkcionalnosti

**WOULD HAVE**

#### Sprejemni testi

+ Uporabnik naj vpiše potrebne podatke za dodajanje predmeta
+ Uporabnik naj doda predmet, ki ga je že v preteklosti dodal.
+ Uporabnik naj doda predmet, ki se prekriva z drugim v celoti.
+ Uporabnik naj doda predmet, ki se prekriva delno z drugim.
+ Administrator naj vpiše potrebne podatke za dodajanje predmeta za več uporabnikov. 
+ Administrator naj vpiše potrebne podatke za dodajanje predmeta za enega uporabnika.
+ Administrator naj doda tak predmet Uporabniku, da je že vsebovan v njegovem koledarju.
+ Administrator naj doda predmet neobstoječemu uporabniku.
+ Ob klicu na FRI-jev API utipkamo neobstoječo vpisno številko


### 8. Prijava na predmet

#### Povzetek funkcionalnosti

Uporabnik se lahko prijavi na razpisane predemte, ki jih je objavil Administrator ali Urednik, z namenom, da dobi obvestilo na koledarju o prihajajočih preverjanjih.

#### Osnovni tok

**UPORABNIK**

1. Uporabnik izbere funkcionalnost _Prijava na predmet_.
2. Sistem pokaže obrazec, kjer imaš na izbiro vse predmete.
3. Uporabnik označi željene predmete
4. Uporabnik potrdi izbro predmetov
5. Sistem pokaže sporočilo o uspešnem vpisu predmetov.

#### Izjemni tokovi

+ Uporabnik je že vpisan v izbrani predmet.

#### Pogoji

+ Uporabnik se mora najprej vpisati v aplikacijo.

#### Posledice

+ Število predmetov Uporabnika se poveča

#### Posebnosti

+ Ni posebnosti.

#### Prioritete identificiranih funkcionalnosti

**SHOULD HAVE**

#### Sprejemni testi

+ Uporabnik naj izbere željene predmete.
+ Uporabnik naj izbere predmet v katerem je že vpisan.

### 9. Uredi obveznost

#### Povzetek funkcionalnosti

Uporabnik lahko ureja ali pa izbriše svoje obveznosti, ki jih je v preteklosti dodal.

#### Osnovni tok

**UPORABNIK**

1. Uporabnik izbere funkcionalnost _Uredi obveznost_.
2. Sistem pokaže stran, kjer so navoljo podatki o obveznosti in so pripravljeni za urejanje.
3. Uporabnik željene podatke uredi ali pa se odloči, da obveznost izbriše.
4. Uporabnik s klikom na gumb potrdi izbiro.
5. Sistem pokaže sporočilo o uspešnem urejanju podatkov.

#### Izjemni tokovi

+ Uporabnik pusti prazna polja v obrazcu.

#### Pogoji

+ Uporabnik se mora najprej vpisati.
+ Uporabnik mora imeti vsaj eno obveznost dodano.

#### Posledice

+ Spremenjen zapis v bazi.

#### Posebnosti

+ Ni posebnosti

#### Prioritete identificiranih funkcionalnosti

**COULD HAVE**

#### Sprejemni testi

+ Uporabnik naj uredi željene podatke.
+ Uporabnik naj izbriše obveznost.
+ Uporabnik naj pusti prazno polje v obrazcu.

### 10. Pregled obveznosti

#### Povzetek funkcionalnosti

Uporabnik lahko pogleda podrobnosti obveznosti, ki jih je v preteklosti dodal.

#### Osnovni tok

**UPORABNIK**

1. Uporabnik izbere funkcionalnost _Pregled obveznosti_.
2. Sistem pokaže stran, kjer so navoljo podatki o obveznosti in še Google maps za lokacijo.
3. Uporabnik lahko upravlja z Google maps.

#### Izjemni tokovi

+ Ni izjemnih tokov.

#### Pogoji

+ Uporabnik se mora najprej vpisati.
+ Uporabnik mora imeti vsaj eno obveznost dodano.

#### Posledice

+ Ni posledic.

#### Posebnosti

+ Ni posebnosti

#### Prioritete identificiranih funkcionalnosti

**COULD HAVE**

#### Sprejemni testi

+ Uporabnik naj klikne na eni obveznosti

### 11. Dodaj račune

#### Povzetek funkcionalnosti

Administrator lahko ročno dodaja račune tipa Uporabnik, Profesor ali Urednik.

#### Osnovni tok

**ADMINISTRATOR**

1. Administrator izbere funkcionalnost _Dodaj račun_.
2. Sistem prikaže stran, kjer je na voljo obrazec z vnosnimi polji za dodajanje računa.
3. Administrator vnese potrebne podatke za dodajanje računa in s klikom na gumb potrdi vnos.
4. Sistem prikaže sporočilo o uspšeni dodaji računa.

#### Izjemni tokovi

+ Računa ni mogoče dodati, saj je elektronski naslov že zaseden. Sistem pokaže ustrezno obvestilo.
+ Računa ni mogoče dodati, saj je uporabniško ime že zasedeno. Sistem pokaže ustrezno obvestilo.
+ Računa ni mogoče dodati, saj gesli nista enaki. Sistem pokaže ustrezno obvestilo.
+ Računa ni mogoče dodati, saj geslo ni pravilnega formata. Sistem pokaže ustrezno obvestilo.

#### Pogoji

+ Administrator se mora najprej vpisati.

#### Posledice

+ Dodan račun v podatkovno bazo.

#### Posebnosti

+ Ni posebnosti

#### Prioritete identificiranih funkcionalnosti

**MUST HAVE**

#### Sprejemni testi

+ Administrator naj vpiše v ustrezna polja ime, priimek, elektronski naslov, uporabniško ime, geslo in še enkrat ponovi geslo ter potrdi dodajanje računa.
+ Administrator naj vpiše obstoječi elektronski naslov.
+ Administrator naj vpiše že obstoječe uporabniško ime.
+ Administrator naj vpiše različni gesli. 
+ Administrator naj vpiše geslo napačnega formata. 

### 12. Uredi račun

#### Povzetek funkcionalnosti

Administrator lahko spreminja podatke računov in jih tudi po možnosti izbriše.

#### Osnovni tok

**ADMINISTRATOR**

1. Administrator izbere funkcionalnost _Uredi račun_.
2. Sistem prikaže seznam vseh računov.
3. Administrator izbere račun, katerega želi urediti.
4. Sistem prikaže stran, kjer je na voljo obrazec z vnosnimi polji za urejanje računa in gumb za izbris računa.
5. Administrator vnese potrebne podatke, ki jih želi urediti in s klikom na gumb potrdi spremembe ali Administrator klikne na gumb "Izbriši račun" in s klikom na "Potrdi" potrdi brisanje računa.
6. Sistem prikaže sporočilo o uspešni akciji nad računom.

#### Izjemni tokovi

+ Računa ni mogoče urediti, saj vnešeni podatki niso utrezni.

#### Pogoji

+ Administrator se mora najprej vpisati.

#### Posledice

+ Posodobljen ali izbrisan račun iz podatkovne baze.

#### Posebnosti

+ Ni posebnosti

#### Prioritete identificiranih funkcionalnosti

**SHOULD HAVE**

#### Sprejemni testi

+ Administrator naj vpiše v ustrezna polja tiste podatke, ki jih želi spremeniti in potrdi spremembe.
+ Administrator naj vpiše obstoječi elektronski naslov.
+ Administrator naj vpiše že obstoječe uporabniško ime.
+ Administrator naj vpiše različni gesli. 
+ Administrator naj vpiše geslo napačnega formata.
+ Administrator naj pritisne na gumb za izbris računa in potrdi izbris.

### 13. Dodaj preverjanje

#### Povzetek funkcionalnosti

Profesor lahko doda preverjanje znanja.

#### Osnovni tok

**PROFESOR**

1. Profesor izbere funkcionalnost _Dodaj preverjanje_.
2. Sistem prikaže stran z vnosnimi polji za dodajanje preverjanje.
3. Profesor vnese naziv preverjanje in datum za dodajanje preverjanje in s klikom na gumb potrdi vnos.
4. Sistem prikaže sporočilo o uspešnem dodajanju preverjanja.

#### Izjemni tokovi

+ Preverjanja ni mogoče dodati, saj naziv ni ustrezen.
+ Preverjanja ni mogoče dodati, saj datum ni ustrezen.

#### Pogoji

+ Profesor se mora najprej vpisati.

#### Posledice

+ Dodano preverjanje znanja v bazo.

#### Posebnosti

+ Ni posebnosti

#### Prioritete identificiranih funkcionalnosti

**SHOULD HAVE**

#### Sprejemni testi

+ Profesor naj vpiše v ustrezna polja naziv preverjanje in datum le-tega, ter potrdi vnos.
+ Profesor naj vpiše neveljaven naziv preverjanja.
+ Profesor naj vpiše neveljaven datum preverjanja.

### 14. Uredi preverjanje

#### Povzetek funkcionalnosti

Urednik lahko uredi podatke preverjanja znanja.

#### Osnovni tok

**UREDNIK**

1. Urednik izbere funkcionalnost _Uredi preverjanje_.
2. Sistem prikaže stran z izbiro preverjanje znanja.
3. Urednik izbere preverjanje znanja, ki ga želi urediti.
2. Sistem prikaže stran z vnosnimi polji za spreminjane podatkov preverjanja.
3. Urednik vnese podatke, ki jih želi spremeniti in s klikom na gumb potrdi spremembe.
4. Sistem prikaže sporočilo o uspešni spremembi preverjanja.

#### Izjemni tokovi

+ Preverjanja ni mogoče spremeniti, saj naziv ni ustrezen.
+ Preverjanja ni mogoče spremeniti, saj datum ni ustrezen.

#### Pogoji

+ Urednik se mora najprej vpisati.
+ V bazi mora obstajati vsaj eno preverjanje.

#### Posledice

+ Posodobljeno preverjanje znanja v bazi.

#### Posebnosti

+ Ni posebnosti

#### Prioritete identificiranih funkcionalnosti

**COULD HAVE**

#### Sprejemni testi

+ Urednik naj vpiše spremenjen naziv preverjanja ali spremenjen naziv datuma in potrdi spremembe.
+ Urednik naj vpiše neveljaven naziv preverjanja.
+ Urednik naj vpiše neveljaven datum preverjanja.

### 15. Dodaj predmet

#### Povzetek funkcionalnosti

Urednik ali Administrator lahko dodata nov predmet.

#### Osnovni tok

**UREDNIK**

1. Urednik izbere funkcionalnost _Dodaj predmet_.
2. Sistem prikaže stran, kjer je obrazec z vnosimi polji za dodajanje novega predmeta.
3. Urednik vnese podatke predmeta, ki ga želi dodati in s klikom na gumb potrdi spremembe.
4. Sistem prikaže sporočilo o uspešnem dodajanju predmeta.

**ADMINISTRATOR**
1. Administrator izbere funkcionalnost _Dodaj Predmet_.
2. Sistem prikaže stran, kjer je obrazec z vnosimi polji za dodajanje novega predmeta.
3. Administrator vnese podatke predmeta, ki ga želi dodati in s klikom na gumb potrdi spremembe.
4. Sistem prikaže sporočilo o uspešnem dodajanju predmeta.

#### Izjemni tokovi

+ Predmeta ni mogoče dodati, saj predmet z istim imenom že obstaja.

#### Pogoji

+ Urednik se mora najprej vpisati ali
+ Administrator se mora najprej vpisati.

#### Posledice

+ Dodan nov predmet v podatkovni bazi.

#### Posebnosti

+ Ni posebnosti

#### Prioritete identificiranih funkcionalnosti

**SHOULD HAVE**

#### Sprejemni testi

+ Urednik naj vpiše naziv predmeta in s pritiskom na gumb potrdi spremembe.
+ Urednik naj vpiše naziv predmeta, ki že obstaja.
+ Administrator naj vpiše naziv predmeta in s pritiskom na gumb potrdi spremembe.
+ Administrator naj vpiše naziv predmeta, ki že obstaja.

### 16. Uredi predmet

#### Povzetek funkcionalnosti

Urednik lahko ureja podatke predmeta in ga tudi izbriše.

#### Osnovni tok

**UREDNIK**

1. Urednik izbere funkcionalnost _Uredi predmet_.
2. Sistem prikaže seznam vseh predmetov.
3. Urednik izbere predmet, ki ga želi urediti ali izbrisati.
4. Sistem prikaže stran, kjer je obrazec z vnosimi polji za spreminjanje predmeta in gumb za izbris le-tega.
5. Urednik vnese podatke predmeta, ki ga želi dodati in s klikom na gumb potrdi spremembe ali pritisne gumb izbriši in s potrditvijo izbriše predmet.
6. Sistem prikaže sporočilo o uspešni akciji nad predmetom.

#### Izjemni tokovi

+ Predmeta ni mogoče spremeniti, saj predmet z istim imenom že obstaja.

#### Pogoji

+ Urednik se mora najprej vpisati.
+ V bazi mora biti vsaj en predmet.

#### Posledice

+ Spremenjen ali izbrisan predmet v podatkovni bazi.

#### Posebnosti

+ Ni posebnosti

#### Prioritete identificiranih funkcionalnosti

**SHOULD HAVE**

#### Sprejemni testi

+ Urednik naj vpiše spremenjen naziv predmeta in potrdi spremembe.
+ Urednik naj pritisne na gumb "Izbriši" in potrdi spremembe.
+ Urednik naj vpiše naziv predmeta, ki že obstaja.

### 17. Dodaj obveznost

#### Povzetek funkcionalnosti

Uporabnik lahko doda novo obveznost.

#### Osnovni tok

**UREDNIK**

1. Urednik izbere funkcionalnost _Dodaj obveznost_.
2. Sistem prikaže stran, kjer je obrazec z vnosimi polji za dodajanje nove obveznosti.
3. Urednik vnese podatke obveznostiin s klikom na gumb potrdi vnos.
4. Sistem prikaže sporočilo o uspešno dodani obveznosti.

#### Izjemni tokovi

+ V vnosnem polju so prazna polja.
+ Naziv obveznosti ni ustrezen.

#### Pogoji

+ Uporabnik se mora najprej vpisati.

#### Posledice

+ Dodana obveznost v podatkovni bazi.

#### Posebnosti

+ Ni posebnosti

#### Prioritete identificiranih funkcionalnosti

**SHOULD HAVE**

#### Sprejemni testi

+ Uporabnik naj vpiše vnosna polja za dodajanje obveznosti in potrdi spremembe.
+ Uporabnik naj pusti prazno polje ali več polji.
+ Uporabnik naj vpiše neveljavno ime obveznosti.

### 18. TO-DO lista

#### Povzetek funkcionalnosti

Uporabnik lahko pregleda svojo TO-DO listo, kjer so v seznamu vse njegove obveznosti, ki jih še ima za opraviti. Ima tudi možnost, da obveznost obkljuka in odkljuka.

#### Osnovni tok

**UPORABNIK**

1. Uporabnik izbere funkcionlanost _TO-DO_
2. Sistem prikaže seznam uporabnikovih obveznosti
3. Uporabnik lahko obkljuka in odkljuka obveznosti, ki jih je opravil oziroma jih še želi dokončati

#### Izjemni tokovi

+ Ni izjemnih tokov.

#### Pogoji

+ Uporabnik se mora najprej vpisati.

#### Posledice

+ Posodobitev zapisa v bazi.

#### Posebnosti

+ Ni posebnosti.

#### Prioritete identificiranih funkcionalnosti

**MUST HAVE**

#### Sprejemni testi

+ Uporabnik naj obkljuka obveznost na listi.
+ Uporabnik naj odkljuka obveznost na listi.

## 6. Nefunkcionalne zahteve

**Zahteva** (**Vrsta zahteve**)

+ Sistem StraightAs je dosegljiv vsem uporabnikom vse dni v tednu, med katere štejemo tudi vikende. (Zahteva izdelka)
+ Sistem mora biti dostopen na javno dosegljivem naslovu.(Zahteva izdelka)
+ Sistem mora na vsak klic odgovoriti v manj kot 300 milisekundah. (Zahteva izdelka)
+ Sistem mora obdelati najmanj 2000 klicev v eni minuti. (Zahteva izdelka)
+ Sistem mora biti sposoben streči najmanj 10000 uporabnikom hkrati.(Zahteva izdelka)
+ Vsi uporabniki sistema se morajo identificirati s svojim uporabniškim imenom in geslom. (Organizacijska zahteva)
+ Razvoj sistema mora potekati nemoteno do vzpostavitve delujoče produkcijske verzija. Nato mora biti vzdrževanje sprotno. (Organizacijska zahteva)
+ Plačila dodatne opreme sistema morajo biti izvedena v skladu s državnimi standardi. (Zunanja zahteva) 
+ Uvedena mora biti stroga zasebnost uporabnikovih osebnih podatkov. (Zunanja zahteva)

## 7. Prototipi vmesnikov

**Osnutki zaslonskih mask**
![Log in](../img/LogIn.png)
![Registracija](../img/Register.png)
![Koledar Teden](../img/KoledarTeden.png)
![Koledar Dan](../img/KoledarDan.png)
![Koledar Mesec](../img/KoledarMesec.png)
![Koledar Leto](../img/KoledarLeto.png)
![TO-DO](../img/ToDo.png)
![Dodaj Obveznost](../img/DodajObveznost.png)
![Dodaj Predmet](../img/DodajPredmet.png)
![Dodaj Racune](../img/DodajRacune.png)
![Dodaj Preverjanje](../img/DodajPreverjanje.png)
![Dodaj Urnik](../img/DodajUrnik.png)
![Admin Dodaj Urnik](../img/AdminDodajUrnik.png)
![Pregled Obveznosti](../img/PregledObveznosti.png)
![Prijava Na Predmet](../img/PrijavaNaPredmet.png)
![Uredi Predmet](../img/UrediPredmet.png)
![Uredi Obveznost](../img/UrediObveznost.png)
![Uredi Preverjanje](../img/UrediPreverjanje.png)
![Uredi Racune](../img/UrediRacune.png)

**Vmesniki do zunanjih sistemov**
#### Google Maps API
 
Google Maps ponuja funkcijo

 lokacija(naslov)
 
 ki sprejme naslov in vrne lokacijo na mapi, kot 
	results[]: {
	
	 types[]: string,
	 
	 formatted_address: string,
	 
	 address_components[]: {
	 
	   short_name: string,
	   
	   long_name: string,
	   
	   postcode_localities[]: string,
	   
	   types[]: string
	   
	 },
	 
	 partial_match: boolean,
	 
	 place_id: string,
	 
	 postcode_localities[]: string,
	 
	 geometry: {
	 
	   location: LatLng,
	   
	   location_type: GeocoderLocationType
	   
	   viewport: LatLngBounds,
	   
	   bounds: LatLngBounds
	   
	 }
	 
	}

#### FRI urnik API

FRI urnik ponuja funkcijo

 urnik(vpisna_stevilka)
 
 ki sprejme vpisno številko študenta in vrne njegov urnik.
 Urnik vsebuje ime predmeta, okrajšavo imena, tip, predavalnico, dan, uro začetka, uro konca, priimek in ime izvajalca,