# Načrt sistema

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Aleksandar Hristov, Robi Markač, Andrija Vučković in Jernej Vrhunc |
| **Kraj in datum** | Slovanija, 23.4.2019 |



## Povzetek

Da bi razvijalci programske opreme dobro vedeli kaj jih čaka pri izdelavi določene aplikacije je treba njeno atrhitekturo, 
strukturo in obnašanje v različnih primerih kar se da dobro opisati. Sami smo se odločili, da za arhitekturo sistema izberemo MVC(Model-View-Controller),
saj temelji na preprosti interakciji s podatki aplikacije. Naša struktura predstavlja vse razrede, ki jih bomo pri implementaciji uporabili. Le-ti so 
pripravljeni na način, da zadovoljijo vse potrebe, ki jih sistem potrebuje. 

Kot se mora človek v vsaki življenski situaciji - naj gre za preprosto
izdelavo domače naloge pri kakšnem predmetu na fakulteti ali resen zmenek s punco njegovih sanj - znati obnašati pravilno, tako mora tudi aplikacija oz.
programska oprema v vsakem trenutku svojega življenjskega cikla delovati pravilno. Obnašanje smo v projektu opisali z diagrami zaporedja, ki prikazujejo,
kako naj se aplikacija obnaša ob različnih ukazih in izvedbah različnih akcij.



## 1. Načrt arhitekture

![Načrt arhitekture](../img/nacrt_arh.png)

**Razvojni pogled:**
![Razvojni pogled](../img/razvojni_pogled.png)

**Logični pogled:**
![Logični pogled](../img/logicni.png)

## 2. Načrt strukture

### 2.1 Razredni diagram

![Načrt strukture](../img/nacrt_strukture.png)


### 2.2 Opis razredov

* Vsak razred podrobno opišite. Opis posameznega razreda naj ima sledečo strukturo:

#### Oseba

* Objekt razreda **Oseba** predstavlja osebo, ki uporablja sistem.

#### Atributi
    
* ID: int
* ime: String
* priimek: String
* email: String
* tip: String
    * pomen: Tip osebe, ki uporablja aplikacijo z katerim se preverja pravica urejanja, kreiranja, itd... . 
    * zaloga vrednosti: {UPORABNIK, PROFESOR, UREDNIK, ADMINISTRATOR}
    * default : UPORABNIK
* geslo: String

#### Nesamoumevne metode

* String predlagajUporabniškoIme()
    * vrne predlog uporabniškega imena osebe.
    * Uporabniško ime sestavi iz prve črke imena in celotnega priimka.
    
#### Uporabnik

* Objekt razreda **Uporabnik** predstavlja osebo, ki je v procesu šolanja in želi uporabljati aplikacijo.

#### Atributi

* Zaradi dedovanja vsebuje vse atribute in metode, definirane v razredu **Oseba**.
* uporabniškoIme: String
* referenca na objekt razreda **Urnik**, ki predstavlja uporabnikov urnik.
* seznam referenc na objekt razreda **TODO lista**, ki predstavljajo vse uporabnikove TODO liste.
* referenca na objekt razreda **Koledar**, ki predstavlja uporabnikov Koledar.
* seznam referenc na objekt razreda **Predmet**, ki predstavlja vse predmete na katere je uporabnik prijavljen


#### Nesamoumevne metode
* CRUD metode nad referenco na objekt razreda **Urnik**,  seznamom referenc na objekt razreda **TODO lista**, referenco na objekt razreda **Koledar**
in seznamom referenc na objekt razreda **Predmet**.
* void dodajNaKoledar(**Obveznost** obveznost?, **Preverjanje** preverjanje?, **Urnik** urnik?), doda obveznost, preverjanje ali urnik
v uporabnikov koledar, metoda opcijsko sprejme parametre in nastavi ustreznega če obstaja.
* void dodajNaTodoListo(**int** todoListaID, **Obveznost** obveznost?, **Preverjanje** preverjanje?), doda obveznost ali preverjanje 
v uporabnikovo TODO listo, metoda opcijsko sprejme parametra obveznost in preverjanje in nastavi ustreznega če obstaja.
* **TODO lista** vrniAktualnoTodoListo(), vrne objekt **TODO lista**, ki ima najbližje datum kot sysdate.
* void prijaviNaPredmet(**Predmet** predmet), prijavi uporabnika na predmet tako, da doda predmet v seznam referenc predmetov.
    
#### Profesor

* Objekt razreda Profesor predstavlja šolskega profesorja, ki dodaja preverjanje glede na predmet.

#### Atributi

* Zaradi dedovanja vsebuje vse atribute in metode, definirane v razredu **Oseba**.
* seznam referenc na objekt razreda **Predmet**, ki predstavlja vse predmete katerih nosilec je objekt razreda **Profesor**.
* seznam referenc na objekt razreda **Preverjanje**, ki predstavlja vsa preverjanja ki jih kreira objekt razreda **Profesor**.

#### Nesamoumevne metode
* CRUD metode nad seznamom referenc na objekt razreda **Predmet** in seznamom referenc na objekt razreda **Preverjanje**.
* List<**Preverjanje**> pridobiSeznamAktivnihPreverjanj() - vrne seznam objektov razreda **Preverjanje**, katerih datum je >= sysdate.
    
#### Urednik

* Predstavlja osebo z pravico urejanja preverjanja, dodajanja predmeta in urejanja predmeta.

#### Atributi

* Zaradi dedovanja vsebuje vse atribute in metode, definirane v razredu **Oseba**.


#### Nesamoumevne metode

* void posodobiPreverjanje(**int** preverjanjeID, **Preverjanje** novoPreverjanje) - posodobi preverjanje z novim.
* void dodajPredmet(**Predmet** predmet) - doda nov predmet v zbirko predmetov.
* void posodobiPredmet(**int** predmetID, **Predmet** noviPredmet)- posodbi predmet z novim.
    
#### Administrator

* Predstavlja osebo, ki je administrator aplikacije in lahko dodaja, ureja račune, dodaja urnik in predmet.

#### Atributi

* Zaradi dedovanja vsebuje vse atribute in metode, definirane v razredu **Oseba**.

#### Nesamoumevne metode

* void dodajRacun(**Oseba** oseba) - doda novo osebo.
* void posodobiRacun(**int** osebaID, **Oseba** novaOseba) - posodobi osebo z novimi podatki.
* void dodajUrnik(**Urnik** urnik) - doda nov urnik.
* void dodajPredmet(**Predmet** predmet) - doda nov predmet v zbirko predmetov.

#### TODO lista

* Predstavlja seznam še neizpolnjenih obveznosti in preverjanj uporabnika.

#### Atributi

* id: int
* ime: String
* seznam referenc na objekt razreda **Preverjanje**, ki predstavlja seznam še neizpolnjenih preverjanj uporabnika.
* seznam referenc na objekt razreda **Obveznost**, ki predstavlja seznam še neizpolnjenih preverjanj uporabnika.


#### Nesamoumevne metode

* CRUD metode nad seznamom referenc na objekt razreda **Preverjanje** in seznamom referenc na objekt razreda **Obveznost**.
* List<**Preverjanje**> sortirajPoPrioritetiInVrniPreverjanja() - sortira po prioriteti seznam referenc na objekt razreda **Preverjanje**
in vrne seznam.
* List<**Obveznost**> sortirajPoPrioritetiInVrniObveznosti() - sortira po prioriteti seznam referenc na objekt razreda **Obveznost**
in vrne seznam.
    
#### Obveznost

* Predstavlja kakršnokoli dogodek ali stvar, ki jo mora uporabnik izpolnit v bližnji prihodnosti.

#### Atributi

* id: int
* ime : String
* Prioriteta: int
    * predstavlja prioriteto obveznosti, na podlagi katere se sortira pri prikazu
    * zaloga vrednosti : {1,..,10}
    * 1 = max priority, 10 = min priority
* datum : Date
* trajanje: long
* naslov: String
* opis: String 

#### Nesamoumevne metode

* long vrniCasDoObveznosti() - vrne čas v sekundah do obveznosti glede na podan datum in sysdate
* boolean jeAktualna() - vrne true če je datum >= sysdate, drugače false, da vemo ali je obveznost že pretekla
    
#### Preverjanje

* Predstavlja kakršnokoli šolsko preverjanje znanja, ki ga opravlja uporabnik.

#### Atributi

* id: int
* referenca na objekt razreda **Predmet** na katerega je vezano preverjanje.
* tip: String
    * tip preverjanja
    * zaloga vrednosti: šifrant tipov preverjanj
* datum: Date
* predavalnica: String
    * predavalnica v kateri se opravlja preverjanje
    * zaloga vrednosti: šifrant predavalnic
* opis: String

#### Nesamoumevne metode
  
* long vrniCasDoPreverjanja() - vrne čas v sekundah do preverjanja glede na podan datum in sysdate
* boolean jeAktualno() - vrne true če je datum >= sysdate, drugače false, da vemo ali je preverjanje že preteklo
    
#### Urnik

* Predstavlja koncept šolskega urnika, kateri se uporablja za prikazovanje na koledarju uporabnika.

#### Atributi

* id: int
* seznam referenc na objekt razreda **Predmet**, ki predstavlja vse predmete na urniku.
* tip: String
    * tip urnika
    * zaloga vrednosti: šifrant tipov urnika

#### Nesamoumevne metode

* CRUD metode nad seznamom referenc na objekt razreda **Predmet**
    
#### Predmet

* Predstavlja koncept šolskega predmeta, na katerega se uporabnik prijavi in ga vodi profesor.

#### Atributi

* id: int
* ime: String
* okrajsanoIme: String
* imeNosilca: String
* referenca na objekt razreda **Profesor**, ki predstavlja nosilca predmeta.
* seznam referenc objekta razreda **Preverjanje**, ki predstavlja preverjanja tega predmeta
* dneviIzvajanja: Date
    * predstavlja dneve izvajanja predmeta
    * zaloga vrednosti: {pondeljek, ..., petek}
* seznamTrajanj: List<long>
    * predstavlja vzporedni seznam trajanja dnevom izvajanja
* predavalnica: String
    * predavalnica v kateri se opravlja preverjanje
    * zaloga vrednosti: šifrant predavalnic
* opis: String

#### Nesamoumevne metode

* List<**Preverjanje**> vrniAktualnaPreverjanja() - vrne preverjanja katerih datum je >= sysdate
* CRUD metode nad vsemi atributi

    
#### Koledar

* Predstavlja koncept šolskega koledarja, na katerem so prikazane vse obveznosti, preverjanja in urnik uporabnika.

#### Atributi

* id: int
* ime: String
* seznam referenc objekta razreda **Preverjanje**, ki predstavlja preverjanja tega predmeta
* seznam referenc objekta razreda **Obveznosti**, ki predstavlja preverjanja tega predmeta
* referenca na objekt razreda **Urnik**, ki predstavlja šolski urnik uporabnika


#### Nesamoumevne metode

* List<**Preverjanje**> vrniSeznamPreverjanjZaKoledar(**String** tipKoledarja, **Date** datumOd, **Date** datumDo)
    * parameter tipKoledarja je šifrant(dnevni,mesečni, letni, tedenski)
    * metoda vrne seznam preverjanj glede na tip koledarja in podanega datuma od in do
* List<**Obveznosti**> vrniSeznamObveznostiZaKoledar(**String** tipKoledarja, **Date** datumOd, **Date** datumDo)
    * parameter tipKoledarja je šifrant(dnevni,mesečni, letni, tedenski)
    * metoda vrne seznam Obveznosti glede na tip koledarja in podanega datuma od in do
* List<**Predmet**> vrniPredmeteIzUrnikaZaPrikazNaKoledarju(**String** tipKoledarja, **Date** datumOd, **Date** datumDo)
    * parameter tipKoledarja je šifrant(dnevni,mesečni, letni, tedenski)
    * vrne predmete iz urnika, kateri imajo dan izvajanja med obdobjem datum od do.
    

## 3. Načrt obnašanja
![Registracija](../img/Registracija.png)
![Vpis](../img/Vpis.png)
![Vpis Za Admina](../img/VpisZaAdmin.png)
![Koledar Dnevni](../img/Koledar-dan.png)
![Koledar Tedenski ena](../img/Koledar-tedenPrvi.png)
![Koledar Tedenski dva](../img/Koledar-tedenDrugi.png)
![Koledar Mesec ena](../img/Koledar-mesecPrvi.png)
![Koledar Mesec dva](../img/Koledar-mesecDrugi.png)
![Koledar Leto](../img/Koledar_Leto.png)
![Prijava na predmet](../img/PPrijavaNaPredmet.png)
![Uredi Obveznost](../img/UUrediObveznost.png)
![Preglej Obveznost](../img/PPregledObveznosti.png)


![Dodaj urnik Uporabnik](../img/DodajUrnikUporabnik.png)
![Dodaj urnik Administrator](../img/DodajUrnikAdmin.png)
![Dodaj račune](../img/DodajRačun.png)
![Uredi račun](../img/UrediRačun.png)
![Dodaj preverjanje](../img/DodajPreverjanjee.png)
![Uredi preverjanje](../img/UrediPreverjanjee.png)
![Dodaj predmet Urednik](../img/DodajPredmetUrednik.png)
![Dodaj predmet Administrator](../img/DodajPredmetAdmin.png)
![Uredi predmet](../img/UrediPredmett.png)
![Dodaj obveznost](../img/DodajObveznostt.png)
![TO-DO lista](../img/TO-DOLista.png)
