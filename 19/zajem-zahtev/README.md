# Dokument zahtev

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Jernej Križan, Ina Lang, Luka Mravinec, Zorica Ilievska |
| **Kraj in datum** | 7. 4. 2019 |


## Povzetek projekta

Vsi študentje se borimo z roki za oddajo. Sprašujemo se znova in znova, ali sem oddal domačo nalogo?
Kdaj je že razpisan rok za Umetno inteligenco? Ali sem se prijavil za izpit?  
Kot že vemo, biti nenehno v naglici in pod pritiskom vodi k oteženi koncentraciji, tesnobi in napetosti.
Tukaj nastopi naša rešitev, pomagala bo študentom lažje organizirati svoje šolske obveznosti in hkrati
prihranila čas za dodatne obštudijske dejavnosti.   
Odgovor leži v spletnem vmesniku. Ta bo omogočal jasen pregled nad vsemi obveznostmi. V njemu bodo zabeležena vsa predavanja, vaje, domače naloge in izpiti, torej vse pomembne 
zadeve zbrane na enem mestu. Uporabnik si bo lahko določil tudi prioritete aktivnosti, ter tako 
dobil boljši načrt dela za naslednje dni.


## 1. Uvod

Sistem StraighAs bo rešil problem organizacije obveznosti in upravljanje s časom. Namenjen bo študentom, ki se dnevno spopadajo s časovno stisko zaradi velikega števila obveznosti.
Sistem jim bo tako omogčil učinkovitejše razporejanje opravil. Uporabnik bo lahko ustvarjal obveznosti, jim določal roke zapadlosti in prioriteto. Imel bo dostop do svojega seznama obveznosti,
ki jih bo lahko razporedil po datumu, prioriteti, in/ali tipu. Po opravljeni obveznosti bo uporabnik obveznost odkljukal, in na njeno mesto bodo prišle še naslednje, še neopravljene obveznosti.
Tako bo uporabnik vedno vedel, katera aktivnost je najbolj pomembna, bodisi po časovno, bodisi prioritetno. Sistem bo na voljo registriranim in neregistriranim uporabnikom, pedagogom in skrbniku sistema.
Pedagog bo lahko ustvaril zbirko obveznosti, npr. urnik za določeni letnik, kjer bodo notri vsi predmeti, izpitni roki in domače naloge. Uporabniki bodo lahko zbirko enostavno prenesli v svojo bazo.
Skrbnik sistema pa bo imel pregled nad vsemi uporabniki, in morebitne zlonamerne uporabnike (spambot-e) tudi odstranil.


## 2. Uporabniške vloge

* **Neregistriran uporabnik**: Uporabnik, ki ni shranjen v podatkovni bazi, ampak aplikacijo lahko uporablja zgolj na odjemalcu 
(npr. mobilni napravi), kjer se shranjujejo vse njegove informacije
* **Registriran uporabnik**: Uporabnik, čigar informacije so shranjene v bazi. Do svojih podatkov lahko dostopa kjerkoli s prijavo
z uporabniškim imenom in geslom
* **Pedagog**: Lahko ustvarja obveznosti in jih poveče v zbirko obveznosti -  (npr. urnik ali izpiti), ki ga lahko uporabniki enostavno prenesejo (uvozijo)
v svojo bazo
* **Skrbnik**: vzdržuje vse uporabnike in pedagoge, vključene v sistem


## 3. Slovar pojmov

* **Neprijavljen uporabnik**: Oseba, ki trenutno ni prijavljena v aplikacijo.
* **Uporabnik**: neregistriran uporabnik ali pa registriran uporabnik.
* **Obveznost**: aktivnost kot podatkovna struktura, ki ima ime, datum začetka, datum konca, opis, prioriteto, tip in stanje. Vsebuje lahko tudi interval ponavljanja.
* **Datum**: datum, zapisan v formatu *YYYY-MM-DDThh:mm* (npr. *1997-07-16T19:20*).
* **Prioriteta**: celo število z intervala [1,10], ki predstavlja pomembnost obveznosti. Višje število pomeni višjo prioriteto/večjo pomembnost.
* **Opis**: Niz znakov dolžine največ 500, ki smiselno opisuje obveznost.
* **Tip**: je lahko eno izmed {"izpit", "kolokvij", "predavanje", "vaje", "seminar", "domača naloga", "drugo"}.
* **Stanje**: spremenljivka tipa boolean z naborom vrednosti {opavljeno, neopravljeno}, ki predstavlja ali je obveznosti že odpravljena, ali ne.
* **Zbirka obveznosti**: seznam obveznosti, povezan v celoto. Uporabniki ga lahko uvozijo in vključijo med svoje obveznosti.
* **Interval ponavljanja**: kombinacija vrednosti iz množice {dan, teden, mesec, semester, leto} in datuma.
    Na primer, kombinacija {leto, 1997-07-16T19:20}, pomeni, da se obveznost ponovi vsako leto ob 16. julija ob 19.20, od leta 1997 naprej.

## 4. Diagram primerov uporabe

![Diagram primerov uporabe](../img/UML.jpg)

## 5. Funkcionalne zahteve

### Seznam trenutnih obveznosti
#### Povzetek funkcionalnosti
Uporabnik lahko izbere **TO-DO list**, kjer se mu prikažejo vse obveznosti do izbranega datuma, npr. en teden, en mesec, cel semester. 
Razvrščene so lahko glede na datum in/ali prioriteto.
#### Osnovni tok
1. Uporabnik v glavnem meniju klikne na gumb "Seznam trenutnih obveznosti".
2. Sistem uporabnika preusmeri v okno, kjer so nanizane vse njegove obveznosti, razvrščene po poljubnem kriteriju.
#### Alternativni tok(ovi)
**Alternativni tok 1:**
1. Uporabnik uporablja funkcionalnost "Izbira/pregled posamezne obveznosti".
2. Uporabnik klikne na gumb "nazaj", ki ga vrne na "Seznam trenutnih obveznosti".

**Alternativni tok 2:**
1. Uporabnik uporablja funkcionalnost "Ustvarjanje nove obveznosti".
2. Uporabnik klikne na gumb "nazaj", ki ga vrne na "Seznam trenutnih obveznosti".

#### Pogoji
Oseba mora sistem uporabljati kot neregistrirani uporabnik ali registrirani uporabnik.
#### Posledice
Na zaslonu se prikaže okno z seznamom trenutnih obveznosti.
#### Posebnosti
#### Prioritete identificiranih funkcionalnosti
*MUST have*
#### Sprejemni testi
* Uporabnik v glavnem meniju izbere "TO-DO list", ki se mu pravilno prikaže.
* Uporabnik v "Izbira/pregled posamezne obveznosti" izbere gumb "nazaj", ki ga privzeto vrne na seznam obveznosti.
* Uporabnik v "Ustvarjanje nove obveznosti" izbere gumb "nazaj", ki ga privzeto vrne na seznam obveznosti.


### Izbira/pregled posamezne obveznosti
#### Povzetek funkcionalnosti
Uporabnik iz seznama obveznosti izbere neko obveznost. Prikažejo se mu vsi podatki o tej obveznosti (opis, tip ...).
#### Osnovni tok
1. Uporabniku je prikazan seznam trenutnih obveznosti, klikne na eno izmed njih.
2. Odpre se okno, na katerem so vsi podatki o obveznosti.
#### Alternativni tok(ovi)
#### Pogoji
* Oseba mora sistem uporabljati kot neregistrirani uporabnik ali registrirani uporabnik.
* Uporabnik se mora nahajati v Seznamu trenutnih obveznosti.
#### Posledice
Na zaslonu se prikaže okno z vsemi podatki o obveznosti: ime, datum začetka, datum konca, opis, prioriteto, tip in stanje ter morebiten interval ponavljanja.
#### Posebnosti
#### Prioritete identificiranih funkcionalnosti
*MUST have*
#### Sprejemni testi
* Uporabniku, ki se nahaja v oknu s seznamom trenutnih obveznosti, se ob kliku na eno izmed njih prikažejo podrobnosti le-te.

### Urejanje izbrane obveznosti
#### Povzetek funkcionalnosti
Uporabnik ali pedagog, ki je izbral neko obveznost, lahko klikne na gumb "uredi", in spreminja vse njene podatke. Lahko jo tudi v celoti izbriše.
#### Osnovni tok
1. Uporabnik/pedagog klikne na gumb "uredi".
2. Podatki predhodno izbrane obveznosti so mu na voljo za urejanje, spremeni jih po želji.
3. Po končanem urejanju, klikne na gumb "shrani", in sistem ga vrne v okno "Izbira/pregled posamezne obveznosti".
#### Alternativni tok(ovi)
#### Izjemni tok(ovi)
* Vnešeni končni/začetni datum je manjiši od današnjega. Sistem vrne opozorilo in uporabnika vrne v urejanje.
#### Pogoji
* Oseba mora sistem uporabljati kot neregistrirani uporabnik ali registrirani uporabnik ali pedagog.
* Uporabnik/pedagog se mora nahajati v "Izbira/pregled posamezne obveznosti"
#### Posledice
Izbrana obveznost se posodobi z novimi podatki.
#### Posebnosti
#### Prioritete identificiranih funkcionalnosti
*SHOULD have*
#### Sprejemni testi
* Prijavi se kot uporabnik, izberi obveznost in klikni na gumb uredi. Vnesi želene podatke in shrani. Obveznost se uspešno posodobi.
* Prijavi se kot pedagog, izberi obveznost in klikni na gumb uredi. Vnesi želene podatke in shrani. Obveznost se uspešno posodobi.
* Ob vnosu podatkov izberi datum, manjši od današnjega. Aplikacija mora vrniti ustrezno opozorilo in te vrniti v urejanje.

### Ustvarjanje nove obveznosti
#### Povzetek funkcionalnosti
Uporabnik ali pedagog lahko novo obveznost z željenimi podatki.
#### Osnovni tok
1. Uporabnik ali pedagog v glavnem meniju klikne na gumb "ustvari obveznost". Pojavi se okno z vnosnimi polji.
2. Vnese ime obveznosti.
3. Vnese opis obveznosti.
4. Vnese datum začetka in datum konca obveznosti.
5. Izbere prioriteto.
6. Stanje se privzeto nastavi na neopravljeno.
7. Lahko se izbere tudi interval ponavljanja.
8. S klikom na gumb "shrani" se obveznost shrani.
9. Sistem uporabnika vrne na seznam obveznosti, na katerem tudi na novo ustvarjena obveznost.

#### Alternativni tok(ovi)
1. Uporabnik ali pedagog lahko obveznost ustvari tudi, ko se nahaja v oknu "Seznam vseh obveznosti", kjer se z gumbom "+" pojavi okno z vnosnimi polji.
2. Ostali koraki so enaki kot pri osnovnem toku.
3. S klikom na gumb "shrani" se obveznost shrani in pojavi se na seznamu obveznosti.
#### Izjemni tok(ovi)
* Vnešeni končni/začetni datum je manjiši od današnjega. Sistem vrne opozorilo in uporabnika vrne v urejanje.
#### Pogoji
Oseba mora sistem uporabljati kot neregistrirani uporabnik ali registrirani uporabnik ali pedagog.
#### Posledice
Na zaslonu se prikaže okno z seznamom trenutnih obveznosti, skupaj z novo obveznostjo.
#### Posebnosti
#### Prioritete identificiranih funkcionalnosti
*MUST have*
#### Sprejemni testi
* Prijavi se kot uporabnik, izberi "ustvari obveznost", vnesi želene podatke in shrani. Obveznost se pojavi na seznamu.
* Prijavi se kot pedaog, izberi "ustvari obveznost", vnesi želene podatke in shrani. Obveznost se pojavi na seznamu.
* Ob vnosu podatkov izberi datum, manjši od današnjega. Aplikacija mora vrniti ustrezno opozorilo in te vrniti v urejanje.

### Uvoz zbirke obveznosti
#### Povzetek funkcionalnosti
Uporabnik lahko iz obstoječe podatkovne baze izbere poljubno zbirko obveznosti. Te obveznosti se mu prenesejo v njegovo zasebno bazo.
#### Osnovni tok
1. Uporabnik v glavnem meniju izbere "Uvoz zbirke obveznosti"
2. Odpre se okno z vsemi obstoječimi zbirkami obveznosti, ki jih ima sistem v bazi.
3. Uporabnik izbere željeno zbirko.
4. Uporabnikov seznam obveznosti se posodobi, vsebuje tudi nove obveznosti iz izbrane zbirke.
#### Alternativni tok(ovi)
#### Pogoji
Oseba mora sistem uporabljati kot neregistrirani uporabnik ali registrirani uporabnik.
#### Posledice
Uporabnikov seznam obveznosti se posodobi, vsebuje tudi nove obveznosti iz izbrane zbirke.
#### Posebnosti
#### Prioritete identificiranih funkcionalnosti
*COULD have*
#### Sprejemni testi
* Prijavi se kot uporabnik, v glavnem meniju izberi "Uvoz zbitke obveznosti", iz seznama izberi željeno zbirko.
    Uporabnikov seznam obveznosti se posodobi z novimi.

### Integracija z google
#### Povzetek funkcionalnosti
Registriran uporabnik lahko v aplikacijo uvozi izbrane dogodke iz svojega Google koledarja.
#### Osnovni tok
1. Registriran uporabnik v glavnem meniju izbere "Uvozi podatke iz Google koledarja"
2. Sistem pokaže okno kjer registriran uporabnik vpiše svoje Google račun podatke.
3. Sistem se poveže z Google koledar aplikacijo in ponudi možnosti za uvoz dogodkov.
4. Registriran uporabnik izbere gumb "Izberi vse" ali označi željene dogodke.
5. Registriran uporabnik pritisne gumb "Uvozi"
6. Sistem shrani izbrane dogodke v aplikacijo

#### Izjemni tokovi
* Registriran uporabnik uporabi napačne podatke za vpis v Google. Sistem izpiše napako.
#### Pogoji
Registriran uporabnik mora imeti Google račun in enega ali več dogodkov v njemu.
#### Posledice
V aplikaciji so sedaj vidni uvoženi dogodki iz Google koledarja.
#### Posebnosti
Če Google koledar ni dosegljiv, se ta funkcionalnost ne more izvesti.
#### Prioritete identificiranih funkcionalnosti
WOULD have
#### Sprejemni testi
* Registriran uporabnik se v sistem prijavi in uvozi nekaj dogodkov iz Google koledarja. Po tem so vidni v aplikaciji.

### Registracija
#### Povzetek funkcionalnosti
Neregistriran uporabnik se lahko z vnosom osebnih podatkov registrira v sistem in tako postane registriran uporabnik.
#### Osnovni tok
1. Neregistriran uporabnik izbere v glavnem meniju gumb "Registracija".
2. Sistem prikaže obrazec za registracijo novega uporabnika
3. Neregistriran uporabnik izpolne obrazec z ustreznimi podatki
4. Neregistriran uporabnik pritisne na gumb "Registriraj"
5. Sistem izpiše obvestilo o uspešni registraciji

#### Izjemni tokovi
* Neregistriran uporabnik uporabi napačen format podatkov. Izpiše se obvestilo o napaki.
* Uporabnik z istimi podatki je že registriran. Izpiše se obvestilo.
#### Pogoji
Oseba mora uporabljati sistem kot neregistriran uporabnik in se nahajati v glavnem meniju. 
#### Posledice
Na zaslonu se prikaže obvestilo o uspešni registraciji in uporabnik od tedaj naprej uporablja sistem kot registriran uporabnik.
#### Posebnosti
#### Prioritete identificiranih funkcionalnosti
MUST have
#### Sprejemni testi
* V novem oknu brskalnika, brez beleženja zgodovine, se odpre aplikacija in se naredi registracija novega uporabnika. 

### Prijava
#### Povzetek funkcionalnosti
Registriran uporabnik, pedagog ali skrbnik se lahko v sistem prijavi s svojim e-mail naslovom in geslom.
#### Osnovni tok
1. Registriran uporabnik, pedagog ali skrbnik izbere v glavnem meniju gumb "Prijava".
2. Sistem prikaže obrazec za prijavo uporabnika.
3. Registriran uporabnik, pedagog ali skrbnik izpolne obrazec s svojimi podatki.
4. Registriran uporabnik, pedagog ali skrbnik klikne na gumb "Prijavi me".
5. Sistem uporabnika prijavi in mu omogoči ustrezne funkcionalnosti sistema.

#### Izjemni tokovi
* Registriran uporabnik, pedagog ali skrbnik uporabi napačno uporabniško ime ali geslo. Sistem izpiše napako in ponudi možnost pozabljenega gesla.
#### Pogoji
Registriran uporabnik, pedagog ali skrbnik mora imeti pravilno uporabniško ime in geslo, ki je v sistemu ter se nahajati v glavnem meniju.
#### Posledice
Registriran uporabnik, pedagog ali skrbnik sedaj lahko aplikacijo uporablja glede na njegov status.
#### Posebnosti

#### Prioritete identificiranih funkcionalnosti
MUST have
#### Sprejemni testi
* Registriran uporabnik, pedagog ali skrbnik, se s pravilnimi podatki prijavi v aplikacijo.

### Odjava
#### Povzetek funkcionalnosti
Registriran uporabnik, pedagog, ali skrbnik se z klikom na gumb "odjava" odjavi iz sistema.
#### Osnovni tok
1. Registriran uporabnik, pedagog ali skrbnik izbere v glavnem meniju gumb "Odjava".
2. Aplikacija uporabnika obvesti o odjavi.

#### Pogoji
Registriran uporabnik, pedagog, ali skrbnik mora biti predhodno prijavljen v sistem.
#### Posledice
Aplikacijo sedaj uporabljamo kot neprijavljen uporabnik.
#### Posebnosti

#### Prioritete identificiranih funkcionalnosti
MUST have
#### Sprejemni testi
* Registriran uporabnik, pedagog ali skrbnik, ki je prijavljen v sistem klikne gumb "Odjava".

### Ustvarjanje zbirke obveznosti
#### Povzetek funkcionalnosti
Pedagog lahko svoje obstoječe obveznosti združi v zbirko obveznosti, ki postanje na voljo za uvoz vsem registriranim uporabnikom.
#### Osnovni tok
1. Pedagog v glavnem meniju pritisne na gumb "Ustvari zbirko obveznosti".
2. Sistem ponudi pedagogu obrazec za ustvarjanje zbirke.
3. V obrazcu pedagog označi obveznost, ki bodo v zbirki.
4. Pedagog klikne gumb "Shrani zbirko".

#### Alternativni tok(ovi) 
1. Pedagog se nahaja v oknu "Seznam vseh obveznosti".
2. S klikom na kvadratek ob imenu predmeta pedagog označi predmete, ki jih hoče v novi zbirki.
3. Pedagog klikne na gumb "Ustvari novo zbirko".

#### Izjemni tokovi
* Zbirka z istimi obveznostmi že obstaja. Sistem izpiše napako.
* Zbirka z istim imenom že obstaja. Sistem izpiše napako.
#### Pogoji
Oseba mora sistem uporabljati kot pedagog. Obstajati morajo obveznosti.
#### Posledice
Obstaja nova zbirka obveznosti.
#### Posebnosti
#### Prioritete identificiranih funkcionalnosti
COULD have
#### Sprejemni testi
* Pedagog, prijavljen v sistem, v obrazcu za ustvarjanje zbirke iznači 1 ali več obveznosti in ustvari zbirko.
* Pedagog, prijavljen v sistem, v pogledu vseh obveznosti izbere 1 ali več obveznosti in ustvari zbirko.

### Pregled seznama vseh uporabnikov aplikacije
#### Povzetek funkcionalnosti
Skrbnik lahko z klikom na gumb pridobi seznam vseh obstoječih uporabnikov in njihove podatke o uporabi sistema.
#### Osnovni tok
1. Skrbnik v glavnem meniju klikne gumb "Pregled uporabnikov".
2. Sistem prikaže seznam vseh uporabnikov.
3. Skrbnik, s klikom na ime uporabnika lahko izve več podatkov o njemu.

#### Pogoji
Oseba mora biti prijavljeva kot skrbnik in biti v glavnem meniju.
#### Posledice
Pogledam na seznam vseh uporanikov.
#### Posebnosti
#### Prioritete identificiranih funkcionalnosti
MUST have
#### Sprejemni testi
* Skrbnik gre na pregled seznama uporabnikov in preveri, če so tam vidni vsi uporabniki. Pri tem poskrbimo, da je v sistemu registrirano znano število uporabnikov in njihovi podatki. 

### Odstranitev uporabnika
#### Povzetek funkcionalnosti
Skrbnik lahko iz seznama vseh uporabnikov aplikacije izbere zlonamernega uporabnika (npr. spambot) in ga udstrani iz sistema.
#### Osnovni tok
1. Skrbnik v glavnem meniju klikne gumb "Pregled uporabnikov".
2. Sistem prikaže seznam vseh uporabnikov.
3. Skrbnik, s klikom na kvadratek ob imenu, označi. enega ali več uporabnikov, ki jih želi odstraniti.
4. Skrbnik pritisne na gumb "Odstrani".
5. Sistem prikaže okno za potrditev.
6. Skrbnik pritisne gumb "Potrdi".

#### Pogoji
Oseba mora biti prijavljeva kot skrbnik in biti v glavnem meniju.
#### Posledice
Izrbisan uporabnik se več ne more prijavtit ali registrirati v sistem.
#### Posebnosti
Skrbnik ne more odstraniti samega sebe ali drugih skrbnikov.
#### Prioritete identificiranih funkcionalnosti
SHOULD have
#### Sprejemni testi
* Skrbnik obstoječega uporabnika izbriše. Ta uporabnik se skuša po tem prijaviti ali registrirati vendar se ne more, sistem izpiše obvestilo. 





## 6. Nefunkcionalne zahteve

Zahteve izdelka:
* Sistem uporabniku omogoča dostop do podatkov, za katere je pooblaščen; oziroma uporabnik ima dostop samo do svojega to-do seznama.
* Ker bo sistem namenjen delovanju v oblačni storitvi, mora celotna aplikacija zasedati manj kot 100mb.
* Sistem mora biti zmožen služiti najmanj 1400 hkratnim uporabnikom (tevilo studentov prve stopnje na FRI).
* Sistem je 99.5% časa. 
* Sistem mora biti dosegljiv na javno dostopnem spletnem naslovu. 
* Podatkovna baza mora biti sposobna opraviti 200 poizvedb v sekundi.
* Vse CRUD operacije na uporabnikovi strani ne smejo trajati več kot 1 sekundo.

Zunajni zahtevi
* Sistem mora delovati v skladu z Splošno uredbo EU o varstvu podatkov.
* Del zaledja sistema mora teči v Python 2.4 ali več, za enostavno komunikacijo z Google API-ji.
 

Organizacijski zahtevi
* Uporabniki sistema morajo imeti na voljo registracijo in prijavo s svojim uporabniškim imenom in geslom.
* Uporabnik se lahko prijavi tudi z google računom.


## 7. Prototipi vmesnikov

* Zaslonske maske - sledijo si v vrstnem redu v Diagramu primerov uporabe (od zgoraj proti dol) - glej točko 4.

     ![zaslonske_maske1](../img/zaslonske_maske1.png)
     ![zaslonske_maske2](../img/zaslonske_maske2.png)
     
* Komunikacija z Google koledarjem bo potekala preko njihovega vmesnika. Operacije, ki jih bo sistem uporabljal za sinhronizacijo
našega koledarja z Googlovim:

| | | |
|:---|:---|:---|
| **HTTP method** |**URI**| **OPIS** |
|DELETE  |/calendars/calendarId/events/eventId |izbriše dogodek|
|GET     |/calendars/calendarId/events/eventId |vrne dogodek|
|GET    | /calendars/calendarId/events | vrne vse dogodke na koledarju
|POST    |/calendars/calendarId/events | ustvari dogodek|
|PUT    | /calendars/calendarId/events/eventId | posodobi dogodek

Telo HTTP sporočila:
```json
{
  "kind": "calendar#event",
  "etag": etag,
  "id": string,
  "status": string,
  "htmlLink": string,
  "created": datetime,
  "updated": datetime,
  "summary": string,
  "description": string,
  "location": string,
  "colorId": string,
  "creator": {
    "id": string,
    "email": string,
    "displayName": string,
    "self": boolean
  },
  "organizer": {
    "id": string,
    "email": string,
    "displayName": string,
    "self": boolean
  },
  "start": {
    "date": date,
    "dateTime": datetime,
    "timeZone": string
  },
  "end": {
    "date": date,
    "dateTime": datetime,
    "timeZone": string
  },
  "endTimeUnspecified": boolean,
  "recurrence": [
    string
  ],
  "recurringEventId": string,
  "originalStartTime": {
    "date": date,
    "dateTime": datetime,
    "timeZone": string
  },
  "transparency": string,
  "visibility": string,
  "iCalUID": string,
  "sequence": integer,
  "attendees": [
    {
      "id": string,
      "email": string,
      "displayName": string,
      "organizer": boolean,
      "self": boolean,
      "resource": boolean,
      "optional": boolean,
      "responseStatus": string,
      "comment": string,
      "additionalGuests": integer
    }
  ],
  "attendeesOmitted": boolean,
  "extendedProperties": {
    "private": {
      (key): string
    },
    "shared": {
      (key): string
    }
  },
  "hangoutLink": string,
  "conferenceData": {
    "createRequest": {
      "requestId": string,
      "conferenceSolutionKey": {
        "type": string
      },
      "status": {
        "statusCode": string
      }
    },
    "entryPoints": [
      {
        "entryPointType": string,
        "uri": string,
        "label": string,
        "pin": string,
        "accessCode": string,
        "meetingCode": string,
        "passcode": string,
        "password": string
      }
    ],
    "conferenceSolution": {
      "key": {
        "type": string
      },
      "name": string,
      "iconUri": string
    },
    "conferenceId": string,
    "signature": string,
    "notes": string,
    "gadget": {
    "type": string,
    "title": string,
    "link": string,
    "iconLink": string,
    "width": integer,
    "height": integer,
    "display": string,
    "preferences": {
      (key): string
    }
  },
  "anyoneCanAddSelf": boolean,
  "guestsCanInviteOthers": boolean,
  "guestsCanModify": boolean,
  "guestsCanSeeOtherGuests": boolean,
  "privateCopy": boolean,
  "locked": boolean,
  "reminders": {
    "useDefault": boolean,
    "overrides": [
      {
        "method": string,
        "minutes": integer
      }
    ]
  },
  "source": {
    "url": string,
    "title": string
  },
  "attachments": [
    {
      "fileUrl": string,
      "title": string,
      "mimeType": string,
      "iconLink": string,
      "fileId": string
    }
  ]
}

```