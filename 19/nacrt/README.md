# Načrt sistema

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Jernej Križan, Ina Lang, Luka Mravinec, Zorica Ilievska |
| **Kraj in datum** | Ljubljana, 22.4.2019 |



## Povzetek

Za načrt našega informacijskega sistema smo se odločili za standardne in presikušene načrtovalske pristope.
Pri zasnovi arhitekture smo uporabili model-pogled-krmilnik (MVC) vzorec, po katerem je zasnovan naš sistem.
V strukturnem modelu smo z razrednim diagramom opisali statično strukturo sistema v smislu razredov in razmerij med njimi.
Za vsak razred smo določili atribute in metode, ki mu pripadajo.
Načrt obnašanja smo zasnovali s pomočjo UML diagramom primerov uporabe. 
Za vsak primer uporabe smo ustvarili diagram zaporedja za osnovni, alternativni in izjemni tok ter ga po potrebi opremili z spremnim besedilom.

## 1. Načrt arhitekture

<<<<<<< HEAD
**TO-DO**

Pogled:

Prikazuje statično spletno stran (HTML) in komunicira z krmilnikom(vmesnik), ki deluje v zaledju sistema.
Sprejema vnose uporabnika, jih predela v dogodke in jih pošlje krmilniku.
Od krmilnika pričakuje odgovore, ki jih sprocesira in prikaže v brskalniku.

Krmilnik:

Dogodke, ki jih prejme od pogleda, sprocesira.
Po potrebi komunicira z modelom, ki predstavlja podatkovno bazo s pripadajočo poslovno logiko.
Podatke, ki jih prejme od modela, pretvori v odjemalcu prijazen format (JSON).

Model:

Sprejema zahteve za podatke.
Neposredno komunicira s podatkovno bazo.
Podatke streže tistim, ki jih zahtevajo.
=======
>>>>>>> 901adabba8cf7f64758d42539de0f764dd9710d7

## 2. Načrt strukture

### 2.1 Razredni diagram

![pic](../img/razredniDiagram.png)

### 2.2 Opis razredov


#### **Neregistriran uporabnik**
Objekt razreda Negistriran uporabnik predstavlja osebo (študenta) ki uporablja informacijski sistem, 
z omejenim dostopom.

#### Atributi
	id: int
	
#### Metode

	registriraj();
	
	prijava();
	
	pogledajVseObveznosti(interval, od_do,prioriteta)
	Metoda ki omogoca uporabniku pogledati obveznosti v poljubnem casovnem intervalu (teden, mesec ali celi semester),
	lahko tudi ponastavi nacin prikaza po prioriteti ali ne.
	parametri: 
		obdobje: {teden|mesec|semester}
		interval:  string (če smo za atribut obdobje ponastavili mesec pustimo kot string ki je lahko {jan|...|dec};
			če je teden obravnavamo kot int {1|2|...|12};
			če je semester je vrednost atributa interval: null)
		prioriteta: boolean (TRUE če hočemo po prioriteno, sicer po datum obveznosti) 
		

#### **Registriran uporabnik**
Objekt razreda Registriran uporabnik predstavlja osebo (študenta) - primarnega uporabnika sistema.
Je podedovan iz razreda Negistriran uporabnik in zato vsebuje vse njegove atribute in metode.
Poleg teh, razred vsebuje sledeče atribute:

#### Atributi

	uporabniškoIme: string
	ime: String
	priimek: String
	starost: DATE
	email: String
	datumRegistracije: DATE (kdaj je bil registriran)
	
	referenco na razred Obveznosti
	referenco na svojega Google racuna.

#### Metode 

	dodadiDogodkiIzGoogleKoledar();
	Registriran uporabnik lahko v aplikacijo uvozi izbrane dogodke iz svojega Google koledarja.

	odjava();
	
#### **Pedagog**
Objekt razreda Pedagog uporabnik predstavlja osebo - zaposlenega v izobraževalnem sistemu, ki ima dostop do aplikacije.

#### Atributi

	uporabniškoIme: string
    ime: String
    priimek: String
    starost: DATE
    email: String
    datumRegistracije: DATE (kdaj je bil registriran)
    
    predmet: String list (pri katerih predmetih je izvajalec)
    referenco na razred Obveznosti in zbirkoObveznosti;
	
#### Metode

	ustvariZbirkoObveznosti();
	Pedagog lahko svoje obstoječe obveznosti združi v zbirko obveznosti, ki postanje na voljo 
	za uvoz vsem registriranim uporabnikom.

#### **Skrbnik**
Objekt razreda Skrbnik uporabnik predstavlja osebo, ki je odgovorna za aplikacijo in ima največji nabor pravic.
Poleg teh, razred vsebuje sledeče atribute:
#### Atributi
    uporabniškoIme: string
    email: String
#### Metode

	pogledajUporabnike();
	Skrbnik lahko z klikom na gumb pridobi seznam vseh obstoječih uporabnikov in njihove podatke 
	o uporabi sistema.
	
	odstraniUporabnika();
	Skrbnik lahko iz seznama uporabnikov odstrani poljubnega uporabnika.

#### **Obveznost**
Objekt razreda Obveznost predstavlja obveznost.
#### Atributi
		id: int
		ime: String
		opis: String
		datum: DATE
		stanje: prevzeto{neopravljeno}
		prioriteta: int
		tip: {DN|IZPIT|PROJEKT|KVIZ}
		predmet: String
#### Metode
    
    pogledajObveznost(id_Obveznost)
    	nam prikaze vse potrebne informacije za izbrano obveznost
    	parameteri: 
    		id_Obveznosti: int
    
    ustvariObveznost(ime, predmet, datum, prioriteta, stanje)
        naredi novo obveznost
        parameteri:
            ime: {DN|IZPIT|PROJEKT|KVIZ}
            predmet: String (obveznost pri predmetu)
            datum: DATE
            prioriteta: int
            stanje: prevzeta vrednost{neopravljeno}

#### **Zbirka Obveznosti**
Objekt razreda zbirkaObveznosti predstavlja zbrika vec obveznostih.
#### Atributi
		id: int
		obveznosti: List of int (id vseh obveznosti ki sodijo v to zbirko)
		date: DATE (datum poteka zbirke)
#### Metode
    uvoziZbirkeObveznosti();
    	Uporabnik lahko iz obstoječe podatkovne baze izbere poljubno zbirko obveznosti. 
    	Te obveznosti se mu prenesejo v njegovo zasebno bazo.
    urediObveznost();
    	ki omogoci spremembo podatke za dano obveznost
## 3. Načrt obnašanja

V tem poglavju bomo, kjer je možno, uporabljali akterja Uporabnik, ki predstavlja  registriranega ali neregistriranega uporabnika.
Torej, kjer je v toku podatkov uporabljen akter Uporabnik, je enak tok za registriranega in neregistriranega uporabnika.

    
#### 3.1 Seznam trenutnih obveznosti
**Osnovni tok:**
![pic](../img/zaporedje11.png)
**Alternativna tokova:**
![pic](../img/zaporedje12.png)

#### 3.2 Izbira/pregled posamezne obveznosti
**Osnovni tok:**
![pic](../img/zaporedje21.png)

#### 3.3 Urejanje izbrane obveznosti
**Osnovni tok:**
![pic](../img/zaporedje31.png)
**Izjemi tok**
![pic](../img/zaporedje33.png)
(uporabniku se vrne ustrezno opozorilo)

#### 3.4 Ustvarjanje nove obveznosti
**Osnovni tok:**
![pic](../img/zaporedje41.png)
**Alternativni tok:**
![pic](../img/zaporedje42.png)
**Izjemi tok**
![pic](../img/zaporedje43.png)
(uporabniku se vrne ustrezno opozorilo)

#### 3.5 Uvoz zbirke obveznosti
Opomba: Sistemska tabela Obveznosti je v sistemski podatkovni bazi - enaka za vse uporabnike. V njej so podatki, dostopni vsem uporabnikom (npr. obveznosti, ustvarjene s strani pedagogv in vključene v zbirke obveznosti, ki jih lahko vsi uporabniki prenesejo v svojo podatkovno bazo).
Uporabnikova tabela Obveznosti pa je shranjena v uporabnikovi podatkovni bazi, kjer so podatki na voljo le njemu.
**Osnovni tok:**
![pic](../img/zaporedje51.png)

#### 3.6 Integracija z Google
**Osnovni tok:**
![pic](../img/zaporedje61.png)

#### 3.7 Registracija
**Osnovni tok:**
![pic](../img/zaporedje71.PNG)
**Izjemni tok:**
Potek za oba izjemna tokova je enak, le izpisana napaka se spremeni.
![pic](../img/zaporedje72.PNG)

#### 3.8 Prijava
**Osnovni tok:**
Uporabnik je v tem primeru registriran uporabnik, pedagog ali skrbnik.
![pic](../img/zaporedje81.PNG)
**Izjemni tok:**
![pic](../img/zaporedje82.PNG)

#### 3.9 Odjava
**Osnovni tok:**
Uporabnik je v tem primeru registriran uporabnik, pedagog ali skrbnik.
![pic](../img/zaporedje91.PNG)

#### 3.10 Ustvarjanje zbirke obveznosti
**Osnovni tok:**
Uporabnik je v tem primeru pedagog.
![pic](../img/zaporedje101.PNG)
**Alternativni tok:**
![pic](../img/zaporedje102.PNG)
**Izjemni tok:**
Potek za oba izjemna tokova je enak, le izpisana napaka se spremeni.
![pic](../img/zaporedje103.PNG)

#### 3.11 Pregled seznama vseh uporabnikov aplikacije
**Osnovni tok:**
![pic](../img/zaporedje111.PNG)

#### 3.12 Odstranitev uporabnika
**Osnovni tok:**
![pic](../img/zaporedje121.PNG)









