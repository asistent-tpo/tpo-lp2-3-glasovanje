# Načrt sistema

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Damjan Kalšan, Sabina Matjašič, Jan Šušteršič, Aleks Vujić|
| **Kraj in datum** | Ljubljana 21.4.2019 |



## Povzetek

Študentje se pogosto spopadajo s stresom zaradi izpitov, kolokvijev, domačih nalog, oddaj poročil in ostalih sprotnih obveznosti študija. Cilj aplikacije je pomagati študentom do te mere, da jim ne bo treba razmišljati, če so slučajno zamudil kakšen rok oddaje in jih na ta način razbremeniti in bolj organizirati. Spletni vmesnik aplikacije bi omogočal jasen pregled vseh obveznosti, kot so urnik vaj in predavanj, izpiti, kolokviji, domače naloge, oddaje poročil in ostale obveznosti, ki jih uporabnik vnese sam. Aplikacija bi torej datumsko zajemala roke vseh obveznosti v zvezi s študijem na enem mestu. Uporabnik pa bi si lahko aktivnosti razdelil tudi po prioriteti in si tako lažje organiziral delo za naslednje dni kar bi vodilo v učinkovitejše in lažje opravljanje vseh študijskih obveznostih.



## 1. Načrt arhitekture

### 1.1 Logični pogled

![Logicni pogled](../img/logicni-pogled.png)

### 1.2 Razvojni pogled

![Logicni pogled](../img/razvojni-pogled.png)


## 2. Načrt strukture

### 2.1 Razredni diagram

![Razredni diagram](../img/RazredniDiagram.png)

### 2.2 Opis razredov

#### 1. Oseba

`Oseba` je osnoven razred iz katerga so izpeljani vsi tipi oseb (študent, pedagoški delavec, organizator dogodkov in moderator). Vsebuje atribute, ki so vsem skupni.

##### Atributi

* `ID`
* `ime`:
    * **podatkovni tip:** string
* `priimek`:
    * **podatkovni tip:** string
* `email`:
    * **podatkovni tip:** string
* `geslo`:
    * **podatkovni tip:** string
* `zgoscenaVrednost`:
    * **podatkovni tip:** string
    * **pomen:** `geslo` in `nakljucnaVrednost` s pomočjo šifriranja pretvorimo v `zgoscenaVrednost`. Uporabna je pri prijavi v sistem, ko primerjamo, če se vnešen `geslo` skupaj z `nakljucnaVrednost` preslika v pravo `zgoscenaVrednost`.
* `nakljucnaVrednost`:
    * **podatkovni tip:** string
    * **pomen:** Generira se naključno ob registraciji uporabnika. Pri prijavi šifriramo `geslo` in `nakljucnaVrednost` in rezultat primerjamo z `zgoscenaVrednost`. Če pride do ujemanja, je prijava uspešna.

Shema:

![Prijava uporabnika](../img/osebaPrijava.png)

##### Nesamoumevne metode

* `nastaviGeslo(geslo: string) -> void`
    * **pomen:** `nakljucnaVrednost` dobi naključno vrednost dolžine 16 znakov. `zgoscenaVrednost` je rezultat šifriranja parametra `geslo` in prej dobljene `nakljucnaVrednost` s poljubnim šifrirnim algoritmom.
* `preveriGeslo(geslo: string) -> bool`
    * **pomen:** Izračunaj zgoščeno vrednost iz `geslo` in uporabnikove `nakljucnaVrednost`, ki je bila ustvarjena pri registraciji. Vrni `true`, če je dobljena zgoščena vrednost enaka uporabikovi `zgoscenaVrednost`.

#### 2. Študent

`Študent` je razred, ki vsebuje vse atribute in metode, ki jih vsebuje tudi razred `Oseba`. Predstavlja študenta, ki lahko dodaja opravila na svoj TO-DO seznam, dodaja termine na urnik in se prijavlja na kanale, ki so jih ustvarili pedagoški delavci.

##### Atributi

Študent vsebuje vse atribute, ki jih vsebuje tudi razred `Oseba`, torej `ID`, `ime`, `priimek`, `email`, `geslo`, `zgoscenaVrednost` in `nakljucnaVrednost`. Poleg tega vsebuje tudi atribut:

* `urnikZeUvozen`:
    * **podatkovni tip:** bool
    * **pomen:** ima vrednost `true`, če je študent že vnesel termine v urnik preko metode `dodajTerminNaUrnik(int: vpisnaStevilka)` in je bilo dodajanje uspešno
* `vpisnaStevilka`
    * **podatkovni tip:** int

##### Nesamoumevne metode

* `narociNaKanal(int: kanalId) -> void`
    * **pomen:** Metoda sprejme parameter `kanalId` in študenta z `ID`-jem (ki je shranjen v objektu študenta) naroči na kanal. Če kanal z ID-jem `kanalId` ne obstaja, se ne zgodi nič in program izpiše napako.
* `prijavaNaDogodek(dogodekId: int) -> void`
    * **pomen:** Metoda študenta z `ID`-jem (ki je shranjen v objektu študenta) prijavi na dogodek z `ID`-jem `dogodekId`. V primeru, da dogodek z `ID`-jem `dogodekId` ne obstaja, se ne zgodi nič in program vrne napako.
* `vrniVsePotrjeneDogodke() -> [Dogodek]`
    * **pomen:** vrne vse dogodke, na katere se je študent prijavil. Če se študent še ni prijavil na noben dogodek, metoda vrne prazen seznam.
* `poisciStudentaZVpisnoStevilko(vpisnaSt: int) -> Student`
    * **pomen:** vrne študenta, ki ima atribut `vpisnaStevilka` enak parametru `vpisnaSt` oz. `null`, če študent s tako vpisno številko ne obstaja.
* `vrniVseKanale() -> [Kanal]`
    * **pomen:** vrne vse kanale oz. prazen seznam, če ni nobenih kanalov
* `vrniVseDogodke() -> [Dogodek]`
    * **pomen:** vrne vse dogodke oz. prazen seznam, če ni nobenih dogodkov    

#### 3. Pedagoški delavec

`Pedagoški delavec` predstavlja pedagoškega delavca, ki je zaposlen na fakulteti.

##### Atributi

Pedagoški delavec vsebuje vse atribute, ki jih vsebuje tudi razred `Oseba`, torej `ID`, `ime`, `priimek`, `email`, `geslo`, `zgoscenaVrednost` in `nakljucnaVrednost` Dodatno pa vsebuje še:

* `kanali`:
    * **podatkovni tip:** [Kanal]

##### Nesamoumevne metode

* `prijavaNaDogodek(dogodekId: int) -> void`
    * **pomen:** Metoda pedagoškega delavca z `ID`-jem (ki je shranjen v objektu pedagoškega delavca) prijavi na dogodek z `ID`-jem `dogodekId`. V primeru, da dogodek z `ID`-jem `dogodekId` ne obstaja, se ne zgodi nič in program vrne napako.
* `vrniUstvarjeneKanale() -> [Kanal]`
    * **pomen:** vrne atribut `kanali`, torej vse kanale, ki jih je pedagoški delavec ustvaril
* `vrniVsePotrjeneDogodke() -> [Dogodek]`
    * **pomen:** vrne vse dogodke, na katere se je pedagoški delavec prijavil. Če se pedagoški delavec še ni prijavil na noben dogodek, metoda vrne prazen seznam.
* `vrniVseDogodke() -> [Dogodek]`
    * **pomen:** vrne vse dogodke oz. prazen seznam, če ni nobenih dogodkov      

#### 4. Organizator dogodkov

`Organizator dogodkov` predstavlja osebo, ki objavlja dogodke namenjene študentom in pedagoškim delavcem.

##### Atributi

Organizator dogodkov vsebuje vse atribute, ki jih vsebuje tudi razred `Oseba`, torej `ID`, `ime`, `priimek`, `email`, `geslo`, `zgoscenaVrednost` in `nakljucnaVrednost`.

##### Nesamoumevne metode

* `vrniVseDogodke() -> [Dogodek]`
    * **pomen:** vrne vse dogodke, ki jih je organizator ustvaril oz. prazen seznam, če ni ustvaril nobenega dogodka

#### 5. Moderator

`Moderator` predstavlja osebo, ki skrbi za kakovostno vsebino, zato lahko ureja ali briše kanale in ureja ali briše dogodke.

##### Atributi

Moderator vsebuje vse atribute, ki jih vsebuje tudi razred `Oseba`, torej `ID`, `ime`, `priimek`, `email`, `geslo`, `zgoscenaVrednost` in `nakljucnaVrednost`.

##### Nesamoumevne metode

* `vrniVseKanale() -> [Kanal]`
    * **pomen:** vrne vse kanale oz. prazen seznam, če ni nobenih kanalov
* `vrniVseDogodke() -> [Dogodek]`
    * **pomen:** vrne vse dogodke oz. prazen seznam, če ni nobenih dogodkov

#### 5. Kanal

`Kanal` je oblika komunikacije med pedagoškim delavcem in študenti, ki so naročeni na ta kanal.

##### Atributi

* `ID`
* `naziv`:
    * **podatkovni tip:** string
* `kratekOpis`:
    * **podatkovni tip:** string

##### Nesamoumevne metode

* `ustvariKanal(naziv: string, kratekOpis: string) -> Kanal`
    * **pomen:** ustvari kanal s parametri `naziv`, `kratekOpis` in ga vrne uporabniku. Oba parametra sta obvezna.
* `dodajKanal(kanal: Kanal) -> void`
    * **pomen:** sprejme kanal z vsemi potrebnimi parametri in ga ustvari. Študenti se lahko kasneje na kanal naročijo. Če kanal nima vseh potrebnih polj, metoda ne naredi nič in program izpiše napako.  
* `posljiSporociloVsemNaKanalu(sporocilo: string) -> void`
    * **pomen:** vsem študentom, ki so prijavljeni na kanal pošlje sporočilo `sporocilo`. Če je sporočilo dolžine 0 ali `null`, se ne zgodi nič in aplikacija vrne napako.
* `urediKanal(kanal: Kanal) -> void`
    * **pomen:** sprejme kanal `kanal` in posodobi vse njegove atribute. Če kanal ne vsebuje vseh potrebih podatkov, aplikacija vrne napako.
* `izbrisiKanal() -> void`
    * **pomen:** metoda izbriše kanal. Vsi študenti, ki so bili naročeni na ta kanal, so samodejno odjavljeni.

#### 6. Urnik

`Urnik` lahko vsebuje nič ali več terminov predavanj in vaj.

##### Atributi

* `ID`
* `studentId`
* `termini`:
    * **podatkovni tip:** `[Termin]`

##### Nesamoumevne metode

* `vrniVseTermine() -> [Termin]`
    * **pomen:** vrne vse termine, ki so bili dodani na urnik (atribut `termini`). Če na urnik še ni bil dodan noben termin, metoda vrne prazen seznam.
* `dodajTerminNaUrnik(termin: Termin) -> void`
    * **pomen:** Metoda sprejme termin in ga doda na urnik. Če termin ne vsebuje vseh potrebnih parametrov, se ne zgodi nič in aplikacija vrne napako.
* `dodajTerminNaUrnik(int: vpisnaStevilka) -> void`
    * **pomen:** Metoda sprejme vpisno številko študenta, kontaktira zunanji vmesnik `Urnik FRI` in v urnik študenta (z ID-jem `studentId`) vnese vse termine, ki jih pridobi iz zunanjega vmesnika. Če `vpisnaStevilka` ne obstaja, se ne zgodi nič in program izpiše napako.
* `izbrisiTermin(termin: Termin) -> void`
    * **pomen:** metoda sprejme `termin` in ga izbriše iz urnika.

#### 7. Termin

`Termin` vključuje podatke o terminu predavanj in vaj.

##### Atributi

* `ID`
* `nazivPredmeta`:
    * **podatkovni tip:** string
* `terminIzvajanja`:
    * **podatkovni tip:** objekt (`{od: time, do: time}`), kjer mora biti `od < do`
* `danIzvajanja`:
    * **podatkovni tip:** int [0,4], `0` predstavlja ponedeljek, `4` pa petek
* `tipTermina`:
    * **podatkovni tip:** int [0,1], `0` predstavlja predavanja, `1` pa vaje

##### Nesamoumevne metode

* `izbrisiTermin() -> void`
    * **pomen:** izbriše termin

#### 8. TO-DO seznam

`TO-DO seznam` vsebuje opravila, ki jih je dodal uporabnik.

##### Atributi

* `ID`
* `seznamOpravil`
    * **podatkovni tip:** [Opravilo]

##### Nesamoumevne metode

* `dodajOpraviloNaToDoSeznam(opravilo: Opravilo) -> void`
    * **pomen:** Metoda sprejme opravilo in ga doda na TO-DO seznam. Če opravilo ne vsebuje vseh ustreznih parametrov, ali pa je njegov `rokZapadlosti` v preteklosti, se ne zgodi nič in program vrne napako.
* `vrniVsaOpravila() -> [Opravilo]`
    * **pomen:** vrne vsa opravila, torej `seznamOpravil`
* `pridobiOpraviloPoId(int: opraviloId) -> Opravilo`
    * **pomen:** sprejme parameter `opraviloId` in vrne opravilo s tem `ID`-jem. Če opravilo s tem `ID`-jem ne obstaja, metoda vrne `null`.
* `izbrisiOpraviloIzSeznama(idOpravila: int) -> void`
    * **pomen:** metoda sprejme `idOpravila` in izbriše opravilo s tem `ID`-jem. Če opravilo s tem `ID`-jem ne obstaja, se ne zgodi nič in aplikacija vrne napako.

#### 9. Opravilo

`Opravilo` predstavlja nekaj, kar mora študent opraviti. Pri tem si lahko določi tudi rok, do kdaj mora biti opravljeno.

##### Atributi

* `ID`
* `nazivOpravila`:
    * **podatkovni tip:** string
* `rokZapadlosti`
    * **podatkovni tip:** dateTime
* `kratekOpis`
    * **podatkovni tip:** string
* `opravljeno`
    * **podatkovni tip:** bool

##### Nesamoumevne metode

* `ustvariOpravilo(nazivOpravila: string, rokZapadlosti: dateTime, kratekOpis: string) -> Opravilo`
    * **pomen:** ustvari opravilo s parametri `nazivOpravila`, `rokZapadlosti`, `kratekOpis` in ga vrne uporabniku. Parameter `nazivOpravila` je obvezen, ostala dva sta neobvezna. `rokZapadlosti` ne sme biti v preteklosti.
* `urediOpravilo(opravilo: Opravilo) -> void`
    * **pomen:** sprejme opravilo `opravilo` (ki vsebuje atribut `ID`) in posodobi vse njegove atribute. Če opravilo ne vsebuje vseh potrebih podatkov, aplikacija vrne napako.
* `izbrisiOpravilo() -> void`
* `oznaciKotOpravljeno() -> void`
    * **pomen:** označi opravilo kot opravljeno (nastavi `opravljeno` na `true`)

#### 10. Dogodek

`Dogodek` lahko objavijo organizatorji dogodkov, nanj pa se lahko prijavijo predagoški delavci in študenti.

##### Atributi

* `ID`
* `naziv`:
    * **podatkovni tip:** string
* `tip`
    * **podatkovni tip:** string
* `kratekOpis`
    * **podatkovni tip:** string
* `kraj`
    * **podatkovni tip:** string
* `cas`
    * **podatkovni tip:** dateTime

##### Nesamoumevne metode

* `ustvariDogodek(naziv: string, tip: string, kratekOpis: string, kraj: string, cas: dateTime) -> Dogodek`
    * **pomen:** metoda sprejme vse parametre, ki so potrebni, da ustvari dogodek, uporablja vzorec tovarne (angl. factory). Vsi parametri so obvezni. Parameter `cas` ne sme biti v preteklosti. Parametri tipa `string` morajo biti daljši od 0 znakov.
* `preglejPrijavljene() -> [Oseba]`
    * **pomen:** metoda vrne vse osebe, ki so se na ta dogodek prijavile. Oseba je lahko tipa `Študent` ali `Pedagoški delavec`. Če se na dogodek ni prijavil nihče, vrne prazen seznam.
* `dodajDogodek(dogodek: Dogodek) -> void`
    * **pomen:** metoda sprejme dogodek, ki ga je predhodno vrnila metoda `ustvariDogodek` in ustvari dogodek. Če `dogodek` ne vsebuje vseh atributov, se ne zgodi nič in aplikacija izpiše napako.
* `urediDogodek(dogodek: Dogodek) -> void`
    * **pomen:** sprejme dogodek `dogodek` in posodobi vse njegove atribute. Če dogodek ne vsebuje vseh potrebih podatkov, aplikacija vrne napako.
* `izbrisiDogodek() -> void`
    * **pomen:** Izbriše dogodek. Vsi študenti in pedagoški delavci, ki so bili prijavljeni na ta dogodek, so samodejno odjavljeni.

## 3. Načrt obnašanja

### 3.1 Ogled urnika

![Diagram zaporedja za ogled urnika](../img/ogled-urnika.png)

### 3.2 Dodajanje terminov predavanj in vaj

![Diagram zaporedja za dodajanje terminov](../img/dodajanje-terminov.png)

### 3.3 Brisanje terminov predavanj in vaj

![Diagram zaporedja za brisanje terminov](../img/brisanje-terminov.png)

### 3.4 Prikaz TO-DO seznama

![Diagram zaporedja za brisanje terminov](../img/prikazi-TODO.png)

### 3.5 Dodajanje opravil na TO-DO seznam

![Diagram zaporedja za dodajanje opravil na TO-DO seznam](../img/dodajanje-opravil-na-to-do-seznam.png)

### 3.6 Dodajanje dogodka

![Diagram zaporedja za dodajanje dogodka](../img/dodajanje-dogodka.png)

### 3.7 Pregledovanje potrjenih dogodkov

Diagram poteka za pedagoškega delavca je identičen spodnjemu diagramu poteka za študenta. Različni so zgolj objekti in akter in sicer:
* študent: Študent -> pedagog: Pedagoški delavec
* controllerŠtudent: Študent -> controllerPedagog: Pedagoški delavec
* modelŠtudent: Študent -> modelPedagog: Pedagoški delavec

![Diagram zaporedja za pregledovanje potrjenih dogodkov](../img/pregledovanje-potrjenih-dogodkov.png)

### 3.8 Pregledovanje vseh prijavljenih na dogodek

![Diagram zaporedja za pregledovanje vseh prijavljenih na dogodek](../img/pregledovanje-vseh-prijavljenih-na-dogodek.png)

### 3.9 Ustvarjanje kanala za predmet

![Diagram zaporedja za ustvarjanje kanala za predmet](../img/ustvarjanje-kanala-za-predmet.png)

### 3.10 Prijava na dogodek

Diagram poteka za pedagoškega delavca je identičen spodnjemu diagramu poteka za študenta. Različni so zgolj objekti in akter in sicer:
* študent: Študent -> pedagog: Pedagoški delavec
* controllerŠtudent: Študent -> controllerPedagog: Pedagoški delavec
* modelŠtudent: Študent -> modelPedagog: Pedagoški delavec

![Diagram zaporedja za prijavo na dogodek](../img/prijava-na-dogodek.png)

### 3.11 Urejanje dogodka

Diagram poteka za moderatorja je identičen spodnjemu diagramu poteka za organizatorja dogodkov. Različni so zgolj objekti in akter in sicer:
* organizator: Organizator dogodkov -> moderator: Moderator
* controllerOrganizator: Organizator dogodkov -> controllerModerator: Moderator
* modelOrganizator: Organizator dogodkov -> modelModerator: Moderator

![Diagram zaporedja za urejanje dogodka](../img/urejanje-dogodka.png)

### 3.12 Oznaci kot opravljeno

![Diagram zaporedja za dodajanje opravil na TO-DO seznam](../img/oznaciKotOpravljeno12.png)

### 3.13 Naročanje na kanal

![Diagram zaporedja za dodajanje opravil na TO-DO seznam](../img/narocanje_na_kanal13.png)

### 3.14 Urejanje TO-DO seznama

![Diagram zaporedja za dodajanje opravil na TO-DO seznam](../img/urejanje_to-do_seznama14.png)

### 3.15 Urejanje kanala

Diagram poteka za moderatosja je identičen spodnjemu diagramu poteka za pedagoškega delavca. Različni so zgolj objekti, funkcija in akter in sicer:
* pedagog: Pedagoški delavec -> moderator: Moderator
* controller: Pedagoški delavec -> controller: Moderator
* model: Pedagoški delavec -> model: Moderator
* vrniKanali() -> vrniVseKanale()

![Diagram zaporedja za dodajanje opravil na TO-DO seznam](../img/urejanjeKanala15.png)

### 3.16 Brisanje dogodka

Diagram poteka za moderatorja je identičen spodnjemu diagramu poteka za organizatoja dogodkov. Različni so zgolj objekti in akter in sicer:
* organizator dogodkov -> moderator: moderator
* controllerOrganizatorDogodkov: Organizator dogodkov-> controllerModerator: moderator
* modelOrganizatorDogodkov: OrganizatorDogodkov -> modelModerator: moderator

![Diagram zaporedja za dodajanje opravil na TO-DO seznam](../img/brisanjeDogodka16.png)

### 3.17 Pošiljanje sporočil vsem prijavljenim na kanal

![Diagram zaporedja za pošiljanje sporočil vsem prijavljenim na kanal](../img/poslji-sporocilo-kanal.png)

### 3.18 Brisanje TO-DO seznama

![Diagram zaporedja za dodajanje opravil na TO-DO seznam](../img/brisanje_todo_seznama18.png)

### 3.19.1 Brisanje kanala pedagog

![Diagram zaporedja za dodajanje opravil na TO-DO seznam](../img/brisanjeKanalaPedagog19.png)

### 3.19.2 Brisanje kanala moderator

![Diagram zaporedja za dodajanje opravil na TO-DO seznam](../img/brisanjeKanalaModerator19.png)
