# Dokument zahtev

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Damjan Kalšan, Sabina Matjašič, Jan Šušteršič, Aleks Vujić |
| **Kraj in datum** | Ljubljana, 4.4.2019 |



## Povzetek projekta

Študentje se pogosto spopadajo s stresom zaradi izpitov, kolokvijev, domačih nalog, oddaj poročil
in ostalih sprotnih obveznosti študija. Cilj aplikacije je pomagati študentom 
do te mere, da jim ne bo treba razmišljati, če so slučajno zamudil kakšen rok oddaje in jih na ta način 
razbremeniti in bolj organizirati. Spletni vmesnik aplikacije bi omogočal jasen pregled vseh obveznosti,
kot so urnik vaj in predavanj, izpiti, kolokviji, domače naloge, oddaje poročil in ostale obveznosti, ki
jih uporabnik vnese sam. Aplikacija bi torej datumsko zajemala roke vseh obveznosti v zvezi 
s študijem na enem mestu. Uporabnik pa bi si lahko aktivnosti razdelil tudi po prioriteti in si 
tako lažje organiziral delo za naslednje dni kar bi vodilo v učinkovitejše in lažje opravljanje
vseh študijskih obveznostih. 



## 1. Uvod

Veliko študentov se vsakodnevno spopada s stresom, ki ga povzročajo izpiti, kolokviji, obvezne domače naloge,
oddaja poročil, prisotnost na vajah, predavanjih in ostale obveznosti, ki spadajo k uspešnem opravljanju študija. Zato se dogaja,
da dodaten stres povzroča samo misel na to, da smo v množici rokov za oddajo spregledali kakšen datum in zato ne 
bomo mogli pozitivno opraviti predmeta, saj študij od nas zahteva čedalje več pomembnih sprotnih obveznosti. 

Cilj aplikacije je torej rešiti problem v zvezi z roki oddaj in na ta način razbremeniti študenta bojazni,
da je nekaj pozabil in mu na ta način omogočiti bolj organizirano in učinkovitejše opravljanje študija.
Aplikacija bi imela uporabniku prijazen grafični uporabniški vmesnik, kjer bo imel uporabnik pregled nad urnikom predavanj in vaj,
datumi kolokvijev, izpitov, oddaj poročil, domačih nalog in ostalih obveznosti, ki jih zahteva študij.
Uporabnik pa bo imel tudi možnost organiziranja obveznosti po pomembnosti in si tako lažje zadal urnik dela za naslednje dni.    
  
Na ta način bi dosegli, da bi imeli seznam vseh obveznosti na enem mestu in vedno pri roki.
Aplikacija nam pripomore v zanesljivejše in lažje opravljanje obveznosti, to pa vodi k
uspešnejšemu in manj stresnemu študiju. Na spletu lahko najdemo kar nekaj podobnih aplikacij za
organiziranje in prikazovanje rokov. Aplikacija StraightAs pa bi se od ostalih razlikovala v tem, da je bolj
specifično usmerjena, saj je v prvi vrsti namenjena študentom v Sloveniji. Prav tako bi bila izdelana
s strani študentov za študente, ki najbolj vedo kaj si v aplikaciji želijo. Aplikacija, ki bo oblikovana kot spletni 
vmesnik bo omogočala hraniti vse pomembne podatke v zvezi s študijem, torej urnik vaj in predavanj, 
ostale obveznosti kot so oddaje domačih nalog, poročil, kvizov, datumi kolokvijev, izpitov in ostalih 
poljubnih obveznosti, ki jih zadaja študij. Omogočala bo tudi zapis ocen, opisov obveznosti in prijava na dogodke,
 ki so bili objavljenji s strani organizatorja. Obveznosti bo mogoče
dodajati, odstranjevati, jim nastavljati prioritete, opise in roke do zapadlosti. Na ta način dosežemo,
da imamo »TODO list« in točno vidimo kaj smo že opravili, kaj še moramo in do kdaj. 
Možen bo bil pregled vseh obveznosti v izbranem dnevu, tedensko ali mesečno, lahko tudi nastavimo 
prikaz obveznosti določenega tipa ali vse skupaj. Prisotnih bi bilo več uporabniških vlog. Na ta način razdelimo
dostop do različnih funkcionalnosti, ki jih aplikacija omogoča. 

Aplikacija mora biti varna, hitra in odzivna, prav tako mora biti stalno dosegljiva, da lahko uporabniki dostopajo do njekdarkoli želijo. Vsi tej gradniki dajejo aplikaciji nek pečat zanesljivosti in varnosti, kar je za uporabnika zelo pomembno.
  
Cilj aplikacije StraightAs je, da bi študent pred začetkom dela obiskal aplikacijo,
ki mu bo ponudila današnje obveznosti urejene po željeni prioriteti, se tako počutil bolj 
organiziranega in lahko takoj začel z delom.
  


## 2. Uporabniške vloge

* **Študent**
    * Lahko pregleduje, ureja in briše termine predavanj in vaj.
    * Lahko pregleduje svoj TO-DO seznam, nanj dodaja nove aktivnosti in jih označi kot opravljene. Lahko ureja posamezno aktivnost ali jo izbriše.
    * Lahko potrdi udeležbo na dogodku, ki ga je predhodno objavil organizator dogodkov. Lahko pregleduje seznam dogodkov, na katere se je prijavil.
    * Ima pregled nad lastnimi predavanji in vajami, roki za oddajo domačih nalog, datumi izpitnih rokov in opomniki.
    * Lahko se naroči na poljubno število kanalov, ki so jih kreirali pedagoški delavci.
* **Organizator dogodkov**
    * Lahko na koledar doda nov dogodek namenjen študentom in pedagoškim delavcem.
    * Lahko preveri, kdo vse se je prijavil na dogodek.
    * Lahko ureja ali izbriše dogodek.
* **Pedagoški delavec**
    * Za vsak predmet lahko ustvari, ureja in briše kanal.
    * Lahko se prijavlja na dogodke, ki jih je objavil organizator dogodkov.
* **Moderator**
    * Lahko ureja in briše dogodke.
    * Lahko ureja in briše kanale.

## 3. Slovar pojmov

* **TO-DO seznam:** seznam vseh rokov za oddajo domačih nalog, izpitnih rokov in opomnikov, ki jih študent še ni opravil
* **opraviti:** študent lahko opravi domačo nalogo, izpitni rok ali opomnik, le-ta nato izgineta iz TO-DO seznama
* **uvoz terminov predavanj in vaj:** kontaktiranje sistema Urnik FRI, ki posreduje termine pripravljene za uvoz
* **termin:** za opis terminujema potrebo čas začetka (npr. 13:40) in čas konca (14:25), po želji lahko dodamo periodo, s katero se ponavlja (npr. vsak četrtek)
* **predavanje:** oblika pedagoškega dela na fakulteti, ki običajno traja 3 ure, poteka v večjih predavalnicah, izvaja ga pedagoški delavec
* **vaje:** oblika pedagoškega dela na fakulteti v manjših skupinah, ki običajno traja 2 uri, poteka v manjših učilnicah, izvaja jih pedagoški delavec
* **domača naloga:** naloga, ki jo mora študent izdelati doma do določenega roka
* **šolsko leto:** obdobje od 1.10 do 30.9
* **predmet:** predmet je sestavljen iz predavanj in vaj, na koncu študent pridobi oceno na enem izmed razpisanih izpitnih rokov
* **ocena:** število na intervalu od 5 do 10
* **izpitni rok:** v vsakem šolskem letu so pri vsakem predmetu razpisani trije izpitni roki, na izpitnem roku študent piše izpit, za katerega dobi oceno
* **rok:** datum in čas (npr. 23.3.2019, 21:55), do takrat morata biti domača naloga ali opomnik opravljena
* **opomnik:** sestavljen je iz roka, do kdaj mora biti končan in opisa (npr. pošlji sporočilo pedagoškemu delavcu)
* **dogodek:** organizirano srečanje, ki ga organizirajo organizatorji dogodkov
* **e-pošta:** sodoben način izmenjave sporočil preko spleta
* **obvestilo:** pisno obvestilo, ki ga študent prejme po e-pošti
* **kanal:** medij za izmenjavo sporočil z enoličnim imenom, na katerega se lahko študenti naročijo
* **naročiti se:** študent se lahko naroči na kanal pedagoškega delavca, ob vsakem poslanem sporočilu študent prejme obvestilo po e-pošti

## 4. Diagram primerov uporabe

![UML](../img/uml.png)

## 5. Funkcionalne zahteve

### 1. Ogled urnika

#### Povzetek funkcionalnosti
Študent lahko pregleduje svoj urnik na katerem so prikazani termini predavanj in vaj, ki jih je dodal samostojno ali uvozil iz "Urnik FRI". Prav tako so na urniku prikazani tudi vsi dogodki. Ob prikazu urnika se vedno sprva prikaže urnik za tekoči teden, a ima študent možnost izbrati tudi poljuben teden.

#### Osnovni tok
1. Študent izbere funkcionalnost *Ogled urnika*.
2. Sistem prikaže urnik na katerem so vse vaje in predavanja, ki jih je študent vnesel v urnik ter dogodki, ki so jih v sistem vnesli organizatorji dogodkov. Iz urnika je razviden dan in ura posameznih vaj, predavanj in dogodkov.

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot študent, v nasprotnem primeru mu funkcionalnost ni na voljo.

#### Posledice
Ni posledic, saj funkcionalnost vključuje le branje informacij.

#### Posebnosti
Ogled urnika nima posebnosti.

#### Prioriteta
Must have.

#### Sprejemni testi
1. Prijavi se kot študent in izberi funkcionalnost *Ogled urnika*.  Oglej si vse vaje, predavanja in dogodke za tekoči teden.
2. Prijavi se kot študent in izberi funkcionalnost *Ogled urnika*. Izberi poljuben teden. Oglej si vse vaje, predavanja in dogodke za izbrani teden.

### 2. Dodajanje terminov predavanj in vaj

#### Povzetek funkcionalnosti
Študent lahko na svoj urnik doda termine predavanj in vaj. Te lahko vnese ročno ali pa jih uvozi preko "Urnik FRI".

#### Osnovni tok
1. Študent izbere funkcionalnost *Ogled urnika*.
2. Študent izbere funkcionalnost *Dodaj termin predavanj ali vaj*, ki je vezana na gumb 'Dodaj termin'.
3. Sistem prikaže možnost izbire med ročnim vnašanjem termina ali uvozom urnika iz "Urnik FRI".
4. Študent izbere ročno vnašanje termina.
5. Študent vnese uro, dan, tip (vaje ali predavanja) in naziv predmeta.
6. Študent potrdi svojo izbiro.
7. Sistem prikaže sporočilo o uspešno dodanem terminu.

#### Alternativni tok
1. Študent izbere funkcionalnost *Ogled urnika*.
2. Študent izbere funkcionalnost *Dodaj termin predavanj ali vaj*.
3. Sistem prikaže možnost izbire med ročnim vnašanjem termina ali uvozom urnika iz "Urnik FRI".
4. Študent izbere uvoz urnika iz "Urnik FRI".
5. Sistem prikaže polje za vnos svoje vpisne številke.
6. Študent vnese svojo vpisno številko in jo potrdi.
7. Sistem prikaže sporočilo o uspešnem uvozu urnika.

#### Izjemni tokovi
1. Študent je že uvozil urnik iz "Urnik FRI", a želi to storiti ponovno. Sistem ga o tem o obvesti in mu ponudi možnost ponovnega uvoza. V kolikor se študent odloči zanj, se vsi temini predavanj in vaj, ki so bili dodani s predhodnjim uvozom izbrišejo iz urnika, nato pa sistem ponovno uvozi urnik.
2. Študent je ob uvozu urnika iz "Urnik FRI" vnesel napačno vpisno številko. Sistem ga o tem obvesti in ga prosi za ponovem vpis le te.

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot študent, v nasprotnem primeru mu funkcionalnost ni na voljo.

#### Posledice
Ko študent doda termin predavanj oz. vaj ali uvozi urnik iz "Urnik FRI", se vsi dodani termini prikažejo na njegovem urniku.

#### Posebnosti
Dodajanje terminov predavanj in vaj urnika nima posebnosti.

#### Prioriteta
Must have.

#### Sprejemni testi
1. Prijavi se kot študent, odpri urnik in izberi možnost za dodajanje terminov vaj in predavanj. Izberi uvoz urnika in vpiši veljavno vpisno številko.
2. Prijavi se kot študent, odpri urnik in izberi možnost za dodajanje terminov vaj in predavanj. Izberi uvoz urnika in vpiši neveljavno vpisno številko.


### 3. Brisanje terminov predavanj in vaj

#### Povzetek funkcionalnosti
Študent lahko iz svojega urnika izbriše termin predavanj ali vaj.

#### Osnovni tok
1. Študent izbere funkcionalnost *Ogled urnika*.
2. Študent izbere funkcionalnost *Brisanje termina predavanj ali vaj*, ki je vezana na gumb 'Brisanje termina'.
3. Sistem študentu prikaže seznam predavanj in vaj, ki so na njegovem urniku.
4. Študent izbere termin ki ga želi izbrisati in potrdi svojo izbiro.
5. Sistem prikaže sporočilo o uspešno izbrisanem terminu.

#### Alternativni tok
1. Študent izbere funkcionalnost *Ogled urnika*.
2. Študent izbere enega od terminov vaj oz. predavanj, ki sta prikazana na urniku.
3. Študent pritisne gumb 'Izbriši termin'.
4. Sistem prikaže sporočilo o uspešno izbrisanem terminu.

#### Izjemni tokovi
1. Študent izbere funkcionalnost brisanje termina predavanj ali vaj, a na svojem urniku nima nobenih vaj ali predavanj. Sistem študenta obvesti o tem in mu ponudi funkcionalnost *Dodaj termin predavanj ali vaj*.

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot študent, v nasprotnem primeru mu funkcionalnost ni na voljo.

#### Posledice
Ko študent izbriše termin predavanj oz. vaj, se izbrani termin vaj oz. predavanj ne bo več prikazoval na njegovem urniku.

#### Posebnosti
Brisanje terminov predavanj in vaj urnika nima posebnosti.

#### Prioriteta
Must have.

#### Sprejemni testi
1. Prijavi se kot študent, odpri urnik in izberi nek termin predavanj ali vaj. Klikni izbriši termin in potrdi svojo izbiro.

### 4. Pregled TO-DO seznama

#### Povzetek funkcionalnosti
Študent lahko pregleduje svoj TO-DO seznam, na katerem so vsa opravila (npr. domače naloge), ki jih je sam predhodno dodal na list. Iz TO-DO seznama lahko razbere, katera opravila so že dokončana in katera ne.

#### Osnovni tok
1. Študent izbere funkcionalnost *Pregled TO-DO seznama*.
2. Sistem prikaže seznam, na katerem so vsa opravila, ki jih je študent predhodno dodal. Iz njega je razvidno ali so ta že opravljena in pa če imajo rok, do katerega jih je potrebno opraviti.

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot študent, v nasprotnem primeru mu funkcionalnost ni na voljo.

#### Posledice
Ni posledic, saj funkcionalnost vključuje le branje informacij.

#### Posebnosti
Brisanje terminov predavanj in vaj urnika nima posebnosti.

#### Prioriteta
Must have.

#### Sprejemni testi
1. Prijavi se kot študent, odpri TO-DO seznam.
2. Prijavi se kot organizator dogodkov, TO-DO seznam ni na voljo.

### 5. Dodajanje opravil na TO-DO seznam

#### Povzetek funkcionalnosti
Študent lahko na TO-DO seznam dodaja poljubne naloge. To so ponavadi domače naloge, lahko pa tudi povsem poljubne stvari. Pri dodajanju naloge lahko opcijsko študent doda tudi kratek opis in rok.

#### Osnovni tok
1. Študent izbere funkcionalnost *Pregled TO-DO seznama*.
2. Študent izbere funkcionalnost *Dodajanje opravila*, ki je vezana na gumb 'Dodaj opravilo'.
3. Sistem študentu prikaže vnosna polja za naziv, kratek opis in rok opravila.
4. Študent vnese podatke in s pritiskom na gumb 'Dodaj' potrdi svojo izbiro.
5. Sistem študenta obvesti o uspešno dodanem opravilu

#### Izjemni tokovi
1. Študent vnese neveljaven datum za opravilo. Sistem ga o tem obvesti in zahteva ponovnen vnos datuma.

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot študent, v nasprotnem primeru mu funkcionalnost ni na voljo.

#### Posledice
Ko študent doda opravilo bo to prikazano na njegovem TO-DO seznamu.

#### Posebnosti
Dodajanje opravil na TO-DO seznam nima posebnosti.

#### Prioriteta
Must have.

#### Sprejemni testi
1. Prijavi se kot študent, odpri TO-DO listo in klikni na gumb 'Dodaj opravilo'. Vnesi vse zahtevane podatke in potrdi izbiro.


### 6. Dodajanje dogodka

#### Povzetek funkcionalnosti
Organizator dogodkov lahko na seznam doda nov dogodek namenjen študentom in pedagoškim delavcem. Pri tem mora navesti naziv, tip, kratek opis, kraj in čas dogodka.

#### Osnovni tok
1. Organizator dogodkov pritisne navigacijski gumb 'Seznam dogodkov'.
2. Organizator dogodkov izbere funkcionalnost *Dodajanje dogodka*, ki je vezana na gumb 'Dodaj dogodek'.
3. Sistem prikaže vnosna polja za naziv dogodka, tip dogodka, kratek opis, kraj in čas dogodka, ki jih mora organizator dogodkov izpolniti.
4. Organizator dogodkov potrdi dodajanje dogodka.
5. Sistem prikaže sporočilo o uspešnem kreiranju dogodka.

#### Izjemni tokovi
1. Organizator dogodkov ni izpolnil vseh vnosnih polj (naziv dogodka, tip dogodka, kratek opis, kraj in čas dogodka). Sistem v tem primeru ne dovoli kreiranja dogodka in prikaže opozorilo, da morajo biti izpolnjena vsa polja.

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot organizator dogodkov, v nasprotnem primeru mu funkcionalnost ni na voljo.

#### Posledice
Ko organizator dogodkov doda dogodek, se ta prikaže na seznamu dogodkov študentom in pedagoškim delavcem, kjer lahko potrdijo udeležbo.

#### Posebnosti
Dodajanje dogodka nima posebnosti.

#### Prioriteta
Must have.

#### Sprejemni testi
1. Prijavi se kot organizator dogodkov, izpolni vsa vnosna polja (naziv dogodka, tip dogodka, kratek opis, kraj in čas dogodka) in dodaj dogodek.
2. Prijavi se kot organizator dogodkov in izpolni vsa vnosna polja razen naziva dogodka.

### 7. Pregledovanje potrjenih dogodkov

#### Povzetek funkcionalnosti
Študent ali pedagoški delavec lahko pregleduje seznam dogodkov, na katere se je prijavil (potrdil udeležbo).

#### Osnovni tok
1. Študent ali pedagoški delavec izbere funkcionalnost *Pregledovanje potrjenih dogodkov*, ki je vezana na navigacijski gumb 'Odprte prijave'.
2. Sistem prikaže seznam prihajajočih dogodkov, za katere je študent ali pedagoški delavec potrdil udeležbo.

#### Izjemni tokovi
1. Študent ali pedagoški delavec ni potrdil udeležbe za noben dogodek. Sistem prikaže obvesilo, da študent ali pedagoški delavec še ni potrdil udeležbe za noben dogodek.

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot študent ali pedagoški delavec, v nasprotnem primeru mu funkcionalnost ni na voljo.

#### Posledice
Ni posledic, saj funkcionalnost vključuje le branje informacij.

#### Posebnosti
Pregledovanje potrjenih dogodkov nima posebnosti.

#### Prioriteta
Should have.

#### Sprejemni testi
1. Prijavi se kot študent ali pedagoški delavec, potrdi udeležbo na vsaj enem dogodku in preglej dogodke na katere si prijavljen.
2. Prijavi se kot študent ali pedagoški delavec, ki predhodno še ni potrdil udeležbe na nobenem dogodku.

### 8. Pregledovanje vseh prijavljenih na dogodek

#### Povzetek funkcionalnosti
Organizator dogodkov lahko pregleduje seznam, na katerem so vsi študenti, ki so se do tega trenutka že prijavili na dogodek.

#### Osnovni tok
1. Organizator dogodkov izbere funkcionalnost *Pregledovanje vseh prijavljenih na dogodek*, ki je vezana na navigacijski gumb 'Pregled prijav'.
2. Sistem ponudi seznam prihajajočih dogodkov, ki jih je organizator dogodkov kreiral.
3. Organizator dogodkov izbere dogodek, za katerega bi rad preveril vse prijavljene.
4. Sistem prikaže seznam vseh prijavljenih na izbrani dogodek.

#### Izjemni tokovi
1. Organizator izbere dogodek, na katerega ni prijavljen nihče. Sistem v tem primeru prikaže obvestilo, da na ta dogodek ni prijavljen nihče.

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot organizator dogodkov, v nasprotnem primeru mu funkcionalnost ni na voljo.

#### Posledice
Ni posledic, saj funkcionalnost vključuje le branje informacij.

#### Posebnosti
Pregledovanje vseh prijavljenih na dogodek nima posebnosti.

#### Prioriteta
Could have.

#### Sprejemni testi
1. Prijavi se kot organizator dogodkov, izberi funkcionalnost *Pregledovanje vseh prijavljenih na dogodek*, izberi dogodek na katerega je prijavljen vsaj en študent ali pedagoški delavec.
2. Prijavi se kot organizator dogodkov, izberi funkcionalnost *Pregledovanje vseh prijavljenih na dogodek*, izberi dogodek na katerega ni prijavljen nihče.

### 9. Ustvarjanje kanala za predmet

#### Povzetek funkcionalnosti
Pedagoški delavec lahko po potrebi ustvari kanal za predmet, ki ga poučuje. Pri tem mora navesti naziv predmeta in kratek opis.

#### Osnovni tok
1. Pedagoški delavec pritisne na navigacijski gumb 'Seznam kanalov'.
2. Pedagoški delavec izbere funkcionalnost *Ustvarjanje kanala za predmet*, ki je vezana na gumb 'Dodaj kanal'.
3. Sistem prikaže vnosna polja za naziv predmeta in kratek opis.
4. Pedagoški delavec izpolni vsa vnosna polja (naziv predmeta in kratek opis) in potrdi ustvarjanje kanala.
5. Sistem prikaže obvestilo, da je bil kanal uspešno ustvarjen.

#### Izjemni tokovi
1. Pedagoški delavec ni izpolnil vseh vnosnih polj (naziv predmeta in kratek opis). Sistem v tem primeru ne dovoli kreiranja kanala in prikaže opozorilo, da morajo biti izpolnjena vsa polja.

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot pedagoški delavec, v nasprotnem primeru mu funkcionalnost ni na voljo.

#### Posledice
Če pedagoški delavec kreira kanal, se kanal prikaže študentom, ki se nanj lahko prijavijo.

#### Posebnosti
Ustvarjanje kanala za predmet nima posebnosti.

#### Prioriteta
Must have.

#### Sprejemni testi
1. Prijavi se kot pedagoški delavec. Pritisni navigacijski gumb 'Seznam kanalov'. Izberi funkcionalnost *Ustvarjanje kanala za predmet*, izpolni vsa vnosna polja (naziv predmeta in kratek opis) in potrdi kreiranje kanala.
2. Prijavi se kot pedagoški delavec. Pritisni navigacijski gumb 'Seznam kanalov'. Izberi funkcionalnost *Ustvarjanje kanala za predmet*, ne izpolni vseh vnosnih polj.

### 10. Prijava na dogodek

#### Povzetek funkcionalnosti
Študent in pedagoški delavec se lahko prijavita na dogodek, ki se še ni zgodil in ga je predhodno dodal organizator dogodkov.

#### Osnovni tok
1. Študent ali pedagoški delavec izbere funkcionalnost *Prijava na dogodek*.
2. Študent ali pedagoški delavec iz seznama izbere prihajajoči dogodek, na katerega se želi prijaviti.
3. Sistem izpiše podrobnosti dogodka.
4. Študent ali pedagoški delavec potrdi prijavo.
5. Sistem pokaže obvesilo, da je bila prijava na dogodek uspešna.

#### Izjemni tokovi
1. Študent ali pedagoški delavec je na dogodek že prijavljen. Sistem v tem primeru izpiše obvesilo, da prijava ni mogoča.

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot študent ali pedagoški delavec, v nasprotnem primeru mu funkcionalnost ni na voljo.

#### Posledice
Ko se študent ali pedagoški delavec prijavi na dogodek, se število prijavljenih poveča za 1 in študent ali pedagoški delavec se doda na seznam prijavljenih na ta dogodek.

#### Posebnosti
Prijava na dogodek nima posebnosti.

#### Prioriteta
Must have.

#### Sprejemni testi
1. Prijavi se kot študent ali pedagoški delavec, izberi dogodek na katerega še nisi prijavljen in potrdi izbiro.
2. Prijavi se kot študent ali pedagoški delavec, izberi dogodek na katerega si že prijavljen in potrdi izbiro.

### 11. Urejanje dogodka

#### Povzetek funkcionalnosti
Organizator dogodkov in moderator lahko uredita dogodek, pri čemer spremenita naziv, opis, kraj ali datum dogodka. Velja, da lahko organizator dogodkov ureja le dogodke, ki jih je sam dodal, medtem ko lahko moderator ureja tudi dogodke, ki so jih kreirali drugi.

#### Osnovni tok
1. Organizator dogodkov ali moderator pritisne navigacijski gumb 'Seznam dogodkov'
2. Sistem prikaže seznam dogodkov, ki jih je možno urejati.
3. Organizator dogodkov ali moderator izbere dogodek, ki ga želi popraviti.
4. Organizator dogodkov ali moderator izbere funkcionalnost *Urejanje dogodka*, ki je vezana na gumb 'Uredi dogodek'.
5. Organizator dogodkov ali moderator popravi naziv, tip, kratek opis, kraj ali čas dogodka.
6. Organizator dogodkov ali moderator potrdi svoje popravke.
7. Sistem prikaže sporočilo, da je bil dogodek uspešno posodobljen.

#### Izjemni tok
1. Organizator dogodkov ali moderator ni vnesel vseh vnosnih polj. Sistem v tem primeru prikaže opozorilo, da je potrebno vnesti vsa vnosna polja.
2. V sistemu ne obstaja noben prihajajoči dogodek, ki bi ga bilo možno urejati.

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot organizator dogodkov ali moderator, v nasprotnem primeru mu funkcionalnost ni na voljo.

#### Posledice
Parametri dogodka (naziv, tip, kratek opis, kraj in čas dogodka) imajo nove vrednosti, take, kot jih je navedel organizator dogodkov ali moderator ob urejanju.

#### Posebnosti
Urejanje dogodka nima posebnosti.

#### Prioriteta
Would have.

#### Sprejemni testi
1. V sistemu obstaja vsaj 1 ali več dogodkov. Prijavi se kot organizator dogodkov ali moderator, pritisni navigacijski gumb 'Seznam dogodkov', izberi dogodek, ki ga želiš urejati, popravi parametre dogodka na nove vrednosti in potrdi svoje popravke.
2. V sistemu obstaja vsaj 1 ali več dogodkov. Prijavi se kot organizator dogodkov ali moderator, pritisni navigacijski gumb 'Seznam dogodkov', izberi dogodek, ki ga želiš urejati, ne izpolni vseh vnosnih polj in potrdi svoje popravke.
3. V sistemu ne obstaja še noben dogodek. Prijavi se kot organizator dogodkov ali moderator, pritisni navigacijski gumb 'Seznam dogodkov'.

### 12. Označi kot opravljeno

#### Povzetek funkcionalnosti
Študent lahko naloge, ki so del njegovega TO-DO seznama označi kot opravljene.

#### Osnovni tok
1. Študent izbere funkcionalnost *Pregled TO-DO seznama*.
2. Sistem prikaže TO-DO seznam.
3. Študent izbere eno od opravil, ki še ni opravljeno.
4. Študent označi opravilo kot opravljeno.

#### Izjemni tok
1. Študent izbere opravilo, ki je že opravljeno in ga želi označiti kot opravljenega. Sistem opravilo označi kot neopravljeno.

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot študent, v nasprotnem primeru mu funkcionalnost ni na voljo.

#### Posledice
Opravilo je od sedaj naprej obravnavano kot opravljeno.

#### Posebnosti
Označi kot opravljeno nima posebnosti.

#### Prioriteta
Should have.

#### Sprejemni testi
1. Prijavi se kot študent. Na študentovem seznamu je vsaj eno neopravljeno opravilo. Na TO-DO listu izberi eno od neopravljenih opravil in ga označi kot opravljenega.
2. Prijavi se kot študent. Na študentovem seznamu ni neopravljenih opravil. Na TO-DO listu izberi eno od opravljenih opravil in ga označi kot neopravljenega.

### 13. Naročanje na kanal

#### Povzetek funkcionalnosti
Študent se lahko naroči na poljubno število kanalov, ki so jih kreirali pedagoški delavci.

#### Osnovni tok
1. Študent pritisne navigacijski gumb 'Seznam kanalov'.
1. Študent izbere enega od kanalov.
2. Študent pritisne gumb 'Naroči se na kanal'.
3. Sistem začne študenta obveščati o novih objavah na kanalu.

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot študent, v nasprotnem primeru mu funkcionalnost ni na voljo.

#### Posledice
Študent od sedaj naprej dobiva obvestila o novih objavah na kanalu.

#### Posebnosti
Naročanje na kanal nima posebnosti.

#### Prioriteta
Should have.

#### Sprejemni testi
1. Prijavi se kot študent. Pritisni navigacijski gumb 'Seznam kanalov'. Obstaja naj vsaj en kanal. Študent izbere enega od kanalov in klikne gumb 'Naroči se na kanal'.

### 14. Urejanje TO-DO seznama

#### Povzetek funkcionalnosti
Študent lahko uredi nalogo na svojem TO-DO seznamu pri čemer spremeni naziv, opis ali rok le tej.

#### Osnovni tok
1. Študent odpre svoj TO-DO seznam.
2. Študent izbere eno od opravil na seznamu.
3. Študent pritisne gumb 'Uredi podrobnosti'.
4. Sistem študentu ponudi polja za urejanje naziva, kratkega opisa in roka opravila.
5. Študent spremeni željene podatke in potrdi svojo izbiro s pritiskom na gumb 'Shrani'.
6. Sistem posodobi podatke o opravilu.

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot študent, v nasprotnem primeru mu funkcionalnost ni na voljo. Prav tako mora imeti na seznamu opravil vsaj eno opravilo.

#### Posledice
Posodobijo se podatki o urejenem opravilu.

#### Posebnosti
Urejanje TO-DO seznama nima posebnosti.

#### Prioriteta
Should have.

#### Sprejemni testi
1. Prijavi se kot študent, ki ima na svojem TO-DO seznamu vsaj eno opravilo. Izberi eno od opravil. Izberi možnost uredi in spremeni želene podatke. Potrdi svojo izbiro.

### 15. Urejanje kanala

#### Povzetek funkcionalnosti
Pedagoški delavec in moderator lahko uredita kanal, pri čemer spremenita ali opis ali naziv kanala. Moderator lahko uredi katerikoli kanal, pedagoški delavec pa le tiste, ki jih je kreiral sam.

#### Osnovni tok za pedagoškega delavca
1. Pedagoški delavec pritisne navigacijski gumb 'Seznam kanalov'
2. Pedagoški delavec izbere  enega od svojih kanalov in pritisne gumb 'Uredi kanal'.
3. Sistem ponudi vnosna polja za spremembo naziva in opisa kanala.
4. S pritiskom na gumb 'Shrani' uveljavi spremembe.

#### Osnovni tok za moderatorja
1. Moderator pritisne navigacijski gumb 'Seznam kanalov'
2. Moderator izbere enega od možnih kanalov in pritisne gumb 'Uredi kanal'.
3. Sistem ponudi vnosna polja za spremembo naziva in opisa kanala.
4. S pritiskom na gumb 'Shrani' uveljavi spremembe.


#### Pogoji
Uporabnik mora biti v sistem prijavljen kot pedagoški delavec ali moderator. v nasprotnem primeru mu funkcionalnost ni na voljo. Pedagoški delavec mora imeti vsaj en lasten kanal.

#### Posledice
Posodobijo se podatki o kanalu.

#### Posebnosti
Urejanje kanala nima posebnosti.

#### Prioriteta
Should have.

#### Sprejemni testi
1. Prijavi se kot pedagoški delavec. Pritisni navigacijski gumb 'Seznam kanalov'. Imej vsaj en lasten kanal. Izberi enega od kanalov in klikni gumb 'Uredi kanal'. Spremeni želene podatke in potrdi izbiro.

### 16. Brisanje dogodka

#### Povzetek funkcionalnosti
Organizator dogodkov in moderator lahko izbrišeta dogodke. Pri tem se vsem prijavljenim na dogodek le ta odstrani iz seznama prijavljenih dogodkov. Velja, da lahko organizator dogodkov izbriše le dogodke, ki jih je kreiral sam, medtem ko moderator lahko izbriše katerikoli dogodek.

#### Osnovni tok za organizatorja dogodkov
1. Organizator dogodkov pritisne navigacijski gumb 'Seznam dogodkov'.
2. Organizator dogodkov izbere enega od svojih dogodkov na seznamu.
3. Organizator dogodkov pritisne gumb 'Izbriši dogodek'.
4. Sistem ga vpraša če zares želi izbrisati dogodek.
5. Organizator dogodkov potrdi svojo akcijo.
6. Sistem izbriše dogodek in iz njega odjavi vse prijavljene študente in pedagoške delavce.

#### Osnovni tok za moderatorja
1. Moderator pritisne navigacijski gumb 'Seznam dogodkov'.
2. Moderator izbere enega od možnih dogodkov na seznamu.
3. Moderator pritisne gumb 'Izbriši dogodek'.
4. Sistem ga vpraša če zares želi izbrisati dogodek.
5. Moderator potrdi svojo akcijo.
6. Sistem izbriše dogodek in iz njega odjavi vse prijavljene študente in pedagoške delavce.


#### Pogoji
Uporabnik mora biti v sistem prijavljen kot organizator dogodkov ali moderator. v nasprotnem primeru mu funkcionalnost ni na voljo. Organizator dogodkov mora imeti vsaj en lasten dogodek.

#### Posledice
Dogodek se izbriše. Vsi študenti in pedagoški delavci, ki so se nanj prijavili so odjavljeni. Dogodek ni več viden na seznamu dogodkov.

#### Posebnosti
Brisanje dogodka nima posebnosti.

#### Prioriteta
Must have.

#### Sprejemni testi
1. Prijavi se kot organizator dogodkov, ki je ustvaril vsaj en dogodek, ki se še ni zgodil. Pritisni navigacijski gumb 'Seznam dogodkov'. Izbereš tak dogodek in pritisneš gumb izbriši.

### 17. Pošiljanje sporočil vsem prijavljenim na kanal 

#### Povzetek funkcionalnosti
Pedagoški delavec lahko preko kanala za enega od svojih predmetov pošlje sporočilo vsem prijavljenim na kanal. Sporočilo pa mora biti vezano na sam predmet.

#### Osnovni tok
1. Pedagoški delavec pritisne navigacijski gumb 'Seznam kanalov'.
2. Pedagoški delavec izbere enega od svojih kanalov in pritisne gumb 'Pošlji novo sporočilo'.
3. Pedagoški delavec izpolni obrazec in potrdi pošiljanje sporočila.

#### Izjemni tokovi
1. Pedagoški delavec ni izpolnil vseh polj obrazca (izbira kanala in sporočilo). Sistem v tem primeru prikaže opozorilo, da morajo biti izpolnjena vsa polja.

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot pedagoški delavec. v nasprotnem primeru mu funkcionalnost ni na voljo.

#### Posledice
Vsi študenti, ki so prijavljeni na izbrani kanal, prejmejo sporočilo.

#### Posebnosti
Pošiljanje sporočil vsem prijavljenim na kanal nima posebnosti.

#### Prioriteta
Should have.

#### Sprejemni testi
1. Prijavi se kot pedagoški delavec. Pritisni navigacijski gumb 'Seznam kanalov'. Izberi kanal in vnesi sporočilo. Potrdi pošiljanje sporočila.
2. Prijavi se kot pedagoški delavec. Pritisni navigacijski gumb 'Seznam kanalov'. Izberi kanal za vnos sporočila vendar ne izpolni vseh polj v obrazcu.

### 18. Brisanje iz TO-DO seznama 

#### Povzetek funkcionalnosti
Študent lahko iz svojega TO-DO seznama izbriše nalogo. Ta ob naslednjem prikazu seznama ne bo več vidna.

#### Osnovni tok
1. Študent izbere funkcionalnost *Pregled TO-DO seznama*.
2. Sistem prikaže TO-DO seznam
3. Študent izbere eno od opravil na seznamu.
4. Študent pritisne gumb 'Izbriši opravilo'.
5. Sistem ga vpraša če zares želi nadaljevati z izbrisom.
6. Študent potrdi brisanje.

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot študent. V nasprotnem primeru mu funkcionalnost ni na voljo.

#### Posledice
Če študent izbriše opravilo iz TO-DO seznama, je le to izbrisano in se ne prikazuje več.

#### Posebnosti
Brisanje iz TO-DO seznama nima posebnosti.

#### Prioriteta
Could have.

#### Sprejemni testi
1. Prijavi se kot študent, ki ima vsaj eno opravilo na TO-DO seznamu. Izberi poljubno opravilo in ga izbriši.
2. Prijavi se kot študent, ki nima opravil na TO-DO seznamu. Izberi poljubno opravilo in ga izbriši.


### 19. Brisanje kanala

#### Povzetek funkcionalnosti
Pedagoški delavec in moderator lahko izbrišeta kanal in s tem z njega odjavita vse prijavljene študente. Moderator lahko izbriše katerikoli kanal, pedagoški delavec pa le tiste, ki jih je kreiral sam.

#### Osnovni tok za pedagoškega delavca
1. Pedagoški delavec pritisne navigacijski gumb 'Seznam kanalov'.
2. Pedagoški delavec na spustnem seznamu izbere da vidi zgolj svoje kanale.
3. Sistem izpiše vse kanale, ki jih je pedagoški delavec ustvaril. 
4. Pedagoški delavec iz seznama svojih kanalov izbere en kanal.
5. Pedagoški delavec izbere funkcionalnost *Brisanje kanala*, ki je vezana na gumb 'Izbriši kanal'.
6. Sistem ga vpraša če zares želi nadaljevati z izbrisom.
7. Pedagoški delavec potrdi brisanje.

#### Osnovni tok za moderatorja
1. Moderator pritisne navigacijski gumb 'Seznam kanalov'.
2. Moderator iz seznama izbere en kanal.
3. Moderator izbere funkcionalnost *Brisanje kanala*.
4. Sistem ga vpraša če zares želi nadaljevati z izbrisom.
5. Moderator potrdi brisanje.

#### Alternativni tok
1. Pedagoški delavec pritisne navigacijski gumb 'Seznam kanalov'.
2. Pedagoški delavec na spustnem seznamu izbere da vidi vse kanale.
3. Sistem izpiše vse kanale vseh pedagoških delavcev.
4. Pedagoški delavec iz seznama kanalov izbere svoj kanal.
5. Pedagoški delavec izbere funkcionalnost *Brisanje kanala*.
6. Sistem ga vpraša če zares želi nadaljevati z izbrisom.
7. Pedagoški delavec potrdi brisanje.

#### Izjemni tokovi
1. Pedagoški delavec nima nobenega kanala. V tem primeru sistem izpiše obvestilo, da pedagoški delavec ni ustvaril še nobenega kanala.
2. Moderator želi izbrisati kanal, vendar noben ne obstaja. Sistem v tem primeru izpiše obvestilo, da ni kanalov.
3. Pedagoški delavec želi izbrisati kanal, ki ga ni ustvaril.

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot pedagoški delavec ali moderator. V nasprotnem primeru mu funkcionalnost ni na voljo.

#### Posledice
Ko je kanal izbrisan, so iz njega samodejno odjavljeni vsi študenti, ki so bili nanj prijavljeni.

#### Posebnosti
Brisanje kanala nima posebnosti.

#### Prioriteta
Could have.

#### Sprejemni testi
1. Prijavi se v sistem kot pedagoški delavec. Pritisni gumb 'Seznam kanalov'. Izberi, da želiš videti le svoje kanale. Izberi kanal, ki ga želiš izbrisati. Izberi funkcionalnost *Brisanje kanala*. Potrdi izbiro.
2. Prijavi se v sistem kot moderator. Pritisni gumb 'Seznam kanalov'. Izberi kanal ki ga želiš izbrisati. Izberi funkcionalnost *Brisanje kanala*. Potrdi izbiro.
3. Prijavi se v sistem kot pedagoški delavec. Pritisni gumb 'Seznam kanalov'. Izberi, da želiš videti vse kanale. Izberi svoj kanal, ki ga želiš izbrisati. Izberi funkcionalnost *Brisanje kanala*. Potrdi izbiro.
4. Prijavi se v sistem kot pedagoški delavec, ki še ni ustvaril nobenega kanala. Pritisni gumb 'Seznam kanalov'. Izberi, da želiš videti le svoje kanale.
5. Prijavi se v sistem (v katerem ne obstaja noben kanal) kot moderator. Pritisni gumb 'Seznam kanalov'.
6. Prijavi se v sistem kot pedagoški delavec. Pritisni gumb 'Seznam kanalov'. 
Izberi, da želiš videti vse kanale. Izberi kanal, ki ga nisi ustvaril. Izberi funkcionalnost *Brisanje kanala*. Potrdi izbiro.

## 6. Nefunkcionalne zahteve


### 1. Dosegljivost Aplikacije  

#### Opis
Sistem mora biti na voljo najmanj 99,9 odstotkov časa.

#### Utemeljitev
Študent se zanaša, da lahko kadarkoli vstopi v sistem in pregleda svoj seznam obveznosti, če sistem tega ne omogoča velja za nezanesljivega.


### 2. Hitrost nalaganja

#### Opis
Od odprtja aplikacije do trenutka, ko je popolnoma naložena mora miniti manj kot 2 sekundi časa.

#### Utemeljitev
Dlje časa ko se spletna stran nalaga, večja je verjetnost, da uporabnik zapusti spletno stran predno se ta v celoti naloži.


### 3. Enostavnost 

#### Opis
Aplikacija mora biti narejena na način, da jo lahko vsakdo brez poznavanja aplikacije zna uporabljati.


### 4. Varnost podatkov v aplikaciji

#### Opis
Sistem uporabnikom ne sme omogočati dostopa do podatkov, za katere niso pooblaščeni.


### 5. Velikost celotne aplikacije

#### Opis
Velikost celotne aplikacije skupaj z vsemi slikami in besedilom ne sme presegati 3 MB.


### 6. Delovanje na različnih napravah

#### Opis
Aplikacija mora biti funkcionalna in imeti prilagojen grafični uporabniški vmesnik za različne naprave, kot so računalnik, tablica in telefon. 


### 7. Etične zahteve o podatkih

#### Opis
Osebnih podatkov, ki jih uporabnik vnaša v aplikacijo se ne sme zlorabiti na način, da jih uporabimo za kaj drugega kot zgolj za uporabo aplikacije.


### 8. Delovanje na spletnem strežniku

#### Opis
Aplikacija mora biti sposobna teči na Apache HTTP spletnem strežniku.


### 9. Prijava v aplikacijo

#### Opis
Uporabnik se mora identificirati s prijavo v sistem in sicer z uporabniškim imenom in geslom, ki ga nastavi pri registraciji.


### 10. Strinjanje s pogoji uporabe

#### Opis
Uporabnik se mora ob registraciji v sistemu strinjati s pogoji uprorabe aplikacije.

## 7. Prototipi vmesnikov

### Zaslonske maske

#### 1. Ogled urnika:

![Mask image](../img/mask_1.png)

#### 2. Dodajanje terminov predavanj in vaj:

![Mask image](../img/mask_2.png)
![Mask image](../img/mask_3.png)

#### 3. Brisanje terminov predavanj in vaj:

![Mask image](../img/mask_4.png)
![Mask image](../img/mask_5.png)

#### 4. Pregled TO-DO seznama:

![Mask image](../img/mask_6.png)

#### 5. Dodajanje opravil na TO-DO seznam:

![Mask image](../img/mask_6.png)
![Mask image](../img/mask_7.png)

#### 6. Dodajanje dogodka:

![Mask image](../img/mask_14.png)
![Mask image](../img/mask_15.png)

#### 7. Pregledovanje potrjenih dogodkov:

![Mask image](../img/mask_11.png)

#### 8. Pregledovanje vseh prijavljenih na dogodek:

![Mask image](../img/mask_18.png)

#### 9. Ustvarjanje kanala za predmet:

![Mask image](../img/mask_21.png)
![Mask image](../img/mask_24.png)

#### 10. Prijava na dogodek:

![Mask image](../img/mask_12.png)
![Mask image](../img/mask_13.png)

#### 11. Urejanje dogodka:

![Mask image](../img/mask_14.png)
![Mask image](../img/mask_16.png)

#### 12. Označi kot opravljeno:

![Mask image](../img/mask_6.png)
![Mask image](../img/mask_8.png)

#### 13. Naročanje na kanal:

![Mask image](../img/mask_20.png)
![Mask image](../img/mask_19.png)

#### 14. Urejanje TO-DO seznama:

![Mask image](../img/mask_6.png)
![Mask image](../img/mask_8.png)
![Mask image](../img/mask_9.png)

#### 15. Urejanje kanala:

![Mask image](../img/mask_21.png)
![Mask image](../img/mask_22.png)

#### 16. Brisanje dogodka:

![Mask image](../img/mask_14.png)
![Mask image](../img/mask_17.png)

#### 17. Pošiljanje sporočil vsem prijavljenim na kanal:

![Mask image](../img/mask_21.png)
![Mask image](../img/mask_25.png)

#### 18. Brisanje iz TO-DO seznama:

![Mask image](../img/mask_6.png)
![Mask image](../img/mask_8.png)
![Mask image](../img/mask_10.png)

#### 19. Brisanje kanala:

![Mask image](../img/mask_21.png)
![Mask image](../img/mask_23.png)

### Zunanji vmesniki

Naš sistem bo komuniciral z zunanjim vmesnikom Urnik FRI. Preko zunanjega vmesnika bomo pridobili termine predavanj in vaj za vsakega študenta na podlagi vpisne številke, ki jo bo vnesel. Ta funkcionalnost bo na voljo znotraj funkcionalnosti *Dodajanje terminov predavanj in vaj*, kjer bo študent vnesel svojo vpisno številko, sistem pa bo samodejno dodal termine predavanj in vaj na urnik.

Do zunanjega vmesnika bomo dostopali preko spletnega naslova https://urnik.fri.uni-lj.si/timetable/fri-2018_2019-letni-1-13/allocations_ical?student=VPISNA_STEVILKA, kjer *VPISNA_STEVILKA* predstavlja 8-mestno vpisno številko študenta. Zunanji vmesnik kot odgovor vrne datoteko, ki ima spodnjo strukturo. Prikazan je le izsek za predmet *Diplomski seminar*, ki se izvaja vsak petek od 12:00 do 14:00.

```
BEGIN:VCALENDAR  
VERSION:2.0  
PRODID:-//Urnik FRI//urnik.fri.uni-lj.si//  
BEGIN:VEVENT  
SUMMARY:DS - P  
DTSTART;TZID=Europe/Ljubljana;VALUE=DATE-TIME:20190215T120000  
DTEND;TZID=Europe/Ljubljana;VALUE=DATE-TIME:20190215T140000  
DTSTAMP;VALUE=DATE-TIME:20190407T212219Z  
UID:urnikfri-153256  
RRULE:FREQ=WEEKLY;UNTIL=20190603T235959;BYDAY=FR  
DESCRIPTION:Diplomski seminar P @ P01\nFranc Solina  
LOCATION:P01  
END:VEVENT
```

Vrstice med `BEGIN:VEVENT` in `END:VEVENT` opisujejo posamezen termin predavanj ali vaj. Terminov je lahko poljubno mnogo.

Pomembna polja, ki jih bomo potrebovali:  
1. `SUMMARY`: kratka oznaka predmeta
2. `DTSTART`: datum in ura začetka izvajanja predmeta
3. `DTEND`: datum in ura konca izvajanja predmeta
4. `RRULE`: opis periodičnosti termina (npr. vsak petek do 3.6.2019)
5. `DESCRIPTION`: daljši opis predmeta
6. `LOCATION`: predavalnica, v kateri se predmet izvaja

Naš sistem bo procesiral zgoraj opisano datoteko in vse termine predavanj in vaj vpisal v urnik študenta.