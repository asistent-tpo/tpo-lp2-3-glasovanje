# Dokument zahtev

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Alen Juršič, Samo Okorn, Matej Ajster in Gregor Štefanić |
| **Kraj in datum** | Ljubljana, 1. 4. 2019 |



## Povzetek projekta

Neurejenost, lenoba in pomankanje spanca so ponavadi trije najbolj pogosti simptomi študenta. Zato smo se v ekipi odločili (oziroma so
se drugi namesto nas), da razbremenimo študente in odstranimo vse te nadloge. To bomo storili tako, da bomo razvili spletno aplikacijo
za pomoč pri opravljanju vseh študijskih obveznosti. Spletni vmesnik bo omogočal kristalno jasen in hiter pregled nad vsemi obveznostmi 
kot so predavanja, vaje, domače naloge in izpiti, cilj pa je, da si uporabnik nabere vse študijske obveznosti na eno mesto, tam pa si
lahko določi prioritete ter tako izboljša planiranje študentskega življenja. Aplikacija bo tako lahko pomagala k urejenosti, predlagala delo
in mogoče tudi prihranila kakšno uro spanca, saj bo pripomogla k sprotnem delu, ki je ključno za uspešno študiranje.


## 1. Uvod


Veliko študentov se sooča s stresom in problemom težke glave, saj si morajo zapomniti veliko rokov za oddajo in izpitnih rokov, datume kolokvijev in vseh ostalih sprotnih obveznosti, ki spadajo k uspešnemu opravljanju študija. Problemska domena je neurejen in kampanjski odnos do študija, namesto da bi si delo razdelili na posamezne dneve si vse prihranimo na zadnje par dni. To nastalo zmedo lahko rešimo le z urejanjem podatkov, sprotnim delom in doslednostjo.

Prav to je poglavitni razlog, da smo se odločili za aplikacijo StraightAs. Namenjena bo predvsem študentom in profesorjem na Fakulteti za računalništvo in informatiko. 

Aplikacija bo uporabnikom omogočala jasen pregled nad vsemi obveznostmi kot so predavanja, vaje, domače naloge in izpiti, torej vse pomembne zadeve zbrane na enem mestu. Celoten koncept aplikacije je zelo preprost, ustvariti portal za izmenjavo informacij o študiju na preprost in hiter način. Aplikacija bo omogočala registracijo novih uporabnikov, ki bodo bodisi študentje bodisi profesorji. Študentje si beležijo sprotne aktivnosti, ki so s pomočjo koledarja bolj pregledne in lahko lažje organizirajo svoje delo. Profesorji, ki jih bo za aktivacijo računa moral potrditi skrbnik sistema, pa lahko na platformi celo ustanovijo svoje predmete in beležijo aktivnosti za te predmete. Študentje pa lahko nato sledijo predmetom in se aktivnosti za ta predmet vidijo tudi na njihovih koledarjih in pregledih dneva, kjer so zbrane vse aktivnosti za določen dan.

Obstoječi uporabniki se lahko prijavijo v sistem z uporabniškim imenom in geslom. Prijavljeni uporabniki lahko pregledujejo aktivnosti v koledarju. Aplikacija omogoča dodajanje novih aktivnosti, katero lahko kasneje ureja ali izbriše le avtor aktivnosti. Na koledarju študentov so prikazane javne aktivnosti drugih uporabnikov in predmetov, ki jim študent sledi. Uporabniki v vlogi profesorja imajo moč ustvariti predmet, na katerega se lahko uporabniki v vlogi študenta prijavijo. Študenti prijavljeni na predmet dobijo vsa obvestila vezana na ta predmet. Le profesor ki je ustvaril predmet, ga lahko tudi spreminja ali izbriše. Vsak predmet, ki mu študent sledi, je dodan na seznam predmetov, kjer študent upravlja z naročninami na predmete. Študent lahko posamezno aktivnost označi za opravljeno, kar pomaga pri njegovi organizaciji. Za kasnejšno analizi ima moč tudi beležiti porabljen čas za posamezno aktivnost. 

Ker imajo profesorji drugačno moč nad ustvarjanjem predmetov, je potreben mehanizem za validacijo uporabnikov profesorjev. Za to skrbi uporabnik skrbnik, ki ima moč potrditi ali zavrniti prošnjo za registracijo profesorja. 

Zadali smo si tudi druge (nefunkcionalne) zadeve kot so, razpoložljivost aplikacije v vsakem trenutku, čas nedelovanja kdarkoli v dnevu ne sme trajati več kot 10 sekund. Pomembna pa je tudi odzivnost, ki mora biti vsaj 200 ms, ker bo naša aplikacija nedvomno tudi hit pa mora postreči vsaj 10 000 uporabnikov. Držali pa se bomo tudi trenda podobnih aplkacij, da bo aplikacija prenosljiva v oblačne storitve, ter da bo izdelana z MEAN arhitekturo. Uporabniki se prijavljajo v sistem z e-mailom, ki jim ga je dodelila fakulteta .

Naša aplikacija bo vsebovala dve interakciji z zunanjim sistemom Urnik FRI in za obe bomo potrebovali isti vmesnik, saj sta ustvarjanje in urejanje predmeta sorodni funkcionalnosti. Sistem Urnik FRI bi v našem primeru moral ponujati funkcijo, ki ne sprejme nobenih argumentov, vrne pa seznam vseh predmetov v obliki JSON objekta. Vsak predmet pa je opisan s nazivom predmeta, terminom predavanj dnevom začetekom trajanjem številko predavalnice izvajalecem.
 


## 2. Uporabniške vloge

**gost** (poleg prijave in registracije ima omogočen tudi pregled nad aktivnostmi ter predmeti)

**študent** (lahko se odjavi iz aplikacije, dodaja, ureja in pregleduje aktivnosti, pregleduje predmete ter jim tudi sledi, lahko pa si tudi beleži porabljeni čas)

**profesor** (lahko se odjavi iz aplikacije, dodaja, ureja in pregleduje aktivnosti ter ustvarja in ureja predmete)

**skrbnik** (lahko se odjavi iz aplikacije ter potrjuje registracijo profesorjev)


## 3. Slovar pojmov


*Uporabnik* - Osebe, ki uporabljajo aplikacijo v načinu gost/študent/profesor.

*Aktivnost* - Entiteta v aplikaciji, ki lahko predstavlja katerokoli aktivnost študenta (npr. izpit, kolokvij, predavanje, vaje, domača naloga ipd.). Aktivnost je določena z nazivom, opisom, datumom in uro, predmetom in s prioriteto.

*Prijavljen uporabnik* - Osebe, ki uporabljajo aplikacijo v načinu študent/profesor.

*Predmet* - Entiteta v aplikaciji, ki predstavlja učno področje. Predmetu lahko študentje "sledijo", kar pomeni, da vidijo aktivnosti vezane na ta predmet.

*Registracijski obrazec* - sestavljen iz polj, ki zahtevajo uporabniško ime, geslo, potrditev gesla, e-mail. Za uspešno registracijo mora biti vsako polje mora biti izpolnjeno, pri čemer je zahtevano unikatno uporabniško ime, e-mail ter geslom ki mora presegati osem alfanumeričnih znakov.

*Prijavni obrazec* - sestavljen iz dveh polj, ki zahtevata uporabniško ime in geslo. Za uspešno prijavo mora uporabnik vnesti ustrezno geslo in e-mail.

*Obrazec za dodajanje aktivnosti* - sestavljen je iz polj naziv, opis, datum in ura, predmet in prioriteta. Za uspešno dodajanje aktivnosti morata biti izpolnjena vsaj datum in naziv aktivnosti.

*Obrazec za vnos podatkov o novem predmetu* - sestavljen je iz polj naziv predmeta in opis predmeta. Obrazec je ustrezno izpolnjen, če je izpolnjeno vsaj ime predmeta.

## 4. Diagram primerov uporabe

[UML diagram primerov uporabe](https://wireframe.cc/5uKxzH)
![UML](../img/UML.png)

## 5. Funkcionalne zahteve


### 5.1 Registracija

#### Povzetek funkcionalnosti

Gost se lahko registrira v sistem kot študent ali profesor, tako da izpolni registracijski obrazec.

#### Osnovni tok

- Gost:
    1. Gost vnese vse potrebne podatke.
    2. Gost pritisne gumb za registracijo.
    3. Gost prejme ustrezno sporočilo in je ustrezno preusmerjen na začetno stran.

#### Alternativni tok(ovi)

Brez.


#### Izjemni tokovi

1. Gost napačno izpolni registracijski obrazec (glej slovar pojmov). Sistem ga ustrezno obvesti katera polja so napačno izpolnjena.


#### Pogoji

Uporabnik mora ustrezno izpolniti registracijski obrazec z email naslovom fakultete in zbrati unikatno uporabniško ime.

#### Posledice

Če se študent ali profesor uspešno registrira v aplikacijo, ga aplikacija preusmeri na začetno stran prijavljenega kot študenta oziroma profesorja. 

#### Prioriteta

Must have

#### Sprejemni testi

- Registriraj se v aplikacijo, pri čemer vnesi uporabniško ime, ki je že zasedeno.
- Registriraj se v aplikacijo, pri čemer email ne spada pod domeno fakultete.
- Registriraj se v aplikacijo, pri čemer je potrditveno geslo različno od prvotnega.
- Registriraj se v aplikacijo z unikatnim uporabniškim imenom in pravilno izpolni registracijski obrazec.
___


### 5.2 Prijava v aplikacijo

#### Povzetek funkcionalnosti

Gost se lahko prijavi v aplikacijo kot študent, profesor ali administrator, pri čemer mora predhodno vnesti pravilno uporabniško ime in geslo. Aplikacija ga preusmeri na glavno stran aplikacije.

#### Osnovni tok

- Gost:
    1.  Gost vnese uporabniško ime in geslo v podani polji.
    2.  Gost pritisne gumb za prijavo.
    3.  Aplikacija ga ob pravinem uporabniškem imenu in geslu preusmeri na glavno stran.

#### Alternativni tok(ovi)

Brez.

#### Izjemni tokovi

1. Gost napačno izpolni prijavni obrazec (glej slovar pojmov). Sistem ga ustrezno obvesti, da prijava ni uspela.

#### Pogoji

Gost mora imeti ustvarjen račun za uporabo te aplikacije.

#### Posledice

Če se študent, profesor ali skrbnik uspešno prijavi v aplikacijo, ga aplikacija preusmeri na glavno stran, kjer lahko uporablja funkcionalnosti, ki so na voljo za njegovo vlogo in pred prijavo niso bile mogoče.

#### Prioriteta

Must have.

#### Sprejemni testi

- Prijavi se v aplikacijo, pri čemer vnesi napačno uporabniško ime.
- Prijavi se v aplikacijo, pri čemer vnesi napačno geslo.
- Prijavi se v aplikacijo s pravilnim uporabniškim imenom in geslom.
___


### 5.3 Odjava iz aplikacije

#### Povzetek funkcionalnosti

Vsi prijavljeni uporabniki se lahko ob predpostavki, da so v aplikacijo prijavljeni, iz nje tudi odjavijo.

#### Osnovni tok

- Študent:
    1.  Študent pritisne gumb za odjavo iz aplikacije.
    2.  Aplikacija ga odjavi in preusmeri na prijavno stran.

- Profesor:
    1.  Profesor pritisne gumb za odjavo iz aplikacije.
    2.  Aplikacija ga odjavi in preusmeri na prijavno stran.

- Skrbnik:
    1.  Skrbnik pritisne gumb za odjavo iz aplikacije.
    2.  Aplikacija ga odjavi in preusmeri na prijavno stran.
    
#### Alternativni tok(ovi)

Brez.

#### Izjemni tokovi

Brez.

#### Pogoji

Uporabnik mora biti prijavljen v aplikacijo.

#### Posledice

Aplikacija uporabnika preusmeri na prijavno stran in mu onemogoči funkcionalnosti, ki jih ima, če je v aplikacijo prijavljen.

#### Prioriteta

Must have.

#### Sprejemni testi

- Odjavi se iz aplikacije.

___

### 5.4 Pregled aktivnosti v koledarju

#### Povzetek funkcionalnosti

Študent, gost ali profesor lahko s klikom na koledar odpre pogled koledarja. Na koledarju (Gregorjanskega tipa) je poleg številke, ki predstavlja zaporedni dan v mesecu, zapisano število aktivnosti v tem dnevu. Gostu se s klikom na koledar odpre koledar, na katerem so le javne aktivnosti.

#### Osnovni tok

- Gost:
    1.  Gost pritisne gumb za pogled koledarja.
    2.  Aplikacija ga preusmeri na stran s koledarjem z omejenim dostopom.

- Študent:
    1.  Študent pritisne gumb za pogled koledarja.
    2.  Aplikacija ga preusmeri na stran s koledarjem.

- Profesor:
    1.  Profesor pritisne gumb za pogled koledarja.
    2.  Aplikacija ga preusmeri na stran s koledarjem.

#### Alternativni tok(ovi)

Brez.

#### Izjemni tokovi

Brez.

#### Pogoji

Uporabnik mora biti prijavljen v aplikacijo.

#### Posledice

Aplikacija uporabnika preusmeri na stran s koledarjem.

#### Prioriteta

Must have.

#### Sprejemni testi

- Pritisni gumb za pogled koledarja.
- Izberi mesec na koledarju.

___


### 5.5 Pregled dnevnih aktivnosti

#### Povzetek funkcionalnosti

Uporabnik lahko ob kliku na določen dan v koledarju pogledata vse aktivnosti, ki pipadajo tistemu dnevu.

Študentu ali profesorju se ob kliku na zaporedno številko dneva prikaže seznam vseh aktivnosti njegovih naročenih predmetov za določen dan.

#### Osnovni tok


- Gost:
    1.  Gost izbere dan in ga pritisne.
    2.  Aplikacija mu ponudi pregled vseh javnih aktivnosti v izbranem dnevu.

- Študent:
    1.  Študent izbere dan in ga pritisne
    2.  Aplikacija mu ponudi pregled vseh aktivnosti v izbranem dnevu.

- Profesor:
    1.  Profesor izbere dan in ga pritisne
    2.  Aplikacija mu ponudi pregled vseh aktivnosti v izbranem dnevu.

#### Alternativni tok(ovi)

Brez.

#### Izjemni tokovi

Brez.

#### Pogoji

Študent in profesor morata biti prijavljena v aplikacijo, sicer dobita enak pogled kot gost. 

#### Posledice

Uporabniku se prikažejo vse zabeležene aktivnosti v izbranem dnevu.

#### Prioriteta

Must have.

#### Sprejemni testi

- Izberi dan, ki vsebuje vsaj eno aktivnost.
- Izberi dan, ki ne vsebuje nobene aktivnosti.
___


### 5.6 Dodajanje aktivnosti

#### Povzetek funkcionalnosti

Prijavljen uporabnik lahko doda aktivnost.

#### Osnovni tok

- Študent:
    1.  Študent izbere na glavni strani dodaj aktivnost.
    2.  Aplikacija mu ponudi obrazec za dodajanje aktivnosti.
    3.  Študent ustrezno izpolni obrazec in doda aktivnost s klikom na ustrezni gumb.

- Profesor:
    1.  Profesor izbere na glavni strani dodaj aktivnost.
    2.  Aplikacija mu ponudi obrazec za dodajanje aktivnosti.
    3.  Profesor ustrezno izpolni obrazec in doda aktivnost s klikom na ustrezni gumb.

#### Alternativni tok(ovi)

Brez.

#### Izjemni tokovi

1. Študent ali profesor napačno izpolnilni prijavni obrazec (glej slovar pojmov). Sistem ga ustrezno obvesti, kaj je izpolnil narobe.
2. Študent ali profesor poizkuša dodati aktivnost, ki ima ime in datum enako že dodani aktivnosti. Sistem ga opozori, da aktivnost s takim imenom že obstaja.
3. Študent ali profesor hoče zapustiti obrazec in noče ustvariti aktivnosti. Pritisne ustrezen gumb za razveljavo.

#### Pogoji

Uporabnik mora biti prijavljen v aplikacijo.

#### Posledice

Aktivnost je dodana na koledar. Vidna je vsem, ki sledijo predmetu, za katerega je bila aktivnost dodana. Če je aktivnost ustvaril študent, je ta aktivnost vidna samo njemu.

#### Prioriteta

Must have.

#### Sprejemni testi

- Ustvari novo aktivnost v vlogi profesorja in preveri, da je vidna na koledarju.
- Ustvari novo aktivnost v vlogi profesorja in preveri, da je vidna na koledarju uporabnikov, ki sledijo temu predmetu.
- Ustvari novo aktivnost v vlogi profesorja in preveri, da ni vidna na koledarju uporabnikov, ki ne sledijo temu predmetu.
- Ustvari novo aktivnost v vlogi študenta in preveri, da ni vidna na koledarju ostalih uporabnikov.
- Ustvari aktivnost z istim imenom na isti dan.
___


### 5.7 Odstranjevanje aktivnosti

#### Povzetek funkcionalnosti

Prijavljen uporabnik lahko odstrani aktivnost.

#### Osnovni tok

- Študent:
    1.  Študent izbere aktivnost s pomočjo koledarja ali pri pregledu dnevnih aktivnosti.
    2.  Aplikacija mu ponudi podrobnosti aktivnosti.
    3.  Študent izbere možnost za odstranitev aktivnosti, ki mu je vidna le če jo je ustvaril.

- Profesor:
    1.  Profesor izbere aktivnost s pomočjo koledarja ali pri pregledu dnevnih aktivnosti.
    2.  Aplikacija mu ponudi podrobnosti aktivnosti.
    3.  Profesor izbere možnost za odstranitev aktivnosti, ki mu je vidna le če jo je ustvaril.

#### Alternativni tok(ovi)

Brez.

#### Izjemni tokovi

Ta funkcionalnost nima izjemnih tokov.

#### Pogoji

Uporabnik mora biti predhodno prijavljen v aplikacijo.
Prijavljen uporabnik je moral ustvariti aktivnost, če jo želi izbrisati.

#### Posledice

Prijavljenem uporabniku se odstrani aktivnost in ni več vidna na njegovem koledarju in na koledarju sledilcev predmeta, ki mu ta aktivnost pripada.

#### Prioriteta

Must have.

#### Sprejemni testi

- Odstrani aktivnost in preveri, da je vidna na koledarju.
___


### 5.8 Urejanje aktivnosti

#### Povzetek funkcionalnosti

Prijavljen uporabnik lahko uredi aktivnost.

#### Osnovni tok

- Študent:
    1.  Študent izbere aktivnost s pomočjo koledarja ali pri pregledu dnevnih aktivnosti.
    2.  Aplikacija mu ponudi podrobnosti aktivnosti.
    3.  Študent izbere možnost za ureditev aktivnosti, ki mu je vidna.
    4.  Ponudi se mu izpolnjen obrazec kot za dodajanje nove aktivnosti (glej slovar).

- Profesor:
    1.  Profesor izbere aktivnost s pomočjo koledarja ali pri pregledu dnevnih aktivnosti.
    2.  Aplikacija mu ponudi podrobnosti aktivnosti.
    3.  Profesor izbere možnost za ureditev aktivnosti, ki mu je vidna le če jo je ustvaril.
    4.  Ponudi se mu izpolnjen obrazec kot za dodajanje nove aktivnosti (glej slovar).
    
#### Alternativni tok(ovi)

Brez.

#### Izjemni tokovi

1. Študent ali profesor poizkuša urediti aktivnost, ki ima ime in datum enako že dodani aktivnosti. Sistem ga opozori, da aktivnost s takim imenom že obstaja.

#### Pogoji

Uporabnik mora biti predhodno prijavljen v aplikacijo.
Prijavljen uporabnik je moral ustvariti aktivnost če jo želi urediti.

#### Posledice

Prijavljenem uporabniku se uredi aktivnost in spremembe so vidne na njegovem koledarju in na koledarju njegovih sledilcev.

#### Prioriteta

Must have.

#### Sprejemni testi

- Uredi aktivnost in preveri, če se je ureditev aktivnosti upoštevala na koledarju in v podrobnostih aktivnosti.
___


### 5.9 Ustvarjanje predmeta

#### Povzetek funkcionalnosti

Profesor lahko ustvari nov predmet.

#### Osnovni tok

- Profesor:
    1. Profesor pritisne gumb za pogled vseh predmetov.
    2. Aplikacija prikaže seznam vseh predmetov ter gumb za dodajanje novega predmeta.
    3. Profesor pritisne na gumb za dodajanje novega predmeta.
    4. Aplikacija prikaže obrazec za vnos podatkov o novem predmetu(glej slovar).
    5. Profesor vnese vse obvezne podatke in pritisne na gumb za potrditev ustvarjanja novega predmeta.

#### Alternativni tok(ovi)

Brez.

#### Izjemni tokovi

1. Profesor poizkuša ustvariti predmet z napačno izpolnjenim obrazcem. Sistem ga ustrezno opozori na napačno izpolnjen obrazec
2. Profesor poizkuša ustvariti predmet z nazivom, ki je enako že prej ustvarjenem predmetu. Aplikacija ga ustrezno opozori.

#### Pogoji

V sistem moraš biti prijavljen kot profesor. Naziv predmeta mora biti unikaten.


#### Posledice

V sistem se doda nov predmet, ki je viden na seznamu vseh predmetov. Na seznamu vseh predmetov, je pri profesorju, ki je ustvaril ta predmet, ta predmet drugače označen in se nahaja na vrhu seznama.

#### Posebnosti

Brez.

#### Prioritete identificiranih funkcionalnosti

Should have.

#### Sprejemni testi

- Poskušaj ustvariti nov predmet z manjkajočimi podatki.
- Ustvari nov predmet z imenom, ki je že zasedeno.
- Ustvari nov predmet z vsemi ustreznimi podatki.
___


### 5.10 Urejanje predmeta


#### Povzetek funkcionalnosti

Profesor lahko ureja podatko o predmetih, ki jih je dodal v sistem.

#### Osnovni tok

- Profesor:
    1. Profesor pritisne gumb za prikaz vseh predmetov.
    2. Aplikacija prikaže seznam vseh predmetov.
    3. Profesor s seznama vseh predmetov izbere predmet, ki ga želi urediti. Predmeti, ki jih lahko ureja. so na vrhu seznama in drugače označeni.
    4. Aplikacija prikaže informacije o predmetu in gumb za urejanje.
    5. Profesor pritisne na gumb za urejanje predmeta.
    6. Aplikacija prikaže obrazec za urejanje podatkov o predmetu in gumb za uveljavitev sprememb.
    7. Profesor popravi željene podatke in pritisne na gumb za vlejavitev sprememb o predmetu.

#### Alternativni tok(ovi)

Brez.

#### Izjemni tokovi

1. Profesor poizkuša urediti predmet z napačno izpolnjenim obrazcem. Sistem ga ustrezno opozori na napačno izpolnjen obrazec
2. Profesor poizkuša spremeniti naziv predmeta na naziv, ki je enako že prej ustvarjenem predmetu. Aplikacija ga ustrezno opozori.

#### Pogoji

Uporabnik mora biti prijavljen kot profesor. Profesor lahko ureja samo predmete, ki jih je sam ustvaril.


#### Posledice

V sistemu se posodobijo podatki o predmetu.

#### Posebnosti

Brez.

#### Prioritete identificiranih funkcionalnosti

Should have.

#### Sprejemni testi

- V sistem se prijavi kot profesor in s seznama vseh predmetov izberi predmet, ki ga ni ustvaril prijavljen profesor, in ga poskušaj urediti.
- V sistem se prijavi kot profesor in uredi predmet, ki ga je ustvaril ta profesor in preveri ali so spremembe vidne v sistemu.
- V sistem se prijavi kot profesor in uredi predmet tako, da bo vseboval neveljavne podatke.
___

### 5.11 Odstranjevanje predmeta


#### Povzetek funkcionalnosti

Profesor lahko odstrani predmet, ki ga je dodal v sistem.

#### Osnovni tok

- Profesor:
    1. Profesor pritisne gumb za prikaz vseh predmetov.
    2. Aplikacija prikaže seznam vseh predmetov.
    3. Profesor s seznama vseh predmetov izbere predmet, ki ga želi odstraniti. Predmeti, ki jih lahko ureja. so na vrhu seznama in drugače označeni.
    4. Aplikacija prikaže informacije o predmetu in gumb za urejanje.
    5. Profesor pritisne na gumb za urejanje predmeta.
    6. Aplikacija prikaže obrazec za urejanje podatkov o predmetu in gumb za odstranitev predmeta.
    7. Profesor pritisne gumb in po ponovni potrditvi je predmet izbrisan.

#### Alternativni tok(ovi)

Brez.

#### Izjemni tokovi

Brez.

#### Pogoji

Uporabnik mora biti prijavljen kot profesor. Profesor lahko odstrani samo predmete, ki jih je sam ustvaril.


#### Posledice

V sistemu se posodobijo podatki o predmetu.

#### Posebnosti

Brez.

#### Prioritete identificiranih funkcionalnosti

Should have.

#### Sprejemni testi

- V sistem se prijavi kot profesor in s seznama vseh predmetov izberi predmet, ki ga ni ustvaril prijavljen profesor, in ga poskušaj odstraniti.
- V sistem se prijavi kot profesor in odstrani predmet, ki ga je ustvaril ta profesor in preveri ali so spremembe vidne v sistemu.

### 5.12 Pogled vseh predmetov

#### Povzetek funkcionalnosti

Uporabnik lahko zahteva pogled vseh predmetov.

#### Osnovni tok

- Gost:
    1. Gost izbere funkcionalnost *pogled vseh predmetov*.
    2. Aplikacija prikaže seznam vseh predmetov v abecednem vrstnem redu.

- Študent:
    1. Študent izbere funkcionalnost *pogled vseh predmetov*.
    2. Aplikacija prikaže seznam vseh predmetov v abecednem vrstnem redu, pri čemer so na vrhu seznama predmeti, katerim že sledi.

- Profesor:
    1. Profesor izbere funkcionalnost *pogled vseh predmetov*.
    2. Aplikacija prikaže seznam vseh predmetov v abecednem vrstnem redu, pri čemer so na vrhu seznama predmeti, ki jih je ustvaril.

#### Alternativni tok(ovi)

Brez.

#### Izjemni tokovi

Brez.

#### Pogoji

Študent in profesor morata biti prijavljena v aplikacijo.

#### Posledice

Uporabniku se prikaže seznam vseh predmetov. Študentu in profesorju se uredijo drugače na podlagi profila.

#### Posebnosti

Brez.

#### Prioritete identificiranih funkcionalnosti

Should have.

#### Sprejemni testi

- V sistem se prijavi kot študent, izberi funkcionalnost *pogled vseh predmetov* in preveri da so na seznamu vsi predmeti.
- V sistem se prijavi kot profesor, izberi funkcionalnost *pogled vseh predmetov*, pritisni gumb za ustvarjanje novega predmeta, izpolni ustrezne podatke o predmetu, potrdi ustvarjanje novega predmeta in preveri, ali je nov predmet na pri vrhu seznama vseh predmetov.
- V sistem se prijavi kot profesor, izberi funkcionalnost *pogled vseh predmetov*, izberi predmet pri vrhu seznama vseh predmetov, pritisni na gumb za izbris izbranega predmeta ter preveri, da izbrisanega predmeta ni več pri vrhu seznama vseh predmetov.
___


### 5.13 Sledenje predmetu

#### Povzetek funkcionalnosti

Študent lahko sledi predmetu. Med njegovim aktivnostim se dodajo aktivnosti povezane z izbranim predmetom.

#### Osnovni tok

- Študent:
    1. Študent izbere funkcionalnost *pogled vseh predmetov*.
    2. Aplikacija prikaže seznam vseh predmetov v abecednem vrstnem redu, pri čemer so na vrhu seznama predmeti, katerim že sledi.
    3. Študent pritisne na gumb, ki je poleg naziva predmeta, za sledenje predmeta.

#### Alternativni tok(ovi)

**Alternativni tok 1**
- Študent:
    1. Študent izbere funkcionalnost *pogled vseh predmetov*.
    2. Aplikacija prikaže seznam vseh predmetov v abecednem vrstnem redu, pri čemer so na vrhu seznama predmeti, katerim že sledi.
    3. Študent izbere predmet s seznama vseh predmetov.
    4. Aplikacija prikaže informacije o predmetu in gumb za sledenje temu predmetu.
    5. Študent pritisne na gumb za sledenje predmetu.

#### Izjemni tokovi

1. Študent hoče slediti predmetu, kateremu že sledi. Sistem ga opozori, da predmetu že sledi.

#### Pogoji

Uporabnik mora biti prijavljen v aplikacijo kot študent. Če študent že sledi nekemu predmetu, ne more še enkrat izbrati funkcionalnosti za sledenje temu istemu predmetu.

#### Posledice

Študent sedaj sledi aktivnostim izbranega predmeta. Na seznamu vseh predmetov je sedaj poleg naziva predmeta, ki mu slediš, gumb za preklic sledenja predmetu. Izbrani predmet se nahaja pri vrhu seznama vseh predmetov. Ko profesor, ki je ustvaril izbrani predmet, ustvari novo aktivnost, ki pripada temu izbranemu predmetu, se na koledarju od tega študenta prikaže tudi ta na novo dodana aktivnost.

#### Posebnosti

Brez.

#### Prioritete identificiranih funkcionalnosti

Should have.

#### Sprejemni testi

- V sistem se prijavi kot študent, izberi funkcionalnost *pogled vseh predmetov*, pritisni na gumb za sledenje predmeta in preveri, ali je izbran predmet na pri vrhu seznama vseh predmetov. Preveri, da so med aktivnosti bile dodane aktivnosti povezane s tem predmetom.
___


### 5.14 Preklic sledenja predmetu

#### Povzetek funkcionalnosti

Študent lahko prekliče sledenje predmetu. Aktivnosti, ki so povezane s tem predmetom in jih je ustvaril profesor, ki je ustvaril ta predmet, niso več na koledarju tega študenta.

#### Osnovni tok

- Študent:
    1. Študent izbere funkcionalnost *pogled vseh predmetov*
    2. Aplikacija prikaže seznam vseh predmetov v abecednem vrstnem redu, pri čemer so na vrhu seznama predmeti, katerim že sledi.
    3. Študent pritisne na gumb, ki je poleg naziva predmeta, za preklic sledenja predmetu

#### Alternativni tok(ovi)

**Alternativni tok 1**
- Študent:
    1. Študent izbere funkcionalnost *pogled vseh predmetov*.
    2. Aplikacija prikaže seznam vseh predmetov v abecednem vrstnem redu, pri čemer so na vrhu seznama predmeti, katerim že sledi.
    3. Študent izbere predmet s seznama vseh predmetov, ki mu že sledi.
    4. Aplikacija prikaže informacije o predmetu in gumb za preklic sledenja temu predmetu.
    5. Študent pritisne na gumb za preklic sledenja predmetu.
    
#### Izjemni tokovi

1. Študent hoče preklicati sledenje predmetu kateremu ne sledi. Sistem ga ustrezno opozori.

#### Pogoji

Uporabnik mora biti prijavljen v aplikacijo kot študent. Če študent ne sledi predmetu, ne more enkrat izbrati funkcionalnosti za preklic sledenja tega predmeta. 

#### Posledice

Študent sedaj ne sledi aktivnostim izbranega predmeta. Na seznamu vseh predmetov je sedaj poleg naziva predmeta, ki mu slediš, gumb za sledenje predmetu. Izbrani predmet se več ne nahaja pri vrhu seznama vseh predmetov. Aktivnosti, ki jih je ustvaril profesor, ki je ustvaril ta apredmet, in pripadjo temu predmetu, niso več prisotne na koledarju tega študenta.

#### Posebnosti

Brez.

#### Prioritete identificiranih funkcionalnosti

Should have.

#### Sprejemni testi

- V sistem se prijavi kot študent, izberi funkcionalnost *pogled vseh predmetov*, prekliči sledenje na predmet in preveri, da izbranega predmeta ni več pri vrhu seznama vseh predmetov. Preveri, da na koledarju med aktivnostmi ni več tistih aktivnosti, ki so povezane s tm predmetom.
___


### 5.15 Spreminjanje statusa aktivnosti

#### Povzetek funkcionalnosti

Študent lahko označi aktivnost kot *opravljeno* ali *neopravljeno*.

#### Osnovni tok

- Študent:
    1. Študent izbere funkcionalnost *pogled koledarja*.
    2. Aplikacija prikaže koledar.
    3. Študent pritisne na dan na koledarju.
    4. Aplikacija prikaže dnevni pregled aktivnosti.
    5. Študent pritisne na aktivnost na dnevnem pregledu aktivnosti.
    6. Aplikacija prikaže podatke o aktivnosti in gumb za spreminjanje statusa aktivnosti.
    7. Študent pritisne na gumb za spreminjanje statusa aktivnosti.

#### Alternativni tok(ovi)

**Alternativni tok 1**
- Študent:
    1. Študent izbere funkcionalnost *pogled koledarja*.
    2. Aplikacija prikaže koledar.
    3. Študent pritisne na dan na koledarju.
    4. Aplikacija prikaže dnevni pregled aktivnosti.
    5. Študent pritisne na gumb za spreminjanje statusa aktivnosti ob nazivu aktivnosti.

#### Izjemni tokovi

Brez.

#### Pogoji

Uporabnik mora biti prijavljen v aplikacijo kot študent. 

#### Posledice

Aktivnosti se spremeni status. Če je aktivnost označena kot *opravljena*, se na koledarju ob številki dneva indikator števila aktivnosti zmanjša za ena. Na dnevnem predgledu aktivnosti je *opravljena* aktivnost prečtrana, in na dnu seznama dnevnih aktivnosti. 

#### Posebnosti

Brez.

#### Prioritete identificiranih funkcionalnosti

Could have.

#### Sprejemni testi

- Prijavi se kot študent, izberi funkcionalnost za pogled koledarja, izberi funkcionalnost za dnevni pogled aktivnosti, pritisni na aktivnost na seznamu in pritisni na gumb za spremembo statusa aktivnosti.
___

### 5.16 Beleženje porabljenega časa


#### Povzetek funkcionalnosti

Študent lahko beleži, koliko časa je porabil za posamezno aktivnost.

#### Osnovni tok

- Študent:
    1. Študent izbere funkcionalnost *pogled koledarja*.
    2. Aplikacija prikaže koledar.
    3. Študent pritisne na dan na koledarju.
    4. Aplikacija prikaže dnevni pregled aktivnosti.
    5. Študent pritisne na aktivnost na dnevnem pregledu aktivnosti.
    6. Aplikacija prikaže podatke o aktivnosti in gumb za začetek beleženja porabljenega časa.
    7. Študent pritisne na gumb za začetek beleženja porabljenega časa.
    8. Aplikacija prikaže gumb za ustavitev beleženja časa aktivnosti.
    9. Študent pritisne na gumb za ustavitev beleženja časa aktivnosti.

#### Alternativni tok(ovi)

Brez.

#### Izjemni tokovi

1. Študent poizkuša ponovno beležiti čas aktivnosti, čeprav čas že beleži. Sistem ga ustrezno opomni, da že beleži čas za to aktivnost.

#### Pogoji

Uporabnik mora biti prijavljen v aplikacijo kot študent. Status aktivnosti, za katero želimo beležiti porabljen čas, mora biti *neopravljeno*.

#### Posledice

V pogledu informacij o aktivnosti se poveča število, ki pogleduje količino porabljenega časa za izbrano aktivnost.

#### Posebnosti

Brez.

#### Prioritete identificiranih funkcionalnosti

Would have.

#### Sprejemni testi

- Prijavi se kot študent, izberi funkcionalnost za pogled koledarja, izberi funkcionalnost za dnevni pogled aktivnosti, pritisni na aktivnost na seznamu in pritisni na gumb za začetek beleženja časa aktivnosti. Po nekaj sekundah pritisni na gumb za ustavitev beleženja časa aktivnosti in preveri, da se je v pogledu informacij o aktivnosti številka, ki pogleduje porabljen čas ustrezno povečala.
___

### 5.17 Potrditev registracije profesorjev


#### Povzetek funkcionalnosti

Skrbnik preveri podatke registracije profesorja. Ob ustrezni registraciji mu dodeli ustrezne pravice.

#### Osnovni tok

- Skrbnik:
    1. Skrbnik pritisne na gumb za pogled zahtev za registracijo profesorjev.
    2. Skrbnik izbere zahtevo in se mu prikaže stran s podrobnostim zahteve.
    3. Skrbnik odobri ali zavrne zahtevo. 

#### Alternativni tok(ovi)

Brez.

#### Izjemni tokovi

Brez.

#### Pogoji

Uporabnik mora biti prijavljen v aplikacijo kot skrbnik.

#### Posledice

Profesor lahko uporablja funkcionalnosti za vlogo profesorja.

#### Posebnosti

Brez.

#### Prioritete identificiranih funkcionalnosti

Must have.

#### Sprejemni testi

- Prijavi se kot skrbnik, izberi funkcionalnost za pogled zahtev za registracijo profesorjev. Odpri zahtevo profesorja "x" in jo potrdi. Odjavi se. Prijavi se kot profesor "x" in preveri, če ima dostop do funkcionalnosti profesorja.

- Prijavi se kot skrbnik, izberi funkcionalnost za pogled zahtev za registracijo profesorjev. Odpri zahtevo profesorja "x" in jo zavrni. Odjavi se. Prijavi se kot profesor "x" in preveri, če nima dostopa do funkcionalnosti profesorja.
___

## 6. Nefunkcionalne zahteve

**Zahteve izdelka**

* Aplikacija je uporabnikom na voljo 24/7. *(Uporabniku je delujoča aplikacija dosegljiva v vsakem trenutku.)*
* Čas nedelovanja aplikacije kadarkoli v dnevu, ne sme trajati več kot 10 sekund. *(To je maksimalen čas, ki ga sme aplikacija porabiti, da se spet postavi v delujoče stanje oz.
 maksimalen čas v katerem aplikacija ne bo dosegljiva uporabniku.)*
* Aplikacija mora imeti v času delovanja odzivnost manjšo od 200 ms. *(Med preklaplanjem aktivnosti ne sme preteči več kot 200 ms. Strežnik mora torej biti zmožen dovolj hitro generirati in vrniti rezultat, ki je ga uporabnik zahteva.)*
* Aplikacija mora biti zmožna hkrati streči najmanj 10.000 uporabnikov. *(Aplikacija ne sme podleči preobremenjenosti dokler je hkratno število uporabnikov manjše od 10.000.)*
* Aplikacija mora biti prenosljiva v oblačne storitve *(Aplikacijo je mogoče namestiti v oblačne storitve kot so naprimer AWS, Azure ali Google Cloud.)*

**Organizacijske zahteve**

* Uporabniki v vlogi "profesor" in "študent" se morajo prijaviti z mailom, ki jim ga je dodelil fakulteta.
* Aplikacija bo izdelana z MEAN arhitekturo (MongoDB, Express, AngularJS, Node.js).

**Zunanje zahteve**

* Aplikacija mora biti v skladu z pravilnikom o varovanju zasebnih podatkov.




## 7. Prototipi vmesnikov

[Prijava v aplikacijo](https://wireframe.cc/5uKxzH)
![Login screen](../img/wireframe-login.jpg)

[Registracija](https://wireframe.cc/XHaoUn)
![Register screen](../img/wireframe-register.jpg)

[Pregled aktivnosti v koledarju](https://wireframe.cc/Xf4Iel)
![Calendar view](../img/wireframe-calendar.jpg)

[Pregled dnevnih aktivnosti](https://wireframe.cc/jaG2uR)
![Day view](../img/wireframe-day-view.jpg)

[Dodajanje aktivnosti](https://wireframe.cc/OrdP2k)
![Add activity](../img/wireframe-new-activity.jpg)

[Urejanje aktivnosti](https://wireframe.cc/cPeCTH)
![Edit activity](../img/wireframe-edit-activity.jpg)

[Prikaz vseh predmetov](https://wireframe.cc/P67P0j)
![Courses view](../img/wireframe-courses.jpg)

[Urejanje predmeta](https://wireframe.cc/PxitGt)
![Edit course](../img/wireframe-course-add-edit.jpg)

[Potrjevanje registracije profesorjev](https://wireframe.cc/EMGdUv)
![Day view](../img/wireframe-admin.jpg)

## 8. Vmesniki do zunanjih sistemov

Naša aplikacija ima dve interakciji z zunanjim sistemom Urnik FRI, vendar za obe potrebujemo isti vmesnik, saj sta si ustvarjanje in urejanje predmeta sorodni funkcionalnosti.

Sistem Urnik FRI bi v našem primeru moral ponujati funkcijo

```
predmeti()
```

ki ne sprejme nobenih argumentov, vrne pa seznam vseh predmetov v obliki JSON objekta. Vsak predmet pa je opisan s sledečimi podatki:

- naziv predmeta,
- termini predavanj
    - dan
    - začetek
    - trajanje
    - številka predavalnice
    - izvajalec
- termini vaj
    - dan
    - začetek
    - trajanje
    - številka laboratorija
    - izvajalec