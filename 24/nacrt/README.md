# Načrt sistema

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Matej Ajster, Alen Juršič, Samo Okorn in Gregor Štefanič |
| **Kraj in datum** | Ljubljana, 22.04.2019 |



## Povzetek

Načrt arhitekture predstavlja znano MVC arhitekturo. Arhitektura je ustrezna predvsem, ker  ločuje predstavitev in interakcijo od sistemskih podatkov. Sistem je strukturiran v tri logične komponente, ki medsebojno sodelujejo. Komponenta model (M) upravlja sistemske podatke in povezane operacije na teh podatkih. Komponenta pogled (V) opredeljuje in upravlja z načinom predstavitve podatkov uporabniku. Komponenta krmilnik (C) upravlja uporabniško interakcijo (npr. pritiski tipk, kliki miške itd.) in te interakcije prenese v pogled in model, kot je prikazano na sliki logičnega pogleda.

Načrt strukture sistema predstavlja razredni diagram v sklopu 2, ki vsebuje 7 razredov. Razredi Študent, Profesor in Skrbnik predstavljajo uporabnike sistema z različnimi lastnostmi in funkcijami ter podedujejo lastnosti razreda Oseba, ki predstavlja splošnega uporabnika. Razred Aktivnosti, ki zajema lastnosti aktivnosti kot so: izpiti, kolokviji, obvestila, ipd., dopolnjuje razred Predmet. Razred Predmet zajema lastnosti posamezega predmeta, katere upravlja razred Profesor. Vsak študent je v asociaciji z razredom Predmet, saj potrebuje povezavo za prikaz aktivnosti predmeta na glavni strani. Vse možne predmete dobimo preko API-ja, ki jih predstavlja razred Urnik FRI.

Za lažje razumevanje sistema pa smo na koncu za vsako funkcionalnost naredili diagrame zaporedja, ki podrobno prikazujejo kako naj bi se sistem obnašal, ter kronološki potek same funkcionalnosti in njenih odvisnosti.


## 1. Načrt arhitekture

Odločili smo se, da naši aplikaciji najbolj ustreza arhitektura MVC(Model-View-Controller), zato ker potrebujemo več načinov prikaza in interakcije s podatki in imamo v ekipi že nekaj izkušenj s to arhitekturo.

### Logični pogled:

![logicni-mvc](../img/arhitektura-vzorca-MVC.png)

### Razvojni pogled:

![razvojni-mvc](../img/razvojni-pogled.png)


## 2. Načrt strukture

### 2.1 Razredni diagram

![razredni-diagram](../img/razredni-diagram.png)

### 2.2 Opis razredov

#### Razred - Oseba

* Predstavlja splošno osebo, ki uporablja sistem.

##### Atributi

* id: Integer
* ime: String
* priimek: String
* email: String
* geslo: String
    * geslo bo shranjeno kot zgoščena vrednost v obliki String-a.
* vloga: Enum

#### Študent

* Predstavlja osebo, ki uporablja sistem, z vlogo študenta.

##### Atributi

* vpisna_št: Integer
* naročnine: List<Predmet>

##### Nesamoumevne metode

* dodajNaročnino(predmet: Predmet): Boolean
    * doda predmet na seznam naročnin.
    * rezultat je tipa Boolean, ki predstavlja uspešnost metode.
* odstraniNaročnino(predmet: Predmet): Boolean
    * odstrani predmet iz seznama naročnin.
    * rezultat je tipa Boolean, ki predstavlja uspešnost metode.
* pridobiVseAktivnosti(): List<Aktivnosti>
    * vrne seznam aktivnosti vseh predmetov v seznamu naročnin.


#### Razred - Profesor

* Predstavlja osebo, ki uporablja sistem, z vlogo profesorja.

##### Atributi

* predmeti: List<Predmet>

##### Nesamoumevne metode

* dodajPredmet(predmet: Predmet): Boolean
    * doda predmet na seznam predmetov.
    * rezultat je tipa Boolean, ki predstavlja uspešnost metode.
* odstraniPredmet(predmet: Predmet): Boolean
    * odstrani predmet iz seznama predmetov.
    * rezultat je tipa Boolean, ki predstavlja uspešnost metode.

#### Razred - Skrbnik

* Predstavlja osebo, ki uporablja sistem, z vlogo skrbnika.

##### Atributi

* profesorji: List<Profesor>

##### Nesamoumevne metode

* overiProfesorja(profesor: Profesor, overitev: Boolean): Boolean
    * izbranega profesorja iz seznama profesorjev potrdi ali zavrne.
    * rezultat je tipa Boolean, ki predstavlja uspešnost metode.

#### Razred - Predmet

* Predstavlja predmet, ki ga profesor uči in na katerega se lahko študent naroči, da sprejema aktivnosti.

##### Atributi

* naziv: String
* opis: String
* aktivnosti: List<Aktivnosti>

##### Nesamoumevne metode

* urediPredmet(predmet: Predmet): Boolean
    * uredi atribute predmeta.
    * rezultat je tipa Boolean, ki predstavlja uspešnost metode.
* dodajAktivnost(aktivnost: Aktivnost): Boolean
    * doda aktivnost na seznam aktivnosti predmeta.
    * rezultat je tipa Boolean, ki predstavlja uspešnost metode.
* odstraniAktivnost(aktivnost: Aktivnost): Boolean
    * odstrani aktivnost iz seznama predmetov.
    * rezultat je tipa Boolean, ki predstavlja uspešnost metode.

#### Razred - Aktivnost

* Predstavlja aktivnosti posameznega predmeta. To je lahko izpit, kolokvij ali katerikoli dogodek po meri.

##### Atributi

* naziv: String
* datum: Date
* ura: Time
* predmet: Predmet
* prioriteta: Enum

##### Nesamoumevne metode

* UrediAktivnost(aktivnost: Aktivnost): Boolean
    * uredi aktivnost.
    * rezultat je tipa Boolean, ki predstavlja uspešnost metode.

## 3. Načrt obnašanja

###3.1 Registracija
![Registracija](../img/diagrami_zaporedja/Registracija.png)

*Diagram zaporedja, ki prikazuje potek regisracije uporabnikov*

###3.2 Prijava v aplikacijo
![Prijava](../img/diagrami_zaporedja/sequence-prijava.jpg)

###3.3 Odjava iz aplikacije
![Odjava](../img/diagrami_zaporedja/Odjava.png)

*Diagram zaporedja, ki prikazuje potek odjave študenta, profesorja in skrbnika*

###3.4 Pregled aktivnosti v koledarju
![Pregled aktivnosti v koledarju](../img/diagrami_zaporedja/sequence-koledar.jpg)

###3.5 Pregled dnevnih aktivnosti
![Pregled dnevnih aktivnosti](../img/diagrami_zaporedja/Prikaz_aktivnosti.png)

*Diagram zaporedja, ki prikazuje potek prikaza vseh aktivnosti posameznega uporabnika*

###3.6 Dodajanje aktivnosti
![Dodajanje aktivnosti](../img/diagrami_zaporedja/sequence-dodajanje-aktivnosti.jpg)

###3.7 Odstranjevanje aktivnosti
![Odstranjevanje aktivnosti](../img/diagrami_zaporedja/Izbris_aktivnosti.png)

*Diagram zaporedja, ki prikazuje potek odstranitve določene aktivnosti prijavljenega uporabnika*

###3.8 Urejanje aktivnosti
![Urejanje aktivnosti](../img/diagrami_zaporedja/sequence-posodabljanje-aktivnosti.jpg)

###3.9 Ustvarjanje predmeta
![Ustvarjanje predmeta](../img/diagrami_zaporedja/Ustvarjanje_predmeta.png)

*Diagram zaporedja, ki prikazuje potek ustvarjanja predmeta*

###3.10 Urejanje predmeta
![Urejanje predmeta](../img/diagrami_zaporedja/sequence-urejanje-predmeta.jpg)

###3.11 Odstranjevanje predmeta
![Izbris predmeta](../img/diagrami_zaporedja/Izbris_predmeta.png)

*Diagram zaporedja, ki prikazuje potek brisanja predmeta*

###3.12 Pogled vseh predmetov
![Pogled vseh predmetov](../img/diagrami_zaporedja/sequence-prikazi-seznam-predmetov.jpg)

###3.13 Sledenje predmetu
![Sledenje predmetu](../img/diagrami_zaporedja/Sledenje_predmetu.png)

*Diagram zaporedja, ki prikazuje kako poteka pričetek sledenja predmetu*

###3.14 Preklic sledenja predmetu
![Preklic sledenju predmeta](../img/diagrami_zaporedja/Preklic_Sledenju_predmeta.PNG)

*Diagram zaporedja, ki prikazuje kako poteka reklic sledenja predmetu*

###3.15 Spreminjanje statusa aktivnosti
![Spreminjanje statusa](../img/diagrami_zaporedja/Spreminjanje_statusa_aktivnosti.png)

*Diagram zaporedja, ki prikazuje potek spreminjanja statusa posamezne aktivnosti uporabnika*

###3.17 Beleženje porabljenega časa
![Beleženje porabljenega časa](../img/diagrami_zaporedja/sequence-belezenje-casa.jpg)

###3.17 Potrditev registracije profesorjev
![Potrditev registracije](../img/diagrami_zaporedja/Potrditev_registracije.png)

*Diagram zaporedja, ki prikazuje potek potrditve registracije uporabnikov v vlogi 'profesor'*



