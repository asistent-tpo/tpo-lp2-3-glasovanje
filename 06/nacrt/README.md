# Načrt sistema

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Ljupche Milosheski, Elvisa Alibašić, Maj Bajuk, Zala Erič |
| **Kraj in datum** | Ljubljana, 19.4.2019 |



## Povzetek

V tem dokumentu smo opisali naš načrt za zahteve iz drugega lastnega projekta. Odločili smo se, da bomo uporabljali arhitekturni vzorec MVC. V prvem poglavju smo načrtali logični in razvojni pogled. Za logični pogled smo uporabili blokovni diagram namesto razrednega, saj je razredni načrt že zelo podrobno vsebovan v drugem poglavju. Za razvojni pogled smo uporabili UML komponentni diagram. V naslednjem poglavju smo predstavili razredni diagram. Pri načrtovanju tega diagrama smo začeli iz primerov uporabe, in ga nato organizirali. Na koncu so diagrami zaporedja, ki najbolj podrobno opisusejo delovanje naše aplikacije za vsak primer uporabe. Zaradi preglednosti smo v razrednem diagramu spustili parametre metod, so pa navedene v podrobnem opisu razredov.



## 1. Načrt arhitekture

### 1.1 Logični pogled

Pri logičnem modelu smo uporabili blokovni diagram, ki bolj natačno predstavi sistem. Ker je razredni že vsebovan v 2. poglavju, smo se odločili, da ga ne bomo uporabimo tudi za logični pogled.

![Logični pogled](../img/logical_view.png)

### 1.2 Razvojni pogled

![Razvojni pogled](../img/development_view.png)

## 2. Načrt strukture

### 2.1 Razredni diagram

![Graf VOPC](../img/VOPC.png)

### 2.2 Opis razredov

#### 2.2.1 Ime razreda: CommonUser

* Razred CommonUser je entitetni razred, s katerim predstavljamo navadnega uporabnika.

##### Atributi
    
 Razred zaradi dedovanja in svojih povezav v razrednem diagramu že sam po sebi vsebuje sledeče:
     * referenco na fakulteto (razred Faculty), katero obiskuje (neobvezno, uporabnik mogoče pri registraciji ni izbral fakultete)
     * uporabnikove dodane aktivnosti (razred Activity)
     * predmete (razred Course) v katere je uporabnik vpisan
     

| Ime atributa     | Podatkovni tip (če ni očiten)    | Zaloga vrednosti (če ni očitna) |
| -----------------|----------------------------------|---------------------------------| 
| id               | Long                             | [10, inf>*                      | 
| username  | String                                | -                               |
| email            | String                           | -                               |


* **Opomba:** Prvih 10 id-jev je rezervirano za skrbnike. 

##### Nesamoumevne metode

| Ime metode     | Imena in tipovi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| age()  |  -  |  int  | |
| toString()  |  -  |  String | |
| getActivities()  |  -  |  Activity[] | Pridobi reference na vse uporabnikove aktivnosti |
| getCourses()  |  -  |  Course[] | Pridobi reference na vse uporabnikove predmete |
| shareApplication() | - | void | Se kliče takrat, ko uporabnik podeli aplikacijo na družabnem omrežju Facebook |
| activityPriorityChange() | - | void | Se kliče takrat, ko uporabnik želi spremeniti prioriteto aktivnosti |
| requestActivityDescriptionChange() | - | void | Se kliče takrat, ko uporabnik želi spremeniti opis aktivnosti |
| confirmActivityDescriptionChanges() | - | void | Se kliče takrat, ko uporabnik potrdi spremembo opisa aktivnosti |


#### 2.2.2 Ime razreda: GuestUser

* Razred GuestUser je entitetni razred, s katerim predstavljamo neprijavljenega uporabnika.

##### Atributi

V razredu ne hranimo nobenih atributov.

* **Opomba:** Prvih 10 id-jev je rezervirano za skrbnike. 

##### Nesamoumevne metode

Razred ne vsebuje metode.

#### 2.2.3 Ime razreda: PrivilegedUser

* Razred PrivilegedUser je entitetni razred, s katerim predstavljamo priviligiranega uporabnika. Generaliziran je v CommonUser.

##### Atributi

 Razred zaradi dedovanja in svojih povezav v razrednem diagramu že sam po sebi vsebuje sledeče:
     * vse atribute, metode in reference, definirane v razredu CommonUser
     
**Tip uporabnika dodatnih atributov v primerjavi z CommonUser NIMA**

* **Opomba:** Prvih 10 id-jev je rezervirano za skrbnike.  

##### Nesamoumevne metode

**Tip uporabnika dodatnih metod v primerjavi z CommonUser NIMA**

#### 2.2.4 Ime razreda: Admin

* Razred Admin je entitetni uporabnik, ki predstavlja skrbnika aplikacije. Generaliziran je v PrivilegedUser.

##### Atributi

Razred zaradi dedovanja in svojih povezav v razrednem diagramu že sam po sebi vsebuje sledeče:
     * vse atribute, metode in reference, definirane v razredu PrivilegedUser
     
**Tip uporabnika dodatnih atributov v primerjavi z PrivilegedUser NIMA**

* **Opomba:** Čeprav je to možno in si jo lahko izbere, admin ponavadi nima reference na fakulteto.

##### Nesamoumevne metode

**Tip uporabnika dodatnih metod v primerjavi z PrivilegedUser NIMA**

#### 2.2.5 Ime razreda: Faculty

* Razred Faculty je entitetni razred, ki predstavlja šolsko ustanovo, v katero je združenih več predmetov naše aplikacije, poleg tega pa je povezana s uporabnikom, saj si fakulteto, ki jo obiskuje, lahko izbere.

##### Atributi

Razred zaradi dedovanja in svojih povezav v razrednem diagramu že sam po sebi vsebuje sledeče:
     * predmete (razred Course), ki pripadajo tej fakulteti

| Ime atributa     | Podatkovni tip (če ni očiten)    | Zaloga vrednosti (če ni očitna) |
| -----------------|----------------------------------|---------------------------------| 
|      id          |             String                 |          [1, inf>            | 
|      name          |            -                  |          -            | 
|      city          |             -                 |          -            | 
|      country          |             -                 |          -            | 


##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| toString()  |  -  |  String | Vrne vse podatke o uporabniku v obliki niza |
| getCourses()  |  -  |  Course[] | Vrne reference na predmete, ki pripadajo tej fakulteti |

#### 2.2.6 Ime razreda: Course

* Razred Course predstavlja predmete, ki so na voljo v naši aplikaciji. Predstavljajo skupke aktivnosti, povezani so pa z fakultetami.

##### Atributi

 Razred zaradi dedovanja in svojih povezav v razrednem diagramu že sam po sebi vsebuje sledeče:
     * referenco na fakulteto (razred Faculty) na kateri se izvaja
     * referenco na aktivnosti (razred Activity), ki so zajete v njem
     

| Ime atributa     | Podatkovni tip (če ni očiten)    | Zaloga vrednosti (če ni očitna) |
| -----------------|----------------------------------|---------------------------------| 
|      id          |             String                 |          [1, inf>            | 
|      name          |            -                  |          -            | 


##### Nesamoumevne metode


| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| toString()  |  -  |  String | Vrne vse podatke o uporabniku v obliki niza |
| getFaculty()  |  -  |  Faculty | Vrne referenco na fakulteto, kateri predmet pripada  |
| remove()  | user_Id - String  |  boolean | Izbriše uporabnika iz predmeta  |

#### 2.2.7 Ime razreda: Activity

* Razred Activity predstavlja aktivnosti, ki so na voljo v naši aplikaciji. Aktivnost je lahko povezana s predmetom, ki jo zajema.

##### Atributi

 Razred zaradi dedovanja in svojih povezav v razrednem diagramu že sam po sebi vsebuje sledeče:
     * morebitno referenco na predmet (razred Course)
     * referenco na uporabnike (razred CommonUser) aktivnosti
     
| Ime atributa     | Podatkovni tip (če ni očiten)    | Zaloga vrednosti (če ni očitna) |
| -----------------|----------------------------------|---------------------------------| 
|      id          |             String                 |          [1, inf>            | 
|      activityName          |            String                 |          -            | 
|      dateStart          |            Date                  |          -            | 
|      dateEnd          |            Date                  |          -            |
|      generalAvailability          |            boolean                  |          -            | 


##### Nesamoumevne metode


| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| isNear()  |  -  |  Boolean    | Preveri ali se bo aktivnost izvajala v prihodnjem dnevu |
| getCourse()  |  -  |  Course   | Pridobi referenco na predmet, kateremu pripada - če ga ni vrne null  |
| toString()  |  -  |  String   | Vrne vse podatke o uporabniku v obliki niza |
| getUserCount()  |  -  |  int   | Vrne število uporabnikov, ki so vezani na to aktivnost |
| getData() | - | ActivityData | Vrni opis aktivnosti |
| updateData() | data : ActivityData | void | Osveži opis aktivnsoti |
| addActivity() | user_id - String, course - Course, date - Date[], generalAvailability - boolean | void | Doda aktivnost |

#### 2.2.8 Ime razreda: K - LoginController

* Razred LoginController predstavlja kontrolni razred primera uporabe prijave uporabnika.
##### Atributi

V razredu ne hranimo nobenih atributov.

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| login()  |  isFacebookLogin - boolean  |  void    | Poskus prijave uporabnika |
| checkValidity()  |  -  |  boolean    | Preveri pravilnost vnešenih podatkov |
| sendError()  |  -  |  void    | Pošlje napako, če se je kje v postopku zalomilo |

#### 2.2.9 Ime razreda: ZM - Login

* Razred Login predstavlja mejni razred primera uporabe prijave uporabnika.
##### Atributi

| Ime atributa     | Podatkovni tip (če ni očitna)    | Zaloga vrednosti (če ni očitna) |
| -----------------|----------------------------------|---------------------------------| 
|      username          |            String                |          -           | 
|      password          |            String                 |          -            | 

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| readUsername()  |  -  |  String    | Preberi vpisano uporabniško ime |
| readPassword()  |  -  |  String    | Preberi vpisano geslo |
| displayError()  |  -  |  void    | Izpiši morebitno napako |
| finishLogin()  |  -  |  void    | Preusmeri na domačo stran in odpri sejo za uporabnika |

#### 2.2.10 Ime razreda: K - RegistrationController

* Razred RegistrationController predstavlja kontrolni razred primera uporabe registracije uporabnika.
##### Atributi

V razredu ne hranimo nobenih atributov.

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| register()  | - |  void    | Poskus registracije uporabnika |
| checkValidity()  |  -  |  boolean    | Preveri pravilnost vnešenih podatkov |
| getFacultyList()  |  -  |  Faculty[]    | Pridobi seznam fakultet |
| sendError()  |  -  |  void    | Pošlje napako, če je kje v postopku šlo kaj narobe |
| approveRegistration()  |  -  |  Boolean    | Sporoči, da se je proces registracije uspešno končal.|
| finishRegistration()  |  -  |  void    | Sporoči, da se je proces registracije uspešno končal.|


#### 2.2.11 Ime razreda: ZM - Registration

* Razred Registration predstavlja mejni razred primera uporabe registracije uporabnika.
##### Atributi

| Ime atributa     | Podatkovni tip (če ni očiten)    | Zaloga vrednosti (če ni očitna) |
| -----------------|----------------------------------|---------------------------------| 
|      username          |            String                |          -           | 
|      password          |            String                 |          -            | 
|      faculty          |            Faculty                 |          -            | 

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| readUsername()  |  -  |  String    | Preberi vpisano uporabniško ime |
| readPassword()  |  -  |  String    | Preberi vpisano geslo |
| displayPossibleFaculties()  |  faculties - Faculty[]  |  void    | Prikaži možne fakultete v spustnem seznamu |
| readFaculty()  |  -  |  Faculty    | Preberi izbrano fakulteto |
| displayError()  |  -  |  void    | Izpiši napako |
| finishRegistration()  |  -  |  void    | Doda uporabnika v bazo, potem ga preusmeri na domačo stran.  |
| displayFaculties()  |  -  |  void    | |


#### 2.2.12 Ime razreda: K - ProfileChangeController

* Razred ProfileChangeController kontrolni razred primera uporabe spremembe profila.
##### Atributi

V razredu ne hranimo nobenih atributov.

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| assertChanges()  | - |  void   | Uveljavi spremenjene podatke |
| checkIfChanged()  | - |  boolean    | Preveri ali je uporabnik sploh spremenil podatke (ali so isti kot so že bili?) |
| startValidation()  |  String username, String password, Faculty faculty  |  void    | Začne proces preverjanja ustreznosti podatkov.|
| checkValidity()  |  -  |  boolean    | Preveri veljavnost vnešenih podatkov (npr. vrne false če uporabniško ime že obstaja) |
| sendError()  |  -  |  void    | Pošlje napako, če se je kje v postopku zalomilo |
| requestProfileModification()  |  -  |  void    | |
| proceedProfileModification()  |  -  |  void    | |
| modifyProfile()  |  String username, String password, Faculty faculty  |  void    | |
| finishProfileModification()  |  - |  void    | Ustrezno popravi bazo in preusmeri na domačo stran |
| getFacultyList()  |  -  |  Faculty[]    | Pridobi seznam fakultet |
| sendError()  |  -  |  void    | Pošlje napako, če je kje v postopku šlo kaj narobe |


##### Nesamoumevne metode


#### 2.2.13 Ime razreda: ZM - ProfileChange

* Razred ProfileChange mejni razred primera uporabe sprememba profila.
##### Atributi

| Ime atributa     | Podatkovni tip (če ni očiten)    | Zaloga vrednosti (če ni očitna) |
| -----------------|----------------------------------|---------------------------------| 
|      username          |            String                |          -           | 
|      password          |            String                 |          -            | 
|      faculty          |            Faculty                 |          -            | 

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| readUsername()  |  -  |  String    | Preberi vpisano uporabniško ime |
| readPassword()  |  -  |  String    | Preberi vpisano geslo |
| readFaculty()  |  -  |  Faculty    | Preberi vpisano fakulteto |
| displayPossibleFaculties()  |  faculties - Faculty[]  |  void    | Prikaži možne fakultete v spustnem seznamu |
| readChoosenFaculty()  |  -  |  Faculty | Preberi izbrano fakulteto |
| displaySuccess()  |  -  |  void    | Izpiši sporočilo o uspehu spremembe |
| displayError()  |  -  |  void    | Izpiši napako |



#### 2.2.14 Ime razreda: K - CourseViewController

* Razred CourseViewController predstavlja kontrolni razred primera uporabe prikaza predmetnika in prikaza citatov
##### Atributi

V razredu ne hranimo nobenih atributov.

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| getOrderedCourseList()  | user_id - String |  Course[]    | Pridobi seznam vseh predmetov (na začetku so tisti, v katere je uporabnik vpisan, sledijo vsi ostali) |
| searchCourses()  |  text - String  |  Course[]    | Pridobi predmete, ki bi lahko ustrezale vnešenemu nizu (vsebujejo vnešeni niz) |
| sendError()  |  -  |  void    | Pošlje napako, če je kje v postopku šlo kaj narobe |



#### 2.2.15 Ime razreda: ZM - CourseView

* Razred CourseView predstavlja mejni razred primera uporaba prikaza predmetnika in citata
##### Atributi

V razredu ne hranimo nobenih atributov.

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| showCourses()  |  courses - Course[]  |  void    | Prikaže uporabnikove predmete |
| getCourses()  |  -  |  void   | Prevzame uporabniško zahtevo za seznam predmetov |

#### 2.2.16 Ime razreda: K - QuoteController

* Razred QuoteController predstavlja kontrolni razred za primer uporabe prikazovanja citatov.
##### Atributi

V razredu ne hranimo nobenih atributov.

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| requestNewQuote()  |  -  |  void    | Začne postopek zahteve novega citata |
| sendError()  |  -  |  void    | Pošlje napako, če je kje v postopku šlo kaj narobe |


#### 2.2.17 Ime razreda: ZM - QuoteView

* Razred QuoteView predstavlja mejni razred za primer uporabe prikazovanja citatov.
##### Atributi

V razredu ne hranimo nobenih atributov.

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| displayQuote()  |  quote - String|  void   | Prikaže citat |
| requestNewQuote() | - | void | Prične zahtevek strani po osvežitvi citata |

#### 2.2.18 Ime razreda: ZM - QuoteAPI

* Razred QuoteAPI predstavlja mejni razred za komunikacijo z zunanjim API-jem za citate.
##### Atributi

V razredu ne hranimo nobenih atributov.

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| getNewQuote()  |  API_data - String (API key, date and time) |  String    | Pridobi novi citat |

#### 2.2.19 Ime razreda: K - AdminEditProfilesController

* Razred AdminEditProfilesController predstavlja kontrolni razred primera uporabe ureditve uporabnikovih profilov ter privilegijev s strani skrbnika.
##### Atributi

V razredu ne hranimo nobenih atributov.

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| getUserList()  | text - String |  CommonUser[]   | Pridobi vse uporabnike, ki obstajajo v aplikaciji oz. ustrezajo vnešenemu nizu (iskanje po uporabnikih) |
| getFacultyList()  |  -  |  Faculty[]    | Pridobi seznam fakultet (skrbnik lahko uporabniku spreminja fakulteto) |
| checkValidity()  |  username - String, password - String, privilege - String, faculty - Faculty  |  boolean    | Preveri veljavnost vnešenih podatkov |
| assertUserProfileChange()  |  user_id - String, username - String, password - String, privilege - String, faculty - Faculty   |  void    | Spremeni profil uporabnika |
| sendError()  |  -  |  void | Pošlje napako, če se je kje v postopku zalomilo |
| requestProfileModification()  |  -  |  void |  |
| proceedModificationRequest()  |  -  |  void |  |
| modifyProfile()  |  String username, String password, Faculty faculty  |  void |  |
| finishProfileModification()  |  - |  void    | Ustrezno popravi bazo in preusmeri na domačo stran |



#### 2.2.20 Ime razreda: ZM - AdminEditProfiles

* Razred AdminEditProfiles predstavlja mejni razred primera uporabe ureditve uporabnikovih profilov ter privilegijev s strani skrbnika.
##### Atributi

| Ime atributa     | Podatkovni tip (če ni očiten)    | Zaloga vrednosti (če ni očitna) |
| -----------------|----------------------------------|---------------------------------| 
|      username          |            String                |          -           | 
|      password          |            String                 |          -            | 
|      faculty          |            Faculty                 |          -            | 
|      user_id          |            int                 |          -            | 

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| showUsers()  | - |  void    | Prikaže uporabnike |
| readUsername()  | - |  String    |  |
| readPassword()  | - |  String    |  |
| readFaculty()  | - |  Faculty    |  |
| readUser_id()  | - |  int    |  |
| displayPossibleFaculties()  |  faculties - Faculty[]  |  void    | Prikaži možne fakultete v spustnem seznamu |
| readChosenFaculty()  |  -  |  Faculty    | Preberi izbrano fakulteto |
| getEnteredSearchTerm()  | - |  String    | Pridobi vnešen niz za iskanje po uporabnikih |
| showMessage()  |  -  |  void    | Prikaže sporočilo o uspehu/napaki |


#### 2.2.21 Ime razreda: K - RemoveUserController

* Razred RemoveUserController predstavlja kontrolni razred primera uporabe brisanja uporabniških profilov.
##### Atributi

V razredu ne hranimo nobenih atributov.

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| getUserList()  | text - String |  CommonUser[]   | Pridobi vse uporabnike, ki obstajajo v aplikaciji oz. ustrezajo vnešenemu nizu (iskanje po uporabnikih) |
| checkValidity()  |  username - String, password - String |  boolean    | Preveri veljavnost vnešenih podatkov |
| deleteUser()  |  user_id - int |  void    | Odstrani uporabnika |
| sendError()  |  -  |  void    | Pošlje napako, če se je kje v postopku zalomilo |
| requestProfileModification()  |  -  |  void |  |
| proceedModificationRequest()  |  -  |  void |  |
| modifyProfile()  |  String username, String password, Faculty faculty  |  void |  |
| finishProfileModification()  |  - |  void    | Ustrezno popravi bazo in preusmeri na domačo stran |



#### 2.2.22 Ime razreda: ZM - RemoveUser

* Razred RemoveUser predstavlja mejni razred primera uporabe brisanja uporabniških profilov.
##### Atributi

| Ime atributa     | Podatkovni tip (če ni očitna)    | Zaloga vrednosti (če ni očitna) |
| -----------------|----------------------------------|---------------------------------| 
|      username          |            String                |          -           | 
|      password          |            String                 |          -            | 
|      faculty          |            Faculty                 |          -            | 
|      user_id          |            int                 |          -            | 


##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| showUsers()  | - |  void    | Prikaže uporabnike |
| readUsername()  | - |  String    |  |
| readPassword()  | - |  String    |  |
| readFaculty()  | - |  Faculty    |  |
| readUser_id()  | - |  int    |  |
| getEnteredSearchTerm()  | - |  String    | Pridobi vnešen niz za iskanje po uporabnikih |
| showMessage()  |  -  |  void    | Prikaže sporočilo o uspehu/napaki |

#### 2.2.23 Ime razreda: K - AddCourseController

* Razred AddCourseController predstavlja kontrolni razred primera uporabe dodajanja predmetov.
##### Atributi

V razredu ne hranimo nobenih atributov.

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| getFacultyList()  |  -  |  Faculty[]    | Pridobi seznam fakultet |
| checkValidity()  |  courseName - String |  boolean    | Preveri veljavnost vnešenega imena predmeta |
| createCourse()  |  courseName - String, faculty - Faculty |  void  | Ustvari nov predmet |
| sendError()  |  -  |  void    | Pošlje napako, če je kje v postopku šlo kaj narobe |

#### 2.2.24 Ime razreda: ZM - AddCourse

* Razred AddCourse predstavlja mejni razred primera uporabe dodajanja predmetov.
##### Atributi

V razredu ne hranimo nobenih atributov.

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| getCourseName()  | - |  String    | Pridobi vnešeno ime predmeta |
| displayPossibleFaculties()  |  faculties - Faculty[]  |  void    | Prikaži možne fakultete v spustnem seznamu |
| readChosenFaculty()  |  -  |  Faculty    | Preberi izbrano fakulteto |
| showMessage()  |  -  |  void    | Prikaže sporočilo o uspehu/napaki |
| addCourse()  |  courseName - String, faculty - Faculty  |  void  | Sporoči uporabniško zahtevo za dodajanje predmeta |
| inputName()  |  name - String  |  void  | Pridobi ime iz vnosnega polja |
| chooseFaculty()  |  faculty - Faculty  |  void  | Pridobi izbrano fakulteto iz spustnega seznama |

#### 2.2.25 Ime razreda: K - CourseSignInController

* Razred CourseSignInController predstavlja kontrolni razred primera uporabe vpisa v predmet.
##### Atributi

V razredu ne hranimo nobenih atributov.

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| checkValidity()  | user_id - String, course - Course |  boolean    | Preveri veljavnost akcije - mogoče je uporabnik že vpisan v predmet |
| addUserToCourse()  |  user_id - String, course - Course |  void  | Dodaj uporabnika v predmet |
| sendError()  |  -  |  void    | Pošlje napako, če je kje v postopku šlo kaj narobe |


#### 2.2.26 Ime razreda: ZM - CourseSignIn

* Razred CourseSignIn predstavlja mejni razred primera uporabe vpisa v predmet.
##### Atributi

V razredu ne hranimo nobenih atributov.

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| showCourseName()  |  -  |  void    | Prikaže ime predmeta, v katerega se uporabnik hoče vpisati |
| showMessage()  |  -  |  void    | Prikaže sporočilo o uspehu/napaki |

#### 2.2.27 Ime razreda: K - ActivityDescriptionController

* Razred ActivityDescriptionController kontrolni razred primera uporabe pogleda podrobnosti, spreminjanja in brisanja uporabnikovih aktivnosti.
##### Atributi

V razredu ne hranimo nobenih atributov.

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| getActivityList()  | user_id - String |  Activity[]   | Pridobi aktivnosti, vezane na uporabnika |
| alreadyHitDeadline()  |  activity - Activity |  boolean  | Preveri, ali je že potekel rok aktivnosti |
| assertActivityStatusChange()  |  user_id - String, activity - Activity, status - boolean  |  void    | Spremeni status aktivnosti |
| deleteActivity() | activity - Activity | void | Odstrani aktivnost |
| sendError()  |  -  |  void    | Pošlje napako, če je kje v postopku šlo kaj narobe |
| returnData() | user_id : String, activity_id : String | ActivityData | Vrni opis aktivnosti |
| requestActivityData() | user_id : String, activity_id : String | void | Zahtevaj opis aktivnosti |
| setActivityData() | user_id : String, activity_id : String, data : ActivityData | void | Spremeni opisa aktivnosti |


#### 2.2.28 Ime razreda: ZM - ActivityDescription

* Razred ActivityDescription mejni razred primera uporabe pogleda podrobnosti, spreminjanja in brisanja uporabnikovih aktivnosti.
##### Atributi

V razredu ne hranimo nobenih atributov.

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| showActivities()  |  activities - Activity[]  |  void    | Prikaže seznam aktivnosti, ter njihove podatke |
| showActivity()  |  activity - Activity  |  void    | Prikaže podrobnosti izbrane aktivnosti (koledar, status, itd) |
| getChoosenStatus()  |  -  |  String    | Pridobi status, ki ga je izbral uporabnik |
| showMessage()  |  -  |  void    | Prikaže sporočilo o uspehu/napaki spreminjanja statusa/izbrisa aktivnosti |
| getDataAboutActivity() | user_id : String, activity_id : String | ActivityData | Pridobi opis o izbrani aktivnosti |
| updateActivityDescription() | user_id : String, activity_id : String, data : ActivityData | void | Zahtevaj spremembo opisa aktivnosti |
| showNewData() | user_id : String, activity_id : String | void | Osveži opis za določeno aktivnost |
| getActivity() | activity_id | void | Začne zahtevo po pridobitvi/prikazu aktivnosti |
| deleteActivity() | activity_id | void | Začne zahtevo po izbrisu aktivnosti |


#### 2.2.29 Ime razreda: K - AddActivityController

* Razred AddActivityController predstavlja kontrolni razred primera uporabe dodajanja nove aktivnosti.
##### Atributi

V razredu ne hranimo nobenih atributov.

##### Nesamoumevne metode

**Opomba:** Posebna metoda getActivityData() ni potrebna, ker so vsi podatki aktivnosti že v objektu razreda Activity.

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| checkValidity()  | activityName - String, course - Course, date - String | boolean |  Preveri veljavnost vnešenih podatkov |
| addActivity()  |  user_id - String, activityName - String, course - Course, date - String, generalAvailability - boolean  |  void    | Doda novo aktivnost in jo poveže s uporabnikom |
| getCourses()  |  -  |  Course[] | Pridobi vse predmete |
| sendError()  |  -  |  void    | Pošlje napako, če se je kje v postopku zalomilo |



#### 2.2.30 Ime razreda: ZM - AddActivity

* Razred AddActivity predstavlja mejni razred primera uporabe dodajanja nove aktivnosti.
##### Atributi

V razredu ne hranimo nobenih atributov.

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| showAdditionalInfo()  |  -  |  void    | Prikaže dodatna polja za izpolnitev |
| displayPossibleCourses()  |  courses - Course[]  |  void    | Prikaži možne predmete v spustnem seznamu |
| readActivityName()  |  -  |  String    | Preberi vnešeno ime predmeta |
| isCourseDependant()  |  -  |  boolean   | Vrne vrednost temu namenjenega preklopnega gumba (toggle) |
| isGenerallyAvailable()  |  -  |  boolean   | Vrne vrednost temu namenjenega preklopnega gumba (toggle) |
| readChosenCourse()  |  -  |  Course    | Prebere izbran predmet |
| readStartDate()  |  -  |  String    | Prebere izbran datum začetka aktivnosti |
| readEndDate()  |  -  |  String    | Prebere izbran datum konca aktivnosti |
| showMessage()  |  -  |  void    | Prikaže sporočilo o uspehu/napaki dodajanja nove aktivnosti |
| addActivity()  |  user_id - String, activityName - String, course - Course, date - String, generalAvailability - boolean   |  void    | Sporoči uporabniško akcijo za dodajanje aktivnosti |
| inputInfo()  |  -  |  void    | Pošlje vse vnesene podatke |
| getCourses()  |  -  |  void    | Pošlje zahtevo za seznam predmetov |
| chooseCourse()  |  -  |  void    | Zahteva določen predmet |

#### 2.2.31 Ime razreda: K - CourseSignOutController

* Razred CourseSignOutController predstavlja kontrolni razred primera uporabe izpisa iz predmeta.
##### Atributi

V razredu ne hranimo nobenih atributov.

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| checkValidity()  | user_id - String, course - Course |  boolean | Preveri veljavnost akcije - npr. ali je uporabnik sploh vpisan v predmet |
| removeUserFromCourse()  |  user_id - String, course - Course |  void  | Izpiši uporabnika iz predmeta |
| sendError()  |  -  |  void    | Pošlje napako, če je kje v postopku šlo kaj narobe |


#### 2.2.32 Ime razreda: ZM - CourseSignOut

* Razred CourseSignOut predstavlja mejni razred primera uporabe izpisa iz predmeta.
##### Atributi

V razredu ne hranimo nobenih atributov.

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| showCourseName()  |  courseName - String  |  void    | Prikaže ime predmeta, iz katerega se uporabnik hoče izpisati |
| logOutOfCourse()  |  -  |  void    | Sprejme uporabniško zahtevo po izpisu iz predmeta. |
| logUserOutOfCourse()  |  user_id - String  |  void   | Začni postopek za izpis uporabnika iz predmeta. |
| showMessage()  |  -  |  void    | Prikaže sporočilo o uspehu/napaki |


#### 2.2.33 Ime razreda: K - OthersActivityDecisionController

* Razred OthersActivityDecisionController predstavlja kontrolni razred primera uporabe odločitve o aktivnostih drugih uporabnikov
##### Atributi

V razredu ne hranimo nobenih atributov.

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| checkForNewGlobalActivity()  | user_id - String |  Activity | Preveri, ali za uporabnika (in njegove predmete) obstaja kakšna nova aktivnost, ki jo naj bi videli vsi |
| addActivity()  |  user_id - String, activity - Activity |  void    | Dodaj aktivnost uporabniku |
| sendError()  |  -  |  void    | Pošlje napako, če je kje v postopku šlo kaj narobe |


#### 2.2.34 Ime razreda: ZM - OthersActivityDecision

* Razred OthersActivityDecision predstavlja mejni razred primera uporabe odločitve o aktivnostih drugih uporabnikov
##### Atributi

V razredu ne hranimo nobenih atributov.

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| showActivityDescription()  |  -  |  void    | Prikaže dodatne informacije o aktivnosti |
| showMessage()  |  -  |  void    | Prikaže sporočilo o uspehu/napaki dodajanja nove aktivnosti |
| acceptActivity()  |  -  |  void    | Sprejme uporabnikovo potrditev nove aktivnosti |


#### 2.2.35 Ime razreda: K - PushNotificationController

* Razred PushNotificationController predstavlja kontrolni razred primera uporabe potisnih obvestil.
##### Atributi

V razredu ne hranimo nobenih atributov.

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| hasUserPushNotificationsEnabled()  |  -  |  boolean    | Vrne ali ima uporabnik vklopljena potisna obvestila |
| newMessage()  |  -  |  void    | Začne postopek obdelave novega obvestila namenjenega uporabniku |
| sendError()  |  -  |  void    | Pošlje napako, če se je kje v postopku zalomilo |

##### Nesamoumevne metode


#### 2.2.36 Ime razreda: ZM - PushNotification

* Razred PushNotification predstavlja mejni razred primera uporabe potisnih obvestil.
##### Atributi

V razredu ne hranimo nobenih atributov.

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| showPushNotification()  |  -  |  void    | Prikaže dodatne informacije o aktivnosti |
| showMessage()  |  -  |  void    | Prikaže sporočilo o uspehu/napaki dodajanja nove aktivnosti |

#### 2.2.37 Ime razreda: ZM - PushNotificationAPI

* Razred PushNotificationAPI predstavlja mejni razred med aplikacijo in APIjem za potisna obvestila.
##### Atributi

V razredu ne hranimo nobenih atributov.

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| requestPushPermission()  |  -  |  void    | Zahteva dovoljenje brskalnika/sistema za prikaz obvestil |


#### 2.2.38 Ime razreda: K - SharingController

* Razred SharingController predstavlja kontrolni razred primera uporabe deljenja na socialnih omrežjih (Facebooku).
##### Atributi

V razredu ne hranimo nobenih atributov.

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| canUserEarnPrivilegedStatus()  |  -  |  boolean    | Vrne ali lahko uporabnik postane priviligiran (prilagojeno sporočilo) |
| startSharingProcess()  |  -  |  void    | Začne postopek deljenja na Facebooku |
| changeUserType() | user_id - String | void | Uporabnika nadgradi v priviligiranega |
| sendError()  |  -  |  void    | Pošlje napako, če je kje v postopku šlo kaj narobe |
| startSharingProccess()  |  -  |  void    | Začne postopek deljenja aplikacije na socialnem omrežju Facebook |
| castUserToPrivliged()  |  -  |  void    | Sprememni tip uporabnika |
| shareAplication()  |  -  |  void    | Deli aplikacijo |

#### 2.2.39 Ime razreda: ZM - Sharing

* Razred Sharing predstavlja mejni razred primera uporabe deljenja na socialnih omrežjih (Facebooku).
##### Atributi

V razredu ne hranimo nobenih atributov.

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| displayMessage()  |  isPrivileged - boolean  |  void    | Prikaže prilagojeno sporočilo o možnosti sharanja (če uporabnik ni priviligiran, se mu izpiše, da lahko s to akcijo postane) |
| displayError()  |  -  |  void    | Prikaže obvestilo o napaki |
| displaySuccess()  |  isPrivileged - boolean  |  void    | Prikaže sporočilo o uspehu in še dodatno obvesti uporabnika o napredovanju v priviligiranega |
| shareApplication() | - | void | Zatevaj deljenje aplikacijo |

#### 2.2.40 Ime razreda: ZM - FacebookAPI

* Razred FacebookAPI predstavlja mejni razred med aplikacijo in Facebook APIjem.
##### Atributi

V razredu ne hranimo nobenih atributov.

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| requestPageSharing()  |  -  |  void    | Od APIja zahteva deljenje dostop do Facebooka in deljenje strani na Facebooku |
| requestFBLogin()  |  -  |  void    | Zahteva prijavo na socialno omrežje Facebook |
| returnResult() | - | boolean | Vrne "true", če je bilo deljenje aplikacije uspešno |


#### 2.2.41 Ime razreda: K - ActivityPriorityController

* Razred ActivityPriorityController predstavlja kontrolni razred primera uporabe spreminjanja prioritet aktivnosti.
##### Atributi

V razredu ne hranimo nobenih atributov.

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| getActivityList()  | user_id - String |  Activity[]   | Pridobi aktivnosti, vezane na uporabnika |
| sortActivityList()  | user_id - String, activityList - Activity[]  |  Activity[]   | Uredi aktivnosti uporabnika glede na prioriteto |
| assertActivityPriorityChange()  |  user_id - String, activity - Activity, priorityLevel - int  |  void    | Spremeni prioriteto aktivnosti |
| sendError()  |  -  |  void    | Pošlje napako, če se je kje v postopku zalomilo |
| setPriority() | user_id - String, activity_id - String, level - int | void | Zahtevaj spremembo prioritete aktivnsoti |
| sendNewList() | user_id - String, activity_id - String | activityList - Activity[] | Vrni posodobljene podatke za aktivnosti |


#### 2.2.42 Ime razreda: ZM - ActivityPriority

* Razred ActivityPriority predstavlja mejni razred primera uporabe spreminjanja prioritet aktivnosti.
##### Atributi

V razredu ne hranimo nobenih atributov.

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| showActivities()  |  user_id - String, activities - Activity[]  |  void    | Prikaže seznam aktivnosti, ter njihove prioritete |
| getPriority()  |  -  |  int  | Pridobi prioriteto aktivnosti, ki jo je izbral uporabnik |
| getActivity()  |  -  |  Activity  | Pridobi aktivnost, ki jo pravkar ureja uporabnik |
| showMessage()  |  -  |  void    | Prikaže sporočilo o uspehu/napaki spreminjanja statusa/izbrisa aktivnosti |
| changePriority() | user_id - String, activity_id - String, level - int | void | Zahtevaj spremembo prioritete aktivnosti |

#### 2.2.43 Ime razreda: K - LogoutController

* Razred LogoutController predstavlja kontrolni razred primera uporabe odjave uporabnika.
#### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| logout()  |  |  void    | Odjava uporabnika |
| sendError()  |  -  |  void    | Pošlje napako, če je kje v postopku šlo kaj narobe |

##### Nesamoumevne metode


#### 2.2.44 Ime razreda: ZM - Logout

* Razred Logout predstavlja mejni razred primera uporabe odjave uporabnika.
##### Atributi

V razredu ne hranimo nobenih atributov.

##### Nesamoumevne metode

| Ime metode     | Imena in tipi parametrov    | Tip rezultata | Pomen (če ni očiten) |
| --------|---------|-------|-------|
| displaySuccessMessage()  |  -  |  void    | Izpiše sporočilo o uspešnosti odjave |
| displayError()  |  -  |  void    | Izpiši morebitno napako |
| logoutRequest()  |  -  |  void    | Sprejme uporabnikovo zahtevo po izpisu |
| logout()  |  user_id - String  |  void    | Sproži izpis uporabnika s to identifikcijsko številko |


## 3. Načrt obnašanja
### 3.1 - Prijava v sistem.
![Diagram zaporedja Prijava v sistem](../img/PrijavaVSis.png)

### 3.1.2 - Prijava v sistem - alternativni tok, ko uporabnik poda napačne podatke.
![Diagram zaporedja Prijava v sistem - alternativni tok](../img/Alternativnilogin.png)

### 3.2 - Registracija v sistem.
![Diagram zaporedja Registracija v sistem](../img/Registracijavsistem.png)

### 3.3 - Sprememba profila.
![Diagram zaporedja Sprememba profila](../img/SpremembaProfila.png)

### 3.4 - Spreminjanje profilov in pravic drugih uporabnikov.
![Diagram zaporedja Spreminjanje profilov in pravic drugih uporabnikov](../img/Spreminjanjeprofilovinpravicdrugihuporabnikov.png)

### 3.5 - Odstranitev uporabnika.
![Diagram zaporedja Odstranitev uporabnika](../img/Odstranitevuporabnika.png)

### 3.6 - Pogled predmetov.
![Diagram zaporedja Pogleda predmetov](../img/DIA-pogledPredmeta.png)

### 3.7 - Dodajanje predmeta.
![Diagram zaporedja Dodajanja predmeta](../img/DIA-dodajanjePredmeta.png)

### 3.8 - Vpis v predmet.
![Diagram zaporedja Vpisa v predmet](../img/DIA-vpisVPredmet.png)

### 3.8.1 - Vpis v predmet - izjemni tok, kjer je uporabnik že vpisan v predmet.
![Diagram zaporedja izjemnega toka pri Vpisu v predmet](../img/DIA-vpisVPredmet-altTok1.png)

### 3.9 - Pogled podrobnosti aktivnosti.
![Diagram zaporedja Pogleda podrobnosti aktivnosti](../img/DIA-pogledPodrobnostiAktivnosti.png)

### 3.10 - Dodajanje aktivnosti.
![Diagram zaporedja Dodajanja aktivnosti](../img/DIA-dodajanjeAktivnosti.png)

### 3.11 - Odstranitev aktivnosti.
![Diagram zaporedja Odstranitev aktivnosti](../img/DIA-odstranitevAktivnosti.png)

### 3.12 - Odločanje o aktivnosti, ki jo je kreiral drug uporabnik.
![Diagram zaporedja Odločanje o aktivnosti](../img/DIA-odlocitevOAktivnosti.png)

### 3.13 - Izpis iz predmeta.
![Diagram zaporedja Izpis iz predmeta](../img/DIA-izpisIzPredmeta.png)

### 3.14 - Prikaz potisnih obvestil
![Diagram zaporedja Izpis iz predmeta](../img/DIA-obvestila.png)

### 3.15 - Nadgradnja na priviligiran uporabnik
![Diagram zaporedja Nadgradnja na priviligiran uporabnik](../img/NadgradnjaNaPrivUpr.png)

### 3.16 - Deljene na družabnem omrežju Facebook.
![Diagram zaporedja Deljene na družabnem omrežju Facebook](../img/A16.png)

### 3.17 - Izbira prioritete za aktivnosti.
![Diagram zaporedja Izbira prioritete za aktivnosti](../img/A17.png)

### 3.18 - Sprememba statusa za aktivnosti
![Diagram zaporedja Sprememba statusa za aktivnosti](../img/A18.png)

### 3.19 - Prikaz motivacijskega citata
![Diagram zaporedja Prikaz motivacijskega citata](../img/quote.png)

### 3.20 - Odjava iz sistema.
![Diagram zaporedja Odjave iz sistema](../img/DIA-odjavaIzSistema.png)
