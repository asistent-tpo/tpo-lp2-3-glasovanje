# Dokument zahtev

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Ljupche Milosheski, Elvisa Alibašić, Maj Bajuk, Zala Erič |
| **Kraj in datum** | Ljubljana, 4. 4. 2019 |

## Povzetek projekta

Naša projekt je spletna aplikacija StraightAs, ki je namenjena olajšavi organizacije časa študentom univerz v Sloveniji.  Želimo implementirati aplikacijo, ki bo uporabnikom (študentom) pomagala pri organizaciji vseh obveznosti na enem mestu – pri nas. To zajema vse od študijskih obveznosti kot so domače naloge in izpiti, do čisto običajnih kot je naročilo k zobozdravniku. Ponuditi želimo nekaj, kar bo temeljilo na principu študenti za študente – vsi lahko objavljajo aktivnosti, povezane s faksom in s tem spodbudijo ali pa ozavestijo sošolce, da je potrebno pripraviti neko rešitev.



## 1. Uvod

Naša aplikacija se bo ukvarjala s problemom upravljanja oziroma organizacije časa. Vsi vemo, da v današnjem času, kjer vse teče hitro, ni lahko obvladovati vseh obveznosti. Menimo, da je ta specifičen problem še posebej pereč v naših letih, ko težko kombiniramo študij s službo, postopnim prehodom v odraslost, zraven pa še s časom za prijatelje. Zato želimo olajšati organizacijo časa vsaj z vidika sprotnih obveznosti. Odločili smo se, da bo to v naši aplikaciji zajemalo obveznosti vse od peljati kolo na popravilo in druženje s prijatelji, do vsega, kar je povezano z akademskimi obveznostmi, kar vključuje vse od domačih nalog, seminarskih nalog, kolokvijev, in ostalih sprotnih obveznosti, do izpitnih rokov, tako ustnih kot pisnih. 

Aplikacija bo imela štiri različne možne uporabnike. Najmanj pravic bo imel neregistrirani uporabnik, navadni registrirani bo imel večino ostalih, nekaj nadgradenj je prepuščenih le za zveste uporabnike, nadzor nad celoto in dostop z administratorskimi pravicami pa bo imel skrbnik.

Aplikacija bo zasnovana tako, da se bo uporabnik v aplikacijo lahko prijavil in se vpisal na določeno fakulteto. Znotraj le-te se bo lahko vpisal v posamezne predmete in si dodal aktivnosti, ki spadajo k predmetom. Imeli bomo tudi aktivnosti, ki ne bodo imele vezave na predmet in slednje bodo zajemale dogodke kot so iti na pošto, poklicati prijatelja; torej vse, kar ni v povezavi s študijskimi obveznostmi. 

Ko bodo aktivnosti enkrat v seznamu, se bodo kljub prvotni razporeditvi po datumu roka lahko prerazporejale glede na nivo prioritete, ki ga bo določil uporabnik sam. Obveznosti bodo imele tudi nastavljive statuse. Določeval jih bo uporabnik sam, glede na lastno presojo kako daleč je z nalogo, oziroma ali jo je opravil. 

Ker želimo poudariti pristop študenti za študente, bo dodajanje vsake aktivnosti, ki je vezana na določen predmet potekala tako, da jo bo študent ob dodanju, imel možnost deliti z vsemi ostalimi vpisanimi na isti predmet. Na ta način bo vsakdo, ki bo aktivnost naredil globalno pri nekem predmetu, s tem opozoril oziroma spodbudil vse vpisane sošolce, da morajo do določenega roka opravit to aktivnost, pa naj bo to domača naloga ali učenje za izpit. Seveda bodo lahko ostali sami upravljali s predlaganimi aktivnostmi, in če bo za njih irelevantna, jo bodo lahko zavrnili ali kasneje izbrisali.

Ker bi želeli vpeljati tudi sistem nagrajevanja zvestih uporabnikov, bi želeli imeli možnost nadgradnje profila, ki bo uporabniku omogočena preprosto z deljenjem aplikacije med svoje prijatelje na socialnih omrežjih, ali po določenem pretečenem času po registraciji. S tem jim bo omogočena funkcionalnost “push notifications” oziroma obvestil o posameznih aktivnostih tudi izven aplikacije.

Kot dodatek bomo v aplikaciji imeli motivacijske citate, ki se bodo vsakodnevno menjali, da bodo vsakič znova študente spodbudili k učenju in trdemu delu. Ob nadgradnji bi želeli dodati tudi funkcionalnost, ki bi omogočala pregled vremena, da bi lahko uporabniki bolj optimalno načrtovali svoje aktivnosti in maksimalno izkoristili lepe dni za druženje in šport ter slabše dni za delo na seminarskih nalogah in učenje.

Poleg teh zahtev, ki se bolj nanašajo na delovanje naše spletne aplikacije, moramo seveda paziti tudi na nefunkcionalne zahteve, kot so skladnost podatkov z uredbo GDPR, in paziti na preprečevanje žaljivih ali vulgarnih izrazov pri dodajanju predmetov, aktivnosti, uporabniških imen v skladu z zakonom o javnem redu in miru. Ker bo aplikacija temeljila na študentskem sodelovanju, je to definitivno ena od nevarnosti. 

Tudi skalabilnost in možnost nadgradnje aplikacije je nekaj potrebnega, zato bo dodelana tako, da bo dodajanje nove funkcionalnosti kar se da preprosto. 

Za varnost se v bazo ne bodo smela shranjevati gesla kot takšna, temveč zgoščene vrednosti pridobljene iz le-tega.

Zaradi obsega obveznosti, ki jih bo aplikacija zajemala (od študijskih do čisto življenjskih), bomo morali omejiti čas nedelovanja za vzdrževanje in podobno na eno uro na teden. Zagotoviti želimo skoraj neprestano delovanje.

Preprečiti je treba tudi izgube podatkov, ki bi pripeljale do resetiranja predmetov oziroma aktivnosti, ki jih ima uporabnik shranjene, saj so tudi ostali odvisni od njih.

Neveljavna stanja kot so na primer pretekli datumi in podobno, morajo biti preprečena in do njih ne sme pripeljati.

Proti koncu izpeljave bo treba poskrbeti, da vse skupaj deluje. V dokumentu predlagamo še kako bi se funkcionalnosti stestirale, da bo aplikacija pripravljena za uporabo.



## 2. Uporabniške vloge

| Uporabniška vloga             | Opis vloge in funkcionalnosti                                                                                                                                                                                                                                                                                                                                                                                   |
|:------------------------------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **Navadni uporabnik** 		    | Ta uporabniška vloga je najbolj osnovna, zato so funkcionalnosti, ki so zanj predvidene tudi tiste najbolj primarne. Navadni uporabnik bo sprva vsak uporabnik, ki bo opravil osnovni proces registracije - bodisi preko navadne prijave, bodisi s pomočjo socialnega omrežja Facebook. Funkcionalnosti, ki so na voljo navadnemu uporabniku seveda vključujejo vse funkcionalnosti neprijavljenega uporabnika, ponujajo pa tudi nekaj dodatnih. Medtem, ko je neprijavljenemu uporabniku omogočen zgolj ogled na aplikaciji ustvarjenih predmetov in v njih vključenih aktivnosti, je prednost navadnega uporabnika poljubno ustvarjanje, dodajanje in brisanje takšnih predmetov ter ustvarjanje, dodajanje, brisanje, izpolnjevanje in sledenje aktivnosti TO-DO seznamu. Uporabnik si lahko svojo domačo stran poljubno personalizira, tj. zapolni z željenimi predmeti. Tovrsten uporabnik si lahko tudi poljubno spreminja uporabniško geslo in fakulteto, v katero je vpisan. Nazadnje je navadnemu uporabniku omogočeno tudi napredovanje v priviligiranega uporabnika.   |                                                                                                                                        |
| **Neprijavljeni uporabnik**      | Neprijavljeni uporabnik je vsak uporabnik, ki še ni opravil zaželjenega procesa registracije ali pa je trenutno odjavljen. To je torej uporabnik, ki je bodisi prvič obiskovalec naše aplikacije, bodisi je odjavljen, ali pa je v "zasebnem" načinu brskalnika slučajno zašel na našo stran. Ker je takšen uporabnik lahko kdorkoli, mu s strani naše aplikacije ni omogočena polna, prilagojena izkušnja, temveč zgolj ogled že obstoječih predmetov, aktivnosti ter raznih dodatkov na strani kot so prikazovanje motivacijskih citatov ter informacij o vremenu. Lahko bi rekli, da ima neprijavljeni uporabnik zgolj pravice za branje, na samo skupnost registriranih uporabnikov aplikacije pa ne more vplivati.                                                                                                               																																																																							   |
| **Priviligirani uporabnik**       | Vsak navadni uporabnik ima možnost postati priviligirani uporabnik, katerega možnosti uporabe gradijo na že obstoječemu naboru funkcionalnosti navadnega uporabnika. Ker si želimo, da uporabniki pripomorejo k samemu razvoju in prepoznavnosti aplikacije, je eden od načinov pridobitve vloge priviligiranega uporabnika delitev uporabe aplikacije na socialnem omrežju Facebook. Drugi način je daljša uporaba aplikacije, namreč najmanj 3 mesece. Z novo vlogo je priviligiranemu uporabniku omogočen prejem obvestil aplikacije, kar mu z opozarjanjem pomaga pri lažjem sledenju bližajočih se aktivnosti na TO-DO seznamu.                                    																																																																																																					       |
| **Skrbnik**                       | Skrbnik aplikacije je uporabnik z administrativnimi pravicami nad aplikacijo. Skrbnik ima na voljo vse funkcionalnosti ostalih uporabnikov, a njegova primarna vloga je nadzor aplikacije. Njegov cilj je ohraniti aplikacijo organizirano in uporabnikom prijazno. Zato je skrbniku omogočeno odstranjevanju zastarelih ali spornih predmetov in aktivnosti, kot tudi nadzor, odstranjevanje ter spreminanje privilegijev registriranih uporabnikov.   																																																																																																																																																							           |


## 3. Slovar pojmov

| Pojem             | Razlaga pojma                                                                                                                                                                                                                                                                                                                                                                                  |
|:------------------------------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **Aktivnost** 		        | S strani uporabnika zamišljena dejavnost, določena z nazivom, datumom, časom, pripadajočim predmetom ter prioriteto oz. nujnostjo. Aktivnosti zaradi zmožnosti poljubnega dodajanja niso izključno vezane na šolske dejavnosti, čeprav so te dejavnosti poglavitne pri rabi aplikacije.  |
| **Predmet**                   | Zajema skupino različnih aktivnosti. S pojmom predmet imamo poglavitno v mislih šolske predmete, ki kot aktivnosti vključujejo obveznosti tega predmeta, a so zaradi poljubnega dodajanja lahko poljubno definirani oz. poimenovani. 
| **Fakulteta**                 | Naziv šolske ustanove, ki ga pri aplikaciji uporabljamo za boljšo razvrstitev in organizacijo različnih predmetov in aktivnosti.
| **Prijavljen uporabnik**    | Pojem, ki zajema tako **navadnega**, **priviligiranega uporabnika** kot tudi **skrbnika**. Uporablja se, ko govorimo o funkcionalnostih navadnega uporabnika, saj so te definirane tudi za priviligiranega uporabnika in skrbnika. Obratno ne velja. 
| **Uporabniško ime**              | Unikatno zaporedje alfanumeričnih znakov, ki enolično določajo uporabnika. Uporabnik ga uporablja za prijavo, saj predstavlja njegovo identiteto. |
| **Geslo**              | Skrivno, samo uporabniku znano zaporedje alfanumeričnih znakov, ki jih uporabnik skupaj s svojim uporabniškim imenom uporablja za vpis. Geslo ni vidno nikomur. |
| **Vnosno polje**              | Element aplikacije, ki ga uporabnik lahko uporablja za vnašanje teksta. Primer uporabe je vpisovanje uporabniškega imena pri registraciji.|
| **Dropdown**              | Element aplikacije, ki ga uporabnik lahko uporablja za izbiro končnega števila možnosti. Uporabnik s klikom nanj sproži prikaz seznama, nakar se pričakuje njegova izbira.|
| **Checkbox**              | Element aplikacije, ki ga lahko uporabnik uporablja za stanji potrditve izbire ali njeno zavrnitev. Checkbox vedno spremlja tudi napis, ki ga nato uporabnik z kljukico potrdi oz. v nasprotnem primeru zavrni.|
| **Prioriteta aktivnosti**         | Po diskretni skali določene stopnje pomembnosti aktivnosti, uporabniku na voljo za izbiro. |
| **Profil uporabnika**              | Posebna, uporabniku prilagojena in namenjena stran, kjer lahko najde svoje podatke podatke, torej uporabniško ime ter fakulteto. |
| **Prijava**              | Proces, kjer nevpisan uporabnik, z vnosom uporabniškega imena in gesla, postane prijavljen uporabnik in mu je s tem omogočena polnejša uporaba aplikacije.|
| **Registracija**              | Proces, kjer potencialni novi uporabnik z izbiro lastnega uporabniškega imena in gesla postane navaden uporabnik. |
| **Odjava**              | Akcija, ki sproži, da prijavljen uporabnik postane neprijavljen. Njegova izkušnja postane manj polna.|
| **Profil**              | Končna množica metapodatkov uporabnika, ki je lahko dinamična skozi potek delovanja sistema.|
| **Push notification**              |Avtomatizirana sporočila poslana uporabniku s strani aplikacije takrat ko aplikacija ni odprta.|
| **Quote**              | Motivacijski citat. |




## 4. Diagram primerov uporabe

![Diagram primerov uporabe](../img/UseCaseDiagram.png)

## 5. Funkcionalne zahteve

### 1) Prijava v sistem


#### Povzetek funkcionalnosti
Uporabnik se lahko prijavi v sistem. 
Na osnovi njegovega uporabniškega imena in gesla sistem ugotovi ali gre za navadnega uporabnika, priviligiranega uporabnika ali skrbnika sistema.


#### Osnovni tok
1. Sistem prikaže dve vnosni polji. Prvo vnosno polje je namenjeno uporabniškemu imenu, drugo pa geslu.
2. Neprijavljen uporabnik vpiše uporabniško ime v prvo vnosno polje.
3. Neprijavljen uporabnik vpiše geslo v drugo vnosno polje.
4. Neprijavljen uporabnik potrdi svojo izbiro.
5. Uporabniku se prikaže domača stran sistema.


#### Alternativni tok(ovi)
**Alternativni tok 1**

1. Sistem prikaže gumb za prijavo z uporabo Facebook računa.
2. Neprijavljen uporabnik klikne na gumb.
3. Neprijavljen uporabnik je preusmerjen na domačo stran.

**Alternativni tok 2**

1. Sistem prikaže dve vnosni polji. Prvo vnosno polje je namenjeno uporabniškemu imenu, drugo pa geslu.
2. Neprijavljen uporabnik napiše napačno uporabniško ime v prvem vnosnem polju ali napačno geslo v drugem vnosnem polju.
3. Neprijavljen uporabnik potrdi svojo izbiro.
4. Sistem prikaže ustrezno sporočilo, da je uporabniško ime ali geslo napačno.


#### Pogoji
Nobenih pogojev ni.


#### Posledice
Vloga neprijavljenega uporabnika se spremeni. Po koncu osnovnega toka, neprijavljen uporabnik postane navadni uporabnik oziroma privilegirani uporabnik ali skrbnik, če ima te pravice.


#### Posebnosti
Realizacija funkcionalnosti zahteva ustrezen dostop do Facebook API-ja.


### 2) Registracija v sistem


#### Povzetek funkcionalnosti
Neprijavljen uporabnik se lahko registrira v sistem.


#### Osnovni tok
1. Sistem prikaže ustrezno besedilo o morebitni registraciji.
2. Neprijavljen uporabnik klikne na besedilo.
3. Sistem prikaže vnosna polja za uporabniško ime in geslo. Hkrati prikaže dropdown za izbiro fakultete.
4. Neprijavljen uporabnik vpiše ustrezne podatke in izbere fakulteto.
5. Neprijavljen uporabnik potrdi vpisane podatke.
6. Sistem prikaže ustrezno sporočilo o uspešni registraciji.


#### Alternativni tok(ovi)
**Alternativni tok 1**

1. Sistem prikaže ustrezno besedilo o morebitni registraciji.
2. Neprijavljen uporabnik klikne na to besedilo.
3. Sistem prikaže vnosna polja za uporabniško ime in geslo. Hkrati se prikaže dropdown za izbiro fakultete.
4. Neprijavljen uporabnik pusti vsaj eno izmed vnosnih polj prazno.
5. Neprijavljen uporabnik potrdi napisane podatke.
6. Sistem prikaže sporočilo, da vnešeni podatki niso popolni.

**Alternativni tok 2**

1. Sistem prikaže ustrezno besedilo o morebitni registraciji.
2. Neprijavljen uporabnik klikne na to besedilo.
3. Sistem prikaže vnosna polja za uporabniško ime in geslo. Hkrati se prikaže dropdown za izbiro fakultete.
4. Neprijavljen uporabnik izpolni obe vnosni polji vendar ne izbere fakultete s spustnega sezanama.
5. Neprijavljen uporabnik potrdi napisane podatke.
6. Sistem prikaže ustrezno sporočilo o uspešni registraciji.

**Alternativni tok 3**

1. Sistem prikaže ustrezno besedilo o morebitni registraciji.
2. Neprijavljen uporabnik klikne na to besedilo.
3. Sistem prikaže vnosna polja za uporabniško ime in geslo. Hkrati se prikaže dropdown box za izbiro fakultete.
4. Neprijavljen uporabnik izpolni obe vnosni polji, fakulteto izbere ali ne, vendar pa željeno uporabniško ime že obstaja.
5. Neprijavljen uporabnik potrdi napisane podatke.
6. Sistem prikaže ustrezno sporočilo, da uporabniško ime že obstaja v sistemu.

**Alternativni tok 4**

1. Sistem prikaže ustrezno besedilo o morebitni registraciji.
2. Neprijavljen uporabnik klikne na to besedilo.
3. Sistem prikaže vnosna polja za uporabniško ime in geslo. Hkrati se prikaže dropdown box za izbiro fakultete.
4. Neprijavljen uporabnik popolni oba vnosna polja in lahko izbere fakulteto, pri čemer vsebina vnosnih polj ni alfanumerična.
5. Neprijavljen uporabnik potrdi napisane podatke.
6. Sistem prikaže ustrezno sporočilo, da morata uporabniško ime in geslo vsebovati le črke in številke.



#### Pogoji
Nobenih pogojev ni.


#### Posledice
Po izvedbi se uporabnik registrira v sistem, se pravi so njegovi podatki zapisani v bazo.


#### Posebnosti
Posebnosti ni.



### 3) Sprememba profila


#### Povzetek funkcionalnosti
Navadni uporabnik, privilegirani uporabnik ali skrbnik lahko spremenijo svojo geslo ali fakulteto, kjer študirajo.


#### Osnovni tok
1. Sistem prikaže ustrezno besedilo o morebitni spremembi profila na gumbu "Moj profil" na domači strani.
2. Navadni uporabnik, privilegirani uporabnik ali skrbnik klikne na gumb.
3. Sistem prikaže 3 vnosna polja za uprabniško ime, geslo in dropdown za izbiro fakultete.
4. Navadni uporabnik, privilegirani uporabnik ali skrbnik vnese nove podatke in potrdi spremembo.
5. Sistem preveri ustreznost podatkov in sporoči, da je sprememba profila bila uspešna.


#### Alternativni tokovi

**Alternativni tok 1**

1. Sistem prikaže ustrezno besedilo o morebitni spremembi profila na gumbu "Moj profil" na domači strani.
2. Navadni uporabnik, privilegirani uporabnik ali skrbnik klikne na gumb.
3. Sistem prikaže 3 vnosna polja za uprabniško ime, geslo in dropdown za izbiro fakultete.
4. Navadni uporabnik, privilegirani uporabnik ali skrbnik pusti polje za spremembo uporabniškega imena prazno, ostale podatke ustrezno izpolni in potrdi spremembo.
5. Sistem prikaže sporočilo, da mora biti polje za uporabniško ime izpolnjeno.

**Alternativni tok 2**

1. Sistem prikaže ustrezno besedilo o morebitni spremembi profila na gumbu "Moj profil" na domači strani.
2. Navadni uporabnik, privilegirani uporabnik ali skrbnik klikne na gumb.
3. Sistem prikaže 3 vnosna polja za uprabniško ime, geslo in dropdown za izbiro fakultete.
4. Navadni uporabnik, privilegirani uporabnik ali skrbnik napačno izpolni polje za uporabniškega imena, ostale podatke ustrezno izpolni in potrdi spremembo.
5. Sistem prikaže sporočilo, da mora biti polje z uporabniškim imenom veljavno / se ujemati z že obstoječim.

**Alternativni tok 3**

1. Sistem prikaže ustrezno besedilo o morebitni spremembi profila na gumbu "Moj profil" na domači strani.
2. Navadni uporabnik, privilegirani uporabnik ali skrbnik klikne na gumb.
3. Sistem prikaže 3 vnosna polja za uprabniško ime, geslo in dropdown za izbiro fakultete.
4. Navadni uporabnik, privilegirani uporabnik ali skrbnik pusti polje za geslo prazno ali ne izbere fakultete. Polje za potrdilo uporabniškega imena je veljavno. Nato potrdi spremembo.
5. Sistem preveri ustreznost podatkov in sporoči, da je sprememba profila bila uspešna.

**Alternativni tok 4**

1. Sistem prikaže ustrezno besedilo o morebitni spremembi profila na gumbu "Moj profil" na domači strani.
2. Navadni uporabnik, privilegirani uporabnik ali skrbnik klikne na gumb.
3. Sistem prikaže 3 vnosna polja za uprabniško ime, geslo in dropdown za izbiro fakultete.
4. Navadni uporabnik, privilegirani uporabnik ali skrbnik napiše tekst, ki ni alfanumeričen, v eno izmed vnosnih polj in potrdi spremembo.
5. Sistem prikaže sporočilo, da morajo biti podatki alfanumerični.



#### Pogoji
Pogoj te funkcionalnosti je le to, da je uporabnik prijavljen v sistem.


#### Posledice
Po izvedbi osnovnega toka se podatki v podatkovni bazi spremenijo. Posledično se uporaba sistema za tega uporabnika lahko spremeni.


#### Posebnosti
Posebnosti ni.



### 4) Spreminjanje profilov in pravic drugih uporabnikov


#### Povzetek funkcionalnosti
Skrbnik lahko spremeni profil ali pravice navadnih ali privilegiranih uporabnikov.


#### Osnovni tok
1. Sistem prikaže ustrezno besedilo o spremembi profila drugih uporabnikov.
2. Skrbnik izbere to funkcionalnost.
3. Sistem prikaže seznam uporabnikov in mu ponudi možnost po iskanju željenega uporabnika.
4. Skrbnik izbere željenega uporabnika.
5. Sistem prikaže profil željenega uporabnika in mu ponudi možnost urejanja.
6. Skrbnik spremeni podatke uporabnika. 
7. Skrbnik potrdi spremembo.
8. Sistem prikaže sporočilo, da je bila sprememba uspešna.

#### Alternativni tokovi

**Alternativni tok 1**

1. Sistem prikaže ustrezno besedilo o spremembi profila drugih uporabnikov.
2. Skrbnik izbere to funkcionalnost.
3. Sistem prikaže seznam uporabnikov in mu ponudi možnost po iskanju željenega uporabnika.
4. Skrbnik izbere željenega uporabnika.
5. Sistem prikaže profil željenega uporabnika in mu ponudi možnost urejanja.
6. Skrbnik spremeni geslo / uporabniško ime. Doda geslo / up. ime, ki vsebuje znake, ki niso alfanumerični.
7. Sistem prikaže sporočilo, da sprememba ni bila uspešna.



#### Pogoji
Pogoj tej aktivnosti je, da je uporabnik prijavljen v sistem kot skrbnik.


#### Posledice
Uporabnik, katerega profila je bil spremenjen, bo odjavljen iz sistema. Poleg tega se stanje v podatkovni bazi ustrezno spremeni.


#### Posebnosti
Posebnosti ni.



### 5) Brisanje uporabnika


#### Povzetek funkcionalnosti
Skrbnik lahko izbriše navadnega uporabnika ali privilegiranega uporabnika.


#### Osnovni tok

1. Sistem prikaže ustrezno besedilo o brisanju profila drugih uporabnikov.
2. Skrbnik izbere to funkcionalnost.
3. Sistem prikaže 2 vnosni polji. Prvo je za uporabniško ime uporabnika, ki ga skrbnik želi brisati, drugo pa za skrbnikovo geslo.
4. Skrbnik ustrezno izpolni obe vnosni polji.
5. Skrbnik potrdi napisane podatke.
6. Sistem prikaže sporočilo, da je uporabnik bil izbrisan.


#### Alternativni tok(ovi)

**Alternativni tok 1**

1. Sistem prikaže ustrezno besedilo o brisanju profila drugih uporabnikov.
2. Skrbnik izbere to funkcionalnost.
3. Sistem prikaže 2 vnosni polji. Prvo je za uporabniško ime uporabnika, ki ga skrbnik želi brisati, drugo pa za skrbnikovo geslo.
4. Skrbnik neustrezno izpolni obe vnosni polji.
5. Skrbnik potrdi napisane podatke.
6. Sistem prikaže sporočilo o napaki.

**Alternativni tok 2**

1. Sistem prikaže ustrezno besedilo o brisanju profila drugih uporabnikov.
2. Skrbnik izbere to funkcionalnost.
3. Sistem prikaže 2 vnosni polji. Prvo je za uporabniško ime uporabnika, ki ga skrbnik želi brisati, drugo pa za skrbnikovo geslo.
4. Skrbnik vnese svoje podatke.
5. Skrbnik potrdi napisane podatke.
6. Sistem prikaže sporočilo o napaki.

#### Pogoji
Pogoj te aktivnosti je, da je uporabnik prijavljen v sistem kot skrbnik.


#### Posledice
Uporabnik, katerega profila je bil izbrisan, bo odjavljen iz sistema. Poleg tega se stanje v podatkovni bazi ustrezno spremeni.


#### Posebnosti
Posebnosti ni.



### 6) Pogled predmetnika


#### Povzetek funkcionalnosti
Neprijavljeni uporabnik, navaden uporabnik, privilegirani uporabnik ali skrbnik lahko pogledajo predmete.

#### Osnovni tok
1. Navadni uporabnik, privilegirani uporabnik ali skrbnik gre na domačo stran in pogleda predmete. Sistem prikaže najprej predmete, v katerih je uporabnik vpisan. Nato sledijo vse ostale predmete.

#### Alternativni tok

**Alternativni tok 1**

1. Neprijavljeni uporabnik gre na domačo stran. Sistem prikaže vse predmete.

#### Pogoji
Nobenih pogojev ni.

#### Posledice
Sistem prikaže vse predmete.

#### Posebnosti
Posebnosti ni.


### 7) Dodajanje predmeta


#### Povzetek funkcionalnosti
Navadni uporabnik, privilegirani uporabnik ali skrbnik lahko dodajo nov predmet, ki je povezan s fakulteto in še ne obstaja.


#### Osnovni tok
1. Sistem prikaže ustrezno besedilo o morebitnem dodajanju predmetov.
2. Navadni uporabnik, privilegirani uporabnik ali skrbnik izbere to funkcionalnost.
3. Sistem prikaže eno vnosno polje, ki predstavljalo ime predmeta. Polega tega se prikaže dropdown za izbiro fakultete, na kateri se ta predmet izvaja.
4. Uporabnik ustrezno izpolni vnosno polje ter izbere fakulteto.
5. Uporabnik potrdi napisane podatke.
6. Sistem prikaže sporočilo, da je bil predmet uspešno dodan.


#### Alternativni tok(ovi)
**Alternativni tok 1**

1. Sistem prikaže ustrezno besedilo o morebitnem dodajanju predmetov.
2. Navadni uporabnik, privilegirani uporabnik ali skrbnik izbere to funkcionalnost.
3. Sistem prikaže eno vnosno polje, ki predstavljalo ime predmeta. Polega tega se prikaže dropdown za izbiro fakultete, na kateri se ta predmet izvaja.
4. Uporabnik ustrezno izpolni vnosno polje vendar ne izbere fakultete.
5. Uporabnik potrdi napisane podatke.
6. Sistem prikaže sporočilo o napaki.

**Alternativni tok 2**

1. Sistem prikaže ustrezno besedilo o morebitnem dodajanju predmetov.
2. Navadni uporabnik, privilegirani uporabnik ali skrbnik izbere to funkcionalnost.
3. Sistem prikaže eno vnosno polje, ki predstavljalo ime predmeta. Polega tega se prikaže dropdown za izbiro fakultete, na kateri se ta predmet izvaja.
4. Uporabnik ustrezno izpolni vnosno polje ter izbere fakulteto, vendar predmet na tej fakulteti že obstaja.
5. Uporabnik potrdi napisane podatke.
6. Sistem ugotovi, da predmet že obstaja.
7. Sistem prikaže sporočilo o napaki.

**Alternativni tok 3**

1. Sistem prikaže ustrezno besedilo o morebitnem dodajanju predmetov.
2. Navadni uporabnik, privilegirani uporabnik ali skrbnik izbere to funkcionalnost.
3. Sistem prikaže eno vnosno polje, ki predstavljalo ime predmeta. Polega tega se prikaže dropdown za izbiro fakultete, na kateri se ta predmet izvaja.
4. Uporabnik izpolni vnosno polje ter izbere fakulteto, vendar besedilo v polju ni iz alfanumeričnih znakov.
5. Uporabnik potrdi napisane podatke.
6. Sistem prikaže sporočilo, da mora biti ime predmeta alfanumerično.


#### Pogoji
Samo navadni uporabnik, privilegirani uporabnik ali skrbnik lahko dodajajo nov predmet.


#### Posledice
V podatkovno bazo se doda nov predmet. Uporabnik, ki je dodal predmet, se avtomatično vpiše vanj.


#### Posebnosti
Posebnosti ni.




### 8) Vpis v predmet


#### Povzetek funkcionalnosti
Navadni uporabnik, privilegirani uporabnik ali skrbnik se lahko vpišejo v že obstoječi predmet.


#### Osnovni tok
1. Navadni uporabnik, privilegirani uporabnik ali skrbnik iz domače strani izbere predmet, v katerga ni vpisan.
2. Pritisne gumb za vpis v predmet.
3. Vpis je bil uspešen in stran se osveži.

#### Alternativni tok(ovi)
**Alternativni tok 1**

1. Navadni uporabnik, privilegirani uporabnik ali skrbnik iz domače strani izbere predmet, v katerga ni vpisan.
2. Pritisne gumb za vpis v predmet.
3. Sistem prikaže sporočilo, da je uporabnik že vpisan v predmetu.


#### Pogoji
Samo navadni uporabnik, privilegirani uporabnik ali skrbnik se lahko vpiše v predmet.


#### Posledice
Stran se osveži in domača stran se dodatno personalizira.


#### Posebnosti
Nobenih posebnosti ni.



### 9) Pregled podrobnosti aktivnosti


#### Povzetek funkcionalnosti
Navadni uporabnik, privilegirani uporabnik ali skrbnik lahko vidi podrobnosti neke svoje aktivnosti.


#### Osnovni tok
1. Sistem prikaže seznam aktivnosti navadnega uporabnika, privilegiranega uporabnik ali skrbnika.
2. Navadni uporabnik, privilegirani uporabnik ali skrbnik izbere neko aktivnost iz seznama.
3. Sistem prikaže naziv aktivnosti, fakultete, s katero je povezana, kdaj je bila dodana v seznam, rok do katerega bi jo uporabnik rad končal, in gumb za brisanje aktivnosti.

#### Alternativni tok
Alternativnih tokov ni.


#### Pogoji
Samo navadni uporabnik, privilegirani uporabnik ali skrbnik lahko pregledajo podrobnosti svojih aktivnosti.


#### Posledice
Sistem izpiše podatke aktivnosti.


#### Posebnosti
Posebnosti ni.



### 10) Dodajanje aktivnosti


#### Povzetek funkcionalnosti
Navadni uporabnik, privilegirani uporabnik ali skrbnik lahko doda aktivnost, ki je lahko vezana za nek predmet vendar ne nujno. Če je, jo doda za vse uporabnike, sicer je samo zanj.


#### Osnovni tok
1. Sistem prikaže domačo stran na kateri uporabnik klikne na ikono za nastavitve dialoga aktivnosti.
2. Sistem mu ponudi možnost dodajanja aktivnosti in uporabnik jo izbere.
3. Sistem zahteva podrobne podatke o novi aktivnosti (ime, trajanje in fakulteto), ter ponudi možnost predlaganja nove aktivnosti vsem ostalim uporabnikom, ki so vpisani v predmet.
4. Uporabnik pravilno izpolni vse podatke.
5. Sistem prikaže sporočilo, da je aktivnost bila uspešno dodana.

#### Alternativni tok
**Alternativni tok 1**


1. Sistem prikaže domačo stran na kateri uporabnik klikne na ikono za nastavitve dialoga aktivnosti.
2. Sistem mu ponudi možnost dodajanja aktivnosti in uporabnik jo izbere.
3. Sistem zahteva podrobne podatke o novi aktivnosti (ime, trajanja in fakulteto), ter ponudi možnost predlaganja nove aktivnosti vsem ostalim uporabnikom, ki so vpisani v predmet.
4. Razen fakultete so podatki ustrezno izpolnjeni.
5. Sistem prikaže sporočilo, da je bila aktivnost uspešno dodana.

**Alternativni tok 2**

1. Sistem prikaže domačo stran na kateri uporabnik klikne na ikono za nastavitve dialoga aktivnosti.
2. Sistem mu ponudi možnost dodajanja aktivnosti in uporabnik jo izbere.
3. Sistem zahteva podrobne podatke o novi aktivnosti (ime, trajanje in fakulteto), ter ponudi možnost predlaganja nove aktivnosti vsem ostalim uporabnikom, ki so vpisani v predmet.
4. Uporabnik ne izpolni imena aktivnosti.
5. Sistem prikaže sporočilo, da mora aktivnost imeti ime.

**Alternativni tok 3**

1. Sistem prikaže domačo stran na kateri uporabnik klikne na ikono za nastavitve dialoga aktivnosti.
2. Sistem mu ponudi možnost dodajanja aktivnosti in uporabnik jo izbere.
3. Sistem zahteva podrobne podatke o novi aktivnosti (ime, trajanja in fakulteto), ter ponudi možnost predlaganja nove aktivnosti vsem ostalim uporabnikom, ki so vpisani v predmet.
4. Uporabnik pravilno izpolni vse podatke, pri čemer ime aktivnosti ni alfanumerično.
5. Sistem prikaže sporočilo, da mora biti ime aktivnosti alfanumerično.


#### Pogoji
Samo navadni uporabnik, privilegirani uporabnik ali skrbnik lahko dodajajo aktivnosti.


#### Posledice
Stran se osveži. 


#### Posebnosti
Posebnosti ni.



### 11) Odstranitev aktvinosti


#### Povzetek funkcionalnosti
Navadni uporabnik, privilegirani uporabnik ali skrbnik lahko odstranijo aktivnost iz svojega seznama aktivnosti.


#### Osnovni tok
1. Sistem na domači strani prikaže seznam aktivnosti prijavljenega uporabnika.
2. Navadni uporabnik, privilegirani uporabnik ali skrbnik izbere neko aktivnost iz seznama.
3. Sistem izpiše naziv aktivnosti, fakulteta s katero je povezana, kdaj je bila dodana v seznamu, rok do katerega uporabnik bi jo rad končal, prioriteto aktivnosti, status aktivnosti, če je upešno ali neuspešno končana, če je rok aktivnosti potekel in gumb za brisanje aktivnosti.
4. Navadni uporabnik, privilegirani uporabnik ali skrbnik pritisne na gumb za brisanje aktivnosti.
5. Navadni uporabnik, privilegirani uporabnik ali skrbnik potrdi svojo izbiro.
6. Sistem prikaže sporočilo, da je aktivnost bila izbrisana.


#### Alternativni tok(ovi)
Alternativnih tokov ni.

#### Pogoji
Samo navadni uporabnik ali privilegirani uporabnik lahko izbrišejo aktivnost iz svojega seznama.

#### Posledice
Aktivnost ne bo več vidna v seznamu aktivnosti za navadnega uporabnika, privilegiranega uporabnia ali skrbnika.


#### Posebnosti
Posebnosti ni.



### 12) Odločitev o aktivnosti drugih uporabnikov


#### Povzetek funkcionalnosti
Navadni uporabnik, privilegirani uporabnik ali skrbnik se lahko odloči o aktivnosti, ki jo je kreiral drug uporabnik, torej jo lahko sprejme ali zavrne.


#### Osnovni tok

1. Sistem na zaslonu prikaže novo aktivnosti, ki je vezana za predmet, ki ga uporabnik ima v seznamu svojih predmetov. Prikažejo se tudi podatki o aktivnosti in možnost sprejema / zavrnitve nove aktivnosti.
2. Navadni uporabnik, privilegirani uporabnik ali skrbnik sprejeme aktivnost.
3. Sistem prikaže ustrezno sporočilo, da je aktivnost bila sprejeta.


#### Alternativni tok
**Alternativni tok 1**

1. Sistem na zaslonu prikaže novo aktivnosti, ki je vezana za predmet, ki ga uporabnik ima v seznamu svojih predmetov. Prikažejo se tudi podatki o aktivnosti in možnost sprejema / zavrnitve nove aktivnosti.
2. Navadni uporabnik, privilegirani uporabnik ali skrbnik zavrne aktivnost.
3. Sistem prikaže ustrezno sporočilo, da je aktivnost bila zavrnjena.

#### Pogoji
Navadni uporabnik, privilegirani uporabnik ali skrbnik mora biti prijavljen in vpisan v predmet.


#### Posledice
Posodobitev seznama aktivnosti uporabnika.


#### Posebnosti
Posebnosti ni.



### 13) Izpis iz predmeta


#### Povzetek funkcionalnosti
Navadni uporabnik, privilegirani uporabnik ali skrbnik ima možnost izpisa iz predmetov, v katere je vpisan.


#### Osnovni tok
1. Na domači strani se prikaže seznam vseh predmetov.
2. Navadni uporabnik, privilegirani uporabnik ali skrbnik klikne na enega od prikazanih.
2. Navadnemu uporabniku, privilegiranemu uporabniku ali skrbniku se odpre popup profila predmeta.
3. Na profilu predmeta klikne gumb za izpis iz predmeta.
4. Sistem zahteva potrditev / prreklic odločitve.
5. Navadni uporabnik, privilegirani uporabnik ali skrbnik potrdi odločitev.
6. Navadni uporabnik, privilegirani uporabnik ali skrbnik je izpisan iz predmeta.


#### Alternativni tok(ovi)
Alternativnih tokov ni.

#### Pogoji
Navadni uporabnik, privilegirani uporabnik ali skrbnik mora biti prijavljen in vpisan v predmet iz katerega se izpisuje.


#### Posledice
Predmet se izbriše iz predmetnika uporabnika, hkrati tudi vse aktivnosti vezane za ta predmet.


#### Posebnosti
Posebnosti ni.




### 14) Opomniki izven aplikacije - "Push notifications"


#### Povzetek funkcionalnosti
Privilegirani uporabnik ima možnost nastavitve opomnikov o roku določene aktivnosti.


#### Osnovni tok
1. Sistem privilegiranemu uporabniku pošlje opomnik izven aplikacije.


#### Pogoji
To možnost ima le privilegirani uporabnik, ki je vpisan v predmet in ima na seznamu aktivnosti to aktivnost.


#### Posledice
Uporabnikom bo del aplikacije viden tudi takrat ko aplikacija ni odprta. 


#### Posebnosti
Za implementacijo te funkcionalnosti potrebujemo sistem, ki nam omogoča interakcijo z uporabnikom tudi izven uporabe aplikacije.




### 15) Nadgradnja na "Privilegiran uporabnik"


#### Povzetek funkcionalnosti
Navadni uporabnik ima možnost nadgradnje na privilegiranega uporabnika, ki jo doseže z deljenjem uporabe strani na socialnem omrežju Facebook. Tudi priviligirani uporabnik in skrbnit imata možnost deljenja, a se njihov status ne bo spremenil, saj imata že vse funkcionalnosti priviligiranega uporabnika. Drug način doseganja je določen čas uporabe.


#### Osnovni tok
1. Sistem navadnemu uporabniku ponudi gumb za deljenje aplikacije na Facebook-u.
2. Navadni uporabnik deli aplikacijo / link do aplikacije.
3. Navadni uporabnik dobi pravice privilegiranega uporabnika.


#### Alternativni tokovi
**Alternativni tok 1**
1. Uporabnik se registrira.
2. Po določenem času (3 mesci) se nagradi uporabnika z dodajanjem značke privilegirani.

#### Pogoji
Navadni uporabnik mora biti prijavljen.


#### Posledice
Navadni uporabnik postane privilegirani in lahko posledično koristi dodatne ugodnosti, kot so na primer Push notifications.


#### Posebnosti
Posebnosti ni.




### 16) Deljenje na socialnem omrežju Facebook


#### Povzetek funkcionalnosti
Navadni uporabnik, neprijavljeni uporabnik, privilegirani uporabnik ali skrbnik imajo možnost deljenja aplikacije na družabnem omrežju Facebook.


#### Osnovni tok
1. Sistem uporabniku ponudi gumb za deljenje aplikacije na Facebooku.
2. Uporabnik deli aplikacijo/link do aplikacije s klikom na gumb.


#### Pogoji
Uporabnik mora imeti Facebook račun.


#### Posledice
Če uporabnik še nima status privilegiranega, sedaj postane to.


#### Posebnosti
Posebnosti ni.




### 17) Izbira prioritete za aktivnosti


#### Povzetek funkcionalnosti
Navadni uporabnik, privilegirani uporabnik ali skrbnik ima možnost sortiranja svojih aktivnosti glede na prioriteto, ki si jo sam določi. Privzeto so sortirane po roku.


#### Osnovni tok
1. Sistem prikaže domačo stran na kateri uporabnik klikne na ikono za nastavitve dialoga aktivnosti.
2. Sistem mu ponudi možnost urejanja prioritet aktivnosti in jo uporabnik izbere.
3. Uporabnik določi prioritete.
4. Sistem sortira aktivnosti po prioriteti.



#### Alternativni tokovi
Alternativnih tokov ni.



#### Pogoji
Uporabnik mora biti prijavljen.


#### Posledice
Seznam aktivnosti je prilagojen vsakemu posameznemu uporabniku.


#### Posebnosti
Posebnosti ni.



### 18) Sprememba statusa aktivnosti


#### Povzetek funkcionalnosti
Navadni in privilegirani uporabniki imajo možnost spremembe statusa na svojih aktivnosti glede na to ali je opravljena ali ne.


#### Osnovni tok
1. Sistem prikaže seznam aktivnosti.
2. Navadni uporabnik, privilegirani uporabnik ali skrbnik klikne na določeno aktivnost. Odpre se mu dialog s podrobnostmi aplikacije, hkrati mu sistem ponuja možnost urejanja podatkov.
3. Navadni uporabnik, privilegirani uporabnik ali skrbnik spremeni status in potrdi spremembo.


#### Alternativni tokovi
Alternativnih tokov ni.

#### Pogoji
Uporabnik mora bit prijavljen in imeti aktivnost v seznamu aktivnosti.


#### Posledice
Sprememba statusa aktivnosti.


#### Posebnosti
Posebnosti ni.




### 19) Vremenska napoved


#### Povzetek funkcionalnosti
Navadni uporabnik, privilegirani uporabnik ali skrbnik ima možnost videti vreme in si prerazporediti (znižati) prioriteto na aktivnostih glede na vreme po lastni presoji.


#### Osnovni tok
1. Sistem na domači strani prikaže vremensko napoved.


#### Alternativni tokovi
Alternativnih tokov ni.


#### Pogoji
Uporabnik mora biti prijavljen in mora dovoliti aplikaciji dostop do njegove geolokacije.


#### Posledice
Aktivnosti imajo primerno manjšo ali večjo prioriteto glede na uporabnikovo presojo.


#### Posebnosti
Za implementacijo te funkcionalnosti potrebujemo zunanji API.



### 20) Prikaz motivacijskega citata


#### Povzetek funkcionalnosti
Navadnemu uporabniku, privilegiranemu uporabniku ali skrbniku se za motivacijo prikazujejo znani citati o učenju, trdem delu in uspehu.


#### Osnovni tok
1. Sistem na domači strani prikaže motivacijske citate.


#### Pogoji
Uporabnik mora biti prijavljen.


#### Posledice
Uporabnik je motiviran in pripravljen na delo.


#### Posebnosti
Za implementacijo te funkcionalnosti potrebujemo zunanji API.



### 21) Odjava iz sistema


#### Povzetek funkcionalnosti
Navadni uporabnik, privilegirani uporabnik ali skrbnik se lahko odjavi iz sistema.


#### Osnovni tok
1. Navadni uporabnik, privilegirani uporabnik ali skrbnik na domači strani klikne na gumb "Moj profil", ki mu ponudi možnost odjave iz sistema, katero uporabnik izbere.
2. Navadni uporabnik, privilegirani uporabnik ali skrbnik potrdi zahtevo


#### Alternativni tok
**Alternativni tok 1**

1. Seja poteče.
2. Uporabnika se odjavi iz sistema.


#### Pogoji
Uporabnik mora biti prijavljen.


#### Posledice
Uporabnika sedaj obravnavamo kot neprijavljenega.


#### Posebnosti
Posebnosti ni.


#### Prioritete identificiranih funkcionalnosti

| Funkcionalna zahteva             | Prioriteta  | 
|:------------------------------|:--------------|
|	Prijava v sistem	|	MUST	|
|	Registracija v sistem	|	MUST	|
|	Sprememba profila	|	COULD	|
|	Spreminjanje profilov in pravic	|	COULD	|
|	Brisanje uporabnikov	|	MUST	|
|	Pogled predmetnika	|	MUST	|
|	Dodajanje predmeta	|	MUST	|
|	Vpis v predmet	|	MUST	|
|	Pogled podrobnsti aktivnosti	|	MUST	|
|	Dodajanje aktivnosti	|	MUST	|
|	Odstranitev aktivnosti	|	MUST	|
|	Odločitev o aktivnosti novih uporabnikov	|	SHOULD	|
|	Izpis iz predmeta	|	MUST	|
|	Push notifications	|	WOULD	|
|	Nadgradnja na priviligiran uporabnik	|	WOULD	|
|	Delenje na družabnem omrežju Facebook	|	COULD	|
|	Izbira prioritete aktivnosti	|	COULD	|
|	Sprememba statusa aktivnosti	|	MUST	|
|	Vremenska napoved	|	WOULD	|
|	Prikaz motivacijskega citata	|	SHOULD	|
|	Odjava iz sistema	|	MUST	|



#### Sprejemni testi

| Funkcija, ki se testira | Začetno stanje sistema | Vhod | Pričakovani rezultat |
| :---------------------- | :--------------------- | :--- | :------------------ |
| Prijava v sistem | Uporabnik je na domači strani in ni prijavljen. Torej je v vlogi neprijavljenega uporabnika. | Neprijavljeni uporabnik se prijavlja s pravilnim geslom. | Neprijavljeni uporabnik se je uspešno prijavil in njegova vloga se je ustrezno spremenila, v skladu z njegovimi pravicami. |
| Prijava v sistem | Uporabnik je na domači strani in ni prijavljen. Torej je v vlogi neprijavljenega uporabnika. | Neprijavljeni uporabnik se prijavlja s Facebook računom. | Neprijavljeni uporabnik se je uspešno prijavil in njegova vloga se je ustrezno spremenila, v skladu z njegovimi pravicami. |
| Prijava v sistem | Uporabnik je na domači strani in ni prijavljen. Torej je v vlogi neprijavljenega uporabnika. | Neprijavljeni uporabnik se prijavlja z napačnim geslom. | Neprijavljeni uporabnik se je uspešno prijavil v sistem in dobi ustrezno sporočilo. |
| Registracija v sistem | Uporabnik je na domači strani in ni prijavljen. Torej je v vlogi neprijavljenega uporabnika. | Neprijavljeni uporabnik se želi registrirati v sistem, pri čemer ustrezno izpolni vse, kar sistem zahteva. | Neprijavljeni uporabnik se je uspešno registriral. Sistem prikaže ustrezno sporočilo. Stran, na kateri se nahaja, se ne spremeni. Torej, ostane pri registraciji. |
| Registracija v sistem | Uporabnik je na domači strani in ni prijavljen. Torej je v vlogi neprijavljenega uporabnika. | Neprijavljeni uporabnik se želi registrirati v sistem, pri čemer pusti vsaj eno izmed vnosnih polj prazno. | Neprijavljeni uporabnik se ni registral. Sistem prikaže ustrezno sporočilo. Stran, na kateri se nahaja, se ne spremeni. Torej, ostane pri registraciji. |
| Registracija v sistem | Uporabnik je na domači strani in ni prijavljen. Torej je v vlogi neprijavljenega uporabnika. | Neprijavljeni uporabnik se želi registrirati v sistem, pri čemer ustrezno izpolni vse, kar sistem zahteva, razen fakultete. | Neprijavljeni uporabnik se je uspešno registriral. Sistem prikaže ustrezno sporočilo o tem. Stran, na kateri se nahaja, se ne spremeni. Torej, ostane pri registraciji. |
| Registracija v sistem | Uporabnik je na domači strani in ni prijavljen. Torej je v vlogi neprijavljenega uporabnika. | Neprijavljeni uporabnik se želi registrirati v sistem, pri čemer ustrezno izpolni vse, kar sistem zahteva in napiše uporabniško ime, ki že obstaja. | Neprijavljeni uporabnik se ni registral. Sistem prikaže ustrezno sporočilo. Stran, na kateri se nahaja, se ne spremeni. Torej, ostane pri registraciji. |
| Registracija v sistem | Uporabnik je na domači strani in ni prijavljen. Torej je v vlogi neprijavljenega uporabnika. | Neprijavljeni uporabnik se želi registrirati v sistem, pri čemer izpolni vse, kar sistem zahteva pri čemer besedilo, vpisano v vsaj eno izmed vnosnih polj, ni alfanumerično. | Neprijavljeni uporabnik se ni registral. Sistem prikaže ustrezno sporočilo. Stran, na kateri se nahaja, se ne spremeni. Torej, ostane pri registraciji. |
| Sprememba profila | Uporabnik je na domači strani in je prijavljen. Torej je v vlogi navadnega uporabnika, privilegiranega uporabnika ali skrbnika. | Navadni uporabnik, privilegirani uporabnik ali skrbnik ustrezno izpolni vse, kar sistem zahteva. | Navadni uporabnik, privilegirani uporabnik ali skrbnik je uspešno spremenil svoj profil. Sistem izpiše ustrezno sporočilo. Stran, na kateri se nahaja, se ne spremeni. Torej, ostane pri spremembi profila. |
| Sprememba profila | Uporabnik je na domači strani in je prijavljen. Torej je v vlogi navadnega uporabnika, privilegiranega uporabnika ali skrbnika. | Navadni uporabnik, privilegirani uporabnik ali skrbnik pusti polje za potrdilo uporabniškega imena prazno. | Navadni uporabnik, privilegirani uporabnik ali skrbnik ni uspešno spremenil svojega profila. Sistem izpiše ustrezno sporočilo. Stran, na kateri se nahaja, se ne spremeni. Torej, ostane pri spremembi profila. |
| Sprememba profila | Uporabnik je na domači strani in je prijavljen. Torej je v vlogi navadnega uporabnika, privilegiranega uporabnika ali skrbnika. | Navadni uporabnik, privilegirani uporabnik ali skrbnik izpolni vsa polja pri čemer je potrdilo za njegovo uporabniško ime napačno. | Navadni uporabnik, privilegirani uporabnik ali skrbnik ni uspešno spremenil svojega profila. Sistem izpiše ustrezno sporočilo. Stran, na kateri se nahaja, se ne spremeni. Torej, ostane pri spremembi profila. |
| Sprememba profila | Uporabnik je na domači strani in je prijavljen. Torej je v vlogi navadnega uporabnika, privilegiranega uporabnika ali skrbnika. | Navadni uporabnik, privilegirani uporabnik ali skrbnik lahko pusti polje za geslo ali dropdown za izbiro fakultete nespremenjeno (ali oba). | Navadni uporabnik, privilegirani uporabnik ali skrbnik je uspešno spremenil svoj profil. Sistem izpiše ustrezno sporočilo. Stran, na kateri se nahaja, se ne spremeni. Torej, ostane pri spremembi profila. |
| Spreminjanje profilov in pravic drugih uporabnikov | Uporabnik je prijavljen kot skrbnik. | Skrbnik izbere ustrezno fukncionalnost. Potem izbere nekega uporabnika iz seznama tako, da ga ročno poišče. Pravilno izpolni vse, kar sistem zahteva. | Sistem izpiše sporočilo, da je sprememba bila uspešna. Stran, na kateri se nahaja, se ne spremeni. |
| Spreminjanje profilov in pravic drugih uporabnikov | Uporabnik je prijavljen kot skrbnik. | Skrbnik izbere ustrezno fukncionalnost. Potem izbere nekega uporabnika iz seznama tako, da uporabi iskanje. Pravilno izpolni vse, kar sistem zahteva. | Sistem izpiše sporočilo, da je sprememba bila uspešna. Stran, na kateri se nahaja, se ne spremeni. |
| Brisanje uporabnika | Uporabnik je prijavljen kot skrbnik. | Skrbnik izbere ustrezno fukncionalnost. Pravilno izpolni vse, kar sistem zahteva. | Profil je bil uspešno izbrisan. Sistem prikaže ustrezno sporočilo. Uporabnik, čigar profil je bil izbrisan, bo odjavljen iz sistema, če je bil prijavljen pred brisanjem. |
| Brisanje uporabnika | Uporabnik je prijavljen kot skrbnik. | Skrbnik izbere ustrezno fukncionalnost. Napačno izpolni eno od polj, ki jih sistem zahteva. | Brisanje ni bilo uspešno. Sistem prikaže ustrezno sporočilo. |
| Brisanje uporabnika | Uporabnik je prijavljen kot skrbnik. | Skrbnik izbere ustrezno fukncionalnost. Pravilno izpolni vse, kar sistem zahteva, vendar poskuša izbrisati drugega skrbnika. | Brisanje ni bilo uspešno. Sistem prikaže ustrezno sporočilo. |
| Pogled predmetnika | Uporabnik je na domači strani in je prijavljen. Torej je v vlogi navadnega uporabnika, privilegiranega uporabnika ali skrbnika. | - | Sistem naprej prikaže vse predmete, v katerih je navadni uporabnik, privilegirani uporabnik ali skrbnik vpisan. Nato sledijo vsi ostali predmeti. |
| Pogled predmetnika | Uporabnik je na domači strani in ni prijavljen. Torej je v vlogi neprijavljenega uporabnika. | - | Sistem prikaže vse ustvarjene predmete. |
| Dodajanje predmeta | Uporabnik je na domači strani in je prijavljen. Torej je v vlogi navadnega uporabnika, privilegiranega uporabnika ali skrbnika. | Navadni uporabnik, privilegirani uporabnik ali skrbnik izbere ustrezno fukncionalnost. Nato pravilno izpolni vse, kar sistem zahteva. | Dodajanje predmeta je bilo uspešno. Sistem prikaže ustrezno sporočilo. Stran, na kateri se nahaja, se ne spremeni, ostane na dodajanju predmeta. |
| Dodajanje predmeta | Uporabnik je na domači strani in je prijavljen. Torej je v vlogi navadnega uporabnika, privilegiranega uporabnika ali skrbnika. | Navadni uporabnik, privilegirani uporabnik ali skrbnik izbere ustrezno fukncionalnost. Nato pusti eno od vnosnih polje prazno ali ne izbere fakultete. | Dodajanje predmeta ni bilo uspešno. Sistem prikaže ustrezno sporočilo. Stran, na kateri se nahaja, se ne spremeni, ostane na dodajanju predmeta. |
| Dodajanje predmeta | Uporabnik je na domači strani in je prijavljen. Torej je v vlogi navadnega uporabnika, privilegiranega uporabnika ali skrbnika. | Navadni uporabnik, privilegirani uporabnik ali skrbnik izbere ustrezno fukncionalnost. Nato izpolni vse, kar sistem zahteva pri čemer predmet že obstaja na izbrani fakulteti. | Dodajanje predmeta ni bilo uspešno. Sistem prikaže ustrezno sporočilo. Stran, na kateri se nahaja, se ne spremeni, ostane na dodajanju predmeta. |
| Dodajanje predmeta | Uporabnik je na domači strani in je prijavljen. Torej je v vlogi navadnega uporabnika, privilegiranega uporabnika ali skrbnika. | Navadni uporabnik, privilegirani uporabnik ali skrbnik izbere ustrezno fukncionalnost. Nato izpolni vse, kar sistem zahteva pri čemer tekst v vnosnem polju ni alfanumeričen. | Dodajanje predmeta ni bilo uspešno. Sistem prikaže ustrezno sporočilo. Stran, na kateri se nahaja, se ne spremeni, ostane na dodajanju predmeta. |
| Vpis v predmet | Uporabnik je na domači strani in je prijavljen. Torej je v vlogi navadnega uporabnika, privilegiranega uporabnika ali skrbnika. | Navadni uporabnik, privilegirani uporabnik ali skrbnik, lahko s pomočjo iskalnika poišče predmet in izpolni vse, kar sistem zahteva. | Vpis je bil uspešen. Stran, na kateri je uporabnik, se osveži in spet nahaja na domači strani. |
| Vpis v predmet | Uporabnik je na domači strani in je prijavljen. Torej je v vlogi navadnega uporabnika, privilegiranega uporabnika ali skrbnika. | Navadni uporabnik, privilegirani uporabnik ali skrbnik, lahko s pomočjo iskalnika poišče predmet, v kateri je že vpisan. | Vpis ni bil uspešen. Sistem prikaže ustrezno sporočilo. |
| Pogled podrobnosti aktivnosti | Uporabnik je na domači strani in je prijavljen. Torej je v vlogi navadnega uporabnika, privilegiranega uporabnika ali skrbnika. | Navadni uporabnik, privilegirani uporabnik ali skrbnik izbere svojo aktivnost iz seznama. | Sistem prikaže podrobnosti o aktivnosti. |
| Dodajanje aktivnosti | Uporabnik je na domači strani in je prijavljen. Torej je v vlogi navadnega uporabnika, privilegiranega uporabnika ali skrbnika. | Navadni uporabnik, privilegirani uporabnik ali skrbnik izpolnijo vse, kar sistem zahteva. | Aktivnost je bila uspešno dodana. Stran, na kateri se uporabnik nahaja, se osveži in spet se nahaja na domači strani. |
| Odstranitev aktvinosti | Uporabnik je na domači strani in je prijavljen. Torej je v vlogi navadnega uporabnika, privilegiranega uporabnika ali skrbnika. | Navadni uporabnik, privilegirani uporabnik ali skrbnik izbere svojo aktivnost iz seznama in jo nato izbriše. | Aktivnost je bila uspešno izbrisana. Stran, na kateri se uporabnik nahaja, se osveži in spet se nahja na domači strani. |
| Odločitev o aktivnosti drugih uporabnikov | Uporabnik je na domači strani in je prijavljen. Torej je v vlogi navadnega uporabnika, privilegiranega uporabnika ali skrbnika. | Navadni uporabnik, privilegirani uporabnik ali skrbnik izbere aktivnost drugih uporabnikov iz seznama in jo sprejme. | Aktivnost je bila sprejeta. Stran, na kateri se uporabnik nahaja, se osveži in spet se nahaja na domači strani. |
| Odločitev o aktivnosti drugih uporabnikov | Uporabnik je na domači strani in je prijavljen. Torej je v vlogi navadnega uporabnika, privilegiranega uporabnika ali skrbnika. | Navadni uporabnik, privilegirani uporabnik ali skrbnik izbere aktivnost drugih uporabnikov iz seznama in jo zavrne. | Aktivnost je bila zavrnjena. Uporabnik ostane na isti strani, kjer se nahaja. |
| Izpis iz predmeta | Uporabnik je na domači strani in je prijavljen. Torej je v vlogi navadnega uporabnika, privilegiranega uporabnika ali skrbnika. | Navadni uporabnik, privilegirani uporabnik ali skrbnik, lahko s pomočjo iskalnika, poišče predmet, v kateri je že vpisan. Izbere ta predmet in se izpiše. | Izpis je bil uspešen. Stran, na kateri je uporabnik se osveži in spet se nahaja na domači strani. |
| Push notifications | Uporabnik ima aplikacijo odprto v brskalniku ampak ni nujno, da ima fokus na zavihku aplikacije. | Uporabnik, ki mora biti prijavljen in aktivnost, ki je ta dan postala aktivna. | Push notification v obliki opomnika se prikaže na zaslonu uporabnika  |
| Delenje na družabnem omrežju Facebook | Uporabnik je na domači strani in je prijavljen. Torej je v vlogi navadnega uporabnika, privilegiranega uporabnika ali skrbnika. | Prijavljen uporabnik, ki ima Facebook račun, klikne na ustrezen gumb na domači strani. | Na Facebook profilu uporabnika se prikaže link do naše aplikacije, prav tako dobi uporabnik status priviligiranega uporabnika, če ga še nima. |
| Izbira prioritete aktivnosti | Uporabnik je na domači strani in je prijavljen. Torej je v vlogi navadnega uporabnika, privilegiranega uporabnika ali skrbnika.  | Uporabnik klikne na ikono za nastavitve v dialogu aktivnosti in izbere možnost urejanja prioritet aktivnosti. Aktivnostim določi prioritete. | Na domači strani uporabnika se aktivnosti sortirajo glede na prioriteto. |
| Sprememba statusa aktivnosti | Uporabnik je na domači strani in je prijavljen. Torej je v vlogi navadnega uporabnika, privilegiranega uporabnika ali skrbnika.  | Uporabnik klikne na določeno aktivnost in ji uredi status. Potem svojo spremembo potrdi. | Status aktivnosti se je ustrezno spremenil. |
| Vremenska napoved | Uporabnik je na domači strani in je prijavljen. Torej je v vlogi navadnega uporabnika, privilegiranega uporabnika ali skrbnika.  | Uporabnik je na domači strani in ima vklopljeno gelokacijo na svoji napravi. | Na domači strani je vidna vremenska napoved današnjega dne. |
| Prikaz motivacijskega citata | Uporabnik je na domači strani in je prijavljen. Torej je v vlogi navadnega uporabnika, privilegiranega uporabnika ali skrbnika.  | Uporabnik je na domači strani. | Na domači strani se mu prikaže motivacijski citat. |
| Odjava iz sistema | Uporabnik je na domači strani in je prijavljen. Torej je v vlogi navadnega uporabnika, privilegiranega uporabnika ali skrbnika.  | Uporabnik klikne na gumb "Moj profil" na domači strani in izbere možnost odjave iz sistema. Svojo odločitev potrdi | Sistem ga sedaj vidi kot neprijavljenega uporabnika, torej uporabnik vidi le dele domače strani. |


## 6. Nefunkcionalne zahteve

| Nefunkcionalna zahteva             | Razlaga| Tip zahteve                                                                                                                                                                                                                                                                                                                                                                                  |
|:------------------------------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Skladnost z GDPR    | Vsi podatki, ki jih aplikacija zahteva od uporabnika, so v skladu z uredbo GDPR. | Zunanja zahteva |
| Skladnost z zakonom o javnem redu in miru   | Imena predmetov, aktivnosti ter samih uporabniških imen ne smejo biti vulgarna ali žaljiva. | Zunanja zahteva |
| Nadgradljivost | Aplikacija je napisana na način, da je dodajanje novih funkcionalnosti enostavno. | Organizacijska zahteva |
| Shranjevanje gesel | Gesla uporabnikov se ne shranjujejo v bazo, le iz gesla-izračunane hash vrednosti. | Organizacijska zahteva |
| Omejitev časa nedelovanja    | Aplikacija more biti na voljo študentom celoten dan skozi cel teden. Čas nedelovanja v enem tednu ne sme preseči 1 ure. | Zahteva izdelka |
| Zanesljivost stanj uporabnikov    | Aplikacija mora preprečevati kakršnekoli izgube podatkov, ki bi pripeljala do resetiranja predmetov oz. aktivnosti, ki jih ima uporabnik shranjene. | Zahteve izdelka |
| Neveljavna stanja | Aplikacija se uspešno izogiba neveljavnim stanjem. Npr. uporabnikom ne omogočamo vnos datumov aktivnosti v preteklosti, itd. | Zahteva izdelka |

## 7. Prototipi vmesnikov
###Prijava v sistem

Na sliki je prikazana zaslonska maska funkcionalnosti Prijava v sistem in del funkcionalnosti Registracija v sistem.
Primeri in tokovi uporabe so vidni v opisu funkcionalnosti. 

Istočasno imamo predstavljen tudi vmesnik do zunajnega sistema. Namreč, uporabniku omogočamo možnost prijave s pomočjo povezovanja njegovega Facebook računa.
Ko uporabnik konča s povezovanjem njegovih Facebook in StraightAs računov, se v našem sistemu izvedejo klici funkcije getUserInformation(), ki ne sprejme argumente, saj je uporabnik vezan na sejo. 

Reference: [5.1], [5.2]. 
![Zaslonska maska prijave v sistem](../img/1.PNG)

### Registracija v sistem

Drugi del poteka funkcionalnosti. Primeri in tokovi uporabe so vidni v opisu funkcionalnosti. 
Reference: [5.2]. 
![Zaslonska maska registracije v sistem](../img/2.PNG)

### Sprememba profila

Primeri in tokovi uporabe so vidni v opisu funkcionalnosti. 
Reference: [5.3].
![Zaslonska maska funkcionalnosti Sprememba profila](../img/3.PNG)

### Spreminjanje profilov in pravic

Na zaslonski maski je predstavljen prvi del funkcionalnosti, sledi izbira željenega uporabnika in dejansko spreminjanje podatkov.  
Primeri in tokovi uporabe so vidni v opisu funkcionalnosti. 
Reference: [5.4].
![Zaslonska maska funkcionalnosti Spreminjanje profilov in pravic](../img/4.PNG)

### Odstranitev uporabnika

Primeri in tokovi uporabe so vidni v opisu funkcionalnosti. 
Reference: [5.5].
![Zaslonska maska funkcionalnosti Odstranitev uporabnika](../img/5.PNG)


### Pogled predmetov (domača stran)

Pregled predmetov je del najbomembnejše zaslonske maske naše aplikacije. V spodnjem primeru je prikazana na način, ki ga vidi navaden uporabnik. Recimo, skrbnik vidi še dodatna polja (uporavljanje z uporabniki ipd.).

Spodnja slika prikazuje tudi vmesnike do zunajnih sistemov, ki jih potrebujeta dve funkcionalnosti našega sistema.

V okviru funkcionalnosti Vremenska napoved se v sistemu izvede klic getWeather(Location location), ki kot parameter sprejme geolokacijo uporabnika.
Prav tako se v okviru funkcionalnosti izvede klic getQuote(), ki vrne poljuben citat vsak dan.

Kot ponavadi, domača stran sistema predstavlja epicenter številnih funkcionalnosti vsakega sistema. 
Prav tako ponuja dostop do skoraj vseh ostalih uporabniških vmesnikov sistema. 
Zaradi tega se bomo v nadeljevanju pogosto sklicevali na njo.

Primeri in tokovi uporabe so vidni v opisu funkcionalnosti. 
Reference: [5.6], [5.19], [5.20].
![Domača stran](../img/homepage.PNG)

### Dodajanje predmeta

Uporabik pride do zaslonske maske funkcionalosti iz domače strani.
Primeri in tokovi uporabe so vidni v opisu funkcionalnosti. 
Reference: [5.7].
![Zaslonska maska funkcionalnosti Dodajanje predmeta](../img/7.PNG)

### Vpis v predmet

Uporabik pride do zaslonske maske funkcionalosti iz domače strani.
Primeri in tokovi uporabe so vidni v opisu funkcionalnosti. 
Reference: [5.8].
![Zaslonska maska funkcionalnosti Prijava v predmet](../img/8.PNG)

### Pogled podrobnosti, urejanje in odstranitev aktivnosti

Uporabik pride do zaslonske maske funkcionalosti iz domače strani.
Primeri in tokovi uporabe so vidni v opisu funkcionalnosti. 
Reference: [5.9], [5.11], [5.18]. 
![Zaslonska maska funkcionalnosti pogled podrobnosti aktivnosti](../img/9.PNG)

### Dodajanje aktivnosti

V spodnjem primeru uporabnik doda aktivnosti, ki so vezane na določen predmet. V primeru dodajanja navadne aktivnosti se mu potem ne odpre dropdown za izbiro predmeta.
Uporabik pride do zaslonske maske funkcionalosti iz domače strani.
Primeri in tokovi uporabe so vidni v opisu funkcionalnosti. 

Reference: [5.10].
![Zaslonska maska funkcionalnosti Dodajanje aktivnosti](../img/10.PNG)

### Odločitev o aktivnosti drugih uporabnikov

Primeri in tokovi uporabe so vidni v opisu funkcionalnosti. 
Reference: [5.12].

![Zaslonska maska funkcionalnosti odločitev o aktivnosti drugih uporabnikov](../img/12.PNG)

### Izpis iz predmeta

Uporabik pride do zaslonske maske funkcionalosti iz domače strani.
Primeri in tokovi uporabe so vidni v opisu funkcionalnosti. 
Reference: [5.13].
![Zaslonska maska funkcionalnosti izpis iz predmeta](../img/13.PNG)

### Push notifications

Primeri in tokovi uporabe so vidni v opisu funkcionalnosti. 
Reference: [5.14].
![Zaslonska maska funkcionalnosti Push notifications](../img/14.PNG)

### Delenje na socialnem omrežju

Primeri in tokovi uporabe so vidni v opisu funkcionalnosti.

Reference: [5.15], [5.16].
![Zaslonska maska](../img/15,16.PNG)

### Urejanje prioritete funkcionalnosti

Potem, ko je uporabnik kliknil na ikono za nastavitve v dialogu aktivnosti, lahko ureja prioriteto svojih aktivnosti.
Primeri in tokovi uporabe so vidni v opisu funkcionalnosti. 
Reference: [5.17].
![Zaslonska maska](../img/17.PNG)

### Odjava iz sistema

Potem, ko je uporabnik kliknil na gumb "Moj profil" na domači strani, lahko zahteva odjavo iz sistema.
Primeri in tokovi uporabe so vidni v opisu funkcionalnosti. 
Reference: [5.21].
![Zaslonska maska](../img/21.PNG)


