# Načrt sistema

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Krištof Brezar, Nina Ramšak, Kristjan Reba, Mihael Švigelj |
| **Kraj in datum** | Ljubljana, 11. 4. 2019 |



## Povzetek

V dokumentu naredimo načrt arhitekture aplikacije StraightAs s preprostim blokovnim diagramom. Uporabimo MVC arhitekturo ter večplastno arhitekturo. Za specifikacijo strukture naše aplikacije uporabimo razredni diagram in podroben opis razredov. Uporabili smo diagrame zaporedja za opis obnašanja aplikacije za vsak primer uporabe.



## 1. Načrt arhitekture

### 1.1 Vzorec model-pogled-krmilnik (MVC)
![Zaslonska maska registracije](../img/Arhitektura_MVC.png)

### 1.2 Večplastna arhitektura
![Zaslonska maska registracije](../img/Arhitektura_Vecplastna.png)


## 2. Načrt strukture

### 2.1 Razredni diagram

![Razredni diagram za aplikacijo StraightAs](../img/razredni_diagram.png)

### 2.2 Opis razredov

#### 1. Neprijavjen uporabnik

Objekt razreda **Neprijavjen uporabnik** predstavlja osebo, ki uporablja sistem, vendar še ni prijavljena z
veljavnim uporabniškim imenom in geslom, ali pa nima narejenaga uporabniškega računa.

##### Atributi
/


##### Nesamoumevne metode
* `Boolean prijava(String username, String password)`; metoda za prijavo v StraightAs 
z lastnim uporabniškim imenom in geslom, ki vrne `true` v primeru, da se oseba prijavi z veljavnim uporabniški imenom in geslom,
sicer vrne `false`in izpiše ustrezno opozorilo.
* `Void registracija()`; registracija novega uporabnika.

#### 2. Prijavljen uporabnik

Objekt razreda **Prijavljen uporabnik** predstavlja osebo, ki aktivno/trenutno uporablja sistem (je prijavljena z veljavnim 
uporabniškim imenom in geslom).

##### Atributi
* `ID_uporabnika: Integer`
* `username: String`
* `temp_password: String`
* `password: Hash value`
* `email: String`

##### Nesamoumevne metode
* `Void odjava()`; odjava trenutno prijavljenega uporabnika iz aplikacije.
* `Void uredi_podatke()`; metoda za urejanje osebnih podatkov trenutno prijavljenega uporabnika.
* `Hash value hash_function(String temp_password)`; pomožna metoda, ki z uporabo zgoščevalne funkcije 
zagotovi večjo varnost uporabnikovega gesla `temp_password`. Izhod je novo geslo `password` (hash value).

#### 3. Plačljiv uporabnik

Objekt razreda **Plačljiv uporabnik** predstavlja osebo, ki aktivno/trenutno uporablja sistem (je prijavljena z veljavnim 
uporabniškim imenom in geslom) in ima dostop do dodatnih funkcij (namenjene so samo plačljivim uporabnikom) **Praksa**. Razred 
deduje od razreda **Prijavljeni uporabnik**.

##### Atributi
* `ID_uporabnikaP: Integer`
* `username: String`
* `temp_password: String`
* `password: Hash value`
* `email: String`

##### Nesamoumevne metode
* `Void odjava()`; odjava trenutno prijavljenega uporabnika iz aplikacije.
* `Void uredi_podatke()`; metoda za urejanje osebnih podatkov trenutno prijavljenega uporabnika.
* `Hash value hash_function(String temp_password)`; pomožna metoda, ki z uporabo zgoščevalne funkcije 
zagotovi večjo varnost uporabnikovega gesla `temp_password`. Izhod je novo geslo `password` (hash value).

#### 4. Administrator

Objekt razreda **Administrator** predstavlja osebo, ki preglejuje in upravlja sistem, ter po potrebi odstrani uporabnike,
ki kršijo osnovna pravila uporabe.

##### Atributi
* `ID_admin: Integer`
* `password: String`

##### Nesamoumevne metode
* `Void odjava()`; odjava administratorja iz aplikacije.
* `Void izbrisi_uporabnika(Integer ID_uporabnika)`; metoda za odstranjevanje/onemogočanje dostopa uporabniškemu računu `ID_uporabnika`, ki je 
kršil osnovna pravila uporabe aplikacije.

#### 5. Praksa

Objekt razreda **Praksa** je namenjen sledenju in posodabljanju podatkov o izvedbi in izplačilu prakse (število oddelanih ur, urina postavka, skupni znesek zaslužka), 
ki jo opravlja plačljivi uporabnik.

##### Atributi
* `st_ur: Integer`
* `urina_postavka: Double`
* `skupni_znesek: Double`

##### Nesamoumevne metode
* `Void uredi_Upostavko()`; metoda za urejanje urine postavke trenutno prijavljenega plačljivega uporabnika.
* `Void uredi_StUr()`; metoda za urejanje števila oddelanih ur trenutno prijavljenega plačljivega uporabnika uporabnika.

#### 6. Ocena

Objekt razreda **Ocena** je namenjen hranjenju ocen obveznosti in njihovega deleža doprinosa k končni oceni
predmeta, ki ga opravlja uporabnik.

##### Atributi
* `ID_ocene: Integer`
* `vrednost: Integer`
* `delez_doprinosa: Decimal`
* zaloga vrednosti: [0,10]

##### Nesamoumevne metode
* `Boolean dodaj_oceno(Integer ID_predmeta, Integer vrednost, Decimal delez_doprinosa)`; metoda za dodajanje nove ocene 
in njenega doprinosa k končni oceni izbranega pedmeta (`ID_predmeta`). Metoda vrne `true` če je ocena uspešno dodana 
(če ni doseženo maksimalno število ocen), sicer vrne `false`.
* `Void uredi_oceno()`; metoda za urejanje ucene in njenega doprinosa k končni oceni predmeta.
* `Ocena vrni_seznam_ocen(Integer ID_predmeta)`; metoda vrne seznam vseh ocen izbranega predmeta (`ID_predmeta`).

#### 7. Predmet

Objekt razreda **Predmet** je namenjen hranjenju informacij in podrobnosti o predmetih, ki jih uporabnik trenutno opravlja.

##### Atributi
* `ID_predmeta: Integer`
* `ime: String`
* zaloga vrednosti: [0,20]

##### Nesamoumevne metode
* `Boolean dodaj_predmet(String ime)`; metoda za dodajanje novega predmeta z imenom `ime` v predmetnik uporabnika.
Metoda vrne `true` če je predmet uspešno dodan (če ni doseženo maksimalno število predmetov), sicer vrne `false`.
* `Void izbrisi_predmet(Integer ID_predmeta)`; metoda za brisanje izbranega predmeta in njegovih podatkov.
* `Void uredi_predmet()`; metoda za urejanje predmeta in njegovih podatkov.
* `Predmet vrni_seznam_predmetov(Integer ID_predmeta)`; metoda vrne seznam vseh predmetov trenutnega uporabnika.

#### 8. Obveznost

Objekt razreda **Obveznost** je namenjen hranjenju informacij in podrobnosti o uporabnikovih obveznostih, 
njihovih proritetah, datumu zapadlosti, itd.

##### Atributi
* `ID_obveznosti: Integer`
* `prioriteta: Integer`
* `datum_zapadlosti: Date`
* `predmet: Predmet`
* zaloga vrednosti: [0,50]

##### Nesamoumevne metode
* `Boolean dodaj_obveznost(String naslov, Integer prioriteta, Date datum_zapadlosti, Predmet predmet)`; metoda za dodajanje nove obveznosti z imenom `naslov`, prioriteto `prioriteta` in 
datumom zapadlosti `datum_zapadlosti`. Metoda vrne `true` če je obveznost uspešno dodana (če ni doseženo maksimalno število obveznosti), sicer vrne `false`.
* `Void izbrisi_obveznost(Integer ID_predmeta)`; metoda za brisanje izbrane obveznosti in njenih podatkov.
* `Void uredi_obveznost()`; metoda za urejanje obveznosti in njihovih podatkov.
* `Predmet vrni_seznam_predmetov(Integer ID_predmeta)`; metoda vrne seznam vseh obveznosti trenutnega uporabnika.

#### 9. Ura

Objekt razreda **Ura** je namenjen dodajanju in brisanju ur predmeta na urniku.

##### Atributi
* `ime_predmeta: String`
* `zacetek: Integer`
* `konec: Integer`

##### Nesamoumevne metode
* `Boolean dodaj_ure(String ime_predmeta, Integer zacetek, Integer konec)`; metoda za dodajanje novega predmeta
* na urnik. Čas trajanja je podan z argumentoma `zacetek` in `konec`. Metoda vrne `true` če so ure predmeta uspešno dodane 
na urnik (če niso zasedeni vsi časovni termini na urniku), sicer vrne `false`.
* `Void izbrisi_ure()`;  metoda za brisanje izbrane ure na urniku

#### 10. Urnik

Objekt razreda **Urnik** je namenjen sledenju in pregledu urnika uporabnika.

##### Atributi
/

##### Nesamoumevne metode
* `Void uredi_Urnik()`; metoda za urejanje razporeditve ur predmetov na urniku.

## 3. Načrt obnašanja

#### Registracija novega uporabnika
![Diagram za funkcionalnost 'Registracija novega uporabnika'](../img/diagram_registracija.png)

#### Prijava uporabnika
![Diagram za funkcionalnost 'Prijava uporabnika'](../img/diagram_prijava.png)

#### Ponastavitev gesla
![Diagram za funkcionalnost 'Ponastavitev gesla'](../img/diagram_ponastavitev_gesla.png)

#### Nastavitve obveznosti
![Diagram za funkcionalnost 'Nastavitve obveznosti'](../img/diagram_nastavitve_obveznosti.png)

#### Nastavitve seznama predmetov
![Diagram za funkcionalnost 'Nastavitve seznama predmetov'](../img/diagram_nastavitve_seznama_predmetov.png)

#### Nastavitve prakse
![Diagram za funkcionalnost 'Nastavitve prakse'](../img/diagram_nastavitve_prakse.png)

#### Odjava uporabnika
![Diagram za funkcionalnost 'Odjava uporabnika'](../img/diagram_odjava.png)

#### Pregled obveznosti
![Diagram za funkcionalnost 'Pregled obveznosti'](../img/diagram_pregled_obveznosti.png)

#### Dodajanje obveznosti
![Diagram za funkcionalnost 'Dodajanje obveznosti'](../img/diagram_dodajanje_obveznosti.png)

#### Brisanje obveznosti
![Diagram za funkcionalnost 'Brisanje obveznosti'](../img/diagram_brisanje_obveznosti.png)

#### Urejanje obveznosti
![Diagram za funkcionalnost 'Urejanje obveznosti'](../img/diagram_urejanje_obveznosti.png)

#### Pregled predmetov
![Diagram za funkcionalnost 'Pregled predmetov'](../img/diagram_pregled_predmetov.png)

#### Dodajanje predmeta
![Diagram za funkcionalnost 'Dodajanje predmeta'](../img/diagram_dodajanje_predmeta.png)

#### Brisanje predmeta
![Diagram za funkcionalnost 'Brisanje predmeta'](../img/diagram_brisanje_predmetov.png)

#### Dodajanje ocene
![Diagram za funkcionalnost 'Dodajanje ocene'](../img/Dodajanje_ocene.PNG)

#### Urejanje ocene
![Diagram za funkcionalnost 'Urejanje ocene'](../img/Urejanje_ocen.PNG)

#### Pregled prakse
![Diagram za funkcionalnost 'Pregled prakse'](../img/Pregled_prakse.PNG)

#### Urejanje števila oddelanih ur
![Diagram za funkcionalnost 'Urejanje števila oddelanih ur'](../img/Urejanje_st_ur.PNG)

#### Urejanje urne postavke
![Diagram za funkcionalnost 'Urejanje urne postavke'](../img/Urejanje_urne_postavke.PNG)

#### Pregled urnika
![Diagram za funkcionalnost 'Pregled urnika'](../img/pregled_urnika.PNG)

#### Dodajanje ur na urnik
![Diagram za funkcionalnost 'Dodajanje ur na urnik'](../img/Dodajanje_ur_na_urnik.PNG)

#### Izbris uporabnika
![Diagram za funkcionalnost 'Izbris uporabnika'](../img/Izbris_uporabnika.PNG)




