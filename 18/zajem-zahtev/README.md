# Dokument zahtev

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Krištof Brezar, Nina Ramšak, Kristjan Reba, Mihael Švigelj |
| **Kraj in datum** | Ljubljana, 27.3.2019 |



## Povzetek projekta

Študentom želimo olajšati spremljanje študijskih obveznosti. V ta namen definiramo funkcionalnosti aplikacije, ki bi študentom omogočale spremljanje nalog, ocen in izpitnih rokov na enem mestu. Spletna aplikacija bo omogočala tudi pregled za spremljanje obvezne prakse. Funkcionalnosti aplikacije pa bodo dovolj fleksibilne za spremljanje poteka pisanja  zaključne naloge kot sta diplomska in magisterska naloga.



## 1. Uvod

Aplikacija je namenjena študentom, da rešuje problem spremljanja študijskih obveznosti. Spletna učilnica se pogosto izkaže kot neučinkovita saj podatki in roki za oddajo nalog niso konsistentno objavljeni pri vseh predmetih. Aplikacija StraightAs bi ponujala pregled vseh rokov za naloge in urnik na katerem bo jasno razvidno do kdaj je nalogo potrebno narediti. Aplikacija bo vključevala TO-DO list na katerega študent lahko dodaja študijske obveznosti in jim dodeli datum zapadlosti ter prioriteto. Aplikacija bo podpirala spremljanje ocen in delnih ocen pri posameznih predmetih. Uporabnik bo imel možnost nastaviti število potrebnih ur za opravljeno prakso kot tudi spremljanje zaslužka na praksi. V glavnem oknu aplikacije bo študentu na voljo koledarski pregled obveznosti in oceno pri posameznem predmetu. Če ima uporabnik vklopljeno nastavitev za spremljanje opravljanja prakse se mu v glavnem oknu izpiše število oddelanih ur in zaslužek.



## 2. Uporabniške vloge

Ciljna skupina aplikacije StraightAs so študenti in dijaki.

**Neprijavljen uporabnik** ima na voljo pogled na vstopno stran aplikacije. Iz te vstopne strani ima možnost dostopati do strani za prijavo v aplikacijo in do strani za registracijo.

**Prijavljen uporabnik** ima možnost dodajati nalog v Obveznosti in jim določiti datum zapadlosti in prioriteto. Prijavljen uporabnik lahko za vsak predmet doda oceno ter ji dodeli delež doprinosa končni oceni. Omogočen mu je vpogled v glavno okno kjer so urejene vse aktivnosti in navedene ocene za posamezen predmet. Prijavljen uporabnik lahko dostopa do strani za urejanje profila kjer se lahko spremeni osebne podatke, podatke o uporabniškem računu in nastavitve aplikacije. Prijavljen uporabnik se lahko odjavi iz aplikacije.

**Plačljivi uporabnik** ima poleg vseh funkcionalnosti prijavljenega uporabnika, med nastavitvami možnost vklopiti  opravljanje prakse. Če je ta možnost vklopljena, se Plačljiviemu uporabniku doda funkcija za spremljanje oddelanih ur prakse kot tudi nastavitev urne postavke ter izračun zaslužka.


## 3. Slovar pojmov

* **profesor** - kvalificiran strokovnjak (z ustrezno izobrazbo), ki predava o svojih področjih dela in raziskovanja na fakulteti. 
* **asistent** - strokovni sodelavec, pomočnik profesorja na univerzi, ki je zadolžen za izvedbo vaj. Ima vsaj diplomsko izobrazbo, lahko pa v času dela nadaljuje svoje izobraževanje in sodeluje v raziskovalni dejavnosti.
* **predmet** - strokovno področje, ki je študentom predstavljeno v obliki predavanj, vaj in študijskih obveznosti.
* **predavanja** - govorna predstavitev strokovne, vsebisnko zaokrožene tematike ki jo študentom posreduje profesor.
* **vaje** - praktična predstavitev strokovne, vsebinsko zaokrožene tematike iz predavanj v obliki programerskih ali/in računskih nalog, ki jo študentom predstavi asistent.
* **pogoj** - množica nalog in opravil, ki jih mora študent uspešno opraviti (s pozitvno oceno), da lahko pristopi k izpitu.
* **(pisni/ustni) izpit** - končni preizkus znanja, , ki ga izvede profesor, udeležijo pa se ga študentje, ki po končanem preizkusu prejmejo oceno. Lahko je v ustni in/ali pisni obliki.
* **kolokvij** - pisni preizkus znanja, ki ga izvedejo profesor in asistenti, udeležijo pa se ga študentje, ki po končanem prizkusu prejmejo oceno oziroma so seznanjeni z uspehom (v obliki procentov - %). Pogosto je uspešno opravljen kolokvij potreben za pristop k izpitu.
* **(študijske) obveznosti** - množica nalog in opravil (npr. domače naloge, kvizi, projekti, seminarske naloge, itd.), ki jih mora študent opraviti do v naprej predvidenega datuma.
* **projekt** - časovno in ciljno usmerjen proces enega ali (po navadi) več študentov, ki v določenem zaporedju izvajajo dejavnosti in poddejavnosti, ki so usmerjene k doseganu končnega cilja. Sprotne naloge, delo in napredek se dokumentirajo. Na koncu projekta sledi predstavitev delujočega izdelka.
* **seminarska naloga** - strokovno znanstveno delo manjšega obsega v katerem študent obravnava v naprej določen problem iz strokovne tematike, ter poroča o svojih ugotovitvah in rezultatih.
* **domača naloga** - pisna, praktična obveznostu študenta, ki jo mora slednji opraviti samostojno, do v naprej dolčenega datuma.
* **kviz** - pisni preizkus sprotnega preverjanja znanja, ki se ga udeležijo študenti in po končanem preizkusu prejmejo oceno oziroma so seznanjeni z uspehom (v obliki procentov - %).
* **ocena** - celo število iz intervala [5,10], ki označuje stopnjo uspešnosti opravljanja izpita. Da študent opravi izpit je potrebna ocena vsaj 6.
* **semester** - v naprej določeni časovni interval v katerem študent opravlja svoje študijske obveznosti.
* **seznam predmetov** - grafično predstavljen seznam vseh predmetov, ki jih študent trenutno opravlja. Vsebuje ocene obveznosti pri posameznih predmetih.
* **Obveznosti** - seznam obveznosti, ki jih mora študent še opraviti.
* **beležka** - grafično predstavljen list papirja na katerega študent zapisuje misli, opravila, naloge itd. Ima možnost opozarjanja na pomemben dogodek.
* **prioriteta** - celo število iz intervala [1,5], ki določa pomembnost posameznim obveznostim. Obveznossti s prioriteto 1 so najpomembnejše, obveznosti s prioriteto 5 pa so najmanj pomembne.
* **delovna praksa** - redna študijska obveznost študenta (navadno prvostopenjskega visokošolsko strokovnega študijskega programa), katere namen je prilagoditi znanje študentov potrebam gospodarstva in javnega sektorja ter jih usposobiti do take mere, da se bodo ob zaposlitvi sposobni produktivno vključiti v delo v podjetjih in ustanovah.
* **začetna stran** - grafična predstavitev vseh funkcionalnosti aplikacije za prijavljenega uporabnika (Obveznosti, Moje beležke, Predmeti, Praksa).

## 4. Diagram primerov uporabe

![Diagram primerov uporabe](../img/useCase.gif)

## 5. Funkcionalne zahteve

### 1. Registracija novega uporabnika

#### Povzetek funkcionalnosti

Neprijavljen uporabnik lahko ustvari novi uporabniški račun s katerim se bo lahko v prihodnje prijavil v aplikacijo. Za uspešno registracijo mora navesti veljavem e-mail na katerega bo prejel potrditev registracije. 

#### Osnovni tok

1. Neprijavljen uporabnik izbere funkcionalnost _Registracija uporabnika_
2. Aplikacija prikaže polja za vnos vseh potrebnih informacij (e-mail, uporabniško ime in geslo) 
3. Neprijavljen uporabnik izpolni vsa polja
4. Neprijavljen uporabnik s klikom na gumb _Registracija_ potrdi registracijo
5. Neprijavljen uporabnik prejeme potrditveni e-mail na naslov, ki ga je navedel v vnosna polja
6. Neprijavljen uporabnik odpre potrditveni e-mail
7. Neprijavljen uporabnik klikne na link _Potrditev regisracije_, ki ga preusmeri na njegovo začetno stran v aplikaciji

#### Alternativni tok(ovi)

/

#### Izjemni tok(ovi)

* Neprijavljen uporabnik se ne more registrirati, ker že ima veljaven uporabniški račun vezan na njegov e-mail.
* Neprijavljen uporabnik se ne more registrirati, ker je izbrano uporabniško ime že zasedeno.
* Neprijavljen uporabnik poda neveljaven e-mail.

#### Pogoji

* Neprijavljen uporabnik mora biti neregistriran/ ne sme imeti obstoječega uporabniškega računa (z enakim uporabniškim imenom).
* Izbrano uporaniško ime še ne sme biti v uporabi.

#### Posledice

* Neprijavljen uporabnik ima narejen uporabniški račun s katerim se lahko prijavi in uporablja aplikacijo.

#### Posebnosti

/

#### Prioriteta identificiranih funkcionalnosti

* MUST HAVE

#### Sprejemni testi

* Registriraj se kot uporabnik; navedi veljaven e-mail in ga potrdi; preusmerjen si na začetno stran aplikacije (home page).
* Registriraj se kot uporabnik; navedi neveljaven e-mail; aplikacija izpiše opozorilo "Neveljaven e-mail naslov".  
* Registriraj se kot uporabnik; navedi uporabniško ime, ki je že zasedeno; aplikacija izpiše opozorilo "To uporabniško ime je zasedeno!".  

### 2. Prijava uporabnika

#### Povzetek funkcionalnosti

Neprijavljeni uporabnik z veljavnim uporabniškim računom se lahko s svojim uporabniškim imenom in geslom prijavi v aplikacijo.


#### Osnovni tok

1. Neprijavljen uporabnik izbere funkcionalnost _Prijava_
2. Aplikacija prikaže polja za vnos uporabniškega imena in gesla 
3. Neprijavljen uporabnik izpolni vsa polja (vpiše svoje uporabniško ime in geslo)
4. Neprijavljen uporabnik s klikom na gumb _Prijava_ potrdi svoj vpis
5. Aplikacija preusmeri (sedaj) prijavljenega uporabnika na njegovo začetno stran


#### Alternativni tok(ovi)

/

#### Izjemni tok(ovi)

* Neprijavljen uporabnik se ne more vpisati v sistem, ker je navedel neveljavno uporabniško ime.
* Neprijavljen uporabnik se ne more vpisati v sistem, ker je navedel neveljavno geslo.
* Neprijavljen uporabnik se ne more vpisati v sistem, ker nima narejenega uporabniškega računa.

#### Pogoji

* Neprijavljen uporabnik ima veljaven uporabniški račun (je v preteklosti že opravil registracijo kot novi uporabnik).


#### Posledice

* (Sedaj) prijavljeni uporabnik lahko dostopa do svojih predhodno shranjenih obveznosti in funkcionalnosti, ki jih aplikacija nudi.


#### Posebnosti

/
#### Prioriteta identificiranih funkcionalnosti

* MUST HAVE

#### Sprejemni testi

* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page).
* Prijavi se z neveljavnim uporabniškim imenom; aplikacija izpiše opozorilo "Uporabniško ime in/ ali geslo ni veljavno!""; ponudi ti možnosti za ponastavitev gesla in registracijo.
* Prijavi se z veljavnim uporabniškim imenom in neveljavnim/pozabljenim geslom; aplikacija izpiše opozorilo  "Uporabniško ime in/ ali geslo ni veljavno!"; ponudi ti možnosti za ponastavitev gesla in registracijo.
* Prijavi se z neveljavnim uporabniškim imenom in neveljavnim geslom; aplikacija izpiše opozorilo  "Uporabniško ime in/ ali geslo ni veljavno!"; ponudi ti možnosti za ponastavitev gesla in registracijo.

### 3. Ponastavitev gesla

#### Povzetek funkcionalnosti

Neprijavljeni uporabnik z veljavnim uporabniškim računom lahko ponastavi svoje geslo, v primeru da ga je izgubil oz. pozabil.


#### Osnovni tok

1. Neprijavljen uporabnik izbere funkcionalnost _Prijava_
2. Aplikacija prikaže polja za vnos uporabniškega imena in gesla 
3. Neprijavljen uporabnik izpolni polje za uporabniško ime
4. Neprijavljen uporabnik s klikom na gumb _Prijava_ poskusi potrditi svoj vpis
5. Aplikacija neprijavljenemu uporabniku izpiše obvestilo "Uporabniško ime in/ ali geslo ni veljavno!" in ponudi možnosti za ponastavitev gesla in registracijo
6. Neprijavljeni uporabnik s klikom na gumb _Ponastavi geslo_ izbere funkcionalnost ponastavitve gesla
7. Aplikacija prikaže polje za vnos e-mail naslova 
8. Neprijavlen uporabnik izpolni polje
9. Neprijavljen uporabnik potrdi izbiro z gumbom _Pošlji_
10. Neprijavljen uporabnik prejeme e-mail za ponastavitev gesla na naslov, ki ga je navedel v vnosna polja
11. Neprijavljen uporabnik odpre e-mail
12. Neprijavljen uporabnik klikne na link _Ponastavitev gesla_, ki ga preusmeri na stran za ponastavitev gesla
13. Aplikacija prikaže polji za vpis novega gesla, ter njegovo potrditev
14. Neprijavljeni uporabnik izpolni obe polji (v obe napiše novo geslo)
15. Neprijavljeni uporabnik s klikom na gumb _Ponastavi_ potrdi ponastavitev gesla
16. Aplikacija preusmeri neprijavljenega uporabnika na začetno stran


#### Alternativni tok(ovi)

/

#### Izjemni tok(ovi)

* Neprijavljen uporabnik ne more ponastaviti gesla, ker je navedel neveljaven e-mail.

#### Pogoji

* Neprijavljen uporabnik ima veljaven uporabniški račun (je v preteklosti že opravil registracijo kot novi uporabnik).


#### Posledice

* Neprijavljeni uporabnik je ponastavil svoje geslo, s katerim se bo lahko uspešno prijavil v aplikacijo.


#### Posebnosti

/
#### Prioriteta identificiranih funkcionalnosti

* SHOULD HAVE

#### Sprejemni testi

* Prijavi se z veljavnim uporabniškim imenom in neveljavnim/pozabljenim geslom; aplikacija izpiše opozorilo " Uporabniško ime in/ ali geslo ni veljavno!"; ponudi ti možnosti za ponastavitev gesla in registracijo; izberi možnost za potrditev gesla; v prikazano polje vpiši svoj e-mail; s klikom na gumb _Pošlji_ potrdi svojo izbiro; pojdi na svoj e-mail in odpri potrditveno pošto; klikni na link _Ponastavitev gesla_; na strani aplikacije za ponastavitev gesla v vsa polja vpiši enako (novo) geslo; s klikom na gumb _Ponastavi_ potrdi spremembe; preusmerjen si na začetno stran aplikacije za neprijavljene uporabnike.
* Prijavi se z neveljavnim uporabniškim imenom in neveljavnim geslom; aplikacija izpiše opozorilo " Uporabniško ime in/ ali geslo ni veljavno!"; ponudi ti možnosti za ponastavitev gesla in registracijo; izberi možnost za potrditev gesla; v prikazano polje vpiši svoj e-mail; s klikom na gumb _Pošlji_ potrdi svojo izbiro; ker je email neveljaven ne boš dobil pošte za ponastavitev gesla.

### 4. Nastavitve Obveznosti

#### Povzetek funkcionalnosti
Prijavljen in plačljivi uporabnik lahko spreminja nastavitve Obveznosti, kot so način razporeditve obveznosti (po prioriteti ali datumu zapadlosti). Prav tako lahko omogoči funkcijo, ki omogoči, da aplikacija sama zbriše obveznosti, ko te prekoračijo datum zapadlosti.

#### Osnovni tok

##### Prijavljen uporabnik
1. Prijavljen uporabnik izbere funkcionalnost _Obveznosti_
2. Aplikacija preusmeri uporabnika na stran s seznamom njegovih obveznosti
3. Aplikacija prikaže na isti strani dva gumba/izbirni polji ob polju _Uredi po:_ (_prioriteta_ in _datum zapadlosti_)
4. Prijavljen uporabnik v polju izbere način kako naj bodo urejene Obveznosti 
5. Aplikacija prikaže obvestilo "Ali želite shraniti spremembe nastavitev?"
6. Prijavljen uporabnik potrdi spremembe klikom na gumb _Shrani spremembe_
7. Aplikacija prijavljenega uporabnika preusmeri na stran profila

##### Plačljivi uporabnik
1. Plačljivi uporabnik izbere funkcionalnost _Obveznosti_
2. Aplikacija preusmeri uporabnika na stran s seznamom njegovih obveznosti
3. Aplikacija prikaže na isti strani dva gumba/izbirni polji ob polju _Uredi po:_ (_prioriteta_ in _datum zapadlosti_)
4. Plačljivi uporabnik v polju izbere način kako naj bodo urejene Obveznosti 
5. Aplikacija prikaže obvestilo "Ali želite shraniti spremembe nastavitev?"
6. Plačljivi uporabnik potrdi spremembe klikom na gumb _Shrani spremembe_
7. Aplikacija plačljivegaa uporabnika preusmeri na stran profila

#### Alternativni tok(ovi)

/

#### Izjemni tok(ovi)

* Neprijavljen uporabnik ne more dostopati do funkcionalnosti _Uredi profil_.

#### Pogoji

* Neprijavljen uporabnik ima veljaven uporabniški račun (je v preteklosti že opravil registracijo kot novi uporabnik) in je trenutno prijavljen v aplikacijo.

#### Posledice

* Prijavljen/ plačljivi uporabnik ima posodobljene nastavitve svojeih Obveznosti.

#### Posebnosti

/

#### Prioriteta identificiranih funkcionalnosti

* MIGHT HAVE

#### Sprejemni testi

* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page); klikni na gumb _Uredi profil_ in si preusmerjen na stran profila; klikni na gumb _nastavitve Obveznosti_; v izbirnih poljih izberi željene nastavitve; s klikom na gumb _Shrani spremembe_ potrdi svoje spremembe ; preusmerjen si na stran profila.

### 5. Nastavitve seznama predmetov

#### Povzetek funkcionalnosti
Prijavljeni in plačljivi uporabnik lahko spreminja nastavitve seznama predmetov. Nastavi lahko način razporeditve predmetov (po abecednem vrstnem redu ali datumu dodajanja predmeta).

#### Osnovni tok
##### Prijavljeni uporabnik
1. Prijavljen uporabnik izbere funkcionalnost _Uredi profil_
2. Aplikacija prikaže polja z vsemi informacijami o uporabniku (ime, priimek, uporabniško ime, e-mail, geslo)
3. Prijavljen uporabnik s klikom na gumb _Nastavitve predmetov_ potrdi funkcionalnost nastavitve seznama predmetov
4. Aplikacija prikaže izbirno polje z dvema opcijama: _Abecedni vrstni red_ in _Datum nastanka_
5. Prijavljen uporabnik označi željeno polje
6. Prijavljen uporabnik potrdi spremembe klikom na gumb _Shrani spremembe_
7. Aplikacija prijavljenega uporabnika preusmeri na stran profila

##### Plačljivi uporabnik
1. Plačljivi uporabnik izbere funkcionalnost _Uredi profil_
2. Aplikacija prikaže polja z vsemi informacijami o uporabniku (ime, priimek, uorabniško ime, e-mail, geslo)
3. Plačljivi uporabnik s klikom na gumb _Nastavitve predmetov_ potrdi funkcionalnost nastavitve seznama predmetov
4. Aplikacija prikaže izbirno polje z dvema opcijama: _Abecedni vrstni red_ in _Datum nastanka_
5. Plačljivi uporabnik označi željeno polje
6. Plačljivi uporabnik potrdi spremembe klikom na gumb _Shrani spremembe_
7. Aplikacija Plačljiviega uporabnika preusmeri na stran profila

#### Alternativni tok(ovi)

/

#### Izjemni tok(ovi)

* Neprijavljen uporabnik ne more dostopati do funkcionalnosti _Uredi profil_.

#### Pogoji

* Neprijavljen uporabnik ima veljaven uporabniški račun (je v preteklosti že opravil registracijo kot novi uporabnik) in je trenutno prijavljen v aplikacijo.

#### Posledice

* Prijavljen/ Plačljivi uporabnik ima posodobljene nastavitve svojega seznama predmetov.

#### Posebnosti

/

#### Prioriteta identificiranih funkcionalnosti

* MIGHT HAVE

#### Sprejemni testi

* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page); klikni na gumb _Uredi profil_ in si preusmerjen na stran profila; klikni na gumb _nastavitve predmetov; v izbirnih poljih izberi željene nastavitve; s klikom na gumb _Shrani spremembe_ potrdi svoje spremembe ; preusmerjen si na stran profila.


### 6. Nastavitve Prakse

#### Povzetek funkcionalnosti
Plačljivi uporabnik lahko omogoči možnost dodatne funkcionalnosti _Praksa_ 

#### Osnovni tok

1. Plačljivi uporabnik izbere funkcionalnost _Uredi profil_
2. Aplikacija prikaže polja z vsemi informacijami o uporabniku (ime, priimek, uorabniško ime, e-mail, geslo)
3. Plačljivi uporabnik s klikom na gumb _Nastavitve prakse_ potrdi funkcionalnost nastavitve prakse
4. Aplikacija prikaže izbirno polje z dvema opcijama: _Omogoči prakso_ in _Onemogoči prakso_
5. Plačljivi uporabnik označi željeno polje
6. Plačljivi uporabnik potrdi spremembe klikom na gumb _Shrani spremembe_
7. Aplikacija Plačljiviega uporabnika preusmeri na stran profila

#### Alternativni tok(ovi)

/

#### Izjemni tok(ovi)

* Neprijavljen uporabnik ne more dostopati do funkcionalnosti _Uredi profil_.
* Prijavljen uporabnik, ki ni Plačljivi ne more dostopati do funkcionalnost _Praksa_.

#### Pogoji

* Neprijavljen uporabnik ima veljaven Plačljivi uporabniški račun (je v preteklosti že opravil registracijo kot novi uporabnik) in je trenutno prijavljen v aplikacijo.

#### Posledice

* Plačljivi uporabnik ima posodobljene nastavitve izbirne funkcionalnosti _Praksa_.

#### Posebnosti

/

#### Prioriteta identificiranih funkcionalnosti

* SHOULD HAVE

#### Sprejemni testi

* Prijavi se z veljavnim uporabniškim imenom in geslom (kot Plačljivi uporabnik); preusmerjen si na začetno stran aplikacije (home page); klikni na gumb _Uredi profil_ in si preusmerjen na stran profila; klikni na gumb _nastavitve Prakse_; v izbirnem polju izberi željeno nastavitev; s klikom na gumb _Shrani spremembe_ potrdi svoje spremembe ; preusmerjen si na stran profila.

### 7. Odjava uporabnika
#### Povzetek funkcionalnosti
Prijavljen in plačljivi uporabnik se lahko odjavi iz aplikacije.


#### Osnovni tok
**Prijavljen uporabnik**
1. Prijavljen uporabnik na začetni strani izbere funkcionalnost _Odjava_
2. Aplikacija prikaže obvestilo "Ali ste prepričani, da se želite odjaviti?"
3. Prijavljen uporabnik s klikom na gumb _Da_ potrdi odjavo
4. Aplikacija (sedaj) neprijavljenega uporabnika preusmeri na začetno stran za neprijavljenega uporabnika

**Plačljivi uporabnik**

1. Plačljivi uporabnik na začetni strani izbere funkcionalnost _Odjava_
2. Aplikacija prikaže obvestilo "Ali ste prepričani, da se želite odjaviti?"
3. Plačljivi uporabnik s klikom na gumb _Da_ potrdi odjavo
4. Aplikacija (sedaj) neprijavljenega uporabnika preusmeri na začetno stran za neprijavljenega uporabnika


#### Alternativni tok(ovi)

/

#### Izjemni tok(ovi)

* Neprijavljen uporabnik ne more dostopati do funkcionalnosti _Odjava_.

#### Pogoji

* Neprijavljen uporabnik ima veljaven uporabniški račun (je v preteklosti že opravil registracijo kot novi uporabnik) in je trenutno prijavljen v aplikacijo.

#### Posledice

* Prijavljen/ Plačljivi uporabnik je odjavljen iz aplikacije in posledično kot neprijavljen uporabnik ne more dostopati do njenih funkcionalnosti.

#### Posebnosti

/

#### Prioriteta identificiranih funkcionalnosti

* MUST HAVE

#### Sprejemni testi

* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page); klikni na gumb _Odjava_; ob obvestilu aplikacije klikni na gumb _Da_; preusmerjen si na začetno stran za neprijavljenega uporabnika.

### 8. Pregled Obveznosti
#### Povzetek funkcionalnosti
Prijavljen in plačljivi uporabnik lahko podrobneje preuči in pregleda svoje obstoječe in veljavne Obveznosti, urejene glede na nastavitve aplikacije (po prioriteti ali datumu zapadlosti).

#### Osnovni tok
##### Prijavljen uporabnik
1. Prijavljen uporabnik na začetni strani izbere funkcionalnost _Obveznosti_
2. Aplikacija prikaže scroll seznam z vsemi obveznostmi urejenimi po prioriteti ali datumu zapadlosti
3. Prijavljen uporabnik poljubno scrolla po seznamu 

##### Plačljivi uporabnik
1. Plačljivi uporabnik na začetni strani izbere funkcionalnost _Obveznosti_
2. Aplikacija prikaže scroll seznam z vsemi obveznostmi urejenimi po prioriteti ali datumu zapadlosti
3. Plačljivi uporabnik poljubno scrolla po seznamu 


#### Alternativni tok(ovi)

/

#### Izjemni tok(ovi)

* Neprijavljen uporabnik ne more dostopati do funkcionalnosti _Obveznosti_.

#### Pogoji

* Neprijavljen uporabnik ima veljaven uporabniški račun (je v preteklosti že opravil registracijo kot novi uporabnik) in je trenutno prijavljen v aplikacijo.

#### Posledice

* Prijavljen/ Plačljivi uporabnik ima prikazane vse svoje obveznosti.

#### Posebnosti

/

#### Prioriteta identificiranih funkcionalnosti

* SHOULD HAVE

#### Sprejemni testi

* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page); klikni na gumb _Obveznosti_; preusmerjen si na Obveznosti; preglej seznam.

### 9. Dodajanje obveznosti
#### Povzetek funkcionalnosti 
Prijavljen in plačljivi uporabnik lahko v Obveznosti doda novo obveznost, ji določi prioriteto, ter datum zapadlosti.

#### Osnovni tok
##### Prijavljen uporabnik
1. Prijavljen uporabnik na začetni strani izbere funkcionalnost _Obveznosti_
2. Aplikacija prikaže scroll seznam z vesmi obveznostmi urejenimi po prioriteti ali datumu zapadlosti
3. Prijavljen uporabnik s klikom na gumb _Dodaj obveznost_ izbere funkcionalnost dodajanja zahteve
4. Aplikacija prikaže polja za vpis potrebnih informacij (ime obveznosti, prioriteta obveznosti in datum zapadlosti obveznosti)
5. Prijavljen uporabnik vpiše v polja vse potrebne informacije
6. Prijavljen uporabnik s klikom na gumb _Potrdi_ potrdi dodajanje obveznosti
7. Aplikacija preusmeri prijavljenega uporabnika na Obveznosti, ki ima na ustreznem mestu (glede na prioriteto ali datum zapadlosti) dodano novo obveznost

##### Plačljivi uporabnik
1. Plačljivi uporabnik na začetni strani izbere funkcionalnost _Obveznosti_
2. Aplikacija prikaže scroll seznam z vesmi obveznostmi urejenimi po prioriteti ali datumu zapadlosti
3. Plačljivi uporabnik s klikom na gumb _Dodaj obveznost_ izbere funkcionalnost dodajanja zahteve
4. Aplikacija prikaže polja za vpis potrebnih informacij (ime obveznosti, prioriteta obveznosti in datum zapadlosti obveznosti)
5. Plačljivi uporabnik vpiše v polja vse potrebne informacije
6. Plačljivi uporabnik s klikom na gumb _Potrdi_ potrdi dodajanje obveznosti
7. Aplikacija preusmeri Plačljiviega uporabnika na Obveznosti, ki ima na ustreznem mestu (glede na prioriteto ali datum zapadlosti) dodano novo obveznost


#### Alternativni tok(ovi)
##### Prijavljen uporabnik
1. Prijavljeni uporabnik na začetni strani ob funkcionalnosti _Obveznosti_ klikne na gumb _Dodaj obveznost_
2. Aplikacija prikaže polja za vpis potrebnih informacij (Ime obveznosti, prioriteta obveznosti in datum zapadlosti obveznosti)
3. Prijavljen uporabnik vpiše v polja vse potrebne informacije
4. Prijavljen uporabnik s klikom na gumb _Potrdi_ potrdi dodajanje obveznosti
5. Aplikacija preusmeri prijavljenega uporabnika na začetno stran

##### Plačljivi uporabnik
1. Plačljivi uporabnik na začetni strani ob funkcionalnosti _Obveznosti_ klikne na gumb _Dodaj obveznost_
2. Aplikacija prikaže polja za vpis potrebnih informacij (Ime obveznosti, prioriteta obveznosti in datum zapadlosti obveznosti)
3. Plačljivi uporabnik vpiše v polja vse potrebne informacije
4. Plačljivi uporabnik s klikom na gumb _Potrdi_ potrdi dodajanje obveznosti
5. Aplikacija preusmeri Plačljiviega uporabnika na začetno stran

#### Izjemni tok(ovi)
* Neprijavljen uporabnik ne more dostopati do funkcionalnosti Obveznosti in posledično dodati nove obveznosti.
* Obveznosti je dosegel maksimalno število obveznosti (npr. 50) in posledično ne more dodati nove.

#### Pogoji
* Neprijavljen uporabnik ima veljaven uporabniški račun (je v preteklosti že opravil registracijo kot novi uporabnik) in je trenutno prijavljen v aplikacijo.
* Obveznosti ni dosegel maksimalno število obveznosti (npr. 50).

#### Posledice
* Prijavljen/ Plačljivi uporabnik ima v Obveznostiu na ustrezno mesto (glede na prioriteto ali datum zapadlosti) dodano novo obveznost.

#### Posebnosti
/

#### Prioriteta identificiranih funkcionalnosti
* SHOULD HAVE

#### Sprejemni testi

* Prijavi se z veljavnim uporabniškim imenom  in geslom; preusmerjen si na začetno stran aplikacije (home page); klikni na gumb _Obveznosti_; preusmerjen si na seznam obveznosti; klikni na gumb _Dodaj obveznost_; v izpisana polja vpiši podatke o obveznosti(npr. ime: APS2 izpit, prioriteta: 1, datum zapadlosti: 3.9.2019) in s klikom na gumb _Potrdi_ shrani spremembe; preusmerjen si na Obveznosti; v seznamu poiščeš novo dodano obveznost.
* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page); ob funkcionalnosti _Obveznosti_ klikni na gumb _Dodaj novo_; v izpisana polja vpiši podatke o obveznosti(npr. ime: APS2 izpit, prioriteta: 1, datum zapadlosti: 3.9.2019) in s klikom na gumb _Potrdi_ shrani spremembe; preusmerjen si na začetno stran; klikni na gumb _Obveznosti_; preusmerjen si na Obveznosti; v seznamu poiščeš novo dodano obveznost.
* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page); klikni na gumb _Obveznosti_; preusmerjen si na seznam obveznosti ki je poln; klikni na gumb _Dodaj obveznost_; aplikacija izpiše obvestilo "Seznam obveznosti je poln!".
* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page); ob funkcionalnosti _Obveznosti_ klikni na gumb _Dodaj obveznost_; aplikacija izpiše obvestilo "Seznam obveznosti je poln!".

### 10. Brisanje obveznosti
#### Povzetek funkcionalnosti
Prijavljen in plačljivi uporabnik lahko iz Obveznosti zbriše poljubne obveznosti, ki še niso dosegle datuma zapadlosti.

#### Osnovni tok
##### Prijavljeni uporabnik
1. Prijavljen uporabnik na začetni strani izbere funkcionalnost _Obveznosti_
2. Aplikacija prikaže scroll seznam z vsemi obveznostmi urejenimi po prioriteti ali datumu zapadlosti
3. Prijavljen uporabnik s klikom na gumb _Izbriši_ izbere funkcionalnost brisanja obveznosti
4. Aplikacija ob vsaki obveznosti prikaže polje za izbiro
5. Prijavljen uporabnik s klikom na polja ob obveznostih izbere katere obveznosti naj se zbrišejo
6. Prijavljen uporabnik s pritiskom na gumb _Izbriši_ potrdi izbiro
7. Aplikacija prikaže obvestilo “Ali ste prepričani, da želite izbrisati X obveznosti?” (X - število izbranih obveznosti)
8. Prijavljen uporabnik s klikom na gumb _Da_ portrdi izbiro
9. Aplikacija preusmeri prijavljenega uporabnika na Obveznosti

##### Plačljivi uporabnik
1. Plačljivi uporabnik na začetni strani izbere funkcionalnost _Obveznosti_
2. Aplikacija prikaže scroll seznam z vsemi obveznostmi urejenimi po prioriteti ali datumu zapadlosti
3. Plačljivi uporabnik s klikom na gumb _Izbriši_ izbere funkcionalnost brisanja obveznosti
4. Aplikacija ob vsaki obveznosti prikaže polje za izbiro
5. Plačljivi uporabnik s klikom na polja ob obveznostih izbere katere obveznosti naj se zbrišejo
6. Plačljivi uporabnik s pritiskom na gumb _Izbriši_ potrdi izbiro
7. Aplikacija prikaže obvestilo “Ali ste prepričani, da želite izbrisati X obveznosti?” (X - število izbranih obveznosti)
8. Plačljivi uporabnik s klikom na gumb _Da_ portrdi izbiro
9. Aplikacija preusmeri Plačljiviega uporabnika na Obveznosti

#### Alternativni tok(ovi)
/

#### Izjemni tok(ovi)
* Neprijavljen uporabnik ne more dostopati do funkcionalnosti _Obveznosti_ in posledično izbrisati obveznosti.
* Obveznosti nima obveznosti, ki bi jih bilo mogoče izbrisati (je prazen).

#### Pogoji
* Neprijavljen uporabnik ima veljaven uporabniški račun (je v preteklosti že opravil registracijo kot novi uporabnik) in je trenutno prijavljen v aplikacijo.
* Obveznosti vsebuje vsaj eno obveznost.

#### Posledice
* Prijavljen/ Plačljivi uporabnik ima v Obveznostiu eno ali več obveznosti manj.

#### Posebnosti
/

#### Prioriteta identificiranih funkcionalnosti
* SHOULD HAVE

#### Sprejemni testi

* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page); klikni na gumb _Obveznosti_; preusmerjen si na seznam obveznosti; klikni na gumb _Izbriši_; izberi nekaj (npr. 3) obveznosti, ki jih želiš izbrisati; klikni na gumb _Izbriši_; ob obvestilu aplikacije klikni na gumb _Da_; preusmerjen si na Obveznosti, ki vsebuje nekaj (npr. 3) obveznosti manj.
* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page); klikni na gumb _Obveznosti_; preusmerjen si na seznam obveznosti, ki je prazen; gumb _Izbriši_ ni prikazan ker ni obveznosti.

### 11. Urejanje obveznosti
#### Povzetek funkcionalnosti
Prijavljen in plačljivi uporabnik lahko izbrani obveznosti poljubno spremeni prioriteto in/ali datum zapadlosti, ter spremembe nato uveljavi.

#### Osnovni tok
##### Prijavljen uporabnik
1. Prijavljen uporabnik na začetni strani izbere funkcionalnost _Obveznosti_
2. Aplikacija prikaže scroll seznam z vsemi obveznostmi urejenimi po prioriteti ali datumu zapadlosti
3. Prijavljen uporabnik s klikom na gumb _Uredi_ ob izbrani obveznosti izbere funkcionalnost urejanja
4. Aplikacija prikaže polja s podrobnostmi obveznosti (ime, prioriteta in datum zapadlosti)
5. Prijavljen uporabnik spremeni vsebino željenih polj
6. Prijavljen uporabnik s pritiskom na gumb _Potrdi_ potrdi spremembe
9. Aplikacija preusmeri prijavljenega uporabnika na Obveznosti

##### Plačljivi uporabnik
1. Plačljivi uporabnik na začetni strani izbere funkcionalnost _Obveznosti_
2. Aplikacija prikaže scroll seznam z vsemi obveznostmi urejenimi po prioriteti ali datumu zapadlosti
3. Plačljivi uporabnik s klikom na gumb _Uredi_ ob izbrani obveznosti izbere funkcionalnost urejanja
4. Aplikacija prikaže polja s podrobnostmi obveznosti (ime, prioriteta in datum zapadlosti)
5. Plačljivi uporabnik spremeni vsebino željenih polj
6. Plačljivi uporabnik s pritiskom na gumb _Potrdi_ potrdi spremembe
9. Aplikacija preusmeri Plačljiviega uporabnika na Obveznosti

#### Alternativni tok(ovi)
/

#### Izjemni tok(ovi)
* Neprijavljen uporabnik ne more dostopati do funkcionalnosti _Obveznosti_ in posledično izbrisati obveznosti.
* Obveznosti nima obveznosti, ki bi jih bilo mogoče urediti (je prazen).

#### Pogoji
* Neprijavljen uporabnik ima veljaven uporabniški račun (je v preteklosti že opravil registracijo kot novi uporabnik) in je trenutno prijavljen v aplikacijo.
* Obveznosti vsebuje vsaj eno obveznost.

#### Posledice
* Prijavljen/ Plačljivi uporabnik ima v Obveznostiu spremenjeno izbrano obveznost.

#### Posebnosti
/

#### Prioriteta identificiranih funkcionalnosti
* COULD HAVE

#### Sprejemni testi

* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page); klikni na gumb _Obveznosti_; preusmerjen si na seznam obveznosti; klikni na gumb _Uredi_ ob izbrani obveznosti;spremni vsebino vsaj enega polja (npr. prioriteta); s klikom na gumb _Potrdi_ potrdi svoje spremembe; preusmerjen si naObveznosti; poišči urejeno obveznost.
* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page); klikni na gumb _Obveznosti_; preusmerjen si na seznam obveznosti, ki je prazen; gumb _Uredi_ ni prikazan ker ni obveznosti.

### 12. Pregled predmetov
#### Povzetek funkcionalnosti
Prijavljen in plačljivi uporabnik lahko podrobneje preuči in pregleda seznam predmetov, ki jih trenutno opravlja.

#### Osnovni tok
##### Prijavljen uporabnik
1. Prijavljen uporabnik na začetni strani izbere funkcionalnost _Predmeti_
2. Aplikacija prikaže scroll seznam z vsemi predmeti, ki jih prijavljeni uporabnik trenutno opravlja
3. Prijavljen uporabnik poljubno scrolla in preglejuje predmete

##### Plačljivi uporabnik
1. Plačljivi uporabnik na začetni strani izbere funkcionalnost _Predmeti_
2. Aplikacija prikaže scroll seznam z vsemi predmeti, ki jih Plačljivi uporabnik trenutno opravlja
3. Plačljivi uporabnik poljubno scrolla in preglejuje predmete

#### Alternativni tok(ovi)
/

#### Izjemni tok(ovi)
* Neprijavljen uporabnik ne more dostopati do funkcionalnosti _Predmeti_.

#### Pogoji

* Neprijavljen uporabnik ima veljaven uporabniški račun (je v preteklosti že opravil registracijo kot novi uporabnik) in je trenutno prijavljen v aplikacijo.

#### Posledice

* Prijavljen/ Plačljivi uporabnik ima prikazane vse predmete, ki jih trenutno opravlja.

#### Posebnosti
/

#### Prioriteta identificiranih funkcionalnosti
* MUST HAVE

#### Sprejemni testi

* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page); klikni na gumb _Predmeti_; preusmerjen si na seznam predmetov; preglej predmete.

### 13. Dodajanje predmeta
#### Povzetek funkcionalnosti
Prijavljen in lačljivi uporabnik lahko v seznam predmetov doda nov predmet.

#### Osnovni tok
##### Prijavljen uporabnik
1. Prijavljen uporabnik na začetni strani izbere funkcionalnost _Predmeti_
2. Aplikacija prikaže scroll seznam z vsemi predmeti, ki jih prijavljeni uporabnik trenutno opravlja
3. Prijavljen uporabnik s klikom na gumb _Dodaj predmet_ izbere funkcionalnost dodajanja novega predmeta
4. Aplikacija prikaže polja za vpis imena predmeta
5. Prijavljen uporabnik ime predmeta v polje
6. Prijavljen uporabnik s klikom na gumb _Potrdi_ potrdi dodajanje predmeta
7. Aplikacija preusmeri prijavljenega uporabnika na predmetnik, ki ima na ustreznem mestu (glede na abecedni vrstni red ali datum dodajanja predmeta) dodan nov predmet

##### Plačljivi uporabnik
1. Plačljivi uporabnik na začetni strani izbere funkcionalnost _Predmeti_
2. Aplikacija prikaže scroll seznam z vsemi predmeti, ki jih Plačljivi uporabnik trenutno opravlja
3. Plačljivi uporabnik s klikom na gumb _Dodaj predmet_ izbere funkcionalnost dodajanja novega predmeta
4. Aplikacija prikaže polja za vpis imena predmeta
5. Plačljivi uporabnik ime predmeta v polje
6. Plačljivi uporabnik s klikom na gumb _Potrdi_ potrdi dodajanje predmeta
7. Aplikacija preusmeri Plačljiviega uporabnika na predmetnik, ki ima na ustreznem mestu (glede na abecedni vrstni red ali datum dodajanja predmeta) dodan nov predmet


#### Alternativni tok(ovi)
##### Prijavljen uporabnik
1. Prijavljen uporabnik na začetni strani ob funkcionalnosti _Predmeti_ klikne na gumb _Dodaj predmet_
2. Aplikacija prikaže polje za vpis imena novega predmeta
3. Prijavljen uporabnik vpiše ime predmeta v polje
4. Prijavljen uporabnik s klikom na gumb _Potrdi_ potrdi dodajanje predmeta
5. Aplikacija preusmeri prijavljenega uporabnika na začetno stran

##### Plačljivi uporabnik
1. Plačljivi uporabnik na začetni strani ob funkcionalnosti _Predmeti_ klikne na gumb _Dodaj predmet_
2. Aplikacija prikaže polje za vpis imena novega predmeta
3. Plačljivi uporabnik vpiše ime predmeta v polje
4. Plačljivi uporabnik s klikom na gumb _Potrdi_ potrdi dodajanje predmeta
5. Aplikacija preusmeri Plačljiviega uporabnika na začetno stran


#### Izjemni tok(ovi)
* Neprijavljen uporabnik ne more dostopati do funkcionalnosti _Predmeti_ in posledično dodati novih predmetov.
* Seznam predmetov je dosegel maksimalno število predmetov (npr. 20) in posledično ne more dodati novega.

#### Pogoji
* Neprijavljen uporabnik ima veljaven uporabniški račun (je v preteklosti že opravil registracijo kot novi uporabnik) in je trenutno prijavljen v aplikacijo.
* Seznam predmetov ni dosegel maksimalnega števila predmetov (npr. 20).

#### Posledice
* Prijavljen/ Plačljivi uporabnik ima v predmetniku na ustrezno mesto (abecedni vrstni red ali datum dodajanja predmeta) dodan novi predmet.

#### Posebnosti
/

#### Prioriteta identificiranih funkcionalnosti
* MUST HAVE

#### Sprejemni testi

* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page); klikni na gumb _Predmeti_; preusmerjen si na predmetnik; klikni na gumb _Dodaj predmet_; v izpisano polje vpiši ime predmeta (npr. Tehnologja programske opreme) in s klikom na gumb _Potrdi_ shrani spremembe; preusmerjen si na seznam predmetov; v seznamu poiščeš nov dodan predmet.
* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page); ob funkcionalnosti _Predmeti_ klikni na gumb _Dodaj predmet_; v izpisano polje vpiši ime predmeta (npr. Tehnologja programske opreme) in s klikom na gumb _Potrdi_ shrani spremembe; preusmerjen si na začetno stran; klikni na gumb _Predmeti_; preusmerjen si na seznam predmetov; v seznamu poiščeš novo dodan predmet.
* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page); klikni na gumb _Predmeti_; preusmerjen si na seznam predmetov, ki je poln; klikni na gumb _Dodaj predmet_; aplikacija izpiše obvestilo "Seznam predmetov je poln!".
* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page); ob funkcionalnosti _Predmeti_ klikni na gumb _Dodaj predmet_; aplikacija izpiše obvestilo "Seznam predmetov je poln!".

### 14. Brisanje predmeta
#### Povzetek funkcionalnosti
Prijavljen in plačljivi uporabnik lahko iz seznama predmetov zbriše poljuben predmet, ki ga je opravil.

#### Osnovni tok
##### Prijavljen uporabnik
1. Prijavljen uporabnik na začetni strani izbere funkcionalnost _Predmeti_
2. Aplikacija prikaže scroll seznam z vsemi predmeti, ki jih prijavljeni uporabnik trenutno opravlja
3. Prijavljen uporabnik s klikom na gumb _Izbriši_ izbere funkcionalnost brisanja predmetov
4. Aplikacija ob vsakem predmetu prikaže polje za izbiro
5. Prijavljen uporabnik s klikom na polja ob predmetih izbere kateri predmeti naj se zbrišejo
6. Prijavljen uporabnik s pritiskom na gumb _Izbriši_ potrdi izbiro
7. Aplikacija prikaže obvestilo “Ali ste prepričani, da želite izbrisati X predmetov?” (X - število izbranih predmetov)
8. Prijavljen uporabnik s klikom na gumb _Da_ portrdi izbiro
9. Aplikacija preusmeri prijavljenega uporabnika na seznam predmetov

##### Plačljivi uporabnik
1. Plačljivi uporabnik na začetni strani izbere funkcionalnost _Predmeti_
2. Aplikacija prikaže scroll seznam z vsemi predmeti, ki jih Plačljivi uporabnik trenutno opravlja
3. Plačljivi uporabnik s klikom na gumb _Izbriši_ izbere funkcionalnost brisanja predmetov
4. Aplikacija ob vsakem predmetu prikaže polje za izbiro
5. Plačljivi uporabnik s klikom na polja ob predmetih izbere kateri predmeti naj se zbrišejo
6. Plačljivi uporabnik s pritiskom na gumb _Izbriši_ potrdi izbiro
7. Aplikacija prikaže obvestilo “Ali ste prepričani, da želite izbrisati X predmetov?” (X - število izbranih predmetov)
8. Plačljivi uporabnik s klikom na gumb _Da_ portrdi izbiro
9. Aplikacija preusmeri Plačljiviega uporabnika na seznam predmetov


#### Alternativni tok(ovi)
/

#### Izjemni tok(ovi)
* Neprijavljen uporabnik ne more dostopati do funkcionalnosti _Predmeti_ in posledično izbrisati predmetov.
* Seznam predmetov nima predmetov, ki bi jih bilo mogoče izbrisati (je prazen).

#### Pogoji
* Neprijavljen uporabnik ima veljaven uporabniški račun (je v preteklosti že opravil registracijo kot novi uporabnik) in je trenutno prijavljen v aplikacijo.
* Seznam predmetov vsebuje vsaj en predmet.

#### Posledice
* Prijavljen/ Plačljivi uporabnik ima v seznamu predmetov enega ali več predmetov manj.

#### Posebnosti
/

#### Prioriteta identificiranih funkcionalnosti
* SHOULD HAVE

#### Sprejemni testi

* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page); klikni na gumb _Predmeti_; preusmerjen si na seznam predmetov; klikni na gumb _Izbriši_; izberi nekaj (npr. 2) predmetov, ki jih želiš izbrisati; klikni na gumb _Izbriši_; ob obvestilu aplikacije klikni na gumb _Da_; preusmerjen si na seznam predmetov, ki vsebuje nekaj (npr. 2) predmeta manj.
* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page); klikni na gumb _Predmeti_; preusmerjen si na seznam predmetov, ki je prazen; gumb _Izbriši_ ni prikazan ker ni predmetov.


### 15. Dodajanje ocene

#### Povzetek funkcionalnosti
Prijavljen in plačljivi uporabnik lahko izbranemu predmetu doda oceno, ter ji določi delež doprinosa k končni oceni predmeta.

#### Osnovni tok
##### Prijavljen uporabnik
1. Prijavljen uporabnik na začetni strani izbere funkcionalnost _Predmeti_ 
2. Aplikacija prikaže scroll seznam z vsemi predmeti, ki jih prijavljeni uporabnik trenutno opravlja
3. Prijavljen uporabnik s klikom na gumb _Dodaj oceno_ ob izbranem predmetu izbere funkcionalnost dodajanja nove ocene
4. Aplikacija prikaže polji kamor mora uporabnik vnesti vrednost ocene in določiti njen delež doprinosa k oceni v procentih
5. Prijavljen uporabnik vnese potrebne informacije v polji
6. Prijavljen uporabnik s pritiskom na gumb _Potrdi_ potrdi spremembe
9. Aplikacija preusmeri prijavljenega uporabnika na seznam predmetov

##### Plačljivi uporabnik
1. Plačljivi uporabnik na začetni strani izbere funkcionalnost _Predmeti_ 
2. Aplikacija prikaže scroll seznam z vsemi predmeti, ki jih plačljivi uporabnik trenutno opravlja
3. Plačljivi uporabnik s klikom na gumb _Dodaj oceno_ ob izbranem predmetu izbere funkcionalnost dodajanja nove ocene
4. Aplikacija prikaže polji kamor mora uporabnik vnesti vrednost ocene in določiti njen delež doprinosa k oceni v procentih
5. Plačljivi uporabnik vnese potrebne informacije v polji
6. Plačljivi uporabnik s pritiskom na gumb _Potrdi_ potrdi spremembe
9. Aplikacija preusmeri plačljivega uporabnika na seznam predmetov


#### Alternativni tok(ovi)
/

#### Izjemni tok(ovi)
* Neprijavljen uporabnik ne more dostopati do funkcionalnosti _Predmeti_ in posledično dodati ocen k predmetom.
* Seznam predmetov nima predmetov, ki bi jim lahko dodali oceno (je prazen).
* Izbran predmet je dosegel maksimalno število ocen (npr. 10).

#### Pogoji
* Neprijavljen uporabnik ima veljaven uporabniški račun (je v preteklosti že opravil registracijo kot novi uporabnik) in je trenutno prijavljen v aplikacijo.
* Seznam predmetov vsebuje vsaj en predmet.
* Izbran predmet ni dosegel maksimalnega števila ocen (npr. 10).

#### Posledice
* Prijavljen/ plačljivi uporabnik ima v izbranem predmetu dodano novo oceno.

#### Posebnosti
/

#### Prioriteta identificiranih funkcionalnosti

* MUST HAVE

#### Sprejemni testi

* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page); klikni na gumb _Predmeti_; preusmerjen si na seznam predmetov; izberi en predmet, ter klikni na gumb _Dodaj  oceno_; v izpisani polji vpiši vrednost ocene in njen doprinos k končni oceni; s klikom na gumb Potrdi shrani spremembe; preusmerjen si na seznam predmetov; v seznamu poiščeš nov dodan predmet in pregledaš njegove podrobnosti s klikom na gumb _Preglej_.
* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page); klikni na gumb _Predmeti_; preusmerjen si na seznam predmetov, ki je prazen; aplikacija izpiše obvestilo "Seznam predmetov je prazen!".
* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page); izberi en predmet, ki ima maksimalno število ocen in ter klikni na gumb _Dodaj  oceno_; aplikacija izpiše obvestilo "Predmet vsebuje maksimalno število ocen!".

### 16. Urejanje ocene
#### Povzetek funkcionalnosti
Prijavljen in plačljivi uporabnik lahko uredi/spremeni oceno pri izbranem predmetu (v primeru, da je npr. ponovno opravljal obveznost predmeta z namenom zviševanja ocene) in spremembo nato uveljavi.

#### Osnovni tok
##### Prijavljen uporabnik
1. Prijavljen uporabnik na začetni strani izbere funkcionalnost _Predmeti_
2. Aplikacija prikaže scroll seznam z vsemi predmeti, ki jih prijavljeni uporabnik trenutno opravlja
3. Prijavljeni uporabnik s klikom na gumb _Uredi_ ob izbranem predmetu izbere funkcionalnost urejanja ocen
4. Aplikacija prikaže polji z ocenami in njihovi deležem doprinosa k končni oceni
5. Prijavljen uporabnik spremeni vsebino željenih polj
6. Prijavljen uporabnik s pritiskom na gumb _Potrdi_ potrdi spremembe
7. Aplikacija preusmeri prijavljenega uporabnika na stran s podrobnostmi predmeta

##### Plačljivi uporabnik
1. Plačljivi uporabnik na začetni strani izbere funkcionalnost _Predmeti_
2. Aplikacija prikaže scroll seznam z vsemi predmeti, ki jih plačljivi uporabnik trenutno opravlja
3. Plačljivi uporabnik s kliko na gumb _Uredi_ ob izbranem predmetu izbere funkcionalnost urejanja ocen
4. Aplikacija prikaže polji z ocenami in njihovi deležem doprinosa k končni oceni
5. Plačljivi uporabnik spremeni vsebino željenih polj
6. Plačljivi uporabnik s pritiskom na gumb _Potrdi_ potrdi spremembe
7. Aplikacija preusmeri plačljivega uporabnika na stran s podrobnostmi predmeta


#### Alternativni tok(ovi)
/

#### Izjemni tok(ovi)
* Neprijavljen uporabnik ne more dostopati do funkcionalnosti _Predmeti_ in posledično urejati ocen predmetov.
* Seznam predmetov nima predmetov, ki bi jim lahko urejali oceno (je prazen).
* Izbran predmet ne vsebuje nobenih ocen.

#### Pogoji
* Neprijavljen uporabnik ima veljaven uporabniški račun (je v preteklosti že opravil registracijo kot novi uporabnik) in je trenutno prijavljen v aplikacijo.
* Seznam predmetov vsebuje vsaj en predmet.
* Izbran predmet ni prazen (vsebuje vsaj eno oceno).

#### Posledice
* Prijavljen/ plačljivi uporabnik ima v izbranem predmetu spremenjeno določeno oceno.

#### Posebnosti
/

#### Prioriteta identificiranih funkcionalnosti

* COULD HAVE

#### Sprejemni testi

* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page); klikni na gumb _Predmeti_; preusmerjen si na seznam predmetov; izberi en predmet in ob njemu sklikni na gumb _Uredi_; v prikazanih poljih za urejanje spremeni eno oceno in njen doprinos k končni oceni predmeta; s klikom na gumb _Potrdi_ shrani spremembe; preusmerjen si na stran podrobnosti predmeta; preglej če so vidne spremembe ocene. 
* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page); klikni na gumb _Predmeti_; preusmerjen si na seznam predmetov, ki je prazen; aplikacija izpiše obvestilo "Seznam predmetov je prazen!".
* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page); klikni na gumb _Predmeti_; preusmerjen si na seznam predmetov; izberi en predmet, ki še nima ocen in klikni na gumb _Uredi_; aplikacija izpiše obvestilo "Izbran predmet nima ocen!".
 

### 17. Pregled prakse
#### Povzetek funkcionalnosti
Plačljivi uporabnik lahko podrobneje preuči in pregleda vsebino svojega izvajanja prakse (število oddelanih ur, urno postavko in svoj zaslužek).

#### Osnovni tok
1. Plačljivi uporabnik na začetni strani izbere funkcionalnost _Praksa_
2. Aplikacija prikaže polja z informacijami o številu oddelanih ur, urni postavki in končnemu zaslužku
3. Plačljivi uporabnik lahko pregleda vse informacije

#### Alternativni tok(ovi)
/

#### Izjemni tok(ovi)
* Neprijavljen uporabnik ne more dostopati do funkcionalnosti _Praksa_ in posledično pregledati svojih informacij o praksi.
* Prijavljeni uporabnik, ki ni plačljivi ne more dostopati do funkcionalnosti _Praksa_ in posledično pregledat pregledati svojih informacij o praksi.
* Plačljivi uporabnik ima onemogočeno funkcionalnost _Praksa_ znotraj nastavitev in ne more dostopati do le-te.

#### Pogoji
* Neprijavljen uporabnik ima veljaven uporabniški račun (je v preteklosti že opravil registracijo kot novi uporabnik) in je trenutno prijavljen v aplikacijo.
* Plačljivi uporabnik ima omogočeno funkcionalnost _Praksa_ znotraj nastavitev.

#### Posledice
* Plačljivi uporabnik ima prikazane vse informacije o svoji praksi.

#### Posebnosti
/

#### Prioriteta identificiranih funkcionalnosti
* SHOULD HAVE

#### Sprejemni testi

* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page); klikni na gumb _Praksa_; preusmerjen si na stran z vsemi informacijami o praksi; preglej prakso.
* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page); klikni na gumb _Praksa_; aplikacija izpiše obvestilo "Funkcionalnost Praksa je onemogočena!".


### 18. Urejanje števila oddelanih ur
#### Povzetek funkcionalnosti
Plačljivi uporabnik lahko v podrobnostih prakse spremeni število oddelanih ur. 

#### Osnovni tok
1. Plačljivi uporabnik na začetni strani izbere funkcionalnost _Praksa_
2. Aplikacija prikaže polja z informacijami o številu oddelanih ur, urni postavki in končnemu zaslužku
3. Plačljivi uporabnik s klikom na gumb _Uredi_ ob polju število oddelanih ur izbere funkcionalnost urejanja
4. Aplikacija prikaže polje za urejanje s številom oddelanih ur
5. Plačljivi uporabnik spremeni vsebino polja
6. Plačljivi uporabnik s pritiskom na gumb _Potrdi_ potrdi spremembe
9. Aplikacija preusmeri plačljivega uporabnika na stran prakse

#### Alternativni tok(ovi)
/

#### Izjemni tok(ovi)
* Neprijavljen uporabnik ne more dostopati do funkcionalnosti _Praksa_ in posledično urediti števila oddelanih ur.
* Prijavljen uporabnik, ki ni plačljivi ne more dostopati do funkcionalnosti _Praksa_ in posledično urediti števila oddelanih ur.
* Plačljivi uporabnik ima onemogočeno funkcionalnost _Praksa_ znotraj nastavitev in ne more dostopati do le-te.

#### Pogoji
* Neprijavljen uporabnik ima veljaven uporabniški račun (je v preteklosti že opravil registracijo kot novi uporabnik) in je trenutno prijavljen v aplikacijo.
* Plačljivi uporabnik ima omogočeno funkcionalnost _Praksa_ znotraj nastavitev.

#### Posledice
* Plačljivi uporabnik ima spremenjeno število oddelanih ur.

#### Posebnosti
/

#### Prioriteta identificiranih funkcionalnosti
* MUST HAVE

#### Sprejemni testi

* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page); klikni na gumb _Praksa_; preusmerjen si na stran z vsemi informacijami o praksi; klikni na gumb _Uredi_ ob številu oddelanih ur; spremeni število ur (npr. povečaj za 8); s klikom na gumb _Potrdi_ potrdi svoje spremembe; preusmerjen si na stran prakse. 
* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page); klikni na gumb _Praksa_; aplikacija izpiše obvestilo "Funkcionalnost Praksa je onemogočena!".


### 19. Urejanje urne postavke
#### Povzetek funkcionalnosti
Plačljivi uporabnik lahko v podrobnostih prakse spremeni vrednost urne postvke. 

#### Osnovni tok
1. Plačljivi uporabnik na začetni strani izbere funkcionalnost _Praksa_
2. Aplikacija prikaže polja z informacijami o številu oddelanih ur, urni postavki in končnemu zaslužku
3. Plačljivi uporabnik s klikom na gumb _Uredi_ ob polju urne postavke izbere funkcionalnost urejanja
4. Aplikacija prikaže polje za urejanje s trenutno vrednostjo urne postavke
5. Plačljivi uporabnik spremeni vsebino polja
6. Plačljivi uporabnik s pritiskom na gumb _Potrdi_ potrdi spremembe
9. Aplikacija preusmeri plačljivega uporabnika na stran prakse

#### Alternativni tok(ovi)
/

#### Izjemni tok(ovi)
* Neprijavljen uporabnik ne more dostopati do funkcionalnosti _Praksa_ in posledično urediti vrednost urne postavke.
* Prijavljen uporabnik, ki ni plačljivi ne more dostopati do funkcionalnosti _Praksa_ in posledično urediti vrednost urne postavke.
* Plačljivi uporabnik ima onemogočeno funkcionalnost _Praksa_ znotraj nastavitev in ne more dostopati do le-te.

#### Pogoji
* Neprijavljen uporabnik ima veljaven uporabniški račun (je v preteklosti že opravil registracijo kot novi uporabnik) in je trenutno prijavljen v aplikacijo.
* Plačljivi uporabnik ima omogočeno funkcionalnost _Praksa_ znotraj nastavitev.

#### Posledice
* Plačljivi uporabnik ima spremenjeno vrednost urne postavke.

#### Posebnosti
/

#### Prioriteta identificiranih funkcionalnosti
* WOULD HAVE

#### Sprejemni testi

* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page); klikni na gumb _Praksa_; preusmerjen si na stran z vsemi informacijami o praksi; klikni na gumb _Uredi_ ob vrednosti urne postavke; spremeni vrednost postavke (npr. povečaj za 0.50€); s klikom na gumb _Potrdi_ potrdi svoje spremembe; preusmerjen si na stran prakse. 
* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page); klikni na gumb _Praksa_; aplikacija izpiše obvestilo "Funkcionalnost Praksa je onemogočena!".

### 20. Pregled urnika
#### Povzetek funkcionalnosti
Prijavljen in plačljivi uporabnik lahko podrobneje preuči in pregleda predmete, ki jih trenutno opravlja v grafični obliki tedenskega urnika.

#### Osnovni tok
##### Prijavljen uporabnik
1. Prijavljen uporabnik na začetni strani izbere funkcionalnost _Urnik_
2. Aplikacija prikaže grafičen prikaz urnika v obliki tabele dnevov v tednu (Ponedeljek - Nedelja) in ur v dnevu (6.00h - 18.00h)
3. Prijavljen uporabnik poljubno pregleda razporeditev predmetov na urniku 

##### Plačljivi uporabnik
1. Plačljivi uporabnik na začetni strani izbere funkcionalnost _Urnik_
2. Aplikacija prikaže grafičen prikaz urnika v obliki tabele dnevov v tednu (Ponedeljek - Nedelja) in ur v dnevu (6.00h - 18.00h)
3. Plačljivi uporabnik poljubno pregleda razporeditev predmetov na urniku 

#### Alternativni tok(ovi)

/

#### Izjemni tok(ovi)

* Neprijavljen uporabnik ne more dostopati do funkcionalnosti _Urnik_.

#### Pogoji

* Neprijavljen uporabnik ima veljaven uporabniški račun (je v preteklosti že opravil registracijo kot novi uporabnik) in je trenutno prijavljen v aplikacijo.

#### Posledice

* Prijavljen/ Plačljivi uporabnik ima grafično prikazane vse svoje predmete v obliki urnika.

#### Posebnosti

/

#### Prioriteta identificiranih funkcionalnosti

* SHOULD HAVE

#### Sprejemni testi

* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page); klikni na gumb _Urnik_; preusmerjen si na Urnik; preglej grafično predstavitev predmetov.

### 21. Dodajanje ur na urnik
#### Povzetek funkcionalnosti 
Prijavljen in plačljivi uporabnik lahko v Urnik na ustrezen dan in uro doda nov predmet.

#### Osnovni tok
##### Prijavljen uporabnik
1. Prijavljen uporabnik na začetni strani izbere funkcionalnost _Urnik_
2. Aplikacija prikaže grafičen prikaz urnika v obliki tabele dnevov v tednu (Ponedeljek - Nedelja) in ur v dnevu (6.00h - 18.00h)
3. Prijavljen uporabnik s klikom na gumb _Dodaj ure_ izbere funkcionalnost dodajanja novih ur predmeta na urnik
4. Aplikacija v prostih celicah urnika prikaže polja za vpis novega predmeta
5. Prijavljen uporabnik v željena polja vpiše ime predmeta, ki ga dodaja na urnik
6. Prijavljen uporabnik s klikom na gumb _Potrdi_ potrdi dodajanje ur na urnik
7. Aplikacija preusmeri prijavljenega uporabnika na Urnik, ki ima na ustreznem mestu (dan in ura) dodan nov predmet

##### Plačljivi uporabnik
1. Plačljivi uporabnik na začetni strani izbere funkcionalnost _Urnik_
2. Aplikacija prikaže grafičen prikaz urnika v obliki tabele dnevov v tednu (Ponedeljek - Nedelja) in ur v dnevu (6.00h - 18.00h)
3. Plačljivi uporabnik s klikom na gumb _Dodaj ure_ izbere funkcionalnost dodajanja novih ur predmeta na urnik
4. Aplikacija v prostih celicah urnika prikaže polja za vpis novega predmeta
5. Plačljivi uporabnik v željena polja vpiše ime predmeta, ki ga dodaja na urnik
6. Plačljivi uporabnik s klikom na gumb _Potrdi_ potrdi dodajanje ur na urnik
7. Aplikacija preusmeri plačljivega uporabnika na Urnik, ki ima na ustreznem mestu (dan in ura) dodan nov predmet

#### Alternativni tok(ovi)
/
#### Izjemni tok(ovi)
* Neprijavljen uporabnik ne more dostopati do funkcionalnosti Urnik in posledično dodati novih ur.
* Urnik je povsem poln (ni več prostih celic za dodajanje ur).

#### Pogoji
* Neprijavljen uporabnik ima veljaven uporabniški račun (je v preteklosti že opravil registracijo kot novi uporabnik) in je trenutno prijavljen v aplikacijo.
* Urnik ni poln (ima vsaj eno prosto celico za dodajanje ur).

#### Posledice
* Prijavljen/ Plačljivi uporabnik ima v Urniku na ustreznem mestu (dan in ura) dodan nov predmet.

#### Posebnosti
/

#### Prioriteta identificiranih funkcionalnosti
* SHOULD HAVE

#### Sprejemni testi

* Prijavi se z veljavnim uporabniškim imenom  in geslom; preusmerjen si na začetno stran aplikacije (home page); klikni na gumb _Urnik_; preusmerjen si na Urnik; klikni na gumb _Dodaj ure_; v željena polja (celice urnika) vpiši ime predmeta, ki ga dodajaš (npr. Diplomski seminar) in s klikom na gumb _Potrdi_ shrani spremembe; preusmerjen si na Urnik;v grafični predstavitvi urnika je na ustreznem mestu (dan in ura) dodan nov predmet.
* Prijavi se z veljavnim uporabniškim imenom in geslom; preusmerjen si na začetno stran aplikacije (home page); klikni na gumb _Urnik_; preusmerjen si na Urnik ki je poln (nia prostih celic); klikni na gumb _Dodaj ure_; aplikacija izpiše obvestilo "Urnik je poln!".

## 6. Nefunkcionalne zahteve

* Sistem mora biti dosegljiv na javno dostopnem spletnem naslovu.
* Sistem mora uporabniku preprečiti dostop do podatkov, za katere ni izrecno pooblaščen.
* Sistem mora biti dnevno na voljo vsaj 99% časa.
* Sistem ne sme biti naenkrat nedosegljiv več kot petnajst minut.
* Sistem mora biti zmožen streči najmanj 10 hkratnim uporabnikom.
* Sistem mora podpirati integracijo z urniki na FRI in FMF.
* Sistem gesel ne sme hraniti v plain text obliki.
* Sistem mora biti polno funkcionalen na vseh aktualnih standardnih platformah.
* Sistem ne sme kršiti zakonodaje o osebnih podatkih.
* Sistem ne sme uporabniku omogočati kakršnegakoli načina za kršenje avtorskih pravic.

## 7. Prototipi vmesnikov

![Zaslonska maska prijvnega zaslona](../img/prijava.png)

* Prijava uporabnika

![Zaslonska maska registracije](../img/registracija.png)

* Registracija novega uporabnika

![Zaslonska maska funkcije obveznosti](../img/obveznosti.png)

* Nastavitve seznama obveznosti
* Pregled seznama obveznosti
* Dodajanje obveznosti
* Brisanje obveznosti
* Urejanje obveznosti

![Zaslonska maska funkcije predmeti](../img/predmeti.png)

* Pregled predmetov
* Dodajanje predmeta
* Urejanje predmeta
* Dodajanje ocene
* Urejanje ocene

![Zaslonska maska funkcije obveznosti](../img/Praksa.png)

* Pregled prakse
* Urejanje števila oddelanih ur
* Urejanje urne postavke

![Zaslonska maska funkcije obveznosti](../img/Urnik.png)

* Pregled urnika
* Urejanje urnika


Vmesnik med aplikacijo in strežnikom z urniki je poizvedba (npr. urnik(vpisna_stevilka)); odgovor je v obliki csv datoteke, 
kjer vsak vnos predstavlja termin na urniku in je sestavljen iz petih podatkov:

* ura začetka,
* ura konca,
* dan v tednu,
* ime predmeta,
* učilnica.
