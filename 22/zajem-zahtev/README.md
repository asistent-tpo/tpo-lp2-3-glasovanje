# Dokument zahtev

|                             |                                                        |
|:----------------------------|:-------------------------------------------------------|
| **Naziv projekta**          | Čiste desetke                                          |
| **Člani projektne skupine** | Timotej Knez, Ana Kronovšek, David Miškić, Katja Logar |
| **Kraj in datum**           | Ljubljana, marec 2019                                  |



## Povzetek projekta


V dokumentu zahtev se nahaja seznam uporabniških vlog za aplikacijo Čiste desetke. 
Neznani pojmi so razloženi v slovarju. 
Najobsežnejši del dokumenta so funkcionalne zahteve, kjer smo za vsako funkcionalnost opisali njihov osnovni tok in možne alternativne ter izjemne tokove, pogoje in posledice dane funkcionalnosti. 
Prav tako smo navedli tudi posebnosti, prioriteto in sprejemne teste.
Za lažje razumevanje interakcije med uporabniškimi vlogami in funkcionalnostmi je priložen tudi UML diagram.
Poleg funkcionalnih zahtev smo opisali tudi nefunkcionalne zahteve, ki smo jih razdelili na zahteve izdelka, organizacijske zahteve in zunanje zahteve.
Na koncu so priložene tudi slike zaslonskih mask in opisani vmesniki do zunanjih sistemov.


## 1. Uvod

Aplikacija Čiste desetke je namenjena študentom v Sloveniji, ki bi si želeli na enem mestu imeti zbrane obveznosti, povezane s študijskim procesom, 
kot so kolokviji, izpiti, roki za izdelke in predavanja ter vaje.
Uporabna vrednost izvira iz veliko različnih obveznosti, ki se dostikrat naberejo študentom, zaradi česar jih je včasih težko obvladovati. 
Naša aplikacija bi nam ponudila možnost za lažje obvladovanje vseh obveznosti na enem mestu ter enostavno ustvarjanje različnih TO-DO listov.
Tako bi študentje dosegli boljšo organizacijo, ob tem pa bi občutili manj stresa, saj bi del organizacije lahko prepustili aplikaciji, prav tako pa bi
aplikacija poskrbela, da se pravočasno spomnimo na obveznosti ter tako povečala količino prostega časa.

Aplikacija je personalizirana do te mere, da je nima smisla uporabljati, če uporabnik ni prijavljen, zato neprijavljen uporabnik ne more dostopati do vseh možnih funkcionalnosti. 
Omogoča registracijo, prijavo in odjavo. 
Šele ko je uporabnik registriran in prijavljen pa lahko nato normalno uporablja aplikacijo.


Aplikacija omogoča uporabnikom vnos poljubno oblikovanih predavanj, vaj ter seminarjev v urnik, tj. tistih obveznosti, ki se periodično ponavljajo vsak teden. 
Te lahko potem študent spremlja na prikazu urnika, ki je del prve strani aplikacije.
Ker verjetno že uporablja Google koledarje ima na voljo tudi možnost uvoza že ustvarjenih dogodkov za dodatno uporabno vrednost aplikacije. 
Poleg tega, ko ustvari dogodek v naši aplikaciji, le-ta poskrbi, da se dogodek ustvari tudi na Google koledarju. Uporabniku tako ni potrebno skrbeti samemu za medsebojno
usklajenost, ob tem pa je Google koledar na voljo na več platformah, kar mu omogoči spremljanje obveznosti tudi z aplikacijami Googla.

Ena od glavnih funkcionalnosti je tudi vnašanje obveznosti, te lahko študent kasneje tudi spreminja in jih dodaja na različne TO-DO liste.
Ko je obveznost zaključena, jo lahko potrdi in s tem dobi tudi psihološki občutek ugodja, saj je njegova količina dela vidno manjša.

Čiste desetke omogoča tudi ustvarjanje in pregled TO-DO listov, na katere lahko študent dodaja poljubne obveznosti.  
Že na začetku uporabe pa so na voljo 3 TO-DO listih (prioritetni TO-DO listi), na katerih se nahajajo obveznosti, razdeljene glede na prioriteto - tj. TO-DO list z visoko, srednjo in nizko prioriteto.


Za boljšo prirejenost študentskemu načinu življenja je urnik organiziran v semestre, točne datume pa vnaša skrbnik, saj se ne začnejo vsako leto na enake datume. 

Ker smo sami študenti FRI, smo se za njih še posebej potrudili, saj smo opazili možnost, da bi lahko izboljšali prikaz FRI urnika, ter ga ponudili v bolj priročni obliki za študente.
Študentom FRI je omogočena posebna funkcionalnost, s katero lahko vnesejo samo svojo vpisno številko, sistem pa nato sam poskrbi za uvoz njihovega urnika s fakultetne strani.

Projekt mora zadostiti tudi več nefunkcionalnim zahtevam. Pomembno je, da bo aplikacija dovolj odzivna, da bo uporabna tudi, ko jo bo uporabljalo veliko študentov hkrati.
Pričakujemo, da bo aplikacijo uporabljalo nekje do 50 študentov istočasno, zato mora biti aplikacija odzivna tudi pri obremenitvi takšnega števila uporabnikov.
Aplikacijo upoštevamo kot odzivno, če je strežnik na vsako zahtevo sposoben odgovoriti v največ treh sekundah. Zahteva se tudi nekaj stvari povezanih z varnostjo, kot sta komunikacija preko
zaščitenega HTTPS kanala in avtomatsko varnostno kopiranje podatkovne baze. Med razvojem aplikacije je potrebno skrbeti za urejeno kodo, hkrati pa je potrebno zadostiti tudi zakonom, ki veljajo za takšne projekte.


## 2. Uporabniške vloge

* **študent** (lahko dodaja in ureja svoje obveznosti ter jih dodaja na različne TO-DO liste)
* **študent FRI** (poleg funkcionalnosti študenta lahko uvozi svoj fakultetni urnik prek vpisne številke)
* **skrbnik** (vnaša datume semestrov)
* **neregistriran uporabnik** (lahko se registrira)


## 3. Slovar pojmov

* **predavanje** - govorno podajanje učne snovi v prostorih fakultete, ponavadi vsak teden ob točno določenem času
* **vaje** - praktična uporaba snovi ob pomoči asistenta v prostorih fakultete, ponavadi vsak teden v večih terminih
* **seminar** - skupina študentov/pedagog bolj podrobno predstavi določeno tematiko, izvaja se ob določeni uri v prostorih fakultete
* **izpit** - končni preizkus znanja, ki ga izvede profesor, udeležijo pa se ga študentje, ki po končanem preizkusu prejmejo oceno
* **prijava na izpit** - odprta nekaj dni pred izpitom in je obvezna za opravljanje izpita; ponavadi jo študentje opravijo preko študijskega informacijskega sistema
* **kolokvij** - delni preizkus znanja, ki ga izvede profesor, udeležijo pa se ga študentje; lahko je pogoj ali pa nadomestilo za opravljanje izpita
* **izdelek** - programska koda in/ali seminarska naloga, ki mora ustrezati določenim zahtevam in jo študent ali skupina študentov ponavadi napiše doma ali na vajah
* **rok** - določen datum, do katerega naj bi vsi študentje oddali svoj izdelek/mora biti obveznost opravljena
* **predmet** - učno področje, organizirano na fakulteti kot sklop predavanj, vaj in preizkusov znanja
* **preizkus znanja** - izpit, kolokvij ali izdelek, ki ga pregleda pedagog in na njegovi podlagi določi oceno ali del ocene
* **ocena** - celo število iz intervala [5, 10], ki označuje uteženo povprečje uspešnosti pri opravljanju preizkusov znanja pri enem predmetu
* **obveznost** - izpit, kolokvij, rok oddaje izdelka, domače naloge ali rok prijave na izpit, vse imajo rok (datum) do katerega morajo biti opravljene
* **ponavljajoča obveznost** - predavanja, vaje, seminar
* **skupina** - skupina študentov z istim ciljem, na primer opraviti določen predmet
* **semester** - polovica študijskega leta, ki služi kot zaključena celota za predmete - na koncu semestra se opravljajo zaključni preizkusi znanja za predmete, ki so se začeli na začetku tega semestra (ker bomo prikazovali na urniku obveznosti s trajanjem, se semester zaključi takrat, ko se zaključi pedagoška dejavnost)
* **zimski semester** - semester, ki traja od oktobra do januarja 
* **poletni semester*** - semester, ki traja od februarja do junija
* **TO-DO list** - seznam obveznosti, ki jih študent še mora opraviti
* **prioriteta** - študentova ocena, kako zahtevna je posamezna aktivnost, lahko zaseda vrednosti visoka, srednja, nizka
* **pedagog** - profesor ali asistent na fakulteti
* **fakulteta** - matična ustanova študenta, kjer potekajo njegove obveznosti in kjer delajo pedagogi
* **prioritetni TO-DO list** - TO-DO list, na katerem so obveznosti z enako prioriteto (visoko, srednjo, nizko)
* **potrjevanje obveznosti** - ko je obveznost zaključeno, jo študent lahko potrdi kot opravljeno, od takrat naprej na TO-DO listih ni več vidna
* **vpisna številka** - enolični identifikator študenta, ki jo vsak študent prejme ob vpisu na fakulteto
* **FRI** - Fakulteta za računalništvo in informatiko, Univerza v Ljubljani


## 4. Diagram primerov uporabe

Prikazan je diagram primerov uporabe v jeziku UML. Poudariti bi želeli, da so akterji na diagramu prikazani dvakrat, ker je diagram razdeljen na dva dela za lažjo berljivost.

![Primeri uporabe 1](../img/use_cases_1.png)
![Primeri uporabe 2](../img/use_cases_2.png)



## 5. Funkcionalne zahteve


Naša aplikacija bo obsegala sledeče funkcionalnosti:

| prioriteta | naziv                                          | študent  | študent FRI | skrbnik  | neregistriran uporabnik |
|:-----------|:-----------------------------------------------|:---------|:------------|:---------|:------------------------|
| must       | vnos ponavljajoče obveznosti v urnik           | [x]      | [x]         |          |                         |
| must       | prikaz urnika                                  | [x]      | [x]         |          |                         |
| must       | vnos nove obveznosti                           | [x]      | [x]         |          |                         |
| should     | spreminjanje obveznosti                        | [x]      | [x]         |          |                         |
| should     | izdelava TO-DO lista                           | [x]      | [x]         |          |                         |
| must       | registracija novega uporabnika                 |          |             |          | [x]                     |
| must       | prijava obstoječega uporabnika                 | [x]      | [x]         | [x]      |                         |
| must       | odjava                                         | [x]      | [x]         | [x]      |                         |
| should     | prikaz seznama TO-DO listov                    | [x]      | [x]         |          |                         |
| should     | prikaz TO-DO lista                             | [x]      | [x]         |          |                         |
| should     | potrjevanje obveznosti                         | [x]      | [x]         |          |                         |
| must       | prikaz obveznosti                              | [x]      | [x]         |          |                         |
| would      | uvažanje urnika FRI                            |          | [x]         |          |                         |
| could      | sinhronizacija koledarja z Google Calendar API | [x]      | [x]         |          |                         |
| could      | prikaz semestrov                               |          |             | [x]      |                         |
| could      | dodajanje novega semestra                      |          |             | [x]      |                         |
| could      | nastavljanje podatkov semestra                 |          |             | [x]      |                         |


### Vnos v urnik

#### Povzetek funkcionalnosti
Študent lahko doda predavanja, vaje, seminar ali drugo ponavljajočo obveznost, ta se doda na urnik vsak teden od takrat do konca izbranega semestra.

#### Osnovni tok
1. Študent izbere funkcionalnost *Vnos v urnik*.
2. Sistem prikaže obrazec z vsemi možnostmi - ime predmeta, vnos predavanj/vaj/seminarja/drugo, dan v tednu, ura in semester.
3. Študent izbere željene možnosti in potrdi izbiro.
4. Sistem ustvari novo ponovljajočo obveznost.
5. Sistem prikaže sporočilo o uspešnem vnosu.
6. Sistem na urniku prikaže dodano aktivnost.

#### Izjemni tokovi
1. Študent izbere funkcionalnost *Vnos v urnik*
2. Sistem prikaže obrazec z vsemi možnostmi - ime predmeta, vnos predavanj/vaj/seminarja/drugo, dan v tednu, ura in semester.
3. Študent izbere čas, ki se prekriva z že obstoječimi predavanji/vajami/seminarji.
4. Sistem prikaže opozorilo o prekrivanju.

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot študent. Če ni prijavljen, ali pa če je prijavljen kot skrbnik, mu funkcionalnost ni na voljo.

#### Posledice
Če je vnos v urnik uspešen, zdaj urnik prikazuje tudi nov vnos, poleg tega se na študentov TO-DO list vsak teden do konca semestra vnos doda kot obveznost.

#### Prioriteta
Must have. Gre za eno ključnih funkcionalnosti sistema.

#### Sprejemni testi
* Prijavi se kot študent, izberi *Vnos v urnik*, željene možnosti, in potrdi izbiro.
* Prijavi se kot študent, vnesi željeno ponavljajočo obveznost, nato pa vnesi še eno ponavljajočo obveznost, ki se s prejšnjim vnosom časovno prekriva, sistem mora prikazati opozorilo.



### Prikaz urnika

#### Povzetek funkcionalnosti
Študent lahko vidi vse ponavljajoče obveznosti prikazane v obliki koledarja.

#### Osnovni tok
1. Študent navigira do strani s svojim urnikom.
2. Sistem prikaže njegove ponavljajoče obveznosti v koledarju za vsak teden.

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot študent. Če ni prijavljen, ali pa če je prijavljen kot skrbnik, mu funkcionalnost ni na voljo.

#### Posledice
Na zaslonu se prikaže študentov urnik.

#### Prioriteta
Must have. To je ključna funkcionalnost sistema.

#### Sprejemni testi
* Prijavi se kot študent, izberi *Prikaz urnika*, pokaže se koledar z njegovim urnikom.



### Vnos nove obveznosti

#### Povzetek funkcionalnosti
Študent lahko doda novo obveznost - določi ime, rok, prioriteto, lahko pa tudi opis. Prav tako lahko izbere na katerem TO-DO listu bo obveznost prikazana (avtomatsko na TO-DO listih, kjer so obveznosti razvrščene po prioriteti). 

#### Osnovni tok
1. Študent izbere funkcionalnost *Nova obveznost*.
2. Sistem prikaže obrazec za vnos imena obveznosti, opisa, rok, prioritete in TO-DO lista ali listov.
3. Študent izpolni obrazec in potrdi izbiro (ne izbere nobenega izmed TO-DO listov, izbere pa prioriteto).
4. Sistem ustvari novo obveznost.
5. Sistem obveznost doda na prioritetni TO-DO list.
6. Sistem prikaže sporočilo o uspešno dodani obveznosti.

#### Alternativni tok 1
1. Študent izbere funkcionalnost *Nova obveznost*.
2. Sistem prikaže obrazec za vnos imena obveznosti, opisa, rok, prioritete in TO-DO lista ali listov.
3. Študent izpolni obrazec in potrdi izbiro (poleg prioritete izbere tudi željen(e) dodatne TO-DO list(e)).
4. Sistem ustvari novo obveznost.
5. Sistem obveznost doda na prioritetni TO-DO list.
6. Sistem obveznost doda na izbran(e) TO-DO list(e).
7. Sistem prikaže sporočilo o uspešno dodani obveznosti.

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot študent. Če ni prijavljen, ali pa če je prijavljen kot skrbnik, mu funkcionalnost ni na voljo.

#### Posledice
Študentov ima zdaj 1 obveznost več, ki se prikaže na pripadajočem prioritetnem TO-DO listu. Če je študent izbral še kakšen TO-DO list, se obveznost prikaže tudi na njem.

#### Posebnosti
Ker se bodo obveznosti sinhronizirale z Google koledarjem, je potrebno zagotoviti, da bo naš format obveznosti kompatibilen z Googlovim.

#### Prioriteta
Must have. To je ključna funkcionalnost aplikacije.

#### Sprejemni testi
* Prijavi se kot študent in dodaj novo obveznost, ta se prikaže na seznamu obveznosti.



### Spreminjanje obveznosti

#### Povzetek funkcionalnosti
Študent lahko spremeni lastnosti obveznosti, kot so ime, opis, rok, ali prioriteto. Lahko jo tudi dodaja na oziroma briše iz obstoječih TO-DO listov.

#### Osnovni tok
1. Študent navigira na svoj TO-DO list.
2. Študent izbere funkcionalnost *Uredi* ob željeni obveznosti.
2. Sistem prikaže obrazec za spreminjanje izbranih lastnosti.
3. Študent izpolni obrazec in potrdi izbiro.
4. Sistem spremeni obveznost.
5. Sistem doda/izbriše obveznost iz ustreznih TO-DO listov glede na to, kaj je študent spremenil.
6. Sistem prikaže sporočilo o uspešno spremenjeni obveznosti.

#### Alternativni tok 1
1. Študent navigira na prikaz vseh obveznosti.
2. Študent izbere funkcionalnost *Uredi* ob željeni obveznosti.
2. Sistem prikaže obrazec za spreminjanje izbranih lastnosti.
3. Študent izpolni obrazec in potrdi izbiro.
4. Sistem spremeni obveznost.
5. Sistem doda/izbriše obveznost iz ustreznih TO-DO listov glede na to, kaj je študent spremenil.
6. Sistem prikaže sporočilo o uspešno spremenjeni obveznosti.

#### Pogoji
Uporabnik mora biti prijavljen v sistem. Če je v sistem prijavljen kot skrbnik, mu funkcionalnost ni na voljo.

#### Posledice
Obveznost ima spremenjene lastnosti. Če se spremeni prioriteta, se obveznost izbriše s trenutnega prioritetnega TO-DO lista in ustvari na novem prioritetnem TO-DO listu, ki ustreza novi prioriteti. Če se obveznost doda na TO-DO list, se na njem ustvari, če se jo izbriše, je na tistem TO-DO listu ni več.

#### Prioriteta
Should have. Tudi če obveznosti ne moremo spreminjati, aplikacija vseeno deluje v okrnjeni obliki.

#### Sprejemni testi
* Prijavi se kot študent, izberi obveznost na prikazu vseh obveznosti in spremeni naslov. Obveznost mora zdaj biti prikazana z novim naslovom.
* Prijavi se kot študent, izberi obveznost v TO-DO listu in spremeni prioriteto. Obveznost se mora izbrisati iz prejšnjega prioritetnega TO-DO lista in se pojaviti na tistem prioritetnem TO-DO listu, čigar prioriteto si izbral.
* Prijavi se kot študent, izberi obveznost v TO-DO listu in obveznost dodaj na nov TO-DO list. Obveznost se mora pojaviti tudi na tem TO-DO listu.



### Izdelava TO-DO lista

#### Povzetek funkcionalnosti
Študent lahko ustvari nov TO-DO list in nanj lahko doda obveznosti.

#### Osnovni tok
1. Študent izbere funkcionalnost *Ustvari nov TO-DO list*.
2. Sistem vrne obrazec za vnos naslova in izbiro obveznosti.
3. Študent vnese ime TO-DO lista.
4. Iz spustnega seznama izbere vse obveznosti, ki jih želi imeti na novem TO-DO listu.
5. Študent potrdi izbiro.
6. Sistem ustvari nov TO-DO list z izbranimi obveznostmi
7. Sistem prikaže sporočilo o uspešni izdelavi novega TO-DO lista.
8. Na strani se prikaže nov TO-DO list.

### Alternativni tok 1
1. Študent izbere funkcionalnost *Ustvari nov TO-DO list*.
2. Sistem vrne obrazec za vnos naslova in izbiro obveznosti.
3. Študent vnese ime TO-DO lista.
4. Študent potrdi izbiro.
5. Sistem ustvari prazen nov TO-DO list.
6. Sistem prikaže sporočilo o uspešni izdelavi novega TO-DO lista.
7. Na strani se prikaže nov TO-DO list.


#### Pogoji
Prijavljen kot študent, v vsakem drugem primeru dostopa do dodajanja TO-DO listov nima. 

#### Posledice
Nov TO-DO list se prikaže na seznamu TO-DO listov z dodanimi obveznostmi, če so bile le-te izbrane. Študent lahko sedaj obveznosti dodaja na nov TO-DO list.

#### Posebnosti
Med ponujenimi obveznostmi na spustnem seznamu se mu prikažejo samo nepotrjene.

#### Prioriteta
Should have. Brez te funkcionalnosti bo uporaba aplikacije okrnjena.

#### Sprejemni testi
* Prijavi se kot študent in izberi *Ustvari nov TO-DO list*, vpiši naslov, če obstajajo obveznosti, 1 dodaj na TO-DO list, potrdi TO-DO list, na seznamu TO-DO listov je sedaj 1 TO-DO list več.



### Registracija novega uporabnika

#### Povzetek funkcionalnosti
Neregistriran uporabnik si lahko ustvari uporabniški račun.

#### Osnovni tok
1. Neregistriran uporabnik izbere funkcionalnost *Registriraj se*.
2. Sistem prikaže stran z obrazcem, ki vsebuje vnosna polja za uporabniško ime in geslo ter ponovitev gesla.
3. Neregistriran uporabnik izpolni obrazec in potrdi izbiro.
4. Sistem ustvari novega uporabnika.
5. Sistem prikaže sporočilo o uspešnosti.
6. Neregistriran uporabnik postane registrirani uporabnik/študent ter se lahko vpiše.

#### Izjemni tokovi
1. Neregistriran uporabnik izbere funkcionalnost *Registriraj se*.
2. Sistem prikaže stran z obrazcem, ki vsebuje vnosna polja za uporabniško ime in geslo ter ponovitev gesla.
3. Neregistriran uporabnik v obrazec napiše neveljavne podatke (prekratko geslo, neveljaven email naslov ...).
4. Uporabnik pritisne gumb za oddajo.
5. Sistem prikaže ustrezno opozorilo, označi neustrezno polje in prepreči oddajo.

#### Pogoji
Obiskovalec strani ni prijavljen ali registriran v aplikacijo.

#### Posledice
V sistemu se ustvari vnos za novega uporabnika tipa študent.

#### Posebnosti
* Uporabniško ime mora biti v obliki emaila. 
* Ustvarjanje skrbniškega računa na ta način ni možno. Skrbniški račun je en sam in je ustvarjen še pred začetkom delovanja aplikacije.

#### Prioriteta
Must have. Aplikacija ponuja funkcionalnosti le registriranim uporabnikom, če se ne morejo registrirati, aplikacije ne morejo uporabljati.

#### Sprejemni testi
* Navigiraj na funkcionalost za registracijo, ustrezno izpolni vnosna polja obrazca, sistem prikaže sporočilo o uspešnosti.
* Navigiraj na funkcionalost za registracijo, podatke izpolni neustrezno (napačno geslo), sistem prikaže opozorilo in prepreči oddajo.



### Prijava obstoječega uporabnika

#### Povzetek funkcionalnosti
Obstoječi uporabnik (študent ali skrbnik) se lahko prijavi v sistem.

#### Osnovni tok
1. Obstoječi uporabnik izbere funkcionalnost *Prijava*.
2. Sistem prikaže stran z obrazcem, ki vsebuje vnosna polja za uporabniško ime in geslo.
3. Uporabnik izpolni obrazec in potrdi izbiro.
4. Sistem prijavi uporabnika.
5. Sistem prikaže sporočilo o uspešnosti.
6. Uporabnik lahko začne uporabljati aplikacijo.

#### Izjemni tok 1
1. Obstoječi uporabnik izbere funkcionalnost *Prijava*.
2. Sistem prikaže stran z obrazcem, ki vsebuje vnosna polja za uporabniško ime in geslo.
3. Uporabnik vpiše napačno uporabniško ime in potrdi izbiro.
4. Sistem uporabnika obvesti o nepravilnem uporabniškem imenu in ne dovoli prijave.

#### Izjemni tok 2
1. Obstoječi uporabnik izbere funkcionalnost *Prijava*.
2. Sistem prikaže stran z obrazcem, ki vsebuje vnosna polja za uporabniško ime in geslo.
3. Uporabnik vpiše obstoječe uporabniško ime, vendar neustrezno geslo in potrdi izbiro.
4. Sistem uporabnika obvesti o nepravilnem geslu in ne dovoli prijave.

#### Pogoji
Obiskovalec strani ni prijavljen v aplikacijo.

#### Posledice
Uporabnik je prijavljen in lahko uporablja funkcionalnosti, ki neprijavljenemu uporabniku niso na voljo.

#### Posebnosti
Skrbnik se na ta način lahko prijavi v aplikacijo.

#### Prioriteta
Must have. Aplikacija ponuja ključne funkcionalnosti le prijavljenim uporabnikom - če se ne morejo prijaviti, aplikacije ne morejo uporabljati.

#### Sprejemni testi
* Navigiraj na funkcionalost za prijavo in ustrezno izpolni vnosna polja obrazca. Po kliku na gumb Prijava moraš biti prijavljen.
* Navigiraj na funkcionalost za prijavo in neustrezno (napačno geslo) izpolni vnosna polja obrazca. Po kliku na gumb Prijava mora sistem vrniti sporočilo o napaki.



### Odjava

#### Povzetek funkcionalnosti
Prijavljen uporabnik (študent ali skrbnik) se lahko odjavi iz sistema.

#### Osnovni tok
1. Prijavljen uporabnik izbere funkcijo *Odjava*.
2. Sistem odjavi uporabnika.
3. Sistem preusmeri uporabnika na začetno stran.

#### Pogoji
Uporabnik je prijavljen v aplikacijo.

#### Posledice
Uporabnik je odjavljen in mu funkcionalnosti (razen prijave in registracije) niso več na voljo.


#### Prioriteta
Must have. Aplikacija potrebuje delujočo odjavo za normalno delovanje.

#### Sprejemni testi
* Prijavi se kot študent, nato izberi funkcionalnost *Odjava*. Sistem te odjavi, ne moreš več uporabljati nobene funkcionalnosti, razen prijave in registracije.



### Prikaz seznama TO-DO listov

#### Povzetek funkcionalnosti
Študent lahko vidi vse TO-DO liste, ki jih je ustvaril, prav tako pa tudi prioritetne TO-DO liste.

#### Osnovni tok
1. Študent izbere funkcionalnost *Prikaz TO-DO listov*.
2. Sistem prikaže stran s seznamom vseh njegovih TO-DO listov.

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot študent. Če ni prijavljen, ali pa če je prijavljen kot skrbnik, mu funkcionalnost ni na voljo.

#### Posledice
Sistem zdaj prikazuje seznam vseh njegovih TO-DO listov.

#### Prioriteta
Should have. Brez te funkcionalnosti bo uporaba aplikacije okrnjena.

#### Sprejemni testi
* Prijavi se kot študent, klikni na zavihek TO-DO listi, mora se odpreti seznam.



### Prikaz TO-DO lista

#### Povzetek funkcionalnosti
Študent lahko vidi izbrani TO-DO list in obveznosti na njem

#### Osnovni tok
1. Študent iz seznama TO-DO listov izbere posamezni TO-DO list.
2. Sistem prikaže TO-DO list z vsemi njegovimi obveznostmi.

#### Izjemni tok
1. Študent iz seznama TO-DO listov izbere posamezni TO-DO list
2. TO-DO list je prazen, na njem ni nobene obveznosti
3. Sistem napiše obvestilo, da trenutno za ta TO-DO list ne obstaja nobena obveznost.

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot študent. Če ni prijavljen, ali pa če je prijavljen kot skrbnik, mu funkcionalnost ni na voljo.

#### Posledice
Prikazan izbrani TO-DO list.

#### Prioriteta
Should have. Brez te funkcionalnosti bo uporaba aplikacije okrnjena.

#### Sprejemni testi
* Prijavi se kot študent, izberi TO-DO list, ki se mora prikazati skupaj z vsemi njegovimi obveznostmi.



### Potrjevanje obveznosti

#### Povzetek funkcionalnosti
Študent lahko posamezno aktivnost označi kot opravljeno.

#### Osnovni tok
1. Sistem prikaže TO-DO list z vsemi njegovimi obveznostmi.
2. Študent klikne na polje za potrjevanje obveznosti.
3. Sistem prikaže okno za potrditev, da je obveznost opravljena.
4. Študent potrdi, da je obveznost opraveljena.
5. Za obveznost se zabeleži, da je potrjena.
6. Uporabniku se obveznost na vseh TO-DO listih, kjer je bila obveznost prisotna, ne prikazuje več.

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot študent. Če ni prijavljen, ali pa če je prijavljen kot skrbnik, mu funkcionalnost ni na voljo.

#### Posledice
Obveznost se na TO-DO listu se prikazuje več. Obveznost je zabeležena kot potrjena.

#### Prioriteta
Should have. Brez te funkcionalnosti bo uporaba aplikacije okrnjena.

#### Sprejemni testi
* Prijavi se kot študent, izberi TO-DO list in označi obveznost kot opravljeno, obveznosti ni več na seznamu nepotrjenih obveznosti.



### Prikaz obveznosti

#### Povzetek funkcionalnosti
Študent lahko vidi vse obveznosti na enem mestu.

#### Osnovni tok
1. Študent izbere funkcionalnost *Prikaz obveznosti*.
2. Sistem prikaže seznam z vsemi njegovimi nepotrjenimi obveznostmi.

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot študent. Če ni prijavljen, ali pa če je prijavljen kot skrbnik, mu funkcionalnost ni na voljo.

#### Posledice
Prikazan seznam obveznosti.

#### Prioriteta
Must have. Brez te funkcionalnosti je uporaba aplikacije nemogoča.

#### Posebnosti
Študentu se prikažejo samo nepotrjene obveznosti, lahko pa izbere tudi, da se mu prikažejo samo potrjene obveznosti.

#### Sprejemni testi
* Prijavi se kot študent, navigiraj do prikaza obveznosti, morajo se prikazati vse še nepotrjene obveznosti.



### Uvažanje urnika FRI

#### Povzetek funkcionalnosti
Študent FRI lahko v urnik aplikacije uvozi tudi svoj fakultetni urnik s termini vaj in predavanj.

#### Osnovni tok
1. Študent FRI izbere funkcionalnost *Uvozi FRI urnik*
2. Sistem prikaže vnosno polje za vpisno številko
3. Študent FRI vnese vpisno številko
4. Sistem z vpisno številko pridobi urnik s strani https://urnik.fri.uni-lj.si/
5. Sistem doda predavanja/vaje v urnik in prikaže obvestilo o uspehu

#### Izjemni tokovi
1. Študent FRI izbere funkcionalnost *Uvozi FRI urnik*
2. Sistem prikaže vnosno polje za vpisno številko
3. Študent FRI vnese vpisno številko
4. Sistem s to vpisno številko ne more pridobiti urnika
5. Sistem prikaže obvestilo o nesupehu

#### Pogoji
Uporabnik mora imeti veljavno vpisno številko FRI in zanjo mora obstajati urnik, sicer funkcionalnost ne deluje.

#### Posledice
V urniku uporabnika se prikažejo obveznosti iz urnika FRI.

#### Posebnosti
Ker na urniku FRI lahko pride do prekrivanj, mi pa tega v naši aplikaciji ne dovolimo, bo sistem kot pravilno ponavljajočo obveznost vzel prvo, na katero naleti. Če potem naleti na naslednjo, ki se prekriva, je sistem ne doda na urnik.

#### Prioriteta
Would have. Ker tarčna publika aplikacije niso ekskluzivno študenti FRI, ta funkcionalnost ni nujna za uporabno vrednost aplikacije, ko pa bo implementirana, bo dostopna le deležu uporabnikov.

#### Sprejemni testi
* Prijavi se kot študent, izberi uvoz in vnesi vpisno številko. Po potrditvi se prikaže obvestilo o uspehu ter obveznosti na koledarju.



### Sinhronizacija koledarja z Google Calendar API

#### Povzetek funkcionalnosti
Študent lahko obveznosti iz urnika aplikacije prenese na koledar svojega Google računa.

#### Osnovni tok
1. Uporabnik navigira na stran z urnikom.
2. Uporabnik izbere funkcionalnost *Sinhroniziraj z Googlom*
3. Pojavi se okno, kamor uporabnik vnese svoje podatke Google računa.
4. Uporabnik potrdi vnos.
5. Sistem ob uspešni prijavi in sinhronizaciji obvesti uporabnika.

#### Izjemni tok 1
1. Uporabnik navigira na stran z urnikom.
2. Uporabnik izbere funkcionalnost *Sinhroniziraj z Googlom*
3. Pojavi se okno, kamor uporabnik vnese napačne podatke Google računa.
4. Uporabnik potrdi vnos.
5. Prijava v google spodleti.
6. Sistem uporabnika o tem obvesti.

#### Izjemni tok 2
1. Uporabnik navigira na stran z urnikom.
2. Uporabnik izbere funkcionalnost *Sinhroniziraj z Googlom*
3. Pojavi se okno, kamor uporabnik vnese svoje podatke Google računa.
4. Uporabnik potrdi vnos.
5. Med sinhronizacijo koledarja pride do napake.
6. Sistem uporabnika o tem obvesti.

#### Izjemni tok 3
1. Uporabnik ustvari novo opravilo.
2. Pri ustvarjanju opravila na Google koledarju pride do napake.
3. Sistem uporabnika o tem obvesti.

#### Pogoji
Uporabnik ima Google račun.

#### Posledice
Na koledarju se pojavijo opravila, ki jih je uporabnik ustvaril v aplikaciji.

#### Posebnosti
Tu je potrebno uporabiti API za Google koledar.

#### Prioriteta
Could have. Funkcionalnost se nanaša le na tiste uporabnike z Google računom, ki uporabljajo Google Calendar, ne pa na vse. Funkcionalnost ni nujna za delovanje aplikacije.

#### Sprejemni testi
* Navigiraj na nastavitve, izberi funkcionalnost *Sinhroniziraj z Googlom* in ustrezno izpolni vnosna polja obrazca. Počakaj da sistem prikaže sporočilo o uspehu in poglej sinhronizirana opravila na Google Calendar.
* Navigiraj na nastavitve, izberi funkcionalnost *Sinhroniziraj z Googlom* in izpolni vnosna polja obrazca z napačnimi podatki. Sistem mora prikazati obvestilo o napaki.



### Prikaz semestrov

#### Povzetek funkcionalnosti
Skrbnik aplikacije lahko vidi vse trenutno ustvarjene semestre.

#### Osnovni tok
1. Skrbnik v navigacijski vrstici izbere funkcionalnost *Semestri*.
2. Sistem prikaže seznam ustvarjenih semestrov.

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot skrbnik. V nasprotnem primeru mu ta funkcionalnost ni na voljo.

#### Posledice
Uporabniku se pirkaže stran z seznamom vseh semestrov.

#### Prioriteta
Could have. Ker so začetki in konci semestrov precej predvidljivi, bi verjetno zadoščalo, da so njihovi podatki zapisani že v programski kodi.

#### Sprejemni testi
* Prijavi se kot skrbnik, ter pojdi na stran aktivnih semestrov. Na ekranu se ti mora prikazati seznam vseh semstrov.



### Dodajanje novega semestra

#### Povzetek funkcionalnosti
Skrbnik aplikacije lahko doda nov semester, ki je nato uporabnikom na voljo za izbiro, ko izbirajo semester, v katerem se izvaja aktivnost.

#### Osnovni tok
1. Skrbnik v navigacijski vrstici izbere funkcionalnost *Semestri*.
2. Sistem prikaže seznam ustvarjenih semestrov.
3. Skrbnik na seznamu izbere možnost *Dodaj nov semester*.
4. Sistem prikaže okno za nastavljanje podatkov semestra - ime, začetek in konec semestra.
5. Skrbnik vpiše podatke semestra in pritisne gumb za potrditev.
6. Sistem shrani semester in prikaže obvestilo o uspehu.

#### Izjemni tokovi
1. Skrbnik v navigacijski vrstici izbere funkcionalnost *Semestri*.
2. Sistem prikaže seznam ustvarjenih semestrov.
3. Skrbnik na seznamu izbere možnost *Dodaj nov semester*.
4. Sistem prikaže okno za nastavljanje podatkov semestra - ime, začetek in konec semestra.
5. Skrbnik vpiše neustrezne podatke semestra (na primer začetni datum je po končnem datumu) in pritisne gumb za potrditev.
6. Sistem shrani semester in prikaže obvestilo o uspehu.

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot skrbnik. V nasprotnem primeru mu ta funkcionalnost ni na voljo.

#### Posledice
V sistemu se ustvari nov semester, ki je potem študentom na voljo za izbiro, ko vpisujejo novo aktivnost.

#### Prioriteta
Could have. Ker so začetki in konci semestrov precej predvidljivi, bi verjetno zadoščalo, da so njihovi podatki zapisani že v programski kodi.

#### Sprejemni testi
* Prijavi se kot skrbnik, ter pojdi na stran aktivnih semestrov. Tam izberi *Dodaj nov semester* in vpiši ustrezne podatke. Po potrditvi se na ekranu prikaže obvestilo o uspehu. Nov semester lahko vidiš na seznamu semestrov.



### Nastavljanje podatkov semestra

#### Povzetek funkcionalnosti
Skrbnik aplikacije lahko uredi podatke semestra, kot so ime, datuma začetka in konca semestra.

#### Osnovni tok
1. Skrbnik v navigacijski vrstici izbere funkcionalnost *Semestri*
2. Sistem prikaže seznam ustvarjenih semestrov
3. Skrbnik na seznamu izbere semester, ki ga želi urediti
4. Sistem prikaže okno za urejanje podatkov semestra
5. Skrbnik vpiše posodobljene podatke in potrdi spremembo
6. Sistem posodobi podatke in prikaže obvestilo o uspehu.

#### Izjemni tokovi
1. Skrbnik v navigacijski vrstici izbere funkcionalnost *Semestri*
2. Sistem prikaže seznam ustvarjenih semestrov
3. Skrbnik na seznamu izbere semester, ki ga želi urediti
4. Sistem prikaže okno za urejanje podatkov semestra
5. Skrbnik vpiše neveljavne posodobljene podatke (na primer začetni datum je po končnem datumu) in potrdi spremembo
6. Sistem uporabniku prikaže obvestilo o napaki.

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot skrbnik. V nasprotnem primeru mu ta funkcionalnost ni na voljo.

#### Posledice
V sistemu se spremenijo podatki o semestru. Posledično se tudi uporabnikom, ki so v urnik dodali dogodke, ki veljajo v posodobljenem semestru, spremenita začetek in konec ponavljanja teh dogodkov.

#### Prioriteta
Could have. Ker so začetki in konci semestrov precej predvidljivi, bi verjetno zadoščalo, da so njihovi podatki zapisani že v programski kodi.

#### Sprejemni testi
* Prijavi se kot skrbnik, ter pojdi na stran aktivnih semestrov. Tam izberi semester in ga uredi. Po potrditvi se na ekranu prikaže obvestilo o uspehu. Na seznamu semestrov lahko vidimo semester z vnešenimi spremembami.


## 6. Nefunkcionalne zahteve

**Zahteve izdelka**:

* Spletni strežnik se mora na vsako zahtevo odzvati v največ 3 sekundah.
* Komunikacija s strežnikom mora potekati preko REST API-ja.
* Sistem mora biti sposoben obvladovati vsaj 30 hkratnih uporabnikov.
* Podatkovna baza mora skrbeti za samodejno ustvarjanje varnostnih kopij podatkov.
* Sistem mora biti dostopen na javnem spletnm naslovu.

**Organizacijske zahteve**:

* V izvorni kodi projekta mora biti vsaka metoda opremljena z opisom delovanja.
* Za izdelavo sistema mora biti uporabljen sistem za obvladovanje verzij.

**Zunanje zahteve**:

* Sistem mora upoštevati GDPR predpise o zbiranju osebnih podatkov.
* Sistem mora delovati tako na Linux kot na Windows okoljih.


## 7. Prototipi vmesnikov


### Vse naslovne vrstice
![Naslovne vrstice](../img/stran-naslovne-vrstice.png)

Zaslonska maska prikazuje naslovne vrstice za vse tipe uporabnikov.

Prijavljenim uporabnikom je na voljo gumb za odjavo, tako da vmesnik omogoča primer uporabe **Odjava**.

### Prijava v sistem
![Prijava v sistem](../img/stran-prijava.png)

Prijava v sistem zahteva vpis elektronske pošte ter gesla.

Vmesnik omogoča primer uporabe **Prijava obstoječega uporabnika**.

### Registracija v sistem
![Registracija v sistem](../img/stran-registracija.png)

Registracija v sistem zahteva vpis elektronske pošte ter gesla in ponovitve gesla, ki se morata ujemati.

Vmesnik omogoča primer uporabe **Registracija novega uporabnika**.

### Prikaz vseh TO-DO listov
![Vsi seznami](../img/stran-vsi-seznami.png)

Vmesnik prikaže vse TO-DO liste, ki jih je študent ustvaril.

Omogoča tudi, da študent ustvari nov seznam obveznosti.

Vmesnik omogoča primer uporabe **Prikaz seznama TO-DO listov**

### Prikaz seznama obveznosti
![Seznam obveznosti](../img/stran-prikaz-seznama.png)

Vmesnik prikazuje seznam vseh obveznosti, ki se nahajajo na izbranem seznamu. Vsebuje tudi gumb, ki preklaplja med tem, ali se prikažejo tudi že opravljena opravila ali ne.

Vmesnik omogoča primer uporabe **Prikaz TO-DO lista**

Na vmesniku je  mogoče tudi izbrati, da je obveznost opravljena, s čimer se pokrije tudi primer uporabe **Potrjevanje obveznosti**.

### Ustvarjanje TO-DO lista
![Ustvari seznam obveznosti](../img/stran-ustvari-seznam.png)

Uporabnik lahko preko tega vmesnika izbere ime in ustvari nov TO-DO list.

Vmesnik omogoča primer uporabe **Izdelava TO-DO lista**

### Ustvarjanje nove obveznosti
![Ustvari obveznost](../img/stran-ustvari-obveznost.png)

Uporabnik lahko preko tega vmesnika ustvari novo obveznost in ji določi ime, opis, rok in prioriteto ter jo doda na enega ali več od seznamov obveznosti.

Vmesnik omogoča primer uporabe **Vnos nove obveznosti**

### Spreminjanje obveznosti
![Uredi obveznost](../img/stran-uredi-obveznost.png)

Uporabnik lahko preko tega vmesnika uredi obstoječo obveznost in ji določi ime, opis, rok in prioriteto ter jo doda na/zbriše iz enega ali več od seznamov obveznosti.

Vmesnik omogoča primer uporabe **Spreminjanje obveznosti**


### Prikaz urnika
![prikaz urnika](../img/stran-prikaz-urnika.png)

Uporabnik lahko preko tega vmesnika vidi svoj urnik za izbrani teden.
Vmesnik omogoča tudi dodajanje nove ponavljajoče obveznosti ter sinhronizacijo urnika z Google koledarjem.

Vmesnik omogoča primer uporabe **Prikaz urnika**

Vmesnik ima tudi gumb za sinhronizacijo z Google koledarjem. Ob izbiri tega gumba, se uporabniku prikaže dodatno okno, v katerem se odpre Goglova stran za prijavo. Tam se uporabnik lahko prijavi in sinhronizira koledar.

S tem vmesnik omogoča tudi primer uporabe **Sinhronizacija koledarja z Google Calendar API**

Vmesnik ima tudi gumb za uvoz urnika za FRI. Ob kliku na gumb se odpre okno za vnos vpisne številke.

### Dodajanje obveznosti na urnik
![dodaj obveznost na urnik](../img/stran-dodaj-obveznost-urnik.png)

Uporabnik lahko preko tega vmesnika doda novo ponavljajočo obveznost na svoj urnik. Izbere lahko njeno ime, opis, uro začetka in konca, dan v tednu na katerega se obveznost izvaja in vsaj en semester, v katerem se obveznost izvaja.

Vmesnik omogoča primer uporabe **Vnos v urnik**

### Prikaz vseh obveznosti
![Vse obveznosti](../img/stran-vse-obveznosti.png)

Vmesnik prikaže vse obveznosti, ki jih je ustvaril študent. Omogoča tudi urejenje posamezne obveznosti.

Vmesnik omogoča primer uporabe **Prikaz obveznosti**

Na vmesniku je  mogoče tudi izbrati, da je obveznost opravljena, s čimer se pokrije tudi primer uporabe **Potrjevanje obveznosti**.

### Uvažanje urnika FRI
![Uvoz urnika FRI](../img/stran-uvoz-urnika-fri.png)

Vmesnik študentu FRI omogoča vpis vpisne številke, s katero lahko sistem nato pridobi njegov urnik.

Vmesnik omogoča primer uporabe **Uvažanje urnika FRI**

### Prikaz vseh semestrov
![Vsi semestri](../img/stran-semestri.png)

Vmesnik prikaže vse semestre, ki so trenutno ustvarjeni.

Vmesnik omogoča primer uporabe **Prikaz semestrov**

### Dodajanje semestra
![Dodaj semester](../img/stran-ustvari-semester.png)

Vmesnik omogoča ustvarjanje novega semestra. Uporabnik lahko izbere ime, datum začetka in datum konca.

Vmesnik omogoča primer uporabe **Dodajanje novega semestra**

### Urejanje semestra
![Uredi semester](../img/stran-uredi-semester.png)

Vmesnik omogoča urejanje obstoječega semestra. Uporabnik lahko izbere ime, datum začetka in datum konca.

Vmesnik omogoča primer uporabe **Nastavljanje podatkov semestra**

## 8. Vmesniki do zunanjih sistemov

Za zunanji vir bo uporabljen Google Calendar API tipa REST. 
Vmesnik zahteva vpis z Google računom tako, da se ob izbiri fukncionalnosti pojavi okno z obrazcem za prijavo, izpolnjeni podatki 
pa so posredovani strežnikom Googla, če račun obstaja pa se aplikaciji vrne žeton, s katerim lahko ta dostopa do podatkov koledarja.

Za uvažanje urnika FRI bomo implementirali lastni vmesnik. 

#### Vpis v Google račun in pridobited avtentikacijskih podatkov
Uporabljena bo storitev Google Sign-In, ki implementira svojo programsko knjižnico. 
Na strani bo gumb za prijavo, ob pritisku pa se bo odprlo okno za prijavo, 
nato pa se bo klicala funkcija getAuthResponse().id_token, ki bo pridobila žeton. 
Po poteku veljave žetona, ga je potrebno osvežiti.


#### Ustvarjanje novih dogodkov na Google koledarju
Za kreiranje novih dogodkov ponuja API funkcijo events.insert({calendarId, event}),
ki je tipa POST, posredovana na naslov https://www.googleapis.com/calendar/v3/calendars/*calendarId*/events
Parameter funkcije je JSON objekt, ki vsebuje enolični identifikator koledarja uporabnika ali pa le "primary" za glavni koledar uporabnika, ter JSON objekt s podatki o dogodku - začetek, konec, opis, lokacija...
Uspešen klic vrne podatke o ustvarjenem dogodku, neuspešen pa ustrezno kodo napake.
Poleg uporabe te funkcije lahko enak učinek dosežemo le z pošiljanjem zahteve POST, saj parameter *calendarId*, ki je del URL naslova že vsebuje identifikator koledarja, v telesu zahteve pa navedemo podrobnejše podatke o dogodku.
Primer uporabe je dostopen na naslovu https://developers.google.com/calendar/v3/reference/events/insert#examples

#### Sinhronizacija koledarja
Za sinhronizacijo ponuja API funkcionalnost list, ki vrne dogodke specifičnega koledarja.
Deluje prek http zahteve GET posredovane na naslov https://www.googleapis.com/calendar/v3/calendars/*calendarId*/events
Parameter *calendarId*, ki je del URL naslova, enolično specificira koledar, če je "primary", se dostopa do glavnega koledarja uporabnika.
Telo zahteve mora biti prazno.
Privzeti uspešen odziv na zahtevo je JSON objekt z vsemi dogodki koledarja, neuspešen pa ustrezno kodo napake. 
Ponujena je tudi paginacija in inkrementalna sinhronizacija prek parametrov URL *syncToken*.
Primer uporabe je dostopen na naslovu https://developers.google.com/calendar/v3/reference/events/list#examples

#### Uvažanje urnika FRI
Za potrebe uvažanja urnika za študente FRI bomo uporablili funkcionalnost izvoza urnika, ki jo ponuja spletna aplikacija za pregled FRI urnikov.
Aplikacija ponuja izvoz urnika v tekstovnem formatu ical, ki ga bomo naložili in uvozili v našo aplikacijo.
Izvožen koledar je mogoče naložiti z GET zahtevo na naslovu https://urnik.fri.uni-lj.si/timetable/fri-2018_2019-letni-1-13/allocations_ical?student=*{vpisna številka}*,
pri čemer je potrebno podati vpisno številko študenta, sicer pa ni zahtevana nobena avtentikacija.