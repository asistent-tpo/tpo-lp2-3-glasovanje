# Načrt sistema

|                             |                                                        |
|:----------------------------|:-------------------------------------------------------|
| **Naziv projekta**          | Čiste desetke                                          |
| **Člani projektne skupine** | Timotej Knez, Ana Kronovšek, David Miškić, Katja Logar |
| **Kraj in datum**           | Ljubljana, 14. 4. 2019                                 |



## Povzetek

V načrtu sistema so vsi podatki, ki jih programer potrebuje za implementacijo sistema.
Najprej je prikazan logični načrt arhitekture aplikacije, ki je osnovana na MVC arhitekturnem vzorcu, nato pa še razvojni načrt arhitekture.
Naredili smo tudi razredni diagram s podrobneje opisanimi razredi, to vključuje tudi seznam metod z njihovimi parametri.
Največji del predstavlja načrt obnašanja, v katerem smo z diagrami zaporedja prikazali obnašanje posameznih funkcionalnosti.

## 1. Načrt arhitekture

### Logični načrt arhitekture
![Logični načrt](../img/LogicnaArhitektura.png)


### Razvojni načrt arhitekture
![Razvojni načrt](../img/RazvojnaArhitektura.png)

Pri pogledu in krmilniku se komponente krmilnika v črtkano črtnem pravokotniku povezujejo z istoimenskimi 
komponentami pogledov v drugem delu, a zaradi preglednosti to na načrtu ni bilo prikazano.

##### Povezave so sledeče:

* prijava 
    * pogled prijave
* TO-DO list
    * Pogled TO-DO lista
* TO-DO listi
    * Pogled TO-DO listov
* ustvarjanje TO-DO lista
    * Pogled dodajanja/urejanja TO-DO lista
* dodajanje/spreminjanje periodične obveznosti
    * pogled dodajanja/spreminjanja periodične obveznosti
* urnik
    * pogled urnika
    * pogled periodične obveznosti
* prikaz semestrov
    * pogled semestrov
* registracija
    * pogled registracije
* obveznosti
    * pogled obveznosti
* dodajanje nove/spreminjanje obveznosti
    * pogled dodajanja/spreminjanja obveznosti
* uvoz 
    * pogled Google uvoza
    * pogled FRI uvoza
* dodaj/spremeni semester
    * pogled dodajanja/urejanja semestrov

Naslovna vrstica se prikaže v vseh primerih.

### Fizični načrt arhitekture
![Fizični načrt](../img/fizicnipogled.png)


##### Razdelitev nalog

| komponenta                          | David | Timotej | Katja | Ana |
|:------------------------------------|:------|:--------|:------|:----|
| prijava                             |       |         |       | [x] |
| registracija                        |       |         |       | [x] |
| prikaz TO-DO                        |       | [x]     |       |     |
| prikaz obveznosti                   |       |         | [x]   |     |
| ustvarjanje TO-DO                   |       | [x]     |       |     |
| ustvarjanje/spreminjanje obveznosti |       |         | [x]   |     |
| urnik                               | [x]   | [x]     |       |     |
| uvoz                                |       |         | [x]   | [x] |
| prikaz semestrov                    | [x]   |         |       |     |
| dodajanje/spreminjanje semestra     | [x]   |         |       |     |



## 2. Načrt strukture

### Razredni diagram

![Razredni diagram](../img/celotni-razredni-diagram.png)

### Entitetni razredi

#### Razredni diagram za samo entitetne razrede

![Razredni diagram](../img/razredni-diagram.PNG)

#### 1. Oseba (Študent ali Skrbnik)

Objekt razreda **Oseba** predstavlja registriranega uporabnika. 

##### Atributi

* email: String 
    * Unikaten
    * Obvezen
* hashGesla: String
    * **Pomen:** Zgostitev gesla, ki se izračuna z uporabo zgoščevalnega algoritma SHA-265 na podlagi kombinacije gesla in soli.
    * Obvezen
* sol: String (Uporabi se za izračun zgostitve gesla.)
    * Obvezen

##### Nesamoumevne metode

* void nastaviGeslo(String geslo)
* boolean preveriGeslo(String geslo)
* void ustvariUporabnika(String email, String geslo)
* pridobiPodatkeUporabnika()
* prekliciJWT(Student student)
* generirajJWT()

*NastaviGeslo(geslo)* izbere sol (naključna vrednost) in na podlagi soli in gesla izračuna hashGesla (zgoščeno vrednost).

*PreveriGeslo(geslo)* na podlagi gesla in soli izračuna hashGesla ter preveri ali se ujema s hashGeslom v bazi.


**Oseba** je lahko dveh tipov: **Študent** ali **Skrbnik**. Od tega so potem odvisne nadaljnje povezave.

**Admin** vsebuje reference na **Semestre**, **Študent** pa na **Obveznosti**, **Periodične obveznosti** in **TO-DO liste**.

---

#### 2. Obveznost

Objekt razreda **Obveznost** predstavlja obveznost, ki mora biti izpoljena do roka.

##### Atributi

* ime: String 
    * Obvezen atribut
* rok: Date 
    * Obvezen atribut
* opravljen: Boolean
    * **Pomen:** Določa, ali je uporabnik obveznost že opravil (odkljukal)
    * Obvezen atribut
* opis: String
* prioriteta: [visoka, srednja, nizka]
    * Obvezen atribut

##### Nesamoumevne metode

* void obveznostOpravljena(id) 
* void ustvariNovoObveznost(String ime, Date rok, String opis, String prioriteta, [TODOlist] todos)
* void spremeniObveznost(String ime, Date rok, String opis, String prioriteta, [TODOlist] todos)
* void dodajTODOObveznosti([TODOlist] todos)
* Obveznost[] pridobiObveznosti(Student student, boolean opravljen)
* Obveznost pridobiObveznost(id)


*obveznostOpravljena()* označi neopravljeno obveznost za opravljeno.

Pri metodah *ustvariNovoObveznost(...)* in *spremeniObveznost(...)* je opis opcijski, vsi ostali atributi so obvezni. Atribut *opravljen* se nastavi na false.
Prav tako obe metodi vsebujeta seznam **TODOlist**-ov, na katerih se nahajata. V okviru obeh metod se kliče metoda *dodajNaToDoListe(...)*.

Metoda *dodajTODOObveznosti(...)* doda **Obveznosti** vse **TODOlist**-e iz seznama.

Metoda *pridobiObveznosti(...)* sprejme poleg študenta tudi, ali želimo opravljene ali neopravljene obveznosti.

**Obveznost** tudi vsebuje referenco na vse **TODOlist**-e, na katerih se pojavlja.

---

#### 3. Periodična obveznost

Objekt razreda **Periodična obveznost** predstavlja obveznost, ki ima neko časovno trajanje in se periodično ponavlja.

##### Atributi

* ime: String
    * Obvezen atribut
* opis: String
* od: String
    * Obvezen atribut
    * **Pomen:** Ob kateri uri v dnevu se obveznost začne.
    * **Zaloga vrednosti:** Ura zapisana v formatu **hh:mm**
* do: String
    * Obvezen atribut
    * **Pomen:** Ob kateri uri v dnevu se obveznost konča.
    * **Zaloga vrednosti:** Ura zapisana v formatu **hh:mm**
* semester: Semester
    * Obvezen atribut
    * Referenca na objekt tipa Semester
    * **Pomen:** V katerem semestru se obveznost izvaja.
* dan: [ponedeljek, torek, sreda, četrtek, petek, sobota, nedelja]
    * Obvezen atribut
    * **Pomen:** Dan v tednu, na katerega se obveznost izvaja.

##### Nesamoumevne metode

* void ustvariNovoPeriodicnoObveznost(String ime, String opis, String od, String do, Semester semester, String dan)
* PeriodicnaObveznost[] pridobiObveznostiNaUrniku(Student student)
* boolean preveriPrekrivanje(Obveznost obveznost)

Pri metodi *ustvariPeriodicnoObveznost(...)* je opis opcijski atribut, vsi ostali so obvezni.

Metoda *pridobiObveznostiNaUrniku(...)* vrne vse **Periodične obveznosti** od posamičnega študenta.

---

#### 4. TODOlist

Objekt razreda **TODOlist** predstavlja seznam obveznosti, ki smiselno spadajo skupaj.

##### Atributi

* ime: String
    * Obvezen atribut

* obveznosti: Obveznost array
    * **Pomen:** Vse obveznosti, ki pripadajo seznamu.
    * Skupina referenc na objekte tipa Obveznost (realizira se lahko tudi s pomožno tabelo)

##### Nesamoumevne metode

* void ustvariTODOList(String ime)
* TODOlist[] pridobiSeznamTODOListov(Student student)
* void dodajNaPrioritetniTODOList(Obveznost obveznost, String prioriteta)
* void dodajNaDodatneTODOListe(Obveznost obveznost, TODOlist[] todos)
* TODOlist pridobiTODOList(String id)
* void spremeniTODOList(Obveznost obveznost, String operacija)

Metoda *spremeniTODOList(...)* list dobi obveznost, ki jo glede na operacijo (lahko dodaj ali izbriši) doda ali izbriše iz TO-DO lista.

---

#### 5. Semester

Objekt razreda **Semester** predstavlja semester različnih fakultet po Sloveniji.

##### Atributi

* ime: String
    * Obvezen atribut
* začetek: Date
    * Obvezen atribut
* konec: Date
    * Obvezen atribut

##### Nesamoumevne metode

* void ustvariSemester(String ime, Date začetek, Date konec)
* void spremeniSemester(String ime, Date začetek, Date konec)
* Semester[] pridobiVseSemestre()

---


### Kontrolni razredi

#### 1. K - Urnik

##### Nesamoumevne metode

* prikaziUrnik(Date od, Date do)

#### 2. K - Dodajanje periodične obveznosti

##### Nesamoumevne metode

* prikaziVnosVUrnik()
* vnosVUrnik(String imePredmeta, String vrstaVnosa, String dan, String od, String do, Semester semester)

#### 3. K - Dodajanje nove obveznosti

##### Nesamoumevne metode

* prikaziVnosNoveObveznosti()
* ustvariNovoObveznost(String ime, Date rok, String prioriteta, String opis)

#### 4. K - Obveznosti

##### Nesamoumevne metode

* prikaziVnosNoveObveznosti()
* prikaziObvestilo(boolean uspesno)

#### 5. K - Ustvarjanje TO-DO lista

##### Nesamoumevne metode

* prikaziVnosNoveObveznosti()
* prikaziNovoObveznost(String ime)
* prikaziObvestilo(boolean uspesno)

#### 6. K - TO-DO list

##### Nesamoumevne metode

* prikaziUstvariTODOList()
* pridobiVseSezname()
* prikaziTODOList(TODOlist todo)
* obveznostOpravljena(boolean opravljena)

#### 7. K - Registracija

##### Nesamoumevne metode

* registracija(String email, String hashGesla)

#### 8. K - Prijava

##### Nesamoumevne metode

* prijava(String email, String hashGesla)
* odjava()

#### 9. K - Uvoz

##### Nesamoumevne metode

* getAuthResponse(boolean includeAuthorizationData)
* uvoziUrnik(String vpisnaStevilka)

#### 10. K - Semestri

##### Nesamoumevne metode

* vrniVseSemestre()

#### 11. K - Dodajanje/urejanje semestrov

##### Nesamoumevne metode

* ustvariSemester(String ime, Date od, Date do)
* spremeniSemester(Semester semester, String ime, Date od, Date do)


#### 12. K - Urejanje obveznosti

##### Nesamoumevne metode

* spremeniObveznost(Obveznost obveznost, String ime, Date rok, String prioriteta, String opis)
* spremeniTODOList(TODOlist todo, String ime)


### Mejni razredi

#### 1. ZM - Dodajanje periodične obveznosti

##### Nesamoumevne metode

* prikaziObrazecZaVnosVUrnik()
* vnosVUrnik(String imePredmeta, String vrstaVnosa, String dan, String od, String do, Semester semester)

#### 2. ZM - Urnik

##### Nesamoumevne metode

* prikaziVnosVUrnik()
* zahtevajPrikazUrnika(Date od, Date do)
* prikaziNovoObveznost(Obveznost obveznost)
* prikaziSporociloOUspehu()
* prikaziSporociloOPrekrivanju()
* prikaziUrnik()

#### 3. ZM - Obveznosti

##### Nesamoumevne metode

* prikaziVnosNoveObveznosti()
* prikaziObveznosti()
* urediObveznost (Obveznost obveznost, String ime, Date rok, String prioriteta, String opis)
* prikaziNovoObveznost(Obveznost obveznost)
* pokaziObvestilo(boolean uspesno)
* prikaziSeznamObveznosti()


#### 4. ZM - Dodajanje nove obveznosti

##### Nesamoumevne metode

* ustvariNovoObveznost(Obveznost obveznost, String ime, Date rok, String prioriteta, String opis)

#### 5. ZM - Dodajanje TO-DO lista

##### Nesamoumevne metode

* ustvariTODOList(String ime)
* prikaziObrazecZaVnosTODOLista()

#### 6. ZM - TO-DO list

##### Nesamoumevne metode

* zahtevajPrikazTODOLista()
* prikaziUstvariTODOList()
* potrdiObveznost(Obveznost obveznost)
* vprasajZaPotrditev()
* prikaziObrazecZaVnosTODOLista()
* prikaziSporociloOUspehu()
* prikaziNovTODOList(TODOlist todo)
* obveznostPrikaziOpravljeno()

#### 7. ZM - Registracija

##### Nesamoumevne metode

* registracija(String email, String hashGesla)
* preveriUstreznostPodatkov()
* prikaziUspeh()
* prikaziNapako()

#### 8. ZM - Prijava

##### Nesamoumevne metode

* prijava(String email, String hashGesla)
* prikaziUspeh()
* prikaziNapako()

#### 9. ZM - Naslovne vrstice

##### Nesamoumevne metode

* odjava()
* preusmeriNaStranPrijave()

#### 10. ZM - TO-DO listi

##### Nesamoumevne metode

* prikaziTODOList(String id)
* prikaziSeznamTODOjev()
* prikaziSpremenjenTODOList()
* prikaziSporociloOUspehu()

#### 11. ZM - Google uvoz

##### Nesamoumevne metode

* sinhronizirajZGooglom()

#### 12. ZM - Semestri

##### Nesamoumevne metode

* vrniVseSemestre()


#### 13. ZM - Dodajanje/urejanje semestrov

##### Nesamoumevne metode

* ustvariSemester(String ime, Date od, Date do)
* izberiInSpremeniSemester(Semester semester, String ime, Date od, Date do)
* prikaziUspesenStatus()
* prikaziNeuspesenStatus()

#### 14. ZM - Urejanje obveznosti

##### Nesamoumevne metode

* prikaziUrejanjeObveznosti()
* potrdiUrejanje()
* prikaziPodatkeVObrazcu(Obveznost obveznost)

#### 15. ZM - FRI uvoz

##### Nesamoumevne metode

* uvoziUrnik()
* prikaziVnosVpisneStevilke()
* prikaziNovUrnik()

#### 16. EX - Google maps api

##### Nesamoumevne metode

* eventsInsert()
* getAuthResponse(boolean includeAuthorizationData)
* getEvents(String eventId)

#### 16. EX - FRI

##### Nesamoumevne metode

* PeriodicnaObveznost[] uvoziUrnik(int vpisnaStevilka)


## 3. Načrt obnašanja

### 1. Vnos v urnik

![Vnos v urnik](../img/vnosVUrnik.png)

### 2. Prikaz urnika

![Prikaz urnika](../img/prikaz-urnika.png)

### 3. Vnos nove obveznosti

![Vnos nove obveznosti](../img/nova-obveznost.png)

### 4. Spreminjanje obveznosti

#### Osnovni tok

![Spreminjanje obveznosti osnovni tok](../img/SpreminjanjeObveznosti-osnovni.PNG)

#### Alternativni tok 1

![Spreminjanje obveznosti alternativni tok](../img/SpreminjanjeObveznosti-alternativni.PNG)

### 5. Izdelava TO-DO lista

#### Osnovni tok

![Izdelava TO-DO lista osnovni tok](../img/ustvari-TODO-osnovni.png)

#### Alternativni tok 1

![Izdelava TO-DO lista](../img/ustvari-TODO.png)

### 6. Registracija novega uporabnika

![Registracija novega uporabnika](../img/registracija.png)

### 7. Prijava obstoječega uporabnika

![Prijava obstoječega uporabnika](../img/prijava.png)

### 8. Odjava

![Odjava](../img/odjava.png)

### 9. Prikaz seznama TO-DO listov

![Prikaz seznama](../img/vsi-seznami.png)

### 10. Prikaz TO-DO lista

![Prikaz TODO lista](../img/PrikaziTODOlist.PNG)

### 11. Potrjevanje obveznosti

![Potrjevanje obveznosti](../img/PotrdiObveznost.PNG)

### 12. Prikaz obveznosti

![Prikaz obveznosti](../img/PrikaziObveznosti.PNG)

### 13. Uvažanje urnika FRI

![Uvažanje urnika FRI](../img/UvoziUrnikFRI.PNG)

### 14. Sinhronizacija koledarja z Google Calendar API	

![Sinhronizacija koledarja z Google Calendar API](../img/Sinhronizacijakoledarja.png)

### 15. Prikaz semestrov	

![Prikaz semestrov](../img/Prikazsemestrov.png)

### 16. Dodajanje novega semestra	

![Dodajanje novega semestra](../img/Novsemester.png)

### 17. Nastavljanje podatkov semestra	

![Nastavljanje podatkov semestra](../img/Nastavljanjepodatkovsemestra.png)
