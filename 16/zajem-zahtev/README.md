# Dokument zahtev

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Jan Pelicon, Jure Vitežnik |
| **Kraj in datum** | Ljubljana, 30.3.2019 |



## Povzetek projekta

Veliko študentov se sooča s stresom in problemom težke glave, saj si morajo zapomniti veliko rokov za oddajo in izpitnih rokov, datume kolokvijev in vseh ostalih sprotnih obveznosti,
ki spadajo k uspešnemu opravljanju študija. Naš cilj je, da jih razbremenimo, tako da jim razvijemo spletno aplikacijo za pomoč pri opravljanju njihovih študijskih obveznosti.
Spletni vmesnik bo omogočal jasen pregled nad vsemi obveznostmi kot so predavanja, vaje, domače naloge in izpiti, torej vse pomembne zadeve zbrane na enem mestu, 
uporabnik pa si bo lahko določil tudi prioritete aktivnosti ter tako dobil boljši načrt dela za naslednje dni. Ob uporabi aplikacije se bo tako študent organizirano in 
učinkovito spopadal z vsemi študijskimi obveznostmi.

## 1. Uvod

StraightAs predstavlja pripomoček s katerim bodo uporabniki lahko sledili svojim zadanim nalogam 
in jih učinkovito izvedli pred določenim rokom. Aplikacija bo tudi svetovala in usmerjala uporabnika tako, 
da bo svoje obveznosti, ki so pregledno predstavljene, kar najučinkovitejše opravljal. 
Poleg tega želimo dostop do aplikacije omogočti na različnih napravah, kar poveča dostopnost. 
Uporabnik lahko tako kadarkoli dostopa, spreminja in dodaja aktivnosti.

## 2. Uporabniške vloge

Ker je spletna aplikacija namenjena predvsem slovenskim študentom, je to tudi največja skupina, ki bo aplikacijo uporabljala. 
To seveda ne pomeni, da je njena uporabnost striktno omejena le na to skupino ljudi. Cilji aplikacije oziroma storitve je omogočiti uporabniku, 
da sledi svojim obveznostim, kar pomeni, da lahko storitev koristi vrsto različnih posameznikov:

* študent (vsi slovenski študentje dobijo brezplačen dostop do vseh funkcionalnosti)
* premium uporabnik (uporabnik, ki plačuje za dodatne funkcionalnosti in storitve)
* navaden uporabnik (uporabnik, ki ima dostop le do osnovnih funkcionalnosti)
* skrbnik storitve (oseba, ki skrbi, da storitev deluje)
* razvijalec (oseba, ki storitev nadgrajuje in razvija)

## 3. Slovar pojmov

**Premium uporabnik** - uporabnik, ki je storitev plačal in ima dostop do dodatnih funkcionalnosti sistema.
**Začetna stran** - stran, ki jo vidimo, če v storitev nismo prijavljeni.
**Glavna stran** - stran, ki jo vidi uporabnik, ko se prijavi v storitev. Tukaj ima pregled nad aktivnostmi in načrtom izvajanja aktivnosti.

## 4. Diagram primerov uporabe

![Graf UML](../img/LP2_UML_DIAGRAM.JPG)

## 5. Funkcionalne zahteve

Seznam posameznih funkcionalnosti:

* (1) odjava in prijava v storitev
* (2) vnos nove aktivnosti
* (3) urejanje obstoječe aktivnosti
* (4) izbris aktivnosti
* (5) dodajanje odvisnosti med aktivnostmi
* (6) označenje aktivnosti kot opravljene
* (7) organiziranje načrta opravljanja aktivnosti
* (8) nastavitev opomnikov za aktivnosti
* (9) urejanje prioritet aktivnosti
* (10) vpis komentarja/rezultata opravljene aktivnosti
* (11) popravljanje načrta v primeru neopravljene aktivnosti
* (12) urejanje sistemskih nastavitev
* (13) administracija uporabnikov

#### Povzetek funkcionalnosti

(1) odjava in prijava v storitev

* Vsi uporabniki, akterji se lahko prijavijo v storitev. Če je oseba prijavljena se lahko nato odjavi.

(2) vnos nove aktivnosti

* Vsi uporabniki, akterji lahko dodajajo novo aktivnost z določenimi parametri kot so ime in rok zapadlosti v svoj načrt.

(3) urejanje obstoječe aktivnosti

* Vsi razen navadnih uporabnikov imajo možnost urejanja obstoječih aktivnosti.

(4) izbris aktivnosti

* Vsi uporabniki lahko iz svojega načrta izvajanja aktivnosti odstranijo obstoječo aktivnost, ki še ni zaključena.

(5) dodajanje odvisnosti med aktivnostmi

* Vsi razen navadnih uporabnikov lahko definirajo relacije med različnimi aktivnostmi. Odvisne aktivnosti se nemorejo izvajati in končati pred zaključkom aktivnosti, od katerih so odvisne.

(6) zaključek uspešno opravljene aktivnosti

* Vsi uporabniki lahko označijo aktivnost kot opravljeno. Če so katere od ostalih aktivnosti odvisne od opravljene aktivnosti, se odvisnosti sprostijo. 

(7) organiziranje načrta opravljanja aktivnosti

* Vsi razen navadnih uporabnikov lahko izdelajo načrt poteka opravljanja aktivnosti. Uporabijo lahko avtomatsko generiran potek, ki ga storitev pripravi namesto njih, lahko pa sestavijo popolnoma svoj načrt izvajanja aktivnosti.

(8) nastavitev opomnikov za aktivnosti

* Vsi razen navadnih uporabnikov lahko za aktivnost ustvarijo opomnik, katerega naloga je opomniti uporabnika, da se rok za opravljanje aktivnosti bliža koncu. 

(9) urejanje prioritet aktivnosti

* Vsi razen navadnih uporabnikov lahko spreminjajo prioritete obstoječih aktivnosti. Prioriteta aktivnosti se upošteva pri načrtu opravljanja aktivnosti.

(10) vpis komentarja/rezultata aktivnosti

* Vsi uporabniki lahko ob zaključeni aktivnosti komentirajo izvedbo, rezultat izvajanja ali dodajo opombe.

(11) popravljanje načrta izvajanja aktivnosti v primeru neopravljene aktivnosti

* Vsi razen navadnih uporabnikov lahko ob neopravljeni aktivnosti razrešijo odvisnosti in popravijo potek izvajanja aktivnosti. 

(12) urejanje sistemskih nastavitev

* Skrbnik sistema in razvijalec lahko dostopata do sistemskih nastavitev storitve.

(13) administracija uporabnikov

* Skrbnik sistema lahko dostopa in ureja dostop uporabnikov in njihove pravice.

#### Osnovni tok

**Odjava in prijava v storitev**

* Za vse uporabniške vloge (prijava):

    1. Uporabnik izbere funkcionalnost “prijava”.
    2. Sistem preusmeri uporabnika na stran s polji za vpis uporabniškega imena in gesla.
    3. Uporabnik izpolni podani polji in pritisne gumb “prijava”.
    4. Sistem preveri prijavo. Če je uspešna sledi preusmeritev na glavno stran in uporabniku postreže z njegovimi aktivnostmi.

* Za vse uporabniške vloge (odjava):

	1. Uporabnik izbere funkcionalnost “odjava”.
    2. Sistem prikaže sporočilo o uspešni odjavi in preusmeri uporabnika na začetno stran.

**Vnos nove aktivnosti**

* Vsi razen navadnega uporabnika:
 
    1. Uporabnik izbere funkcionalnost “nova aktivnost”.
    2. Sistem prikaže okno v katerem mora uporabnik obvezno določiti ime aktivnosti in trajanje (rok zapadlosti). Neobvezna polja so prioriteta, odvisnost od ostalih aktivnosti in oznaka (tag).
    3. Uporabnik izpolni potreba polja in shrani aktivnost.
    4. Sistem doda aktivnost v načrt izvajanja in ga posodobi glede na možne odvisnosti in prioritete.

* Navaden uporabnik:

    1. Navaden uporabnik izbere funkcionalnost “nova aktivnost”.
    2. Sistem prikaže okno v katerem mora navaden uporabnik obvezno določiti ime aktivnosti in trajanje (rok zapadlosti).
    3. Uporabnik izpolni potreba polja in shrani aktivnost.
    4. Sistem doda aktivnost v načrt izvajanja.
    
**Urejanje obstoječe aktivnosti**

* Vsi razen navadnega uporabnika, ki nima dostopa do funkcionalnosti.
   
    1. Uporabnik izbere funkcionalnost “uredi aktivnost”.
    2. Sistem prikaže okno, ki je enako oknu, ko uporabnik določa novo aktivnost.
    3. Uporabnik po potrebi spremeni podatke o aktivnosti in shrani aktivnost.
    4. Sistem posodobi aktivnost in načrt izvajanja aktivnosti glede na spremembe. 
    
**Izbris aktivnosti**

* Navaden uporabnik:

    1. Uporabnik izbere aktivnost in pritisne gumb “odstrani aktivnost”.
    2. Sistem povpraša uporabnika ali želi trajno izbrisati aktivnost.
    3. Uporabnik potrdi zahtevo.
    4. Sistem odstrani aktivnost.
    
* Ostali uporabniki:

    1. Uporabnik izbere aktivnost in pritisne gumb “odstrani aktivnost”.
    2. Sistem povpraša uporabnika ali želi trajno izbrisati aktivnost.
    3. Uporabnik potrdi zahtevo.
    4. Sistem povpraša uporabnika ali želi, da se načrt izvajanja samodejno posodobi.
    5. Uporabnik izbere željeno funkcionalnost. Nato se vrne na glavno stran.

**Dodajanje odvisnosti med aktivnostmi**

* Študenti, premium uporabniki:

    1. Uporabnik želi postaviti odvisnosti med aktivnosti – izbere aktivnost ki bo postala odvisna.
    2. Sistem ponudi uporabniku možnosti urejanja in dodajanja odvisnosti.
    3. Uporabnik definira tip odvisnosti in opis le te ter shrani spremembe.
    4. Sistem posodobi odvisnosti in morebiten načrt izvajanja, če nova odvisnost krši trenuten načrt izvajanja.

**Označenje aktivnosti kot opravljene** 

* Navaden uporabnik:
    
    1. Uporabnik izbere aktivnost in pritisne gumb “zaključi aktivnost”.
    2. Sistem povpraša uporabnika ali želi označiti aktivnost kot opravljeno. 
    3. Uporabnik potrdi zahtevo.
    4. Sistem označi aktivnost kot opravljeno in postavi možnost komentarja na opravljeno aktivnost.

* Ostali uporabniki:
    
    1. Uporabnik izbere aktivnost in pritisne gumb “zaključi aktivnost”.
    2. Sistem povpraša uporabnika ali želi označiti aktivnost kot opravljeno. 
    3. Uporabnik potrdi zahtevo.
    4. Sistem označi aktivnost kot opravljeno, sprosti morebitne odvisnosti, po možnosti posodobi načrt izvajanja aktivnosti in postavi možnost komentarja na opravljeno aktivnost.

**Organiziranje načrta opravljanja aktivnosti** 

* Vsi razen navadnih uporabnikov:
  
    1. Uporabnik izbere funkcionalnost “nov načrt izvajanja aktivnosti”.
    2. Sistem povpraša uporabnika ali želi, da sam ročno postavi načrt izvajanja ali naj se načrt izvajanja samodejno generira.
    3. Če uporabnik izbere samodejno generiranje se postopek zaključi. Če izbere ročno načrtovanje, ga sistem preusmeri na novo stran za pripravo načrta.
    4. Sistem v primeru samodejnega načrtovanja vrne uporabnika na glavno stran.
    5. Ko uporabnik zaključi načrtovanje mora načrt shraniti s pritiskom na gumb “shrani načrt izvajanja”.
    6. Sistem izbriše prejšnji načrt izvajanja in uvede novega, ki ga je uporabnik sestavil, ter uporabnika vrne na glavno stran.

**Nastavitev opomnikov za aktivnosti**

* Vsi razen navadnih uporabnikov:
    
    1. Uporabnik izbere aktivnost kateri želi dodati opomnik in klikne gumb “dodaj opomnik”
    2. Sistem odpre okno za dodajanje opomnika. Uporabnik mora obvezno podati čas opomnika in ime.
    3. Uporabnik izpolni obvezna polje in shrani opomnik.
    4. Sistem postavi opomnik na aktivnost in preusmeri uporabnika na glavno stran. 
     
**Urejanje prioritet aktivnosti**

* Vsi razen navadnih uporabnikov:
      
    1. Uporabnik izbere aktivnost kateri želi spreminjati prioritete in izbere novo prioriteto, ki se rangira od 1 do 5, kjer 5 predstavlja najvišjo prioriteto in 1 najnižjo.
    2. Sistem sprejme zahtevo in posodobi prioriteto aktivnosti. V primeru samodejnega načrtovanja tudi slednjega posodobi in uporabnika vrne na glavno stran.

**Vpis komentarja/rezultata aktivnosti**

* Vsi uporabniki:

    1. Uporabnik izbere zaključeno aktivnost in klikne gumb “dodaj komentar aktivnosti”.
    2. Sistem uporabniku odpre novo stran na kateri lahko ureja besedilo za komentar.
    3. Uporabnik po zaključenem urejanju komentar shrani.
    4. Sistem preusmeri uporabnika na glavno stran.

**Popravljanje načrta izvajanja v primeru neopravljene aktivnosti**

* Vsi razen navadnih uporabnikov:

    1. Uporabnik lahko aktivnost, kateri je rok potekel in ni bila dokončana, zaključi pod pogojem, da razreši morebitne odvisnosti in preuredi načrt izvajanja aktivnosti.
    2. Sistem ponudi uporabniku seznam odvisnih aktivnosti od izbrane.
    3. Uporabnik razreši odvisnosti in ročno postavi nov načrt izvajanja ali izbere, da storitev samodejno generira nov načrt izvajanja.
    4. Sistem preveri, če so konflikti odpravljeni in uvede spremembe načrta izvajanja ali ga generira. Uporabnika vrne na glavno stran.

**Urejanje in spreminjanje sistemstkih nastavitev**

* Skrbnik in razvijalec:

    1. Skrbnik ali razvijalec lahko dostopata do sistemskih nastavitev in spremenljivk.
    2. Sistem preveri, če ima oseba potrebne pravice za dostop do nastavitev in ga preusmeri na novo stran.
    3. Skrbnik ali razvijalec uvede željene spremembe.
    4. Sistem shrani nove nastavitve, ki jih bo uvedel ob naslednji posodobitvi in osebo preusmeri na glavno stran.

**Administracija uporabnikov**

* Skrbnik:
  
    1. Skrbnik lahko dostopa do seznama uporabnikov pod dodatnimi možnostmi, ki so dostopne le njemu.
    2. Sistem vrne skrbniku seznam uporabnikov, njihove pravice, stanja računov ter ostale podatke.
    3. Skrbnik shrani spremembe uporabnikov.
    4. Sistem preveri, če so spremenjeni uporabniki aktivni. Če niso uvede spremembe, drugače pa uporabnika odjavi iz sistema, nakar se mora ta ponovno prijaviti. Srbnika vrne na glavno stran. 

#### Alternativni tok(ovi)

**Alternativni tok 1**

* vnos nove aktivnosti:

    1. Uporabnik izbere funkcionalnost “nova aktivnost”.
    2. Sistem prikaže okno v katerem mora uporabnik obvezno določiti ime aktivnosti in trajanje (rok zapadlosti). Neobvezna polja so prioriteta, odvisnost od ostalih aktivnosti in oznaka (tag)
    3. Uporabnik ni vnesel obveznih polj in shrani aktivnost.
    4. Sistem vpraša uporabnika ali želi dodati aktivnost brez potrebnih definicij. Poleg tega ga obvesti o težavah, ki se bodo lahko pojavile pri generiranju načrta izvajanja.
    5. Uporabnik shrani aktivnost z vednostjo, da bo v prihodnosti posodobil informacije o aktivnosti.
    6. Sistem shrani aktivnost, ki pa jo označi kot nepopolno in vrne uporabnika na glavno stran.

**Alternativni tok 2**

* organiziranje načrta opravljanja aktivnosti 
    
    1. Uporabnik izbere funkcionalnost “nov načrt izvajanja aktivnosti”.
    2. Sistem povpraša uporabnika ali želi, da sam ročno postavi načrt izvajanja ali naj se načrt samodejno generira.
    3. Če uporabnik izbere ročno postavljanje načrta, ga sistem preusmeri na novo stran za postavitev načrta izvajanja.
    4. Uporabnik načrta izvajanja ne dokonča popolnoma in poskuša načrt izvajanja uvesti.
    5. Sistem uporabnika obvesti o težavi ter mu ponudi samodejno dopolnitev preostalih aktivnosti v njegov načrt izvajanja.
    6. Uporabnik dokonča načrt izvajanja ali pa izbere samodejno dopolnitev.
    7. Sistem posodobi načrt izvajanja in uporabnika vrne na glavno stran.

**Alternativni tok 3**

*  označenje aktivnosti kot opravljene 

    1. Uporabnik izbere aktivnost, ki je odvisna od aktivnosti, ki ni še opravljena in pritisne gumb “zaključi aktivnost”.
    2. Sistem povpraša uporabnika ali želi označiti aktivnost kot opravljeno in ga opozori, na odvisnosti, ki ostajajo. 
    3. Uporabnik razreši odvisnost in shrani spremembe ali prekine proces označevanja aktivnosti kot opravljene.
    4. Sistem označi aktivnost kot opravljeno ali uporabnika opozori o neizvedenih spremembah in ga vrne na glavno stran.

#### Izjemni tok(ovi)

**Dodajanje odvisnosti med aktivnostmi**

Uporabnik je dodal odvisnosti tako, da so nekatere aktivnosti medsebojno odvisne in zaradi tega načrt izvajanja ni možno ustvariti. Sistem prikaže ustrezno obvestilo.

**Popravljanje načrt izvajanja v primeru neopravljene aktivnosti**

Uporabnik je nepravilno razrešil konfliktne odvisnosti in nadalnje načrtovanje ni mogoče. Sistem prikaže ustrezno obvestilo.

**Urejanje prioritet aktivnosti**

Uporabnik je nesmiselno prioritiziral določene aktivnosti – posledica je slabo generiran načrt izvajanja. Sistem prikaže ustrezno obvestilo o napakah v načrtovanju.

#### Pogoji

| funkcjonalnost | Pogoj |
| :-------- | :--------- |
| Prijava v storitev | Nihče ni prijavljen |
| Odjava v storitev | Uporabnik je prijavljen |
| Vnos nove aktivnosti | Uporabnik je prijavljen |
| Urejanje obstoječe aktivnosti | Obstaja aktivnost |
| Izbris aktivnosti | Obstaja nazaklučena aktivnost |
| Dodajanje odvisnosti med aktivnostmi | Prijavljeni smo kot študent ali premium uporabnik |
| Označenje aktivnosti kot opravljene | Obstaja nazaklučena aktivnost |
| Organiziranje načrta opravljanja aktivnosti | Prijavljeni kot študent |
| Nastavitev opomnikov za aktivnosti | Prijavljeni kot študent, obstaja aktivnost |
| Vpis komentarja/rezultata aktivnosti | Obstaja zaklučena aktivnost |
| Popravljanje načrt izvajanja v primeru neopravljene aktivnosti | Prijavljeni kot študent, obstaja načrt izvajanja |
| Urejanje in spreminjanje sistemstkih nastavitev | Prijavljeni kot razvijalec |
| Administracija uporabnikov | Prijavljeni kot skrbnik |
| Alternativni tok 1 - vnos nove aktivnosti | Prijavljeni kot študent |
| Alternativni tok 2 - organiziranje načrta opravljanja aktivnosti | Prijavljeni kot študent |
| Alternativni tok 3 - označenje aktivnosti kot opravljene | Nezaključeni aktivnosti z odvisnostmi med sabo |


#### Posledice

| funkcjonalnost | Posledica |
| :-------- | :--------- |
| Prijava v storitev | Uporabnik prijavljen |
| Odjava v storitev | Uporabnik odjavljen |
| Vnos nove aktivnosti | Uporabnik je prijavljen |
| Urejanje obstoječe aktivnosti | Aktivnost spremenjena |
| Izbris aktivnosti | Aktivnost se premakne na seznam izbrisanih aktivnosti |
| Dodajanje odvisnosti med aktivnostmi | Aktivnosti so sedaj odvisne med sabo |
| Označenje aktivnosti kot opravljene | Aktivnost se premakne na seznam opravljenih aktivnosti |
| Organiziranje načrta opravljanja aktivnosti | Vstvarjen načrta opravljanja |
| Nastavitev opomnikov za aktivnosti | Spremenjeni opomnik pri aktivnosti |
| Vpis komentarja/rezultata aktivnosti | Dodan komentar/rezultat pri tej aktvinosti, če aktivnost ni med zakjlučenimi jo premakni tja |
| Popravljanje načrta izvajanja v primeru neopravljene aktivnosti | Popravljen načrt izvajanja |
| Urejanje in spreminjanje sistemstkih nastavitev | Spremenjene nastavitve |
| Administracija uporabnikov | Upravljanje z uporabniki |


#### Posebnosti

| funkcjonalnost | Posebnost |
| :-------- | :--------- |
| Prijava v storitev | / |
| Odjava v storitev | / |
| Vnos nove aktivnosti | / |
| Urejanje obstoječe aktivnosti | / |
| Izbris aktivnosti | / |
| Dodajanje odvisnosti med aktivnostmi | / |
| Označenje aktivnosti kot opravljene | /|
| Organiziranje načrta opravljanja aktivnosti | / |
| Nastavitev opomnikov za aktivnosti | / |
| Vpis komentarja/rezultata aktivnosti | / |
| Popravljanje načrta izvajanja v primeru neopravljene aktivnosti | / |
| Urejanje in spreminjanje sistemstkih nastavitev | Dodatna programska oprema narejena le za to funkcjonalnost |
| Administracija uporabnikov | Oseba mora imeti dostop do podatkovne baze |


#### Prioritete identificiranih funkcionalnosti

| FUNCIONALNOST | PRIORITETA |
| :-------- | :--------- |
| odjava in prijava v storitev | MUST have |
| vnos nove aktivnosti | MUST have |
| urejanje obstoječe aktivnosti | SHOULD have | 
| izbris aktivnosti | SHOULD have |
| dodajanje odvisnosti med aktivnostmi | SHOULD have |
| zaključek uspešno opravljene aktivnosti| MUST have |
| organiziranje načrta opravljanja aktivnosti | COULD have |
| nastavitev opomnikov za aktivnosti | COULD have |
| urejanje prioritet aktivnosti | SHOULD have |
| vpis komentarja/rezultata aktivnosti | COULD have |
| popravljanje načrta izvajanja v primeru neopravljene aktivnosti | WOULD have |
| urejanje sistemskih nastavitev | MUST have |
| administracija uporabnikov | SHOULD have |

#### Sprejemni testi

| funkcjonalnost | začetno stanje | vhod | pričakovan rezultat |
| :-------- | :--------- | | :-------- | :--------- |
| prijava v storitev | Nihče ni prijavljen | Uporabniško ime in geslo | Prijava, če je uporabnik registriran |
| odjava v storitev | Uporabnik je prijavljen |  / | Odjava uporabnika |
| Vnos nove aktivnosti | Uporabnik je prijavljen | Izpolnjen obrazec za novo aktivnost | Nova aktivnost dodana |
| Urejanje obstoječe aktivnosti | Obstaja aktivnost | Spremembe, ki jih želimo dodati tej aktivnosti | Spremenjena aktivnost |
| Izbris aktivnosti | Obstaja aktivnost | Potrditev brisanja | Aktivnosti ni več |
| Dodajanje odvisnosti med aktivnostmi | Prijavljeni smo kot študent ali premium uporabnik | Aktivnosti ki sta odvisni ter opis | Med izbanimi aktivnostmi je vidna odvisnost ter opis |
| Označenje aktivnosti kot opravljene | Obstaja aktivnost | Potrditev opravljenosti | Aktivnosti je med ostalimi opravljenimi ter ne več me neopravljenimi |
| Organiziranje načrta opravljanja aktivnosti | Prijavii se kot študent | Vstvari nov načrt opravljanja, ter pusti naj se samodejno generira | Samodejno generiran načrt |
| Nastavitev opomnikov za aktivnosti | Prijavi se kot študent, vstvari aktivnost | Opomnik za aktivnost | Aktivnost ima sedaj opomnik |
| Urejanje prioritet aktivnosti | Prijavi se kot študent, več vstvarjenih aktivnosti | Aktivnostim spremeni prioriteto | Aktivnosti se sedaj razporedijo glede na nove prioritete|
| Vpis komentarja/rezultata aktivnosti | Obstaja zaključena aktivnost | Obrazec za komentarje | Zaključena aktivnost z komentarjem |
| Popravljanje načrta izvajanja v primeru neopravljene aktivnosti | Prijavi se kot študent, obstaja načrt izvajanja | V tem načrt izvajanja izbriši neopravljeno aktivnost | Načrt izvajanja se mora sam popraviti |
| Urejanje in spreminjanje sistemstkih nastavitev | Prijavi se kot razvijalec | spremeni neko sistemsko nastavitev | Sistemsta nastavitev spremenjena |
| Administracija uporabnikov | Prijavi se kot skrbnik | / | Preveri ali lahko dostopaš do uporabiških imen in gesel |
| Alternativni tok 1 - vnos nove aktivnosti | Prijavi se kot študent | Nepopolen obrazec za novo aktivnost | Nova aktivnost dodana vendar označena kot nedokončana |
| Alternativni tok 2 - organiziranje načrta opravljanja aktivnosti | Prijavi se kot študent | Nov načrt izvajanja, ki ga ročno dokončamo | Nov načrt izvajanja narejen |
| Alternativni tok 3 - označenje aktivnosti kot opravljene | Aktivnosti z odvisnostmi med sabo | Zaključimo eno aktivnost | Sistem nas opozori na odvisnost |

## 6. Nefunkcionalne zahteve

* zahteve izdelka:
    1. Aplikacija mora delovati cel dan (99,9% uptime)
    2. Odzivnost aplikacije nesme biti počasnjejša od 500 milisekund
    3. Aplikacija mora biti dosegljiva na javno dostopnem spletnem naslovu
    4. Aplikacija mora biti zmožna streči 1000 hkratnih uporabnikov
    5. Aplikacija mora delovati tako na računalniku, kot na mobilnih napravah
    
* Organizacijske zahteve:
    1. Uporabniki se morajo prijaviti z svojim uporabniškim imenom in geslom
    2. Z enim email-om je mozna le ena registracija

* Zunanje zahteve:
    1.
    2.

## 7. Prototipi vmesnikov

**Prijava**
![Prijava](../img/Prijava.JPG)

**Aktivnosti**
![Aktivnosti](../img/Aktivnosti.JPG)

**Nova Aktivnost**
![Nova_aktivnost](../img/Nova_aktivnost.JPG)

**Meni**
![Meni](../img/Meni.JPG)

**Nacrti**
![Nacrti](../img/Nacrti.JPG)
