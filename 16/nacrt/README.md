# Načrt sistema

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Jan Pelicon, Jure Vitežnik |
| **Kraj in datum** | 19.4.2019 |


## Povzetek

V dokumentu so predstavljeni različni pogledi na arhitekturo sistema in njegov razvoj skozi čas.
Predstavi abstrakcijo storitve oziroma sistema preko logičnega pogleda, njegovo abstrakcijo poteka razvoja ter se spusti na razredni nivo ter natančneje opiše povezave med razredi.
Zadnji del je namenjen podrobnim specifikacijam storitve kjer se natančno določi njeno obnašanje z uporabo diagramov, avtomatov ali psevdokode. Dokument vsebuje vse potrebne informacije in nudi smernice
za začetek implementacije storitve. Prav tako podaja tudi podroben opis obnašanja sistema v različnih situacijah, ki razvijalcu služi kot opora, oziroma ga usmerja tako, da bo razvojni del projekta
učinkovito izvajan. Cilj je tudi zmanjšati število nepredvidljivih okoliščin pri razvoju.

## 1. Načrt arhitekture

Za načrt arhitekture sistema, natančneje logični in razvojni pogled, sva uporabila UML diagram, ker je enostaven za uporabo in smiselno abstrahira arhitekturo sistema. 

**LOGIČNI POGLED**

V logičnem pogledu so predstavljene glavne programske komponente sistema ter njihove medsebojne relacije.

![Logični pogled](../img/arc_logical.PNG)

**RAZVOJNI POGLED**

V razvojnem pogledu so zbrane tri ključne komponente sistema ter njihov postopek razvoja.

![Razvojni pogled](../img/arc_development.PNG)

## 2. Načrt strukture

### 2.1 Razredni diagram

Na spodnji sliki je prikazan razredni diagram za storitev. Osredotoča se na glavne razrede ter relacije med njimi, ki so nujno potrebni za funkcionalnosti sistema.
Za boljše razumevanje razrednega diagrama so pri opisih razredov dodatne informacije, ki se dopolnjujejo z diagramom. 

![Logični pogled](../img/class.PNG)

### 2.2 Opis razredov

**OSEBA**

Je glavni razred, katerega otroci predstavljajo ljudi, ki interaktirajo s storitvijo ali sistemom.

Atributi:

* ID: int
* ime: String
* priimek: String
* uporabniško_ime: String
* geslo: String
* email: String
* tip_osebe: int

Pripadajoče metode:

* kreiraj_osebo(String uporabniško_ime, String geslo, String email)
* dodaj_info(String ime, String priimek)
* doloci_tip_osebe(int tip)

Dodatne informacije:

* tip_uporabnika lahko zavzame 5 različnih vrednosti, saj imamo 5 različnih tipov oseb razdeljenih v 2 skupini (vzdrževalci sistema in uporabniki).
* ime in priimek sta za uporabnike neobvezni polji - za vzrževalce sistema sta obvezni.

**UPORABNIK** (deduje od razreda oseba)

Je otrok razreda oseba in predstavlja človeka, ki storitev uporablja.

Atributi:

* seznam_aktivnosti: seznam_aktivnosti
* načrt_izvajanja: načrt_izvajanja

Dodatne informacije:

* razred uporabnik predstavlja osnovo za vsakega uporabnika storitve, ki ni vključen v razvoj ali skrb za storitev.
* načrt_izvajanja za navadnega uporabnika je null.

**ŠTUDENT** (deduje od razreda uporabnik)

Uporabnik, ki lahko uporablja vse funkcionalnosti storitve.

Atributi:

* datum_registracije: date 
* izobrazevalna_ustanova: String
* potrjen_uporabnik: boolean
* zacetek_solanja: date
* zakljucek_solanja: date

Pripadajoče metode:

* date vrni_cas_zapadlosti_racuna()

Dodatne informacije:

* študentu za dostop do celotne storitve ni potrebno plačati. To lahko doseže z dokazom o izobraževanju v priznani ustanovi. Ko skrbnik potrdi, da so informacije, ki jih je podal legitimne nastavi vrednost potrjen_uporabnik na true.
* njegov celotni dostop traja do zaključka šolanja.

**PREMIUM UPORABNIK** (deduje od razreda uporabnik)

Uporabnik, ki lahko uporablja vse funkcionalnosti storitve.

Atributi:

* datum_registracije: date
* premium_dostop_do: date

Pripadajoče metode:

* date vrni_cas_zapadlosti_racuna()

Dodatne informacije:

* premium uporabnik mora za dostop do celotne storitve plačati. To pomeni, da se iz navadnega uporabnika prelevi v premium uporabika, ko je plačilo potrjeno in skrbnik opravi ustrezno administracijo.
* trajanje premium računa je odvisno od zakupa trajanja. 

**NAVADEN UPORABNIK** (deduje od razreda uporabnik)

Je uporabnik z najmanj pravicami.

Atributi:

* datum_registracije: date

Dodatne informacije:

* navaden uporabnik ima omejen dostop do funkcionalnosti.

**VZDRŽEVALEC SISTEMA** (deduje od razreda oseba)

Je oseba, ki lahko upravlja, spreminja in nadzoruje storitev.

Atributi:

* zadnja_akcija: String
* zgodovina_aktivnosti: String[]

Pripadajoče metode:

* void pridobi_zadnjo_akcijo()
* void dodaj_akcijo_v_arhiv()

Dodatne informacije:

* pod vzrževalce sistema spadata skrbnik in razvijalec.
* razred hrani zgodovino početja v primeru težav in iskanja odgovornosti.

**SKRBNIK** (deduje od razreda vzdrževalec_sistema)

Skrbnik spada pod vzdrževalce sistema. Njegova naloga je administracija uporabnikov.

Atributi:

* id_skrbnika: int

Pripadajoče metode:

* void administriraj_uporabnika(uporabnik up, int tip, date start, date end)

Dodatne informacije:

* skrbnik ima nadzor nad uporabniki in njihovimi privilegiji.

**RAZVIJALEC** (deduje od razreda vzdrževalec_sistema)

Razvijalec spada pod vzdrževalce sistema. Ima dodatne privilegije, ki mu omogočajo spreminanje nastavitev sistema in spremembo celotne storitve.

Atributi:

* id_razvijalca: int

Pripadajoče metode:

* void spremeni_nastavitve()

Dodatne informacije:

* lahko upravlja s sistemom in uvaja potrebne spremembe.

**AKTIVNOST**

Razred aktivnost vsebuje vse potrebne podatke o aktivnosti. Posamezna aktivnost lahko pripada le enemu uporabniku.

Atributi:

* id_aktivnosti: int
* ime_aktivnosti: String
* tag_aktivnosti: String
* rok_zapadlosti: date
* prioriteta: int
* opravljena: boolean
* komentar: String
* zaključni_komentar: String
* odvisnosti: aktivnost[]

Pripadajoče metode:

* aktivnost kreiraj_aktivnost(String ime_aktivnosti, date rok_zapadlosti)
* void spremeni_info_aktivnosti(int prioritea, String tag, date rok_zapadlosti, String ime)
* void dodaj_prioriteto(int prioriteta)
* void dodaj_komentar(String komentar)
* void dodaj_zaklj_komentar(String komentar)
* void dodaj_tag(String tag)
* void oznaci_aktivnost_kot_opravljeno()
* void odstrani_aktivnost()

**SEZNAM AKTIVNOSTI**

Seznam aktivnosti vsebuje zgodovino vseh aktivnosti uporabnika. Podani so tudi dodatni atributi za lažjo komunikacijo in kreiranje načrta.

Atributi:

* id_seznama: int
* st_aktivnosti: int
* aktivnosti: aktivnost[]
* prioritete: boolean
* tagi: boolean
* st_tagov: int
* tagi_aktivnosti: String[] 

Pripadajoče metode:

* void posodobi_seznam()
* aktivnost[] vrni_aktivnosti_glede_na_tag(String tag)
* aktivnost[] vrni_aktivnosti_glede_na_prioriteto(int prioriteta)
* void sortiraj_po_prioriteti()
* void sortiraj_glede_na_tag()

Dodatne informacije:

* aktivnost[] predstavlja seznam aktivnosti. Funkcije sortiraj preuredijo vrstni red v seznamu.
* spremenljivki "tagi" in "prioritete" olajša delo sistemu pri kreiranju načrta izvajanja, saj ta takoj prepozna, če se prioritete aktivnosti razlikujejo ali, če se aktivnosti delijo na "tag". 

**NAČRT IZVAJANJA**

Načrt izvajanja vsebuje vse nedokončane aktivnosti, ki jih mora uporabnik opraviti. Opravljanje aktivnosti razdeli skozi čas glede na prioriteto aktivnosti in rokom zapadlosti.

Atributi:

* id_nacrta: int
* st_aktivnosti_v_nacrtu: int
* aktivnosti: aktivnost[]

Pripadajoče metode:

* nacrt_izvajanja samodejna_kreacija(aktivnost[] aktivnosti)
* nacrt_izvajanja rocna_kreacija(aktivnost[] aktivnosti, date[] datumi_zacetka, date[] datumi_zakljucka)
* void izbrisi_nacrt()

Dodatne informacije:

* uporabnik lahko izbira med samodejno in ročno kreacija načrta izvajanja. V primeru, da se odloči načrt kreirati sam, mora podati ustrezne parametre za kreacijo načrta.
* v primeru samodejne kreacije sistem sam pridobi potrebne informacije za izgradnjo načrta.

## 3. Načrt obnašanja

![Prijava](../img/Prijava.JPG)
![Dodaj Aktivnost](../img/Dodaj_aktivnost.JPG)
![urejanje Aktivnosti](../img/Urejanje_aktivnosti.JPG)
![Izbris Aktivnosti](../img/Izbris_aktivnosti.JPG)
![Dodaj Komentar](../img/Dodaj_komentar.JPG)
![Opravljena Aktivnost](../img/Opravljena_aktivnost.JPG)
![Nov Nacrt](../img/Nov_nacrt.JPG)



