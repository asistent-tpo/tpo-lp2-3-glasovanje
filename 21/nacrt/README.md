# Načrt sistema

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Jure Vreček, Aljaž Pavišič, Yannick Kuhar in Jernej Vivod |
| **Kraj in datum** | Ljubljana, 22. 4. 2019 |



## Povzetek

V načrtu naše spletne rešitve se bomo sprva dotaknili arhitekture. Predstavljeni so trije diagrami prvi prikazuje funkcionalnosti v plasteh, drugi orodje t.i.
MEAN(MongoDB, ExpressJS, AngularJS in NodeJS) arhitekturo s katero bomo zgradili aplikacijo in tretji podrobnosti implementacije z MVC(Model, View in Controller).
Nato preidemo v strukturo, kjer s podrobnimi opisi razrednih diagramov, atributov in ne samoumevnih metod predstavimo morebitno osnovo naše podatkovne baze. In nazadnje
si bomo pogledali še načrt obnašanja. Za vsak primer uporabe iz LP2 smo sestavili diagram zaporedja, ki prikazujejo tokove dogodkov (osnovni, alternativni in izjemni).



## 1. Načrt arhitekture

**Funkcionalnosti naše spletne aplikacije v diagramu plasti:**

![alt text](../img/layers.png "Funkcionalnosti naše spletne aplikacije v diagramu plasti.")
 
**MEAN arhitektura s katero bomo implementirali našo aplikacijo:**

![alt text](../img/mean.png "MEAN arhitektura.")

**Razvojni diagram naše aplikacije z MVC pristopom:**

![alt text](../img/mvc.png "Razvojni diagram naše aplikacije z MVC pristopom.")


## 2. Načrt strukture

### 2.1 Razredni diagram

![alt text](../img/classDiagram.png "Razredni diagram")

### 2.2 Opis razredov

## Pojasnitev razredov:

---------------------------------------
**Registrirani Uporabnik**

Registrirani uporabnik je uporabnik, ki ima veljaven račun in so mu dostopne vse neskrbniške funkcionalnosti.

**Atributi:**

    - ID: unikatni identifikator
    - UporabniskoIme: unikatno ime, s katerim se uporabnik prijavi v račun
    - Geslo: le uproabniku znano geslo, s katerim se prijavi v račun

**Metode:**

    - PridobiSeznamAktivnosti(): Vrne seznam aktivnosti, povezan z uprabnikom
    - PridobiUrnik(): Vrne urnik, povezan z uporabnikom

---------------------------------------
**Student**

Podrazred razreda RegistriraniUporabnik. Nima nobenih dodatnih pravic.

**Atributi:**
    
    /

**Metode:**

    - PovisajPraviceUS(): metoda spremeni pravice Studenta na nivo Uporabniskega Skrbnika
    - IzbrisiRacun(): metoda dokončno izbriše uporabnika in vse njegove podatke iz baze

---------------------------------------
**Skrbnik**

Podrazred razreda RegistriraniUporabnik. Ima pravico uregati javno objavljene urnike.

**Atributi:**
    
    /

**Metode:**
    
    /
    
---------------------------------------
**Uporabniski Skrbnik**

Podrazred razreda Skrbnik.

**Atributi:**
    
    /

**Metode:**

    - PovisajPraviceSS(): metoda spremeni pravice Studenta na nivo Sistemskega Skrbnika.
    - ZnizajPraviceST(): metoda spremeni pravice Uporabniskega Skrbnika za nivo Studenta
    - IzbrisiRacun(): metoda dokončno izbriše uporabnika in vse njegove podatke iz baze
    
---------------------------------------
**Sistemski Skrbnik**

Podrazred razreda Skrbnik. Ima dostop do vseh skrbniških funkcionalnosti aplikacije in je edini, ki lahko uporablja metode za 
spremembo pravic uporabnikov.

**Atributi:**
    
    /

**Metode:**

    - ZnizajPraviceUS(): metoda spremeni pravice Sistemskega Skrbnika za nivo Uporabniskega Skrbnika

---------------------------------------
**Seznam Aktivnosti**

Razred prestavlja seznam aktivnosti vsakega Registriranega Uporabnika.

**Atributi:**
    
    - ID: unikatni identifikator

**Metode:**

    - OdstraniAktivnost( Aktivnost_ID): metoda odstrani izbrano aktivnost s seznama
    - DodajAktivnost(Aktivnost_Data): metoda s podanimi podatki ustvari novo aktivnost.
    - UrediAktivnost(Aktivnost_ID, Aktivnost_Data): metoda spremeni izbrano aktivnost z novo vnešenimi podatki
    - UvoziUrnikIzBaze(Urnik_ID): metoda izbrani urnik iz javne baze pretvori v aktivnosti in jih doda na seznam
    - UvoziUrnikIzDatoteke( iCalFile): metoda urnik v podani datoteki pretvori v aktinosti in jih doda na seznam
    
    Aktivnost_Data:
         {
             Ime : String,
             Trajanje : Int,
             DatumZacetka : Date,
             KrajAktivnosti : String,
             Opis: String
         }
         
---------------------------------------
**Aktivnost**

Posamezen vnost na seznamu aktivnosti oziroma urniku

**Atributi:**
    
    - ID: unikatni identifikator
    - Ime: ime aktivnosti
    - Trajanje: čas trajanja v urah
    - DatumZacetka: datum začetka aktivnosti
    - Kraj: Kraj dogajanja aktivnosti
    - DatumNastanka: datum nastanka aktivnosti
    - Opis: opis aktivnosti

**Metode:**

    - UrediAktivnost(Aktivnost_Data): metoda spremeni aktivnost s podanimi podatki
    
    Aktivnost_Data:
         {
             Ime : String,
             Trajanje : Int,
             DatumZacetka : Date,
             KrajAktivnosti : String,
             Opis: String
         }
         
---------------------------------------
**Urnik**

Razred predstavlja Urnik vsakega Registriranega Uporabnika

**Atributi:**
    
    - ID: unikatni identifikator

**Metode:**

    - IzvoziKotDatoteko(): metoda Urnik pretvori v .iCal datodeko, ki jo uporabnik nato prenese. V primeru, da datoteka ni prave oblike oziroma
      nima veljavne vsebine, metoda sproži izdjemo invalidFileException.
    - IzvoziVBazo(): matoda Urnik pretvori v JSON objekt in ga doda v javno bazo, kjer čaka na potrditev. V primeru da je kot vhod podana .iCal datoteka,
      metoda preveri veljavnost datoteke. Če ni veljavna, vrne primerno izjemo (invalidFileException, invalidFileTypeException).
    - DodajAktivnost(Aktivnost_ID, Datum): metoda izbrano aktivnost doda na željeno mesto na urniku
    - OdstraniAktivnost(Aktivnost_ID, Datum): metoda izbrano aktivnost odstrani z izbranega mesta na urniku
    - PremakiAktivnost(Aktivnost_ID, DatumSrc, DatumDst): metoda izbrano aktivnost iz izbranega mesta na urniku prestavi na
      novo mesto na urniku
    
    Urnik_JSON:
        {
            Avtor: RegistriraniUporabnik,
            Urnik: [Aktivnosti],
            Potrjen: Boolean,
            Potrditelj: Skrbnik
        }

---------------------------------------
**IzvozeniUrnik**

Razred predstavlja Urnike, ko so jih uporabniki objavili za javno rabo. Dokler je atribut *Potrjen* nastavljen na *false*,
je objevljeni urnik viden le skrbnikom. Ko ga skrbnik potrdi, se atribut nastavi na *true* in je urnik viden vsem uporabnikom.
V bazi so objavljeni urniki shranjeni kot JSON objekti s strukturo definirano v Urnik_JSON in se po potrebi pretvorijo v .iCal datoteko.

**Atributi:**
    
    - ID: unikatni identifikator
    - Potrjen: Boolean, ki označuje ali je urnik že potrjen s strani skrbnika ali ne

**Metode:**

    - Potrdi(): metoda atribut Potrjen nastavi na true
    - Zavrni(): metoda urnik izbriše iz javne baze
    - Preglej(): metoda urnik prikaže v pregledni obliki
    
    Urnik_JSON:
        {
            Avtor: RegistriraniUporabnik,
            Urnik: [Aktivnosti],
            Potrjen: Boolean,
            Potrditelj: Skrbnik
        }

---------------------------------------



## 3. Načrt obnašanja

### 1. Registracija neregistriranega uporabnika

![alt text](../img/registracija.png "registracija uporabnika")

### 2. Prijava registriranega uporabnika

![alt text](../img/prijava.png "prijava uporabnika")

### 3. Odjava registriranega uporabnika

![alt text](../img/odjava_prim.png "odjava uporabnika")

### 4. Odjava registriranega uporabnika - alternativni tok

![alt text](../img/odjava_alt.png "odjava uporabnika alt")

### 4. Dodajanje aktivnosti

![alt text](../img/dodajanje_aktivnosti.png "dodajanje aktivnosti")

### 5. Razporejanje aktivnosti

![alt text](../img/razporejanje_aktivnosti_prim.png "razporejanje aktivnosti")

### 6. Razporejanje aktivnosti - alternativni tok 1

![alt text](../img/razporejanje_aktivnosti_alt1.png "razporejanje aktivnosti alt1")

### 7. Razporejanje aktivnosti - alternativni tok 2

![alt text](../img/razporejanje_aktivnosti_alt2.png "razporejanje aktivnosti alt2")

### 8. Spreminjanje aktivnosti

![alt text](../img/spreminjanje_aktivnosti_prim.png "spreminjanje aktivnosti")

### 9. Spreminjanje aktivnosti - alternativni tok

![alt text](../img/spreminjanje_aktivnosti_alt.png "spreminjanje aktivnosti alt")

### 10 Registrirani uporabnik -> brisanje aktivnosti
![alt text](../img/diagrami_zaporedja_jure/brisanje_aktivnosti.png "registrirani uporabnik -> brisanje aktivnosti")

### 11 Registrirani uporabnik -> uvoz javnega urnika

#### 11.1 uvoz .ical datoteke
![alt text](../img/diagrami_zaporedja_jure/uvoz_ical.png "registrirani uporabnik -> uvoz .ical datoteke")

#### 11.2 uvoz urnika iz javne baze
![alt text](../img/diagrami_zaporedja_jure/uvoz_iz_baze.png "registrirani uporabnik -> uvoz iz baze")

### 12 Objava javnega seznama

#### 12.1 objava trenutnega urnika
![alt text](../img/diagrami_zaporedja_jure/objava_seznama.png "registrirani uporabnik -> objava trenutnega urnika")

#### 12.2 objava .ical datoteke
![alt text](../img/diagrami_zaporedja_jure/objava_ical.png "registrirani uporabnik -> objava .ical urnika")

### 13 Brisanje računa

#### 13.1 študent
![alt text](../img/diagrami_zaporedja_jure/brisanje_racuna.png "registrirani uporabnik -> brisanje računa")

#### 13.2 Uporabniški / sistemski skrbnik
![alt text](../img/diagrami_zaporedja_jure/brisanje_racuna2.png "uporabniški skrbnik / sistemski skrbnik -> brisanje računa")

### 14 Urejanje javnih virov
![alt text](../img/diagrami_zaporedja_jure/urejanje_javnih_virov.png "uporabniški skrbnik / sistemski skrbnik -> urejanje virov")

### 15 Vzdrževanje podatkovne baze

#### 15.1 Sistemski skrbnik pregleda registriranega uporabnika
![alt text](../img/diagrami_zaporedja_jure/vzdrzevanje.png "sistemski skrbnik -> vzdrževanje")

#### 15.2 Sistemski skrbnik pregleda izvožen urnik
![alt text](../img/diagrami_zaporedja_jure/vzdrzevanje2.png "sistemski skrbnik -> vzdrževanje urnik")

#### 15.3 Registrirani uporabnik ni avtoriziran
![alt text](../img/diagrami_zaporedja_jure/vzdrzevanje3.png "registrirani uporabnik -> 401")
