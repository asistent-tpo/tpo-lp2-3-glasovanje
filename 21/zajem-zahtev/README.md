# Dokument zahtev

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Jure Vreček, Yannick Kuhar, Aljaž Pavišič in Jernej Vivod |
| **Kraj in datum** | Ljubljana, 7. 4. 2019 |



## Povzetek projekta

Projekt zadeva razvoj spletne aplikacije, ki bo študentom omogočala centralizirani pregled nad roki vseh njihovih obveznosti ter njihovim urnikom. Aplikacija bo registriranim uporabnikom omogočala dodajanje
obveznosti in urnika v centralno lokacijo, katere uporabniški vmesnik bo študentom omogočal enostaven in intuitiven pregled nad vso vsebino. Aplikacija bo omogočala tudi uvoz urnika z uporabo urnikov
oziroma koledarjev, kodiranih v datotekah po standardu iCalendar, katerih generiranje podpira mnogo spletnih strani. Uporabnik bo kreirano vsebino lahko tudi izvozil v tem formatu in jih javno objavil v centralnem repozitoriju.


## 1. Uvod

Mnogim študentom k komulativi stresa po nepotrebnem prispevajo preglavice, ki jih povzroča obilica rokov in terminov, katerih spoštovanje je velikokrat ključ do uspešnega študija.
Spregledati pomemben rok za oddajo seminarske naloge lahko za študenta pomeni cel kup težav, ki bi se jim lahko izognil, če bi takšen študent imel centraliziran pregled nad vsem,
česar ne sme pozabiti. Takšen sistem pomeni tudi možnost, da študent mirno odmisli te skrbi in posledično več svojega uma prihrani ostalim stvarem.

Aplikacija StraightAs bo študentu ponujala pregleden in intuitiven vmesnik do vseh njegovih rokov in urnikov, ki jih potrebuje. Študent bo imel pregled nad urniki predavanj in vaj,
datumi kolokvijev, roki za oddajo seminarskih nalog, izpitnimi roki in ostalimi stvarmi, ki spadajo k študentkem življenju. Uporabniku aplikacije bo omogočeno uvažanje urnikov in koledarjev z uporabo
zapisov v formatu, ki ga definira standard iCalendar in katerih generiranje podpira mnogo urnikov, ki so objavljeni na spletnih straneh fakultet. Svoj urnik lahko študent tudi izvozi v tej obliki in
ga objavi v javnem repozitoriju takšnih datotek.

Zaledni sistem aplikacije bo izpostavljen v obliki dobro dokumentiranega (z uporabo OpenApi specifikacije) aplikacijskega programskega vmesnika zgrajenega po principu REST, ki bo omogočal integracijo podatkov z aplikacijami,
ki bodo za to domeno morda razvite kasneje (na primer Android in iOS aplikaciji).

Uporabnik bo za uporabo aplikacije kreiral uporabniški račun, z uporabo katerega se bo lahko prijavil in ustvaril svoj osebni koledar, ki bo ponujal intuitiven pregled nad dnevnimi, tedenskimi, mesečnimi in letnimi obveznostimi
in dogodki. Aplikacija se od množice aplikacij, ki zajemajo podobno splošno domeno razlikuje po tem, da je ustvarjena posebno za študente in je zaradi večje specifičnosti za takšne namene preglednejša in lažja za uporabo.


## 2. Uporabniške vloge

Uporabniška vloga 			           | Opis
-------------------------------------- | ----
**Neregistrirani uporabnik:**          | Ima na voljo le domačo stran aplikacije in strani za registracijo.
**študent (Registrirani uporabnik):**  | Ima svoj seznam aktivnosti, lahko jih dodaja, spreminja, odstranjuje, ali uvozi iz zunanjih virov. Lahko tudi ponudi svoj seznam aktivnosti za javno objavo.
**Družbeni skrbnik:** 	               | Ima na voljo vse funkcionalnosti registriranega uporabnika, le da lahko tudi odobri objavo virov in urnikov, ki so jih drugi uporabniki predlagali.
**Sistemski skrbnik:** 				   | Poleg vseh funkcionalnosti Družbenega skrbnika ima tudi moč izbrisati uporabnika iz apliakcije, če je le-ta kršil pravila. Ima tudi dostop do podatkovne baze za namene vzdrževanje in upravljanja.



## 3. Slovar pojmov

Pojem 			          | Razlaga
--------------------------| -------
**Seznam aktivnosti:**    | seznam vseh aktivnosti, ki jih je uporabnik dodal ali uvozil
**Urnik:** 			      | pregledno prikazan razpored aktivnosti za določeno časovno obdobje, na katerem lahko uporabnik poljubno razporeja aktivnosti s svojega seznama aktivnosti
**Aktivnost:**            | vnos na seznamu aktivnosti, ki ima sledeče vrednosti: naziv, čas trajanja, datum in čas začetka (opcijsko), lokacija izvajanja (opcijsko), prioriteta, kratek opis (opcijsko)	
**Prioriteta:**           | številska vrednost z itervala [0, 10], ki določa pomembnost aktivnosti (nižja vrednosti pomeni višjo prioriteto).
**Javna baza:**           | podatkovna baza, dostopna vsem registriranim uporabnikom, v kateri so shranjeni vsi objavljeni in s strani skrbnika odobreni seznami aktivnosti.
**Domača stran:**         | osnovna stran aplikacije, ki na voljo vsem uporabnikom. Ponuja funkcionalnosti *prijava* in *registracija*
**Nadzorna plošča:**   	  | stran, ki je dostopna uporabniku po prijavi. Ponuja vse funkcionalnosti, ki so na voljo registriranemu uporabniku
**Javna baza:**	          | podatkovna baza, dostopna vsem registriranim uporabnikom, v kateri so shranjeni vsi objavljeni in s strani skrbnika odobreni seznami aktivnosti.
**Podatkovna baza:**      | podatkovna baza, dostopna le članom razvojne ekipe, v kateri so shranjeni vsi podatki o registriranih uporabnikih
**Struktura baze:**       | kako je baza sestavljena, število tabel in stolpcev ter kako so tabele med sabo povezane
**Proizvajalec baze:**    | podjetje, ki je ustvarilo podatkovno bazo, ki jo uporabljamo
**Licenca proizvajalca:** | pogoji za uporabo podatkovne baze proizvajalca
**Varnostna kopija:**     | posebna kopija podatkov, ki se uporablja za obnovo baze po napaki
**Domača stran:** 		  | osnovna stran aplikacije, ki na voljo vsem uporabnikom. Ponuja funkcionalnosti *prijava* in *registracija*
**Arhivirani podatki:**	  | podatki, ki se ne nahajajo v aktivni bazi, ampak na arhivnih strežnikih
**Nadzorna plošča:**      | stran, ki je dostopna uporabniku po prijavi. Ponuja vse funkcionalnosti, ki so na voljo registriranemu uporabniku
**Registrirani uporabnik**| študent, uporabniški skrbnik ali sistemski skrbnik (krajše "RU")



## 4. Diagram primerov uporabe

![Primeri-uporabe](../img/primeri_uporabe_diag.png)

## 5. Funkcionalne zahteve

--------------------------------------------------------------------------------------------------

**Registracija:**

--------------------------------------------------------------------------------------------------
	
**Povzetek:** Neregistrirani uporabnik lahko v obrazec na strani za registracijo vnese
		  zahtevane podatke, s katerimi sistem nato oblikuje nov uporabniški račun.

**Osnovni tok:**

	1. Neregistrirani uporabnik izbere funkcionalnost *registracija*
	2. Sistem prikaže obrazec za vnos potrebnih podatkov
	3. Sistem pridobi vnosno poljo Google reCAPTCHA iz zunanjega vira in ga prikaže
	3. Neregistrirani uporabnik obrazec izpolni
	4. Neregistrirani uporabnik potrdi svoj vnos
	5. Sistem iz vnešenih podatkov oblikuje nov uporabniški račun
	6. Sistem sedaj registriranegu uporabniku prikaže obvestilo o uspehu registracije
	
**Alternativni tok:**
	/
	
**Izjemni tok:**

	- Neregistrirani uporabnik je vnesel neustrezne podatke. Sistem prikaže ustrezno
	  obvestilo.
	- Neregistrirani uporabnik že ima uporabniški račun z vnešenim e-poštnim naslovom.
	  Sistem prikaže ustrezno obvestilo.
	- Neregistrirani uporabnik je vnesel uporabiško ime, ki je že zasedeno. Sistem 
	  prikaže ustrezno obvestilo.

**Pogoji:**

	- Uporabnik ne sme biti prijavljen v sistem. Če je uprabnik prijavljen, funkcionalnost
	  ni na voljo.
	
**Posebnosti:**
	/
	
**Prioriteta:**

	- Must have
	
**Sprejemni testi:**

	- Izpolni obrazec za registracijo z ustreznimi podatki. Ko sistem vrne sporočilo o
	  uspehu, se poskusi prijaviti z vnešenim uporabniškim imenom in geslom.
	- Izpolni obrazec z neustreznimi podatki.
	- Poskusi se registrirati z uporabniškim imenom, ki že obstaja.

--------------------------------------------------------------------------------------------------

**Prijava:**

--------------------------------------------------------------------------------------------------
		
**Povzetek:** študent lahko v obrazec na strani za prijavo vnese uporabniško ime in geslo. S tem se prijavi v svoj uporabniški račun.

**Osnovni tok:**

	1. RU izbere funkcionalnost *prijava*
	2. Sistem prikaže obrazec za vnos uporabniškega imena in gesla
	3. RU obrazec izpolni
	4. RU potrdi svoj vnos
	5. Sistem preveri veljavnost podatkov
	6. Sistem študentu prikaže nadzorno ploščo in omogoči vse funkcionalnosti
	   registriranega uporabnika
	
**Alternativni tok:**
	/
	
**Izjemni tok:**

	- RU je vnesel napačno ime ali geslo. Sistem prikaže opozorilo "Napačno
	  ime ali geslo".

**Pogoji:**

	- RU ne sme biti prijavljen v sistem. Če je prijavljen, je namesto prijave na voljo 
	  funkcionalnost *odjava*
	- RU mora imeti veljaven uporabniški račun in poznati prijavne podatke
	
**Posebnosti:**
	/
	
**Prioriteta:**

	- Must have
	
**Sprejemni testi:**

	- Izpolni obrazec za prijavo z veljavnimi podatki. Sistem mora prijavo sprejeti in prikazati
	  domačo stran uporabnika.
	- Izpolni obrazec z neveljavnimi podatki. Sistem mora prijavo zavrniti in izpisati opozorilo.	

--------------------------------------------------------------------------------------------------

**Odjava:**

--------------------------------------------------------------------------------------------------
	
**Povzetek:** Regstrirani uporabnik se lahko izpiše iz svojega računa. S tem se uporabiška seja zaključi.

**Osnovni tok:**

	1. RU izbere funkcionalnost *odjava*
	2. Sistem zaključi uporabniško sejo
	3. Sistem uporabniku prikaže domačo stran
	
**Alternativni tok:**

	1. RU je na strani neaktiven dalj časa
	2. Sistem neaktivnost zazna in avtomatsko zaključi uporabnikovo sejo
	
**Izjemni tok:**
	/

**Pogoji:**

	- RU mora biti prijavljen v sistem. Če ni prijavljen, je namesto odjave na voljo 
	  funkcionalnost *prijava*
	
**Posebnosti:**
	/
	
**Prioriteta:**

	- Must have
	
**Sprejemni testi:**

	- Prijavi se v veljaven uporabniški račun in se poskusi odjaviti. Sistem mora prikazati domačo
	  stran in sejo končati.
	- Po odjavi se preko brskalnika poskusi vrniti na prejšnjo stran. Sistem mora ponovno zahtevati 
	  prijavo.
	- Po prijavi ostani neaktiven dalj časa. Sistem mora sejo avtomatsko končati.

--------------------------------------------------------------------------------------------------

**Dodajanaje aktivnosti:**

--------------------------------------------------------------------------------------------------

**Povzetek:** Študent lahko v obrazec na strani za dodajanje aktivnosti vnese
		  zahtevane podatke, s katerimi aplikacija nato oblikuje novo aktivnost in jo doda
		  na uporabnikov urnik.

**Osnovni tok:**

	1. RU izbere funkcionalnost *dodajanje aktivnost*
	2. Sistem prikaže obrazec za vnos potrebnih podatkov
	3. RU obrazec izpolni
	4. RU potrdi svoj vnos
	5. Sistem iz vnešenih podatkov oblikuje novo aktivnost
	6. Sistem uporabniku prikaže obvestilo ustvarjeni aktivnosti
	7: Sistem ustvarjeno aktivnost doda na uporabnikov urnik
	
**Alternativni tok:**
	/
	
**Izjemni tok:**

	- RU je vnesel neustrezne podatke. Sistem prikaže ustrezno
	  obvestilo.

**Pogoji:**

	- RU mora biti prijavljen z veljavnim uporabniškim računom
	
**Posebnosti:**
	/
	
**Prioriteta:**

	- Must have
	
**Sprejemni testi:**

	- Poskusi dodati novo aktivnost z ustreznimi podatki. Aktivnost se pojavi na seznamu 
	  aktivnosti in sistem izpiše obvestilo o uspehu.
	- Poskusi dodati novo aktivnost z neustreznimi podatki. Sistem prikaže opozorilo.

--------------------------------------------------------------------------------------------------

**Razporejanje aktivnosti:**

--------------------------------------------------------------------------------------------------

Povzetek: RU lahko aktivnosti s seznama razporeja po svojem urniku.

**Osnovni tok:**

	1. RU izbere funkcionalnost aktivnost s seznama aktivnosti
	2. RU izbrano aktivnost povleče (drag & drop) na željeno mesto na
	   urniku
	3. Sistem aktivnost prikaže na izbranem mestu na urniku
	
**Alternativni tok 1:**

	1. RU izbere aktivnost, ki je že na urniku.
	2. RU aktivnost prestavi (drag & drop) na drugo mesto na urniku.
	3. Sistem aktivnost izbriše iz trenutnega mesta in jo postavi na novo izbrano mesto.
	 
**Alternativni tok 2:**

	1. RU izbere aktivnost, ki je že na urniku.
	2. RU izbere možnost za odstranitev aktivnosti z urnika.
	3. Sistem aktivnost izbriše aktivnost iz urnika.
	
**Izjemni tok:**

	- RU je izbrano aktivnost želel postaviti na mesto, ki ga že zaseda
	  neka aktivnost. Sistem poskus zavrne in izpiše opozorilo.

**Pogoji:**

	- RU mora biti prijavljen z veljavnim uporabniškim računom in imeti na seznamu 
	  aktivnosti vsaj eno aktivnost.
	
**Posebnosti:**
	/
	
**Prioriteta:**

	- Must have (dodajanje in odstranjevanje aktivnosti)
	- Should have (premikanje obstoječih aktivnosti)
	
**Sprejemni testi:**

	- Izberi aktivnost s seznama in jo poskusi dodati na prazno mesto na urniku. Sistem jo
	  mora dodati na urnik.
	- Izberi aktivnost s seznama in jo poskusi dodati na že zasedeno mesto na urniku. Sistem 
	  mora poskus zavrniti in prikazati opozorilo.
	- Izbrei aktivnost na urniku in jo poskusi premakniti na praznomesto na urniku. Sistem 
	  mora aktivnost dodati na novo mesto in jo izbrisati s starega.
	- Izbrei aktivnost na urniku in jo poskusi premakniti na že zasedeno mesto na urniku. 
	  Sistem mora aktivnost poskus zavrniti in prikazati opozorilo.
	- Izberi aktivnost na urniku in jo poskusi izbrisati. Sistem mora aktivnost odstraniti z 
	  urnika

--------------------------------------------------------------------------------------------------

**Spreminjanje aktivnosti:**

--------------------------------------------------------------------------------------------------

**Povzetek:**  Registrirani uporabnik lahko obstoječo aktivnost na seznamu spreminja po želji.

**Osnovni tok:**

	1. RU izbere funkcionalnost *spreminjanje aktivnosti*
    2. Sistem prikaže obrazec za vnos potrebnih podatkov
    3. RU obrazec izpolni
	4. RU potrdi svoj vnos
	5. Sistem iz vnesenih podatkov spremeni obstoječo aktivnost
    6. Sistem uporabniku prikaže obvestilo spremenjeni aktivnosti
    7. Sistem aktivnost na uporabnikovem urniku ustrezno spremeni
	
**Alternativni tok:**

	- RU izbere aktivnost, ki je na urniku.
    - RU izbrano aktivnost izbriše iz urnika.
    - RU doda izbrisano aktivnost z novimi podatki.
	
**Izjemni tok:**

	- RU je vnesel neustrezne podatke. Sistem prikaže ustrezno obvestilo.

**Pogoji:**

	- RU mora biti prijavljen z veljavnim uporabniškim računom in imeti na seznamu aktivnosti vsaj eno aktivnost.

**Posebnosti:**
	- Should have
	
**Prioriteta:**
	/

**Sprejemni testi:**

	- Izberi aktivnost na urniku in ji poskusi spremeniti naziv. 
	  Sistem mora aktivnosti spremeniti naziv.  
    - Izberi aktivnost na urniku in ji poskusi spremeniti naziv 
	  na naziv druge aktivnosti. Sistem mora poskus zvrniti in prikazati opozorilo.
    - Izberi aktivnosti na urniku in ji poskusi spremeniti čas trajanja.
	  Sistem ji mora spremeni čas trajanja.
    - Izberi aktivnost na urniku in ji poskusi spremeniti čas trajanja na nič.
	  Sistem mora poskus zvrniti in prikazati opozorilo.
    - Izberi aktivnost na urniku in ji poskusi spremeniti prioriteto.
	  Sistem mora aktivnosti spremeniti prioriteto.
    - Izberi aktivnost na urniku in ji poskusi spremeniti prioriteto izven intervala [0, 10].
	  Sistem mora poskus zvrniti in prikazati opozorilo.
		  
--------------------------------------------------------------------------------------------------

**Brisanje aktivnosti:**

--------------------------------------------------------------------------------------------------

**Povzetek:** Registrirani uporabnik lahko obstoječo aktivnost na seznamu odstrani.

**Osnovni tok:**

	1. RU izbere funkcionalnost *brisanje aktivnosti*
    2. RU izbere aktivnost na seznamu
    3. Sistem izbrano aktivnost odstrani s seznama. Prav tako odstrani vse pojavitve aktivnosti
	   z urnika
	
**Alternativni tok:**
	/
	
**Izjemni tok:**

	/

**Pogoji:**

	- Študent mora biti prijavljen z veljavnim uporabniškim računom in imeti na seznamu aktivnosti vsaj eno aktivnost.

**Posebnosti:**
	/
	
**Prioriteta:**

	- Must have

**Sprejemni testi:**

	 - Izberi aktivnost na seznamu in jo poskusi izbrisati. Sistem mora aktivnost odstraniti s seznama in urnika.		

--------------------------------------------------------------------------------------------------

**Uvoz javnega urnika:**

--------------------------------------------------------------------------------------------------

**Povzetek:** Registrirani uporabnik lahko iz zunanjega vira ali javne baze pridobi .ical datoteko
		  in v njej opisane aktivnost vnese v svoj seznam.

**Osnovni tok:**
	
	1. RU izbere funkcionalnost *uvoz javnega urnika*
    2. Sistem prikaže okno za vnos .ical datoteke
    3. RU izbere .ical datoteko in jo povleče (drag & drop) v okno
    4. Sistem prebere podatke z datoteke in jih doda na seznam aktivnosti
	
**Alternativni tok:**
	/
	
**Izjemni tok:**
	
	- RU je vnesel datoteko napačnega tipa. Sistem prikaže ustrezno
      obvestilo.
	- RU je vnesel datoteko, ki vsebuje napake. Sistem prikaže ustrezno
      obvestilo.

**Pogoji:**

	- RU mora biti prijavljen z veljavnim uporabniškim računom

**Posebnosti:**
	/
	
**Prioriteta:**
	
	- Could have

**Sprejemni testi:**
	
	- Vnos .ical datoteke. Sistem mora prebrati .ical datoteko in v njej opredeljene
	  aktivnosti dodati na seznam aktivnosti.
    - Vnos .ical datoteke z napako. Sistem mora poskus zavrniti in
      prikazati opozorilo.
    - Vnos datoteke, ki nima končnice .ical. Sistem mora poskus zavrniti in
      prikazati opozorilo.

--------------------------------------------------------------------------------------------------

**Objava javnega seznama:**

--------------------------------------------------------------------------------------------------

**Povzetek:** Študent lahko svoj urnik pretvori v .ical datoteko in jo predlaga za
		  objavo v javni bazi.

**Osnovni tok:**
	
	1. RU izbere funkcionalnost *objava javnega seznama*
    2. Sistem zahteva potrditev izbire
    3. RU izbiro potrdi
    4. Sistem na podlagi urnika generira .ical datoteko
	5. Sistem datoteko doda na seznam datotek za pregled s strani skrbnika
	
**Alternativni tok:**
	
	- RU .ical datoteko pridobi iz zunanjega vira in jo poda v temu namenjeno polje. Sistem
	  datoteko doda na seznam datotek za pregled s strani skrbnika.
	
**Izjemni tok:**
	
	- RU poskusi objaviti datoteko napačnega formata
	- RU poskusi v .ical datoteko pretvorit prazen urnik. Sistem poposkus zavrne in prikaže 
	  opozorilo.

**Pogoji:**
	/

**Posebnosti:**

	- RU mora biti prijavljen z veljavnim uporabniškim računom in imeti na urniku vsaj eno aktivnost.
	
**Prioriteta:**
	
	- Would have

**Sprejemni testi:**
	
	- Sestavi urnik iz aktivnosti s seznama in ga poskusi pretvoriti v .ical datoteko. Sistem datoteko
	  generira in jo doda na seznam za pregled.
	- Že generirano .ical datoteko uvozimo. Sistem mora datoteko dodati na seznam za pregled.
    - Poskusi pretvoriti prazen urnik v .ical datoteko. Sistem poskus zavrne in prikaže ustrezno opozorilo.
	- Poskusi objaviti datoteko napačnega formata. Sistem mora poskus zavrniti in prokazati ustrezno opozorilo.

--------------------------------------------------------------------------------------------------

**Brisanje uporabniškega računa:**

--------------------------------------------------------------------------------------------------

**Povzetek:** Študent lahko svoj račun dokončno izbriše.

**Osnovni tok:**

	1. RU izbere funkcionalnost *Brisanje uporabniškega računa*
	2. Sistem zahteva potrdilo in ponovn vnos gesla
	3. RU vnese geslo in potrdi odločitev
	4. Sistem uporabnika odjavi
	5. Sistem iz podatkovne baze odstrani račun uporabnika
	6. Sistem prikaže uporabniku domačo stran in obvestilo o uspehu izbrisa računa
	
**Alternativni tok:**

	1. Sistemski skrbnk je obveščen o kršitvi pravil aplikacije
	2. Sistemski skrbnik izbere funkcionalnost *Brisanje uporabniškega računa*
	3. Sistem skrbniku ponudi možnost izbire računa za izbrisa
	4. Sistemski skrbnik izbere željeni račun za izbris
	5. Sistem poskrbi, da je izbrani uporabnik odjavljen
	6. Sistem iz podatkovne baze izbriše izbrani uporabniški račun
	7. Sistem skrbniku prikaže obvestilo o uspešnem izbrisu uporabnika 
	
**Izjemni tok:**

	- Sistemski skrbnik skuša izbrati račun, ki ne obstaja. Sistem prikaže ustrezno obvestilo.

**Pogoji:**

	- RU je prijavljen v uporabniški račun
	ALI
	- Sistemski skrbnik je obveščen o kršitvi pravil aplikacije

**Posebnosti:**
	/
	
**Prioriteta:**

	Should have

**Sprejemni testi:**

	- Prijavi se v veljaven uporabniški račun in ga poskusi izbrisati. Sistem mora izvesti odjavo
	  in izbris, ter prikazati domačo stran in obvestilo o uspešnem izbrisu.
	- Prijavi se kot sistemski skrbnik in poskusi izbrisati veljaven uporabniški račun. Sistem mora
	  izbranega uporabnika odjaviti, račun izbrisati in skrbniku prikazati obvestilo o uspešnem izbrisu.
	- Prijavi se kot sistemski skrbnik in poskusi izbrisati neveljaven uporabniški račun. Sistem
	  prikazati obvestilo o napaki.
	

--------------------------------------------------------------------------------------------------

**Urejanje javnih virov:**

--------------------------------------------------------------------------------------------------

**Povzetek:** Uporabniški ali sistemski skrbnik lahko vidi vse iCalalendar datoteke ki so jih ostali predlagal za javno objavo. Po pregledu ustreznosti jih, če so ustrezne, javno objavi.

**Osnovni tok:**

	1. Uporabniški ali sistemski skrbnik izbere funkcinalnost *Urejanje javnih virov*
	2. Sistem prikaže seznam vseh .ical datotek, ki čakajo potrditev
	3. Skrbnik izbere datoteko in jo pregleda
	4. Skrbnik datoteko potrdi za objavo
	5. Sistem datoteko odsterani s seznama čakajočih in jo doda v javno bazo
	
**Alternativni tok:**
	
	- Skrbnik se odloči, da datoteka ni primerna za objavo. Sistem datoteko izbriše.
	
**Izjemni tok:**
	/

**Pogoji:**

	- Uporabnik je prijavljen v račun s skrbniškimi pravicami

**Posebnosti:**
	/
		
**Prioriteta:**

	- Could have

**Sprejemni testi:**
	
	- Prijavi se kot skrbnik. Izberi datoteko s seznama čakajočih in izberi možnost za pregled.
	  Sistem prikaže vsebino datoteke
	- Prijavi se kot skrbnik. Izberi datoteko s seznama čakajočih in jo potrdi za objavo. Sistem
	  mora datoteko dodati v javno bazo in jo odstraniti s seznama čakajočih
	- Prijavi se kot skrbnik. Izberi datoteko s seznama čakajočih in jo izbriši. Sistem mora datoteko
	  odstraniti s seznama čakajočih

--------------------------------------------------------------------------------------------------

**Vzdrževanje podatkovne baze:**

--------------------------------------------------------------------------------------------------

**Povzetek:** Sistemski skrbnik si lahko ogleda celotno vsebino podatkovne baze

**Osnovni tok:**

	1. Sistemki skrbnik izbere funkcionalnost *vzdrževanje podatkovne baze*
	2. Sistem prikaže seznam vseh uporabnikov in vseh datotek v javni bazi in seznamu čakajočih
	3. Sistemski skrbnik izbere uporabnika s seznama
	4. Sistem skrbniku prikaže uporabnikov urnik in njegov seznam aktivnosti
	
**Alternativni tok:**
	
	- Sistemski skrbnik si želi ogledati datoteko s seznama čakajočih. Ko jo izbere, jo sistem prikaže
	- Sistemski skrbnik si želi ogledati datoteko iz javne baze. Ko jo izbere, jo sistem prikaže
	
**Izjemni tok:**
	
	- Uporabnik brez skrbniških pravic je skušal dostopati do funkcionalnosti. Sistem poskus zavrne
	  in prikaže ustrezno obvestilo.

**Pogoji:**

	 - RU mora biti prijavljen v račun s pravicami sistemskega skrbnika.

**Posebnosti:**
	/
	
**Prioriteta:**
	
	- Must have

**Sprejemni testi:**
	
	- Prijavi se kot sistemski skrbnik. Dostopaj do okna za upravnjanje z bazo
	- Izberi uporabnika s seznama. Sistem mora prikazati uporabnikov urnik in seznam aktivnosti
	- izberi datoteko s seznama čakajočih. Sistem prikaže vsebino datoteke
	- izberi datoteko iz javne baze. Sistem prikaže vsebino datoteke
	

## 6. Nefunkcionalne zahteve

--------------------------------------------------------------------------------------------------

**Zahteve izdelka:**

--------------------------------------------------------------------------------------------------

1. Aplikacija mora biti razvita tako, da bo uporabna in estetsko ustrezna na vseh pogosto uporabljenih napravah.
	V ta namen bo uporabniški vmesnik zasnovan tako, da se bodo ključni elementi avtomatsko prilagajali
	velikosti zaslona naprave. Knjižnica oziroma ogrodje, ki omogoča hiter in enostaven razvoj takšnih uporabniških vmesnikov je
	Bootstrap.

2. Aplikacija mora spočetka podpirati vsaj 1000 hkratnih uporabnikov. Kot verifikacijo izpolnjenosti te zahteve lahko uporabimo orodje JMeter, ki
	simulira različne stopnje obremenitve spletne strani. Za zadostitev tej zahtevi bo potrebo razviti učinkovito kodo in aplikacijo gostiti na dovolj zmogljivi
	strojni opremi.

3. Aplikacija mora biti na voljo 99.9% časa, kar je enako 87.6 ur nedosegljivosti v letu. Za dosego te zahteve je potrebno aplikacijo gostiti
	na zanesljivi strojni opremi, ki ima določeno stopnjo redundance. 

4. Podatki posameznih registriranih uporabnikov morajo biti v pregled in spreminjanje na voljo le njim. V ta namen je potrebno zagotoviti
	ustrezno mero varnosti vseh elementov aplikacije, kar se lahko vrednoti tudi z orodji za avtomatsko testiranje.  


5. Sistem mora biti na voljo na javno dostopnem spletnem naslovu, ki je dosegljiv iz vseh lokacij, ki imajo prost dostop do
	svetovnega spleta. V ta namen je potrebno priskrbeti ustrezno domeno na kateri bo ta aplikacija na voljo.  
	
--------------------------------------------------------------------------------------------------

**Organizacijske zahteve:**

--------------------------------------------------------------------------------------------------

1. Uporabnik mora pri registraciji potrditi svojo pristnost. S tem postopkom se izognemo preglavicam povezanim z možnostjo masovnih strojno podprtih
	registracij. Tej zahtevi lahko zadostimo z uporabo sistemov za preverjanje pristnosti uporabnikov kot je na primer prosto dostopna storitev ReCAPTCHA podjetja Google.
	Za uspešno registracijo mora uporabnik potrditi svoj elektronski naslov.
	

2. Uporabnik se za pregled svojih osebnih podatkov identificira s uporabniškim imenom, ki je hkrati tudi uporabnikov primarni identifikator, ter geslom.

--------------------------------------------------------------------------------------------------

**Zunanje zahteve:**

--------------------------------------------------------------------------------------------------

1. Aplikacija mora podpirati uvoz standardnih tipov datotek, ki se navezujejo na koledarje, kot je na primer standard iCalendar.

2. Aplikacija mora biti skladna s Splošno uredba o varstvu podatkov (GDPR), kar pomeni, da ne sme zbirati oziroma hraniti določenih
	podatkov. Vsakemu uporabniku je zagotovljena tudi možnost izbrisa vseh svojih podatkov, ki so shranjeni v podatkovni bazi. Vsak uporabnik
	mora biti tudi obveščen o vsakršnem zbiranju njegovih podatkov.




## 7. Prototipi vmesnikov

### 1. Neregistrirani uporabnik -> registracija

Neregistrirani uporabnik mora na obrazcu za registracijo vnesti uporabniško ime, e-poštni naslov in geslo. Zraven polja za vnos gesla je prikazan kriterij za sprejem le-tega.
Izjemni tokovi:

* V primeru neustreznih podatkov se izpiše opozorilo "Vnešeni podatki niso ustrezni!".
* V primeru zasedenega uporabniškega imena se izpiše opozorilo "To uporabniško ime že obstaja!".
* V primeru že obstoječega uporabniškega računa s tem emailom se izpiše opozorilo "Za ta email že obstaja uporabniški račun!".

![alt text](../img/zaslonske_maske/neregistrirani_uporabnik_registracija.png "Neregistrirani uporabnik -> registracija")

### 2. Registrirani uporabnik -> prijava

Obrazec za prijavo je za študenta, uporabniškega skrbnika in sistemskega skrbnika enak. Vpisati morajo svoje uporabniško ime in geslo, ki se morata navezovati na obstoječ račun.
Izjemni tokovi:

* V primeru napačnih podatkov se izpiše opozorilo "Napačno ime ali geslo!"

![alt text](../img/zaslonske_maske/student_prijava.png "registrirani uporabnik -> prijava")

### 3. Registrirani uporabnik -> odjava

Postopek odjave za študenta, uporabniškega skrbnika in sistemskega skrbnika je enak. Vsakemu bo v desnem delu navigacijske vrstice prikazan gumb "Odjava", ne glede na ostalo vsebino strani, ki bo sprožil ustrezno funkcionalnost.

![alt text](../img/zaslonske_maske/student_odjava.png "registrirani uporabnik -> odjava")

### 4. Registrirani uporabnik -> dodajanje aktivnosti

Obrazec za dodajanje nove aktivnosti je dostopen s klikom na "Nadzorna plošča"->"Dodaj aktivnost". Uporabnik mora vnesti ime aktivnosti in njeno trajanje, polja za začetek in konec ter opis pa so opcijska.
Izjemni tokovi:

* V primeru vnosa neustreznih podatkov se izpiše opozorilo "Vnešeni podatki niso ustrezni!"

![alt text](../img/zaslonske_maske/student_dodajanje_aktivnosti.png "registrirani uporabnik -> dodajanje aktivnosti")

### 5. Registrirani uporabnik -> razporejanje aktivnosti

Registrirani uporabnik lahko aktivnost iz seznama aktivnosti in jo povleče na urnik, če je tam prostor. Desni klik na posamezno aktivnost prikaže seznam možnosti.
Izjemni tokovi:

* V primeri, da na izbrani poziciji urnika aktivnost že obstaja, se poleg kurzorja pojavi opozorilo "Na tej poziciji aktivnost že obstaja!"

![alt text](../img/zaslonske_maske/student_razporejanje_aktivnosti.png "registrirani uporabnik -> razporejanje aktivnosti")

### 6. Registrirani uporabnik -> spreminjanje aktivnosti

Registrirani uporabnik lahko klikne na možnost "Uredi..." iz menija, ki se prikaže ob desnem kliku na aktivnost. Nato se prikaže obrazec za urejanje le-te aktivnosti.
Izjemni tokovi:

* V primeru vnosa neustreznih podatkov se izpiše opozorilo "Vnešeni podatki niso ustrezni!"

![alt text](../img/zaslonske_maske/student_spreminjanje_aktivnosti.png "registrirani uporabnik -> spreminjanje aktivnosti")

### 7. Registrirani uporabnik -> brisanje aktivnosti

Registrirani uporabnik lahko klikne na možnost "Izbriši..." iz menija, ki se prikaže ob desnem kliku na aktivnost. Nato se prikaže potrdilno okno.

![alt text](../img/zaslonske_maske/student_brisanje_aktivnosti.png "registrirani uporabnik -> brisanje aktivnosti")

### 8. Registrirani uporabnik -> uvoz javnega urnika

Registrirani uporabnik s klikom na gumb "Uvozi" odpre obrazec za uvoz .ical datotek, ki vsebujejo podatke o urnikih. Svojo datoteko lahko povleče v za to namenjeni gradnik.
Izjemni tokovi:

* V primeru vnosa datoteke neustreznega formata se izpiše opozorilo "Datoteka ni v .ical formatu!"
* V primeru vnosa poškodovane datoteke / datoteke z napakami se izpiše opozorilo "Datoteka vsebuje napake!"

![alt text](../img/zaslonske_maske/student_uvoz_urnika.png "registrirani uporabnik -> uvoz urnika")

### 9. Registrirani uporabnik -> objava javnega seznama

Registrirani uporabnik s klikom na gumb "Objavi" odpre potrditveno okno za objavo svojega urnika.
Izjemni tokovi:

* V primeru praznega urnika se namesto potrditvenega okna poleg kurzorja prikaže opozorilo "Vaš urnik je prazen!"

![alt text](../img/zaslonske_maske/student_objava_urnika.png "registrirani uporabnik -> objava urnika")

### 10. Registrirani uporabnik -> brisanje uporabniškega računa

Registrirani uporabnik lahko z desnim klikom na "Odjava" odpre stran za izbris svojega uporabniškega računa, kjer mora vtipkati svoje geslo in klikniti na gumb za potrditev.
Izjemni tokovi:

* V primeru napačnega gesla se izpiše opozorilo "Geslo ni pravilno!"

![alt text](../img/zaslonske_maske/student_brisanje_racuna.png "registrirani uporabnik -> brisanje računa")

Če je uporabnik sistemski skrbnik, potem mora samo vpisati uporabniško ime.
Izjemni tokovi:

* V primeru neobstoječega uporabniškega imena opozorilo "To uporabniško ime ne obstaja!"

![alt text](../img/zaslonske_maske/sys_admin_brisanje_racuna.png "sistemski skrbnik -> brisanje računa")

### 11. Uporabniški skrbnik / sistemski skrbnik -> urejanje javnih virov

Uporabniški skrbnik / Sistemski skrbnik lahko na seznamu urnikov izbirata tiste, ki jih želita urediti/overiti. Izbran urnik je odebeljen.

![alt text](../img/zaslonske_maske/sysadmin_urejanje_virov.png "uporabniški skrbnik / sistemski skrbnik -> urejanje javnih virov")

### 12. Sistemski skrbnik -> vzdrževanje podatkovne baze

Sistemski skrbnik lahko z desnim klikom na gumb "Nadzorna plošča" odpre stran za vzdrževanje podatkovne baze. Na levi strani lahko vidi seznam vseh javnih virov, ki še niso bili odobreni, na desni pa seznam vseh uporabnikov. Ob kliku na uporabnika se mu odpre podokno, kjer lahko vidi vse njegove .ical datoteke, ki čakajo na odobritev in njegov trenutni urnik.

![alt text](../img/zaslonske_maske/sysadmin_baza.png "sistemski skrbnik -> baza 1")

![alt text](../img/zaslonske_maske/sysadmin_baza2.png "sistemski skrbnik -> baza 2")

## 8. Vmesniki do zunanjih sistemov

Aplikacija bo kot zunanjo storitev uporabljala storitev reCAPTCHA podjetja Google. Ta storitev omogoča
verifikacijo resničnosti uporabnikov, ki se želijo registrirati za uporabo storitve.

Google reCAPTCHA ponuja preprost način za integracijo te storitve v spletno stran. Ko uporabnik reši
izziv, ki mu ga reCAPTCHA ponudi, se na spletni strežnik posreduje posebna vrednost, ki je specifična za
to interakcijo. Ker bi uporabnik z nekaj znanja na spletni strežnik naslovil POST zahtevo z naključno vrednostjo
moramo to vrednost potrditi z uporabo Google-ovega aplikacijskega programskega vmesnika.

Interakcija je v nadaljevanju opisana z uporabo OpenAPI specifikacije.

-----------------------

```yaml
openapi: 3.0.0
servers:
  - description: SwaggerHub API Auto Mocking
    url: https://virtserver.swaggerhub.com/Architecton/ReCAPTCHA/1.0.0
info:
  description: |
    Description of Google ReCAPTCHA results verification
  version: "1.0.0"
  title: ReCAPTCHA
tags:
  - name: Verification
    description: ReCAPTACHA value verification
paths:
  '/recaptcha/api/siteverify':
    post:
      tags:
        - Verification
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/VerificationRequest'
      responses:
        '200':
          description: Verification completed
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/VerificationResponse'
components:
  schemas:
    VerificationResponse:
      type: object
      properties:
        success:
          type: boolean
          description: whether this request was a valid reCAPTCHA token for your site
        score:
          type: number
          description: the score for this request (0.0 - 1.0)
        action:
          type: string
          description: the action name for this request (important to verify)
        challenge_ts:
          type: string
          description: timestamp of the challenge load
        hostname:
          type: string
          description: the hostname of the site where the reCAPTCHA was solved
        error-codes:
          type : array
          items: 
            type: string
          description: optional
    VerificationRequest:
      type: object
      properties:
        secret:
          type: string
          description: The shared key between your site and reCAPTCHA.
        response:
          type: string
          description: The user response token provided by the reCAPTCHA client-side integration on your site.
        remoteip:
          type: string
          description: Optional. The user's IP address.
```

-----------------------

![ReCAPTCHA OAS](../img/captchaOAS.png)

Kot zunanjo storitev bo aplikacija uporabljala tudi podatke o urnikih oziroma koledarjih kodirane po standardu iCalendar. Mnogo urnikov, ki so objavljeni na spletu namreč ponujajo
možnost izvoza le tega v takšnem formatu.

Uporabnik bo datoteko ki predstavlja urnik oziroma koledar v tem formatu lahko uvozil v aplikacijo na t.i. način povleči in spusti (angl. drag and drop).