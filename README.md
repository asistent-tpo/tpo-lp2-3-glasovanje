# Seznam vseh oddanih specifikacij projektov (2. LP in 3. LP)

Na podlagi pregleda spodnjih oddaj glasujte za najboljšo specifikacijo projekta.

* **01. skupina** ([zajem zahtev](./01/zajem-zahtev), [načrt](./01/nacrt))
* **02. skupina** ([zajem zahtev](./02/zajem-zahtev), [načrt](./02/nacrt))
* **03. skupina** ([zajem zahtev](./03/zajem-zahtev), [načrt](./03/nacrt))
* **04. skupina** ([zajem zahtev](./04/zajem-zahtev), [načrt](./04/nacrt))
* **05. skupina** ([zajem zahtev](./05/zajem-zahtev), [načrt](./05/nacrt))
* **06. skupina** ([zajem zahtev](./06/zajem-zahtev), [načrt](./06/nacrt))
* **07. skupina** ([zajem zahtev](./07/zajem-zahtev), [načrt](./07/nacrt))
* **08. skupina** ([zajem zahtev](./08/zajem-zahtev), [načrt](./08/nacrt))
* **09. skupina** ([zajem zahtev](./09/zajem-zahtev), [načrt](./09/nacrt))
* **10. skupina** ([zajem zahtev](./10/zajem-zahtev), [načrt](./10/nacrt))
* **11. skupina** ([zajem zahtev](./11/zajem-zahtev), [načrt](./11/nacrt))
* **12. skupina** ([zajem zahtev](./12/zajem-zahtev), [načrt](./12/nacrt))
* **13. skupina** ([zajem zahtev](./13/zajem-zahtev), [načrt](./13/nacrt))
* **14. skupina** ([zajem zahtev](./14/zajem-zahtev), [načrt](./14/nacrt))
* **15. skupina** ([zajem zahtev](./15/zajem-zahtev), [načrt](./15/nacrt))
* **16. skupina** ([zajem zahtev](./16/zajem-zahtev), [načrt](./16/nacrt))
* **17. skupina** ([zajem zahtev](./17/zajem-zahtev), [načrt](./17/nacrt))
* **18. skupina** ([zajem zahtev](./18/zajem-zahtev), [načrt](./18/nacrt))
* **19. skupina** ([zajem zahtev](./19/zajem-zahtev), [načrt](./19/nacrt))
* **20. skupina** ([zajem zahtev](./20/zajem-zahtev), [načrt](./20/nacrt))
* **21. skupina** ([zajem zahtev](./21/zajem-zahtev), [načrt](./21/nacrt))
* **22. skupina** ([zajem zahtev](./22/zajem-zahtev), [načrt](./22/nacrt))
* **23. skupina** ([zajem zahtev](./23/zajem-zahtev), [načrt](./23/nacrt))
* **24. skupina** ([zajem zahtev](./24/zajem-zahtev), [načrt](./24/nacrt))
* **25. skupina** ([zajem zahtev](./25/zajem-zahtev), [načrt](./25/nacrt))
* **26. skupina** ([zajem zahtev](./26/zajem-zahtev), [načrt](./26/nacrt))
* **27. skupina** ([zajem zahtev](./27/zajem-zahtev), [načrt](./27/nacrt))
* **28. skupina** ([zajem zahtev](./28/zajem-zahtev), [načrt](./28/nacrt))
* **29. skupina** ([zajem zahtev](./29/zajem-zahtev), [načrt](./29/nacrt))
