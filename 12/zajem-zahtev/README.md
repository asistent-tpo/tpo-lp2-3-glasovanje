# Dokument zahtev

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** |1. Matic Anžič, 2. Vanesa Gradišar, 3. Živa Škof in 4. Miha Zadravec |
| **Kraj in datum** | Ljubljana, 6. 4. 2019 |

## Povzetek projekta

Naša rešitev bo predstavljena v obliki spletne aplikacije, za katero smo zasnovali funkcionalnosti, kjer pripada vsaka določenemu akterju. Ustvarili smo 5 uporabniških vlog: neregistriran uporabnik, študent, profesor, administrator fakultete in glavni administrator. S slovarjem smo pojasnili pojme, ki lahko bralcu dokumenta predstavljajo nejasnosti. Z diagramom primera uporabe smo prikazali povezave posameznih akterjev in pri tem označili prioritete posameznih funkcionalnosti z must, should, could ali won't have. V funkcionalnih zahtevah so z osnovnimi in alternativnimi tokovi opisani poteki uporabe, izjemni deli/napake, pogoji in posledice. Razčlenili smo tudi nefunkcionalne zahteve, ki se delijo na zahteve izdelka, organizacijske in zunanje zahteve. Predstavili smo tudi prototipe vmesnikov, kjer smo z zaslonskimi maskami predstavili našo zamisel in opisali sistemske ter zunanje vmesnike. Zunanji vmesnik v naši aplikaciji predstavlja Google Calendar API.

## 1. Uvod

Problemska domena naše aplikacije je **enoten sistem za študijske obveznosti**. Ker zaenkrat ne obstaja v najboljši različici sebe, bi radi to popravili. 
Za dostop do vseh funkcionalnosti bosta potrebni *registracija* ter *prijava* v sistem.
Na enem mestu bo združeno naslednje:

* *pregled aktivnosti na koledarju* aplikacije z možnostjo *sinhronizacije z uporabnikovim Google koledarjem*, 
* *pregled, vnos in urejanje dnevnih aktivnosti* in obveznosti na *seznamu TO-DO*, možno pa bo tudi *naročanje na obveščanje o aktivnostih*,
* naročanje študentov na *kanale*, ki so jih ustvarili profesorji (kjer bodo npr. dostopne vse informacije in pomembni datumi v zvezi z določenim predmetom),
* študenti bodo lahko *dodajali svoje prijatelje*, s katerimi si bodo lahko vzajemno dodajali aktivnosti na koledar.

Da pa ne bi mogel kar kdorkoli urejati pomembnih zadev in zahtev, sta zato vključeni vlogi administratorja fakultete in glavnega administratorja, ki lahko:

* pregledata in odobrita *zahteve za višje vloge*,
* vključno z vlogo profesorja *brišeta uporabnike*.

Samo glavni administrator pa lahko *dodaja zapise v šifrante in jih ureja*.

Uporabnik se bo lahko tudi *odjavil* iz sistema, ko ga ne bo več uporabljal.



## 2. Uporabniške vloge
* **Neregistriran uporabnik**: Vsaka oseba, ki prvič dostopa do naše aplikacije, je neregistriran uporabnik. Na voljo ima le en pogled, in sicer prijavo v aplikacijo, kjer lahko klikne na gumb *Registracija* in se registrira. Po opravljeni registraciji (in morebitni zahtevi po višji vlogi) postane en izmed štirih možnih vlog uporabnika.

* **Študent**: Vsaka oseba, ki se registrira v našo aplikacijo, se ji dodeli vloga študenta (pri nas je to osnovni uporabnik). Imel bi osnovne funkcionalnosti: dodajati dogodke v koledar, delati zapiske, opomnike, ...). Vsak študent pa bo preko prošenj za prijateljstvo lahko dodal tudi prijatelje, z namenom, da bi določene aktivnosti, ki jih ustvari, lahko dodal tudi na njihove koledarje.

* **Profesor**: Vloga je eno stopnjo višje od osnovne. Do nje imajo dostop registrirani uporabniki, katerim je administrator njihove fakultete ugodil prošnjo za odobritev vloge. Profesor lahko ustvarja kanale (npr. kanal je lahko predmet), znotraj katerih bo nato ustvarjal aktivnosti: datume oddaj nalog, datume kolokvijev in izpitov, ... Študenti bi se na te kanale lahko izbirno prijavljali, prednost tega pa bo ta, da avtomatsko v svoj koledar prejmejo vse aktivnosti določenega kanala.

* **Administrator fakultete**: Administrator fakultete bo lahko sprejel in pregledoval prošnje za dodeljevanje vlog profesorjem, hkrati pa bo lahko tudi sam uporabljal funkcionalnosti vloge profesorja. Fakulteta administratorja bi bila vezana na šifrant: seznam fakultet, za katerega bi skrbel glavni administrator.

* **Glavni administrator**: Skrbi, da v aplikaciji stvari potekajo tako, kot morajo. Ima možnost urejanja in dodajanja raznih šifrantov, ki so lahko tipi dogodkov, pomembnost dogodkov, seznam fakultet, ... Lahko pa bo tudi pregledal in sprejel prošnje za dodeljevanje vloge administratorjem fakultet in jih dodal v sistem.


## 3. Slovar pojmov

- **uporabnik** -> Skupno ime (nadpomenka) za neregistriranega in registriranega (študent, profesor in administrator fakultete) uporabnika.
- **uporabniško ime** -> Uporabniško ime je predstavljeno z e-pošto uporabnika, ki jo je vnesel ob registraciji.
- **aplikacija** -> Spletna aplikacija StraightAs.
- **aktivnost** -> Skupno ime za dogodke in zapiske, ki jih lahko dodajamo v koledar. Primer aktivnosti so lahko različni dogodki, opomniki, datumi kolokvijev/izpitov, idr.
- **kanal** -> Medij za povezovanje aktivnosti, ki jih ustvarijo profesorji ali administratorji fakultete, z drugimi uporabniki.
- **šifrant** -> Predstavlja različne sezname, ki se pojavijo znotraj same aplikacije, to so: seznami fakultet, seznami aktivnosti, seznam tipov dogodkov.
- **prijatelj** -> [Prijatelja](https://www.youtube.com/watch?v=sbBEm7emSao) postaneta študenta, ki preko prošnje za prijateljstvo skleneta sodelovanje znotraj aplikacije.

## 4. Diagram primerov uporabe

![UseCaseDiagram](../img/UseCaseDiagram.png)

**Legenda**: 

- Must have zahteve: turkizna
- Should have zahteve: vijolična
- Could have: rumena
- Won't have this time: rdeča

## 5. Funkcionalne zahteve

### **5.1 Registracija v sistem**

### Povzetek funkcionalnosti 
Neregistrirani uporabnik se lahko registrira v našo aplikacijo in s tem pridobi vlogo študenta, profesorja ali administratorja fakultete. Za vlogo profesorja in administratorja fakultete mora zaprositi pri registraciji in nato počakati na odobritev. 

### Akterji 
Neregistrirani uporabnik. 

### Osnovni tok
**Neregistrirani uporabnik se registrira za vlogo ŠTUDENT:** 

1. Neregistrirani uporabnik izbere funkcionalnost *Registracija*. 
2. Sistem prikaže obrazec za registracijo. 
3. Neregistrirani uporabnik izpolni polja za ime, priimek, e-pošto, geslo in ponovno geslo. 
4. Neregistrirani uporabnik potrdi registracijo. 



### Alternativni tok(ovi)
**ALTERNATIVNI TOK 1**

**Neregistrirani uporabnik se registrira za vlogo PROFESOR:** 

1. Neregistrirani uporabnik izbere funkcionalnost *Registracija*. 
2. Sistem prikaže obrazec za registracijo. 
3. Neregistrirani uporabnik izpolni polja za ime, priimek, e-pošto, geslo in ponovno geslo. 
4. Neregistrirani uporabnik izbere opcijo *zahtevaj vlogo profesorja*. 
5. Neregistrirani uporabnik iz spustnega meni-ja izbere svojo fakulteto. 
6. Neregistrirani uporabnik potrdi registracijo. 
7. Neregistrirani uporabnik počaka, da adminstrator izbrane fakultete odobri izbrano vlogo. 

**ALTERNATIVNI TOK 2**

**Neregistrirani uporabnik se registrira za vlogo ADMINISTRATOR FAKULTETE:** 

1. Neregistrirani uporabnik izbere funkcionalnost *Registracija*. 
2. Sistem prikaže obrazec za registracijo. 
3. Neregistrirani uporabnik izpolni polja za ime, priimek, e-pošto, geslo in ponovno geslo. 
4. Neregistrirani uporabnik izbere opcijo *zahtevaj vlogo administratorja fakultete*. 
5. Neregistrirani uporabnik vpiše v tekstovno polje ime fakultete, za katero zaprosi. 
6. Neregistrirani uporabnik potrdi registracijo. 
7. Neregistrirani uporabnik počaka, da glavni administrator odobri izbrano vlogo.  

### Izjemni del/napake
Neregistrirani uporabnik - IZJEMNI TOK 1:

1. Neregistrirani uporabnik izbere funkcionalnost *Registracija*. 
2. Sistem prikaže obrazec za registracijo. 
3. Neregistrirani uporabnik ne izpolni vseh zahtevanih podatkov. 
4. Neregistrirani uporabnik potrdi registracijo. 
5. Sistem izpiše obvestilo, da registracija ni uspešna zaradi manjkajočih podatkov.

Neregistrirani uporabnik - IZJEMNI TOK 2: 

1. Neregistrirani uporabnik izbere funkcionalnost *Registracija*. 
2. Sistem prikaže obrazec za registracijo. 
3. Neregistrirani uporabnik vnese prekratko geslo. 
4. Neregistrirani uporabnik potrdi registracijo. 
5. Sistem izpiše obvestilo, da registracija ni uspešna zaradi manjkajočih podatkov.

Neregistrirani uporabnik - IZJEMNI TOK 3: 

1. Neregistrirani uporabnik izbere funkcionalnost *Registracija*. 
2. Sistem prikaže obrazec za registracijo. 
3. Neregistrirani uporabnik ne vnese enakega gesla pod vnosnima poljema. 
4. Neregistrirani uporabnik potrdi registracijo. 
5. Sistem izpiše obvestilo, da registracija ni uspešna zaradi neujemanja gesel.


### Pogoji
* Neregistrirani uporabnik še ne sme biti registriran.
* Administrator določene fakultete mora biti registriran pred profesorjem te fakultete, da se profesor lahko registrira.

### Posledice
Neregistrirani uporabnik je uspešno registriran v vlogi študenta, profesorja ali administratorja fakultete. Njegovi podatki so shranjeni v našo bazo, on pa lahko dostopa do naše aplikacije.  

### Posebnosti
Funkcionalnost nima posebnosti. 

### Prioritete identificiranih funkcionalnosti
Must have.

**SPREJEMNI TESTI**

|**Primer uporabe**            | **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod**   | **Pričakovani rezultat** |
|:-----------------            |:-------------------------- |:-------------------------- |:---------- | :----------------------- |
| Registracija kot študent | Registracija | Neregistrirani uporabnik | Ime, priimek, epošta, geslo in ponovno geslo | Uspešno registriran študent |
| Registracija kot študent | Registracija | Neregistrirani uporabnik | Nepopoln vhod (manjka ime, priimek, e-pošta, geslo ali ponovno geslo) | Obvestilo sistema o neizpolnjenih vnosnih poljih |
| Registracija kot študent | Registracija | Neregistrirani uporabnik | Geslo in ponovno geslo se ne ujemata | Obvestilo sistema o neujemanju gesel |
| Registracija kot profesor | Registracija, Zahteva za višjo vlogo | Neregistrirani uporabnik | Ime, priimek, e-pošta, izbrana fakulteta, geslo, ponovno geslo in odkljukana izbira zahteve za vlogo profesorja | Uspešno registriran profesor, administrator fakultete prejme zahtevo za višjo vlogo |
| Registracija kot administrator fakultete | Registracija, Zahteva za višjo vlogo | Neregistrirani uporabnik | Ime, priimek, e-pošta, vpisana fakulteta, geslo, ponovno geslo in odkljukana izbira zahteve za vlogo administratorja fakultete | Uspešno registriran administrator fakultete, glavni administrator prejme zahtevo za višjo vlogo |

___

### **5.2 Prijava v sistem**

### Povzetek funkcionalnosti
Študent, profesor ali administrator fakultete se lahko prijavi v našo aplikacijo.

### Akterji 
Študent, profesor, administrator fakultete, glavni administrator.

### Osnovni tok
ŠTUDENT: 

1. Študent izbere funkcionalnost *Prijava*.
2. Sistem prikaže obrazec za prijavo
3. Študent izpolni polja za uporabniško ime in geslo.
4. Študent potrdi prijavo.
 
PROFESOR: 

1. Profesor izbere funkcionalnost *Prijava*.
2. Sistem prikaže obrazec za prijavo.
3. Profesor izpolni polja za uporabniško ime in geslo.
4. Profesor potrdi prijavo.

ADMINISTRATOR FAKULTETE:

1. Administrator fakultete izbere funkcionalnost *Prijava*.
2. Sistem prikaže obrazec za prijavo.
3. Administrator fakultete izpolni polja za uporabniško ime in geslo.
4. Administrator fakultete potrdi prijavo.

GLAVNI ADMINISTRATOR:

1. Glavni administrator izbere funkcionalnost *Prijava*.
2. Sistem prikaže obrazec za prijavo.
3. Glavni administrator izpolni polja za uporabniško ime in geslo.
4. Glavni administrator potrdi prijavo.

### Alternativni tok(ovi)
Funkcionalnost nima alternativnega toka.

### Izjemni del/napake
ŠTUDENT - IZJEMNI TOK 1:

1. Študent izbere funkcionalnost *Prijava*.
2. Sistem prikaže obrazec za prijavo.
3. Študent ne izpolni vseh zahtevanih podatkov.
4. Študent potrdi prijavo.
5. Sistem izpiše opozorilo, da prijava ni uspešna zaradi manjkajočih podatkov.

ŠTUDENT - IZJEMNI TOK 2:

1. Študent izbere funkcionalnost *Prijava*.
2. Sistem prikaže obrazec za prijavo.
3. Študent vnese napačno uporabniško ime.
4. Študent potrdi prijavo.
5. Sistem izpiše opozorilo, da prijava ni uspešna zaradi napačnega uporabniškega imena.

ŠTUDENT - IZJEMNI TOK 3:

1. Študent izbere funkcionalnost *Prijava*.
2. Sistem prikaže obrazec za prijavo.
3. Študent vnese napačno geslo.
4. Študent potrdi prijavo.
5. Sistem izpiše opozorilo, da prijava ni uspešna zaradi napačnega gesla.

PROFESOR - IZJEMNI TOK 1:

1. Profesor izbere funkcionalnost *Prijava*.
2. Sistem prikaže obrazec za prijavo.
3. Profesor ne izpolni vseh zahtevanih podatkov.
4. Profesor potrdi prijavo.
5. Sistem izpiše opozorilo, da prijava ni uspešna zaradi manjkajočih podatkov.

PROFESOR - IZJEMNI TOK 2:

1. Profesor izbere funkcionalnost *Prijava*.
2. Sistem prikaže obrazec za prijavo.
3. Profesor vnese napačno uporabniško ime.
4. Profesor potrdi prijavo.
5. Sistem izpiše obvestilo, da prijava ni uspešna zaradi napačnega uporabniškega imena.

PROFESOR - IZJEMNI TOK 3: 

1. Profesor izbere funkcionalnost *Prijava*.
2. Sistem prikaže obrazec za prijavo.
3. Profesor vnese napačno geslo.
4. Profesor potrdi prijavo.
5. Sistem izpiše obvestilo, da prijava ni uspešna zaradi napačnega gesla.

ADMINISTRATOR FAKULTETE - IZJEMNI TOK 1:

1. Administrator fakultete izbere funkcionalnost *Prijava*.
2. Sistem prikaže obrazec za prijavo.
3. Administrator fakultete ne izpolni vseh zahtevanih podatkov.  
4. Administrator fakultete potrdi prijavo.
5. Sistem izpiše obvestilo, da prijava ni uspešna zaradi manjkajočih podatkov.

ADMINISTRATOR FAKULTETE - IZJEMNI TOK 2: 

1. Administrator fakultete izbere funkcionalnost *Prijava*.
2. Sistem prikaže obrazec za prijavo.
3. Administrator fakultete vnese napačno uporabniško ime.
4. Administrator fakultete potrdi prijavo.
5. Sistem izpiše obvestilo, da prijava ni uspešna zaradi napačnega uporabniškega imena.

ADMINISTRATOR FAKULTETE - IZJEMNI TOK 3: 

1. Administrator fakultete izbere funkcionalnost *Prijava*.
2. Sistem prikaže obrazec za prijavo.
3. Administrator fakultete vnese napačno geslo.
4. Administrator fakultete potrdi prijavo.
5. Sistem izpiše obvestilo, da prijava ni uspešna zaradi napačnega gesla.

GLAVNI ADMINISTRATOR - IZJEMNI TOK 1:

1. Glavni administrator izbere funkcionalnost *Prijava*.
2. Sistem prikaže obrazec za prijavo.
3. Glavni administrator ne izpolni vseh zahtevanih podatkov.
4. Glavni administrator potrdi prijavo.
5. Sistem izpiše obvestilo, da prijava ni uspešna zaradi manjkajočih podatkov.

GLAVNI ADMINISTRATOR - IZJEMNI TOK 2:

1. Glavni administrator izbere funkcionalnost *Prijava*.
2. Sistem prikaže obrazec za prijavo.
3. Glavni administrator vnese napačno uporabniško ime.
4. Glavni administrator potrdi prijavo.
5. Sistem izpiše obvestilo, da prijava ni uspešna zaradi napačnega uporabniškega imena.

GLAVNI ADMINISTRATOR - IZJEMNI TOK 3:

1. Glavni administrator izbere funkcionalnost *Prijava*.
2. Sistem prikaže obrazec za prijavo.
3. Glavni administrator vnese napačno geslo.
4. Glavni administrator potrdi prijavo.
5. Sistem izpiše obvestilo, da prijava ni uspešna zaradi napačnega gesla.

### Pogoji
Študent, profesor ali administrator fakultete mora biti pred prvo prijavo uspešno registriran.

### Posledice
Študent, profesor, administrator fakultete ali glavni administrator so uspešno prijavljeni, omogočen jim je dostop do naše aplikacije.

### Posebnosti
Funkcionalnost nima posebnosti.

### Prioritete identificiranih funkcionalnosti
Must have.

**SPREJEMNI TESTI**

|**Primer uporabe**            | **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod**   | **Pričakovani rezultat** |
|:-----------------            |:-------------------------- |:-------------------------- |:---------- | :----------------------- |
| Prijava kot uporabnik | Prijava | Registriran uporabnik | Uporabniško ime in geslo | Uspešno prijavljen uporabnik |
| Prijava kot uporabnik | Prijava | Neprijavljen neregistriran uporabnik | Uporabniško ime in geslo | Neuspešna prijava, sporočilo sistema, da uporabniško ime ne obstaja.  |
| Prijava kot uporabnik | Prijava | Neprijavljen registriran uporabnik | Vnešeno napačno geslo | Neuspešna prijava, sporočilo sistema, da je vnešeno geslo napačno.  |
| Prijava kot glavni administrator | Prijava kot glavni administrator (posebnost, saj je kreiran in se ne registrira sam) | Neprijavljen kreiran uporabnik | Uporabniško ime in  geslo | Uspešno prijavljen kot administrator |

___
### **5.3 Odjava iz sistema**

### Povzetek funkcionalnosti
Študent, profesor, administrator fakultete ali glavni administrator se lahko odjavi iz naše aplikacije.

### Akterji
Študent, profesor, administrator fakultete in glavni administrator.

### Osnovni tok
ŠTUDENT: 

1. Študent izbere funkcionalnost *Odjava*.
2. Študent potrdi odjavo. 

PROFESOR: 

1. Profesor izbere funkcionalnost *Odjava*.
2. Profesor potrdi odjavo. 

ADMINISTRATOR FAKULTETE: 

1. Administrator fakultete izbere funkcionalnost *Odjava*.
2. Administrator fakultete potrdi odjavo. 

GLAVNI ADMINISTRATOR: 

1. Glavni administrator izbere funkcionalnost *Odjava*.
2. Glavni administrator potrdi odjavo.

### Alternativni tok(ovi)
Funkcionalnost nima alternativnega toka. 

### Izjemni del/napake
Funkcionalnost nima izjemnega dela/napak.

### Pogoji
Študent, profesor, administrator fakultete ali glavni administrator mora biti prijavljen v našo aplikacijo.

### Posledice
Študent, profesor, administrator fakultete ali glavni administrator je uspešno odjavljen. Njegova seja je zaključena.

### Posebnosti
Funkcionalnost nima posebnosti.

### Prioritete identificiranih funkcionalnosti
Must have.

**SPREJEMNI TESTI**

|**Primer uporabe**            | **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod**   | **Pričakovani rezultat** |
|:-----------------            |:-------------------------- |:-------------------------- |:---------- | :----------------------- |
| Odjava uporabnika | Odjava | Prijavljen uporabnik | brez | Uspešno odjavljen uporabnik |
| Odjava uporabnika | Zapiranje spletnega brskalnika | Prijavljen uporabnik | brez | Uporabnik ostane prijavljen. |

___

### **5.4 Pregled koledarja**

### Povzetek funkcionalnosti
Študent, profesor ali administrator fakultete lahko pregleduje koledar. Za posamezen dan vidi vpisane aktivnosti, praznike, opomnike in dogodke.

### Akterji
Študent, profesor, administrator fakultete.

### Osnovni tok
ŠTUDENT: 

1. Študent odpre domačo stran aplikacije.
2. Študentu je omogočeno pregledovanje koledarja po mesecih.
3. Študent izbere določen dan.
4. Študent ima možnost vpogleda v izbrani dan.
 
PROFESOR: 

1. Profesor odpre domačo stran aplikacije.
2. Profesorju je omogočeno pregledovanje koledarja po mesecih.  
3. Profesor izbere določen dan.
4. Profesor ima možnost vpogleda v izbrani dan.

ADMINISTRATOR FAKULTETE: 

1. Administrator fakultete odpre domačo stran aplikacije.
2. Administratorju fakultete je omogočeno pregledovanje koledarja po mesecih.
3. Administrator fakultete izbere določen dan.
4. Administrator fakultete ima možnost vpogleda v izbrani dan.

### Alternativni tok(ovi)
Funkcionalnost nima alternativnega toka.

### Izjemni del/napake
Funkcionalnost nima izjemnega dela/napak.

### Pogoji
Študent, profesor ali administrator fakultete mora biti prijavljen v našo aplikacijo.

### Posledice
Študent, profesor ali administrator fakultete ima pregled nad lastnim koledarjem in se lahko po njem premika.

### Posebnosti
Funkcionalnost nima posebnosti.

### Prioritete identificiranih funkcionalnosti
Must have.


**SPREJEMNI TESTI**

|**Primer uporabe**            | **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod**   | **Pričakovani rezultat** |
|:-----------------            |:-------------------------- |:-------------------------- |:---------- | :----------------------- |
| Pregled koledarja v vlogi študenta, profesorja ali administratorja fakultete | Možnost sprehajanja po koledarju, torej, da lahko prijavljen uporabnik pregleduje pretekle in prihajajoče mesece. | Pogled današnjega meseca | Ni pričakovanega vhoda | Možnost pregleda mesecev v koledarju |
| Pregled koledarja v vlogi študenta, profesorja ali administratorja fakultete | Možnost vpogleda določenega dne | Pogled današnjega meseca | Izbran dan v koledarju | Možnost pregleda posameznega dneva v koledarju |

___

### **5.5 TO-DO list**

### Povzetek funkcionalnosti
Študent, profesor ali administrator fakultete lahko za tekoči dan pregleda, katere aktivnosti ima zabeležene, opravljene pa lahko odkljuka.

### Akterji
Študent, profesor, administrator fakultete.

### Osnovni tok
ŠTUDENT: 

1. Študent odpre domačo stran aplikacije. 
2. Sistem prikaže seznam aktivnosti današnjega dne. 
3. Študent klikne na kljukico ob aktivnosti, ki jo je danes že opravil. 
4. Izbrana aktivnost je označena kot opravljena.

PROFESOR: 

1. Profesor odpre domačo stran aplikacije. 
2. Sistem prikaže seznam aktivnosti današnjega dne. 
3. Profesor klikne na kljukico ob aktivnosti, ki jo je danes že opravil.
4. Izbrana aktivnost je označena kot opravljena.

ADMINISTRATOR FAKULTETE: 

1. Administrator fakultete odpre domačo stran aplikacije. 
2. Sistem prikaže seznam aktivnosti današnjega dne. 
3. Administrator fakultete klikne na kljukico ob aktivnosti, ki jo je danes že opravil. 
4. Izbrana aktivnost je označena kot opravljena.

### Alternativni tok(ovi)
Funkcionalnost nima alternativnega toka.

### Izjemni del/napake
Funkcionalnost nima izjemnega dela/napak.

### Pogoji
* Študent, profesor ali administrator fakultete mora biti prijavljen v našo aplikacijo.
* Študent, profesor ali administrator fakultete mora imeti za prikaz aktivnosti le-te vnešene v koledarju.

### Posledice
Študent, profesor ali administrator fakultete ima boljši pregled in organizacijo današnjega dne.

### Posebnosti
V primeru, da uporabnik želi dodati novo opravilo za današnji dan na TO-DO list, ga je potrebno vnesti preko koledarja.

### Prioritete identificiranih funkcionalnosti
Should have.

**SPREJEMNI TESTI**

|**Primer uporabe**            | **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod**   | **Pričakovani rezultat** |
|:-----------------            |:-------------------------- |:-------------------------- |:---------- | :----------------------- |
| Pregled aktivnosti trenutnega dne | Prikaz aktivnosti trenutnega dne | Seznam aktivnosti | Ni vhoda | Sistem uspešno prikaže aktivnosti trenutnega dne. |
| Pregled aktivnosti trenutnega dne | Odkljukanje že opravljene aktivnosti | Seznam aktivnosti | Izbrana aktivnost, ki jo želimo odkljukati | Na seznamu aktivnosti se odkljukana aktivnost ustrezno označi. |

___

### **5.6 Vnos aktivnosti**

### Povzetek funkcionalnosti
Študent, profesor ali administrator fakultete lahko v koledar vnese razne aktivnosti.

### Akterji
Študent, profesor, administrator fakultete.

### Osnovni tok
ŠTUDENT: 

1. Študent izbere določen dan v koledarju. 
2. Študent klikne na gumb *dodaj aktivnost*.
3. Študent si izbere tip aktivnosti.
4. Študent glede na izbrani tip aktivnosti izpolni še ostala polja, ki opisujejo izbrani tip aktivnosti.
5. Študent potrdi vnos aktivnosti.

PROFESOR: 

1. Profesor izbere določen dan v koledarju.
2. Profesor klikne na gumb *dodaj aktivnost*.
3. Profesor si izbere tip aktivnosti.
4. Profesor glede na izbrani tip aktivnosti izpolni še ostala polja, ki opisujejo izbrani tip aktivnosti.
5. Profesor lahko izbere kanal, v katerega želi vnesti izbrano aktivnost.
6. Profesor potrdi vnos aktivnosti.

ADMINISTRATOR FAKULTETE:

1. Administrator fakultete izbere določen dan v koledarju.
2. Administrator fakultete klikne na gumb *dodaj aktivnost*.
3. Administrator fakultete si izbere tip aktivnosti.
4. Administrator fakultete glede na izbrani tip aktivnosti izpolni še ostala polja, ki opisujejo izbrani tip aktivnosti.
5. Administrator fakultete lahko izbere kanal, v katerega želi vnesti izbrano aktivnost.
6. Administrator fakultete potrdi vnos aktivnosti.

### Alternativni tok(ovi)
ŠTUDENT - ALTERNATIVNI TOK 1:

1. Študent s klikom na gumb *dodaj aktivnost* izbere funkcionalnost *Vnos aktivnosti v koledar*.
2. Študent si izbere tip aktivnosti.
3. Študent izpolni polje za vnos datuma.
4. Študent glede na izbrani tip aktivnosti izpolni še ostala polja, ki opisujejo izbrani tip aktivnosti.
5. Študent potrdi vnos aktivnosti.

PROFESOR - ALTERNATIVNI TOK 1:

1. Profesor s klikom na gumb *dodaj aktivnost* izbere funkcionalnost *Vnos aktivnosti v koledar*.
2. Profesor si izbere tip aktivnosti.
3. Profesor izpolni polje za vnos datuma.
4. Profesor glede na izbrani tip aktivnosti izpolni še ostala polja, ki opisujejo izbrani tip aktivnosti.
5. Profesor lahko izbere kanal, v katerega želi vnesti izbrano aktivnost.
6. Profesor potrdi vnos aktivnosti.

ADMINISTRATOR FAKULTETE - ALTERNATIVNI TOK 1:

1. Administrator fakultete s klikom na gumb *dodaj aktivnost* izbere funkcionalnost *Vnos aktivnosti v koledar*.
2. Administrator fakultete si izbere tip aktivnosti.
3. Administrator fakultete izpolni polje za vnos datuma.
4. Administrator fakultete glede na izbrani tip aktivnosti izpolni še ostala polja, ki opisujejo izbrani tip aktivnosti aktivnosti.
5. Administrator fakultete lahko izbere kanal, v katerega želi vnesti izbrano aktivnost.
6. Administrator fakultete potrdi vnos aktivnosti.

### Izjemni del/napake
ŠTUDENT - IZJEMNI TOK 1: 

1. Študent izbere funkcionalnost *Vnos aktivnosti v koledar*.
2. Sistem prikaže obrazec za vnos aktivnosti.
3. Študent ne izpolni vseh zahtevanih podatkov.
4. Študent potrdi vnos aktivnosti.
5. Sistem izpiše obvestilo, da vnos aktivnosti ni uspešen zaradi manjkajočih podatkov.

ŠTUDENT - IZJEMNI TOK 2: 

1. Študent izbere funkcionalnost *Vnos aktivnosti v koledar*.
2. Sistem prikaže obrazec za vnos aktivnosti.
3. Študent ne vnese tipa aktivnosti.
4. Študent potrdi vnos aktivnosti.
5. Sistem izpiše obvestilo, da vnos aktivnosti ni uspešen zaradi manjkajočih podatkov.


PROFESOR - IZJEMNI TOK 1:

1. Profesor izbere funkcionalnost *Vnos aktivnosti v koledar*.
2. Sistem prikaže obrazec za vnos aktivnosti.
3. Profesor ne izpolni vseh zahtevanih podatkov.
4. Profesor potrdi vnos aktivnosti.
5. Sistem izpiše obvestilo, da vnos aktivnosti ni uspešen zaradi manjkajočih podatkov.

PROFESOR - IZJEMNI TOK 2:

1. Profesor izbere funkcionalnost *Vnos aktivnosti v koledar*.
2. Sistem prikaže obrazec za vnos aktivnosti.
3. Profesor ne vnese tipa aktivnosti.
4. Profesor potrdi vnos aktivnosti.
5. Sistem izpiše obvestilo, da vnos aktivnosti ni uspešen zaradi manjkajočih podatkov.


ADMINISTRATOR FAKULTETE - IZJEMNI TOK 1:

1. Administrator fakultete izbere funkcionalnost *Vnos aktivnosti v koledar*.
2. Sistem prikaže obrazec za vnos aktivnosti.
3. Administrator fakultete ne izpolni vseh zahtevanih podatkov.
4. Administrator fakultete potrdi vnos aktivnosti.
5. Sistem izpiše obvestilo, da vnos aktivnosti ni uspešen zaradi manjkajočih podatkov.

ADMINISTRATOR FAKULTETE - IZJEMNI TOK 2:

1. Administrator fakultete izbere funkcionalnost *Vnos aktivnosti v koledar*.
2. Sistem prikaže obrazec za vnos aktivnosti.
3. Administrator fakultete ne vnese tipa aktivnosti.
4. Administrator fakultete potrdi vnos aktivnosti.
5. Sistem izpiše obvestilo, da vnos aktivnosti ni uspešen zaradi manjkajočih podatkov.


### Pogoji
* Študent, profesor ali administrator fakultete mora biti prijavljen v našo aplikacijo.
* Študent, profesor ali administrator fakultete mora za ustvaritev zapiska izven koledarja izbrati ustrezno izbiro tipa aktivnosti.

### Posledice
* Študent, profesor ali administrator fakultete ima v koledarju zabeležene vnešene aktivnosti.
* Študent, profesor ali administrator fakultete ima poleg koledarja možnost ustvarjanja zapiskov.

### Posebnosti
Funkcionalnost nima posebnosti.

### Prioritete identificiranih funkcionalnosti
Must have.


**SPREJEMNI TESTI**

|**Primer uporabe**            | **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod**   | **Pričakovani rezultat** |
|:-----------------            |:-------------------------- |:-------------------------- |:---------- | :----------------------- |
| Izbira vnosa preko osnovnega toka | Samodejno vnešen datum. **in** Posodobitev aktivnosti v koledarju ali zapiskih. | Koledar/zapiski brez dodane aktivnosti | Izbira tipa aktivnosti, opis aktivnosti | Uspešno vnešena aktivnost v koledarju ali zapiskih. |
| Izbira vnosa preko alternativnega toka | Aktivnost je prikazana na pravilnem datumu. **in** Posodobitev aktivnosti v koledarju ali zapiskih. | Koledar/zapiski brez dodane aktivnosti | Izbira tipa aktivnosti, vnos datuma, opis aktivnosti | Uspešno vnešena aktivnost v koledarju ali zapiskih pod in s pravilnim datumom. |
| Uporabnik izbere tip aktivnosti | Zapis aktivnosti na pravilno izbrano mesto, torej ali v koledar ali v zapiske. | Koledar/zapiski brez dodane aktivnosti | Izbira tipa aktivnosti | Aktivnost je zapisana na pravilno mesto (v koledar ali v zapiske). |
| Profesor ali administrator fakultete vnese aktivnost na kanal | Dodajanje aktivnosti v koledarje vseh oseb, povezanih preko kanala. | Koledarji brez dodane aktivnosti | Izbira tipa aktivnosti, Izbira kanala | Aktivnost je zapisana v koledarje vseh oseb, pridruženih temu kanalu. |

___

### **5.7 Brisanje aktivnosti**

### Povzetek funkcionalnosti
Študent, profesor ali administrator fakultete lahko iz koledarja izbriše vnešene aktivnosti.

### Akterji
Študent, profesor, administrator fakultete.

### Osnovni tok
ŠTUDENT: 

1. Študent izbere dan v koledarju, za katerega želi aktivnost izbrisati. 
2. Študent klikne na gumb *izbriši aktivnost*, ki je ob izbrani aktivnosti. 
3. Študent potrdi svojo izbiro. 

PROFESOR: 

1. Profesor izbere dan v koledarju, za katerega želi aktivnost izbrisati. 
2. Profesor klikne na gumb *izbriši aktivnost*, ki je ob izbrani aktivnosti. 
3. Profesor potrdi svojo izbiro. 

ADMINISTRATOR FAKULTETE: 

1. Administrator fakultete izbere dan v koledarju, za katerega želi aktivnost izbrisati. 
2. Administrator fakultete klikne na gumb *izbriši aktivnost*, ki je ob izbrani aktivnosti. 
3. Administrator fakultete potrdi svojo izbiro. 

### Alternativni tok(ovi)
Funkcionalnost nima alternativnega toka.

### Izjemni del/napake
Funkcionalnost nima izjemnega dela/napak.

### Pogoji
* Študent, profesor ali administrator fakultete mora biti prijavljeni v našo aplikacijo.
* Študent, profesor ali administrator fakultete mora za izbris aktivnosti to aktivnost imeti vnešeno.

### Posledice
Študent, profesor ali administrator fakultete iz koledarja ali zapiskov izbriše izbrano aktivnost. 

### Posebnosti
Funkcionalnost nima posebnosti.

### Prioritete identificiranih funkcionalnosti
Must have.

**SPREJEMNI TESTI**

|**Primer uporabe**            | **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod**   | **Pričakovani rezultat** |
|:-----------------            |:-------------------------- |:-------------------------- |:---------- | :----------------------- |
| Izbris aktivnosti aktivnosti s strani študenta, profesorja ali administratorja fakultete | Brisanje aktivnosti iz lastnega koledarja/zapiskov. | Lastni koledar/zapiski vsebujejo izbrano aktivnost. | Izbrana aktivnost | Uspešno izbrisana aktivnost v lastnem koledarju/zapiskih |
| Izbris aktivnosti s strani profesorja ali administratorja fakultete | Brisanje aktivnosti, ki pripada nekemu kanalu | Lastni koledar/zapiski vsebuje(jo) izbrano aktivnost. **in**  Koledarji/zapiski vseh študentov, naročenih na ta kanal, vsebuje(jo) izbrano aktivnost. | Izbrana aktivnost | Uspešno izbrisana aktivnost v lastnem koledarju/zapiskih. **in** Uspešno izbrisana aktivnost v koledarjih/zapiskih študentov, naročenih na kanal. |
___

### 5.8 **Urejanje aktivnosti**

### Povzetek funkcionalnosti
Študent, profesor ali administrator fakultete lahko v koledarju uredi vnešene aktivnosti.

### Akterji
Študent, profesor, administrator fakultete.

### Osnovni tok
ŠTUDENT:

1. Študent izbere dan v koledarju, za katerega želi urejati aktivnost.
2. Študent klikne na gumb *uredi aktivnost*, ki se nahaja ob izbrani aktivnosti. 
3. Študent vnese spremembe.
4. Študent potrdi svojo izbiro.

PROFESOR:

1. Profesor izbere dan v koledarju, za katerega želi urejati aktivnost.
2. Profesor klikne na gumb *uredi aktivnost*, ki se nahaja ob izbrani aktivnosti.
3. Profesor vnese spremembe.
4. Profesor potrdi svojo izbiro.

ADMINISTRATOR FAKULTETE:

1. Administrator fakultete izbere dan v koledarju, za katerega želi urejati aktivnost.
2. Administrator fakultete klikne na gumb *uredi aktivnost*, ki se nahaja ob izbrani aktivnosti.
3. Administrator fakultete vnese spremembe.
4. Administrator fakultete potrdi svojo izbiro. 

### Alternativni tok(ovi)
ŠTUDENT - ALTERNATIVNI TOK 1:

1. Študent izbere dan v koledarju, za katerega želi urejati aktivnost.
2. Študent klikne na gumb *izbriši aktivnost*, ki se nahaja ob izbrani aktivnosti.
3. Študent ustvari novo aktivnost z željenimi podatki.
4. Študent potrdi svojo izbiro.

PROFESOR - ALTERNATIVNI TOK 1:

1. Profesor izbere dan v koledarju, za katerega želi urejati aktivnost.
2. Profesor klikne na gumb *izbriši aktivnost*, ki se nahaja ob izbrani aktivnosti.
3. Profesor ustvari novo aktivnost z željenimi podatki.
4. Profesor potrdi svojo izbiro.

ADMINISTRATOR FAKULTETE - ALTERNATIVNI TOK 1:

1. Administrator fakultete izbere dan v koledarju, za katerega želi urejati aktivnost.
2. Administrator fakultete klikne na gumb *izbriši aktivnost*, ki se nahaja ob izbrani aktivnosti.
3. Administrator fakultete ustvari novo aktivnost z željenimi podatki.
4. Administrator fakultete potrdi svojo izbiro.

### Izjemni del/napake
Funkcionalnost nima izjemnega dela/napak.

### Pogoji
* Študent, profesor ali administrator fakultete mora biti prijavljen v našo aplikacijo.
* Študent, profesor ali administrator fakultete mora za urejanje aktivnost to aktivnost imeti vnešeno.

### Posledice
Študent, profesor ali administrator fakultete v koledarju posodobijo že vnešeno aktivnost. V primeru osnovnega toka jim ni potrebno aktivnosti najprej izbrisati in je vnesti na novo.

### Posebnosti
Funkcionalnost nima posebnosti.

### Prioritete identificiranih funkcionalnosti
Should have.


**SPREJEMNI TESTI**

|**Primer uporabe**            | **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod**   | **Pričakovani rezultat** |
|:-----------------            |:-------------------------- |:-------------------------- |:---------- | :----------------------- |
| Urejanje aktivnosti | Posodabljanje aktivnosti v koledarju/zapiskih | Koledar/zapiski vsebujejo izbrano aktivnost. | Nov opis aktivnosti | Uspešno posodobljena aktivnost v koledarju/zapiskih. |
| Sprememba pripadnosti aktivnosti kanalu | Posodabljanje aktivnosti v koledarju/zapiskih vseh uporabnikov, naročenih na kanal | Koledar/zapiski vsebujejo izbrano aktivnost. | Nov kanal aktivnosti | Uspešno posodobljena aktivnost v koledarju/zapiskih vseh uporabnikov, naročenih na kanal. |

___

### **5.9 Prijava uporabnika na obveščanje o aktivnostih**

### Povzetek funkcionalnosti
Študent, profesor ali administrator fakultete se lahko naroči na prejemanje obvestil o aktivnostih.

### Akterji
Študent, profesor, administrator fakultete.

### Osnovni tok
ŠTUDENT: 

1. Študent izbere možnost *Obveščaj me o aktivnostih*. 
2. Sistem prikaže obvestilo, da je uporabnik uspešno naročen na obveščanje o njegovih aktivnostih. 

PROFESOR: 

1. Profesor izbere možnost *Obveščaj me o aktivnostih*. 
2. Sistem prikaže obvestilo, da je uporabnik uspešno naročen na obveščanje o aktivnostih.

ADMINISTRATOR FAKULTETE:  

1. Administrator fakultete izbere možnost *Obveščaj me o aktivnostih*. 
2. Sistem prikaže obvestilo, da je uporabnik uspešno naročen na obveščanje o aktivnostih. 

### Alternativni tok(ovi)
Funkcionalnost nima alternativnega toka.

### Izjemni del/napake
Funkcionalnost nima izjemnega dela/napak.

### Pogoji
Uporabnik mora imeti dodano vsaj eno aktivnost.

### Posledice
Uporabnik je obveščen o aktivnostih.

### Posebnosti
Funkcionalnost nima posebnosti.

### Prioritete identificiranih funkcionalnosti
Should have.

**SPREJEMNI TESTI**

|**Primer Uporabe**            | **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod**   | **Pričakovani rezultat** |
|:-----------------            |:-------------------------- |:-------------------------- |:---------- | :----------------------- |
| Prijava na obveščanje v vlogi študenta, profesorja ali administratorja fakultete | Prijava na obveščanje | Obveščanje o aktivnostih ni nastavljeno. | Klik na ikono za obveščanje | Sistem prikaže obvestilo, da je obveščanje nastavljeno. |

---

### **5.10 Odjava uporabnika od obveščanja o aktivnostih**

### Povzetek funkcionalnosti
Študent, profesor ali administrator fakultete se lahko odjavi od prejemanja obvestil o aktivnostih.

### Akterji
Študent, profesor, administrator fakultete.

### Osnovni tok
ŠTUDENT: 

1. Študent izbere možnost *Ne obveščaj me o aktivnostih*. 
2. Sistem prikaže obvestilo, da je uporabnik uspešno odjavljen od obveščanja o njegovih aktivnostih.

PROFESOR: 

1. Profesor izbere možnost *Ne obveščaj me o aktivnostih*. 
2. Sistem prikaže obvestilo, da je uporabnik uspešno odjavljen od obveščanja o njegovih aktivnostih.

ADMINISTRATOR FAKULTETE: 

1. Administrator fakultete izbere možnost *Ne obveščaj me o aktivnostih*. 
2. Sistem prikaže obvestilo, da je uporabnik uspešno odjavljen od obveščanja o aktivnostih.

### Alternativni tok(ovi)
Funkcionalnost nima alternativnega toka.

### Izjemni del/napake
Funkcionalnost nima izjemnega dela/napak.

### Pogoji
Uporabnik mora biti prijavljen na obveščanje o aktivnostih.

### Posledice
Uporabnik ne bo več obveščen o aktivnostih.

### Posebnosti
Funkcionalnost nima posebnosti.

### Prioritete identificiranih funkcionalnosti
Should have.

**SPREJEMNI TESTI**

|**Primer uporabe**            | **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod**   | **Pričakovani rezultat** |
|:-----------------            |:-------------------------- |:-------------------------- |:---------- | :----------------------- |
| Odjava od obveščanja v vlogi študenta, profesorja ali administratorja fakultete | Odjava od obveščanja | Obveščanje o aktivnostih je nastavljeno. | Klik na ikono za obveščanje | Sistem prikaže obvestilo, da uporabnik ne bo več obveščen o aktivnostih. |

### 5.11 **Sinhronizacija aktivnosti z Google koledarjem**

### Povzetek funkcionalnosti
Študent, profesor, administrator fakultete se lahko sinhronizira z Google koledarjem. Aktivnosti, ki so vnešene v Google koledarju s prijavljenim Google računom, vidi tudi v tej aplikaciji.

### Akterji
Študent, profesor, administrator fakultete, Google Calendar API (kot zunanji sistem).

### Osnovni tok
ŠTUDENT:

1. Študent izbere funkcionalnost *Sinhronizacija z Google koledarjem.*
2. Sistem pokaže več Google računov, s katerimi je sinhronizacija možna.
3. Študent izbere enega izmed ponujenih računov.
4. Študent potrdi svojo izbiro.
5. Sistem pokaže sporočilo o uspešni sinhronizaciji aktivnosti s koledarjem.

PROFESOR: 

1. Profesor izbere funkcionalnost *Sinhronizacija z Google koledarjem.*
2. Sistem pokaže več Google računov, s katerimi je sinhronizacija možna.
3. Profesor izbere enega izmed ponujenih računov.
4. Profesor potrdi svojo izbiro.
5. Sistem pokaže sporočilo o uspešni sinhronizaciji aktivnosti s koledarjem.

ADMINISTRATOR FAKULTETE: 

1. Administrator fakultete izbere funkcionalnost *Sinhronizacija z Google koledarjem.*
2. Sistem pokaže več Google računov, s katerimi je sinhronizacija možna.
3. Administrator fakultete izbere enega izmed ponujenih računov.
4. Administrator fakultete potrdi svojo izbiro.
5. Sistem pokaže sporočilo o uspešni sinhronizaciji aktivnosti s koledarjem.

### Alternativni tok(ovi)
Funkcionalnost nima alternativnega toka.

### Izjemni del/napake
ŠTUDENT - IZJEMNI TOK 1:

1. Študent izbere funkcionalnost *Sinhronizacija z Google koledarjem.*
2. Sistem pokaže več Google računov, s katerimi je sinhronizacija možna.
3. Študent izbere enega izmed ponujenih računov.
4. Študent potrdi svojo izbiro.
5. Sistem zavrne sinhronizacijo aktivnosti s koledarjem.
6. Sistem pokaže sporočilo o neuspešni sinhronizaciji aktivnosti s koledarjem.

PROFESOR - IZJEMNI TOK 1:

1. Profesor izbere funkcionalnost *Sinhronizacija z Google koledarjem.*
2. Sistem pokaže več Google računov, s katerimi je sinhronizacija možna.
3. Profesor izbere enega izmed ponujenih računov.
4. Profesor potrdi svojo izbiro.
5. Sistem zavrne sinhronizacijo aktivnosti s koledarjem.
6. Sistem pokaže sporočilo o neuspešni sinhronizaciji aktivnosti s koledarjem.

ADMINISTRATOR FAKULTETE - IZJEMNI TOK 1:

1. Administrator fakultete izbere funkcionalnost *Sinhronizacija z Google koledarjem.*
2. Sistem pokaže več Google računov, s katerimi je sinhronizacija možna.
3. Administrator fakultete izbere enega izmed ponujenih računov.
4. Administrator fakultete potrdi svojo izbiro.
5. Sistem zavrne sinhronizacijo aktivnosti s koledarjem.
6. Sistem pokaže sporočilo o neuspešni sinhronizaciji aktivnosti s koledarjem.

### Pogoji
Študent, profesor ali administrator fakultete mora vnesti vsaj en Google račun, s katerim želi imeti povezavo do aktivnosti.

### Posledice
Študent, profesor ali administrator fakultete je uspešno sinhroniziral aktivnosti z Google koledarjem. Te aktivnosti so shranjene v našo bazo in do njih lahko tudi dostopa.

### Posebnosti
Funkcionalnost nima posebnosti.

### Prioritete identificiranih funkcionalnosti
Should have.


**SPREJEMNI TESTI**

|**Primer uporabe**            | **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod**   | **Pričakovani rezultat** |
|:-----------------            |:-------------------------- |:-------------------------- |:---------- | :----------------------- |
| Sinhronizacija aktivnosti z Google koledarja z računom | Sinhronizacija aktivnosti | Nesinhronizirani podatki | Google račun | Aktivnosti, vidne na seznamu v aplikaciji |
| Sinhronizacija aktivnosti z računom | Sinhronizacija aktivnosti | Nesinhronizirani podatki | Google račun, zavrnjena sinhronizacija | Ustrezno obvestilo o neuspešni sinhronizaciji z računom |

___

### 5.12 **Dodajanje prijatelja**

### Povzetek funkcionalnosti
Študent lahko doda prijatelja. Ko prijatelj prošnjo sprejme, lahko študent doda aktivnost/i na koledar prijatelja.

### Akterji
Študent.

### Osnovni tok
ŠTUDENT: 

1. Študent izbere funkcionalnost *Seznam prijateljev.* 
2. Sistem prikaže vse osebe, ki jih je možno dodati oz. povabiti k sodelovanju.
3. Študent pri osebi, ki jo želi dodati za prijatelja, izbere možnost *Dodaj*. 
4. Študent potrdi svojo izbiro. 
5. Sistem pokaže sporočilo o uspešno poslani prošnji za prijateljstvo izbrani osebi. 
6. Študent počaka, da oseba sprejme prošnjo za prijateljstvo.

### Alternativni tok(ovi)
ŠTUDENT - ALTERNATIVNI TOK 1: 

1. Študent izbere funkcionalnost *Seznam prijateljev.* 
2. Sistem prikaže vse osebe, ki so študentu poslale prošnjo za prijateljstvo. 
3. Študent pri osebi, ki jo želi dodati za prijatelja, izbere možnost *Potrdi*. 
4. Študent potrdi svojo izbiro. 
5. Sistem pokaže sporočilo o uspešno dodani osebi za sodelovanje.

### Izjemni del/napake
ŠTUDENT - IZJEMNI TOK 1:

1. Študent izbere funkcionalnost *Seznam prijateljev.* 
2. Sistem prikaže vse osebe, ki jih je možno dodati oz. povabiti k sodelovanju. 
3. Osebe, ki jo študent želi dodati, ni na seznamu možnih oseb.

### Pogoji
Študent, ki ga nekdo želi dodati, mora biti registriran.

### Posledice
Študent je uspešno dodal prijatelja, s katerim bi sodeloval. Sedaj lahko študenta en drugemu dodajata aktivnosti na koledar.

### Posebnosti
Funkcionalnost nima posebnosti.

### Prioritete identificiranih funkcionalnosti
Should have.

**SPREJEMNI TESTI**

|**Primer uporabe**            | **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod**   | **Pričakovani rezultat** |
|:-----------------            |:-------------------------- |:-------------------------- |:---------- | :----------------------- |
| Dodajanje prijatelja k sodelovanju | Dodajanje prijatelja | Študenta nista prijatelja in ne moreta dodajati aktivnosti en drugemu. | Izbrani študent, ki ga želimo dodati za prijatelja | Študenta sta odslej prijatelja in imata možnost dodajanja aktivnosti en drugemu. |
| Dodajanje prijatelja k sodelovanju | Potrjevanje prošnje za prijateljstvo | Študenta nista prijatelja in ne moreta dodajati aktivnosti en drugemu. | Izbrani študent, kateremu želimo potrditi prošnjo za prijateljstvo | Študenta sta odslej prijatelja in imata možnost dodajanja aktivnosti en drugemu. |

___

### 5.13 **Odstranjevanje prijatelja**

### Povzetek funkcionalnosti
Študent lahko odstrani prijatelja. Študent po tem ne more več dodajati aktivnosti v koledar tega prijatelja.

### Akterji
Študent.

### Osnovni tok
ŠTUDENT:

1. Študent izbere funkcionalnost *Seznam prijateljev.*
2. Sistem prikaže vse osebe, ki jih ima študent dodane kot prijatelje.
3. Študent pri osebi, ki jo želi odstraniti od prijateljev, izbere možnost *Odstrani*.
4. Študent potrdi svojo izbiro.
5. Sistem pokaže sporočilo o uspešno odstranjenem prijatelju.

### Alternativni tok(ovi)
Funkcionalnost nima alternativnih tokov.

### Izjemni del/napake
ŠTUDENT - IZJEMNI TOK 1:

1. Študent izbere funkcionalnost *Seznam prijateljev.*
2. Sistem prikaže vse osebe, ki jih ima študent dodane kot prijatelje.
3. Osebe, ki jo študent želi odstraniti, ni na seznamu možnih oseb.

### Pogoji
Študent, ki naj bi bil odstranjen, mora biti prijatelj.

### Posledice
Študent je uspešno odstranil prijatelja, s katerim ne želi več sodelovati. Sedaj študenta drug drugemu ne moreta več dodajati aktivnosti na koledar.

### Posebnosti
Funkcionalnost nima posebnosti.

### Prioritete identificiranih funkcionalnosti
Should have.


**SPREJEMNI TESTI**

|**Primer uporabe**            | **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod**   | **Pričakovani rezultat** |
|:-----------------            |:-------------------------- |:-------------------------- |:---------- | :----------------------- |
| Odstranjevanje prijatelja | Odstranjevanje prijatelja | Študenta sta prijatelja in imata možnost dodajanja aktivnosti en drugemu. | Izbrani študent, ki ga želimo odstraniti od prijateljev | Študenta nista več prijatelja in nimata možnosti dodajanja aktivnosti en drugemu. |

___

### 5.14 **Ustvarjanje kanala**

### Povzetek funkcionalnosti
Profesor ali administrator fakultete lahko ustvari kanal, kjer so vsebovane aktivnosti, povezane s tem kanalom (npr.datumi oddaj nalog, datumi izpitov, ...).

### Akterji
Profesor, administrator fakultete.

### Osnovni tok
PROFESOR:

1. Profesor izbere funkcionalnost *Ustvari kanal*.
2. Profesor vnese naziv kanala.
3. Profesor potrdi izbiro.
4. Sistem pokaže sporočilo o uspešno ustvarjenem kanalu.

ADMINISTRATOR FAKULTETE:

1. Administrator fakultete izbere funkcionalnost *Ustvari kanal.*
2. Administrator fakultete vnese naziv kanala.
3. Administrator fakultete potrdi izbiro.
4. Sistem pokaže sporočilo o uspešno ustvarjenem kanalu.

### Alternativni tok(ovi)
Funkcionalnost nima alternativnega toka.

### Izjemni del/napake
Funkcionalnost nima izjemnega dela/napak.

### Pogoji
Uporabnik, ki želi ustvarjati kanale, mora biti ali profesor ali administrator fakultete.

### Posledice
Profesor ali administrator fakultete je uspešno ustvaril svoj nov kanal.

### Posebnosti
Profesor ali administrator fakultete lahko ustvari več različnih kanalov.

### Prioritete identificiranih funkcionalnosti
Should have.


**SPREJEMNI TESTI**

|**Primer uporabe**            | **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod**   | **Pričakovani rezultat** |
|:-----------------            |:-------------------------- |:-------------------------- |:---------- | :----------------------- |
| Ustvarjanje prvega kanala za določen predmet | Ustvarjanje kanala | Profil brez kanalov| Zahtevane aktivnosti (datumi oddaj nalog, datumi izpitov, ...) in njihovi datumi | Ustvarjen kanal z izbranimi aktivnostmi |

___

### 5.15 **Seznam kanalov**

### Povzetek funkcionalnosti
Študent, profesor ali administrator fakultete lahko pregleduje kanale, na katere se je prijavil, ali pa jih je ustvaril.

### Akterji
Študent, profesor, administrator fakultete.

### Osnovni tok
ŠTUDENT:

1. Študent izbere funkcionalnost *Prikaži seznam vseh kanalov*.
2. Sistem prikaže seznam vseh kanalov, ki so na voljo študentu.

PROFESOR:

1. Profesor izbere funkcionalnost *Prikaži seznam mojih kanalov*.
2. Sistem prikaže seznam vseh kanalov, ki so na voljo profesorju.

ADMINISTRATOR FAKULTETE:

1. Administrator fakultete izbere funkcionalnost *Prikaži seznam mojih kanalov*.
2. Sistem prikaže seznam vseh kanalov, ki so na voljo administratorju fakultete.

### Alternativni tok(ovi)
Funkcionalnost nima alternativnega toka.

### Izjemni del/napake
Funkcionalnost nima izjemnega dela/napak.

### Pogoji
Uporabnik, ki želi pregledovati seznam kanalov, mora biti prijavljen v aplikacijo.

### Posledice

* Prikaz seznama tistih ustvarjenih kanalov profesorju ali administratorju fakultete, katere je ustvaril.
* Prikaz seznama vseh ustvarjenih kanalov študentu, na katere se lahko prijavi.

### Posebnosti
Funkcionalnost nima posebnosti.

### Prioritete identificiranih funkcionalnosti
Could have.


**SPREJEMNI TESTI**

|**Primer uporabe**            | **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod**   | **Pričakovani rezultat** |
|:-----------------            |:-------------------------- |:-------------------------- |:---------- | :----------------------- |
| Pregledovanje kanalov | Seznam/pregled kanalov | Profil uporabnika | Odprtje seznama s klikom na gumb | Odpre se seznam vseh kanalov. |

___

### **5.16 Prijava na kanal** 

### Povzetek funkcionalnosti 					   
Študent se lahko prijavi na kanal. S tem dobi vpogled do vseh aktivnosti na tem kanalu. 

### Akterji 									   
Študent. 

### Osnovni tok 								   
ŠTUDENT: 

1. Študent izbere funkcionalnost *Vsi kanali.* 
2. Sistem prikaže seznam kanalov, ki trenutno obstajajo. 
3. Študent izbere kanal, na katerega se želi prijaviti. 
4. Študent potrdi svojo izbiro. 
5. Sistem pokaže sporočilo o uspešni  prijavi na kanal.

### Alternativni tok(ovi) 					   
Funkcionalnost nima alternativnega toka.

### Izjemni del/napake							  
Funkcionalnost nima izjemnega dela/napak.

### Pogoji 									   
Vsak študent, ki bi želel biti del kanala, mora biti prijavljen.

### Posledice 							       
Študent uspešno postane član kanala in prejema aktivnosti tega kanala v koledar.

### Posebnosti 		                           
Funkcionalnost nima posebnosti. 

### Prioritete identificiranih funkcionalnosti   
Should have.


### SPREJEMNI TESTI

|**Primer uporabe**            | **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod**   | **Pričakovani rezultat** |
|:-----------------            |:-------------------------- |:-------------------------- |:---------- | :----------------------- |
| Prijava prvega študenta v kanal | Prijava v kanal | Prisoten samo ustvarjalec kanala | Google račun/e-pošta študenta | Nov član kanala |

___

### **5.17 Odjava od kanala**             

### Povzetek funkcionalnosti 					   
Študent se lahko odjavi od kanala. S tem izgubi vpogled do vseh aktivnosti na tem kanalu. 

### Akterji 									   
Študent.

### Osnovni tok 								   
ŠTUDENT: 

1. Študent izbere funkcionalnost *Vsi kanali.* 
2. Sistem prikaže seznam kanalov, ki trenutno obstajajo. 
3. Študent izbere kanal, od katerega se želi odjaviti. 
4. Študent potrdi svojo izbiro. 
5. Sistem pokaže sporočilo o uspešni odjavi od kanala.

### Alternativni tok(ovi) 					   
Funkcionalnost nima alternativnega toka.

### Izjemni del/napake							  
Funkcionalnost nima izjemnega dela/napak.

### Pogoji 									   
Vsak študent, ki se želi odjaviti od kanala, mora biti prijavljen.

### Posledice 							       
Študent je uspešno odjavljen od kanala in v koledar ne prejema več aktivnosti tega kanala. 

### Posebnosti 		                           
Funkcionalnost nima posebnosti.

### Prioritete identificiranih funkcionalnosti   
Should have.



### SPREJEMNI TESTI

|**Primer uporabe**            | **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod**   | **Pričakovani rezultat** |
|:-----------------            |:-------------------------- |:-------------------------- |:---------- | :----------------------- |
| Odjava študenta od kanala | Odjava od kanala | Študent je član kanala in prejema aktivnosti tega kanala. | Google račun/e-pošta študenta | Študent ni več član tega kanala in ne prejema več aktivnosti tega kanala. |

___


### **5.18 Pregledovanje zahtev za višje vloge in dodeljevanje le-teh**             

### Povzetek funkcionalnosti 					   
Glavni administrator ali administrator fakultete lahko pregleduje zahteve za višje vloge ter jih tudi dodeli. Vlogo lahko dodeli uporabniku, ki je natanko nivo nižje od njega. 

### Akterji 									   
Administrator fakultete, glavni administrator. 

### Osnovni tok 								   
ADMINISTRATOR FAKULTETE: 

1. Administrator fakultete izbere funkcionalnost *Preglej zahteve za višje vloge*.
2. Administratorju fakultete se prikaže seznam vseh zahtev za višjo vlogo. 
3. Administrator fakultete preveri, ali je izbrani uporabnik upravičen do zahtevane višje vloge. 
4. V primeru, da je uporabnik upravičen, administrator fakultete s klikom na gumb *odobri zahtevo*, odobri zahtevo po višji vlogi. V nasprotnem primeru s klikom na gumb *zavrni zahtevo* zavrne izbrano zahtevo. 
5. Profesor dobi preko e-pošte povratno informacijo, ali je bila njegova prošnja ugojena ali ne.  

GLAVNI ADMINISTRATOR: 

1. Glavni administrator izbere funkcionalnost *Preglej zahteve za višje vloge*.
2. Glavnemu administratorju se prikaže seznam vseh zahtev za višjo vlogo. 
3. Glavni administrator preveri, ali je izbrani uporabnik upravičen do zahtevane višje vloge. 
4. V primeru, da je uporabnik upravičen, glavni administrator s klikom na gumb *odobri zahtevo* odobri zahtevo po višji vlogi. V nasprotnem primeru s klikom na gumb *zavrni zahtevo* zavrne izbrano zahtevo. 
5. Administrator fakultete dobi preko e-pošte povratno informacijo, ali je bila njegova prošnja ugojena ali ne. 

### Alternativni tok(ovi) 					   
Funkcionalnost nima alternativnega toka.

### Izjemni del/napake							  
Funkcionalnost nima izjemnega dela/napak. 

### Pogoji 									   
Vsaka oseba, ki bi želela dodeljevati vloge drugim osebam, mora imeti višjo vlogo od zahtevane.

### Posledice 		

* Administrator fakultete dodeli vlogo profesorju.
* Glavni administrator dodeli vlogo administratorju fakultete.

### Posebnosti 		

* Administrator fakultete lahko dodeli vlogo le profesorjem.
* Glavni administrator lahko dodeli vlogo le administratorju fakultete.

### Prioritete identificiranih funkcionalnosti   
Must have.


### SPREJEMNI TESTI

|**Primer uporabe**            | **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod**   | **Pričakovani rezultat** |
|:-----------------            |:-------------------------- |:-------------------------- |:---------- | :----------------------- |
| Ugoditev prošnje profesorju za dotično vlogo | Dodeljevanje vloge profesorju | Nov registriran uporabnik | Oddana registracija uporabnika | Administrator fakultete ugodi prošnjo profesorju. |

___

### **5.19 Brisanje uporabnikov**             

### Povzetek funkcionalnosti 					   
Profesor, administrator fakultete ali glavni administrator lahko izbriše uporabnika, ki je en nivo pod njim. 

### Akterji 									   
Profesor, administrator fakultete, glavni administrator. 

### Osnovni tok 								   
PROFESOR: 

1. Profesor izbere funkcionalnost *Izbriši uporabnika.* 
2. Sistem prikaže vse študente, ki so registrirani v aplikaciji. 
3. Profesor izbere enega ali več študentov. 
4. Profesor potrdi svojo izbiro. 
5. Sistem pokaže sporočilo o uspešno izbrisani/h osebi/ah.
  
ADMINISTRATOR FAKULTETE: 

1. Administrator fakultete izbere funkcionalnost *Izbriši uporabnika.* 
2. Sistem prikaže vse profesorje, ki so registrirani v aplikaciji. 
3. Administrator fakultete izbere eno ali več možnih oseb. 
4. Administrator fakultete potrdi svojo izbiro. 
5. Sistem pokaže sporočilo o uspešno izbrisani/h osebi/ah. 
 
GLAVNI ADMINISTRATOR: 

1. Glavni administrator fakultete izbere funkcionalnost *Izbriši uporabnika.* 
2. Sistem prikaže vse administratorje fakultete, ki so registrirani v aplikaciji. 
3. Administrator fakultete izbere eno ali več možnih oseb. 
4. Administrator fakultete potrdi svojo izbiro. 
5. Sistem pokaže sporočilo o uspešno izbrisani/h osebi/ah. 

### Alternativni tok(ovi) 					   
Funkcionalnost nima alternativnega toka.

### Izjemni del/napake							  
Funkcionalnost nima izjemnega dela/napak. 

### Pogoji 		

* Vsaka oseba, ki bi želela brisati druge osebe, mora biti prijavljena.
* Profesor lahko briše samo študente.
* Administrator fakultete lahko briše samo profesorje.
* Glavni administrator lahko briše samo administratorja fakultete.

### Posledice 							       
Oseba je uspešno izbrisana iz baze.

### Posebnosti 		                           
Brisanje je omogočeno samo nadrejenim osebam. 


### Prioritete identificiranih funkcionalnosti
Won't have this time.

### SPREJEMNI TESTI

|**Primer uporabe**            | **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod**   | **Pričakovani rezultat** |
|:-----------------            |:-------------------------- |:-------------------------- |:---------- | :----------------------- |
| Brisanje osebe, ki je objavljala neprimerno vsebino | Brisanje osebe | Dotična oseba in njeni podatki se nahajajo v bazi. | Google račun/e-pošta izbrane osebe | Izbris osebe iz baze |

---

### **5.20 Dodajanje zapisov v šifrante**             

### Povzetek funkcionalnosti 					   
Glavni administrator fakultete lahko dodaja nove zapise v šifrante. 

### Akterji 									   
Glavni administrator.  

### Osnovni tok 								   
GLAVNI ADMINISTRATOR: 

1. Glavni administrator izbere šifrant, kateremu želi dodati nov zapis.
2. Glavni administrator klikne na gumb *Nov zapis*.
3. Glavni administrator izpolni polje za naziv novega zapisa.
4. Glavni administrator s klikom na gumb doda nov zapis.

### Alternativni tok(ovi) 					   
Funkcionalnost nima alternativnega toka.

### Izjemni del/napake							  
GLAVNI ADMINISTRATOR - IZJEMNI TOK 1: 

1. Glavni administrator izbere funkcionalnost *Dodajanje zapisov v šifrant*. 
2. Sistem prikaže obrazec za vnos novega zapisa v šifrant. 
3. Glavni administrator ne izpolni vseh zahtevanih podatkov. 
4. Glavni administrator potrdi vnos novega zapisa v šifrant. 
5. Sistem izpiše opozorilo, da vnos novega zapisa v šifrant ni uspešen zaradi manjkajočih podatkov. 

### Pogoji 									   
Glavni administrator mora biti prijavljen v našo aplikacijo. 

### Posledice 							       
Glavni administrator uspešno doda nov zapis v šifrante.

### Posebnosti 		                           
Funkcionalnost nima posebnosti.

### Prioritete identificiranih funkcionalnosti   
Should have.


### SPREJEMNI TESTI

|**Primer uporabe**            | **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod**   | **Pričakovani rezultat** |
|:-----------------            |:-------------------------- |:-------------------------- |:---------- | :----------------------- |
| Glavni administrator vnese nov zapis v šifrante | Dodajanje zapisa v šifrant | Šifrant brez novega zapisa | Naziv zapisa šifranta |Uspešen zapis novega vpisa v šifrant |
| Glavni administrator vnese nov zapis v šifrante  | Dodajanje zapisa v šifrant | Šifrant brez novega zapisa | Nepopolno izpolnjen naziv zapisa šifranta | Sistem opozori glavnega administratorja o nevnešenih podatkih. |

---

### **5.21 Urejanje zapisov v šifrantih**             

### Povzetek funkcionalnosti 					   
Glavni administrator fakultete lahko uredi zapise v šifrantih. 

### Akterji 									   
Glavni administrator.  

### Osnovni tok 								   
GLAVNI ADMINISTRATOR: 

1. Glavni administrator izbere šifrant, kateremu želi urediti zapis.
2. Glavni administrator klikne na gumb *Uredi zapis*.
3. Glavni administrator izpolni polje za naziv novega zapisa.
4. Glavni administrator s klikom na gumb doda urejen zapis.

### Alternativni tok(ovi) 					   
GLAVNI ADMINISTRATOR - ALTERNATIVNI TOK 1: 

1. Glavni administrator izbere šifrant, kateremu želi izbrisati zapis.
2. Glavni administrator klikne na gumb *Izbris zapisa*. 
3. Glavni administrator s klikom na gumb izbriše zapis.
4. Glavni administrator izbere šifrant, kateremu želi dodati nov zapis.
5. Glavni administrator klikne na gumb nov zapis.
6. Glavni administrator izpolni polje za naziv novega zapisa.
7. Glavni administrator s klikom na gumb doda nov zapis.

### Izjemni del/napake							  
GLAVNI ADMINISTRATOR - IZJEMNI TOK 1: 

1. Glavni administrator izbere funkcionalnost *Urejanje zapisov v šifrant*. 
2. Sistem prikaže obrazec za urejanje novega zapisa v šifrant. 
3. Glavni administrator ne izpolni vseh zahtevanih podatkov. 
4. Glavni administrator potrdi vnos urejenega zapisa v šifrant. 
5. Sistem izpiše opozorilo, da posodobitev vnosa zapisa v šifrant ni uspešna zaradi manjkajočih podatkov. 

### Pogoji 			

* Glavni administrator mora biti prijavljen v našo aplikacijo. 
* Če želi glavni administrator urediti zapis v šifrantu, mora šifrant že vsebovati vsaj en zapis.

### Posledice 							       
Glavni administrator uspešno uredi zapis v šifrante. 

### Posebnosti 		                           
Funkcionalnost nima posebnosti.

### Prioritete identificiranih funkcionalnosti   
Could have.


### SPREJEMNI TESTI

|**Primer uporabe**            | **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod**   | **Pričakovani rezultat** |
|:-----------------            |:-------------------------- |:-------------------------- |:---------- | :----------------------- |
| Glavni administrator uredi zapis v šifrantu  | Urediti zapis v šifrant | Zapis v šifrantu, katerega zapis želimo spremeniti | Urejen naziv zapisa šifranta |Uspešno urejen zapis vpisa v šifrant|
| Glavni administrator uredi zapis v šifrantu.  | Urediti zapis v šifrant | Zapis v šifrantu, katerega zapis želimo spremeniti | Vnešeni niso vsi zahtevani podatki. | Sistem opozori glavnega administratorja o nevnešenih podatkih.|

---
### **5.22 Brisanje zapisov v šifrantih**        

### Povzetek funkcionalnosti 					   
Glavni administrator fakultete lahko izbriše zapise v šifrantih. 

### Akterji 									   
Glavni administrator.  

### Osnovni tok 								   
GLAVNI ADMINISTRATOR: 

1. Glavni administrator izbere šifrant, kateremu želi izbrisati zapis.
2. Glavni administrator klikne na gumb *Izbris zapisa*. 
3. Glavni administrator s klikom na gumb izbriše zapis.

### Alternativni tok(ovi) 					   
Funkcionalnost nima alternativnega toka.

### Izjemni del/napake							  
Funkcionalnost nima izjemnega dela/napak.

### Pogoji 								

* Glavni administrator mora biti prijavljen v našo aplikacijo. 
* Če želi glavni administrator izbrisati zapis v šifrantu, mora šifrant že vsebovati vsaj en zapis. 

### Posledice 							       
 Glavni administrator uspešno izbriše zapis v šifrantu.

### Posebnosti 		                           
 Funkcionalnost nima posebnosti.

### Prioritete identificiranih funkcionalnosti   
 Should have.

### SPREJEMNI TESTI

|**Primer uporabe**            | **Funkcija, ki se testira** | **Začetno stanje sistema** | **Vhod**   | **Pričakovani rezultat** |
|:-----------------            |:-------------------------- |:-------------------------- |:---------- | :----------------------- |
| Glavni administrator izbriše zapis v šifrantu  | Izbris zapisa v šifrantu | Zapis v šifrantu, katerega zapis želimo izbrisati | Izbrani zapis v šifrantu |Uspešno izbrisan zapis vpisa v šifrantu |



## 6. Nefunkcionalne zahteve


**Zahteve izdelka / kakovostne značilnosti:**

- **Sistem mora omogočati sočasno delo najmanj 100 uporabnikom sistema, odzivni časi ne smejo prekoračiti 3000 ms.** *Želimo imeti odzivno aplikacijo. V kolikor se ugotovi, da nam te zahteve ne uspe izpolniti, zahtevamo vsaj, da je uporabnik obveščen, ko poteka nalaganje vsebine, ki zahteva več časa od predvidenega.*
- **Sistem mora biti dosegljiv 99 % časa.** *Posebnih izpadov aplikacije sicer ne pričakujemo, vseeno pa pustimo 1 % prostora za izpade zaradi različnih tveganj (npr. izpad strežnika).*
- **Sinhronizacija podatkov iz Google Calendar API-ja se mora v 95 % primerih izvesti v <= 5000 ms.** *Ta zahteva sicer ni v popolnosti odvisna od nas, saj gre za API, vendarle pa lahko tudi v tem primeru zahtevamo vsaj, da je uporabnik obveščen, kadar sinhronizacija poteka dlje od predvidenega časa.*
- **Samo glavni administrator lahko dodaja, ureja ali briše šifrante.** *Delo s šifranti je pomembna odgovornost, ki jo zaupamo najvišjemu členu v hierahiji vlog. V kolikor pride do neustreznih vnosov ali napak, ima možnost dodajanja, brisanja in urejanja.*
- **Študent lahko dodaja aktivnosti samo v koledarje študentov, ki so sprejeli njegovo prošnjo za prijateljstvo.** *Da ne bi prišlo do zlorabe pri dodajanju aktivnosti v koledarje drugih študentov, je potrebna obojestranska potrditev prijateljstva. Pri tem dodatno 'varovalko' predstavlja funkcionalnost Odstrani prijatelja, kjer ima študent še vedno možnost odstranitve, če se izkaže, da mu prijatelj povzroča nevšečnosti.*
- **Pri registraciji mora biti izbrano geslo dolžine vsaj 6 ter vsebovati vsaj 1 veliko črko in 1 številko.** *Z vidika varnosti želimo poskrbeti za to, da uporabniki ne morejo nastaviti gesel, ki v splošnem ne veljajo za varna, zato smo pri tem postavili smiselno omejitev, ki je prisotna pri številnih spletnih aplikacijah.*

**Organizacijske zahteve:**

- **Projekt mora biti izveden po Scrum metodologiji.** *V okviru Sprint Planninga želimo predvideti, katere funkcionalnosti se bodo implementirale v določeni fazi, Daily Scrum Meetingi pa bodo poskrbeli za to, da lahko na dnevni bazi nadziramo, ali projekt poteka po načrtu, ali smo kje v zaostanku.* 
- **Na projektu naj se uporablja sistem za nadzor različic (version control)**. *Aplikacijo bodo sočasno izdelovali štirje člani ekipe, zato je pomembno, da na 'master' veji vselej ohranimo stabilno verzijo aplikacije, medtem ko se vse funkcionalnosti razvijajo v okviru dodeljenih 'taskov' v posameznih vejah razvoja. Vsako uveljavljanje sprememb naj bo povezano z dodeljeno nalogo.*


**Zunanje zahteve:**

- **Sistem mora delovati na brskalnikih Google Chrome, Mozilla Firefox, Microsoft Edge, Safari**. *Po podatkih o deležu trga, ki ga zavzema posamezni brskalnik, so ti štirje brskalniki med najbolj uporabljenimi. V prvi peterici sicer najdemo tudi Internet Explorer, kateremu pa delež strmo pada, zato smo se odločili podpreti Edge kot njegovega naslednika.* 
- **Sistem mora delovati in se ustrezno prilagajati na različnih velikostih zaslonov (računalniki, mobilne naprave, tablice)**. *Da bo uporabniška izkušnja čim boljša, se mora sistem znati prilagoditi velikosti naprave in njenemu položaju (pokončnemu ali ležečemu).*
- **Sistem mora delovati v skladu s protokolom RFC 4918.** *Na tem protokulu je namreč zasnovano delovanje Googlovega API-ja, kar je za našo aplikacijo pomembno zaradi integracije koledarja z Googlovim koledarjem.*

## 7. Prototipi vmesnikov

### 7.1 Zaslonske maske
Vpis  
![Vpis](../img/vpis.PNG)  
Registracija  
![Registracija](../img/registracija.PNG)  
Registracija profesor  
![Registracija profesor](../img/registracija-profesor.PNG)  
Registracija administrator fakultete  
![Registracija administrator fakultete](../img/registracija-administrator-fakultete.PNG)  
Prva stran študent  
![Prva stran študent](../img/prva-stran-student.PNG)  
Prva stran profesor  
![Prva stran profesor](../img/prva-stran-profesor.PNG)  
Prva stran administrator fakultete  
![Prva stran administrator fakultete](../img/prva-stran-administrator-fakultete.PNG)  
Prva stran glavni administrator  
![Prva stran glavni administrator](../img/prva-stran-glavni-administrator.PNG)  
Kanali študenta  
![Kanali študenta](../img/kanali-studenta.PNG)  
Seznam prijateljev  
![Seznam prijateljev](../img/seznam-prijateljev.PNG)  
Kanali (nov) profesor  
![Kanali (nov) profesor](../img/kanali-nov-profesor.PNG)  
Kanali (uredi) profesor  
![Kanali (uredi) profesor](../img/kanali-uredi-profesor.PNG)  
Uredi šifrant  
![Uredi šifrant](../img/uredi-sifrant.PNG)  
Dodaj zapis v šifrant  
![Dodaj zapis v šifrant](../img/dodaj-zapis-v-sifrant.PNG)  
Uredi zapis v šifrant  
![Uredi zapis v šifrant](../img/uredi-zapis-v-sifrant.PNG)  
Potrditev profesorja  
![Potrditev profesorja](../img/potrditev-profesorja.PNG)  
Dodaj aktivnosti  
![Dodaj aktivnosti](../img/dodaj-aktivnosti.PNG)  
Uredi aktivnosti  
![Uredi aktivnosti](../img/uredi-aktivnosti.PNG)  
Pregled dneva  
![Pregled dneva](../img/pregled-dneva.PNG)  
TO-DO seznam  
![TO-DO](../img/to-do.PNG)  


### 7.2 Opisi sistemskih vmesnikov

Za našo aplikacijo smo predvideli naslednje sistemske vmesnike:

> *Opomba: Stolpec pogoji predstavlja zahteve, ki jih mora uporabnik izpolnjevati znotraj forme, da mu je dovoljena interakcija z elementom. Če je vnos polja obvezen, je vnos obvezen samo v primeru, da so pogoji izpolnjeni(pogoj ima prednost pred obveznostjo polja)*


#### **Registracija:**

| Naziv elementa           | Vrsta elementa    | Je vnos obvezen  | Pogoji                                    |
| :----------------------- | :--------------   | :--------------: | :---------------------------------------- |
| Ime                      | Tekstovno polje   | Da               |                                           |
| Priimek                  | Tekstovno polje   | Da               |                                           |
| E-pošta                  | Tekstovno polje   | Da               |                                           |
| Geslo                    | Tekstovno polje   | Da               |                                           |
| Ponovi geslo             | Tekstovno polje   | Da               |                                           |
| Želim vlogo profesorja   | Potrditveno polje | Ne               |                                           |
| Želim vlogo administratorja fakultete   | Potrditveno polje | Ne               |                                           |
| Izbira fakultete         | Spustni meni      | Da               | Izbrano polje: Želim vlogo profesorja     |
| Vnos fakultete           |Tekstovno polje    | Da               | Izbrano polje: Želim vlogo administratorja fakultete     |

#### **Prijava:**

| Naziv elementa           | Vrsta elementa    | Je vnos obvezen  | Pogoji                                    |
| :----------------------- | :--------------   | :--------------: | :---------------------------------------- |
| E-pošta                  | Tekstovno polje   | Da               |                                           |
| Geslo                    | Tekstovno polje   | Da               |                                           |


#### **Dodajanje kanala:**

| Naziv elementa           | Vrsta elementa    | Je vnos obvezen  | Pogoji                                    |
| :----------------------- | :--------------   | :--------------: | :---------------------------------------- |
| Naziv kanala             | Tekstovno polje   | Da               |                                           |


#### **Urejanje kanala:**

| Naziv elementa           | Vrsta elementa    | Je vnos obvezen  | Pogoji                                    |
| :----------------------- | :--------------   | :--------------: | :---------------------------------------- |
| Naziv kanala             | Tekstovno polje   | Da               |                                           |


#### **Dodajanje aktivnosti:**

| Naziv elementa           | Vrsta elementa    | Je vnos obvezen  | Pogoji                                    |
| :----------------------- | :--------------   | :--------------: | :---------------------------------------- |
| Naziv aktivnosti         | Tekstovno polje   | Da               |                                           |
| Tip aktivnosti           | Spustni meni      | Da               |                                           |
| Datum aktivnosti         | Tekstovno polje(datum)   | Da               | Izbrani tip aktivnosti ni: Zapisek                                       |
| Opis aktivnosti          | Tekstovno polje   | Da               |  


#### **Urejanje aktivnosti:**

| Naziv elementa           | Vrsta elementa    | Je vnos obvezen  | Pogoji                                    |
| :----------------------- | :--------------   | :--------------: | :---------------------------------------- |
| Naziv aktivnosti         | Tekstovno polje   | Da               |                                           |
| Tip aktivnosti           | Spustni meni      | Da               |                                           |
| Datum aktivnosti         | Tekstovno polje(datum)   | Da               | Izbrani tip aktivnosti ni: Zapisek                                       |
| Opis aktivnosti          | Tekstovno polje   | Da               |  |


#### **Dodajanje zapisa v šifrant:**

| Naziv elementa           | Vrsta elementa    | Je vnos obvezen  | Pogoji                                    |
| :----------------------- | :--------------   | :--------------: | :---------------------------------------- |
| Naziv šifranta           | Tekstovno polje   | Da               |                                           |


#### **Urejanje zapisa v šifrantu:**

| Naziv elementa           | Vrsta elementa    | Je vnos obvezen  | Pogoji                                    |
| :----------------------- | :--------------   | :--------------: | :---------------------------------------- |
| Naziv šifranta           | Tekstovno polje   | Da               |                                           |

### 7.3 Opisi zunanjih vmesnikov

Naši aplikaciji bo kot zunanji vmesnik služil **Google Calendar API**. Uporabili ga bomo pri integraciji naše aplikacije (oz. koledarja v njej) z uporabnikovim Google računom, če ga ima le-ta v uporabi. Tako se bodo vsi dogodki, ki jih že ima vnešene v Google koledarju, dodali še na koledar v naši aplikaciji.
Če pa uporabnik tega računa nima, mu bo vseeno na voljo naš koledar, ki bo v grobem še vedno hranil vse dogodke, ki jih bo želel vnesti. Le dodatnega vira ne bo.

Primer uporabe, kjer bomo potrebovali zunanji vmesnik, je *Sinhronizacija aktivnosti z Google koledarjem*.
Uporabniki bodo imeli možnost sinhronizacije našega koledarja z Google koledarjem s klikom na gumb: Sinhroniziraj koledar. Ob kliku na gumb, bo uporabnik izbral gmail račun, na katerem ima Google koledar, s katerim se želi sinhronizirati. S tem bo naša aplikacija lahko izvedla OAuth2 avtorizacijo, ki je potrebna za uporabo Google Calendar API.

Nato bo naša aplikacija poslala naslednji HTTP zahtevek:
```
GET /calendars/calendarId/events
```

kjer calendarId predstavlja id koledarja s kaerim se želimo sinhronizirati.
Zahtevek nam bo vrnil JSON odgovor, ki bo vseboval seznam dogodkov ki so vnešeni v koledar. 
Primer JSON odgovora koledarja, ki vsebuje en sam dogodek:
```
{
  "kind": "calendar#events", 
  "defaultReminders": [
    {
      "minutes": 10, 
      "method": "popup"
    }
  ], 
  "items": [
    {
      "status": "confirmed", 
      "kind": "calendar#event", 
      "end": {
        "date": "2019-04-09"
      }, 
      "created": "2019-04-07T10:21:43.000Z", 
      "iCalUID": "4u3eopagmcli5r8d2dlctot6vn@google.com", 
      "reminders": {
        "useDefault": false
      }, 
      "extendedProperties": {
        "private": {
          "everyoneDeclinedDismissed": "-1"
        }
      }, 
      "htmlLink": "https://www.google.com/calendar/event?eid=NHUzZW9wYWdtY2xpNXI4ZDJkbGN0b3Q2dm4gemFkcmF2ZWNtaWhhOTg3QG0", 
      "sequence": 0, 
      "updated": "2019-03-23T10:21:43.503Z", 
      "summary": "Rok za oddajo TPO-LP2", 
      "start": {
        "date": "2019-04-08"
      }, 
      "etag": "\"3109265007006000\"", 
      "transparency": "transparent", 
      "organizer": {
        "self": true, 
        "email": "zadravecmiha987@gmail.com"
      }, 
      "creator": {
        "self": true, 
        "email": "zadravecmiha987@gmail.com"
      }, 
      "id": "4u3eopagmcli5r8d2dlctot6vn"
    }
  ], 
  "updated": "2019-03-23T10:21:43.503Z", 
  "summary": "zadravecmiha987@gmail.com", 
  "etag": "\"p32cd30lhs6uu20g\"", 
  "nextSyncToken": "CJjRgrHhveECEJjRgrHhveECGAU=", 
  "timeZone": "Europe/London", 
  "accessRole": "owner"
}
```
Naš program bo prebral dogodke iz pridobljenega JSON odgovora ter jih primerjal z dogodki, ki so že venešeni v našo aplikacijo. Dogodke, ki jih naša aplikacija še ne vsebuje, bo dodal v naš koledar. Prav tako pa bo določil dogodke, ki obstajajo v našem koledarju, medtem ko jih v Google koledarju še nimamo. Te dogodke bomo posredovali Google koledarju z naslednjim HTTP zahtevkom:

```
POST /calendars/calendarId/events
```
ki bo poklican za vsak dogodek posebaj. V telesu klica bomo poslali naslednje parametre:

- start (čas začetka dogodka)
- end (čas konca dogodka)
- summary (naziv dogodka)
- description (opis dogodka)




## Reference

[1]: Lavbič, D. **Tehnologija programske opreme 2018/2019**. https://teaching.lavbic.net/TPO/, 2019.

[2]: Balsamiq for Desktop. https://balsamiq.com/wireframes/desktop/

[3]: Google Calendar API. https://developers.google.com/calendar/

[4]: Visual Paradigm. https://www.visual-paradigm.com/