# Načrt sistema

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Matic Anžič, Vanesa Gradišar, Živa Škof in Miha Zadravec |
| **Kraj in datum** | Ljubljana, 21. 4. 2019 |

## Povzetek

V prejšnji nalogi smo zasnovali funkcionalnosti in zajeli zahteve aplikacije. Tokrat pa smo se lotili samega načrtovanja. Začeli smo z načrtom arhitekture (iz razvojnega in logičnega pogleda), ki je bil osnova za vse nadaljnje diagrame in ki sam predstavlja vpogled v ozadje dogajanja aplikacije. Poleg izdelave razrednega diagrama, smo opisali tudi same razrede, ki so prisotni v njem - njihove atribute in nesamoumevne metode.
Nato je sledil načrt obnašanja - z diagrami zaporedij, stanj, aktivnosti in sodelovanja smo prikazali načrtovan potek dogajanj (v funkcionalnostih) med uporabo aplikacije, kjer smo predstavili tako osnovne in alternativne tokove kot izjemne dele, do katerih lahko pride.

## 1. Načrt arhitekture
Za načrt arhitekture smo se odločili uporabiti vzorec MVC (model-pogled-krmilnik), ker s tem ločimo predstavitev in interakcijo aplikacije od sistemskih podatkov. Tako lahko na enostaven način nadgradimo/spremenimo posamezno komponento, ne da bi bilo pri tem potrebno spremeniti celotno aplikacijo (primer: menjava baze). Načrt arhitekture smo naredili iz logičnega in razvojnega pogleda.

### 1.1 Načrt arhitekture iz razvojnega pogleda

Načrt prikazuje delitev programske opreme na modele, poglede in kontrolerje ter kako se med sabo povezujejo.

![](../img/TPO-LP3Arhitekturarazvojnipogled.png)

### 1.2 Načrt arhitekture iz logičnega pogleda

Načrt prikazuje ključne entitete sistema in interakcije, ki se med njimi dogajajo.

![](../img/TPO-LP3-logicni_pogled.png)

## 2. Načrt strukture

### 2.1 Razredni diagram

Prikazan je razredni diagram s števnostmi in povezavami med enitetami, kontrolerji in mejniki. 

![](../img/ClassDiagram.png)

Legenda:

* Entitetni razredi - modra barva,
  
* kontrolni - zelena barva,

* mejni - rdeča barva.
___

### 2.2 Opis razredov

#### **2.2.1 Oseba**
Predstavlja osebo oz. uporabnika, ki uporablja sistem.

#### **Atributi:**

* id: int
* ime: String
* priimek: String
* e-pošta: String

#### **Nesamoumevne metode:**
/

#### **2.2.2 Študent**
Predstavlja registriranega uporabnika, ki uporablja aplikacijo/sistem.

#### **Atributi:**

* id: int
* ime: String
* priimek: String
* e-pošta: String

#### **Nesamoumevne metode:**

* void dodajPrijatelja()

    * imena in tipi parametrov: id študenta, ki dodaja (int), id dodanega študenta (int)
    * tip rezultata: / (void)

* void odstraniSSeznama()

    * imena in tipi parametrov: id študenta, ki odstranjuje (int), id izbrisanega študenta (int)
    * tip rezultata: / (void)

* void naročiNaKanal()

    * imena in tipi parametrov: id študenta, ki se prijavlja na kanal (int), id kanala, na katere se prijavlja študent (int)
    * tip rezultata: / (void)

* void odjaviSKanala()
 
    * imena in tipi parametrov: id študenta, ki se odjavlja s kanala (int), id kanala, s katerega se odjavlja študent (int)
    * tip rezultata: / (void)

* void vklopiObveščnje()

    * imena in tipi parametrov: id aktivnosti(int), o kateri želi biti obveščen uporabnik
    * tip rezultata: / (void)

* void izklopiObveščanje()

    * imena in tipi parametrov: id aktivnosti(int), s katere se želi uporabnik odjaviti
    * tip rezultata: / (void)

#### **2.2.3 Profesor**
Predstavlja registriranega uporabnika, ki uporablja aplikacijo/sistem in mu je bila odobrena vloga 'Profesor'.

#### **Atributi:**

* id: int
* ime: String
* priimek: String
* e-pošta: String
* fakulteta: int (šifrant fakultete)

#### **Nesamoumevne metode:**

* void ustvariKanal()

    * imena in tipi parametrov: id profesorja, ki ustvarja kanal (int)
    * tip rezultata: / (void)

#### **2.2.4 Administrator fakultete:**
Predstavlja registriranega uporabnika, ki uporablja aplikacijo/sistem in mu je bila odobrena vloga 'Administrator fakultete'. Sam pa lahko odobri prošnje za vlogo profesorja.

#### **Atributi:**

* id: int
* ime: String
* priimek: String
* e-pošta: String
* fakulteta: int (šifrant fakultete)

#### **Nesamoumevne metode:**

* void potrdiProšnjo()
 
    * imena in tipi parametrov: id uprabnika, ki je zaprosil za vlogo profesorja (int)
    * tip rezultata: / (void)
    
* void ustvariKanal()

    * imena in tipi parametrov: id profesorja, ki ustvarja kanal (int)
    * tip rezultata: / (void)

#### **2.2.5 Glavni administrator:**
Predstavlja registriranega uporabnika, ki uporablja aplikacijo/sistem in mu je bila odobrena vloga 'Glavni administrator'. Sam pa lahko odobri prošnje za vlogo administratorja fakultete.

#### **Atributi:**

* id: int
* ime: String
* priimek: String
* e-pošta: String

#### **Nesamoumevne metode:**

* void potrdiProšnjo()

    * imena in tipi parametrov: id uprabnika, ki je zaprosil za vlogo administratorja fakultete (int)
    * tip rezultata: / (void)
    
* void ustvariŠifrant()

    * imena in tipi parametrov: /
    * tip rezultata: / (void)

* void urediŠifrant()

    * imena in tipi parametrov: id šifranta, ki bo urejan (int)
    * tip rezultata: / (void)

#### **2.2.6 Aktivnost**
Predstavlja dogodek oz. pomemben datum (opomnik), ki si ga uporabnik shrani v koledar.

#### **Atributi:**

* id: int
* ime: String
* sodelujoči: List
* datumVpisa: Date
* datumDo: Date

> *Opomba: Tu datumDo pomeni, do katerega datuma mora biti aktivnost zaključena/opravljena, na kater dan je njen rok.*

#### **Nesamoumevne metode:**
/

#### **2.2.7 Kanal**
Predstavlja mesto, kjer so zbrane na primer informacije v zvezi z določenim predmetom (datumi oddaj nalog in izpitov). Ustvarjen je lahko s strani profesorja ali administratorja fakultete, nanj pa se lahko prijavi katerikoli študent.

#### **Atributi:**

* id: int
* ime: String
* sodelujoči: List
* idProfesorja: int

> *Opomba: id_profesorja - id profesorja, ki je kanal ustvaril
> Sodelujoči - id-ji študentov, prijavljenih na kanal*

#### **Nesamoumevne metode:**
/

#### **2.2.8 Seznam TO-DO**
Predstavlja virtualen listek, na katerem bodo zapisane stvari, ki jih je študent predhodno vnesel kot aktivnosti. Ko jih opravijo, le-te lahko označijo kot opravljene. Služi za pregled aktivnosti in mesto, kamor se zapiše stvari, ki se jih ne sme pozabiti.

#### **Atributi:**

* id: int
* aktivnosti: List

#### **Nesamoumevne metode:**
/

#### **2.2.9 Obvestilo**
Predstavlja prikazno okno oz. sporočilo, ki bo študenta spomnilo, da mora opraviti neko aktivnost, za katero si ga je nastavil. Pogostost sporočil bo nastavljiva.

#### **Atributi:**

* id: int
* datumPojavitve: Date

#### **Nesamoumevne metode:**
/

#### **2.2.10 Koledar**
Predstavlja pregled nad celim mesecem (datumsko in po dnevih), kjer so shranjene aktivnosti.

#### **Atributi:**

* id: int
* ime: String

#### **Nesamoumevne metode:**
/

#### **2.2.11 Kontroler koledarja**
Predstavlja kontroler, ki bo prikazoval funkcionalnosti, ki se tičejo aktivnosti.

#### **Atributi:**
/

#### **Nesamoumevne metode:**

* void dodajAktivnosti()
 
    * imena in tipi parametrov: ime aktivnosti(String), sodelujoči (List), datumVpisa(Date), datumDo(Date), idKanala(int)(samo profesor in administrator fakultete)
    * tip rezultata: / (void)
    
* void izbrišiAktivnosti()
 
    * imena in tipi parametrov: id aktivnosti (int), ki bo izbrisana, ime aktivnosti(String), sodelujoči (List), datumVpisa(Date), datumDo(Date)
    * tip rezultata: / (void)
  
* void posodobiAktivnost()
 
    * imena in tipi parametrov: id aktivnosti (int), ki bo urejana, ime aktivnosti(String), sodelujoči (List), datumVpisa(Date), datumDo(Date), idKanala(int)(samo profesor in administrator fakultete)
    * tip rezultata: / (void)
  
* void dodajSinhroniziraneAktivnosti()
 
    * imena in tipi parametrov: /
    * tip rezultata: / (void)
    
___

## 3. Načrt obnašanja
Za naš sistem smo predvideli 21 funkcionalnosti, ki smo jih predstavili z diagramom zaporedja, diagramom stanj, diagramom aktivnosti ali diagramom sodelovanja. Pri vsaki so pokriti tudi alternativni tokovi in izjemni deli, do katerih lahko pride tekom uporabe aplikacije.
Naš načrt obnašanja tako predstavljajo naslednji diagrami in njihovi opisi:

### **3.1 Registracija**
Diagram zaporedja prikazuje osnovni in alternativni tok funkcionalnosti *Registracija*.

![](../img/registracijaDiagZap.PNG)

Za lažjo preglednost smo izjemne tokove prikazali posebej, le-ti so razvidni na ***diagramu aktivnosti***.

![](../img/Registracija-diagramaktivnosti.png)

### **3.2 Prijava v sistem**

Diagram zaporedja prikazuje funkcionalnost *Prijava v sistem*. 
*Zanka* predstavlja izjemni tok, v katerem se uporabnik znajde, kadar podatki za prijavo niso pravilni oz. eno izmed polj za prijavo ni izpolnjeno.

![](../img/Prijava.png)

### **3.3 Odjava iz sistema**

Diagram zaporedja prikazuje funkcionalnost *Odjava iz sistema*.

![](../img/odjava.png)

### **3.4 Pregled koledarja**
Diagram zaporedja prikazuje funkcionalnost *Pregled koledarja*. 

!![](../img/Pregledkoledarja.png)

### **3.5 TO-DO list**

Diagram zaporedja prikazuje funkcionalnost *TO-DO list*. 

![](../img/TO-DOlist.png)

### **3.6 Vnos aktivnosti**
Diagram zaporedja prikazuje funkcionalnost *Vnos aktivnosti* za osnovni tok.

![](../img/vnosAktivnosti.png)

Diagram zaporedja prikazuje funkcionalnost *Vnos aktivnosti*  za alternativni tok.

![](../img/vnosAktivnostiAlternativni.png)

V obeh primerih *zanka* predstavlja izjemni tok. V zanki se uporabnik znajde, kadar ne izbere tipa aktivnosti ali ne izpolni vseh zahtevanih podatkov.

### **3.7 Brisanje aktivnosti**

Diagram zaporedja prikazuje funkcionalnost *Brisanje aktivnosti*. 

![](../img/brisanjeaktivnosti.png)

### **3.8 Urejanje aktivnosti**
Diagram zaporedja prikazuje funkcionalnost *Urejanje aktivnosti*.

![](../img/urejanjeAktivnosti.png)

Diagram zaporedja prikazuje funkcionalnost *Urejanje aktivnosti* z alternativnim tokom.

![](../img/urejanjeAktivnostiAlternativni.png)

Diagram zaporedja prikazuje funkcionalnost *Urejanje aktivnosti* z alternativnim tokom alternativnega toka.

![](../img/urejanjeAktivnostiALternativniTokAlternativnega.png)

*Zanka* predstavlja izjemni tok, v katerem se uporabnik znajde, kadar ne izbere tipa aktivnosti ali ne izpolni vseh zahtevanih polj.

### **3.9 Prijava uporabnika na obveščanje o aktivnostih**
Diagram zaporedja prikazuje funkcionalnost *Prijava na obveščanje o aktivnostih*.

![](../img/Prijavauporabnikanaobveščanjeoaktivnostih.png)

### **3.10 Odjava uporabnika od obveščanja o aktivnostih**
Diagram zaporedja prikazuje funkcionalnost *Odjava uporabnika od obveščanja o aktivnostih*.


![](../img/Odjavauporabnikaodobveščanjaoaktivnostih.png)

### **3.11 Sinhronizacija aktivnosti z Google koledarjem**

Diagram zaporedja predstavlja funkcionalnost *Sinhronizacija aktivnosti z Google koledarjem*.
*Uspešnost prijave* deluje kot zanka - dokler prijava ni uspešna, bo sistem prijave vračal obvestilo, da prijava ni uspešna. V primeru uspešne prijave zunanji sistem vrne avtorizacijsko kodo.

![](../img/SinhronizacijazGoogleračunom.png)

Za dodatno razlago interakcije z zunanjim sistemom smo to funkcionalnost predstavili tudi z ***diagramom sodelovanja***.

![](../img/DiagramsodelovanjazzunanjimAPIjem.png)

### **3.12 Dodajanje prijatelja**

Diagram zaporedja prikazuje funkcionalnost *Dodajanje prijateljev* z osnovnim in alternativnim tokom.

![](../img/dodajPrijatelja.png)

Za boljšo predstavo naše izjeme, ko iskanega študenta ni na seznamu, da bi ga drug študent dodal med prijatelje, smo uporabili še ***diagram stanj***. Ta podrobno prikazuje, kaj se dogaja med funkiconalnostjo dodajanja prijatelja in v katerih stanjih čaka, se konča uspešno ali neuspešno.

![](../img/diagramstanj-dodajanjeprijatelja.png)

### **3.13 Odstranjevanje prijatelja**

Diagram zaporedja prikazuje funkcionalnost *Odstranjevanje prijateljev* 

![](../img/odstraniPrijatelja.png)

Izjemnega toka v diagramu zaporedja nismo upoštevali, saj smo ugotovili, da do izbrisa željenega prijatelja, ki ga ni na seznamu ne more pripeljati, saj to, da prijatelja ni na seznamu pomeni, da sploh ni tvoj prijatelj.

### **3.14 Ustvarjanje kanala**
Diagram zaporedja prikazuje funkcionalnost *Ustvarjanje kanala*. Zanka predstavlja postopek, ki ga ponovimo pri vsakem ustvarjanju kanala.

![](../img/Ustvarjanjekanala.png)

### **3.15 Seznam kanalov**
Diagram zaporedja prikazuje funkcionalnost *Seznam kanalov*.

![](../img/Seznamkanalov.png)

### **3.16 Prijava na kanal**
Diagram zaporedja prikazuje funkcionalnost *Prijava na kanal*.

![](../img/Prijavanakanal.png)

### **3.17 Odjava od kanala**
Diagram zaporedja prikazuje funkcionalnost *Odjava od kanala*.

![](../img/odjavaSKanala.png)

### **3.18 Pregledovanje zahtev za višje vloge in dodeljevanje le-teh**

Diagram zaporedja prikazuje funkcionalnost *Pregledovanje zahtev za višje vloge in dodeljevanje le-teh.*

![](../img/pregledovanjeZahtevZaVisjeVloge.png)

Diagram pokrije oba scenarija, torej če je prošnja potrjena/odobrena ali ne.

### **3.19 Brisanje uporabnikov**
V primeru funkcionalnosti *Brisanje uporabnikov* gre za Won't have this time funkcionalnost, za katero smo že vnaprej predvideli, da bi preveč povečala kompleksnost implementacije in povzročila neskladnost s trenutno predvidenimi načrti. Za potrebe te implementacije bi namreč morali predvideti hierarhijo uporabnikov, kjer bi uporabnik v posamezni vlogi lahko brisal le uporabnike, ki so po hierarhiji eno vlogo nižje od njega. Zaradi tega te logike tudi nismo predvideli v načrtu arhitekture in načrtu strukture. 

### **3.20 Dodajanje zapisov v šifrante**
Diagram zaporedja prikazuje funkcionalnost *Dodaj zapis v šifrantu fakultete*.

![](../img/DodajanjeZapisovVŠifranteFakultete.png)

Diagram zaporedja prikazuje funkcionalnost *Dodaj zapis v šifrantu tipa aktivnosti*.

![](../img/DodajanjeZapisovVŠifranteTipaAktivnosti.png)

Zanka 'vsak nov zapis' v obeh diagramih pomeni, da se ta del izvede vsakič, ko glavni administrator izbere možnost dodajanja novega zapisa v šifrantu z *Nov zapis*, se pravi tudi takrat, ko jih želi vnesti več zaporedoma.

### **3.21 Urejanje zapisov v šifrantih**
Diagram zaporedja prikazuje funkcionalnost *Urejanje zapisa v šifrantu fakultete*. 

![](../img/UrejanjeZapisaVŠifrantuFakultete.png)

Diagram zaporedja prikazuje funkcionalnost *Urejanje zapisa v šifrantu tipa aktivnosti*. 

![](../img/urejanjeZapisaVŠifrantuAktivnosti.png)

Zanka 'vsak izbran zapis' v obeh diagramih pomeni, da se ta del izvede vsakič, ko glavni administrator izbere možnost urejanja zapisa v šifrantu z *Uredi*, se pravi tudi, če jih želi urediti več zaporedoma.

Diagram zaporedja prikazuje alternativni tok funkcionalnosti *Urejanje zapisa v šifrantu fakultet*. 

![](../img/urejanjeZapisaVšifrantuAlternativniTokFakultete.png)

Diagram zaporedja prikazuje alternativni tok funkcionalnosti *Urejanje zapisa v šifrantu tipa aktivnosti*. 

![](../img/urejanjeZapisaVŠifrantuAlternativniTok.png)

### **3.22 Brisanje zapisov v šifrantih**
Diagram zaporedja prikazuje funkcionalnost *Izbris zapisa v šifrantu fakultete*. 

![](../img/BrisanjeZapisovVŠifrantuFakultete.png)

Diagram zaporedja prikazuje funkcionalnost *Izbris zapisa v šifrantu tipa aktivnosti*.

![](../img/BrisanjeZapisovVŠifrantuTipaaktivnosti.png)

Zanka 'vsak izbran zapis' v obeh diagramih pomeni, da se ta del izvede vsakič, ko glavni administrator izbere možnost izbrisa zapisa v šifrantu z *Izbriši*, se pravi tudi, če jih želi izbrisati več zaporedoma.
_______

### Reference
[1] Opis delovanja OAuth prijave. https://developers.google.com/identity/protocols/OAuth2

[2] Risanje diagramov. https://www.draw.io

[3]: Lavbič, D. Tehnologija programske opreme 2018/2019. https://teaching.lavbic.net/TPO/, 2019.

[4]: Google Calendar API. https://developers.google.com/calendar/