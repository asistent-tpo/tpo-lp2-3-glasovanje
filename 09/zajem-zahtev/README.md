# Dokument zahtev

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Arne Simonič, Miha Benčina, Janko Knez, Aljaž Švigelj |
| **Kraj in datum** | Ljubljana, 6.4.2019 |



## Povzetek projekta

Ta dokument natančno opredeljuje vse zahteve, ki morajo veljati za izbran projekt. Gre za spletno aplikacijo, ki študentom omogoča učinkovitejšo organizacijo študijskih obveznosti. V uvodu je na kratko opisana problemska 
domena, ki jo rešujemo. Pod naslovom uporabniške vloge so navedene ter opisane vse vloge, ki bodo nastopale v spletni aplikaciji. Vsi pojmi, ki bi lahko za bralca predstavljali dvoumnost, so navedeni v slovarju. Da bi si 
lažje predstavljali, kako uporabnik upravlja s sistemom, je naveden tudi diagram primerov uporabe, prav tako pa so natančno opisane funkcionalne zahteve, ki opisujejo vse funkcionalnosti sistema, ter nefunkcionalne zahteve, 
ki opisujejo zahteve zunanjih dejavnikov. Poleg teh so navedeni tudi vmesniki do zunanjih sistemov, ki jih bo uporabljala naša spletna aplikacija. Za lažjo grafično predstavo pa so na voljo tudi osnutki zaslonskih mask.

## 1. Uvod

Organizacija študijskih obveznosti se mogoče zdi trivialna, vendar študentom še danes predstavlja težave. Vsak študent se lahko “pohvali” z vsaj enim, če ne celo z večimi zamujenimi roki pri oddaji nalog. Nič kaj redko se 
tudi zgodi, da študenti prespijo predavanja ali vaje, saj niti ne vedo, da jih imajo. Po drugi strani pa seveda študentje vedo, kaj imajo za opraviti ter do kdaj, ne znajo pa si teh obveznosti časovno razporediti. 
Vse našteto vodi v zelo neučinkovito porabo časa ter precej pripomore k nabiranju stresa, kar zelo negativno vpliva na potek študija. Ko bi le obstajalo sredstvo za odpravo vseh teh nevšečnosti.

StraightAs je spletna aplikacija, ki odpravlja vse zgoraj naštete težave, omogoča učinkovito in enostavno organizacijo študijskih in tudi ostalih obveznosti ter posledično pozitivno vpliva na kakovost študija. Z uporabo 
aplikacije bodo študentje prihranili veliko časa pri organizaciji študija, ta čas pa bodo lahko investirali kar v študij sam, ali pa v sprostitev za večjo svežino med pedagoškim procesom.

Glavni funkcionalnosti spletne aplikacije bosta koledar in urnik. Poleg teh pa bodo na voljo tudi manjše funkcionalnosti, ki bodo popestrile spletno aplikacijo ter ji priznale dodano vrednost. Koledar je bistvenega pomena 
pri spletni aplikaciji, saj bo služil kot vsebnik vseh terminsko naravnanih obveznosti. V koledar bo mogoče vnesti nov dogodek, izbrisati in urediti obstoječ dogodek, mogoč pa bo seveda tudi pregled vseh še aktualnih 
dogodkov. Dogodki so lahko različnih vrst, vse od kolokvijev in izpitov do srečanj s prijatelji. Mogoča bo tudi interakcija (uvoz/izvoz) z Google Calendar, kar omogoča učinkovit prehod na spletno aplikacijo.

Študenti bodo vse predmete lahko videli na urniku. Študentom bo omogočen pregled urnika, profesorji pa bodo lahko upravljali s svojimi predmeti. Nove predmete bodo lahko dodajali, spreminjali obstoječe predmete ter brisali 
stare. K spreminjanju predmeta spada spreminjanje terminov predavanj in vaj, prav tako pa bodo lahko dodajali ter spreminjali termine kolokvijev, pisnih ter ustnih izpitov. V kombinaciji s koledarjem bo študentom predlagan 
možen plan dela, aplikacija pa bo implementirala tudi priporočilni sistem, ki bo študentu na podlagi vremenske napovedi predlagal medij prevoza na fakulteto.

Spletna aplikacija bo morala implementirati vsaj vloge študent, profesor in skrbnik, saj so to poglavitne vloge pri spletni aplikaciji. Študenti se bodo morali prijaviti z elektronsko pošto, ki jim jo dodeli univerza ob 
vpisu na fakulteto. Prav tako študenti v koledar ne bodo smeli vnašati kletvic, saj to nasprotuje etični kulturi Slovenije. Z uvedbo GDPR (General Data Protection Regulation / Splošna uredba EU o varstvu podatkov) 
bo potrebno paziti na vse restrikcije, ki jih predpisuje. Ena izmed njih ter najbolj pomembna je dostop do podatkov, kjer uporabnik ne sme videti podatkov drugih uporabnikov. Prav tako pa si želimo dobre 
rezultate v hkratnosti uporabe spletne aplikacije, saj študenti radi stvari počnejo zadnji trenutek - v nedeljo zvečer.

## 2. Uporabniške vloge

### Neregistriran uporabnik  
Se lahko registrira v sistem kot študent.  
### Študent  
Lahko upravlja z dogodki v koledarju, v katerem lahko vidi tudi urnike predmetov in vreme. Prav tako lahko svoj koledar uvozi in izvozi v Google calendar.  
### Profesor  
Upravlja z urniki svojih predmetov, ki jih lahko dodaja in z njimi upravlja.  
### Skrbnik  
Briše uporabnike in dodaja profesorje.  


## 3. Slovar pojmov

**Neregistriran uporabnik** - oseba, ki še nima dostopa do aplikacije  
**Uporabnik** - študent ali profesor  
**Študent** - vrsta uporabnika (definiran v uporabniških vlogah)  
**Profesor** - vrsta uporabnika (definiran v uporabniških vlogah)  
**Skrbnik** - oseba, ki ima možnost dodajanja profesorjev in brisanja uporabnikov  
**Sistem** - celotna aplikacija  
**Google Calendar API** - zunanji sistem za dostopanje do Google Calendar-ja  
**OpenWeatherMap** - API za dostop do podatkov o vremenu  
**Google Calendar** - aplikacija s koledarjem, ki jo ponuja Google  
**Predmet** - učno področje, za katerega profesor, ki skrbi zanj, ureja urnik  
**Urnik** - spisek rezerviranih terminov v tednu za obveznosti predmetov  
**Obveznost** - enota na koledarju, ki ima neko trajanje in se navezuje na nek predmet  
**Dogodek** - enota na koledarju, ki ima neko trajanje in z njim upravlja študent  
**Koledar** - sistematična razdelitev leta na dneve, tedne in mesece, ki vsebuje dogodke  
**Plan dela** - dogodki razvrščeni glede na datum in prioriteto  


## 4. Diagram primerov uporabe

![Diagram primerov uporabe](../img/diagram_primerov_uporabe.png =800x1672)


## 5. Funkcionalne zahteve

### 5.1. Registracija v sistem

#### Povzetek funkcionalnosti

Neregistriran uporabnik se lahko registrira v sistem, s čimer pridobi uporabniško ime in geslo.

#### Osnovni tok

1. Neregistriran uporabnik izbere funkcionalnost registracija.
2. Sistem prikaže polje za vnos študentskega email naslova in gesla.
3. Neregistriran uporabnik vnese svoj študentski email naslov in geslo.
4. Neregistriran uporabnik potrdi svoj vnos.
5. Neregistriran uporabnik dobi na vneseni email naslov potrditev registracije, kjer mu tudi piše njegovo uporabniško ime, ki je enako prvemu delu njegovega email naslova, do znaka @ (za ab1234@student.uni-lj.si je ab1234).
6. Neregistriran uporabnik potrdi svoj email naslov.
7. Sistem prikaže sporočilo o uspešni registraciji.

#### Alternativni tok

Ta funkcionalnost nima alternativnih tokov.

#### Izjemni tokovi

1. Neregistriran uporabnik vnese šibko geslo.
2. Neregistriran uporabnik vnese email naslov, ki ni podan s strani izobraževalne ustanove.

#### Pogoji

Ob izvajanju registracije ne sme biti uporabnik ali skrbnik že prijavljen v uporabljenem brskalniku.


#### Posledice

Neregistriran uporabnik postane študent, pri čemer mu je dodeljeno uporabniško ime in geslo za dostop.


#### Posebnosti

Ob registraciji se na strežniku v dnevnik shranijo določeni podatki o dogodku.

#### Prioritete identificiranih funkcionalnosti

Po metodi MoSCoW dajemo funkcionalni zahtevi prioriteto MUST have.

#### Sprejemni testi

| Funkcija     | začetno stanje    | vhod | Pričakovan izhod |
| --------|---------|-------|-------|
| Uspešna registracija  | Nahajanje na začetni strani z odprto registracijo. | Vnos študentskega email naslova in varnega gesla. | Uspešna registracija v sistem. |
| Preverjanje varnosti gesla | Nahajanje na začetni strani z odprto registracijo. | Vnos študentskega email naslova in šibkega gesla.| Opozorilo o šibkem geslu in preprečevanje registracije.|
| Neveljaven email naslov | Nahajanje na začetni strani z odprto registracijo. | Vnos email naslova, ki ni podan s strani izobraževalne ustanove. | Opozorilo o neveljavnem email naslovu in preprečitev registracije. |


### 5.2. Prijava v sistem

#### Povzetek funkcionalnosti

Uporabnik ali skrbnik se lahko prijavi v sistem z dodeljenim uporabniškim imenom in geslom.

#### Osnovni tok

1. Uporabnik ali skrbnik izbere funkcionalnost prijava.
2. Sistem prikaže polji za vnos uporabniškega imena in gesla.
3. Uporabnik ali skrbnik izpolni prikazani polji.
4. Uporabnik ali skrbnik potrdi svoj vnos.
5. Sistem preveri pravilnost polj.
6. Sistem preusmeri študenta na njegov koledar dogodkov, profesorja na seznam njegovih predmetov, skrbnika pa na stran s seznamom uporabnikov.


#### Alternativni tok

Ta funkcionalnost nima alternativnih tokov.

#### Izjemni tokovi

1. Uporabnik ali skrbnik vnese neobstoječe uporabniško ime.
2. Uporabnik ali skrbnik vnese napačno geslo.


#### Pogoji

Ob izvajanju prijave ne sme biti uporabnik ali skrbnik že prijavljen v uporabljenem brskalniku.


#### Posledice

Uporabnik ali skrbnik je sedaj prijavljen v sistem in je preusmerjen glede na njegovo vlogo.


#### Posebnosti

Ta funkcionalnost nima posebnosti.

#### Prioritete identificiranih funkcionalnosti

Po metodi MoSCoW dajemo funkcionalni zahtevi prioriteto MUST have.

#### Sprejemni testi

| Funkcija     | začetno stanje    | vhod | Pričakovan izhod |
| --------|---------|-------|-------|
| Uspešna prijava  | Nahajanje na začetni strani z odprto prijavo. | Vnos veljavnega uporabniškega imena in gesla.  | Prijava v sistem. |
| Preverjanje uporabniškega imena | Nahajanje na začetni strani z odprto prijavo. | Vnos neobstoječega uporabniškega imena. | Zavrnitev prijave.|
| Preverjanje pravilnosti gesla | Nahajanje na začetni strani z odprto prijavo. | Vnos nepravilnega gesla pripadajočemu uporabniškemu imenu. | Zavrnitev prijave. |


### 5.3. Odjava iz sistema

#### Povzetek funkcionalnosti

Uporabnik ali skrbnik se lahko odjavi iz sistema, pri čemer sedaj nima več dostopa iz uporabljenega brskalnika.

#### Osnovni tok

1. Uporabnik ali skrbnik izbere funkcionalnost odjava.
2. Sistem odjavi uporabnika ali skrbnika iz uporabljenega brskalnika.
3. Uporabnik ali skrbnik je preusmerjen na začetno stran.


#### Alternativni tok

Alternativni tok 1

1. Uporabnik ali skrbnik zapre brskalnik.
2. Sistem izgubi povezavo in odjavi uporabnika ali skrbnika.


#### Izjemni tokovi

1. Uporabnik ali skrbnik izgubno internetno povezavo, posledično je odjavljen iz sistema.

#### Pogoji

V sistem mora biti prijavljen uporabnik ali skrbnik.


#### Posledice

Uporabnik ali skrbnik ni več prijavljen v sistem.


#### Posebnosti

Ta funkcionalnost nima posebnosti.

#### Prioritete identificiranih funkcionalnosti

Po metodi MoSCoW dajemo funkcionalni zahtevi prioriteto MUST have.

#### Sprejemni testi

| Funkcija     | začetno stanje    | vhod | Pričakovan izhod |
| --------|---------|-------|-------|
| Uspešna odjava  | Prijavljen uporabnik ali skrbnik. | Klik na gumb odjava. | Uporabnik ali skrbnik ni več prijavljen v sistem. |
| Zaprtje brskalnika | Prijavljen uporabnik ali skrbnik. | Uporabnik ali skrbnik zapre brskalnik brez predhodne odjave. | Uporabnik ali skrbnik ni več prijavljen v sistem.|
| Izguba povezave | Prijavljen uporabnik ali skrbnik. | Prekinitev internetne povezave iz uporabljenega računalnika. | Uporabnik ali skrbnik ni več prijavljen v sistem. |


### 5.4. Brisanje uporabnikov

#### Povzetek funkcionalnosti

Skrbnik lahko izbriše uporabnika iz sistema, ki sedaj nima več dostopa.

#### Osnovni tok

1. Skrbnik izbere uporabnike, ki jih želi izbrisati iz sistema.
2. Skrbnik izbere funkcionalnost brisanja uporabnikov.
3. Skrbnik potrdi izbiro.
4. Sistem izbriše izbrane uporabnike.


#### Alternativni tok

Ta funkcionalnost nima alternativnih tokov.

#### Izjemni tokovi

1. Uporabnik je v času brisanja prijavljen, kar lahko privede do napak.

#### Pogoji

V sistemu mora obstajati uporabnik, ki ga želimo izbrisati.

#### Posledice

Uporabnik nima več dostopa do sistema z dodeljenim uporabniškim imenom in geslom.


#### Posebnosti

Ob brisanju se na strežniku v dnevnik shranijo določeni podatki o dogodku.

#### Prioritete identificiranih funkcionalnosti

Po metodi MoSCoW dajemo funkcionalni zahtevi prioriteto SHOULD have.

#### Sprejemni testi

| Funkcija     | začetno stanje    | vhod | Pričakovan izhod |
| --------|---------|-------|-------|
| Uspešno brisanje  | Uporabnik še ni izbrisan iz sistema. | Skrbnik izbriše izbranega uporabnika. | Uporabnik nima več dostopa do sistema. |
| Brisanje prijavljenega uporabnika | Uporabnik, ki ga skrbnik želi izbrisati je trenutno prijavljen. | Skrbnik izbriše izbranega uporabnika.| Uporabnik nima več dostopa in je preusmerjen na začetno stran.|


### 5.5. Dodajanje dogodkov v koledar

#### Povzetek funkcionalnosti

Študent lahko dodaja v svoj koledar nov dogodek, kjer mu dodeli datum, ime, opis in prioriteto.

#### Osnovni tok

1. Študent izbere funkcionalnost dodajanja dogodkov.
2. Sistem prikaže potrebna vnosna polja.
3. Študent dodeli dogodku datum, ime, opis in prioriteto.
4. Študent potrdi ustvarjanje dogodka.
5. Sistem doda novi dogodek v koledar.


#### Alternativni tok

Ta funkcionalnost nima alternativnih tokov.

#### Izjemni tokovi

1. Študent želi dodati dogodek, ki ni poimenovan.
2. Študent želi dodati dogodek, ki nima prioritete.
3. Študent želi dodati dogodek, ki nima datuma.


#### Pogoji

Študent mora biti prijavljen v sistem.


#### Posledice

Opis dogodka ne sme biti obvezen s strani dodajanja dogodkov.


#### Posebnosti

Ob registraciji se na strežniku v dnevnik shranijo določeni podatki o dogodku.

#### Prioritete identificiranih funkcionalnosti

Po metodi MoSCoW dajemo funkcionalni zahtevi prioriteto MUST have.

#### Sprejemni testi

| Funkcija     | začetno stanje    | vhod | Pričakovan izhod |
| --------|---------|-------|-------|
| Uspešno dodajanje dogodkov  | Študent je prijavljen v sistem. | Dodajanje dogodka z datumom, imenom, vsebino in prioriteto. | Dogodek je dodan v koledar študenta. |
| Manjkajoče ime  | Študent je prijavljen v sistem. | Dodajanje dogodka, ki mu manjka ime.| Opozorilo o manjkajočem imenu in preprečitev ustvarjanja dogodka.|
| Manjkajoča prioriteta | Študent je prijavljen v sistem.| Dodajanje dogodka, ki mu manjka prioriteta. | Opozorilo o manjkajoči prioriteti in preprečitev ustvarjanja dogodka. |
| Manjkajoči datum | Študent je prijavljen v sistem.| Dodajanje dogodka, ki mu manjka datum. | Opozorilo o manjkajočem datumu in preprečitev ustvarjanja dogodka. |

### 5.6. Brisanje dogodkov iz koledarja

#### Povzetek funkcionalnosti

Študent lahko iz koledarja izbriše dogodek, ki se mu sedaj ne bo več prikazal v koledarju.

#### Osnovni tok

1. Študent izbere dogodek v koledarju.
2. Sistem prikaže podrobnosti in nastavitve dogodka.
3. Študent izbere brisanje dogodka.
4. Študent potrdi izbiro.
5. Sistem izbriše izbrani dogodek iz sistema.


#### Alternativni tok

Ta funkcionalnost nima alternativnih tokov.

#### Izjemni tokovi

1. Sistem izbriše napačen dogodek.

#### Pogoji

Študent mora biti prijavljen v sistem.  
Dogodek, ki ga študent želi izbrisati, mora obstajati.



#### Posledice

Izbrani dogodek se izbriše iz sistema in se ne prikazuje več v koledarju dogodkov.

#### Posebnosti

Ta funkcionalnost nima posebnosti.

#### Prioritete identificiranih funkcionalnosti

Po metodi MoSCoW dajemo funkcionalni zahtevi prioriteto SHOULD have.

#### Sprejemni testi

| Funkcija     | začetno stanje    | vhod | Pričakovan izhod |
| --------|---------|-------|-------|
| Uspešno brisanje dogodkov  | Študent je prijavljen v sistem. | Zahteva po brisanju dogodka. | Izbrani dogodek je izbrisan iz sistema. |


### 5.7. Spreminjanje dogodkov v koledarju

#### Povzetek funkcionalnosti

Študent lahko obstoječemu dogodku v koledarju spremeni datum, ime, opis ali prioriteto.

#### Osnovni tok

1. Študent izbere dogodek v koledarju
2. Sistem prikaže vsebino dogodka.
3. Študent spremeni datum, ime, opis ali prioriteto.
4. Študent potrdi spremembe dogodka.
5. Sistem posodobi dogodek.


#### Alternativni tok

Ta funkcionalnost nima alternativnih tokov.

#### Izjemni tokovi

1. Sistem ne posodobi dogodka. 
2. Sistem posodobi napačen dogodek.

#### Pogoji

Študent mora biti prijavljen v sistem.  
Dogodek, ki ga študent želi spremeniti, mora obstajati.

#### Posledice

Izbrani dogodek se spremeni glede na študentove spremembe.

#### Posebnosti

Ta funkcionalnost nima posebnosti.

#### Prioritete identificiranih funkcionalnosti

Po metodi MoSCoW dajemo funkcionalni zahtevi prioriteto MUST have.

#### Sprejemni testi

| Funkcija     | začetno stanje    | vhod | Pričakovan izhod |
| --------|---------|-------|-------|
| Uspešno spreminjanje dogodkov  | Študent je prijavljen v sistem. | Zahteva po spremembi dogodka. | Izbrani dogodek je ustrezno spremenjen.|



### 5.8. Ogled koledarja

#### Povzetek funkcionalnosti

Študent si lahko ogleda koledar svojih dogodkov.

#### Osnovni tok

1. Študent izbere funkcionalnost ogleda koledarja.
2. Sistem zbere dogodke, ki pripadajo študentu.
3. Sistem študent prikaže njegov koledar dogodkov.

#### Alternativni tok

Ta funkcionalnost nima alternativnih tokov.

#### Izjemni tokovi

Ta funkcionalnost nima izjemnih tokov.

#### Pogoji

Študent mora biti prijavljen v sistem.


#### Posledice

Študentu se prikaže njegov koledar z dogodki.


#### Posebnosti

Ta funkcionalnost nima posebnosti.

#### Prioritete identificiranih funkcionalnosti

Po metodi MoSCoW dajemo funkcionalni zahtevi prioriteto MUST have.

#### Sprejemni testi

| Funkcija     | začetno stanje    | vhod | Pričakovan izhod |
| --------|---------|-------|-------|
| Ogled koledarja  | Študent je prijavljen v sistem. | Zahteva po ogledu koledarja. | Študentu se prikaže njegov koledar dogodkov. |


### 5.9. Ogled možnega plana dela

#### Povzetek funkcionalnosti

Študent si lahko ogleda možen plan dela, ki mu ga predlaga sistem.

#### Osnovni tok

1. Študent izbere funkcionalnost plana dela.
2. Sistem prihajajoče neopravljene dogodke uredi glede na datum in prioriteto.
3. Sistem prikaže rezultate urejanja, ki predstavljajo možni plan dela.


#### Alternativni tok

Ta funkcionalnost nima alternativnih tokov.

#### Izjemni tokovi

1. Dogodkov, ki se morajo zgoditi prvi je preveč in jih ni mogoče prikazati hkrati.

#### Pogoji

Študent mora biti prijavljen v sistem.  
Študent mora imeti neopravljene dogodke v koledarju.

#### Posledice

Sistem prikaže študentu možen plan dela.

#### Posebnosti

Ta funkcionalnost nima posebnosti.

#### Prioritete identificiranih funkcionalnosti

Po metodi MoSCoW dajemo funkcionalni zahtevi prioriteto COULD have.

#### Sprejemni testi

| Funkcija     | začetno stanje    | vhod | Pričakovan izhod |
| --------|---------|-------|-------|
| Ogled možnega plana dela  | Študent je prijavljen v sistem. | Študent izbere funkcionalnost ogleda možnega plana dela. | Prikaže se možni plan dela upoštevajoč datumov dogodkov in njihovi prioriteti. |

### 5.10. Dodajanje profesorjev

#### Povzetek funkcionalnosti

Skrbnik lahko v sistem doda nove profesorje.

#### Osnovni tok

1. Skrbnik izbere funkcionalnost dodajanja profesorjev.
2. Sistem prikaže vnosna polja za ime profesorja, uporabniško ime in geslo.
3. Skrbnik izpolni prikazana vnosna polja.
4. Skrbnik potrdi vnos.
5. Sistem doda novega profesorja v sistem.


#### Alternativni tok

Ta funkcionalnost nima alternativnih tokov.

#### Izjemni tokovi

1. Profesor z vnesenim uporabniškim imenom že obstaja.

#### Pogoji

Skrbnik je prijavljen v sistem.

#### Posledice

Profesor ima sedaj dostop do sistema preko uporabniškega imena in gesla, ki ju je vnesel skrbnik.

#### Posebnosti

Ob dodajanju profesorjev se na strežniku v dnevnik shranijo določeni podatki o dogodku.

#### Prioritete identificiranih funkcionalnosti

Po metodi MoSCoW dajemo funkcionalni zahtevi prioriteto SHOULD have.

#### Sprejemni testi

| Funkcija     | začetno stanje    | vhod | Pričakovan izhod |
| --------|---------|-------|-------|
| Uspešno dodajanje profesorjev  | Skrbnik je prijavljen v sistem. | Skrbnik izbere funkcionalnost dodajanja profesorjev in vnese podatke ter potrdi. | Skrbnik izbere funkcionalnost dodajanja profesorjev in vnese podatke ter potrdi. |
| Uporabniško ime že uporabljeno | Skrbnik je prijavljen v sistem. | Skrbnik izbere funkcionalnost dodajanja profesorja in vnese že uporabljeno uporabniško ime. | Sistem opozori o ponavljajočem uporabniškem imenu in prepreči dodajanje. |



### 5.11. Dodajanje predmetov

#### Povzetek funkcionalnosti

Profesor lahko v zbirko svojih predmetov doda nov predmet.

#### Osnovni tok

1. Profesor izbere funkcionalnost dodajanja predmetov.
2. Sistem prikaže vnosna polja za ime, opis, termin predavanj, termine vaj, termine kolokvijev in termine izpitov.
3. Profesor izpolni prikazana vnosna polja.
4. Profesor potrdi izbiro.
5. Sistem v seznam predmetov doda predmet.


#### Alternativni tok

Ta funkcionalnost nima alternativnih tokov.

#### Izjemni tokovi

1. Predmet, ki ga želi profesor dodati je že v njegovem seznamu predmetov.

#### Pogoji

Profesor je prijavljen v sistem.

#### Posledice

V seznam predmetov se doda novi urnik za izbrani predmet.

#### Posebnosti

Ta funkcionalnost nima posebnosti.

#### Prioritete identificiranih funkcionalnosti

Po metodi MoSCoW dajemo funkcionalni zahtevi prioriteto SHOULD have.

#### Sprejemni testi

| Funkcija     | začetno stanje    | vhod | Pričakovan izhod |
| --------|---------|-------|-------|
| Uspešno dodajanje predmeta  | Profesor je prijavljen v sistem. | Vnos študentskega email naslova in varnega gesla. | V seznam predmetov je dodan novi predmet z vnesenimi podatki.|
| Predmet že obstaja | Skrbnik je prijavljen v sistem. | Profesor izbere funkcionalnost dodajanja predmeta in obstoječe ime predmeta.| Sistem opozori o že obstoječem predmetu in prepreči dodajanje.|

### 5.12. Urejanje predmetov

#### Povzetek funkcionalnosti

Profesor lahko ureja svoje predmete, ki jih je v preteklosti dodal v sistem.

#### Osnovni tok

1. Profesor izbere funkcionalnost urejanja predmeta.
2. Profesor izbere predmet, ki ga želi urediti.
3. Sistem prikaže vnosna polja za ime, opis, termin predavanj, termine vaj, termine kolokvijev in termine izpitov.
4. Profesor spremeni vsebino prikazanih polj.
5. Profesor potrdi izbiro.
6. Sistem shrani spremembe predmeta.


#### Alternativni tok

Ta funkcionalnost nima alternativnih tokov.

#### Izjemni tokovi

Ta funkcionalnost nima izjemnih tokov.

#### Pogoji

Profesor je prijavljen v sistem.

#### Posledice

Izbrani predmet profesorja se spremeni glede na vnesene spremembe.

#### Posebnosti

Ob registraciji se na strežniku v dnevnik shranijo določeni podatki o dogodku.

#### Prioritete identificiranih funkcionalnosti

Po metodi MoSCoW dajemo funkcionalni zahtevi prioriteto SHOULD have.

#### Sprejemni testi

| Funkcija     | začetno stanje    | vhod | Pričakovan izhod |
| --------|---------|-------|-------|
| Uspešno urejanje predmeta  | Profesor je prijavljen v sistem. | Profesor izbere funkcionalnost urejanja predmeta in spremeni obstoječe vrednosti. | Sistem posodobi spremenjena polja izbranega predmeta. |


### 5.13. Brisanje predmetov

#### Povzetek funkcionalnosti

Profesor lahko odstrani predmet iz svoje zbirke predmetov.

#### Osnovni tok

1. Profesor izbere funkcionalnost urejanja predmeta.
2. Sistem prikaže poleg vnosnih polj možnost brisanja.
3. Profesor zahteva izbris predmeta.
4. Sistem izbriše predmet iz seznama predmetov profesorja.


#### Alternativni tok

Ta funkcionalnost nima alternativnih tokov.

#### Izjemni tokovi

Ta funkcionalnost nima izjemnih tokov.

#### Pogoji

Profesor je prijavljen v sistem.


#### Posledice

Izbrani predmet je odstranjen iz seznama predmetov profesorja.


#### Posebnosti

Ta funkcionalnost nima posebnosti.

#### Prioritete identificiranih funkcionalnosti

Po metodi MoSCoW dajemo funkcionalni zahtevi prioriteto SHOULD have.

#### Sprejemni testi

| Funkcija     | začetno stanje    | vhod | Pričakovan izhod |
| --------|---------|-------|-------|
| Uspešno brisanje predmeta  | Profesor je prijavljen v sistem. | Profesor izbere funkcionalnost brisanja predmeta. | Iz seznama predmetov profesorja se izbriše izbrani predmet. |

### 5.14. Ogled predmetov

#### Povzetek funkcionalnosti

Študent si lahko ogleda predmete, ki so jih profesorji dodali.

#### Osnovni tok

1. Študent izbere funkcionalnost ogleda predmetov.
2. Sistem prikaže seznam vseh predmetov, ki so jih vnesli profesorji.
3. Študent izbere predmet, ki ga želi videti.
4. Sistem prikaže opis, ure predavanj, vaj, kolokvijev in izpitov predmeta.


#### Alternativni tok

Ta funkcionalnost nima alternativnih tokov.

#### Izjemni tokovi

Ta funckionalnist nima izjemnih tokov.

#### Pogoji

Študent je prijavljen v sistem.  
Predmet, ki si ga študent želi ogledati, mora biti vnesen s strani profesorja.

#### Posledice

Ta funkcionalnost nima posebnosti.


#### Posebnosti

Ob registraciji se na strežniku v dnevnik shranijo določeni podatki o dogodku.

#### Prioritete identificiranih funkcionalnosti

Po metodi MoSCoW dajemo funkcionalni zahtevi prioriteto SHOULD have.

#### Sprejemni testi

| Funkcija     | začetno stanje    | vhod | Pričakovan izhod |
| --------|---------|-------|-------|
| Ogled predmeta | Študent je prijavljen v sistem in se nahaja na seznamu predmetov. | Študent izbere predmet, ki ga želi videti. | Študentu se prikaže opis, ure predavanj, vaj, kolokvijev in izpitov predmeta. |


### 5.15. Prikaz vremena za trenutni dan

#### Povzetek funkcionalnosti

Študent si lahko ogleda vreme za teden dni.

#### Osnovni tok

1. Študent izbere funkcionalnost ogleda vremena.
2. Sistem iz zunanjega sistema zahteva vreme za celotni teden.
3. Sistem študentu prikaže pridobljene podatke in na podlagi tega predlaga prevoz.


#### Alternativni tok

Alternativni tok 1

1. Študent uporabnik izbere funkcionalnost ogleda vremena.
2. Sistem prikaže povezavo do vremena glede na njegovo lokacijo.
3. Študent odpre povezavo na zunanjo spletno stran z vremenom.


#### Izjemni tokovi

1. Strežnik, ki streže podatke o vremenu preko vmesnika je začasno nedosegljiv.

#### Pogoji

Študent mora biti prijavljen v sistem.


#### Posledice

Študent se v koledarju prikaže vreme za celotni teden.


#### Posebnosti

Ta funkcionalnost nima posebnosti.

#### Prioritete identificiranih funkcionalnosti

Po metodi MoSCoW dajemo funkcionalni zahtevi prioriteto WOULD HAVE.

#### Sprejemni testi

| Funkcija     | začetno stanje    | vhod | Pričakovan izhod |
| --------|---------|-------|-------|
| Prikaz vremena v koledarju | Študent je prijavljen v sistem. | Zahteva se ogled koledarja. | Študentu se prikaže vreme za celotni teden in predlagam prevoz. |
| Nedosegljiv strežnik vremena| Študent je prijavljen v sistem, strežnik vremena pa ni dosegljiv. | Zahteva se ogled koledarja.| Sistem prikaže sporočilo, da vremena ni mogoče pridobiti.|

### 5.16. Izvoz koledarja v Google Calendar

#### Povzetek funkcionalnosti

Študent lahko izvozi koledar v Google Calendar.

#### Osnovni tok

1. Študent izbere funkcionalnost ogleda koledarja.
3. Izbere možnost izvoza koledarja v Google Calendar.
4. Sistem izvozi podatke iz koledarja v Google Calendar.


#### Alternativni tok

Alternativni tok 1  

1. Študent izbere funkcionalnost ogleda koledarja.
2. Študent zahteva izvoz koledarja v datoteko.
3. Sistem ustvari na uporabljenemu računalniku datoteko z dogodki.
5. Študent uvozi datoteko v Google Calendar.


#### Izjemni tokovi

1. Zunanji sistem trenutno ni dosegljiv.

#### Pogoji

Študent mora biti prijavljen v sistem.  
Študent mora biti prijavljen z Google računom.

#### Posledice

Koledar se iz sistema izvozi v Google Calendar.

#### Posebnosti

Pri izvozu z datoteko se mora upoštevati format, ki je kompatibilen z aplikacijo Google Calendar.

#### Prioritete identificiranih funkcionalnosti

Po metodi MoSCoW dajemo funkcionalni zahtevi prioriteto MUST have.

#### Sprejemni testi

| Funkcija     | začetno stanje    | vhod | Pričakovan izhod |
| --------|---------|-------|-------|
| Uspešen izvoz preko vmesnika | Študent je prijavljen v sistem in z Google računom. | Zahteva po izvozu v Google Calendar. | V aplikaciji Google Calendar so prikazani dogodki iz sistema. |
| Uspešen izvoz preko datoteke | Študent je prijavljen v sistem in z Google računom. | Zahteva po izvozu v datoteko in uvoz ustvarjene datoteke v Google Calendar. | V aplikaciji Google Calendar so prikazani dogodki iz sistema. |


### 5.17. Uvoz koledarja iz Google Calendar

#### Povzetek funkcionalnosti

Študent uporabnik lahko uvozi koledar iz Google Calendar v sistem.

#### Osnovni tok

1. Študent izbere funkcionalnost ogleda koledarja.
2. Izbere možnost uvoza koledarja iz Google Calendar.
3. Sistem uvozi podatke iz Google Calendar v koledar.


#### Alternativni tok

Alternativni tok 1

1. Študent izbere funkcionalnost ogleda koledarja.
2. Študent izvozi koledar iz Google Calendar.
3. Zahteva uvoz ustvarjene datoteke v koledar sistema.
4. Sistem uvozi podatke iz datoteke in doda dogodke v koledar.


#### Izjemni tokovi

1. Zunanji sistem trenutno ni dosegljiv.

#### Pogoji

Študent mora biti prijavljen v sistem.  
Študent mora biti prijavljen z Google računom.

#### Posledice

Koledar se iz Google Calendar uvozi v sistem.

#### Posebnosti

Pri uvozu datoteke mora sistem razumeti format datoteke, ki ga izvozi aplikacja Google Calendar.

#### Prioritete identificiranih funkcionalnosti

Po metodi MoSCoW dajemo funkcionalni zahtevi prioriteto MUST have.

#### Sprejemni testi

| Funkcija     | začetno stanje    | vhod | Pričakovan izhod |
| --------|---------|-------|-------|
| Uspešen uvoz preko vmesnika | Študent je prijavljen v sistem in z Google računom. | Zahteva po uvozu iz Google Calendar. | V koledarju se študentu prikažejo uvozeni dogodki. |
| Uspešen uvoz preko datoteke | Študent je prijavljen v sistem in z Google računom. | Zahteva po izvozu v datoteko iz Google Calendar in uvoz ustvarjene datoteke v sistem. | V koledarju se študentu prikažejo uvozeni dogodki.|



## 6. Nefunkcionalne zahteve

Spletna aplikacija mora zadostiti sledečim nefunkcionalnim zahtevam:

**Ime zahteve:** Uporabniške vloge  
**Tip zahteve:** Organizacijska zahteva  
**Opis zahteve:** Spletna aplikacija mora nujno implementirati vloge študent, profesor in skrbnik. To so namreč poglavitne vloge za delovanje spletne aplikacije.  
  
**Ime zahteve:** Prijava v spletno aplikacijo  
**Tip zahteve:** Organizacijska zahteva  
**Opis zahteve:** Študenti se morajo v spletno aplikacijo prijaviti z elektronsko pošto, ki jim jo dodeli univerza. Poštni naslov je oblike ab1234@student.uni-lj.si, kjer a in b predstavljata prvi črki imena in priimka, 
števila 1, 2, 3 in 4 pa predstavljajo dodeljena števila s strani univerze.  
  
**Ime zahteve:** Upoštevanje GDPR  
**Tip zahteve:** Zunanja zahteva  
**Opis zahteve:** Spletna aplikacija bo morala upoštevati Splošno uredbo EU o varstvu podatkov (GDPR). Študenti si lahko ogledajo le svoje koledarje in urnike, ne sme pa jim biti omogočen pregled koledarjev in urnikov 
drugih študentov ali celo profesorjev. Poleg tega GDPR predpisuje še veliko omejitev, ki jih bo potrebno upoštevati.  
  
**Ime zahteve:** Cenzura  
**Tip zahteve:** Zunanja zahteva  
**Opis zahteve:** Študenti v koledar ne bodo smeli vnašati kletvic. S tem bi poskrbeli za dvig etične kulture v Sloveniji.  
  
**Ime zahteve:** Število hkratnih uporabnikov  
**Tip zahteve:** Zahteva izdelka  
**Opis zahteve:** Sistem mora biti zmožen streči najmanj 2000 hkratnim uporabnikom. V Sloveniji trenutno študira okoli 80000 študentov. Ob vikendih, ko je organizacija študentov na vrhuncu, bodo študenti zagotovo 
preživeli okvirno 5 - 10 minut na aplikaciji, seveda pa ne vsi hkrati. Ocenjujemo, da bi hkrati lahko spletno aplikacijo obiskalo okvirno 2,5% vseh študentov, kar znaša 2000 študentov.

## 7. Prototipi vmesnikov

### Zaslonske maske

**NAVODILO:** Za podroben ogled zaslonske maske, kliknite z desno tipko na miški in izberite Ogled slike v novem zavihku, kjer bo slika prikazana v večji kvaliteti.

---
* Povezava med neregistriranim uporabnikom s funkcionalnostjo registracijo v sistem
![alt text](../img/zaslonske-maske/registracija.png)

---

* Povezava med študentom, profesorjem ali skrbnikom s funkcionalnostjo prijava v sistem
![alt text](../img/zaslonske-maske/prijava.png)

---

* Povezava med študentom, profesorjem ali skrbnikom s funkcionalnostjo odjava iz sistema
![alt text](../img/zaslonske-maske/odjava.png)

---

* Povezava med skrbnikom in funkcionalnostjo brisanje uporabnikov
![alt text](../img/zaslonske-maske/brisanje-uporabnikov.png)

---

* Povezava med profesorjem in funkcionalnostjo dodajanje predmetov
![alt text](../img/zaslonske-maske/dodajanje-predmetov.png)

---

* Povezava med profesorjem in funkcionalnostjo urejanje predmetov
![alt text](../img/zaslonske-maske/urejanje-predmetov.png)

---

* Povezava med profesorjem in funkcionalnostjo brisanje predmetov
![alt text](../img/zaslonske-maske/brisanje-predmetov.png)

---

* Povezava med študentom in funkcionalnostjo ogled predmetov.
![alt text](../img/zaslonske-maske/ogled-predmetov.png)

---

* Povezava med študentom in funkcionalnostjo dodajanje dogodkov v koledar.
![alt text](../img/zaslonske-maske/dodajanje-dogodka.png)

---

* Povezava med študentom in funkcionalnostjo brisanje dogodkov iz koledarja.
![alt text](../img/zaslonske-maske/brisanje-dogodkov.png)

---

* Povezava med študentom in funkcionalnostjo spreminjanje dogodkov iz koledarja.
![alt text](../img/zaslonske-maske/spreminjanje-dogodkov.png)

---

* Povezava med študentom in funkcionalnostjo ogled koledarja.
![alt text](../img/zaslonske-maske/ogled-koledarja.png)

---

* Povezava med študentom in funkcionalnostjo ogled možnega plana dela.
![alt text](../img/zaslonske-maske/plan-dela.png)

---

* Povezava med študentom in funkcionalnostjo prikaz vremena za trenutni dan.
![alt text](../img/zaslonske-maske/prikaz-vremena.png)

---

* Povezava med študentom in funkcionalnostjo izvoz koledarja v Google Calendar.
![alt text](../img/zaslonske-maske/izvoz-koledarja.png)

---

* Povezava med študentom in funkcionalnostjo uvoz koledarja iz Google Calendar.
![alt text](../img/zaslonske-maske/uvoz-koledarja.png)

---


### Vmesniki do zunanjih sistemov

Sistem bo komuniciral z dvema zunanjima sistemoma:

* **OpenWeatherMap**
    * **Opis:** Poizvedovali bomo za stanje vremena s podano geolokacijo.
    * **Format:** GET https://api.openweathermap.org/data/2.5/forecast?lat={lat}&lon={lon}
    * **Podatki:** Storitev bo vračala naslednje podatke v formatu json:
    ```
    {  
        "list":[  
            {  
                "dt": string,
                "main":{  
                    "temp": float,
                    "temp_min": float,
                    "temp_max": float,
                    "pressure": float,
                    "humidity":int
                },
                "weather":[  
                    {  
                    "main": string,
                    "description": string
                    }
                ]
            }
        ]
    }
    ```
    * [Primer uprabe](https://openweathermap.org/forecast5)

* **Google Calendar API (kreiranje dogodka)**
    * **Opis:** Kreiranje dogodka na primarnem koledarju v Google Calendar.
    * **Format:** POST https://www.googleapis.com/calendar/v3/calendars/primary/events
    * **Podatki:** V našem primeru, klic potrebuje sledeče parametre:
    ```
    {
        "type": "calendar#event",
        "summary": string,
        "description": string,
        "location": string,
        "colorId": string,
        "start": {
            "date": date,
            "dateTime": datetime,
            "timeZone": string
        },
        "end": {
            "date": date,
            "dateTime": datetime,
            "timeZone": string
        },
        "endTimeUnspecified": boolean,
        "recurrence": [
            string
        ]
    }
    ```
    * [Primer uporabe](https://developers.google.com/calendar/v3/reference/events/insert#examples)

* **Google Calendar API (pridobivanje dogodkov)**
    * **Opis:** Pridobivanje vseh dogodkov na primarnem koledarju.
    * **Format:** GET https://www.googleapis.com/calendar/v3/calendars/primary/events
    * **Podatki:** V našem primeru, klic vrne sledeče parametre, ki so pomembni za našo implementacijo:
    ```
    {
        "kind": "calendar#events",
        "summary": string,
        "description": string,
        "updated": datetime,
        "timeZone": string,
        "nextPageToken": string,
        "nextSyncToken": string,
        "items": [
             {
                "type": "calendar#event",
                "summary": string,
                "description": string,
                "location": string,
                "colorId": string,
                "start": {
                    "date": date,
                    "dateTime": datetime,
                    "timeZone": string
                },
                "end": {
                    "date": date,
                    "dateTime": datetime,
                    "timeZone": string
                },
                "endTimeUnspecified": boolean,
                "recurrence": [
                    string
                ]
            }
        ]
    }
    ```
    * [Primer uporabe](https://developers.google.com/calendar/v3/reference/events/list#examples)