# Načrt sistema

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** |  Arne Simonič, Miha Benčina, Janko Knez, Aljaž Švigelj |
| **Kraj in datum** | Ljubljana, 20.4.2019 |



## Povzetek

Ta dokument opisuje podrobnejši načrt aplikacije StraightAs in služi kot vodilo za implementacijo sistema. V prvem delu je podan načrt arhitekture, najprej v logičnem in nato v razvojnem pogledu, kjer oba pogleda služita kot način za boljšo predstavo arhitekture sistema in kot referenčni točki med procesom implementacije. V drugem delu je predstavljen načrt strukture v obliki razrednega diagrama. Razredni diagram omogoča tehnično natančnejši pogled v strukturo aplikacije in hkrati služi kot osnova za zasnovo sheme podatkovne baze.
V tretjem delu so za vsako funkcionalnost iz naloge zajem zahtev opisani natančni diagrami zaporedja, ki razvijalcu posameznih funkcionalnosti omogočajo natančno implementacijo, ki sledi podanim zahtevam sistema.
S tem dokumentom bo proces implementacije preglednejši in lažje sledljiv, kar bo povečalo hitrost razvoja in zmanjšalo možnost za napake.


## 1. Načrt arhitekture

Na spodnjih slikah je prikazan načrt arhitekture, najprej v logičnem pogledu in nato še v razvojnem pogledu.

* Arhitektura aplikacije StraightAs v logičnem pogledu.
![alt text](../img/arhitektura-logicna.png)

* Arhitektura aplikacije StraightAs v razvojnem pogledu, po vzorcu model-pogled-krmilnik (MVC).
![alt text](../img/arhitektura-razvojna.png)

## 2. Načrt strukture

### 2.1 Razredni diagram

![alt text](../img/razredni-diagram.png)

### 2.2 Opis razredov

#### Oseba
Razred predstavlja generalizacijo kateregakoli izmed 3 že registriranih uporabnikov v spletni aplikaciji.

#### Atributi

- id: String -> Enolični identifikator osebe
- email: String
- ime: String 
- priimek: String

#### Nesamoumevne metode

/

---

#### Študent
Razred predstavlja uporabnika spletne aplikacije tipa študent.

#### Atributi

/

#### Nesamoumevne metode

/

---

#### Skrbnik
Razred predstavlja uporabnika spletne aplikacije tipa skrbnik.

#### Atributi

/

#### Nesamoumevne metode

/

---

#### Profesor
Razred predstavlja uporabnike spletne aplikacije tipa profesor.

#### Atributi

/

#### Nesamoumevne metode

/

---

#### Dogodek
Razred predstavlja dogodek, ki ga uporabnik vnese v koledar.

#### Atributi

- datum: Date
- ime: String
- opis: String
- prioriteta: int -> Nivo pomembnosti, ki jo ima dogodek. Višja vrednost pomeni višjo prioriteto.

#### Nesamoumevne metode

/

---

#### Predmet
Razred predstavlja predmet, ki ga opravljajo študenti.

#### Atributi

- ime: String
- opis: String
- terminiPredavanj: String -> Termini predavanj, ločeni s podpičjem v primeru razkosanosti, v obliki "Dan:Zacetek-Konec". Npr. "PON:12.15-15.00".
- terminiVaj: String -> Termini vaj, ločeni s podpičjem, v obliki "Dan:Zacetek-Konec". Npr. "PON:10.15-12.00;TOR:08.15-10.00".
- terminiKolokvijev: String -> Termini kolokvijev, ločeni s podpičjem, v obliki "Datum:Zacetek-Konec". Npr. "20.4.2019:18.00-20.00;16.5.2019:16.00-18.00".
- terminiIzpitov: String -> Termini izpitov, ločeni s podpičjem, v obliki "Datum:Zacetek-Konec". Npr. "3.6.2019:10.00-12.00;16.6.2019:13.00-15.00".
- profesor: Profesor -> Referenca na profesorja, ki je nosilec predmeta.

#### Nesamoumevne metode

/

---

#### UvozInIzvozKoledarja
Integracija uvoza in izvoza koledarja v in iz Google Calendar.

#### Atributi

/

#### Nesamoumevne metode

- void uvoz() -> Uvoz koledarja iz Google Calendar.
- void izvoz() -> Izvoz obstoječega koledarja v Google Calendar.

---

#### GeneratorPlanaDela
Razred ustvari plan dela, kar pomeni, da na podlagi prioritete in časa do dogodkov razvrsti dogodke v koledarju ter jih vrne v tabeli.

#### Atributi

/

#### Nesamoumevne metode

- Dogodek[] generirajPlanDela() -> Na podlagi prioritete in časa do dogodkov razvrsti dogodke v koledarju ter jih vrne v tabeli tipa Dogodek[].

---

#### PrikazKoledarja
Razred prikaze koledar z vsemi vsebujočimi dogodki.

#### Atributi

/

#### Nesamoumevne metode

- Dogodek[] pridobiDogodke() -> V tabeli vrne vse dogodke iz koledarja.

---

#### UrejanjeDogodkov
Razred omogoča urejanje dogodkov, torej dodajanje, spreminjanje ter brisanje dogodkov.

#### Atributi

/

#### Nesamoumevne metode

- void dodajDogodek(Dogodek d) -> Doda sledeči dogodek v koledar.
- void urediDogodek(Dogodek d) -> Omogoča urejanje izbranega dogodka v koledarju, torej spreminjanje vseh atributov.
- void izbrisiDogodek(String id) -> Izbriše dogodek z identifikatorjem id, ki je tipa uuid in je zato shranjen kot String.

---

#### UpravljanjeRacuna
Razred omogoča upravljanje z uporabniškim računom prijavljenega uporabnika ter registracijo neprijavljenega uporabnika.

#### Atributi

/

#### Nesamoumevne metode

- String prijava(String email, String geslo) -> Prijavi uporabnika z emailom in geslom ter vrne žeton seje.
- void registracija(String email, String geslo) -> Registrira uporabnika z emailom in geslom.
- String potrditevEmailNaslova(String token) -> Pošlje potrditveni email na email naslov, ki je bil podan ob registraciji. Potrditveni email je vezan na žeton.
- void odjava()-> Odjavi uporabnika iz spletne aplikacije.

---

#### UrejanjeUporabnikov
Razred skrbi za administratorske funkcije, s katerimi upravlja skrbnik.

#### Atributi

/

#### Nesamoumevne metode

- void dodajProfesorja(Profesor p) -> V sistem se doda izbranega profesorja.
- void odstraniUporabnika(String id) -> Iz spletne aplikacije zbriše uporabnika z idetifikatorjem id.

---

#### UrejanjePredmetov
Razred omogoča urejanje predmetov, torej določanje terminov predavanj, vaj, kolokvijev ter izpitov.

#### Atributi

/

#### Nesamoumevne metode

- Predmet[] ogledPredmetov() -> Uporabniku prikaže vse predmete, ki so zavedeni v sistemu.
- Predmet ogledPredmeta(String id) -> Uporabniku prikaže vse podrobnosti predmeta z izbranim id-jem.
- void dodajPredmet(Predmet p) -> Omogoča dodajanje predmeta, ki ga opravljajo študenti.
- void urediPredmet(Predmet p) -> Omogoča urejanje obstoječega predmeta, torej spremembo terminov predavanj, vaj, kolokvijev in izpitov.
- void izbrisiPredmet(Predmet p) -> Omogoča brisanje izbranega predmeta.

---

#### PrikazVremena
Razred omogoča ogled vremena, ki ga pridobi od zunanjega aplikacijskega vmesnika.

#### Atributi

/

#### Nesamoumevne metode

- Vreme pridobiVreme() -> Pridobi vreme od zunanjega aplikacijskega vmesnika ter ga vrne v objektu tipa Vreme.

---

#### GoogleCalendarAPIPovezava
Razred predstavlja sistemski vmesnik do aplikacije Google Calendar, do katerega se povezujemo za uvoz in izvoz dogodkov.

#### Atributi

/

#### Nesamoumevne metode

- Dogodek ustvariDogodek(Dogodek d) -> V Google Calendar aplikaciji ustvari podani dogodek ter ga hkrati vrne.
- Dogodek[] pridobiVseDogodke()-> Iz Google Calendar aplikacije pridobi vse dogodke ter jih vrne v objektu tipa Dogodek[].

---

#### OpenWeatherMapPovezava
Razred predstavlja sistemski vmesnik do vremenske aplikacije Open Weather Map, kjer pridobivamo podatke o vremenu.

#### Atributi

/

#### Nesamoumevne metode

- PodatkiVremena pridobiVreme() -> Pridobi ter vrne podatke o vremenu.

---

#### UporabniskiVmesnik
Razred predstavlja celoten uporabniški vmesnik, preko katerega uporabnik upravlja s sistemom.

#### Atributi

/

#### Nesamoumevne metode

- String prijava(String email, String geslo) -> Prijavi uporabnika z emailom in geslom ter vrne žeton seje.
- void registracija(String email, String geslo) -> Registrira uporabnika z emailom in geslom.
- String potrditevEmailNaslova(String token) -> Pošlje potrditveni email na email naslov, ki je bil podan ob registraciji. Potrditveni email je vezan na žeton.
- void odjava()-> Odjavi uporabnika iz spletne aplikacije.
- Uporabnik[] ogledUporabnikov() -> Vrne vse registrirane uporabnike v spletni aplikaciji.
- void dodajProfesorja(Profesor p) -> V sistem se doda izbranega profesorja.
- void odstraniUporabnika(String id) -> Iz spletne aplikacije zbriše uporabnika z idetifikatorjem id.
- Predmet[] ogledPredmetovProfesorja(String id) -> Na podlagi vseh predmetov izbranega profesorja le-te tudi prikaže.
- void dodajPredmet(Predmet p) -> Omogoča dodajanje predmeta, ki ga opravljajo študenti.
- void urediPredmet(Predmet p) -> Omogoča urejanje obstoječega predmeta, torej spremembo terminov predavanj, vaj, kolokvijev in izpitov.
- void izbrisiPredmet(Predmet p) -> Omogoča brisanje izbranega predmeta.
- Dogodek[] ogledKoledarja()-> Na podlagi vseh dogodkov uporabniku prikaže koledar.
- Predmet[] ogledPredmetov()-> Uporabniku prikaže vse predmete, ki so zavedeni v sistemu.
- Predmet ogledPredmeta(String id) -> Uporabniku prikaže vse podrobnosti predmeta z izbranim id-jem.
- Dogodek[] generirajPlanDela()-> Na podlagi prioritete in časa do dogodkov razvrsti dogodke v koledarju ter jih vrne v tabeli tipa Dogodek[].
- Vreme ogledVremena()-> Uporabniku prikaže vreme za celoten teden.
- void dodajDogodek(Dogodek d) -> Doda sledeči dogodek v koledar.
- void urediDogodek(Dogodek d) -> Omogoča urejanje izbranega dogodka v koledarju, torej spreminjanje vseh atributov.
- void izbrisiDogodek(String id) -> Izbriše dogodek z identifikatorjem id, ki je tipa uuid in je zato shranjen kot String.
- void uvoziKoledar()-> Izvrši uvoz koledarja iz Google Calendar aplikacije.
- void izvozKoledar()-> Izvrši izvoz koledarja v Google Calendar aplikacijo.

## 3. Načrt obnašanja

### Opombe
Vsaka omemba **vmesnika** v diagramih zaporedja se nanaša na **uporabniški vmesnik**.

### Diagram zaporedja, ki prikazuje tokove funkcionalnosti Registracija v sistem
![alt text](../img/diagrami-zaporedja/01registracija.png)

### Diagram zaporedja, ki prikazuje tokove funkcionalnosti Prijava v sistem
![alt text](../img/diagrami-zaporedja/02prijava.png)

### Diagram zaporedja, ki prikazuje tokove funkcionalnosti Odjava iz sistema
![alt text](../img/diagrami-zaporedja/03odjava.png)

### Diagram zaporedja, ki prikazuje tokove funkcionalnosti Brisanje uporabnikov
![alt text](../img/diagrami-zaporedja/04brisanje_uporabnikov.png)

### Diagram zaporedja, ki prikazuje tokove funkcionalnosti Dodajanje dogodkov v koledar
![alt text](../img/diagrami-zaporedja/05dodaj_dogodek.png)

### Diagram zaporedja, ki prikazuje tokove funkcionalnosti Brisanje dogodkov iz koledarja
![alt text](../img/diagrami-zaporedja/06brisanje_dogodkov.png)

### Diagram zaporedja, ki prikazuje tokove funkcionalnosti Spreminjanje dogodkov v koledarju
![alt text](../img/diagrami-zaporedja/07spreminjanje_dogodkov.png)

### Diagram zaporedja, ki prikazuje tokove funkcionalnosti Ogled koledarja
![alt text](../img/diagrami-zaporedja/08ogled_koledarja.png)

### Diagram zaporedja, ki prikazuje tokove funkcionalnosti Ogled možnega plana dela
![alt text](../img/diagrami-zaporedja/09ogled_plana.png)

### Diagram zaporedja, ki prikazuje tokove funkcionalnosti Dodajanje profesorjev
![alt text](../img/diagrami-zaporedja/10dodajanje_profesorjev.png)

### Diagram zaporedja, ki prikazuje tokove funkcionalnosti Dodajanje predmetov
![alt text](../img/diagrami-zaporedja/11dodajanje_predmetov.png)

### Diagram zaporedja, ki prikazuje tokove funkcionalnosti Urejanje predmetov
![alt text](../img/diagrami-zaporedja/12urejanje_predmetov.png)

### Diagram zaporedja, ki prikazuje tokove funkcionalnosti Brisanje predmetov
![alt text](../img/diagrami-zaporedja/13brisanje_predmetov.png)

### Diagram zaporedja, ki prikazuje tokove funkcionalnosti Ogled predmetov
![alt text](../img/diagrami-zaporedja/14ogled_predmeta.png)

### Diagram zaporedja, ki prikazuje tokove funkcionalnosti Prikaz vremena za trenutni dan
![alt text](../img/diagrami-zaporedja/15prikaz_vremena_za_trenutni_dan.png)

### Diagram zaporedja, ki prikazuje tokove funkcionalnosti Izvoz koledarja v Google Calendar
![alt text](../img/diagrami-zaporedja/16izvoz_koledarja_v_google_calendar.png)

### Diagram zaporedja, ki prikazuje tokove funkcionalnosti Uvoz koledarja iz Google Calendar
![alt text](../img/diagrami-zaporedja/17uvoz_koledarja_v_google_calendar.png)