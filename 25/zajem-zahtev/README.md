# Dokument zahtev

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Nik Bratuša, Sara Koljić, Žan Pečovnik, Žiga Kleine |
| **Kraj in datum** | Ljubljana, 8.4.2019 |



## Povzetek projekta
V 2. lastnem projektu smo poskušali čim bolj natančno  definirati funkcionalnosti aplikacije za upravljanje s študentskimi obveznostmi StraightAs.
V uvodu smo našteli glavne funkcionalnosti aplikacije, prav tako pa tudi na hitro opisali nefunkcionalne zahteve.  
Definirali smo uporabniške vloge in slovar pojmov. Narisali smo diagram primerov uporabe, ki vizualizira interakcijo sistema z uporabnikom in z zunanjimi sistemi.
V poglavju funkcionalne zahteve smo podrobno razčlenili vsako interakcijo med uporabnikom in sistemom. 
Za vsako funkcionalnost smo opisali scenarije delovanja, našteli pogoje pod katerimi se funkcionalnost izvede, določili njeno prioriteto in navedli sprejemne teste.
Določili smo nefunkcionalne zahteve, ki so splošne omejitve, ki jih moramo upoštevati pri razvoju aplikacije. 
Za vsako funkcionalnost smo pripravili tudi primer zaslonske maske, ki predstavlja osnutek uporabniškega vmesnika za določeno funkcionalnost. 
Na koncu smo opisali še vmesnik za interakcijo sistema z zunanjim sistemom, ki ga bo aplikacija uporabljala.


## 1. Uvod
Študentje se pogosto soočajo z velikimi količinami opravkov in obveznosti povezanih s študijem,  zapomniti si morajo roke oddaj nalog, datume in ure kolokvijev, izpitov, vaj, predavanj itd. 
Zato bi si želeli aplikacijo, ki bi pomagala študentu pri učinkovitem  razporejanju časa za študijske in druge obveznosti, hkrati pa bi mu tudi nudila tudi preprost pregled nad ocenami študijskih obveznosti. 
Delo bi mu olajšali profesorji, ki bi za svoje predmete že vpisali čase predavanj in izpitnih rokov, študent pa bi  lahko z enim klikom te podatke prenesel v svoj profil.

Aplikacija nudi funkcionalnosti, ki se razlikujejo za štiri uporabniške vloge, ki jih lahko določimo uporabnikom aplikacije.
Te vloge so: študent, profesor, starš in administrator, pri tem je glavna vloga študent, ki ponuja največ funkcionalnosti.

Glavna uporabniška vloga, nanj se aplikacija osredotoča je študent. V glavnem meniju ima na voljo tri zavihke, ki ločijo tri glavne sklope funkcionalnosti, ki mu jih aplikacija ponuja. 
V glavnem meniju ima na voljo še funkcionalnost dodajanja staršev. Vsakemu dodanemu staršu lahko določi ali odvzame pravico pregleda urnika in pregleda predmetov.

Zavihek urnik omogoča študentu časovni pregled nad vsemi vnosi obveznosti, pod tem zavihkom pa lahko tudi dodaja in odstranjuje ostale obveznosti, ki niso povezane s študijem.

Zavihek TO-DO seznami omogoča študentu prikaz vseh v preteklosti tvorjenih TO-DO seznamov (TO-DO seznam je seznam obveznosti v izbranem časovnem obdobju, ki je razvrščen po prioriteti), prav tako pa 
omogoča tvorjenje novih TO-DO seznamov za izbrano obdobje in brisanje obstoječih.

Predmeti je še zadnji zavihek v meniju študenta, omogoča mu pregled nad vsemi predmeti na katere je prijavljen, pri tem lahko predmet bodisi izbere iz seznama 
obstoječih predmetov, ki jih nadzorujejo in urejajo profesorji, bodisi ga ustvari sam. S klikom na posamezen predmet v pregledu predmetov, 
si lahko ogleda in ureja podrobnosti pr predmetu, kot so obveznosti in ocene pri izbranem predmetu. 
Na voljo ima tudi funkcionalnost izbire izpitnega roka. Ob izbiri izpitnega roka se mu ta vpiše v urnik. 

Profesor ima na voljo dva zavihka, na zavihku urnik ima kratek pregled aktivnosti, ki jih izvaja v sklopu svojih predmetov, na zavihku predmeti pa se mu izpiše seznam predmetov, ki jih izvaja. 
Tu lahko dodaja nove predmete ali pa odstranjuje že obstoječe. S klikom na predmet iz seznama se mu prikažejo podrobnosti predmeta. 
Tu lahko dodaja in odstranjuje obveznosti izbranega predmeta, kot so roki izpitov in časi predavanj.

Funkcija starša je izključno namenjena ogledu aktivnosti svojih otrok. Starš ima na glavnem meniju seznam iz katerega izbere enega izmed svojih otrok, nato pa izbere enega izmed dveh zavihkov, ki jih ima na voljo. 
Zavihek urnik mu prikaže seznam obveznosti izbranega otroka, zavihek predmeti pa mu prikaže predmete na katere je izbrani otrok vpisan. 
S klikom na enega izmed predmetov se mu prikažejo podrobnosti o izbranem predmetu, kot so ocene in obveznosti povezane s tem predmetom. 

Vloga administratorja v aplikaciji bo izključno namenjena pregledu in nadzoru nad uporabniki, ki se na storitev prijavljajo. Administratorju se bodo novo registrirani uporabniki pojavili na vrhu seznama v njegovem meniju, označeni  z rdečo barvo. 
Te uporabnike bo imel možnost potrditi ali pa izbrisati, če njihovi podatki ne bodo ustrezni.

Nefunkcionalne zahteve so splošne omejitve, ki jih moramo  postaviti in upoštevati pri razvoju aplikacije. Postavili smo zahteve treh tipov: organizacijske zahteve, zunanje zahteve in zahteve izdelka.

Kot zahteve izdelka smo določili maksimalen odziven čas sistema, verjetnost izgube podatkov ob izpadu sistema, maksimalno velikost podatkovne baze, koliko uporabnikov lahko naenkrat dostopa, ter kdaj mora biti sistem dostopen. 
Z organizacijskimi zahtevami smo določili, da mora biti sistem zgrajen s pomočjo MVC in MEAN arhitekture, ter da mora biti dosegljiv na javno dostopnem spletnem naslovu. 
Za zunanje zahteve pa smo določili, da mora sistem ustrezati slovenski zakonodaji in da sistem vsakemu uporabniku nudi le podatke za katere je pooblaščen.


## 2. Uporabniške vloge

**študent** (Vpisovanje svojega urnika, kamor spadajo poleg terminov predavanj in vaj tudi datumi oddaj nalog, kolokvijev in izpitov, ter morebiti kakšnih drugih ocenjevanj, termini za učenje in druge obveznosti. Dodajanje prioritet
svojim obveznostim, dodajanje in odvzemanje pravic za dostop do svojega urnika in redovalnice staršem)  
**starš** (Pregled nad obveznostmi svojih otrok - na voljo bo imel ogled urnika in ocen študenta)  
**profesor** (Pregled predmetov ki jih izvaja in dodajanje le teh, vpisovanje datumov izpitnih rokov in datumov za oddajo nalog)  
**administrator** (Urejanje računov, kamor spada potrjevanje novih in brisanje zastarelih/neprimernih)  

## 3. Slovar pojmov

**Urnik**  
Je nakakšen koledar v katerem so z okenci predstavljeni obveznosti študenta v nekem dnevu. Okenca so postavljena točno določeno glede na datum in uro. Urnik si lahko študent oglejuje za vsak teden posebej ali pa strnjeno v en mesec.  
**Redovalnica**  
Gre za tabelarični prikaz predmetov in njim pripadajočih ocen nekega študenta.  
**Prioriteta**  
Gre za številčno ocenjevanje obveznosti, ki jo ima študent na urniku. Bolj kot je obveznost pomembna večjo prioriteto ima, kar pomeni da mu študent dodeli višjo številko. Lestvica za določanje prioritete bo od 1-5.  
**Izpit**  
Gre za končni preizkus znanja, ki ga mora opraviti vsak študent. Izvede in oceni ga profesor.  
**Kolokvij**  
Gre za preizkus znanja, ki se izvede med študijskim letom in preverja sprotno sodelovanje študentov. Ponekod lahko kolokviji nadomestijo pisni izpit ponekod lahko študentu zvišajo oceno, spet drugje so opravljeni kolokviji pogoj za pristop k izpitu.  
**Predmet**  
Šolsko učno področje, predpisano in vsebinsko določeno z učnim načrtom.  

## 4. Diagram primerov uporabe

![Diagram primerov uporabe](../img/diagram-primerov-uporabe.png)

## 5. Funkcionalne zahteve

### FZ1: Vpis
#### Povzetek funkcionalnosti
Administrator, profesor, starš ali študent lahko v vnosno polje vpiše svoj email in izbrano geslo ter tako dobi dostop do aplikacije.

#### Osnovni tok (za vse uporabniške vloge je enak)
1. Uporabnik vstopi na vpisno stran.
2. Sistem ponudi obrazec za vpis.
3. Uporabnik v polje poleg besede "email" vpiše svoj email, s katerim se je registriral.
4. Uporabnik v polje poleg besede "geslo" vpiše geslo, ki si ga je izbral ob registraciji.
5. Uporabnik pritisne gumb Vpis.
6. Sistem uporabnika preusmeri na uporabnikovo "domačo stran".

#### Alternativni tok
1. Uporabnik vstopi na vpisno stran.
2. Sistem ponudi obrazec za vpis.
3. Uporabnik pritisne gumb Vpis z googlom
4. Sistem ga preusmeri na izbiro uporabniškega računa, s katerim je vpisan v Googlove storitve.
5. Uporabnik izbere enega od uporabniških računov, s katerim se želi prijaviti v aplikacijo.
6. Sistem uporabnika preusmeri na uporabnikovo "domačo stran".

#### Izjemni tokovi
* Uporabnik vpiše napačen email in pravilno geslo, sistem mu onemogoči dostop do aplikacije in prikaže ustrezno obvestilo.
* Uporabnik vpiše pravilen email in napačno geslo, sistem mu onemogoči dostop do aplikacije in prikaže ustrezno obvestilo.
* Uporabnik ne izpolni enega ali obeh vnosnih polj, sistem mu onemogoči dostop do aplikacije in prikaže ustrezno obvestilo.

#### Pogoji
Uporabnik mora biti registriran v sistemu. Če ni registriran, mu funckionalnost ni na voljo.

#### Posledice
Če se uporabnik uspešno vpiše, mu sistem odobri dostop do aplikacije.

#### Posebnosti
Funkcionalnost ne zahteva nobene posebnosti.

#### Prioritete identificiranih funkcionalnosti
Must have.

#### Sprejemni testi
* Vpiši email in geslo, s katerima se je registriral.
* Pritisni gumb Vpis z googlom, izberi uporabniški račun s katerim si vpisan v Googlove storitve.
* Ne izpolni ali napačno izpolni vnosna polja.



### FZ2: Registracija
#### Povzetek funkcionalnosti
Profesor, starš ali študent lahko v vnosna polja vpiše svoje ime, priimek, email ter geslo in si zbere uporabniško vlogo s katero se želi registrirati.

#### Osnovni tok (za vse uporabniške vloge je enak)
1. Uporabnik vstopi na stran za registracijo.
2. Sistem mu ponudi obrazec za registracijo.
3. Uporabnik v polje poleg besede "Ime" vpiše svoje ime.
4. Uporabnik v polje poleg besede "Priimek" vpiše svoj priimek.
5. Uporabnik v polje poleg besede "Email" vpiše svoj email.
6. Uporabnik v polje poleg besede "Geslo" vpiše geslo.
7. Uporabnik označi katero uporabniško vlogo želi uporabljati.
8. Uporabnik pritisne gumb Registracija.
9. Sistem uporabnika preusmeri na uporabnikovo "domačo stran".

#### Alternativni tok
1. Uporabnik vstopi na stran za registracijo.
2. Sistem mu ponudi obrazec za registracijo.
3. Uporabnik pritisne gumb Registracija z googlom
4. Sistem ga preusmeri na izbiro uporabniškega računa, s katerim je vpisan v Googlove storitve.
5. Uporabnik izbere enega od uporabniških računov, s katerim se želi registrirati.
6. Sistem uporabnika preusmeri na uporabnikovo "domačo stran".

#### Izjemni tokovi
* Uporabnik vpiše email, ki je že bil uporabljen pri registraciji v preteklosti (email je že registriran v sistemu), sistem mu onemogoči dostop do aplikacije in prikaže ustrezno obvestilo.
* Uporabik uporabi v imenu ali priimku številke, uporabni napačno obliko emaila ali pa izpusti kakšno polje, sistem mu onemogoči dostop do aplikacije in prikaže ustrezno obvestilo.

#### Pogoji
Uporabnik ne sme biti registriran v sistemu z emailom s katerim se poskuša registrirati. Če je že registriran s tem emailom v sistemu, mu funckionalnost ni na voljo.

#### Posledice
Če se uporabnik uspešno registrira, mu sistem odobri dostop do aplikacije.

#### Posebnosti
Funkcionalnost ne zahteva nobene posebnosti.

#### Prioritete identificiranih funkcionalnosti
Must have.

#### Sprejemni testi
* Vpiši pravilno ime, priimek, email in geslo, email ne sme biti tak, ki je že bil uporabljen v preteklosti.
* Pritisni gumb Registracija z googlom, izberi uporabniški račun s katerim si vpisan v Googlove storitve.
* Vpiši napačne podatke ali pa izpusti kakšno polje.



### FZ3: Pregled urnika
#### Povzetek funkcionalnosti
Študent ali starš si lahko ogledata urnik študenta.

#### Osnovni tok(študent)
1. Študent v glavnem meniju izbere funkcionalnost Urnik. 
2. Sistem mu prikaže stran, ki vsebuje njegov urnik.

#### Osnovni tok(starš)
1. Starš s pomočjo seznama v glavnem meniju izbere otroka.
2. Starš v glavnem meniju izbere funkcionalnost Urnik.  
3. Sistem mu prikaže stran, ki vsebuje urnik izbranega otroka.

#### Osnovni tok(profesor)
1. Profesor v glavnem meniju izbere funkcionalnost Urnik. 
2. Sistem mu prikaže stran, ki vsebuje urnik vnosov predmetov, ki jih izvaja.

#### Alternativni tok(ovi)
/

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot študent, starš, ali profesor. Če ni prijavljen, ali pa če je prijavljen kot administrator, mu funkcionalnost ni na voljo.

#### Posledice
Uporabniku se bo na zaslonu prikazala stran z ustreznim urnikom.

#### Posebnosti
Funkcionalnost ne zahteva nobene posebnosti.

#### Prioritete identificiranih funkcionalnosti
Must have.

#### Sprejemni testi
1. Prijavi se kot študent, izberi funkcionalnost ogleda urnika.
2. Prijavi se kot starš, izberi otroka, izberi funkcionalnost ogleda urnika.
3. Prijavi se kot profesor, izberi funkcionalnost ogleda urnika.



### FZ4: Dodajanje obveznosti
#### Povzetek funkcionalnosti
Študent lahko v urnik dodaja obveznosti.

#### Osnovni tok(študent)  
1. Študent izbere funkcionalnost Dodaj obveznost.  
2. Sistem ponudi obrazec v katerega je mogoče vnesti podatke o obveznosti.  
3. Študent vpiše naziv, opis, datum in prioriteto obveznosti. 
4. Sistem vstavi okence z opisom obveznosti na pravi dan ob pravem času.  
5. Sistem prikaže sporočilo o uspešno dodani obveznosti.  

#### Alternativni tok 1 (študent)
1. Študent poskuša dodati novo obveznost.  
2. Študentu se prekrivata dve ali več obveznosti, zato slednje ne more neposredno dodati.   
3. Sistem prikaže ustrezno obvestilo s katero ga vpraša o prioritetah obveznosti.  
4. Sistem glede na prioritete razvrsti obveznosti.  

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot študent. Če ni prijavljen ali pa če je prijavljen
kot profesor, starš ali administrator, mu funkcionalnost ni na voljo.

#### Posledice
Če si študent uspešno doda obveznost bo to označeno v njegovem urniku z majhnim okencem v katerem 
bo opis obveznosti.

#### Posebnosti
Funkcionalnost ne zahteve nobene posebnosti.

#### Prioritete identificiranih funkcionalnosti
Should have.

#### Sprejemni testi
1. Prijavi se kot študent, dodaj obveznost v času ko na urniku še ni ničesar in potrdi.    
Testira se funkcija ki dodaja obveznost v urnik. Na začetku je urnik prazen v terminu kamor dodajamo obveznost.
Obveznost se uspešno doda na prosto mesto.  
2. Dodaj obveznost v času ko je na urniku istočasno že nekaj drugega.     
Testira se funkcionalnost ki preverja če je na določenem mestu na urniku že kakšna obveznost. Na urnik poskušamo dodati obveznost na enako mesto, kjer je že predhodno dodana neka druga. Sistem mora onemogočiti neposredno 
dodajanje obveznosti in nam poslati obvestilo, ki nas vpraša o prioritetah obveznosti. 



### FZ5: Odstranjevanje obveznosti
#### Povzetek funkcionalnosti
Študent lahko iz urnika odstranjuje obveznosti.

#### Osnovni tok(študent)  
1. Študent izbere svoj urnik.  
2. Študent izbere obveznost, ki jo želi odstraniti in pritisne x v zgornjem desnem kotu obveznosti.
3. Sistem prikaže sporočilo o uspešno odstranjeni obveznosti.  

#### Alternativni tok
/

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot študent. Če ni prijavljen ali pa če je prijavljen
kot profesor, starš ali administrator, mu funkcionalnost ni na voljo.

#### Posledice
Če si študent uspešno odstrani obveznost le ta ne bo več vidna na urniku.

#### Posebnosti
Funkcionalnost ne zahteve nobene posebnosti.

#### Prioritete identificiranih funkcionalnosti
Should have.

#### Sprejemni testi
1. Prijavi se kot študent, odstrani obveznost iz urnika.    
Testira se funkcija ki odstranjuje obveznosti iz urnika. 



### FZ6: Pregled TO-DO seznamov
#### Povzetek funkcionalnosti
Študent si lahko ogleda TO-DO sezname, ki jih je dodal.

#### Osnovni tok(študent)
1. Študent v glavnem meniju izbere funkcionalnost TO-DO seznami. 
2. Sistem mu prikaže vse TO-DO sezname, ki jih je že dodal.

#### Alternativni tok(ovi)
/

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot študent. Če ni prijavljen, ali pa če je prijavljen s katero drugo uporabniško vlogo, mu funkcionalnost ni na voljo.

#### Posledice
Uporabniku se bo na zaslonu prikazala stran s TO-DO seznami, ki jih je že dodal.

#### Posebnosti
Funkcionalnost ne zahteva nobene posebnosti.

#### Prioritete identificiranih funkcionalnosti
Must have.

#### Sprejemni testi
1. Prijavi se kot študent, izberi funkcionalnost pregleda TO-DO seznamov.



### FZ7: Dodajanje TO-DO seznama
#### Povzetek funkcionalnosti
Študent lahko izbere začetek in konec časovnega obdobja, sistem pa mu generira TO-DO seznam, ki je urejen po prioritetah aktivnosti.

#### Osnovni tok
1. Študent izbere funckionalnost Kreiraj TO-DO seznam.
2. Študent izbere začetek časovnega obdobja.
3. Študent izbere konec časovnega obdobja.
4. Študent pritisne gumb Ustvari in zgeneriran TO-DO seznam se doda k ostalim seznamom.

#### Alternativni tok
/

#### Izjemni tokovi
* Študent izbere časovno obdobje, ki ni veljavno, torej se konča preden bi se sploh začelo, sistem prikaže ustrezno obvestilo.
* Študent v izbranem časovnem obdobju nima nobene aktivnosti, TO-DO seznama ni mogoče generirati, sistem prikaže ustrezno obvestilo.

#### Pogoji
Študent mora imeti na svojem urniku vsaj 1 obveznost v izbranem časovnem obdobju. Če nima nobene obveznosti, mu funckionalnost ni na voljo.

#### Posledice
Če sistem uspešno generira TO-DO seznam, se študentu le-ta prikaže poleg ostalih že zgeneriranih seznamov.

#### Posebnosti
Funkcionalnost ne zahteva nobene posebnosti.

#### Prioritete identificiranih funkcionalnosti
Must have.

#### Sprejemni testi
* Študent doda v svoj urnik novo obveznost, izbere časovno obdobje, ki zajema tudi dan v katerem je ta obveznost in zgenerira nov TO-DO seznam.



### FZ8: Brisanje TO-DO seznama
#### Povzetek funkcionalnosti
Študent lahko izbriše že zgenerirane TO-DO sezname.

#### Osnovni tok (za vse uporabniške vloge je enak)
1. Študent pritisne križec v desnem kotu TO-DO seznama.
2. Sistem prikaže obvestilo, če je študent prepričan, da želi izbrisati TO-DO seznam.
3. Študent potrdi brisanje.
4. Sistem izbriše izbrani TO-DO seznam.

#### Alternativni tok
/

#### Izjemni tokovi
/

#### Pogoji
Študent mora imeti vsaj 1 že zgeneriran TO-DO seznam. Če ga nima, mu funckionalnost ni na voljo.

#### Posledice
Če študent uspešno izbriše TO-DO seznam, se le-ta odstrani iz seznama že zgeneriranih TO-DO seznamov.

#### Posebnosti
Funkcionalnost ne zahteva nobene posebnosti.

#### Prioritete identificiranih funkcionalnosti
Should have.

#### Sprejemni testi
* Uporabnik pritisne križec v kotu TO-DO seznama.



### FZ9: Pregled predmetov

#### Povzetek funkcionalnosti
Študent, starš ali profesor si lahko ogledajo seznam predmetov, ki jih imajo dodane.

#### Osnovni tok(študent)
1. Študent v glavnem meniju izbere funkcionalnost Predmeti. 
2. Sistem mu prikaže seznam predmetov, na katere je prijavljen.

#### Osnovni tok(starš)
1. Starš s pomočjo seznama v glavnem meniju izbere otroka.
2. Starš v glavnem meniju izbere funkcionalnost Predmeti. 
3. Sistem mu prikaže seznam predmetov, na katere je izbrani otrok prijavljen.

#### Osnovni tok(profesor)
1. Profesor v glavnem meniju izbere funkcionalnost Predmeti. 
2. Sistem mu prikaže seznam predmetov, ki jih izvaja.

#### Alternativni tok(ovi)
/

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot študent, starš, ali profesor. Če ni prijavljen, ali pa če je prijavljen kot administrator, mu funkcionalnost ni na voljo.

#### Posledice
Uporabniku se bo na zaslonu prikazala stran z ustreznim seznamom predmetov.

#### Posebnosti
Funkcionalnost ne zahteva nobene posebnosti.

#### Prioritete identificiranih funkcionalnosti
Must have.

#### Sprejemni testi
1. Prijavi se kot študent, izberi funkcionalnost pregleda predmetov.
2. Prijavi se kot starš, izberi otroka, izberi funkcionalnost pregleda predmetov.
3. Prijavi se kot profesor, izberi funkcionalnost pregleda predmetov.



### FZ10: Pregled Podrobnosti Predmeta
#### Povzetek funkcionalnosti
Študent, profesor ali starš si lahko ogledajo podrobnosti predmeta.

#### Osnovni tok (za vse uporabniške vloge je enak)
1. Uporabnik pritisne na določen predmet, ki si ga želi ogledati.
2. Sistem mu odpre podrobnosti predmeta(tj. ocene pri staršu in študentu, izpitne roke pri vseh, obveznosti pri vseh.)

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot študent, profesor ali starš. Profesor ali študen morata imeti predhodno dodan že kakšen predmet.

#### Posledice
Ogled podrobnosti predmeta npr. ocene, izpitne roke in ostale obveznosti.

#### Prioritete identificiranih funkcionalnosti
Must have.

#### Sprejemni testi
* Izberi predmet, ki si ga želiš ogledati in pritisni nanj.



### FZ11: Dodajanje predmetov
#### Povzetek funkcionalnosti
Študent ali profesor lahko dodajata nove predmete.

#### Osnovni tok(študent)
1. Študent izbere funkcionalnost Dodaj predmet.  
2. Sistem ponudi obrazec, ki omogoča upravljanje s predmeti.  
3. Študent izbere možnost da dodajanje predmeta ni s strani profesorja.
4. Študent izbere profesorja in predmet ali pa sam vpiše ime predmeta.   
5. Sistem študentu doda predmet v rubriko Moji predmeti.   
6. Sistem v redovalnico in urnik študentu doda predmet.  
7. Sistem izpiše obvestilo o uspešno odstranjenem/dodanem predmetu.  

#### Osnovni tok(profesor)
1. Profesor izbere funkcionalnost Dodaj predmet.  
2. Sistem ponudi obrazec, ki omogoča upravljanje s predmeti.  
3. Profesor izbere možnost da je dodajanje predmeta s strani profesorja.
4. Profesor izbere profesorja in predmet ali pa sam vpiše ime predmeta.   
5. Sistem profesorju doda predmet v rubriko Moji predmeti.   
6. Sistem izpiše obvestilo o uspešno odstranjenem/dodanem predmetu.  


#### Pogoji
Uporabnik mora biti v sistem prijavljen kot študent ali profesor. Če ni prijavljen ali pa če je prijavljen
kot starš ali administrator, mu funkcionalnost ni na voljo.

#### Posledice
Če študent oz. profesor uspešno doda nov predmet, se mu le ta prikaže v njegovi v rubriki Moji predmeti. Prav tako se mu dodajo obveznosti za ta predmet v urnik.

#### Posebnosti
Predmeti morajo biti poimenovani s celotnim in pravim imenom. Kratice niso dovoljene.

#### Prioritete identificiranih funkcionalnosti
Must have.

#### Sprejemni testi
1. Prijavi se kot študent, dodaj predmet in potrdi.    
Testira se funkcija, ki ob dodajanju predmeta, študentu doda predmet v redovalnico, prav tako doda vse obveznosti
povezane s tem predmetom. Ko študent doda predmet, sistem doda vse v povezavi s predmetom.



### FZ12: Odstranjevanje predmetov
#### Povzetek funkcionalnosti
Študent ali profesor lahko odstranjujeta predmete.

#### Osnovni tok(študent)
1. Študent izbere možnost Moji predmeti.  
2. Sistem ponudi seznam študentovih predmetov. 
3. Študent izbere x v zgornjem desnem kotu ob predmetu.
4. Sistem odstrani izbrani predmet.  
5. Sistem izpiše obvestilo o uspešno odstranjenem predmetu.  

#### Osnovni tok(profesor)
1. Profesor izbere možnost Moji predmeti.  
2. Sistem ponudi seznam profesorjevih predmetov. 
3. Profesor izbere x v zgornjem desnem kotu ob predmetu.
4. Sistem odstrani izbrani predmet.  
5. Sistem izpiše obvestilo o uspešno odstranjenem predmetu.  


#### Pogoji
Uporabnik mora biti v sistem prijavljen kot študent ali profesor. Če ni prijavljen ali pa če je prijavljen
kot starš ali administrator, mu funkcionalnost ni na voljo.

#### Posledice
Če študent oz. profesor uspešno odstrani predmet, se mu le ta izbriše v njegovi v rubriki Moji predmeti. Prav tako se mu izbrišejo obveznosti za ta predmet v urnik.

#### Posebnosti
Predmeti morajo biti poimenovani s celotnim in pravim imenom. Kratice niso dovoljene.

#### Prioritete identificiranih funkcionalnosti
Must have.

#### Sprejemni testi
1. Prijavi se kot študent, odstrani predmet in potrdi.    
Testira se funkcija, ki ob brisanju predmeta, študentu izbriše predmet iz redovalnice, prav tako odstrani vse obveznosti
povezane s tem predmetom. Ko študent odstrani predmet, sistem odstrani vse v povezavi s predmetom.



### FZ13: Dodajanje obveznosti predmeta
#### Povzetek funkcionalnosti
Študent ali profesor lahko pri izbranem predmetu dodata različne obveznosti.

#### Osnovni tok (študent)
1. Študent pritisne gumb dodaj obveznost predmeta.
2. Sistem mu ponudi obrazec za dodajanje obveznosti.
3. Študent izbere tip obveznosti, izpolni naziv obveznosti ter izbere datum in uro začetka in konca.
4. Študent izbere prioriteto.
5. Študent potrdi dodajanje obveznosti.
6. Sistem obveznost doda med podrobnosti predmeta in na ustrezno mesto v urnik.

#### Osnovni tok (profesor)
1. Profesor pritisne gumb dodaj obveznost predmeta.
2. Sistem mu ponudi obrazec za dodajanje obveznosti.
3. Profesor izbere tip obveznosti (za razliko od študenta le izpitni rok in predavanja), izpolni naziv obveznosti ter izbere datum in uro začetka in konca.
4. Profesor izbere prioriteto.
5. Profesor potrdi dodajanje obveznosti.
6. Sistem obveznost doda med podrobnosti predmeta in na ustrezno mesto v urnik.

#### Izjemni tokovi
* Profesor izbere termin obveznosti, ki se mu prekriva z že dodano obveznostjo, zato te ne more dodati. Izpiše se mu ustrezno sporočilo in ponudi ponovno izpolnjevanje obrazca.
* Študent ali profesor ne izpolneta vseh polj. Izpiše se ustrezno sporočilo in ponudi ponovno izpolnjevanje obrazca.

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot študent ali profesor. Profesor ali študen morata imeti predmet, za katerega izbirita obveznost že dodan.

#### Posledice
Uporabniku se obveznost doda med podrobnosti predmeta in na urnik.

#### Prioritete identificiranih funkcionalnosti
Must have.

#### Sprejemni testi
* Izberi dodajanje obveznosti, ustrezno izpolni vsa polja in dodaj obveznost.
* Prijavi se kot profesor in dodaj termin obveznosti, ki je enak že predhodno dodanemu.
* Ne izpolni vseh polj za dodajanje obveznosti.



### FZ14: Brisanje obveznosti predmeta
#### Povzetek funkcionalnosti
Študent ali profesor lahko pri izbranem predmetu izbrišeta obveznosti.

#### Osnovni tok (za obe vlogi isto)
1. Uporabik pritisne gumb za odstranjevanje obveznosti predmeta.
2. Sistem ga vpraša, če je v to prepičan.
3. Uporabnik izbere odgovor "da".
4. Sistem odstrani obveznost iz podrobnosti in iz urnika.

#### Izjemni tokovi
* Uporabnik pri vprašanju, če res želi odstraniti obveznost odgovori z "ne". Obveznost se ne odstrani.

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot študent ali profesor. Profesor ali študen morata imeti predmet, za katerega odstranjujeta obveznost že dodan.

#### Posledice
Uporabniku se obveznost odstrani iz podrobnosti predmeta in urnika.

#### Prioritete identificiranih funkcionalnosti
Must have.

#### Sprejemni testi
* Pritisni gumb za odstranjevanje obveznosti in odločitev potrdi.
* Pritisni gumb za odstranjevanje obveznosti in odločitev zavrni.



### FZ15: Vnos ocene
#### Povzetek funkcionalnosti
Študent lahko pri izbranem predmetu vnese oceno.

#### Osnovni tok 
1. Študent pritisne gumb za vnos ocene.
2. Sistem mu ponudi obrazec za vnos ocene.
3. Študent izpolni naziv ocene, vrednost in označi, ali je opravil ali ne.
4. Študent potrdi vnos ocene.
5. Sistem doda ocene pod ocene predmeta.

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot študent. Študent morata imeti predmet, za katerega vnaša oceno že dodan.

#### Posledice
Uporabniku vpiše ocena pod podrobnosti predmeta (ocene). Če je študent označil oceno z opravljeno in so vse ostale ocene označene enako, predmet ostane pri pregledu predmetov zelene barve,
v nasprotnem primeru se predmet obarva rdeče.

#### Prioritete identificiranih funkcionalnosti
Must have.

#### Sprejemni testi
* Pritsni gumb za vnos ocene, izpolni obrazec in izbiro potrdi.



### FZ16: Brisanje ocene
#### Povzetek funkcionalnosti
Študent lahko pri izbranem predmetu izbriše oceno.

#### Osnovni tok 
1. Študent pritisne gumb za brisanje ocene.
2. Sistem ga vpraša, če je v to prepičan.
3. Uporabnik izbere odgovor "da".
4. Sistem odstrani oceno iz podrobnosti predmeta.

#### Izjemni tokovi
* Študent pri vprašanju, če res želi odstraniti oceno odgovori z "ne". Ocena se ne odstrani.

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot študent. Študent morata imeti predmet, za katerega briše oceno že dodan.

#### Posledice
Uporabniku se izbriše ocena pod podrobnostmi predmeta (ocene). Če je študent izbrisal oceno, ki je bila označena z neopravljeno, vse ostale pa so opravljene, se predmet obarva zeleno, drugače ostane rdeč.

#### Prioritete identificiranih funkcionalnosti
Must have.

#### Sprejemni testi
* Pritsni gumb za brisanje ocene in izbiro potrdi.
* Pritsni gumb za brisanje ocene in izbiro zavrni.



### FZ17: Izbira izpitnega roka
#### Povzetek funkcionalnosti
Študent lahko pri izbranem predmetu izbere izpitni rok.

#### Osnovni tok 
1. Študent pritisne gumb za izbiro izpitnega roka.
2. Sistem odpre obrazec za izbiro roka.
3. Študent izbere enega od dodanih rokov.
4. Študent potrdi izbiro.
5. Sistem doda izpitni rok na urnik študentu z najvišjo prioriteto, ostale roke pa odstrani in urnika.

#### Izjemni tokovi
* Študent izbere rok, katerga datum je že zapadel. V tem primeru se mu ponudi ponovna izbira roka.

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot študent. Študent morata imeti predmet in izpitne roke, za katerega izbira izpitni rok že dodane.

#### Posledice
Študentu se doda izpitni rok na urnik z najvišjo prioriteto, ostale roke pa odstrani in urnika.

#### Prioritete identificiranih funkcionalnosti
Could have.

#### Sprejemni testi
* Pritsni gumb za izbiro izpitnega roka, izberi rok in izbiro potrdi.
* Izberi rok, katerga datum je že zapadel.



### FZ18: Potrjevanje uporabniških računov
#### Povzetek funkcionalnosti
Administrator ima lahko pregled nad vsemi uporabniki, pri tem so posebej označeni še nepotrjeni uporabniki, ki jih administrator s klikom potrdi, če so njihovi podatki ustrezni.

#### Osnovni tok(administrator)
1. Administrator izbere funkcionalnost Seznam uporabnikov.
2. Sistem mu vrne obrazec, ki vsebuje seznam vseh uporabnikov, na katerem so rdeče označeni še nepotrjeni uporabniki. Vsak vnos nepotrjenega uporabnika vsebuje možnost potrditve uporabnika.
3. Administrator izbere možnost potrditve uporabnika.
4. Sistem potrdi uporabnika tako, da ga doda na seznam uporabnikov brez rdeče označbe in brez možnosti za potrditev.

#### Alternativni tok(ovi)
/

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot administrator. Če ni prijavljen, ali pa če je prijavljen s katero drugo uporabniško vlogo mu funkcionalnost ni na voljo.

#### Posledice
Če administrator uspešno potrdi uporabnika, se mu ta normalno prikaže v seznamu uporabnikov ( brez rdeče označbe, možnosti potrditve ni več ).

#### Posebnosti
Funkcionalnost ne zahteva nobene posebnosti.

#### Prioritete identificiranih funkcionalnosti
Would have.

#### Sprejemni testi
1. Prijavi se kot administrator, izberi funkcionalnost potrditve še nepotrjenega uporabnika.



### FZ19:  Brisanje uporabniških računov
#### Povzetek funkcionalnosti
Administrator ima lahko pregled nad vsemi uporabniki, pri tem so posebej označeni še nepotrjeni uporabniki. Administrator lahko katerega koli uporabnika s klikom izbriše, če ta nima ustreznih podatkov.

#### Osnovni tok(administrator)
1. Administrator izbere funkcionalnost Seznam uporabnikov.
2. Sistem mu vrne obrazec, ki vsebuje seznam vseh uporabnikov, na katerem so rdeče označeni še nepotrjeni uporabniki. Vsak vnos uporabnika vsebuje možnost izbrisa uporabnika.
3. Administrator izbere možnost izbrisa uporabnika.
4. Sistem preko okenca Administratoja vpraša, če res želi izbrisati uporabnika.
4. Če Administrator odgovori z da, sistem iz seznama uporabnikov izbriše ustreznega uporabnika, sicer sistem vrne enak seznam uporabnikov. 

#### Alternativni tok(ovi)
/

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot administrator. Če ni prijavljen, ali pa če je prijavljen pod katero drugo uporabniško vlogo mu funkcionalnost ni na voljo. 

#### Posledice
Če administrator uspešno izbriše uporabnika, se mu ta odstrani iz seznama uporabnikov. 

#### Posebnosti
Funkcionalnost ne zahteva nobene posebnosti.

#### Prioritete identificiranih funkcionalnosti
Must have.

#### Sprejemni testi
1. Prijavi se kot administrator, izberi funkcionalnost brisanja nepotrjenega uporabnika.
2. Prijavi se kot administrator, izberi funkcionalnost brisanja potrjenega uporabnika.



### FZ20: Dodeljevanje in odvzem dostopa staršu
#### Povzetek funkcionalnosti
Študent lahko doda starša in mu kadarkoli odvzame ali ponovno dodeli dostop do svojega urnika in redovalnice.

#### Osnovni tok (študent)
1. Študent izbere funkcionalnost Starši.  
2. Sistem ponudi seznam staršev in izbiro za dodajanje starša.  
3. Študent izbere ustreznega starša ali pa doda novega.   
4. Sistem študentu ponudi izbiro dodeljevanja in odvzemanja pravic staršu.   
5. Student dodeli/odvzame pravice do dostopa staršu in potrdi izbiro.  
6. Sistem izpiše obvestilo o uspešno dodeljenih/odvzetih pravicah.  


#### Alternativni tok 
/

#### Pogoji
Uporabnik mora biti v sistem prijavljen kot študent. Če ni prijavljen ali pa če je prijavljen
kot administrator, profesor ali starš mu funkcionalnost ni na voljo.

#### Posledice
Če študent uspešno dodeli/odvzame pravice do dostopa staršu, le temu omogoči/onemogoči ogled svojega urnika, redovalnice in dodajanje obveznosti.

#### Posebnosti
Funkcionalnost ne zahteve nobene posebnosti.

#### Prioritete identificiranih funkcionalnosti
Must have.

#### Sprejemni testi
1. Prijavi se kot študent dodeli/odvzemi pravice staršu in potrdi.     
Testira se funkcija, ki ustrezno omogoči/onemogoči staršu dostop do študentovega urnika ali redovalnice. Študent ali
administrator staršu odstranita pravice, sistem pa mu onemogoči dostop.  




## 6. Nefunkcionalne zahteve

1. Sistem mora biti zmožen streči najmanj 200 hkratnim uporabnikom.
Ker je študentov v Ljubljani veliko, more biti strežnik zmožen streči najmanj 200 uporabnikom hkrati, ker menimo, da bo maksimalno okoli 200 študentov na isti čas dostopalo do aplikacije.

2. Sistem mora biti na voljo najmanj 99% časa, vse dni v tednu.
Sistem mora biti na voljo študentov večino časa, saj ga bodo uporabljali skozi celo leto, kadarkoli tekom dneva, vendar pa še vedno obstaja majhna možnost odpovedi sistemi, ki bi lahko povzročila nedostopnost vsake toliko časa za kakšno uro ali celo dve.

3. Sistem mora odgovoriti na vsako zahtevo v največ 200 milisekundah.
Dobro je, da uporabnike ne pustimo čakati pri interakciji s sistemom, saj lahko to povzroči slabo uporabniško izkušnjo in posledično manj aktivnih uporabnikov.

4. Sistem mora uporabljati podatkovno bazo, ki ne presega 100MB velikosti.
Ker so naši viri omejeni, naša podatkovna baza za učinkovito delovanje ne sme presegati 100MB velikosti.

5. Ob izpadu sistema mora biti verjetnost, da pride do izgube podatkov, manj kot 1%.
Podatki morajo biti ustrezno zavarovani in imeti varnostno kopijo, saj izpad sistema ni ne mogoč, podatki pa se ne smejo izgubiti, saj bi to omogočilo propad sistema.

6. Sistem mora biti zgrajen s pomočjo MVC in MEAN arhitekture.
Takšna arhitektura ustreza našemu tipu aplikacije in se bo za njene namene dobro odnesla.

7. Sistem mora biti dosegljiv na javno dostopnem spletnem naslovu.
Vsem z dostopom do interneta more biti omogočena uporaba spletne aplikacije.

8. Sistem mora zadoščati slovenski zakonodaji.
Pri naši aplikaciji ne sme priti do spora z zakonom tj. različnih tožb, ki bi zavirale uporabo in razvoj aplikacije.

9. Sistem uporabniku ne sme omogočiti dostopa do podatkov, za katere ni izrecno pooblaščen.
Aplikacija mora določene podatke hraniti zasebno, da ne bi prišlo do zlorabe osebnih podatkov, ki jih uporabniki zaupajo aplikaciji.




## 7. Prototipi vmesnikov

### Zaslonske maske 

#### ZM1: Vpis
![Vpis](../img/Vpis.png)

#### ZM2: Registracija
![Registracija](../img/Registracija.png)

#### ZM3: Pregled urnika (študent)
![Pregled urnika (študent)](../img/Pregled_urnika_študent.png)

#### ZM3: Pregled urnika (starš)
![Pregled urnika (starš)](../img/Pregled_urnika_starš.png)

#### ZM3: Pregled urnika (profesor)
![Pregled urnika (profesor)](../img/Pregled_urnika_profesor.png)

#### ZM4: Dodajanje obveznosti  
![Maska za dodajanje obveznosti](../img/dodaj-obveznost.png)

#### ZM5: Odstranjevanje obveznosti
* Ko bo neka obveznost prikazana v urniku, bo imela v desnem zgornjem kotu gumbek X, s katerim se obveznost odstrani.
![Odstranjevanje obveznosti](../img/Pregled_urnika_študent.png)

#### ZM6: Pregled TO-DO seznamov
![TO-DO seznami](../img/TO-DO_seznami.png)

#### ZM7: Dodajanje TO-DO seznama
![Dodajanje TO-DO seznama](../img/TO-DO_seznami_popup.png)

#### ZM8: Brisanje TO-DO seznama
![Brisanje TO-DO seznama](../img/TO-DO_seznami_popup_brisanje.png)

#### ZM9: Pregled predmetov(študent)
![Pregled predmetov(študent)](../img/PregledPredmetovŠtudent.png)

#### ZM9: Pregled predmetov(profesor)
![Pregled predmetov(profesor)](../img/PregledPredmetovProfesor.png)

#### ZM9: Pregled predmetov(starš)
![Pregled predmetov(starš)](../img/PregledPredmetovStarš.png)

#### ZM10: Pregled podrobnosti predmeta(študent)
![Podrobnosti Predmeta(študent)](../img/PodrobnostiStud.png)

#### ZM10: Pregled podrobnosti predmeta(starš)
![Podrobnosti Predmeta(starš)](../img/PodrobnostiPredmetaStarš.png)

#### ZM10: Pregled  podrobnosti predmeta(profesor)
![Podrobnosti Predmeta(profesor)](../img/PodrobnostiPredmetaProf.png)

#### ZM11: Dodajanje predmetov(študent)
![Maska za dodajanje predmeta](../img/dodaj-predmet.png)

#### ZM12: Odstranjevanje predmetov(študent)
![Odstranjevanje predmetov](../img/PregledPredmetovŠtudent.png)

#### ZM13: Dodajanje obveznosti predmeta(študent)
![Obveznost Predmeta(študent)](../img/DodajObvStud.png)

#### ZM13: Dodajanje obveznosti predmeta(profesor)
![Obveznost Predmeta(profesor)](../img/ObveznostPredmetaProf.png)

#### ZM14: Brisanje obveznosti predmeta
![Brisanje obveznosti predmeta](../img/PodrobnostiStud.png)

#### ZM15: Vnos ocene
![Vnos ocene](../img/VnosOcene.png)

#### ZM16: Brisanje ocene
![Brisanje ocene](../img/PodrobnostiStud.png)

#### ZM17: Izbira izpitnega roka
![Izberi rok](../img/IzberiRok.png)

#### ZM18: Potrjevanje uporabniških računov
![Potrjevanje uporabniških računov)](../img/PotrjevanjeUporabniškihRačunov.png)

#### ZM19: Brisanje uporabniških računov
![Brisanje uporabniških računov)](../img/BrisanjeUporabniškihRačunov.png)

#### ZM20: Dodajanje in odvzemanje pravic staršem
![Dodajanje pravic staršem)](../img/starsi.png)

## Prototipi vmesnikov

V naši aplikaciji bomo uporabili tudi zunanji sistem in sicer prijavo oz. registracijo z Googlovim računom,
če ga uporabnik naše aplikacije ima.

Uporabili bomo Googlov API, dodali njihov sign-in gumb ter klicali funkcijo getBasicProfile().
Ta funkcija nam bo vrnila uporabnikov ID, ime, email ter sliko, ki pa je ne bomo uporabili.

