# Načrt sistema

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Nik Bratuša, Sara Koljić, Žan Pečovnik, Žiga Kleine |
| **Kraj in datum** | Ljubljana, 22. 4. 2019 |



## Povzetek

Pri 3. lastnem projektu smo sestavili načrt sistema aplikacije StraightAs. Dokument je razdeljen na 3 glavne dele,  in sicer načrt arhitekture, načrt strukture in načrt obnašanja.

Pri načrtu arhitekture smo poskusili intuitivno prikazati arhitekturo programa z uporabo razvojnega in logičnega pogleda.

Načrt strukture smo predstavili z razrednim diagramom, na katerem so označeni objekti ali razredi, ki jih vsebuje aplikacija, ter povezave med njimi.
Za vsak razred smo še navedli njegove atribute in opisali nesamoumevne metode, ki jih razred vsebuje.

Za osnovo načrta obnašanja smo vzeli diagram primerov uporabe iz 2. lastnega projekta. Za vsak primer uporabe sistema smo narisali enega ali več diagramov zaporedja,
ki prikazujejo tokove dogodkov pri uporabi sistema.


## 1. Načrt arhitekture

### Logični pogled
![Logični pogled](../img/logicniPogled.png)

### Razvojni pogled
![Razvojni pogled](../img/razvojniPogled.png)

## 2. Načrt strukture

### 2.1 Razredni diagram

![Razredni diagram](../img/razredniDiagram-zvezdice.png)


#### Uporabnik

#### Atributi
Objekt razreda Uporabnik predstavlja uporabnika, ki uporablja sistem. Razred vsebuje sledeče atribute:

* Id : int  
* ime : string  
* priimek : string  
* email : string  
* geslo : string  

#### Nesamoumevne metode

* vrniUporabnika(String email) - Metoda sprejme email, ki ga uporabnik vnese. Vrne objekt Uporabnik. Ta objekt uporabimo za preverjanje gesla uporabnika.
* dodajUporabnika(String email, String ime, String priimek, String vloga) - Metoda sprejme podane parametre in vrne objekt Uporabnik, ki se shrani v bazo. Ne vrača nič.
* vrniSeznamUporabnikov() - Metoda vrne vse registrirane uporabnike. Ne vrača nič.
* izbrisiUporabnika(int idUporabnika) - Metoda izbriše uporabnika s podanim id-jem, ki ga administrator ni potrdil. Ne vrača nič.

#### Študent

#### Atributi
Objekt razreda Študent razširja objekt razreda Uporabnik, ki uporablja sistem. Razred vsebuje sledeče atribute:

* TO-DO Seznami : [] TO-DO Seznam  
* Obveznosti : [] Obveznost  
* Predmeti : [] Predmet  
* Starši : [] Starši  

#### Nesamoumevne metode

* vrniSeznamOtrokStarsa(int idStars) - Metoda sprejme podan id in vrne vse študente, ki imajo shranjenega starša s tem id-jem. Vrača torej seznam tipa Študent.

#### TO-DOseznam

#### Atributi
Objekt razreda TO-DO seznam vsebuje sledeče atribute:

* Id : int  
* začetek : Date  
* konec : Date  
* Obveznosti : [] Obveznost  
* ObveznostiPredmeta : [] ObveznostPredmeta  

#### Nesamoumevne metode

* vrniVseTodoSeznameUporabnika(int idUporabnika) - Metoda sprejme podan id in vrne seznam tipa TO-DO seznam.
* dodajTODOSeznam(int zacetek, int konec, Obveznost [] obveznosti, ObveznostiPredmeta [] obveznostiP, int idStudenta) - Metoda sprejme podane parametre, ustvari objekt tipa TO-DOseznam in ga doda k študentu z danim id-jem. Torej ne vrača nič, samo spreminja bazo.
* izbrisiTODOSeznam(int idSeznama, int idStudenta) - Metoda sprejme podana parametra, poišče študenta in podan seznam ter ga izbriše. Torej ne vrača nič, le spremeni bazo.

#### Obveznost

#### Atributi
Objekt razreda Obveznost vsebuje sledeče atribute:

* Id : int  
* naziv : String  
* opis : String  
* datum : Date  
* casZacetka : Time  
* casKonca : Time  
* prioriteta : int  (Zaloga: 1-4)

#### Nesamoumevne metode

* vrniVseObveznostiUporabnika(ind idUporabnika) - Metoda sprejme podan ID in poišče vse obveznosti(Obveznost in ObveznostPredmeta). Obveznosti tipa ObveznostPredmeta pretvori v tip Obveznost in vrne seznam tipa Obveznost.
* odstraniObveznost(int idObveznosti, int idUporabnika) - Metoda sprejme podan id in obveznost izbriše. Metoda ne vrača nič, le spremembe baze.
* dodajObveznostVUrnika(String naziv, String opis, int datum, int casZacetka, int casKonca,int prioriteta, int idUporabnika) - Metoda sprejme podane parametre, ustvari objekt tipa Obveznost in obveznost doda na urnik uporabika s podanima id-jem(Torej pod njegove Obveznosti). Metoda ne vrača nič, le spremembe baze.

#### Predmet

#### Atributi
Objekt razreda Predmet vsebuje sledeče atribute:

* Id : int  
* ime : String  
* ocene : [] Ocena  
* obveznostiPredmeta : [] ObveznostPredmeta  

#### Nesamoumevne metode

* vrniVsePredmeteUporabnika(int idUporabnika) - Metoda sprejme podan parameter in vrne seznam tipa Predmet.
* vrniPodrobnostiPredmeta(int idUporabnika, int idPredmeta) - Metoda sprejme podane parametre, poišče predmet in vrne samo en objekt tipa Predmet. Iz njega potem kasneje prikažemo podrobnosti.
* dodajPredmet(int idUporabnika, String ime) - Metoda sprejme podana parametra in ustvari tip Predmet s podanim imenom in praznima seznamom ocen in obveznosti. Metoda ne vrača nič, le predmet doda v bazo.
* odstraniPredmet(int idUporabnika, int idPredmeta) - Metoda sprejme podana parametra, pri uporabnika poišče predmet s podanim idjem in ga odstrani iz baze. Metoda ne vrača nič.

#### ObveznostPredmeta

#### Atributi
Objekt razreda ObveznostPredmeta vsebuje sledeče atribute:

* Id : int  
* vrsta : String  
* naziv : String  
* datumZacetka : Date  
* datumKonca : Date  
* uraZacetka : Time  
* uraKonca : Time  
* prioriteta : int (Zaloga: 1-4)
* jeVidnaNaUrniku : boolean

#### Nesamoumevne metode

* izbrisiObveznostPredmeta(int idObveznosti, int idPredmeta, int idUporabnika) - sprejme podane id-je in obveznost izbriše. Metoda ne vrača nič, le spremembe baze.
* dodajObveznostPredmeta(String vrsta, String naziv, int datumZac, int datumKonca, int uraZacetka, int uraKonca, int prioriteta, boolean jeVidnaNaUrniku, int idPredmeta, int idUporabnika) - sprejme podane parametre, ustvari objekt tipa ObveznostPredmeta in obveznost doda k predmetu(id v parametrih), ki pripradu študentu z podanim id-jem. Metoda ne vrača nič, le spremembe baze.
* vrniIzpitneRoke(int idPredmeta, int idStudenta) - sprejme podane id-je, preveri med obveznostimi katerih vrsta je izpitni rok in le te vrne. Metoda torej vrača tabelo tipa ObveznostPredmeta.
* dodajIzpitniRokVUrnik(int idObveznosti, int idStudenta) - sprejme podana id-ja in izpitni rok doda na urnik študenta(Spremeni atribut jeVidnaNaUrniku na true, ostalim rokom pa na false). Metoda ne vrača nič.

#### Ocena

#### Atributi
Objekt razreda Ocena vsebuje sledeče atribute:

* Id : int  
* naziv : String  
* vrednost : String  
* opravljeno : boolean  

#### Nesamoumevne metode

* izbrisiOceno(int idOcene, int idPredmeta, int idStudenta) - sprejme navedene id-je in izbriše oceno. Metoda ne vrača nič, le spremembe baze.
* vnosOcene(String naziv, String vrednost, boolean opravljeno, int idPredmeta, int idStudenta) - sprejem podane parametre, ustvari objekt tipa Ocena in doda oceno študentu(id v parametrih) določenemu predmetu(id v parametrih). Metoda ne vrača nič, le spremembe baze.
* preglejVseOcene(int idPredmeta) - pri določenem predmetu preveri ali so vse ocene označene z opravljeno ali ne. Če so, potem vrne True, drugače False. Metoda torej vrača boolean.

#### Profesor

#### Atributi
Objekt razreda Profesor vsebuje sledeče atribute:

* Predmeti : [] Predmet  

#### Nesamoumevne metode

/

#### Starši

#### Atributi
Objekt razreda Starši vsebuje sledeče atribute:

* Studenti : [] Student  

#### Nesamoumevne metode

* vrniStarse(int idStudenta) - Metoda sprejme podan id in vrne vse starše, ki jih ima študent dodane. Metoda torej vrača seznam tipa Starš.
* dodajStarsa(int idStudenta, String email) - Metoda sprejme podana parametra, med registriranimi uporabniki poišče starša z podanim emailom in ga doda na seznam staršev k študentu. Metoda ne vrača nič, le sprememba baze.
* shraniPravice(int idStudenta, int idStarša) - Metoda sprejem podana parametra in staršu s podanim id-jem podeli izbrane pravice. Metoda ne vrača nič, le spremeni bazo.

#### Administrator

#### Atributi
Objekt razreda Administrator vsebuje sledeče atribute:

* Uporabniki : [] Uporabnik   

#### Nesamoumevne metode

/


## 3. Načrt obnašanja

### FZ1: Vpis - osnovni tok
![vpis](../img/Vpis-osnovni-tok.png)

### FZ1: Vpis - alternativni tok
![vpis](../img/Vpis-alternativni-tok.png)

### FZ1: Vpis - izjemni tok 1
![vpis](../img/Vpis-izjemni-tok1.png)

### FZ1: Vpis - izjemni tok 2
![vpis](../img/Vpis-izjemni-tok2.png)

### FZ1: Vpis - izjemni tok 3
![vpis](../img/Vpis-izjemni-tok3.png)

### FZ2: Registracija - osnovni tok
![Registracija](../img/Registracija-osnovni-tok.png)

### FZ2: Registracija - alternativni tok
![Registracija](../img/Registracija-alternativni-tok.png)

### FZ2: Registracija - izjemni tok 1
![Registracija](../img/Registracija-izjemni-tok1.png)

### FZ2: Registracija - izjemni tok 2
![Registracija](../img/Registracija-izjemni-tok2.png)

### FZ3: Pregled urnika - osnovni tok(študent, profesor)
![Pregled urnika študent](../img/PregledUrnikaŠtudentDiagram.png)
* Kot akter je lahko tudi profesor.

### FZ3: Pregled urnika - osnovni tok(starš)
![Pregled urnika starš](../img/PregledUrnikaStaršDiagram.png)

### FZ4: Dodajanje obveznosti - osnovni tok
![dodajanje obveznosti](../img/DodajanjeObveznostiDZ.png)  

### FZ4: Dodajanje obveznosti - alternativni tok
![dodajanje obveznosti](../img/DodajanjeObveznostiAlternativniDZ.png)  

### FZ5: Odstranjevanje obveznosti  
![odstranjevanje obveznosti](../img/OdstranjevanjeObveznostiDZ.png)

### FZ6: Pregled TO-DO seznamov - osnovni tok(študent)
![Pregled TO-DO seznamov](../img/PregledTODOSeznamovDiagram.png)

### FZ7: Dodajanje TO-DO seznama - osnovni tok
![Dodajanje TO-DO seznama](../img/DodajanjeTODOSeznama-osnovni-tok.png)

### FZ7: Dodajanje TO-DO seznama - izjemni tok 1
![Dodajanje TO-DO seznama](../img/DodajanjeTODOSeznama-izjemni-tok1.png)

### FZ7: Dodajanje TO-DO seznama - izjemni tok 2
![Dodajanje TO-DO seznama](../img/DodajanjeTODOSeznama-izjemni-tok2.png)

### FZ8: Brisanje TO-DO seznama - osnovni tok
![Brisanje TO-DO seznama](../img/BrisanjeTODOSeznama-osnovni-tok.png)

### FZ9: Pregled predmetov - osnovni tok(študent, profesor)
![Pregled predmetov študent](../img/PregledPredmetovŠtudentDiagram.png)
* Kot akter je lahko tudi profesor.

### FZ9: Pregled predmetov - osnovni tok(starš)
![Pregled predmetov starš](../img/PregledPredmetovStaršDiagram.png)

### FZ10: Pregled podrobnosti predmeta
![Pregled podrobnosti predmeta](../img/PodrobnostiPredmetaDiagram.png)

* Osnovni tok je za vse vloge te funkcionalnosti enak.

### FZ11: Dodajanje predmetov  
![dodajanje predmetov](../img/DodajPredmetDZ.png)

* Kot akter je lahko tudi profesor. Edina razlika je v tem, da profesor označi izbiro dodajanja predmeta s strani profesorja.

### FZ12: Odstranjevanje predmetov  
![odstranjevanje predmetov](../img/OdstraniPredmetDZ.png)

* Kot akter je lahko tudi profesor.

### FZ13: Dodajanje obveznosti predmeta
![Dodajanje obveznosti predmeta](../img/DodajObveznostPredmetuDiagram.png)

* Kot uporabnik je lahko tudi profesor. Razlika je v tem da lahko doda samo predavanje in izpitne roke.

### FZ14: Brisanje obveznosti predmeta
![Brisanje obveznosti predmeta](../img/BrisanjeObveznostiDiagram.png)

* Za obe vlogi enako.

### FZ15: Vnos ocene
![Vnos ocene](../img/VnosOceneDiagram.png)

### FZ16: Brisanje ocene
![Brisanje ocene](../img/BrisanjeOceneDiagram.png)

### FZ17: Izbira izpitnega roka
![Izbira izpitnega roka](../img/IzbiraRokaDiagram.png)

### FZ18: Potrjevanje uporabniških računov - osnovni tok(administrator)
![Potrjevanje uporabniških računov](../img/PotrditevUporabnikaDiagram.png)

### FZ19: Brisanje uporabniških računov - osnovni tok(administrator)
![Brisanje uporabniških računov](../img/IzbrisUporabnikaDiagram.png)

### FZ20: Dodeljevanje in odvzem dostopa staršem  
![dodajanje pravis starsem](../img/PraviceStarsevDZ.png)

