# Načrt sistema

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Žan Horvat, Filip Grčar, Anže Gorjan Novak in Gašper Vrhovnik |
| **Kraj in datum** | Kamnik, 23. 4. 2019 |



## Povzetek

Dokument opisuje naše načrtovalske rešitve glede na zajem zahtev aplikacije StraightAs. Cilj dokumenta je, da glede na podane zahteve začrta rešitev, ki bo smiselno razčlenjena in jo bo lahko vzdrževati ter hkrati ugodi vsem zahtevam.

Glede na podane zahteve in želeno obliko implementacije v obliki spletne aplikacije smo se odločili za arhitekturni vzorec model-pogled-krmilnik. Tega smo ustrezno predstavili tudi v logičnem in razvojnem pogledu, katera sta tudi v nadaljevanju ustrezno grafično predstavljena. V razvojnem pogledu smo razrede ločili v smiselne komponente, ki združujejo funkcionalnosti kot so upravljanje, generiranje vsebine ter urejanje vlog možnih uporabnikov.

Pri načrtovanju strukture smo sestavili razredni diagram in si pri tem pomagali s prej omenjenim razvojnim pogledom. Ta prikazuje povezave med posameznimi entitetnimi, kontrolnimi in mejnimi razredi.

Načrt obnašanja vsebuje diagram zaporedja za vsako podano funkcionalno zahtevo. Podrobnosti vseh atributov in ne-samoumevnih metod so opisane v načrtu strukture.



## 1. Načrt arhitekture

### Logični pogled
![Blokovni diagram Logični pogled](../img/nacrt_arhitekture/Logicni_diagram.svg)

### Razvojni pogled
![Blokovni diagram Razvojni pogled](../img/nacrt_arhitekture/Razvojni_diagram.svg)


## 2. Načrt strukture

V spodnjem diagramu so predstavljeni vsi razredi s svojimi atributi in metodami, ter povezavami med njimi. Kontrolni razredi pred vsako izvedbo metod najprej preverijo, če je uporabnik, ki je klical metodo prijavljen in ali ima vlogo, ki dovoli izvajanje te metode. Kontrolni razredi imajo tudi vedno dostop do konteksta v katerem so podatki uporabnika, ki je poslal zahtevo, zato v metodah kontrolnih razredov nikjer niso navedeni parametri uporabnikovih podatkov. V diagramu se kot tip rezultata določenih metod pojavlja `status`. To je objekt z atributoma `HTTPkoda: int` in `opis: String`. `HTTPkoda`, ki je _HTTP response status code_, se uporabi pri pošiljanju odgovora odjemalcu, `opis` pa je opis napake, v primeru da je do nje prišlo.

### 2.1 Razredni diagram
![Blokovni diagram Razredni diagram](../img/Razredni_diagram.svg)


### 2.2 Opis razredov

Tukaj so opisani netrivialni atributi in metode.

#### Aktivnost

* Predstavlja posamezna opravila na koledarju ali TODO seznamu.

##### Atributi

* obseg: ocena težavnosti med 1 in 10

##### Nesamoumevne metode

* `pridobiAktivnostiObdobja(datumOd: Date, datumDo: Date): Aktivnost[]` metoda vrne vse aktivnosti, ki se končajo v podanem intervalu.
* `izbrisiOznakoAktivnostim(idOznake: int): void` metoda odstrani izbrano oznako vsem aktivnostim, ki jo vsebujejo.
* `najdiAktivnost(idOznak: int[], delNaziva: String)` metoda vrne seznam aktivnosti, ki imajo oznako z `id`-jem, ki je v `idOznak[]`, ali pa naziv vsebuje `delNaziva`.

#### UporavljanjeAktivnosti

##### Nesamoumevne metode

* `preveriVeljavnost(Aktivnost): boolean` metoda preveri če so vsa polja `Aktivnost`-i, ki je podana kot parameter, prisotna in veljavna.
* `uvoziDatoteko(datoteka: File): status` metoda razčleni datoteko v formatu iCalendar in dobljene dogodke shrani kot aktivnosti študenta.
* `izvozKoledarja(): File` metoda vse aktivnosti študenta zapiše v datoteko v formatu iCalendar.

#### PrikazMaskZVsebino

* Predstavlja pogled znotraj MCV arhitekture.

##### Atributi

* JWT: JSON Web Token za vzdrževanje seje uporabnika v katerem so shranjeni tudi podatki uporabnika
* TrenutniUporabnik

##### Nesamoumevne metode

* `prikaziMeni(): void` metoda se kliče ob kliku na gumb za glavni meni in prikaže meni.
* `novaAktivnost(): void` metoda se kliče ob kliku na gumb za dodajanje aktivnosti v glavnem meniju in prikaže formo za vnos aktivnosti.
* `koledar(): void` metoda se kliče ob kliku na gumb za prikaz koledarja v glavnem meniju in proži funkcionalnost prikaza koledarja.
* `uvozKoledarja(): void` metoda se kliče ob kliku na gumb za uvoz koledarja v glavnem meniju in proži funkcionalnost uvoza koledarja.
* `izvozKoledarja(): void` metoda se kliče ob kliku na gumb za izvoz koledarja v glavnem meniju in proži funkcionalnost izvoza koledarja.
* `GoogleKoledar(): void` metoda se kliče ob kliku na gumb za povezavo z Google koledarjem v glavnem meniju in proži funkcionalnost integracije Google koledarja.
* `prikaziPrijavo(): void` metoda se kliče ob kliku na gumb za prijavni obrazec, kateri se nato prikaže.
* `prikaziOcene(): void` metoda se kliče ob kliku na gumb za ocene v glavnem meniju in proži funkcionalnost prikaza ocen.
* `odjaviUporabnika(): void` metoda se kliče ob kliku na gumb za odjavo in odjavi uporabnika iz sistema.
* `prikaziRegistracijo(): void` metoda se kliče ob kliku na gumb za registracijo in prikaže obrazec za registracijo.
* `prikaziTODO(): void` metoda se kliče ob kliku na gumb na maski TODO seznamov in proži funkcionalnost generiranja TODO seznama.
* `isciAktivnosti(): void` metoda se kliče ob kliku na gumb na maski za iskanje aktivnosti in proži funkcionalnost iskanja aktivnosti glede na oznako oz. ime.
* `dodajOceno(): void` metoda se kliče ob kliku na gumb na maski za prikaz ocen in prikaže obrazec za vnos nove ocene.
* `obrazecAktivnosti(): void` metoda se proži ob klikih na gumbe znotraj obrazca za vnos in urejanje aktivnosti.
* `obrazecOcen(): void` metoda se proži ob klikih na gumbe znotraj obrazcev za vnos in brisanje aktivnosti.
* `obrazecAdmin(): void` metoda se proži ob klikih na gumbe znotraj obrazcev admina.
* `obrazecAvtorizacije(): void` metoda se proži ob klikih na gumbe za prijavo in registracijo.

#### UpravjanjeGoogleSinhronizacije

* Predstavlja mejni razred za komunikacijo z Googlovimi strežniki za namene sinhronizacije Google koledarjev premium študentov.

##### Nesamoumevne metode

* `pridobiUrlZaAvtorizacijo(): URL` metoda vrne URL spletne strani Googlovega strežnika za avtorizacijo, kjer študent lahko dovoli dostop aplikaciji StraightAs do svojega koledarja za namene sinhronizacije
* `zamenjajKodoZaŽetona(koda: String)` metoda je namenjena implementaciji Google OAuth 2.0 avtorizacije. Metoda se kliče ko študent potrdi dostop aplikaciji in ga Googlova spletna stran preusmeri nazaj v aplikacijo StraightAs. Parameter “koda” je avtorizacijska koda, ki jo pošljejo Googlovi strežniki skupaj z zahtevo po preusmeritvi. Metoda nato pošlje Googlovim avtorizacijskim strežnikom to kodo in v odgovoru dobi žeton za dostop in žeton za osveževanje, ki ju nato shrani za nadaljno uporabo pri pošiljanju zahtevkov za pridobivanje dogodkov v Google koledarju.  
* `vklopiSinhronizacijo(koledar: URL)` metoda shrani URL Google koledarja za sinhronizacijo in izvede začetno pridobivanje vseh dogodkov koledarja.
* `sinhroniziraj()` metoda pošlje poizvedbo GoogleCalendarAPI-ju o morebitnih novih dogodkih v Google koledarju trenutno prijavljenega premium študenta. V kolikor so novi dogodki, jih metoda shrani in osveži pogled.

#### GeneratorTODOSeznamov

* Predstavlja kontrolni razred, ki je zadolžen za generiranje seznama aktivnosti za namene prikaza na TO-DO seznamu.

##### Nesamoumevne metode

* `generirajTODOSeznam(datum:Date, Aktivnost[]) : Aktivnost[]` metoda kot parametra dobi datum za kateri dan je potrebno generirati TO-DO seznam in seznam vseh aktualnih aktivnosti (ki imajo končni datum v prihodnosti glede na podani datum) študenta. Metoda s pomočjo algoritma, ki vsaki aktivnosti dodeli število točk, uredi seznam aktivnosti naraščajoče, nato pa vrne vse aktivnosti, ki imajo manj kot 21 točk. Algoritem je sledeč: točke(aktivnost: Aktivnost) = (aktivnost.datum - datum) + 2 * (10 - aktivnost.obseg). Tako se aktivnosti z največjim obsegom pojavijo na seznamu že 3 tedne pred rokom in nas seznam opozori, da naj začnemo dovolj zgodaj, aktivnosti z najlažjo težavnostjo 1 pa se na seznamu pojavijo 3 dni pred rokom, saj ne zahtevajo veliko dela.

## 3. Načrt obnašanja

Vidik interaktivnega obnašanja sistema je prikazan s spodnjimi diagrami zaporedja, ki prikazujejo vse funkcionalnosti in njihove tokove. Alternativi tokovi obrobljeni z oznako _"alt."_, izjemni pa z oznako _"izj."_. Parametri vseh metod so zaradi preglednosti izpuščeni, saj so že opisani v poglavju _2. Načrt strukture_.

### 1. Ustvarjanje aktivnosti 
![Diagram zaporedja Ustvarjanje aktivnosti](../img/diagrami_zaporedja/1_Ustvarjanje_aktivnosti.svg)

### 2. Urejanje aktivnosti
![Diagram zaporedja Urejanje aktivnosti](../img/diagrami_zaporedja/2_Urejanje_aktivnosti.svg)
#### Izjemna tokova
![Diagram zaporedja Urejanje aktivnosti - izjemni](../img/diagrami_zaporedja/2_Urejanje_aktivnosti_izjemni_tokovi.svg)

### 3. Brisanje  aktivnosti 
![Diagram zaporedja Ustvarjanje aktivnosti](../img/diagrami_zaporedja/3_Brisanje_aktivnosti.svg)

### 4. Pregled aktivnosti v določenem časovnem obdobju v obliki koledarja
![Diagram zaporedja Pregled aktivnosti v določenem časovnem obdobju v obliki koledarja](../img/diagrami_zaporedja/4_Pregled_aktivnosti_v_določenem_časovnem_obdobju_v_obliki_koledarja.svg)

### 5. Prijava uporabnika 
![Diagram zaporedja Prijava uporabnika](../img/diagrami_zaporedja/5_Prijava_uporabnika.svg)

### 6. Odjava uporabnika 
![Diagram zaporedja Odjava uporabnika](../img/diagrami_zaporedja/6_Odjava_uporabnika.svg)

### 7. Registracija
![Diagram zaporedja Registracija](../img/diagrami_zaporedja/7_Registracija.svg)

### 8. Pregled TO-DO seznama
![Diagram zaporedja Pregled TO-DO seznama](../img/diagrami_zaporedja/8_Pregled_TO_DO_seznama.svg)

### 9. Vnos označbe napredka pri opravljanju aktivnosti
![Diagram zaporedja Vnos označbe napredka pri opravljanju aktivnosti](../img/diagrami_zaporedja/9_Vnos_označbe_napredka_pri_opravljanju_aktivnosti.svg)

### 10. Uvoz zunanjih urnikov in koledarjev 
![Diagram zaporedja Uvoz zunanjih urnikov in koledarjev](../img/diagrami_zaporedja/10_Uvoz_zunanjih_urnikov_in_koledarjev.svg)

### 11. Izvoz koledarja
![Diagram zaporedja Izvoz koledarja](../img/diagrami_zaporedja/11_Izvoz_koledarja.svg)

### 12. Iskanje aktivnosti glede na naziv in oznako
![Diagram zaporedja Iskanje aktivnosti glede na naziv in oznako](../img/diagrami_zaporedja/12_Iskanje_aktivnosti_glede_na_naziv_in_oznako.svg)

### 13. Avtomatsko resetiranje gesla
![Diagram zaporedja Avtomatsko resetiranje gesla](../img/diagrami_zaporedja/13_Avtomatsko_resetiranje_gesla.svg)

### 14. Spreminjanje podatkov uporabnikov
![Diagram zaporedja Spreminjanje podatkov uporabnikov](../img/diagrami_zaporedja/14_Spreminjanje_podatkov_uporabnikov.svg)

### 15. Izbris računa uporabnika
![Diagram zaporedja Izbris računa uporabnika](../img/diagrami_zaporedja/15_Izbris_računa_uporabnika.svg)

### 16. Vklop sinhronizacije z Google koledarjem
![Diagram zaporedja Vklop sinhronizacije z Google koledarjem 1](../img/diagrami_zaporedja/16_Vklop_sinhronizacije_z_Google_koledarjem.svg)
#### Izjemni tok
![Diagram zaporedja Vklop sinhronizacije z Google koledarjem 2](../img/diagrami_zaporedja/16_Vklop_sinhronizacije_z_Google_koledarjem.svg)

### 17. Izklop sinhronizacije z Google koledarjem
![Diagram zaporedja Izklop sinhronizacije z Google koledarjem](../img/diagrami_zaporedja/17_Izklop_sinhronizacije_z_Google_koledarjem.svg)

### 18. Pregled predmetov z vpisanimi ocenami
![Diagram zaporedja Pregled predmetov z vpisanimi ocenami](../img/diagrami_zaporedja/18_Pregled_predmetov_z_vpisanimi_ocenami.svg)

### 19. Vnos ocene pri predmetih
![Diagram zaporedja Vnos ocene pri predmetih](../img/diagrami_zaporedja/19_Vnos_ocene_pri_predmetih.svg)

### 20. Izbris ocene pri predmetih
![Diagram zaporedja Izbris ocene pri predmetih](../img/diagrami_zaporedja/20_Izbris_ocene_pri_predmetih.svg)

### 21. Izbris predmeta na seznamu predmetov
![Diagram zaporedja Izbris predmeta na seznamu predmetov](../img/diagrami_zaporedja/21_Izbris_predmeta_na_seznamu_predmetov.svg)

### 22. Prikaz oglaševalskih vsebin
![Diagram zaporedja Izklop sinhronizacije z Google koledarjem](../img/diagrami_zaporedja/22_Prikaz_oglaševalskih_vsebin.svg)
