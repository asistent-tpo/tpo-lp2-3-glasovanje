# Dokument zahtev

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Denis Derenda Cizel, Gaj Lipovšek, Metej De Faveri, Jure Bevc |
| **Kraj in datum** | Ljubljana, april 2019 |



## Povzetek projekta


V tem dokumentu so predstavljene zahteve projekta StraightAs. Opredeljeni so pojmi in uporabniške vloge, ki so predvidene za aplikacijo, skupaj s primeri uporabe in specifičnimi funkcionalnimi ter nefunkcionalnimi zahtevami. Prav tako so predstavljeni prototipi uporabniških in sistemskih vmesnikov, ki so osnova za vse interakcije naše aplikacije.



## 1. Uvod

Aplikacija rešuje problem spremljanja obveznosti posameznika, konkretno študenta. Uporabnik bo preko spletnega vmesnika uporabljal več funkcij aplikacije, ki bodo olajšale organizacijo obveznosti. Prva funkcionalnost, ki bo predstavljena uporabniku, bo prijava. Študent, profesor ali asistent bo moral napisati ustrezno uporabniško ime in geslo, ki ju je določil ob registraciji. Prijava bo nudila tudi možnost, da uporabnik ostane prijavljen v sistem, nato pa bo po pritisku na gumb za prijavo vstopil v aplikacijo. V primeru, da je uporabnik pozabil geslo, bo na strani za prijavo lahko kliknil na gumb za ponastavitev gesla, kjer bo vpisal elektronski naslov, na katerega bo prejel povezavo za vpis novega gesla.

Za uspešno prijavo mora uporabnik vnaprej registrirati račun v našem sistemu. To bo storil na strani za registracijo, kjer bo vpisal nekaj svojih osnovnih podatkov. Stran za registracijo bo dostopna preko gumba za registraicjo na strani za prijavo. Registracija bo zahtevala obvezne podatke kot so ime, priimek, geslo in elektronski naslov, prav tako pa bodo na voljo opcijska polja za vpisno in telefonsko številko. Za uspešno registracijo mora uporabnik klikniti povezavo, ki jo bomo poslali na njegov elektronski naslov in za tem se bo lahko prijavil v aplikacijo.

Znotraj aplikacije je uporabniku prikazan koledar, ki vsebuje vse obveznosti uporabnika. Uporabnik lahko doda obveznost s klikom na gumb za dodajanje obveznosti, ki odpre novo okno za vnos podatkov. Obveznost potrebuje ime, datum začetka in konca, prioriteto in opomnik, če uporabnik želi, da prejme obvestilo ob določenem času. Uporabnik lahko označi, da obveznost pripada določenemu predmetu in lahko doda opravila, ki jih ta obveznost zajema. Obveznost se lahko deli z drugimi uporabniki, s tem da je potrebna potrditev o sodelovanju z njihove strani.

Poleg koledarja je vsekemu uporabniku prikazan tudi seznam opravil, ki je lahko dnevni, tedenski ali mesečni seznam različnih obveznosti, ki mu pripadajo. S tem ima vsak uporabnik organiziran pregled nad opravljenim in neopravljenim delom v določenem časovnem obdobju.

Podobno kot obveznost lahko uporabnik ustvari tudi predmet. Vsak uporabnik lahko ustvari predmet in s tem se mu dodeli status profesorja pri dodanem predmetu. V predmet lahko doda druge uporabnike kot asistente in obveznosti, ki temu predmetu pripadajo. Asistenti imajo delež pravic, ki jih ima profesor predmeta in imajo možnost vnašanja komentarjev, točk in opomnikov pri predmetih. Pri urejanju predmeta lahko uporabnik zahteva, da ga potrdi administrator, ki preveri, če je lastnik uradni vodja predmeta na fakulteti in s tem označi predmet kot "uradnega", kar omogoča, da se predmet poveže s sistemom eUčilnica.

Uporabnik ima lahko pri različni predmetih različne vloge. Pri predmetih, ki jih je ustvaril sam ima status profesorja, pri nekaterih se mu lahko dodeli status asistenta, pri ostalih pa ima status študenta. Asistent in profesor lahko vpisujeta ocene pri svojih predmetih, ki so nato vidne študentom. Možen je tudi vpis krajšega komentarja in števila točk.

Vsaki uporabniški vlogi pripadajo določene pravice in s tem omejen dostop do funkcionalnosti aplikacije. Naš sistem zato ne sme omogočati dostop do podatkov, če uporabnik nima dodeljenih potrebnih pravic. Prav moramo zagotoviti, da je naš sistem implementiran v skladu s spločno uredbo EU o varstvu podatkov in Googlovimi smernicami za kvaliteto. Za udobno uporabo mora naš sistem biti na voljo vsaj 95 odstotkov časa in mora na vsako zahtevo odgovoriti v največ 500ms.

Aplikacija mora biti uporabna na vseh modernih brskalnikih in na voljo v slovenskem in angleškem jeziku.


## 2. Uporabniške vloge

Spodaj so opredeljene vse predvidene uporabniške vloge:

* **študent** - v aplikacijo je vpisan s svojo vpisno številko, lahko se vpisuje v predmete, ureja svoj koledar in opomnike
* **asistent** - lahko vnaša komentarje, točke in opomnike pri predmetih
* **profesor** - enako kot **asistent**, le da lahko ustvari in ureja predmet
* **administrator** - skrbi za potrjevanje predmetov


## 3. Slovar pojmov


* **opravilo** - je manjši cilj znotraj **obveznosti**.
* **obveznost** - je večji cilj, ki ga študent mora doseči. Obveznost je lahko sestavljena iz več **opravil** in je povezana z določenim **predmetom**.
* **seznam opravil** - je skupina časovno bližnjih **opravil**, ki pripada uporabniku. Vsebuje lahko opravila iz več različnih obveznosti. Seznam je lahko dnevni, tedenski ali mesečni. 
* **koledar** - je osebni koledar uporabnika, kjer so prikazane njegove **obveznosti**, skupaj z vpisanimi **predmeti**, vajami in **aktivnostmi**. 
* **predmet** - je študijski predme, ki ga lahko ustvari **profesor** ali pa si ga uporabnik ročno doda na svoj **koledar**.
* **opomnik** - je določeno obvestilo, ki si ga nastavi uporabnik. Opomnik je lahko povezan z **opravilom** ali pa s poljubno **aktivnostjo**.

## 4. Diagram primerov uporabe

![Primeri uporabe](../img/usecase.png)

## 5. Funkcionalne zahteve

### Prijava v aplikacijo

Študent, asistent ali profesor se lahko prijavijo v aplikacijo. 

#### Osnovni tok

1. Študent, asistent ali profesor odpre aplikacijo.
2. Študent, asistent ali profesor v ustrezno polje vpiše svoje uporabniško ime.
3. Študent, asistent ali profesor v ustrezno polje vpiše geslo.
4. Študent, asistent ali profesor označi, če želi ostati prijavljen v sistem.
5. Študent, asistent ali profesor pritisne na gumb za prijavo.
6. Sistem prikaže začetno stran aplikacije.

#### Alternativni tok

Študent, asistent ali profesor lahko ob zadnji prijavi v sistem označi, da želi ostati prijavljen v sistem. V tem primeru aplikacija ob zagonu ne vpraša za uporabniško ime ter geslo, ampak takoj prikaže začetno stran aplikacije.


#### Izjemni tokovi

**Študent, asistent ali profesor vnese napačno uporabniško ime ali geslo.**

Pri vnosu prijavnih podatkov lahko pride do tipkarske napake in tako se uporabniško ime in geslo ne ujema s podatki o uporabnikih v podatkovni bazi. Aplikacija uporabnika v tem primeru obvesti, da je vnesel neustrezno kombinacijo in ga pozove, naj poskusi ponovno.

**Študent, asistent ali profesor pozabi svoje geslo.**  

Zgodi se, da lahko pozabiš geslo za vpis. Uporabnikom mora na prijavni strani biti na voljo gumb za ponastavitev gesla. S klikom nanj bi aplikacija uporabnika vprašala za njegov elektronski naslov, kamor bi prejel povezavo, na kateri lahko ponastavi svoje uporabniško geslo.

#### Posledice

Z uspešno prijavo dobi študent, asistent ali profesor dostop do njegovih zasebnih podatkov in dostop do podatkov, ki so bili z njim deljeni. Prav lahko uporablja funkcionalnosti, ki so mu na voljo v okviru njegove vloge.

#### Prioritete identificiranih funkcionalnosti

Must have

#### Sprejemni testi

* Prijavi se že z predhodno registriranim računom. Sistem bi nas moral uspešno prijaviti in prikazati koledar obveznosti.

* Prijavi se z neustrezno kombinacijo uporabniškega imena in gesla. Sistem nas obvesti, da prijava ni bila uspešna.

* Uporabnik je pozabil svoje geslo. Po kliku na gumb za ponastavitev gesla, se mu odpre polje za vnos elektronskega naslova, kamor mu sistem pošlje povezavo za ponastavitev.


### Registracija v aplikacijo

Študent, asistent ali profesor se mora pred pred prvo prijavo v sistem registrirati in podati nekaj osnovnih podatkov. 

#### Osnovni tok

1. Študent, asistent ali profesor klikne na gumb registracija.
2. Študent, asistent ali profesor v ustrezno polje vpiše svoje ime, priimek, elektronski naslov in geslo ter neobvezne podatke: vpisna številka, telefon.
3. Sistem uporabnika obvesti, da je za potrditev registracije potrebno klikniti na povezavo, ki mu je bila posredovana na vpisani elektronski naslov.
4. Študent, asistent ali profesor potrdi svojo istovetnost s klikom na povezavo v prejetem elektronskem sporočilu.
5. Študent, asistent ali profesor se lahko prijavi v aplikacijo.

#### Izjemni tokovi

**Vpisan je nesmiselen podatek**  

Pri vnosu podatkov sistem preveri, če polja ime in priimek vsebujejo le črke, če je elektronski naslov primerne oblike ter ali so v polju za telefon vnesene le številke. Če kateri izmed pogojev ni izpolnjen, sistem izpiše obvestilo o napaki pri vnosu in ne dovoli registracije uporabnika.

**Registracija z že registriranim emailom.**  

Pred ustvarjanjem novega računa aplikacija preveri, če še ne obstaja uporabik s tem elektronskim naslovom. V primeru da najde uporabnika, s tem naslovom, ga pozove, da se prijavi v sistem.

#### Posledice

Z registracijo uporabnik pridobi status študenta in dostop do pripadajočih funkcionalnosti aplikacije.

#### Prioritete identificiranih funkcionalnosti

Must have

#### Sprejemni testi

* Uporabnik vnese svoje pravo ime, priimek ter elektronski naslov, ki še ni registiran na nobenega uporabika, ter potrdi registracijo. V elektronski predal prejme povezavo za potrditev in nastavitev gesla. S tem ima uspešno kreiran račun v aplikaciji.

* Uporabnik vnese elektronski naslov, s katerim je že registiran. Uporabnik prejme obvestilo, da že ima obstoječ račun in ga povoze, da se prijavi v sistem.

### Dodajanje nove obveznosti 

Študent, asistent ali profesor lahko na svoj koledar doda lastno obveznost, ki ni odvisna od nobenega dogodka. 

#### Osnovni tok

1. Študent, asistent ali profesor na osnovnem zaslonu aplikacije klikne na gumb za dodajanje obveznosti.
2. Sistem mu odpre novo okno za vnos podatkov.
3. Študent, asistent ali profesor vnese ime, datum začetka in konca, opis, prioriteto in nastavi opomnik, če ga želi.
4. Študent, asistent ali profesor izbere, ali obveznost pripada kateremu izmed predmetov.
5. Študent, asistent ali profesor doda opravila, ki jih zajema ta obveznost.
6. Študent, asistent ali profesor izbere, s katerimi drugimi uporabniki aplikacije želi deliti obveznost.
7. Študent, asistent ali profesor shrani obveznost.
8. Aplikacija uporabnika vrne na osnovni zaslon.


#### Alternativni tok

Obveznost lahko v tvojem imenu doda tudi nekdo drug in te doda kot sodelavca pri obveznosti. Uporabniku se takšno povabilo prikaže kot obvestilo, ali želi zares sodelovati na tem projektu. Če uporabnik potrdi sodelovanje, ima popoln dostop do upravljanje obveznosti, prav tako je uvrščena na njegov koledar.

#### Izjemni tok

**Datum konca aktivnosti je pred datumom začetka aktivnosti**

Pred shranjevanjem nove aktivnosti sistem preveri, ali je vnesen datum smiseln. V primeru da uporabnik vnese datume, kjer bi se aktivnost prej končala kot začela, shranjevanje ni dovoljeno in sistem uporabnika pozove, da popravi svoj vnos.

#### Posledice

Obveznost je dodana in vidna na uporabnikovem koledarju.

#### Prioritete identificiranih funkcionalnosti

Must have

#### Sprejemni testi

* Uporabnik se prijavi v aplikacijo. Klikne na gumb dodaj obveznost. Vnese ime, datum aktivnost in kratek opis. Shrani obveznost in obveznost je vidna na njegovem koledarju in na seznamu opravil.

* Uporabnik1 ustvari obveznost in za sodelavca na obveznosti doda Uporabnik2. Uporabnik2 mora prejeti obvestilo, da je bil povabljen k obveznosti. Obvestilo sprejme in obveznost se vpiše v njegov koledar.


### Urejanje obveznosti

Študent, asistent ali profesor lahko ureja vse obveznosti, ki so v njegovi lasti - ali so vezane na predmet, pri katerem ima vlogo profesor ali asistent, ali je eden izmed sodelavcev na tej obveznosti ali pa je obveznost sam dodal v lastni koledar.

#### Osnovni tok

1. Študent, asistent ali profesor na koledarju poišče obveznost, ki jo želi spremeniti.
2. Študent, asistent ali profesor klikne na gumb za urejanje obveznosti.
3. Študent, asistent ali profesor lahko spremeni podrobnosti o obveznostih.
4. Študent, asistent ali profesor shrani spremembe
5. Aplikacija uporabnika vrne na osnovni zaslon.

#### Alternativni tok

Če imajo obveznosti deljeno lastništvo (so vezane na predmet oz. na več uporabnikov), je sprememba, ki jo opravi eden izmed lastnikov, vidna vsem njenim uporabnikom. Tako lahko npr. profesor ali asistent spremeni datum kolokvija, ki je dodan nekemu predmetu kot obveznost, sprememba pa bo opazna vsem, ki imajo to obveznost na svojem koledarju.

#### Izjemni tok

Enak primer kot pri dodanju nove obveznosti.


#### Posledice

Podatki o obveznosti so posodobljeni in na voljo vsem uporabnikom, ki dostopajo do te obveznosti.

#### Prioritete identificiranih funkcionalnosti

Should have

#### Sprejemni testi

* Študent odpre obveznost, ki jo želi popraviti. Zaradi povečanega obsega dela podaljša datum konca obveznosti za 3 dodatne dni. Ko shrani spremembo, se obveznost na koledarju prikaže pri popravljenih datumih. Sprememba je vidna vsem uporabnikom, ki sodelujejo na tej obveznosti.


### Dodajanje novega predmeta

Študent, asistent ali profesor lahko v aplikaciji ustvari nov predmet.

#### Osnovni tok

1. Študent, asistent ali profesor na osnovnem zaslonu aplikacije klikne gumb za dodajanje novega predmeta.
2. Sistem mu odpre novo okno za vnos podatkov.
3. Študent, asistent ali profesor vnese ime predmeta in termin izvajanja predmeta na urniku.
4. Študent, asistent ali profesor lahko doda še ostale uporabnike kot asistente.
5. Študent, asistent ali profesor lahko doda obveznosti pri tem predmetu (predvidene kolokvije, skupinske projekte, domače naloge).
6. Študent, asistent ali profesor označi ali želi, da se njegov predmet preveri pri administratorju in pridobi status uradnega predmeta.
7. Študent, asistent ali profesor shrani predmet.
8. Uporabnik, ki je prvi shranil nov predmet, pridobi vlogo profesorja pri tem predmetu.
9. Aplikacija uporabnika vrne na osnovni zaslon.


#### Izjemni tokovi

**Predmet s tem imenom pri tem profesorju že obstaja**

V izogib podvajanju predmetov, sistem pred shranjevanjem preveri, ali že obstaja predmet z istim imenom in ali ima uporabnik, ki ustvarja nov predmet, že slučajno ima vlogo profesorja pri tem predmetu. V tem primeru aplikacija ne pusti shranjevanja novega predmeta.

#### Posledice

V aplikacijo je dodan nov predmet, v katerega se lahko vpišejo ostali uporabniki.

#### Pogoji

Dodajanje novih predmetov je mogoče le registriranim uporabnikom.

#### Prioritete identificiranih funkcionalnosti

Must have

#### Sprejemni testi

* Študent želi odpreti nov predmet. Klikne na gumb in v vnosno polje vnese ime in termin izvajanja predmeta. Ker ne zahteva potrditve s strani administratorja, je predmet takoj ustvarjen. Viden mora biti tudi ostalim uporabnikom.

* Profesor želi pomagati študentom in se odloči, da ustvari nov uraden predmet. Vnese podatke in izbere potrditev s strani administratorja. Administrator je obveščen, da je prišla nova zahteva za uraden predmet. Administrator preveri, ali predmet in uporabnik, ki je ustvaril predmet, izpolnjujeta zahteve in potrdi predmet. Profesorju je od tega trenutka naprej na voljo uradno potrjen predmet, s katerim prosto razpolaga.


### Vpis na predmet
	
Študentje se lahko vpišejo na predmete, ki jih poslušajo.

#### Osnovni tok

1. Študent na osnovnem zaslonu aplikacije klikne gumb za prijavo na predmet.
2. Sistem mu odpre novo okno, kjer mu je na voljo seznam vseh predmetov.
3. Študent na abecednem seznamu ali s pomočjo iskalnika po imenu predmeta poišče predmet in v primeru, da je predmetov s tem imenom več, izbere tistega, ki ima pravilnega profesorja.
4. Študent izbere ta predmet in se vanj vpiše. S tem mu je na voljo dostop do vsebin predmeta, prav tako se na njegov koledar avtomatsko vpišejo vse obveznosti tega predmeta.
5. Aplikacija uporabnika vrne na osnovni zaslon.

#### Alternativni tok

V predmet lahko študente vpišejo tudi profesorji ali asistentje. Dodani uporabniki so o tem obveščemi z obvestilom v svoji aplikaciji, ki ga morajo potrditi, da se dejansko vpišejo v ta predmet. Na tak način študent pridobi popolnoma enake funkcionalnosti, kot če se sam vpiše v predmet.

#### Posledice

Uporabnik ima dostop do vseh podatkov in obveznosti, ki so vezane na določen predmet.

#### Prioritete identificiranih funkcionalnosti

Should have

#### Sprejemni testi

* Študent klikne na gumb za vpis na predmet. Z abecednega seznama poišče predmet, ki ga išče. Klikne nanj in izbere gumb za vpis. Po vpisu mu morajo biti na voljo vse obveznosti in podrobnosti o predmetu.

* Profesor pri predmetu želi v predmet vpisati vse študente, ki pri njem obiskujejo predavanja. Odpre polje za urejanje predmeta in vnese elektronske naslove študentov. Vsi vpisani študentje morajo prejeti obvestilo, da so bili povabljeni k vpisu. Če povabilo sprejmejo, so vpisani v predmet in lahko na svojem koledarju spremljajo vse obveznosti tega predmeta.

### Urejanje predmeta

Profesor lahko ureja vse podrobnosti o predmetu, če se te skozi semester spremenijo. Če želi, lahko predmet tudi izbriše.

#### Osnovni tok

1. Profesor poišče predmet, ki ga želi urejati.
2. Profesor klikne na gumb za urejanje predmeta.
3. Profesor popravi podrobnosti predmeta, ki jih želi spremeniti. V primeru da želi izbrisati predmet, mu je to na voljo s klikom na gumb, ki se pojavi po kliku na gumb za urejanje predmeta. Če klikne izbriši predmet, se po potrditvi predmet izbriše in se primer uporabe zaključi. Če je predmet potrjen s strani adimistratorja aplikacije ima možnost, da predmet poveže z predmetom na eUčilnici in s tem omogoči avtomatski prikaz ocen iz eUčilnici v aplikacijo.
4. Profesor uveljavi spremenjene podatke.

#### Izjemni tok

**Ime predmeta je prazno**

Če je ime predmeta spremenjeno na prazno besedo, sistem javi napako.

#### Posledice

Po zaključku primera uporabe so informacije o predmetu posodobljene ali pa je predmet izbrisan.

#### Prioritete identificiranih funkcionalnosti

Should have


#### Sprejemni testi

* Profesor klikne na predmet, ki želi urejati in popravi ustrezne podrobnosti. Po potrditvi sprememb, so te vidne na opisu predmeta. 

### Koledar

Študent, asistent ali profesor lahko pregleduje svoj osebni koledar. Koledar je urejen kronološko, in omogoča filtriranje po predmetih in prioritetah. Študent, asistent ali profesor lahko pogled na koledar spreminja glede na predmete ter prioritete. Prav tako lahko preko koledarja dostopa do vseh do sedaj dodanih obveznosti. 

#### Osnovni tok

1. Študent, asistent ali profesor se prijavi.
2. Sistem prikaže koledar, ki vsebuje vse do sedaj dodane obveznosti.
3. Študent, asistent ali profesor preveri, kaj ga čaka v prihajajočih dneh.

#### Prioritete identificiranih funkcionalnosti

Must have

#### Sprejemni testi

* Prijavi se v aplikacijo. Sistem pokaže osebni koledar uporabnika. 


### Dodajanje opomnikov

Študent lahko doda opomnik, ki ga opozori o nečemu, vendar sam po sebi ni viden v koledarju.

#### Osnovni tok

1. Študent pritisne na gumb za dodajanje opomnika.
2. Študent za opomnik določi datum, ko se bo opomnik sprožil, njegovo ime in opcijsko kratek opis.
3. Študent lahko določi tudi ali se bo opomnik sprožil le enkrat, ali v več intervalih in koliko časa.
4. Študent ima možnost opomnik kar pripeti v svojo vrstico z opozorili.
5. Študent z pritiskom na gumb shrani opomnik.
6. Sistem prikaže sporočilo o uspešnem dodajanju opomnika.

#### Alternativni tok

1. Študent izbere obveznost in pritisne gumb za dodajanje opomnika.
2. Študent izbere koliko časa pred začetkom obveznosti se bo opomnik sprožil.
3. Študent lahko določi tudi ali se bo opomnik sprožil le enkrat, ali v več intervalih in koliko časa.
4. Študent z pritiskom na gumb shrani opomnik.
5. Sistem prikaže sporočilo o uspešnem dodajanju opomnika.

#### Posledice

Uporabniku se bo prikazal opomnik glede na nastavitev.

#### Prioritete identificiranih funkcionalnosti

Should have

#### Sprejemni testi

* Izberi dodajanje opomnika, vpiši veljavne podatke (ime, opis) in izberi datum začetka, ter določi čase proženja. Opomnik se mora sprožiti ob ustreznem času. 

### Dodajanje ocen 

Asistent ali profesor lahko vpisujeta ocene za vsakega študenta po izteku opomnika za določeno dejavnost pri predmetu. Ocene je mogoče vnašati le za potrjene predmete.

#### Osnovni tok

1. Profesor ali asistent odpre preteklo obveznost.
2. Profesor ali asistent izbere dodajanje ocen.
3. Sistem prikaže okno z vsemi študenti, ki so sodelovali v tej obveznosti. Sistem mora dovoliti vnos ocen tudi preko excel datoteke za več študentov hkrati. 
4. Profesor ali asistent izbere ali bo dodal oceno, število točk ali samo komentar.
5. Profesor ali asistent vpiše izbrano vrednost.
6. Sistem sistem prikaže obvestilo, da so bile ocene dodane. 

#### Alternativni tok

Ocene se v predmet lahko vpišejo tudi avtomatsko, če je profesor pri tem predmetu vzpostavil povezavo med aplikacijo in eUčilnico. V tem primeru je postopek dodajanja ocen avtomatiziran. 

#### Izjemni tok

**Vnos neveljavne vrednosti točk**

Sistem opozori o neštevilskih vrednostih pri dodajanju točk. 

#### Pogoji

Obveznost se je morala že izteči, sicer funkcionalnost ni na voljo.

#### Posledice

Študentom je po dodajanju vidna ocena.

#### Prioritete identificiranih funkcionalnosti

Would have

#### Sprejemni testi

* Izberi preteklo obveznost in dodaj oceno. Po potrditvi oceno ta naj bi bila vidna vpisanemu študentu.

### Pregledovanje ocen

Študent lahko za vsak predmet pregleduje pretekle obveznosti in vpisane ocene za vsako, če so te na voljo. 
#### Osnovni tok

1. Študent odpre predmet.
2. Sistem prikaže vse obveznosti pri predmetu.
3. Študent ima možnost filtriranja obveznosti glede na to ali so že potekle in glede na to ali imajo vpisane ocene ali ne. 

#### Prioritete identificiranih funkcionalnosti

Would have

#### Sprejemni testi

* Izberi predmet in označi, da sistem prikaže pretekle obveznosti, ki imajo vpisane ocene. Vpisane ocene bi morale biti prikazane. 

### Potrjevanje predmetov 

Administrator lahko vsak predmet, ki je bil do sedaj ustvarjen, uradno potrdi. To omogoči da se predmet poveže z zunanjim sistemom eUčilnica in poleg imena dobi znak kljukice, ki študente obvešča, da je predmet potrjen s strani uredništva.

#### Osnovni tok

1. Administrator izbere predmet s seznama nepotrjenih predmetov.
2. Administrator s klikom potrdi predmet ali zavrne predmet.

#### Posledice

Predmet je potrjen in tako se je možno povezati s sistemom eUčilnica.

#### Prioritete identificiranih funkcionalnosti

Could have

#### Sprejemni testi

* Izberi predmet iz seznama nepotrjenih predmetov in ga potrdi. Profesor, ki je ustvaril ta predmet je s tem dobil vse funkcionalnosti. 


## 6. Nefunkcionalne zahteve

* Sistem uporabniku ne sme omogočiti dostopa do podatkov drugih uporabnikov, če mu niso deljeni.
* Sistem mora na vsako zahtevo odgovoriti v največ 500 ms. 
* Sistem mora biti na voljo vsaj 95 odstotkov časa.
* Sistem mora opraviti čim manj prenosa podatkov.
* Sistem mora biti na voljo v angleškem in slovenskem jeziku. 
* Sistem mora biti implementiran v skladu s splošno uredbo EU o varstvu podatkov (GDPR).
* Sistem mora slediti Googlovimi smernicami za kvaliteto.
* Sistem mora biti kompatibilen z vsemi modernimi brskalniki.
* Študentje se morajo za pravilno delovanje vseh funkcionalnosti identificirati s svojo vpisno številko. 


## 7. Prototipi vmesnikov

Za uporabo aplikacije, se bo uporabnik moral registrirati (preko prijavne strani). Za registracijo potrebuje **elektronski naslov v uporabi, vpisno številko ter telefonsko**.

![REGISTRATION SCREEN](../img/Register.png)

Sicer se obstoječi uporabnik prijavi v aplikacijo s svojim **e-naslovom in geslom**.

![LOGIN SCREEN](../img/Login.png)

Ob prijavi v aplikacijo se prikaže glavna stran, na kateri je osebni koledar ter seznami predmetov, obveznosti in opravil uporabnika. Seznam opravil je lahko dnevni, tedenski ali mesečni. Na vrhu zaslona je orodna vrstica, na koncu nje pa spustni meni na katerem se lahko izbira med profilom in nastavitvami. Pod njim so trije gumbi, vsak od njiju sproži različno pojavno okno. Prvo prikaže okno z dvema zavihkoma, eden je za vpisovanje v predmet iz seznama možnih predmetov, drugi pa za dodajanje predmeta. Drugo prikaže okno, na kateremu si uporabnik lahko ustvari novo obveznost. Tretji pa prikaže možnosti za dodajanje opomnikov in aktivnosti na koledar uporabnika.

![MAIN SCREEN](../img/Main.png)

Profil uporabnika. Tu si lahko uporabnik ureja svoje opomnike in opravila. Lahko izbira katere podatke preostali uporabniki aplikacije vidijo na njegovem profilu ter personalizira svojo profilno sliko.

![PROFILE SCREEN](../img/Profil.png)

V nastavitvah si uporabnik lahko zamenja geslo s predpostavko, da pozna obstoječe geslo. Nastavlja svoj primaren e-naslov in telefonsko številko. Si nastavi koliko časa pred rokom opomnikov naj aplikacija uporabnika opomne in na katere načine naj to stori.

![SETTS SCREEN](../img/Nastavitve.png)

Stran posameznega predmeta, na kateri ima uporabnik pregled nad predmetom.

![SUBJECT SCREEN](../img/Predmet.png)

# Vmesnik do zunanjega sistema  

Moodle ponuja Gradebook API za pridobivanje ocen iz učilnice. API dostopamo z uporabo datoteke gradelib.php, ki jo ponuja Moodle.  
Enostavna poizvedba ocen bi bila narejana na naslednji način:
  

```javascript

//$users == array of users
$grading_info = grade_get_grades($courseid, 'mod', $modname, $modinstance->id, array_keys($users));
 
$grade_item_grademax = $grading_info->items[0]->grademax;
foreach ($users as $user) {
    $user_final_grade = $grading_info->items[0]->grades[$user->id];
}
```