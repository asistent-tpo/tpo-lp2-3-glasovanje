# Načrt sistema

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Denis Derenda Cizel, Gaj Lipovšek, Metej De Faveri, Jure Bevc |
| **Kraj in datum** | Ljubljana, april 2019 |



## Povzetek

V tem dokumentu je predstavljen načrt sistema StraightAs. Arhitektura sistema je dokumentirana v obliki diagrama entitet in razmerij. Dokumentacija prikazuje tudi zasnovo arhitekture v logičnem in razvojnem pogledu.


Za tem je predstavljen načrt strukture sistema v obliki razrednega diagrama, kjer so opisani posamezni razredi in povezave med njimi. Vsak razred je opisan z pomenom, atributi in metode, ki jih implementira. V načrtu obnašanja je narisan diagram zaporedja, ki prikazuje tokove dogodkov zajetih v diagramu primerov uporabe. Diagrami zaporedja uporabljajo razrede in metode opisane v razrednem diagramu.



## 1. Načrt arhitekture

Spodaj je prikazan vzorec *model-pogled-krmilnik* našega sistema: 

| ![MVC diagram arhitekture sistema](../img/arhitektura-sistema-mvc.png) |
|:--:|
| *MVC vzorec sistema* |

Sistem je smiselno načrtovati v večih plasteh, ki so predstavljene v spodnjem diagramu: 

| ![Večplastni diagram arhitekture sistema](../img/arhitektura-sistema-plasti.png) |
|:--:|
| *Večplastna arhitektura sistema* |


## 2. Načrt strukture

### 2.1 Razredni diagram

| ![MVC diagram arhitekture sistema](../img/razredni-diagram.png) |
|:--:|
| *Razredni diagram* |

### 2.2 Opis razredov


#### Študent

* Osnovni uporabnik aplikacije

##### Atributi

* ime: String
* priimek: String
* email: String
* vpisnaStevilka: int
* telefon: String


##### Nesamoumevne metode

* pridobiOcene(predmet) - vrne vse ocene študenta pri podanem predmetu
* ustvariUporabniskoIme() - na podlagi imena študenta se ustvari uporabniško ime, ki se prikazuje v aplikaciji

#### Asistent

* Uporabnik s pravicami urejanja predmeta

##### Atributi

* Asistent ima enake atribute kot študent


##### Nesamoumevne metode

* Asistent deduje motode študenta

#### Profesor

* Uporabnik s pravicami urejanja predmeta in dodajanje asistentov

##### Atributi

* Profesor ima enake atribute kot asistent


##### Nesamoumevne metode

* Profesor deduje motode asistenta

#### Koledar

* Koledar uporabnika, ki vsebuje vse njegove obveznosti

##### Atributi

* steviloPredmetov: int
* steviloOpravil: int


##### Nesamoumevne metode

* prikaziObveznosti(user) - prikaže vse obveznosti uporabnika
* steviloZasedenihUr() - izračuna in vrne skupno število zasedenih ur v tednu
* steviloProstihUr() - izračuna in vrne število prostih ur v tednu

#### Predmet

* Predmet na katerega je lahko vpisanih več uporabnikov in je lahko sestavljeni iz večih obveznosti

##### Atributi

* ime: String
* predavalnica: String

##### Nesamoumevne metode

* vrniStudente() - vrne list vseh študentov, ki so vpisani na določen predmet
* vnesiOcene(ocene) - zabeleži podane ocene študentov, ki so jih dobili pri predmetu


#### Obveznost

* Obveznost študenta, ki lahko pripada določenemu predmetu in je lahko sestavljena iz večih opravil

##### Atributi

* naziv: String
* rok: Date
* prioriteta: Int



#### Opravilo

* Pripada določeni obveznosti in ima lahko nastavljen opomnik

##### Atributi

* naziv: String
* rok: Date
* prioriteta: Int


##### Nesamoumevne metode

* opravljeno() - označi opravilo kot opravljeno

#### Opomnik

* Nastavljeno obvestilo o določenem opravilu

##### Atributi

* sporocilo: String
* cas: Date


##### Nesamoumevne metode

* sproziOpomnik() - pošlje obvestilo uporabniku, kateremu pripada opomnik

#### eUcilnica

* Zunanji razred za pridobivanje informacij o uradnem predmetu

##### Atributi

* Razred implementira metode in uporablja atribute zunanje knjižnice, ki je opisana v zajemu zahtev
* pridobiOcene() - pridobi ocene iz spletne učilnice predmeta

##### Nesamoumevne metode

* Razred implementira metode zunanje knjižnice za pridobivanje ocen uradno potrjenih predmetov

#### Administrator

* Priviligiran uporabnik, ki lahko potrjuje predmete in jih s tem označi kot 'uradne'

##### Atributi

* steviloPotrjenihPredmetov: int


##### Nesamoumevne metode

* dodajZahtevo(uporabnik, predmet) - metoda sprejme predmet, ki ga določen uporabnik želi narediti uradnega. Predmet se doda v čakalno v vrsto administratorja in se odstrani, ko ga administrator potrdi ali zavrne.


## 3. Načrt obnašanja

**Diagram zaporedja za primer uporabe Prijava**

![Diagram zaporedja za primer uporabe Prijava](../img/prijava.png)

**Diagram zaporedja za primer uporabe Registracija**

![Diagram zaporedja za primer uporabe Registracija](../img/registracija.png)

**Diagram zaporedja za primer uporabe Pregled koledarja**

![Diagram zaporedja za primer uporabe Pregled koledarja](../img/pregledkoledarja.png)

**Diagram zaporedja za primer uporabe Dodajanje obveznosti**

![Diagram zaporedja za primer uporabe Dodajanje obveznosti](../img/dodajanjeobveznosti.png)

**Diagram zaporedja za primer uporabe Urejanje obveznosti**

![Diagram zaporedja za primer uporabe Urejanje obveznosti](../img/urejanjeobveznosti.png)

**Diagram zaporedja za primer uporabe Vpis na predmet**

![Diagram zaporedja za primer uporabe Vpis na predmet](../img/vpisnapredmet.png)

**Diagram zaporedja za primer uporabe Potrditev predmeta**

![Diagram zaporedja za primer uporabe Potrditev predmeta](../img/potrditev_predmeta.png)

**Diagram zaporedja za primer uporabe Dodajanje predmeta**

![Diagram zaporedja za primer uporabe Dodajanje predmeta](../img/dodaj_predmet.png)

**Diagram zaporedja za primer uporabe Urejanje predmeta**

![Diagram zaporedja za primer uporabe Urejanje predmeta](../img/uredi_predmet.png)

**Diagram zaporedja za primer uporabe Pregled predmeta**

![Diagram zaporedja za primer uporabe Pregled predmeta](../img/pregled_predmeta.png)

**Diagram zaporedja za primer uporabe Pregled ocen**

![Diagram zaporedja za primer uporabe Pregled ocen](../img/pregled_ocen.png)

**Diagram zaporedja za primer uporabe Dodajanje ocen**

![Diagram zaporedja za primer uporabe Dodajanje ocen](../img/dodaj_ocene.png)

**Diagram zaporedja za primer uporabe Dodajanje opomnikov**

![Diagram zaporedja za primer uporabe Dodajanje opomnikov](../img/dodaj_opomnik.png)
